//
//  LocationSearchResult.swift
//  dr.CAFE Coffee
//
//  Created by Creative Solutions on 9/22/20.
//  Copyright © 2020 Ravi Nadendla. All rights reserved.
//

import Foundation
class LocationSearchResult {
    
    var mainText,secondaryText,id: String!
    
    init(mainText: String,secondaryText: String,id: String) {
        self.mainText = mainText
        self.secondaryText = secondaryText
        self.id = id
    }
}
