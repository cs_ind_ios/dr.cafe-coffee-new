//
//  LocationAutocompleteVC.swift
//  drCafe
//
//  Created by Creative Solutions on 12/18/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftUI
import GoogleMaps
import GooglePlaces

protocol LocationAutocompleteVCDelegate {
    func didTapAction(latitude:Double,longitude:Double,MainAddress:String, TotalAddress:String,Status:String)
}
class LocationAutocompleteVC: UIViewController, CLLocationManagerDelegate {
    
    var locationAutocompleteVCDelegate:LocationAutocompleteVCDelegate!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var allowBtn: UIButton!
    @IBOutlet weak var topMsgLbl: UILabel!
    @IBOutlet weak var orNameLbl: UILabel!
    
    var isSearch : Bool = false
    var isCancel:Bool = false
    var countryCode = "SA"
    var whereObj = 1
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var isFirst = false
    
    var currentLat: Double!
    var currentLong: Double!
    var currentLocationAddress: String!
    var locationSearch: [LocationSearchResult] = []
    var app = AppDelegate()
    
    var isPopup = false
    var isBanner = false
    var bannerId = 0
    var bannerDetailsArray = [BannerDetailsModel]()
    var selectBannerDetails = [BannerDetailsModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        app = UIApplication.shared.delegate as! AppDelegate
        tableView.delegate = self
        tableView.dataSource = self
        
        searchTF.addTarget(self, action: #selector(searchTextChanged(sender:)), for: .editingChanged)
        searchTF.delegate = self
        
        searchTF.inputAccessoryView = UIView()
        
        if whereObj == 1 || whereObj == 2 || whereObj == 10 || whereObj == 3{//1&2 Stores Screen and 10- Dashboard screen, 3-More screen
            backBtn.isHidden = false
            tabBarController?.tabBar.isHidden = true
        }else{
            backBtn.isHidden = true
            tabBarController?.tabBar.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            allowBtn.backgroundColor = .white
            allowBtn.setTitleColor(.black, for: .normal)
            topMsgLbl.textColor = .white
            orNameLbl.textColor = .white
        }else{
            allowBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            allowBtn.setTitleColor(.white, for: .normal)
            topMsgLbl.textColor = .darkGray
            orNameLbl.textColor = .darkGray
        }
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        isFirst = true
        locationCheck()
    }
    func locationCheck(){
        if !hasLocationPermission() {
            
        }else{
            if whereObj == 3{
                self.locationAutocompleteVCDelegate.didTapAction(latitude: 0.0, longitude: 0.0, MainAddress: "", TotalAddress: "", Status: "Allow")
                self.navigationController?.popViewController(animated: false)
            }else if whereObj == 10{
                self.locationManager = CLLocationManager()
                self.locationManager.delegate = self
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
                self.locationManager.startMonitoringSignificantLocationChanges()
            }
        }
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        if whereObj == 0{
            self.dismiss(animated: false, completion: nil)
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        currentLat = (location?.coordinate.latitude)!
        currentLong = (location?.coordinate.longitude)!
        DispatchQueue.main.async {
            if self.isFirst == true{
                if self.isBanner == true && self.isPopup == false{
                    self.getBannerDetailsService(action: 1)
                }else if self.isBanner == false && self.isPopup == true{
                    self.getBannerDetailsService(action: 2)
                }
            }
        }
        self.locationManager.stopUpdatingLocation()
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "selectLocationComeback"), object: nil)
        self.locationAutocompleteVCDelegate.didTapAction(latitude: 0.0, longitude: 0.0, MainAddress: "", TotalAddress: "", Status: "Back")
        if whereObj == 3 || whereObj == 10{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    @IBAction func allowBtn_Tapped(_ sender: Any) {
        if whereObj == 10 || whereObj == 3{
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }else{
            self.locationAutocompleteVCDelegate.didTapAction(latitude: 0.0, longitude: 0.0, MainAddress: "", TotalAddress: "", Status: "Allow")
            self.dismiss(animated: false, completion: nil)
        }
    }
    @IBAction func searchBtn_Tapped(_ sender: Any) {
        searchTF.resignFirstResponder()
        tableView.isHidden = true
        print(searchTF.text!)
        var language = "en"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "en"
        }else{
            language = "ar"
        }
        CartModuleServices.LocationGetService(url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTF.text!)&components=country:\(countryCode)&language=\(language)&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM", success: { (Data) in
            print(Data)
            self.locationSearch.removeAll()
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["predictions"] as! NSArray
            for i in 0..<(data ).count {
                let dic = ((data)[i] as! NSDictionary)["structured_formatting"] as! NSDictionary
                self.locationSearch.append(LocationSearchResult(mainText: dic["main_text"] as! String, secondaryText: dic["secondary_text"] == nil ? "" : dic["secondary_text"] as! String, id: ((data )[i] as! NSDictionary)["place_id"] as! String))
            }
            if self.searchTF.text! != "" {
                self.tableView.reloadData()
                self.tableView.isHidden = false
            }
        }) { (error) in
            print(error)
        }
    }
}
//MARK: TableView Delegate Methods
extension LocationAutocompleteVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "LocationAddressTVCell", bundle: nil), forCellReuseIdentifier: "LocationAddressTVCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationAddressTVCell", for: indexPath) as! LocationAddressTVCell
        let selectedItem = locationSearch[indexPath.row]
        cell.titleLbl.text = selectedItem.mainText
        cell.addressLbl.text = selectedItem.secondaryText
        self.currentLocationAddress = selectedItem.secondaryText
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.titleLbl.textAlignment = .left
            cell.addressLbl.textAlignment = .left
        }else{
            cell.titleLbl.textAlignment = .right
            cell.addressLbl.textAlignment = .right
        }
        
        tableView.layoutIfNeeded()
        cell.selectionStyle = .none
        tableViewHeightConstraint.constant = tableView.contentSize.height
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = locationSearch[indexPath.row]
        CartModuleServices.LocationGetService(url: "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(selectedItem.id!)&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM", success: { (Data) in
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["result"] as! NSDictionary
            let coordinates = ((data )["geometry"] as! NSDictionary)["location"] as! NSDictionary
            let address = ((data )["formatted_address"] as! String)
            let name = ((data )["name"] as! String)
            self.currentLat = (coordinates["lat"] as! Double)
            self.currentLong = (coordinates["lng"] as! Double)
            self.app.currentlocation = CLLocation(latitude: self.currentLat!, longitude: self.currentLong!)
            let dic = ["latitude":self.currentLat!,
                       "longitude":self.currentLong!,
                       "Address" :address,
                       "Name":name] as [String : Any]
            UserDefaults.standard.set(dic, forKey:"SelectLocation")
            UserDefaults.standard.synchronize()
            self.app.manualLocationStr = "Done"
            if self.whereObj != 10{
                self.locationAutocompleteVCDelegate.didTapAction(latitude: self.currentLat!, longitude: self.currentLong!, MainAddress: "\(String(describing: selectedItem.mainText!))", TotalAddress: "\(String(describing: selectedItem.mainText!)) \(String(describing: selectedItem.secondaryText!))", Status: "Manual")
                self.locationSearch.removeAll()
                DispatchQueue.main.async {
                    if self.whereObj == 3{
                        NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.dismiss(animated: true) {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                        }
                    }
                }
            }else{
                self.locationSearch.removeAll()
                if self.isBanner == true && self.isPopup == false{
                    self.getBannerDetailsService(action: 1)
                }else if self.isBanner == false && self.isPopup == true{
                    self.getBannerDetailsService(action: 2)
                }
            }
        }){ (error) in
            print(error)
        }
    }
    //MARK: DashBoard Banner Details Get Service
    func getBannerDetailsService(action:Int){
        if isFirst == true{
            isFirst = false
        }
        var originLat = String(format: "%.6f", currentLat)
        var originLong = String(format: "%.6f", currentLong)
        if currentLat > 0 && currentLong > 0{
            originLat = String(format: "%.6f", currentLat)
            originLong = String(format: "%.6f", currentLong)
        }else{
            originLat = ""
            originLong = ""
        }
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        let dic = ["UserId": UserDef.getUserId(),"AppVersion" : "\(appversion)","AppType": "IOS","DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2, "IsSubscription":false, "Latitude":originLat,"Longitude":originLong, "action" : action] as [String : Any]
        OrderModuleServices.DashBoardService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }))
                }
            }else{
                DispatchQueue.main.async {
                    self.bannerDetailsArray = data.Data!.Result!
                    self.selectBannerDetails = self.bannerDetailsArray.filter({$0.Id == self.bannerId})
                    if self.selectBannerDetails.count > 0{
                        self.bannerSelection()
                        if self.isPopup == true{
                            self.popUpUpdateService()
                        }
                    }else{
                        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "Banner your looking for is not available in selected location", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func bannerSelection(){
        var orderTypeId:Int!
        if selectBannerDetails[0].DineId == true{
            orderTypeId = 1
        }else if selectBannerDetails[0].DriveThru == true{
            orderTypeId = 2
        }else if selectBannerDetails[0].TakeAway == true{
            orderTypeId = 3
        }else if selectBannerDetails[0].Delivery == true{
            orderTypeId = 4
        }
        if selectBannerDetails[0].StoreList != nil{
            if selectBannerDetails[0].StoreList!.count == 0{
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "Banner your looking for is not available in selected location", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }))
            }else if selectBannerDetails[0].StoreList!.count == 1{
                if selectBannerDetails[0].StoreList![0].StoreStatus == false{//Store is Closed
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }))
                }else{//Store is Open
                    for i in 0...selectBannerDetails[0].StoreList![0].BusySchedule!.count - 1{
                        if selectBannerDetails[0].StoreList![0].BusySchedule![i].OrderTypeId == orderTypeId{
                            if selectBannerDetails[0].StoreList![0].BusySchedule![i].IsBusy == true{
                                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(selectBannerDetails[0].StoreList![0].BusySchedule![i].OpensInMinute) mins", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                                    DispatchQueue.main.async {
                                        NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }))
                            }else{
                                AppDelegate.getDelegate().cartStoreId = self.selectBannerDetails[0].StoreList![0].Id
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    AppDelegate.getDelegate().cartStoreName = self.selectBannerDetails[0].StoreList![0].NameEn
                                }else{
                                    AppDelegate.getDelegate().cartStoreName = self.selectBannerDetails[0].StoreList![0].NameAr
                                }
                                AppDelegate.getDelegate().selectAddress = ""
                                if selectBannerDetails[0].ItemId == 0{// single store
                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                    var obj = ItemsVC()
                                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                                    obj.sectionType = 0
                                    obj.isV12 = false
                                    obj.selectOrderType = orderTypeId!
                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        obj.address = selectBannerDetails[0].StoreList![0].NameEn
                                    }else{
                                        obj.address = selectBannerDetails[0].StoreList![0].NameAr
                                    }
                                    obj.selectItemName = selectBannerDetails[0].BannerName
                                    obj.categoryId = selectBannerDetails[0].CategoryId
                                    obj.storeId = selectBannerDetails[0].StoreList![0].Id
                                    obj.selectAddressID = 0
                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        obj.selectItemName = selectBannerDetails[0].MainCategoryEn
                                        obj.categoryName = selectBannerDetails[0].CategoryEn
                                    }else{
                                        obj.selectItemName = selectBannerDetails[0].MainCategoryAr
                                        obj.categoryName = selectBannerDetails[0].CategoryAr
                                    }
                                    obj.isBanner = true
                                    obj.isManualLocation = true
                                    self.navigationController?.pushViewController(obj, animated: true)
                                }else{//single store and Single item
                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                    var obj = AdditionalsVC()
                                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
                                    obj.itemID = selectBannerDetails[0].ItemId
                                    obj.selectOrderType = orderTypeId!
                                    obj.sectionType = 0
                                    obj.storeId = selectBannerDetails[0].StoreList![0].Id
                                    obj.selectAddressId = 0
                                    obj.isV12 = false
                                    obj.isManualLocation = true
                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        obj.address = selectBannerDetails[0].StoreList![0].NameEn
                                        obj.titleLbl = selectBannerDetails[0].ItemEn
                                    }else{
                                        obj.address = selectBannerDetails[0].StoreList![0].NameAr
                                        obj.titleLbl = selectBannerDetails[0].ItemAr
                                    }
                                    self.navigationController?.pushViewController(obj, animated: true)
                                }
                            }
                        }
                    }
                }
            }else{//Multiple stores
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = StoresVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                obj.whereObj = 2
                obj.isBanner = true
                obj.orderType = orderTypeId!
                obj.isManualLocation = true
                obj.bannerId = selectBannerDetails[0].Id
                obj.storeDetailsArray = selectBannerDetails[0].StoreList!
                SaveAddressClass.selectBannerStores = [selectBannerDetails[0]]
                obj.itemId = selectBannerDetails[0].ItemId
                obj.categoryId = selectBannerDetails[0].CategoryId
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.itemTitle = selectBannerDetails[0].ItemEn
                    obj.categoryName = selectBannerDetails[0].CategoryEn
                }else{
                    obj.itemTitle = selectBannerDetails[0].ItemAr
                    obj.categoryName = selectBannerDetails[0].CategoryAr
                }
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            DispatchQueue.main.async {
                NotificationCenter.default.post(name:Notification.Name(rawValue: "selectLocationComeback"), object: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func popUpUpdateService(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId() ,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "PopupId":"\(bannerId)"]
        OrderModuleServices.PopUpUserVisibilityUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                print("Done")
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
//MARK: UITextField delegate
extension LocationAutocompleteVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("text cleared")
        textField.resignFirstResponder()
        isSearch = false
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //myScrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
        return true
    }
    @objc func searchTextChanged(sender: NSObject){
        if searchTF.text!.count > 0{
            myScrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
        }
        if searchTF.text != "" {
            tableView.isHidden = true
            print(searchTF.text!)
            var language = "en"
            if AppDelegate.getDelegate().appLanguage == "English"{
                language = "en"
            }else{
                language = "ar"
            }
            CartModuleServices.LocationGetService(url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTF.text!)&components=country:\(countryCode)&language=\(language)&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM", success: { (Data) in
                print(Data)
                self.locationSearch.removeAll()
                let Totaldata = Data as! NSDictionary
                let data = Totaldata["predictions"] as! NSArray
                for i in 0..<(data ).count {
                    let dic = ((data)[i] as! NSDictionary)["structured_formatting"] as! NSDictionary
                    self.locationSearch.append(LocationSearchResult(mainText: dic["main_text"] as! String, secondaryText: dic["secondary_text"] == nil ? "" : dic["secondary_text"] as! String, id: ((data )[i] as! NSDictionary)["place_id"] as! String))

                }
                if self.searchTF.text! != "" {
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                }
            }) { (error) in
                print(error)
            }
        }else{
            tableView.isHidden = true
        }
    }
}
