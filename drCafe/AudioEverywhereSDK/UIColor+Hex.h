//
//  UIColor+Hex.h
//  Basil
//
//  Created by Paula Chavarría on 4/10/14.
//  Copyright (c) 2014 Cecropia Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha;

@end
