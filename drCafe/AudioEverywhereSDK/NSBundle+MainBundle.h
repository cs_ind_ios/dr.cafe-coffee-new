//
//  NSBundle+MainBundle.h
//  Basil
//
//  Created by Paula Chavarría on 5/8/14.
//  Copyright (c) 2014 Cecropia Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (MainBundle)

+ (NSString *) getMainBundleAppVersionNumber;
+ (NSString *) appName;
@end
