//
//  NSString+Random.h
//  Basil
//
//  Created by Paula Chavarría on 1/12/15.
//  Copyright (c) 2015 Cecropia Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Random)

+ (NSString *) randomStringWithLength: (int) length;

@end
