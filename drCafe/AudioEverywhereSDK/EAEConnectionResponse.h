//
//  EAEConnectionResponse.h
//  AudioEverywhereSDK
//
//  Created by Miguel Hernandez on 10/28/14.
//  Copyright (c) 2014 ExXothermic. All rights reserved.
//

#ifndef AudioEverywhereSDK_EAEConnectionResponse_h
#define AudioEverywhereSDK_EAEConnectionResponse_h

typedef void (^EAEConnectionSuccessResponse)(NSArray *array);
typedef void (^EAEConnectionFailureResponse)(NSError *error);

#endif
