//
//  UIColor+Random.h
//  Basil
//
//  Created by Paula Chavarría on 11/5/14.
//  Copyright (c) 2014 Cecropia Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Random)

+ (UIColor *) random;

@end
