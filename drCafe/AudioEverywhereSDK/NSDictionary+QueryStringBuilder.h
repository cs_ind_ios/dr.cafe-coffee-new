//
//  NSDictionary+QueryStringBuilder.h
//  Basil
//
//  Created by Paula Chavarría on 4/11/14.
//  Copyright (c) 2014 Cecropia Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (QueryStringBuilder)

- (NSString *)queryString;

@end
