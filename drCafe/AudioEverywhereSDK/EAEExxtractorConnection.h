//
//  EAEExxtractorConnection.h
//  AudioEverywhereSDK
//
//  Created by Paula Chavarría on 10/22/14.
//  Copyright (c) 2014 ExXothermic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EAEConnectionResponse.h"
#import "EAEExxtractorAPIConstants.h"
#import "EAEConnectionConstants.h"


@protocol EAEExxtractorStreamRequesting;
@protocol EAEDiscoveredVenuePolling;
@protocol EAEDiscovering;
@protocol EAEExxtractorLocationRequesting;
@protocol EAEExxtractorDocumentRequesting;
@protocol EAEExxtractorContentRequesting;
@protocol EAEExxtractorAuthenticationRequesting;
@protocol EAEExxtractorStatsRequesting;
@protocol EAEExxtractorChannelRequesting;

@class EAEExxtractorWebService;
@class EAEFeaturesHelper;
@class EAEAudioChannel;
@class EAELocation;
@class BARestAdapter;

extern NSString * const kEAEAppStoreUrlKey;

@interface EAEExxtractorConnection : NSObject

@property (assign, nonatomic,readonly) NSInteger activeAudioChannelIndex;
@property (strong, nonatomic,readonly) EAEAudioChannel* activeAudioChannel;
@property (strong, nonatomic,readonly) NSArray* audioChannels;
@property (assign, nonatomic,readonly) BOOL isBusy;
@property (strong, nonatomic,readonly) NSArray* featuredContents;
@property (strong, nonatomic,readonly) EAEFeaturesHelper *featureVerifier;
@property (nonatomic) EAEExxtractorConnectionState state;
@property (strong, nonatomic) NSString *version;
@property (strong, nonatomic, readonly) NSString *partnerUniqueIdentifier;
@property (strong, nonatomic, readonly) NSString *exxtractorUniqueIdentifier;
@property (nonatomic) BOOL demoModeEnabled;
@property (nonatomic) BOOL isDemoInTransition;
@property (nonatomic) BOOL displayPrivateChannels;
@property (nonatomic, strong) NSString *isAdminEnabled;


- (id) initWithMainStreamWebService:(EAEExxtractorWebService<EAEExxtractorStreamRequesting> *) mainStreamWebService
             channelsPollingService:(id<EAEDiscoveredVenuePolling>) discoveredVenuePolling
                   discoveryService:(id<EAEDiscovering>) discoveryService
                 locationWebservice:(EAEExxtractorWebService<EAEExxtractorLocationRequesting>*)locationWebservice
                 documentWebservice:(EAEExxtractorWebService<EAEExxtractorDocumentRequesting>*)documentWebservice
                  contentWebservice:(EAEExxtractorWebService<EAEExxtractorContentRequesting>*)contentWebservice
                    statsWebservice:(EAEExxtractorWebService<EAEExxtractorStatsRequesting>*)statsWebservice
                  channelWebservice:(EAEExxtractorWebService<EAEExxtractorChannelRequesting>*)channelWebservice
                  authenticationWebservice:(EAEExxtractorWebService<EAEExxtractorAuthenticationRequesting>*)authenticationWebservice
              connectionRestAdapter:(BARestAdapter *) connectionRestAdapter;

+ (EAEExxtractorConnection *) exxtractorConnectionWithPartnerUniqueIdentifier:(NSString *)partnerUniqueIdentifier
                                                                     loglevel: (EAEExxtractorConnectionLogLevel) logLevel;
+ (instancetype) sharedExxtractorConnection;

- (void) scanWithTimeout: (int) timeout
                 success: (EAEConnectionSuccessResponse) success
                 failure: (EAEConnectionFailureResponse) failure;
- (void) connectToVenueServer: (NSString *) targetVenueServer
                  withTimeout: (int) timeout
                      success:(EAEConnectionSuccessResponse) success
                      failure: (EAEConnectionFailureResponse) failure;

- (void) startChannelPlaybackWithChannel: (EAEAudioChannel *) channel
                   playStereoIfAvailable: (BOOL) playStereoIfAvailable
                                 success: (EAEConnectionSuccessResponse) success
                                 failure: (EAEConnectionFailureResponse) failure;

- (void) stopChannelPlaybackWithSuccess: (EAEConnectionSuccessResponse) success
                                failure: (EAEConnectionFailureResponse) failure;

- (void) validatePrivateChannelsAndSync: (NSError **) error;

- (void) adminLogin: (NSString*) password
                         success: (EAEConnectionSuccessResponse) success
                         failure: (EAEConnectionFailureResponse) failure;

- (void) updateChannelsPrivacy: (NSArray*) channels
                        isPrivate: (BOOL) isPrivate
                        passphrase: (NSString*) passphrase
                        success: (EAEConnectionSuccessResponse) success
                        failure: (EAEConnectionFailureResponse) failure;
- (void) adminLogout;
- (void) startSyncingChannelsWithExxtractor: (NSError **) error
                                isPrivateChannelsEnabled: (BOOL) isPrivateChannelsEnabled
                             isAdminEnabled: (NSString*) isAdminEnabled;
                            

- (void) getLocationWithSuccess: (EAEConnectionSuccessResponse) success
                        failure: (EAEConnectionFailureResponse) failure;

- (void) getDocumentsWithSuccess: (EAEConnectionSuccessResponse) success
                         failure: (EAEConnectionFailureResponse) failure;

- (void) getOffersWithSuccess: (EAEConnectionSuccessResponse) success
                      failure: (EAEConnectionFailureResponse) failure;

- (void) stopAndResetManager;

- (void) restartPlayback: (BOOL) playStereoIfAvailable;

- (void) dialogEnhacementShouldBeEnabled: (BOOL) enabled;

- (void) disconnectWithNotification: (NSString*) notificationName
                           demoMode:(BOOL) isDemoModeEnabled;
- (void) disconnect;
- (BOOL) isExxtractorConnected;

#pragma mark - Connection history methods
- (void) saveConnection:(NSString *) serverConnection;
- (NSArray *) getConnections;
- (void) clearConnections;
- (void) startWebSocket;
@end
