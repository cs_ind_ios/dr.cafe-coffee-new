//
//  NSArray+Resource.h
//  Basil
//
//  Created by Paula Chavarría on 9/16/13.
//  Copyright (c) 2013 Cecropia Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Resource)

+ (NSArray *) arrayFromResourceWithName: (NSString *) resource;

@end
