//
//  Reachability+AudioEverywhere.h
//  Basil
//
//  Created by Paula Chavarría on 8/12/14.
//  Copyright (c) 2014 ExXothermic. All rights reserved.
//

#import "Reachability.h"

@interface Reachability (Google)

+ (BOOL) isGoogleReachable;

@end
