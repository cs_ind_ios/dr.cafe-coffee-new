//
//  UserDefaults.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 1/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
func getJWT() -> String{
    return UserDefaults.standard.value(forKey: "JWT") == nil ? "Bearer" : "Bearer \(UserDefaults.standard.value(forKey: "JWT")!)"
}
func isUserLogIn() -> Bool {
    return UserDefaults.standard.value(forKey: "UserId") == nil ? false : true
}
func getMobileNumber() -> String{
    return UserDefaults.standard.value(forKey: "Mobile") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "Mobile")!)"
}
func getSTCMobileNumber() -> String{
    return UserDefaults.standard.value(forKey: "STCMobile") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "STCMobile")!)"
}
func isSelectCategory() -> Bool {
    return UserDefaults.standard.value(forKey: "Category") == nil ? false : true
}
func isLocationArray() -> Bool {
    return UserDefaults.standard.value(forKey: "LocationDetails") == nil ? false : true
}
func isDeviceToken() -> Bool {
    return UserDefaults.standard.value(forKey: "DeviceToken") == nil ? false : true
}
func iskVERSION() -> Bool {
    return UserDefaults.standard.value(forKey: "kVERSION") == nil ? false : true
}
func getDeviceLanguage() -> String {
    var language = Bundle.main.preferredLocalizations[0]
    language = language.components(separatedBy: "-")[0]
    if (language == "en") {
         return "English"
    }
    return "Arabic"
}
func getDeviceInfo()-> [String:Any]{
    var language = "Ar"
    if AppDelegate.getDelegate().appLanguage == "English"{
        language = "En"
    }
    var timeZone = ""
        if let _ = TimeZone.current.abbreviation() {
        timeZone = TimeZone.current.abbreviation()!
    }
    let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
    let deviceInfo:[String:Any] = [
        "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
        "applang":language,
        "OSversion":"IOS(\(UIDevice.current.systemVersion))",
        "SDKversion":"",
        "Modelname":"\(UIDevice.current.name)",
        "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
        "AppVersion":"IOS(\(appVersion))",
        "TimeZone":timeZone,
        "TimeZoneRegion":"\(TimeZone.current.identifier)"]
    return deviceInfo
}
func getUserDetails() -> signupModel {
    let dic = UserDefaults.standard.value(forKey: "userDic") as! [String:Any]
    let signUpDic:signupModel = signupModel.init( UserId: dic["UserId"] as! Int, FullName: dic["FullName"] as! String, Email: dic["Email"] as! String, Mobile: dic["Mobile"] as! String)
    return signUpDic
}

// remove from UserDefault
func removeFromUserDefaultForKey(key:String){
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
    
}
func isMaterialShowcase() -> Bool {
    return UserDefaults.standard.value(forKey: "MaterialShowcase") == nil ? false : true
}
