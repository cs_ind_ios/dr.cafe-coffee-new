//
//  AppUtility.swift
//  Samkra
//
//  Created by RAVI on 25/10/18.
//  Copyright © 2018 RAVI. All rights reserved.
//

import Foundation
import UIKit

class AppUtility{
        //Method for get Build Version and Build Number
        class func methodForApplicationVersion() ->String{
            let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
            let version = nsObject as! String
            
            let buildNumber: AnyObject? =  Bundle.main.infoDictionary?["CFBundleVersion"] as AnyObject?
            let number = buildNumber as! String
            
            let stringApplicationVersion = version+"("+number+")"
            return stringApplicationVersion
        }
        class func getApplicationVersion() ->String{
            let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
            let version = nsObject as! String
            return version
        }
        
        //Method for change StatusBar Color
        class func methodStatusBarColorChange() {
            UIApplication.shared.isStatusBarHidden = false
            let statusBar:UIView = UIApplication.shared.value(forKey: "statusBar")as!UIView
            statusBar.backgroundColor = UIColor(red: 0.255, green: 0.314, blue: 0.631, alpha: 1.0)
        }   
}
func getAppverson() -> String {
    return UserDefaults.standard.value(forKey: "AppVerson") == nil ? "0" : UserDefaults.standard.value(forKey: "AppVerson") as! String
}
