//
//  Validations.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/21/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class Validations: NSObject {
    static var menuSelectedName = String()
    
//    static func isValidEmail(email:String) -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: email)
//    }
    static func isValidEmail(email:String) -> Bool {
        let stricterFilter = false
        let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        let emailRegex = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidPasswordWithRG(password:String) -> Bool {
        let pwdRegEx = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", pwdRegEx)
        return emailTest.evaluate(with: password)
    }
    static func isSTCMobileNumberValid(mobile : String) -> Bool{
        if mobile.count == 12{
            return true
        }
        return false
    }
    static func isPasswordValid(password : String) -> Bool{
        if (password.count >= 4) && (password.count <= 20){
            return true
            }
        return false
    }
    static func isNameValid(name : String) -> Bool{
        if (name.count >= 2) && (name.count <= 50){
            return true;
        }
        return false;
    }
    static func isCharCount(name : String) -> Bool{
        if (name.count >= 2) && (name.count <= 50){
            return true
        }
        return false
    }
    static func isZipCount(zip : String) -> Bool{
        if zip.count == 6{
            return true
        }
        return false
    }
    static func isOTPValid(otp : String) -> Bool{
        if otp.count == 4{
            return true
        }
        return false
    }
    static func isMobileNumberValid(mobile : String, numOfDifits : Int) -> Bool{
        if mobile.count == numOfDifits{
            return true
        }
        return false
    }
    static func isMobileNumberValidWithCountryCode(mobile : String) -> Bool{
        if mobile.count == 14{
            return true
        }
        return false
    }
    static func isMobileNumberAddressValid(mobile : String) -> Bool{
        if mobile.count >= 9 ||  mobile.count <= 12{
            return true
        }
        return false
    }
    static func isMobileNumberValidWithNumOfDigits(mobile : String, minDigits : Int, maxDigits : Int) -> Bool{
        if mobile.count >= minDigits &&  mobile.count <= maxDigits{
            return true
        }
        return false
    }
}

