//
//  DateConverts.swift
//  IKSAB
//
//  Created by Creative Solutions on 10/4/19.
//  Copyright © 2019 creative solutions. All rights reserved.
//

import Foundation
enum DateFormatType: String {
    case time = "HH:mm:ss"
    case timeSSS = "HH:mm:ss.SSS"
    case timeA = "hh:mm a"
    case HH = "HH"
    case HHMMA = "hh:mm:ss a"

    case date = "dd-MM-yyyy"
    case dateM = "MM-dd-yyyy"
    case ddMMyyyy = "dd/MM/yyyy"
    case MMddyyyy = "MM/dd/yyyy"
    case yyyyMMdd = "yyyy/MM/dd"
    case ddMMMyyyy = "dd MMM yyyy"
    case ddMMMyyyyTime = "dd MMM yyyy hh:mm:ss a"
    case ddMMMyyyyTimeA = "dd MMM yyyy hh:mm a"
    case MMMyyyy = "MMM yyyy"
    case dateR = "yyyy-MM-dd"
    case dateDots = "dd.MM.yyyy"
    case ddMMM = "dd MMM"
    case ddMM = "dd/MM"
    case dateNTimeA = "dd-MM-yyyy hh:mm a"
    case dateSlashTimeA = "dd-MM-yyyy / hh:mm a"
    case dateNTime = "dd-MM-yyyy HH:mm:ss"
    case dateTime = "yyyy-MM-dd HH:mm:ss"
    case dateTimeA = "yyyy-MM-dd hh:mm a"
    case dateTtime = "yyyy-MM-dd'T'HH:mm:ss"
    case dateTtimeSS = "yyyy-MM-dd'T'HH:mm:ss.SS"
    case dateTtimeSSS = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case dateTimeSSS = "yyyy-MM-dd HH:mm:ss.SSS"
    case yy = "yy"
    case year = "yyyy"
    case EEEE = "EEEE"
    case EEE = "EEE"
    case MMM = "MMM"
    case MM = "MM"
    case dd = "dd"
    case e = "e"
    case E = "E"
    case MMYYYY = "MM/yy"
    case MMMDYYYY = "MMM d, yyyy"
    case yyyyyMMddHHmmssSSS = "yyyyMMddHHmmssSSS"
    case dateSql = "ddMMyyyy"
    case dateTimeZ = "yyyy-MM-dd HH:mm:ss Z"
    case EEEYear = "EEE yyyy"
    case EEEEddYear = "EEEE, dd MMMM"
}

class DateConverts: NSObject {
    class func convertDateToString(date: Date, dateformatType:DateFormatType) -> String{
        let dateformat = DateFormatter()
        dateformat.dateFormat = dateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        let dateStr = dateformat.string(from: date)
        return dateStr
    }
    class func convertStringToDate(date: String, dateformatType:DateFormatType) -> Date{
        let dateformat = DateFormatter()
        dateformat.dateFormat = dateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        return dateformat.date(from: date)!
    }
    class func convertStringToStringDates(inputDateStr: String, inputDateformatType:DateFormatType, outputDateformatType:DateFormatType) -> String{
        let dateformat = DateFormatter()
        dateformat.locale = Locale(identifier: "EN")
        //Input
        dateformat.dateFormat = inputDateformatType.rawValue
        let inputDate = dateformat.date(from: inputDateStr)
        //Output
        dateformat.dateFormat = outputDateformatType.rawValue
        return dateformat.string(from: inputDate!)
    }

    class func convertddMMMYYFormate(inputDateStr: String, inputDateformatType:DateFormatType) -> String{
        let dateformat = DateFormatter()
        dateformat.locale = Locale(identifier: "EN")

        //Input
        dateformat.dateFormat = inputDateformatType.rawValue
        let inputDate = dateformat.date(from: inputDateStr)
        //Output
        dateformat.dateFormat = "dd MMM"
        let dateformat1 = DateFormatter()
        dateformat1.dateFormat = "yy"
        
        return "\(dateformat.string(from: inputDate!))'\(dateformat1.string(from: inputDate!))"
    }
    
    class func convertDateToDateFormate(date: Date) -> Date{
        let dateformat = DateFormatter()
        dateformat.locale = Locale(identifier: "EN")
        dateformat.dateFormat = DateFormatType.MMddyyyy.rawValue

        //Convert String
        let inputDate = dateformat.string(from:date)
        //Output Date
        return dateformat.date(from: inputDate)!
    }
    class func convertDateToDateRFormate(date: Date) -> Date{
        let dateformat = DateFormatter()
        dateformat.locale = Locale(identifier: "EN")
        dateformat.dateFormat = DateFormatType.dateR.rawValue

        //Convert String
        let inputDate = dateformat.string(from:date)
        //Output Date
        return dateformat.date(from: inputDate)!
    }
    class func convertDateToDateFormate(date: Date, dateformatType:DateFormatType) -> Date{
        let dateformat = DateFormatter()
        dateformat.locale = Locale(identifier: "EN")
        dateformat.dateFormat = dateformatType.rawValue

        //Convert String
        let inputDate = dateformat.string(from:date)
        //Output Date
        return dateformat.date(from: inputDate)!
    }
    class func getMonth(Month:Int) ->String{
        let dateformat = DateFormatter()
        dateformat.dateFormat =  DateFormatType.MMM.rawValue
        dateformat.locale = Locale(identifier: "EN")
        let month = dateformat.monthSymbols[Month - 1]
        return month
            //String(month.prefix(3))
    }
}



extension Date {
static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
           var dates: [Date] = []
           var date = fromDate
           while date <= toDate {
               dates.append(date)
               guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
               date = newDate
           }
           return dates
}
    
//static func NoOfDates(from fromDate: Date, to toDate: Date) -> Int {
//             var dates: [Date] = []
//             var date = fromDate
//             while date <= toDate {
//                 dates.append(date)
//                 guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
//                 date = newDate
//             }
//             if dates.count == 0{
//               return 1
//             }
//             var NoOfDates:Int = 0
//             for index in 1...(dates.count) {
//                //New Logic
//                let dateStr =  DateConverts.convertDateToString(date: dates[index-1], dateformatType: .dateR)
//                let array = SaveAddressClass.CalenderHolidaysArray.filter{$0.parmTransDate == "\(dateStr)T12:00:00"}
//                if array.count > 0 {
//                    NoOfDates = NoOfDates + 0
//                }else{
//                    NoOfDates = NoOfDates + 1
//                }
//                
//                // Old Logic
////               let weekend = DateConverts.convertDateToString(date: dates[index-1], dateformatType: .EEEE)
////               if weekend.lowercased() == "friday" || weekend.lowercased() == "saturday"{
////                   NoOfDates = NoOfDates + 0
////               }else{
////                   NoOfDates = NoOfDates + 1
////               }
//             }
//             return NoOfDates
//}
    
//static func maximumDate(from fromDate: Date, numberOfDates:Int) -> Date {
//       var maxDate =  Date()
//       var date = fromDate
//       if numberOfDates-1 == 0 || numberOfDates == 0{
//          return fromDate
//       }else{
//             for index in 1...numberOfDates {
//               print(index)
//                if index == 1 {
//                    date = fromDate
//                }else{
//               guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
//               date = newDate
//                }
//               let weekend = DateConverts.convertDateToString(date: date, dateformatType: .EEEE)
//               if weekend.lowercased() == "friday" {
//                   date = Calendar.current.date(byAdding: .day, value: 2, to: date)!
//               }else if weekend.lowercased() == "saturday" {
//                   date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
//               }
//               maxDate = date
//            }
//        }
//       return maxDate
//    }

//  static func today() -> Date {
//      return Date()
//  }

  func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.next,
               weekday,
               considerToday: considerToday)
  }

  func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.previous,
               weekday,
               considerToday: considerToday)
  }

  func get(_ direction: SearchDirection,
           _ weekDay: Weekday,
           considerToday consider: Bool = false) -> Date {

    let dayName = weekDay.rawValue

    let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }

    assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")

    let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1

    let calendar = Calendar(identifier: .gregorian)

    if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
      return self
    }

    var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
    nextDateComponent.weekday = searchWeekdayIndex

    let date = calendar.nextDate(after: self,
                                 matching: nextDateComponent,
                                 matchingPolicy: .nextTime,
                                 direction: direction.calendarSearchDirection)

    return date!
  }

}

// MARK: Helper methods
extension Date {
  func getWeekDaysInEnglish() -> [String] {
    var calendar = Calendar(identifier: .gregorian)
    calendar.locale = Locale(identifier: "en_US_POSIX")
    return calendar.weekdaySymbols
  }

  enum Weekday: String {
    case monday, tuesday, wednesday, thursday, friday, saturday, sunday
  }

  enum SearchDirection {
    case next
    case previous

    var calendarSearchDirection: Calendar.SearchDirection {
      switch self {
      case .next:
        return .forward
      case .previous:
        return .backward
      }
    }
  }
}
