//
//  Extensions.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/19/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func customButton(cornerRadius:CGFloat, borderColor:UIColor, borderWidth:CGFloat, shadow:Bool) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
}

//extension UIImageView {
//
//    func GradientColors(topColorHex:String, buttomColorHex:String, aplpha:CGFloat) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradientLayer.colors = [UIColor.hexStringToUIColor(hex: topColorHex, aplpha:1.0).cgColor,UIColor.hexStringToUIColor(hex:buttomColorHex, aplpha: aplpha).cgColor]
//        self.layer.addSublayer(gradientLayer)
//    }
//
//    func GradientColorsR(topColorHex:String, buttomColorHex:String, aplpha:CGFloat) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradientLayer.colors = [UIColor.hexStringToUIColor(hex: topColorHex, aplpha:1.0).cgColor,UIColor.hexStringToUIColor(hex:buttomColorHex, aplpha: aplpha).cgColor]
//        self.layer.addSublayer(gradientLayer)
//    }
//}

//extension UIView {
//
//    func GradientViewColors(topColorHex:String, buttomColorHex:String, aplpha:CGFloat) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradientLayer.colors = [UIColor.hexStringToUIColor(hex: topColorHex, aplpha:1.0).cgColor,UIColor.hexStringToUIColor(hex:buttomColorHex, aplpha: aplpha).cgColor]
//        self.layer.addSublayer(gradientLayer)
//    }
//}

//extension UILabel {
//    
//    func GradientLabelColors(topColorHex:String, buttomColorHex:String, aplpha:CGFloat) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
//        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
//        gradientLayer.colors = [UIColor.hexStringToUIColor(hex: topColorHex, aplpha:1.0).cgColor,UIColor.hexStringToUIColor(hex:buttomColorHex, aplpha: aplpha).cgColor]
//        self.layer.addSublayer(gradientLayer)
//    }
//}
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in:label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
    
    
}

extension String {
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet =  CharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }
    // Maximum Char
    func safelyLimitedTo(length n: Int)->Bool {
        if (self.count <= n) {
            return true
        }else{
            return false
        }
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
}

//extension UILabel {
//    private struct AssociatedKeys {
//        static var padding = UIEdgeInsets()
//    }
//    
//    public var padding: UIEdgeInsets? {
//        get {
//            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
//        }
//        set {
//            if let newValue = newValue {
//                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            }
//        }
//    }
//    
//    override open func draw(_ rect: CGRect) {
//        if let insets = padding {
//            self.drawText(in: rect.inset(by: insets))
//        } else {
//            self.drawText(in: rect)
//        }
//    }
//    
//    override open var intrinsicContentSize: CGSize {
//        guard let text = self.text else { return super.intrinsicContentSize }
//        
//        var contentSize = super.intrinsicContentSize
//        var textWidth: CGFloat = frame.size.width
//        var insetsHeight: CGFloat = 0.0
//        
//        if let insets = padding {
//            textWidth -= insets.left + insets.right
//            insetsHeight += insets.top + insets.bottom
//        }
//        
//        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
//                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                        attributes: [NSAttributedString.Key.font: self.font], context: nil)
//        
//        contentSize.height = ceil(newSize.size.height) + insetsHeight
//        
//        return contentSize
//    }
//}

extension Double {
    func withCommas() -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencySymbol = ""
        
        // localize to your grouping and decimal separator
        //currencyFormatter.locale = Locale.current
        currencyFormatter.locale = Locale(identifier: "EN")
        return currencyFormatter.string(from: NSNumber(value: self))!
    }
    
}
extension Int {
    func intWithCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value: self))!
    }
    
}
extension Double{
    func withDecimal() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "EN")
        return formatter.string(from: NSNumber(value: self))!
      }
}
extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
}
