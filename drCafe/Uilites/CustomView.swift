//
//  CustomView.swift
//  IKSAB
//
//  Created by Creative Solutions on 10/4/19.
//  Copyright © 2019 creative solutions. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomView: UIView {
        @IBInspectable var borderWidth : CGFloat = 0{
            didSet{
                self.layer.borderWidth = borderWidth
                
            }
        }
        @IBInspectable var borderColor : UIColor = UIColor.clear{
            didSet{
                self.layer.borderColor = borderColor.cgColor
            }
        }
        @IBInspectable var cornerRadis : CGFloat = 0{
            didSet{
                self.layer.cornerRadius = cornerRadis
                self.layer.masksToBounds = true
            }
        }
        
        @IBInspectable var shadow : Bool = false{
            didSet{
                if shadow == true{
                    
                    self.layer.shadowPath =
                        UIBezierPath(roundedRect: self.bounds,
                                     cornerRadius: self.layer.cornerRadius).cgPath

                    self.layer.shadowColor = UIColor.black.cgColor
                    self.layer.shadowOpacity = 0.4
                    self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
                    self.layer.shadowRadius = 1
                    self.layer.masksToBounds = false
                }
            }
        }
}
extension UIView {
    // Specifies Corner Radius for the UIView directly from Storyboard
    @IBInspectable
    var cornerRadius1: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    // Specifies borderWidth for the UIView directly from Storyboard
    @IBInspectable
    var borderWidth1: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    // Specifies borderColor for the UIView directly from Storyboard
    @IBInspectable
    var borderColor1: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    // Specifies shadowRadius for the UIView directly from Storyboard
    @IBInspectable
    var shadowRadius1: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    // Specifies shadowOpacity for the UIView directly from Storyboard
    @IBInspectable
    var shadowOpacity1: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    // Specifies shadowOffset for the UIView directly from Storyboard
    @IBInspectable
    var shadowOffset1: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    // Specifies shadowColor for the UIView directly from Storyboard
    @IBInspectable
    var shadowColor1: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
}


@IBDesignable
class CardViewAllSides: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowSize: CGFloat = 0.5
    @IBInspectable var shadowColor: UIColor? = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: 0 ,
                                                   width: bounds.width + shadowSize/2,
                                                   height: self.bounds.height + shadowSize+1))
        
        //let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false

    }
    
}

@IBDesignable
class HeaderCardView: UIView {
    
    @IBInspectable var shadowSize: CGFloat = 3.0
    @IBInspectable var shadowColor: UIColor? = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: CGRect(x: -2,
                                                   y: self.bounds.height/2,
                                                   width: bounds.width+4,
                                                   height: self.bounds.height/2 + shadowSize))
        
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
    }
    
}
class CustomDashedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0

    var dashBorder: CAShapeLayer?

    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}
@IBDesignable
class TopCardView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var shadowSize: CGFloat = 2.0
    @IBInspectable var shadowColor: UIColor? = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: CGRect(x: -2,
                                                   y: -shadowSize,
                                                   width: bounds.width+4,
                                                   height: self.bounds.height/2))
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.cornerRadius = cornerRadius

    }
    
}
