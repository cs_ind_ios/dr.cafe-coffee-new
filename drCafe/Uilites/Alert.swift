//
//  Alert.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/19/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
@available(iOS 13.0, *)
struct Alert {
    static func showAlert(on vc:UIViewController, title:String, message:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default , handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    static func TwoActionShowAlert(on vc:UIViewController, title:String, message:String, action1: UIAlertAction, action2:UIAlertAction){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(action1)
        alert.addAction(action2)
        vc.present(alert, animated: true, completion: nil)
    }
    static func OneActionShowAlert(on vc:UIViewController, title:String, message:String, action1: UIAlertAction){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    static func loginAlert(on vc:UIViewController){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: "Login to proceed further", message: nil, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            print("You've pressed cancel");
                }
        
        let action2 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Login", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            print("You've pressed cancel");
              // AppDelegate.getDelegate().viewController()
        }
        
        alert.addAction(action1)
        alert.addAction(action2)
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func OkActionAlert(on vc:UIViewController, title:String, message:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            vc.navigationController?.popViewController(animated:false)
        }
    
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    static func OkActionAlertDismiss(on vc:UIViewController, title:String, message:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            vc.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    static func OkRootViewControllerAlert(on vc:UIViewController, title:String, message:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            vc.navigationController?.popToRootViewController(animated: false)
        }
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    static func showToast(on vc:UIViewController, message : String, seconds: Double = 2.0) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        vc.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    static func showToastDown(on vc:UIViewController, message : String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let toastLabel = UILabel(frame: CGRect(x: vc.view.frame.size.width/2 - 100, y: vc.view.frame.size.height-150, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
       // toastLabel.font = UIFont(name: FontBook, size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        vc.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    static func showToastAlert(on vc:UIViewController, message : String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let toastLabel = UILabel(frame: CGRect(x: vc.view.frame.size.width/2 - 75, y: vc.view.frame.size.height-135, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Gotham-Book", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        vc.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}


