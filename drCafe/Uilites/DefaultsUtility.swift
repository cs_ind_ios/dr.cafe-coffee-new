//
//  DefaultUtility.swift
//  CoffeeStation
//
//  Created by RAVI on 18/07/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit

//func isUserLogIn() -> Bool {
//    return UserDefaults.standard.value(forKey: "UserId") == nil ? false : true
//}

class UserDef{
 
    //MARK: UserId
    class func getUserId() -> String{
        return UserDefaults.standard.value(forKey: "UserId") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "UserId")!)"
    }
    
    //MARK: Insert User Profile Dic
    class func storeUserProfileDic(dict: [String : Any], withKey key: String) {
        let defaults = UserDefaults.standard
        var data: NSData? = nil
        if !dict.isEmpty {
            data = NSKeyedArchiver.archivedData(withRootObject: dict) as NSData?
        }
        defaults.set(data, forKey: key)
        defaults.synchronize()
    }
    
    
    //MARK: Get User Profile Dic
    class func getUserProfileDic(key:String) -> Dictionary<String, Any>  {
        
        let newData = UserDefaults.standard.object(forKey: key) as? Data
        var dic = [String : Any]()
        if newData != nil
        {
            dic = NSKeyedUnarchiver.unarchiveObject(with: newData!)as! Dictionary<String, Any>
            return dic
        }
        else {
            return dic
        }
    }
    
    
    //MARK: UserDefault Save
    class func saveToUserDefault(value:Any, key:String)
    {
        UserDefaults.standard.set(value, forKey:key)
        UserDefaults.standard.synchronize()
    }
    
    // remove from UserDefault
    class func removeFromUserDefaultForKey(key:String)
    {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
        
    }
    
    
    // Ravi Nadendla
    //Method for remove Object from NSUserDefaults
    class func methodForRemoveObjectValue(_ stringKey: String){
        UserDefaults.standard.removeObject(forKey: stringKey)
    }
    
    class func isValueInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //Method for fetch Object from NSUserDefaults
    class func methodForFetchStringObjectValue(_ stringKey: String)->String{
        let stringFetchResult = UserDefaults.standard.object(forKey: stringKey) as! String
        return stringFetchResult
    }
    
    //Method for save Object in NSUserDefaults
    class func methodForSaveStringObjectValue(_ stringObjectName: String, andKey stringKey: String){
        UserDefaults.standard.setValue(stringObjectName, forKey:stringKey)
    }
}


