//
//  SubScriptionCategoryModel.swift
//  drCafe
//
//  Created by Devbox on 17/09/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
struct SubScriptionCategoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [SubScriptionDetailsModel]?
}
struct SubScriptionDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Category : [SubScriptionCategoryDetailsModel]?
}
struct SubScriptionCategoryDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let SubCategory : [SubScriptionSubCategoryDetailsModel]?
}
struct SubScriptionSubCategoryDetailsModel : Decodable {
    let Id: Int
    let NameEn : String
    let NameAr : String
    //let Items : [SubScriptionSubCategoryDetailsModel]?
}
//struct SubScriptionSubCategoryDetailsModel : Decodable {
//    let Id: Int
//    let NameEn : String
//    let NameAr : String
//    let Items : [SubScriptionSubCategoryDetailsModel]?
//}
//"Id": 233,
//                  "MainCategoryId": 11,
//                  "CategoryId": 75,
//                  "SubCategoryId": 76,
//                  "NameEn": "African Magic 227 Grams",
//                  "NameAr": "افريكان ماجيك 227 جرام",
//                  "MetaKeyWordsEn": "Blend from Afican Region with bright honey, floral, sweet, fruity notes with nutty & blueberry finish",
//                  "MetaDescriptionEn": "Blend from Afican Region with bright honey, floral, sweet, fruity notes with nutty & blueberry finish",
//                  "MetaKeyWordsAr": "مزيج من المنطقة الإفريقة مع العسل، والأزهار، والفواكه مع لمسة نهائية من الجوز والتوت",
//                  "MetaDescriptionAr": "مزيج من المنطقة الإفريقة مع العسل، والأزهار، والفواكه مع لمسة نهائية من الجوز والتوت",
//                  "CategoryMetaKeyWordsEn": "dr. Cafe Premium Blend Beans",
//                  "CategoryMetaDescriptionEn": "dr. Cafe Premium Blend Beans",
//                  "CategoryMetaKeyWordsAr": "مزيج حبوب V12 المختصة والإستثنائية",
//                  "CategoryMetaDescriptionAr": "مزيج حبوب V12 المختصة والإستثنائية",
//                  "ItemsDescEn": "Blend from Afican Region with bright honey,floral, sweet, fruity notes with nutty & blueberry finish",
//                  "ItemsDescAr": "مزيج من المنطقة الإفريقة مع العسل، والأزهار، والفواكه مع لمسة نهائية من الجوز والتوت",
//                  "CoffeeBeverage": false,
//                  "V12": false,
//                  "ServingTime": 4,
//                  "SeqId": 178,
//                  "Image": "8777african magic.png",
//                  "MaxOrderQty": 10,
//                  "SKU": "110220701071",
//                  "Lenght": 0,
//                  "Weight": 0,
//                  "WeightUOM": "",
//                  "Height": 0,
//                  "Breadth": 0,
//                  "Tags": [],
//                  "Prices": [
//                    {
//                      "ItemPriceId": 10387,
//                      "SizeId": 5,
//                      "Price": 60,
//                      "SizeEn": "Regular",
//                      "SizeAr": "عادي"
//                    }
//                  ]
//                },
