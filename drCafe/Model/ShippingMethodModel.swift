//
//  ShippingMethodModel.swift
//  drCafe
//
//  Created by Devbox on 28/09/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

//MARK: Load Cart
struct LoadCartShippingModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : [ShippingMethodDetailsModel]?
}
struct ShippingMethodDetailsModel : Decodable {
    let Id : Int
    let DisplayNameEn : String
    let FutureDays : Int
    let DisplayNameAr : String
    let ShippingCost : Double
    let ShippingTime : String
    let CostId : Int?
    let MaxDeliveryHours : Int
    let OffDay : String
    let StartTime : String
    let EndTime : String
    let ShippingMethodId : Int
    let FreeDeliveryAmount : Double
    var isSelect : Bool?
    var ShippingSlots : [ShippingSlotsDetailsModel]?
}
struct ShippingSlotsDetailsModel : Decodable {
    let weekday : Int
    let wkDay : String
    var Slot : [SlotDetailsModel]?
}
struct SlotDetailsModel : Decodable {
    let Id : Int
    let ShippingMethodId : Int
    let WeekDay : Int
    let StartTime : String
    let EndTime : String
    let IsClose : Bool
    let IsSlotFull : Bool
    var isSelect : Bool?
}


//MARK: Load Cart
struct OrderDetailsShippingModel : Decodable {
    let Status : Bool
    let MessageEn : String?
    let MessageAr : String?
    var Data : [ShippingMethodDetailsModel]?
}
