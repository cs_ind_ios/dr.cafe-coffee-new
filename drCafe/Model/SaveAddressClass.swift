//
//  SaveAddressClass.swift
//  drCafe
//
//  Created by Devbox on 01/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

class SaveAddressClass{
    static var themeColorArray = [themesDetailsModel]()
    static var selectBannerStores = [BannerDetailsModel]()
    static var categoryArray = [StoresCategoryDetailsModel]()
    static var CountryCodesListArray = [CountryCodeDetailsModel]()
    static var getOrderHistoryArray = [OrderHistoryDetailsModel]()
    static var getDCSmileHistoryArray = [DCSmileHistoryDetailsModel]()
    static var cartDetailsArray = [CartDetailsModel]()
    static var discoverCategoryArray = [CategoryDetailsModel]()
    static var newCategoryArray = [CategoryMainDetailsModel]()
    static var expectDatesArray = [ExpectDates]()
    static var promotionsArray = [PromotionsDetailsModel]()
    static var selectFilterDetailsArray = [StoresListModel]()
    static var SubscriptionCategory = [SubScriptionDetailsModel]()
    static var getSubscriptionOrderHistoryArray = [SubscriptionHistoryDetailsModel]()
    static var carInformationArray = [SubOrderDetailsDataModel]()
    static var upcomingEventDetailsArray = [BetaPublicEventsDataModel]()
}

