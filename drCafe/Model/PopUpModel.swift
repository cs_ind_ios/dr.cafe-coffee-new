//
//  PopUpModel.swift
//  drCafe
//
//  Created by Devbox on 09/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

struct UserOrderModel {
    let Id : Int
    let BannerName : String
    let BannerImageEn : String
    let BannerImageAr : String
    let IsClick : Bool
    let MainCategoryId : Int
    let MainCategoryEn : String
    let MainCategoryAr : String
    let CategoryId : Int
    let CategoryEn : String
    let CategoryAr : String
    let SubCategoryId : Int
    let SubCategoryEn : String
    let SubCategoryAr : String
    let ItemId : Int
    let DineId : Bool
    let TakeAway : Bool
    let DriveThru : Bool
    let Delivery : Bool
    let SequenceId : Int
    let StoreList : [StoreDetailsModel]?
}
