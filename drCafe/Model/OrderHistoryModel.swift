//
//  OrderHistoryModel.swift
//  drCafe
//
//  Created by Devbox on 21/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Order History
struct OrderHistoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let TotalOrders : Int
    let PageNo : Int
    let PageSize : Int
    let Orders : [OrderHistoryDetailsModel]?
}
struct OrderHistoryDetailsModel : Decodable {
    let OrderId : Int
    let IsArchive : Bool?
    let IsSubscription : Bool?
    let InvoiceNo : String
    let FavouriteName : String?
    let ExpectedTime : String
    let OrderStatus : String
    let OrderType : String?
    let NetPrice : Double
    let TotalItems : Int
    var Favourite : Bool
    let SectionType : Int
    let ShippingTime : String
    let Upcoming : Int?
    let PaymentIcon : String
    let PaymentType : String
    let PaymentTypeAr : String
    let PaymentTypeId : Int
    let Store : [OrderHistoryStoreDetailsModel]?
    let DeliveryAddress : [OrderHistoryDeliveryDetailsModel]?
    let OrderRatings : [OrderRatingsDetailsModel]?
    let OrderPaymentDetails : [OrderPaymentDetailsModel]?
}
struct OrderHistoryStoreDetailsModel : Decodable {
    let StoreNameEn : String
    let StoreNameAr : String
    let StoreCode : String
    let StoreAddressEn : String
    let StoreAr : String
    let StoreLat : String
    let StoreLng : String
}
struct OrderHistoryDeliveryDetailsModel : Decodable {
    let Lat : Double
    let Lng : Double
    let Details : String
    let ContactPerson : String
    let ContactNumber : String
}
struct OrderRatingsDetailsModel : Decodable {
    let Id : Int
    let OrderId : Int
    let Value : Int
    let Comment : String?
    let IsActive : Bool
    let CreatedBy : Int
    let CreatedOn : String
    let OrderType : String
}
struct OrderHistoryPaymentDetailsModel : Decodable {
    let UserId : Int
    let LastDigits : String
    let CardHolder : String
    let ExpireMonth : String
    let ExpireYear : String
    let CardBrand : String
    let Status : Bool
}

//Order Details
struct OrderDetailsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [OrderDetailsDataModel]?
}
struct OrderDetailsDataModel : Decodable {
    let Items : [OrderDetailsItemsModel]?
    let Tracking : [OrderDetailsTrackingModel]?
    let storeId : Int
    let StoreName : String
    let storeNameAr : String
    let Favourite : Bool
    let FavouriteName : String?
    let StoreAddressEn : String
    let StoreAddressAr : String
    let SubOrderNameAr : String?
    let SubOrderNameEn : String?
    let SubOrderTypeId : Int?
    let CarId : Int?
    let CarJson : String?
    let UserId : Int
    let FullName : String
    let mobile : String
    let countofItems : Int
    let comments : String
    let OrderDate : String
    let orderType : String
    let orderTypeAr : String
    let Expectedtime : String
    let registrationId : String?
    let NetTotal : Double
    let SubTotal : Double
    let Vat : Double
    let VatPercentage : Int
    let Discount : Double?
    let Wallet : Double?
    let SubscriptionId : Int?
    let OrderStatusId : Int
    let OrderStatus : String
    let OrderStatusAr : String
    let TotalPrice : Double
    let DeliveryCharge : Double
    let DeviceToken : String
    let DeviceToken1 : String
    let PaymentTypeId : Int
    let Invoice : String
    let SectionType : Int?
    let ShippingTime : String
    let PaymentType : String
    let PaymentTypeAr : String
    let PaymentIcon : String?
    let AcceptedTime : String
    let ReadyTime : String
    let AddressId : Int
    let HouseNo : String
    let CAddress : String
    let Latitude : String
    let Longitude : String
    let ExpectedDate : String
    let CurrentTime : String
    let OrderRatings : [OrderRatingsModel]?
    let FrequencyOptions : [FrequencyOptionsModel]?
    let FrequencyId : Int?
    let FrequencyNameEn : String?
    let FrequencyNameAr : String?
    let ShippingMethodEn : String?
    let FrequencyDiscount : Double?
    let IsPause : Int?
    let Upcoming : Int?
    let IsSubscription : Bool?
    let ConsumedPoints : Int?
    let PromoCode : String?
    let OrderId : Int?
    let IsSuspended : Int?
    let PaymentProcess : Bool?
    let OrderPaymentDetails : [OrderPaymentDetailsModel]?
}
struct OrderDetailsItemsModel : Decodable {
    let OrderId : Int
    let OrderItemId : Int
    let ItemNameEn : String
    let ItemNameAr : String
    let ItemNo : String
    let itemType : String
    let itemTypeAr : String
    let ItemId : Int
    let quantity : Int
    let comments : String
    let ItemPrice : Double
    let Size : Int
    let ItemImage : String
    let GrinderId : Int?
    let GrinderNameEn : String?
    let GrinderNameAr : String?
    let Amount : Double
    let ItemSubTotal : Double
    let TotalPrice : Double
    let RowNumber : Int
    let ConsumedPoints : Int?
    let RewardBand : Int?
    let ItemComments : String?
    let Additionals : [OrderDetailsAdditionalsModel]?
}
struct OrderDetailsAdditionalsModel : Decodable {
    let Id : Int
    let ItemId : Int
    let AdditionalEn : String
    let AdditionalAr : String
    let additionalprice : Double
    let Qty : Int
}
struct OrderDetailsTrackingModel : Decodable {
    let AcceptedBy : Int
    let Id : Int
    let OrderId : Int
    let OrderStatus : String
    let OrderStatusAr : String
    let TrackingTime : String
    let ActionBy : String
}
struct OrderRatingsModel : Decodable {
    let Comment : String?
    let CreatedBy : Int
    let CreatedOn : String
    let Id : Int
    let IsActive : Bool
    let OrderId : Int
    let OrderType : String
    let Value : Int
}
struct FrequencyOptionsModel : Decodable {
    let Id : Int?
    let OptionEN : String?
    let OptionEn : String?
    let OptionAr : String?
    let IsActive : Bool?
    let DeletedOn : String?
    let DeletedBy : Int?
    let IsDeleted : Bool?
}
struct OrderPaymentDetailsModel : Decodable{
    let Id : Int
    let FirstDigits : String
    let LastDigits : String
    //let CardHolder : String
    let ExpireMonth : String
    let ExpireYear : String
    let CardBrand : String
}

//Old Order Details
struct OldOrderDetailsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [OldOrderDetailsDataModel]?
}
struct OldOrderDetailsDataModel : Decodable {
    let OrderJson : [OrderDetailsDataModel]?
}
