//
//  AppVersion.swift
//  drCafe
//
//  Created by Mac2 on 08/06/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation

//MARK: App Version History
struct AppVersionModel : Decodable {
    let Status : Bool
    let MessagEn : String
    let MessagAr : String
    let Data : AppVersionDetailsModel?
}
struct AppVersionDetailsModel : Decodable {
    let AppVersion : String?
    let UpdatesAvailable : Int?
    let UpdateSeverity : Int?
    let AppType : String?
    let Severity : Int?
}
