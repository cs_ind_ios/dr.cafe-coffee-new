//
//  SubscriptionDetailsModel.swift
//  drCafe
//
//  Created by Devbox on 23/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

//MARK: Subscription History
struct SubscriptionHistoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let TotalOrders : Int
    let PageNo : Int
    let PageSize : Int
    let Orders : [SubscriptionHistoryDetailsModel]?
    let ReJectedOrders : [SubscriptionHistoryDetailsModel]?
}
struct SubscriptionHistoryDetailsModel : Decodable {
    let IsArchive : Bool
    let OrderId : Int
    let InvoiceNo : String
    let ExpectedTime : String
    let OrderType : String
    let SectionType : Int
    let ShippingTime : String
    let OrderStatus : String
    let OrderDate : String
    let NetPrice : Double
    let IsSubscription : Bool
    let TotalItems : Int
    var Favourite : Bool
    let FavouriteName : String
    let FrequencyId : Int
    let FrequencyNameEn : String
    let FrequencyNameAr : String
    let Upcoming : Int
    let Retrypayment : Int
    let RegistrationId : String
    let PaymentMethod : String?
    let PaymentMethodAr : String?
    let PaymentBrandIcon : String?
    let Store : [OrderHistoryStoreDetailsModel]?
    let DeliveryAddress : [OrderHistoryDeliveryDetailsModel]?
    let OrderRatings : [OrderRatingsDetailsModel]?
    let PaymentDetails : [OrderHistoryPaymentDetailsModel]?
}

//MARK: Subscription Track
struct SubscriptionTrackModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : SubscriptionTrackDetailsModel?
}
struct SubscriptionTrackDetailsModel : Decodable {
    //let Orders : [SubscriptionTrackOrdersModel]?
    let OrderDetails : [SubscriptionTrackOrderDetailsModel]?
    let PauseHistory : [SubscriptionTrackPauseHistoryModel]?
}
struct SubscriptionTrackOrdersModel : Decodable {
    let OrderId : Int
    let InvoiceNo : String
    let OrderExpected : String
    let NetTotal : Double
    let OrderStatusId : Int
    let OrderStatusEn : String
    let OrderStatusAr : String
    let InitiatedDate : String
    let CustomerName : String
    let Email : String
    let PhoneNo : String
}
struct SubscriptionTrackOrderDetailsModel : Decodable {
    let storeId : Int
    let StoreName : String
    let storeNameAr : String
    let StoreCode : String
    let Favourite : Bool
    let StoreAddressEn : String
    let StoreAddressAr : String
    let StoreMobileNo : String
    let UserId : Int
    let InitiatedDate : String
    let FullName : String
    let mobile : String
    let countofItems : Int
    let NetTotal : Double
    let OrderDate : String
    let orderType : String
    let orderTypeAr : String
    let Expectedtime : String
    let ShippingMethod : String
    let ShippingMethodAr : String
    let ShippingTime : String
    let OrderStatusId : Int
    let OrderStatus : String
    let StartDate : String?
    let MaxDays : Int?
    let RescheduleMaxDays : Int?
    let IsReschedule : Bool?
    let OrderStatusAr : String
    let Invoice : String
    let PaymentType : String
    let PaymentTypeAr : String
    let AcceptedTime : String
    let ReadyTime : String
    let AddressId : Int
    let HouseNo : String
    let CAddress : String
    let Latitude : String
    let Longitude : String
    let AddressType : String
    let HouseName : String
    let ExpectedDate : String
    let CurrentTime : String
    let SectionType : Int
    let OrderId : Int
    let registrationId : String?
    let IsArchive : Bool
    let FrequencyId : Int
    let FrequencyNameEn : String
    let FrequencyNameAr : String
    let IsPause : Int
    let IsSuspended : Int
    let Upcoming : Int
    let IsSubscription : Bool
    let PaymentProcess : Bool
    let SubscriptionId : Int
    let VatPercentage : Int
    let Vat : Double
    let TotalPrice : Double
    let DeliveryCharge : Double
    let Wallet : Double?
    let Discount : Double?
    let RetryPayment : Int
    let Items : [SubscriptionOrderItemsDetailsModel]?
    let Tracking : [SubscriptionTrackingDetailsModel]?
    //let OrderRatings : [OrderRatingDetailsModel]?
    //let FrequencyOptions : [FrequencyOptionsModel]?
}
struct SubscriptionOrderItemsDetailsModel : Decodable {
    let ItemNameEn : String
    let ItemNameAr : String
    let Image : String
    let itemType : String
    let itemTypeAr : String
    let quantity : Int
    let ItemPrice : Double
    let Amount : Double
    let ItemSubTotal : Double
    let TotalPrice : Double
    let GrinderNameEn : String?
    let GrinderNameAr : String?
    let ItemComments : String?
    let Additionals : [SubscriptionOrderItemsAdditionalsModel]?
}
struct SubscriptionOrderItemsAdditionalsModel : Decodable {
    let ItemId : Int
    let Id : Int
    let AdditionalEn : String
    let AdditionalAr : String
    let Qty : Int
    let additionalprice : Double
}
struct SubscriptionTrackingDetailsModel : Decodable {
    let AcceptedBy : Int
    let Id : Int
    let OrderId : Int
    let OrderStatusId : Int
    let OrderStatus : String
    let OrderStatusAr : String
    let TrackingTime : String
    let ActionBy : String
}
struct SubscriptionTrackPauseHistoryModel : Decodable {
    let Id : Int
    let SubscriptionId : Int
    let PauseState : String
    let OrderId : Int
    let Remarks : String
}
