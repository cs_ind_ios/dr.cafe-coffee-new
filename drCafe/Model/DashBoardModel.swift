//
//  DashBoardModel.swift
//  drCafe
//
//  Created by Devbox on 11/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// DashBoard Details
struct DashBoardModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : DashBoardDetailsModel?
}
struct DashBoardDetailsModel : Decodable {
    let Orders : [OrdersDetailsModel]?
    let updateInfo : [updateInfoDetailsModel]?
    let themes : [themesDetailsModel]?
    let OrderTypes : OrderTypesDetailsModel?
    let Programs : ProgramsDetailsModel?
    let CartDetails : CartDetailsModel?
    let Result : [BannerDetailsModel]?
}
struct BannerDetailsModel : Decodable {
    let Id : Int
    let BannerName : String
    let BannerImageEn : String
    let BannerImageAr : String
    let IsClick : Bool
    let MainCategoryId : Int
    let MainCategoryEn : String
    let MainCategoryAr : String
    let CategoryId : Int
    let CategoryEn : String
    let CategoryAr : String
    let SubCategoryId : Int
    let SubCategoryEn : String
    let SubCategoryAr : String
    let ItemAr : String
    let ItemEn : String
    let ItemId : Int
    let DineId : Bool
    let TakeAway : Bool
    let DriveThru : Bool
    let Delivery : Bool
    let SequenceId : Int
    let StoreList : [StoreDetailsModel]?
}
struct PopUpsDetailsModel : Decodable {
    let Id : Int
    let Name : String
    let TypeOfPopUp : String
    let ImageEn : String
    let ImageAr : String
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let ItemId : Int
    let OfferId : Int
    let PackageId : Int
    let SubscriptionId : Int
    let WebUrl : String
}
struct OrdersDetailsModel : Decodable {
    let ExpectedTime : String
    let InvoiceNo : String
    let OrderId : Int
    let OrderStatus : String
    let OrderStatusAr : String
    let OrderStatusId : Int
    let OrderType : String
}
struct updateInfoDetailsModel : Decodable {
    let AppVersion : String?
    let UpdatesAvailable : Int?
    let Severity : Int?
}
struct themesDetailsModel : Decodable {
    let Id : Int
    let ThemeName : String
    let BackgroundColor : String?
    let Image : String
    let ForegroundColor : String?
    let DefaultSelection : Bool
    let IsSelected : Bool
}
struct OrderTypesDetailsModel : Decodable {
    let DineIn : DetailsModel?
    let PickUp : DetailsModel?
    let Delivery : DetailsModel?
    let DriveThru : DetailsModel?
}
struct ProgramsDetailsModel : Decodable {
    let Subscription : DetailsModel?
    let Catering : DetailsModel?
    let Gift : DetailsModel?
}
struct DetailsModel : Decodable {
    let Id : Int
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
}
struct CartDetailsModel : Decodable {
    let CartItemsQuantity : Int?
    let SubItemQty : Int?
    let StoreId : Int?
    let TotalPrice : Double?
    let SubTotalPrice : Double?
    let LoyaltyPoints : Double?
    let LoyaltySmiles : Double?
    let WalletAmount : Double?
    let ImageBasePathUrl : String?
    let BadgeId : Int?
    let BandName : String?
    let BadgeNameEn : String?
    let BadgeNameAr : String?
    let GreetingsEn : String?
    let GreetingsAr : String?
    let DescriptionEn : String?
    let DescriptionAr : String?
    let UpgradeMsgEn : String?
    let UpgradeMsgAr : String?
    let BadgeExpiry : String?
    let NextBadge : Double?
    let StoreNameEn : String?
    let StoreNameAr : String?
    let CurrentTime : String?
    let ExpectedTime : String?
    let ClosedOrder : [CartRatingDetailsModel]?
    let IsFavourite : Int?
    let BetaInfoTitle1 : String?
    let BetaInfoMobile1 : String?
    let BetaInfoTitle2 : String?
    let BetaInfoMobile2 : String?
    let BetaInfoTitleAr1 : String?
    let BetaInfoTitleAr2 : String?

}
struct CartRatingDetailsModel : Decodable {
    let Id : Int
    let OrderType : String
    //let OrderRating : [OrderRatingDetailsModel]?
    //let DriverRating : [OrderRatingDetailsModel]?
    let StoreName : String
    let StoreNameAr : String
    let Address :[ClosedOrderAddressDetailsModel]?
}
struct ClosedOrderAddressDetailsModel : Decodable {
    let Id : Int
    let UserId : Int
    let HouseNo : String
    let HouseName : String
    let LandMark : String
    let Address : String
    let AddressType : String
    let CountryCode : String
    let Latitude : String
    let Longitude : String
    let Default : Int
    let ContactPerson : String
    let ContactNo : String
    let IsActive : Bool
    let CreatedDate : String
}

// Bulk Order Details
struct BulkOrderModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
}
