//
//  AddressDetailsModel.swift
//  drCafe
//
//  Created by Devbox on 15/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Address List
struct AddressListModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [AddressDetailsModel]?
}
struct AddressDetailsModel : Decodable {
    let Id : Int
    let UserId : Int
    let HouseNo : String
    let HouseName : String
    let LandMark : String
    let Address : String
    let AddressType : String
    let CountryCode : String
    let Default : Int
    let Latitude : String
    let Longitude : String
    let ContactPerson : String
    let ContactNo : String
    let DeliveryInfo : String

}

//Save Address
struct SaveAddressModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String?
    let Data : SaveAddressDetailsModel?
}
struct SaveAddressDetailsModel : Decodable {
    let Id : Int
    let UserId : Int
    let Longitude : String
    let Latitude : String
    let LandMark : String
    let HouseNo : String
    let HouseName : String
    let Default : Int
    let CountryCode : String
    let ContactPerson : String
    let ContactNo : String
    let AddressType : String
    let Address : String
    let DeliveryInfo : String
}

//Update Address
struct UpdateAddressModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
}
