//
//  CartModel.swift
//  drCafe
//
//  Created by Devbox on 01/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

//MARK: Add To Cart
struct AddToCartModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : AddToCartDetailsModel?
}
struct AddToCartDetailsModel : Decodable {
    let CartItemsQuantity : Int?
    let SubItemQty : Int?
    let TotalPrice : Double?
    //let SugestiveItems : SuggestiveItemsModel?
}
struct SugestiveItemsModel : Decodable {
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let ItemId : Int
    let ItemNameEn : String
    let ItemNameAr : String
    let ItemImage : String
}

//MARK: Load Cart
struct LoadCartModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : LoadCartDataModel?
}
struct LoadCartDataModel : Decodable {
    var Carts : [LoadCartDetailsModel]?
    var Payment : [PayementMethodsModel]?
}
struct LoadCartDetailsModel : Decodable {
    let Subscription : Bool?
    let Cart : [CartModel]?
    let Stores : [CartStoreDetailsModel]?
    let Address : [AddressModel]?
    var Items : [CartItemsModel]?
    let CrossItems : [CrossItemsModel]?
    let Frequency : [FrequencyModel]?
    let RewardsArray : [RewardsArrayModel]?
}
struct CartModel : Decodable {
    let Id : Int?
    let UserId : Int?
    let StoreId : Int?
    let Comments : String?
    let Wallet : Int?
    let OrderDate : String?
    let SectionType : Int?
    let ConsumedPoints : Int?
    let OrderStatus : String?
    let OrderType : String?
    let ItemTotal : Double?
    let DeliveryCharge : Double?
    let Discount : Double?
    let VatPercentage : Int?
    let PromoCode : String?
    let Vatcharges : Double?
    let SubTotal : Double?
    let NetTotal : Double?
    let AvailableWalletAmt : Double?
    let ShippingTime : String?
    let SlotId : Int?
    let PaymentType : Int?
    let CartItemsQuantity : Int?
    let IsSubscription : Bool?
    let FrequencyId : Int?
}
struct CartStoreDetailsModel : Decodable {
    let Id : Int?
    //let StoreCode : String?
    let NameEn : String?
    let NameAr : String?
    //let DescEn : String?
    //let DescAr : String?
    let LandlineNo : String?
    let Latitude : String?
    let Longitude : String?
    //let Distance : String?
    let MobileNo : String?
    let AddressEn : String?
    let AddressAr : String?
    //let ExclusiveForWomen : Bool?
    let StoreStatus : Bool?
    //let IsTrending : Bool?
    //let IsActive : Bool?
//    let StoreType : Int?
//    let ImagePath : String?
//    let Favourite : Bool?
//    let GroupId : Int?
//    let GroupNameEn : String?
//    let GroupNameAr : String?
//    let StoreLaunchDate : String?
    let is24x7 : Bool?
    //let isStoreCloseToday : Bool?
    let isShiftOpen : Bool?
    let OpensInMinutes : Int?
    let OrderTypeBusy : Bool?
    //let BusySchedule : [BusyScheduleDetailsModel]?
    //let Shifts : [ShiftsModel]?
    //let FreeDrinkLimit : String?
    //let CountryId : Int?
    //let CountryNameEn : String?
    //let CountryNameAr : String?
    //let Facilities : [FacilitiesDetailsModel]?
    //let Segments : [SegmentsDetailsModel]?
    let CurrentDateTime : String?
    let ShiftBusinessDate : String
}
struct AddressModel : Decodable {
    let Id : Int?
    let Name : String?
    let Latitude : String?
    let Longitude : String?
    let Address : String?
    let ContactPerson : String?
    let ContactPersonPhone : String?
    let TypeId : String?
    let Landmark : String?
    let DeliveryInfo : String?
}
struct CartItemsModel : Decodable {
    let OrderId : Int?
    let MainCategoryId : Int?
    let CategoryId : Int?
    let SubCategoryId : Int?
    let ItemId : Int?
    let CartItemId : Int?
    let ItemNameEn : String?
    let ItemNameAr : String?
    let GrinderId : Int?
    let GrinderNameEn : String?
    let GrinderNameAr : String?
    let ItemImage : String?
    let MaxOrderQty : Int?
    let SizeId : Int?
    let SizeNameEn : String?
    let SizeNameAr : String?
    let Quantity : Int?
    let ItemSubTotal : Double?
    let TotalPrice : Double?
    let Price : Double?
    let Comments : String?
    let DiscountPrice : Double?
    let RewardEligibility : Bool?
    let RewardBand : Int?
    let ConsumedPoints : Int?
    //let StoreAddressEn : String?
    //let StoreAddressAr : String?
    let ServingTime : Double?
    let CatalogUpdated : Bool?
    let IsOutOfStock : Int?
    let OutOfStockMessageEn : String?
    let OutOfStockMessageAr : String?
    let CatlogMessageEn : String?
    let CatlogMessageAr : String?
    let CatalogChanged : Bool?
    let ChangeMessageEn : String?
    let ChangeMessageAr : String?
    let Additionals : [CartAdditionalsModel]?
    //let ComboItems : [ComboItemsModel]?
}
struct CartAdditionalsModel : Decodable {
    let GroupId : Int
    let AdditionalId : Int
    let NameEn : String
    let NameAr : String
    let Quantity : Int
    let Image : String
    let Price : Double
    let CatalogUpdated : Bool
    let CatalogChanged : Int
    let ChangeMessageEn : String
    let ChangeMessageAr : String
    let IsOutOfStock : Int
}
struct CrossItemsModel : Decodable {
    let OrderId : Int?
    let Id : Int?
    let MainCategoryId : Int?
    let CategoryId : Int?
    let SubCategoryId : Int?
    let Image : String?
    let NameEn : String?
    let NameAr : String?
    let DescAr : String?
    let DescEn : String?
    let MaxOrderQty : Int?
    let ItemImage : String?
    let HasVariables : Bool?
    let Prices : [PriceDetailsModel]?
}
struct FrequencyModel : Decodable {
    let Days : Int?
    let Discount : Double?
    let Id : Int?
    let IsActive : Bool?
    let NameEn : String?
    let NameAr : String?
}
struct RewardsArrayModel : Decodable {
    let RewardBand : Int?
}
struct PayementMethodsModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let DineIn : Bool?
    let TakeAway : Bool?
    let Delivery : Bool?
    let Subscription : Bool?
    let Packages : Bool?
    let DisplaySequence : Int?
    let Icon : String?
    let IconId : Int?
    let IsActive : Bool?
    var isSelect : Bool?
    let IsDeleted : Bool?
}
struct ComboItemsModel : Decodable {
    let MainCategoryId : Int?
    let CategoryId : Int?
    let SubCategoryId : Int?
    let MenuItemId : Int?
    let ItemNameEn : String?
    let ItemNameAr : String?
    let ItemImage : String?
    let SizeId : Int?
    let SizeNameEn : String?
}
//MARK: Load Cart
struct ConfirmOrderModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : ConfirmOrderDataModel?
}
struct ConfirmOrderDataModel : Decodable {
    let InvoiceNo : String?
    let OrderId : Int?
    let TotalAmount : Double?
}

public class ExpectDates{
    var id : Int
    var date : String
    var isSelect : Bool
    
    init(id : Int, date : String, isSelect : Bool) {
        self.id = id
        self.date = date
        self.isSelect = isSelect
    }
}


//MARK: Promotion Service
struct PromotionModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
}

//MARK: Current Time Service
struct CurrentTimeModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : String
}

//MARK: Multiple Orders
struct MultipleOrdersCartModel : Decodable {
    let Status : Bool
    let MessageEn : String?
    let MessageAr : String?
    var Data : [MultipleOrdersDetailsModel]?
}
struct MultipleOrdersDetailsModel : Decodable {
    let Id : Int
    let OrderTypeEn : String
    let OrderTypeAr : String
    let OrderTypeId : String
    let SectionType : Int
    let StoreId : Int
    let StoreNameEn : String
    let StoreNameAr : String
    let StoreAddressEn : String
    let StoreAddressAr : String
    let CartItemsQty : Int
    let IsSubscription : Bool
    let UserAddressDetails : [CartUserAddressDetailsModel]?
}
struct CartUserAddressDetailsModel : Decodable {
    let Id : Int?
    let Address : String?
    let ContactPerson : String?
    let ContactPersonPhone : String?
    let LandMark : String?
    let Latitude : String?
    let Longitude : String?
    let Name : String?
    let TypeId : String?
}


//MARK: Sub Order Details
struct SubOrderDetailsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : SubOrderDetailsDataModel?
}
struct SubOrderDetailsDataModel : Decodable {
    var SubOrderTypes : [SubOrderTypesModel]?
    var CarBrands : [CarColorsModel]?
    var CarColors : [CarColorsModel]?
    var Cars : [CarsModel]?
}
struct SubOrderTypesModel : Decodable {
    let Id : Int
    let StoreId : Int
    let SubTypeEn : String
    let SubTypeAr : String
    let Image : String
}
struct CarColorsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
}
struct CarsModel : Decodable {
    let Id : Int
    let UserId : Int
    let VehicleNumber : String
    let BrandId : Int
    let BrandEn : String
    let BrandAr : String
    let ColorId : Int
    let ColorEn : String
    let ColorAr : String
    let IsDefault : Bool
    let ColorCode : String
}
