//
//  StoreItemsModel.swift
//  drCafe
//
//  Created by Devbox on 22/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Store Items
struct StoreItemsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [StoreItemsDetailsModel]?
}
struct StoreItemsDetailsModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let DescEn : String?
    let DescAr : String?
    let Image : String?
    var sectionIsExpanded : Bool?
    let Items : [ItemsModel]?
}
struct ItemsModel : Decodable {
    let Id : Int
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let NameEn : String
    let NameAr : String
    let ItemsDescEn : String
    let ItemsDescAr : String
    let CoffeeBeverage : Bool
    let V12 : Bool
    let ServingTime : Double
    let SeqId : Int
    let Image : String
    let Tags : [TagsModel]?
    let Prices : [PricesModel]?
}
struct PricesModel : Decodable {
    let IsOutOfStock : Int?
    let ItemPriceId : Int
    let SizeId : Int
    let Price : Double
    let SizeEn : String
    let SizeAr : String
}
