//
//  RewardsModel.swift
//  drCafe
//
//  Created by Devbox on 03/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

struct ConvertPointsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    //let Orders : [OrderHistoryDetailsModel]?
}
//struct OrderHistoryDetailsModel : Decodable {
//    let OrderId : Int
//    let InvoiceNo : String
//    let FavouriteName : String?
//}


//DCSmile Details
struct DCSmileModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : DCSmileDetailsModel?
}
struct DCSmileDetailsModel : Decodable {
    let Promotions : [DCSmilePromotionsDetailsModel]?
    let UserInfo : [DCSmileUserInfoDetailsModel]?
    let Tiers : [DCSmileTiersDetailsModel]?
    let ExpiredPromotions : [DCSmilePromotionsDetailsModel]?
    let UsedPromotions : [DCSmilePromotionsDetailsModel]?
}
struct DCSmilePromotionsDetailsModel : Decodable {
    let Id : Int
    let PromoCode : String
    let PromoType : Int
    let ImageEn : String
    let ImageAr : String
    let DescriptionEn : String
    let DescriptionAr : String
    let MessageEn : String
    let MessageAr : String?
    let StoresType : Int
    let ValidTill : Int
    let DiscountType: Int
    let DiscountValue: Double
    let MaxAmt: Double
    let StartDate : String
    let EndDate : String
    let StartTime : String
    let EndTime : String
    let IsEnable : Int
    let Quantity : Int
    let ValidDays : [DCSmileValidDaysModel]?
    let MainCategory : [DCSmileMainCategoryModel]?
    let Additionals : [DCSmileAdditionalsModel]?
    let Sizes : [DCSmilePaymentModel]?
    let OrderTypes : [DCSmilePaymentModel]?
    let PaymentMethods : [DCSmilePaymentModel]?
    let StoreList : [DCSmileStoreListModel]?
    let ConsumedDate : String?
}
struct DCSmileValidDaysModel : Decodable {
    let Id : Int
    let WeekDay : Int
    let PromotionId : Int
}
struct DCSmileMainCategoryModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Category : [DCSmileCategoryModel]?
}
struct DCSmileCategoryModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let SubCategory : [DCSmileSubCategoryModel]?
}
struct DCSmileSubCategoryModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Items : [DCSmilePaymentModel]?
}
struct DCSmileAdditionalsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let AdditionalItems : [DCSmileAdditionalItemsModel]?
}
struct DCSmileAdditionalItemsModel : Decodable {
    let Id : Int
    let Name_En : String
    let Name_Ar : String
}
struct DCSmilePaymentModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
}
struct DCSmileStoreListModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let StoreCode : String
    let PromotionId : Int
}
struct DCSmileUserInfoDetailsModel : Decodable {
    let UserId : Int
    let FullName : String
    let LoyaltyPoints : Double
    let LoyaltySmiles : Double
    let WalletAmount : Double
    let MinWalletLimit : String
    let BadgeId : Int
    let BadgeName : String
    let BadgeNameEn : String
    let BadgeNameAr : String
    let GreetingsEn : String
    let GreetingsAr : String
    let DescriptionEn : String
    let DescriptionAr : String
    let UpgradeMsgEn : String
    let UpgradeMsgAr : String
    let BadgeExpiry : String
    let HistoricalSmiles : Double
    let SmilesEarningFrom : String
    let PointsExpiryAr : String?
    let PointsExpiryEn : String?
}
struct DCSmileTiersDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let PromoSmiles : Double
    let MinSmiles : Double
    let EveryRiyal : Double
    let SmilesPerPoint : Double
    let DescriptionEn : String
    let DescriptionAr : String
    let MessageEn : String
    let MessageAr : String
}

//Loyalty Points Details
struct LoyaltyPointsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : LoyaltyPointsDetailsModel?
}
struct LoyaltyPointsDetailsModel : Decodable {
    let LoyaltyPoints : Double
    let WalletAmount : Double
    let BadgeId : Int
    let BadgeNameEn : String
    let BadgeNameAr : String
}


//DCSmile History Details
struct DCSmileHistoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [DCSmileHistoryDetailsModel]?
}
struct DCSmileHistoryDetailsModel : Decodable {
    let Id : Int
    let ReferenceNo : Int
    let InvoiceNo : String
    let BadgeId : Int
    let NameEn : String?
    let NameAr : String?
    let OrderNetTotal : Double
    let Points : Double
    let Operation : String
    let HavingPoint : Double
    let Smiles : Double
    let ExpiryDate : String
    let Converted : Bool
    let CreatedOn : String
    let TotalRows : Int
}

//DCSmile Deals Details
struct DCSmileDealsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [DCSmileDealsDetailsModel]?
}
struct DCSmileDealsDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Value : Int
    let DescriptionEn : String
    let DescriptionAr : String
    let Image : String
    let DisplaySequence : Int
}
