//
//  PromotionsModel.swift
//  drCafe
//
//  Created by Devbox on 28/09/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

//MARK: Load Cart
struct LoadCartPromotionsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : [PromotionsDetailsModel]?
}
struct PromotionsDetailsModel : Decodable {
    let OrderId : Int
    let Id : Int
    let PromoCode : String
    let PromoType : Int
    let ImageEn : String
    let ImageAr : String
    let DescriptionEn : String
    let DescriptionAr : String
    let MessageEn : String?
    let MessageAr : String?
    let StartDate : String
    let EndDate : String
    let StartTime : String
    let EndTime : String
    let IsEnable : Int
    var isSelect : Bool?
    var quantity : Int?
    let Quantity : Int
    let ValidDays : [ValidDaysDetailsModel]?
    let MainCategory : [PromotionsMainCategoryModel]?
    let Additionals : [PromotionsAdditionalsModel]?
    var Sizes : [SizesDetailsModel]?
    var OrderTypes : [SizesDetailsModel]?
    var PaymentMethods : [SizesDetailsModel]?
}
struct ValidDaysDetailsModel : Decodable {
    let Id : Int?
    let WeekDay : Int?
    let PromotionId : Int?
}
struct PromotionsMainCategoryModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let Category : [PromotionsCategoryModel]?
}
struct PromotionsCategoryModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let SubCategory : [PromotionsSubCategoryModel]?
}
struct PromotionsSubCategoryModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let Items : [SizesDetailsModel]?
}
struct PromotionsAdditionalsModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let AdditionalItems : [PromotionsAdditionalItemsModel]?
}
struct PromotionsAdditionalItemsModel : Decodable {
    let Id : Int?
    let Name_En : String?
    let Name_Ar : String?
}
struct SizesDetailsModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
}
