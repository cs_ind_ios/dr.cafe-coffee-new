//
//  StoreListModel.swift
//  drCafe
//
//  Created by Devbox on 18/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Store List
struct StoresListModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [StoreDetailsModel]?
    var Segments : [SegmentsDetailsModel]?
    var Facilities : [SegmentsDetailsModel]?
    var OrderTypes : [OrderTypeDetailsModel]?
    var StoreTypes : [FacilitiesDetailsModel]?
}
struct StoreDetailsModel : Decodable {
    let Id : Int
    let StoreCode : String
    let NameEn : String
    let NameAr : String
    let DescEn : String
    let DescAr : String
    let LandlineNo : String
    let Latitude : String
    let Longitude : String
    let Distance : String?
    let StoreServes : Int?
    let ServDistMsgEn : String?
    let ServDistMsgAr : String?
    let MobileNo : String
    let AddressEn : String
    let AddressAr : String
    let ExclusiveForWomen : Bool
    let StoreStatus : Bool
    let IsTrending : Bool
    let IsActive : Bool
    let StoreType : Int
    let ImagePath : String
    let Favourite : Bool
    let GroupId : Int
    let GroupNameEn : String
    let GroupNameAr : String
    let StoreLaunchDate : String
    let is24x7 : Bool?
    let isStoreCloseToday : Bool?
    let isShiftOpen : Bool
    let OpensInMinutes : Int?
    let OrderTypeBusy : Bool?
    let BusySchedule : [BusyScheduleDetailsModel]?
    let Shifts : [ShiftsModel]?
    let FreeDrinkLimit : String
    let CountryId : Int
    let CountryNameEn : String
    let CountryNameAr : String
    let Facilities : [FacilitiesDetailsModel]?
    let Segments : [SegmentsDetailsModel]?
    var OrderTypes : [SegmentsDetailsModel]?
    let CurrentDateTime : String?
    let ShiftBusinessDate : String?
    let StoreDistance : Double?
}
struct ShiftsModel : Decodable {
    let Id : Int
    let ShiftId : Int
    let WeekDay : Int
    let wkday : String
    let StartTime : String
    let StartDateTime : String
    let EndTime : String
    let EndDateTime : String
    let IsClose : Bool
    let BusinessDate : String
}
struct FacilitiesDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Image : String
    let IsActive : Bool?
    var isSelect : Bool?
}
struct OrderTypeDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Status : Bool
    let Image : String
    let MessageEn : String
    let MessageAr : String
    let IsActive : Bool
    var isSelect : Bool?
}
struct SegmentsDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let Image : String?
    var isSelect : Bool?
}
struct BusyScheduleDetailsModel : Decodable {
    let OrderTypeId : Int
    let IsBusy : Bool
    let OpensInMinute : Int
}

// Store Category
struct StoresCategoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : StoresCategoryDetailsModel?
}
struct StoresCategoryDetailsModel : Decodable {
    let Category : [CategoryDetailsModel]?
    let DiscoverCategory : [CategoryDetailsModel]?
    let NewCategory : [CategoryMainDetailsModel]?
}
struct CategoryMainDetailsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let DescEn : String
    let DescAr : String
    let Image : String
    let BackgroundColor : String?
    let Category : [CategoryDetailsModel]?
}
struct CategoryDetailsModel : Decodable {
    let StoreId : Int?
    let Id : Int
    let NameEn : String
    let NameAr : String
    let DescEn : String
    let DescAr : String
    let Image : String
    let BackgroundColor : String?
    let ForeGroundColor : String?
}

// Store Favourite
struct StoresFavouriteModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [StoresFavouriteDetailsModel]?
}
struct StoresFavouriteDetailsModel : Decodable {
    let Id : Int
    let UserId : Int
    let StoreId : String
    let IsActive : Bool
}
