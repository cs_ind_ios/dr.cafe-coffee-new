//
//  MoreModel.swift
//  drCafe
//
//  Created by Mac2 on 18/05/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation
class MoreModel {
    var name,details,image: String!
    init(name: String,details: String,image: String) {
        self.name = name
        self.details = details
        self.image = image
    }
}
