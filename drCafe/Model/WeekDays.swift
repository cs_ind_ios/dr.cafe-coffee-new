//
//  WeekDays.swift
//  drCafe
//
//  Created by Devbox on 17/06/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
import UIKit
class WeekDaysModel: NSObject {
    static func weekDayNum(Name: String) -> Int {
        switch Name {
        case "Saturday":
            return 1
        case "Sunday":
            return 2
        case "Monday":
            return 3
        case "Tuesday":
            return 4
        case "Wednesday":
            return 5
        case "Thursday":
            return 6
        case "Friday":
             return 7
        default:
            return 0
        }
    }
    static func weekDayName(Num: Int) -> String {
        switch Num {
        case 1:
            return "Saturday"
        case 2:
            return "Sunday"
        case 3:
            return "Monday"
        case 4:
            return "Tuesday"
        case 5:
            return "Wednesday"
        case 6:
            return "Thursday"
        case 7:
             return "Friday"
        default:
            return ""
        }
    }
}
