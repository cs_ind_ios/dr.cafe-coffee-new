//
//  ItemsAdditionalsModel.swift
//  drCafe
//
//  Created by Devbox on 25/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Store Items Additionals
struct StoreItemsAdditionalsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [AdditionalsDetailsModel]?
}
struct AdditionalsDetailsModel : Decodable {
    let Id : Int
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let NameEn : String
    let NameAr : String
    var Image : String
    let MaxOrderQty : Int
    let DescEn : String
    let DescAr : String
    let ServingTime : Double
    let RewardBand : Int?
    let RewardEligibility : Bool?
    let ManualBrew : Bool?
    let CoffeeBeverage : Bool
    let V12 : Bool
    let SeqId : Int
    let Allergens : String?
    let SuggestiveItems : [SuggestiveItemsModel]?
    let Tags : [TagsModel]?
    let Prices : [PriceDetailsModel]?
    var AdditionalGroups : [AdditionalGroupsModel]?
    var Grinders : [GrindersModel]?
    var ItemImages : [ItemImagesModel]?
    //var MoreBeans : [MoreBeansDetailsModel]?
}
struct ItemImagesModel : Decodable {
    let Image : String
    let IsFeatured : Bool
}
struct SuggestiveItemsModel : Decodable {
    let Id : Int
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let NameEn : String
    let NameAr : String
    let Image : String
    let DescEn : String
    let DescAr : String
    let Prices : [PriceDetailsModel]?
}
struct TagsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    var Image : String
}
struct PriceDetailsModel : Decodable {
    let ItemPriceId : Int?
    let SizeId : Int?
    let Price : Double?
    let CatalogItemId : Int?
    let SizeEn : String?
    let SizeAr : String?
    let DescEn : String?
    let DescAr : String?
    let Image : String?
    let Calories : String?
    let CaloriesFromFat : String?
    let UOM : String?
    let TotalFat : String?
    let SaturatedFat : String?
    let TransFat : String?
    let Cholestrol : String?
    let Sodium : String?
    let Carbohydrates : String?
    let DietaryFibers : String?
    let Sugars : String?
    let Protein : String?
    let VitaminA : String?
    let VitaminC : String?
    let Calcium : String?
    let Iron : String?
    let Weight : Double?
    let WeightUOM : String?
    let IsOutOfStock : Int?
    let DefaultSize : Bool?
}
struct AdditionalGroupsModel : Decodable {
    let Id : Int?
    let NameEn : String?
    let NameAr : String?
    let MinSelection : Int?
    let MaxSelection : Int?
    var sectionIsExpand : Bool?
    let GroupType : Int?
    var Additionals : [AdditionalsModel]?
}
struct AdditionalsModel : Decodable {
    let AdditionalItemId : Int
    let ItemId : Int
    let NameEn : String
    let NameAr : String
    var ItemSelect : Bool?
    var additionalQty : Int?
    let Image : String
    let MaxQty : Int
    let HasVariant : Bool
    let IsOutOfStock : Int?
    let AdditionalPrices : [AdditionalPricesModel]?
}
struct AdditionalPricesModel : Decodable {
    let AdditionalPriceId : Int
    let AdditionalItemId : Int
    let SizeId : Int
    let NameEn : String
    let NameAr : String
    let Price : Double
    let IsRecommended : Bool?
}
struct GrindersModel : Decodable {
    let Id : Int
    var selectGrinder : Bool?
    let NameEn : String
    let NameAr : String
    let Price : Double
    let Sequence : Int
    let Recommended : Bool
    let Image : String
    let Icon : String
}
struct MoreBeansDetailsModel : Decodable {
    let Id : Int
    var isSelect : Bool?
    let NameEn : String
    let NameAr : String
    let DescEn : String
    let DescAr : String
    let MetaKeywordsEn : String
    let MetaDescriptionAr : String
    let MetaDescriptionEn : String
    let MetaKeywordsAr : String
    let MainCategoryId : Int
    let CategoryId : Int
    let SubCategoryId : Int
    let ServingTime : Int
    let DisplaySequence : Int
    let IsReturnable : Bool
    let IsSubscribable : Bool
    let OnPackage : Bool
    let GiftWrap : Bool
    let AvailableOn : Int
    let AvailabilitySchedule : Int
    let ItemTag : Int
    let IsDeliverable : Bool
    let IsActive : Bool
    let CreatedOn : String
    let CreatedBy : Int
    let ModifiedOn : String
    let ModifiedBy : Int
    let DeletedOn : String
    let DeletedBy : Int
    let IsDeleted : Bool
    let IsCoffeeBeverage : Bool
    let IsV12 : Bool
    let BrewType : Int
    let CoffeeTypeId : Int?
    let ManualBrew : Bool?
}
