//
//  BetaModel.swift
//  drCafe
//
//  Created by Mac2 on 24/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation

//MARK: Beta Defination Model
struct BetaDefinationModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [BetaDefinationDataModel]?
}
struct BetaDefinationDataModel : Decodable {
    let KeyEn : String
    let KeyAr : String
    let NameEn : String
    let NameAr : String
    let DefinationEn : String
    let DefinationAr : String
    let SequenceNo : Int
    let Programs : [BetaDefinationProgramsModel]?
}
struct BetaDefinationProgramsModel : Decodable {
    let TitleEn : String
    let TitleAr : String
    let DescEn : String
    let DescAr : String
    let SubTitleEn : String
    let SubTitleAr : String
    let ImageEn : String
    let ImageAr : String
    let iCon : String
}

//MARK: Beta Public Events Model
struct BetaPublicEventsModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : BetaPublicEventsDataModel?
}
struct BetaPublicEventsDataModel : Decodable {
    let Events : [BetaEventsModel]?
    let BetaStores : [BetaEventStoresModel]?
    let Bookings : [BetaEventBookingsModel]?
    let Filters : [BetaEventFilterModel]?
}
struct BetaEventsModel : Decodable {
    let Id : Int
    let TitleEn : String
    let TitleAr : String
    let DescEn : String
    let DescAr : String
    let ImageEn : String
    let ImageAr : String
    let Icon : String
    let InstructionEn : String
    let InstructionAr : String
    let EventDate : String
    let StartTime : String
    let EndTime : String
    let MaxGuest : Int
    let PricePerSeat : Double
    let MaxSeatsPerbook : Int
    let AvailableSeats : Int
    let StoreId : Int
    let StoreNameEn : String
    let StoreNameAr : String
    let GeoCoordinates : String
    let StoreAddressEn : String
    let StoreAddressAr : String
    let StoreContact : String
    let StoreCode : String
    let StoreCity : String
    let StoreRegion : String
    let SequenceNo : Int
    let CategorId : Int
    let CategoryEn : String
    let CategoryAr : String
}
struct BetaEventStoresModel : Decodable {
    let Id : Int
    let StoreCode : String
    let NameEn : String
    let NameAr : String
    let AddressEn : String
    let AddressAr : String
    let DescEn : String
    let DescAr : String
    let Latitude : String
    let Longitude : String
    let MobileNo : String
}
struct BetaEventBookingsModel : Decodable {
    let OrderStatusId : Int
    let GuestName : String
    let GuestContact : String?
    let NetTotal : Double
    let Guests : Int
    let Bookingfor : Int
    let CreatedOn : String
    let InvoiceNo : String
    let OrderStatus : String
    let RequestStatus : String
    let UserId : Int
    let OTP : Int
    let TitleEn : String
    let TitleAr : String
    let DescEn : String
    let DescAr : String
    let EventDate : String
    let EndTime : String
    let Icon : String
    let ImageEn : String
    let ImageAr : String
    let StartTime : String
    let CustomerName : String
    let CustomerMobile : String
    let CurrentDate : String
    let CategorId : Int
    let CategoryEn : String
    let CategoryAr : String
}
struct BetaEventFilterModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let `Type` : String
}

//MARK: Beta Booking Areas Model
struct BetaBookingAreasModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [BetaBookingAreasDataModel]?
}
struct BetaBookingAreasDataModel : Decodable {
    let Id : Int
    let TitleEn : String
    let TitleAr : String
    let SubTitleEn : String
    let SubTitleAr : String
    let DescEn : String
    let DescAr : String
    let MaxGuestCapacity : Int
    let isTempOff : Bool
    let TempMsgEn : String
    let Image : String
}

//MARK: History Model
struct BetaHistoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : BetaHistoryDetailsModel?
}
struct BetaHistoryDetailsModel : Decodable {
    let Events : [BetaHistoryEventsModel]?
    let Enquiry : [BetaHistoryEnquiryModel]?
}
struct BetaHistoryEventsModel : Decodable {
    let EventTitleEn : String
    let EventTitleAr : String
    let EventDate : String
    let StartTime : String
    let EndTime : String
    let Reviewed : Bool
    let Icon : String
    let ReviewComment : String
    let RequestStatus : String
}
struct BetaHistoryEnquiryModel : Decodable {
    let TitleEn : String
    let TitleAr : String
    let EventDate : String
    let StartTime : String
    let EndTime : String
    let Reviewed : Bool
    let ReviewComment : String
    let RequestStatus : String
}

//MARK: Beta Booking Model
struct BetaBookingModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [BetaBookingDataModel]?
    let PaymentMethods : [PayementMethodsModel]?
}
struct BetaBookingDataModel : Decodable {
    let Id : Int?
    let EventId : Int?
    let UserId : Int?
    let BookingFor : Int?
    let Guests : Int?
    let InvoiceNo : String?
    let Price : Double?
    let GrossTotal : Double?
    let ItemTotal : Double?
    let Vatpercentage : Int?
    let Vat : Double?
    let NetTotal : Double?
    let MinutesLeft : Int?
    let Mobile : String?
    let FullName : String?
    let Email : String?
    let OTP : Int?
    let GuestName : String?
    let GuestContact : String?
    let TitleEn : String?
    let TitleAr : String?
    let DescEn : String?
    let DescAr : String?
    let ImageEn : String?
    let ImageAr : String?
    let Icon : String?
    let MaxGuest : Int?
    let StartTime : String?
    let EndTime : String?
    let EventDate : String?
    let PricePerSeat : Double?
    let StoreId : Int?
    let StoreNameEn : String?
    let StoreNameAr : String?
    let GeoCoordinates : String?
    let StoreAddressEn : String?
    let StoreAddressAr : String?
    let StoreContact : String?
    let StoreCode : String?
    let StoreCity : String?
    let StoreRegion : String?
}


//MARK: Filter Model
class FilterModel {
    var name: String!
    var isSelect: Bool!
    init(name: String,isSelect: Bool) {
        self.name = name
        self.isSelect = isSelect
    }
}

//MARK: New History Model
struct NewBetaHistoryModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : NewBetaHistoryDataModel?
}
struct NewBetaHistoryDataModel : Decodable {
    let MyBookings : [BetaHistoryBookingsModel]?
    let Filters : [BetaHistoryFilterModel]?
}
struct BetaHistoryBookingsModel : Decodable {
    let Id : Int
    let UserId : Int
    let EventId : Int?
    let ContactPerson : String?
    let ContactPhone : String?
    let GuestsReserved : Int?
    let InvoiceNo : String?
    let GrossTotal : Double?
    let Discount : Double?
    let ItemTotal : Double?
    let PaymentId : String?
    let PaymentOn : String?
    let GuestsArrived : Int?
    let OTP : Int?
    let Bookingfor : Int?
    let Reviewed : Int?
    let ReviewComment : String?
    let Reviewedon : String?
    let deviceinfo : String?
    let RequestStatus : String?
    let TitleEn : String?
    let TitleAr : String?
    let DescEn : String?
    let DescAr : String?
    let ImageEn : String?
    let ImageAr : String?
    let Icon : String?
    let EventDate : String?
    let StartTime : String?
    let EndTime : String?
    let MaxGuest : Int?
    let PricePerSeat : Double?
    let MaxSeatsPerbook : Int?
    let StoreNameEn : String?
    let StoreNameAr : String?
    let StoreMobileNo : String?
    let AddressEn : String?
    let AddressAr : String?
    let Latitude : String?
    let Longitude : String?
    let BookingType : String?
    let CreatedOn : String?
    let CategoryId : Int?
    let CategoryEn : String?
    let CategoryAr : String?
    let ShareableMsg : String?
    let MinutesLeft : Int?
    let OrderStatusId : Int?
    var OTPOpen : Bool?
    var IsCancellable : Bool?
    let InstructionEn : String?
    let InstructionAr : String?
}
struct BetaHistoryFilterModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
    let BookingType : String
}
