//
//  MySubscriptionsModel.swift
//  drCafe
//
//  Created by Devbox on 16/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

//MARK: My Subscription
struct MySubscriptionModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    var Data : [MySubscriptionDetailsModel]?
}
struct MySubscriptionDetailsModel : Decodable {
    let OrderId : Int
    let SubOrderId : Int
    let NetTotal : Double
    let InitiateDate : String
    let Discount : Double?
    let InvoiceNo : String
    let SubInvoiceNo : String?
    let DeliveryAddress : String
    let PaymentMethod : String
    let PaymentMethodAr : String?
    let PaymentBrandIcon : String?
    let Frequency : Int
    let OrderStatus : String
    let OrderStatusEn : String
    let SubscriptionId : Int
    let RegistrationId : String?
    let FrequencyNameEn : String
    let FrequencyNameAr : String
    let IsSuspended : Bool
    let IsReschedule : Bool?
    let ShippingMethodAr : String
    let ShippingMethodEn : String
    let RunningOrderDate : String
    let SlotTiming : String
    let StartDate : String?
    let PauseStartDate : String?
    let PauseMaxDays : Int
    let RescheduleStartDate : String?
    let MaxDays : Int?
    let RetryPayment : Int?
    let RescheduleMaxDays : Int
    let ItemDetails : [MySubscriptionItemsModel]?
    let FrequencyOptions : [FrequencyOptionsModel]?
    let PaymentDetails : [OrderHistoryPaymentDetailsModel]?
}
struct MySubscriptionItemsModel : Decodable {
    let Image : String
    let ItemNameEn : String
    let Qty : Int
    let ItemNameAr : String
}
