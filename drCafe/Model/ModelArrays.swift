//
//  ModelArrays.swift
//  drCafe
//
//  Created by Devbox on 06/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation
struct AttachementArrayModel : Decodable {
    let FileName :String!
    let FileType :String!
    let Base64 :String!
}

struct LanguageArrayModel : Decodable {
    let Name :String!
    var IsSelect :Bool!
}
