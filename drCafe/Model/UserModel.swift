//
//  UserModel.swift
//  drCafe
//
//  Created by Devbox on 10/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Sign In
struct SignInModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : SignInUserDetailsModel?
}
struct SignInUserDetailsModel : Decodable {
    let Id : Int?
    let OTP : String?
    let FullName : String?
    let NickName : String?
    let Mobile : String?
    let IsVerified : Bool?
    let Email : String?
    let EmailVerified : Bool?
    let CountryCode : String?
    let ProfilePic : String?
    let Gender : String?
    let Language : String?
    let DateOfBirth : String?
    let Profession : Int?
    let Interests : String?
    let BadgeId : Int?
    let BadgeExpiry : String?
    let RegistrationType : String?
    let NotificationARN : String?
    let isActive : Bool?
    let CreatedDate : String?
    let ModifiedOn : String?
    let CartItemsQuantity : String?
    let SubCartItem : Int?
    let StoreId : Int?
    let StoreNameAr : String?
    let StoreNameEn : String?
    let TotalPrice : Double?
    let subTotalPrice : Double?
    let Themes : [themesDetailsModel]?
}

// Forget Password
struct ForgetPasswordModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : ForgetPasswordDetailsModel?
}
struct ForgetPasswordDetailsModel : Decodable {
    let Id : Int?
    let OTP : String?
}

// Change Password
struct ChangePasswordModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    //let Data : SignInUserDetailsModel?
}

// SignUp Model
struct SignUpModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : SignUpDetailsModel?
}
struct SignUpDetailsModel : Decodable {
    let Id : Int?
    let OTP : String?
    let Mobile : String?
}

//Verify OTP Model
struct VerifyOTPModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : VerifyOTPDetailsModel?
}
struct VerifyOTPDetailsModel : Decodable {
    let Id : Int?
    let FullName : String?
    let NickName : String?
    let Email : String?
    let OTP : String?
    let Gender : String?
    let Mobile : String?
    let isActive : Bool?
    let CreatedOn : String?
    let ModifiedOn : String?
    let CountryCode : String?
}

//Country Codes
struct CountryCodeModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [CountryCodeDetailsModel]?
}
struct CountryCodeDetailsModel : Decodable {
    let Id : Int
    let CountryNameEn : String
    let CountryNameAr : String
    let CountryShortCode : String
    let MobileCode : Int
    let NumberOfDigits : Int
    let StatusCode : String
    let StatusMessage : String
    let MaxLength : Int?
    let MinLength : Int?
}

//Selection
struct DropDownModel : Decodable {
    var ID : String
    var Name : String
}

//Profile Screen
struct ProfileModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : ProfileDetailsModel?
}
struct ProfileDetailsModel : Decodable {
    let User : ProfileUserDetailsModel?
    let StandardProfessions : [StandardInterestsModel]?
    let StandardInterests : [StandardInterestsModel]?
}
struct ProfileUserDetailsModel : Decodable {
    let Id : Int
    let FullName : String
    let NickName : String
    let Mobile : String
    let IsVerified : Bool
    let Email : String
    let EmailVerified : Bool
    let CountryCode : String
    let ProfilePic : String
    let Gender : String
    let Language : String
    let DateOfBirth : String
    let Profession : Int
    let Interests : [StandardInterestsModel]?
}
struct StandardInterestsModel : Decodable {
    let Id : Int
    let NameEn : String
    let NameAr : String
}

// Share App
struct ShareAppModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : ShareAppDataModel?
}
struct ShareAppDataModel : Decodable {
    let Message : String
    let ReferalCode : String

}
// Response model
struct ResponseModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
}

struct signupModel : Decodable {
    let UserId : Int
    let FullName : String
    let Email : String
    let Mobile : String
}
