//
//  OrderTrackingModel.swift
//  drCafe
//
//  Created by Devbox on 20/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Order Tracking
struct OrderTrackingModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : [OrderTrackingDetailsModel]?
}
struct OrderTrackingDetailsModel : Decodable {
    let OrderId : Int?
    let Favourite : Bool?
    let OrderTypeId : String
    let SectionType : Int?
    let ShippingTime : String
    let OrderStatusEn : String?
    let OrderStatusAr : String?
    let ItemTotal : Double?
    let DeliveryCharge : Double?
    let VatPecentage : Int?
    let Wallet :Double?
    let Vatcharges : Double?
    let NetTotal : Double?
    let Discount: Double?
    let CartItemsQuantity : Int?
    let PaymentTypeEn : String?
    let PaymentTypeAr : String?
    let OrderTypeEn : String?
    let OrderTypeAr : String?
    let ExpecttedDateTime : String
    let EnableCancel : Bool?
    let InvoiceNo : String?
    let UserDetails : [UserDetailsModel]?
    let Store : [OrderStoreDetailsModel]?
    let DeliveryAddress : [DeliveryAddressDetailsModel]?
    let Items : [OrderItemsDetailsModel]?
    let Tracking : [TrackingDetailsModel]?
    let OrderRating : [OrderRatingDetailsModel]?
    let DriverRating : [OrderRatingDetailsModel]?
    let FrequencyId : Int?
    let FrequencyNameEn : String?
    let FrequencyNameAr : String?
    let IsSubscription : Bool?
    let ConsumedPoints : Int?
    let PromoCode : String?
    let SubOrderNameAr : String?
    let SubOrderNameEn : String?
    let SubOrderTypeId : Int?
    let CarId : Int?
    let CarJson : String?
}
struct UserDetailsModel : Decodable {
    let Name : String?
    let Phone : String?
}
struct OrderStoreDetailsModel : Decodable {
    let PhoneNumber : String?
    let StoreCode : String?
    let StoreNameEn : String?
    let StoreNameAr : String?
    let StoreAddressEn : String?
    let StoreAddressAr : String?
    let StoreLat : String?
    let StoreLong : String?
}
struct DeliveryAddressDetailsModel : Decodable {
    let Lat : String?
    let Long : String?
    let Details : String?
    let ContactPerson : String?
    let ContactNo : String?
}
struct OrderItemsDetailsModel : Decodable {
    let ItemNameEn : String?
    let ItemNameAr : String?
    let Image : String?
    let itemTypeEn : String?
    let itemTypeAr : String?
    let Quantity : Int?
    let ItemPrice : Double?
    let ItemTotalPrice : Double?
    let ItemSubTotal : Double?
    let TotalPrice : Double?
    let GrinderId : Int?
    let GrinderNameEn : String?
    let GrinderNameAr : String?
    let ConsumedPoints : Int?
    let RewardBand : Int?
    let ItemComments : String?
    let Additionals : [OrderItemsAdditionalsModel]?
}
struct OrderItemsAdditionalsModel : Decodable {
    let GroupId : Int?
    let AdditionalId : Int?
    let NameEn : String?
    let NameAr : String?
    let Quantity : Int?
    let Image : String?
    let Price : Double?
    let HasVariant : Bool?
    let IsOutOfStock : Int?
    let CatalogUpdated : Bool?
}
struct TrackingDetailsModel : Decodable {
    let OrderStatusId : Int
    let OrderStatusEn : String?
    let OrderStatusAr : String?
    let Time : String?
    let Comment : String?
}
struct OrderRatingDetailsModel : Decodable {
//    let Value : Int
//    let Comment : String
}
