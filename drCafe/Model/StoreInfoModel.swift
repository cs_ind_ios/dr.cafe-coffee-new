//
//  StoreInfoModel.swift
//  drCafe
//
//  Created by Devbox on 28/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

// Store Items Additionals
struct StoreInfoModel : Decodable {
    let Status : Bool
    let MessageEn : String
    let MessageAr : String
    let Data : StoreInfoDetailsModel?
}
struct StoreInfoDetailsModel : Decodable {
    let StoreImages : [StoreImagesModel]?
    let Id : Int
    let StoreCode : String
    let NameEn : String
    let NameAr : String
    let DescEn : String
    let DescAr : String
    let LandlineNo : String
    let Latitude : String
    let Longitude : String
    let StoreRatings : Int
    let MobileNo : String
    let AddressEn : String
    let AddressAr : String
    let ExclusiveForWomen : Bool
    let StoreStatus : Bool
    let IsTrending : Bool
    let IsActive : Bool
    let ImagePath : String
    let StoreType : Int
    var Favourite : Bool
    let GroupId : Int
    let GroupNameEn : String
    let GroupNameAr : String
    let StoreLaunchDate : String
    let is24x7 : Bool
    let isStoreCloseToday : Bool?
    let isShiftOpen : Bool
    let Shifts : [StoreInfoShiftsModel]?
    let FreeDrinkLimit : String
    let CountryId : Int
    let CountryNameEn : String
    let CountryNameAr : String
    let Facilities : [FacilitiesDetailsModel]?
    let Segments : [SegmentsDetailsModel]?
    let OrderTypes : [SegmentsDetailsModel]?
    let StoreTypes : [SegmentsDetailsModel]?
    let BusySchedule : [OrderTypeBusyModel]?
    let Kilometers : Double
    let BetaAreas : [BetaAreasModel]?
}
struct OrderTypeBusyModel : Decodable {
    let OrderTypeId : Int
    let IsBusy : Bool
    let OpensInMinute : Int
}
struct StoreImagesModel : Decodable {
    let Id : Int
    let Image : String
    let StoreId : Int
}
struct StoreInfoShiftsModel : Decodable {
    let Id : Int
    let ShiftId : Int
    let WeekDay : Int
    let StartTime : String
    let EndTime : String
    let IsClose : Bool
}
struct BetaAreasModel : Decodable {
    let Id : Int
    let TitleEn : String
    let TitleAr : String
    let SubTitleEn : String
    let SubTitleAr : String
    let MaxGuestCapacity : Int
    let Image : String
}
