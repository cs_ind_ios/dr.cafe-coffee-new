//
//  SubscriptionModuleServices.swift
//  drCafe
//
//  Created by Devbox on 26/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
class SubscriptionModuleServices{
    class func MySubscriptionServiceWithOutLoader(dic:[String:Any],success: @escaping (_ result:MySubscriptionModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.MySubscriptions.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(MySubscriptionModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func SubscriptionHistoryService(dic:[String:Any],success: @escaping (_ result:SubscriptionHistoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.SubscriptionHistory.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(SubscriptionHistoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func SubscriptionTrackService(dic:[String:Any],success: @escaping (_ result:SubscriptionTrackModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.SubscriptionTrackService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(SubscriptionTrackModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func SubscriptionCancelService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.SubscriptionCancelService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func RetryService(dic:[String:Any],success: @escaping (_ result:GetAddCardStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.retryService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(GetAddCardStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func PauseOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.pauseOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func RescheduleOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.rescheduleOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ResumeOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.resumeOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
}
