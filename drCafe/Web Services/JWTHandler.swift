//
//  JWTHandler.swift
//  drCafe
//
//  Created by Creative Solutions on 10/26/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation
import Alamofire
class JWTHandler {
    class func Request(mainUrl:String, params:Parameters, method:HTTPMethod, isLoader:Bool, isUIDisable:Bool, success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            if isLoader == true {
                ANLoader.showLoading("", disableUI: isUIDisable)
            }
            let jwtTimeManage = Alamofire.SessionManager.default
                jwtTimeManage.session.configuration.timeoutIntervalForRequest = 60
                jwtTimeManage.session.configuration.timeoutIntervalForResource = 60
            let headers: HTTPHeaders = ["Authorization":authorization,"Content-Type":"application/json"]
            let oAuthUrl = url.getOAuthService.path()
            let escapedString = oAuthUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
            let url = URL(string:escapedString)!
            let dic:[String:Any] = ["Username": username,"Key_Pair": key_Pair, "Pair_Value":pair_Value]
            jwtTimeManage.request(url, method: .post, parameters: dic, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                    if let jwtData = response.data{
                        do {
                            //let json = try JSONDecoder().decode(String.self, from: jwtData)
                            let json =  try JSONSerialization.jsonObject(with: jwtData, options: [.allowFragments]) as! String
                            UserDef.saveToUserDefault(value: json, key: "JWT")
                            let timeManage = Alamofire.SessionManager.default
                            timeManage.session.configuration.timeoutIntervalForRequest = 60
                            timeManage.session.configuration.timeoutIntervalForResource = 60
                            let headers: HTTPHeaders = ["Authorization":"Bearer \(json)","Content-Type":"application/json"]
                            let url1 = URL(string:mainUrl)!
                            if method == .get {
                                timeManage.request(url1,headers: headers).responseJSON { (response) in
                                    if isLoader == true {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                            ANLoader.hide()
                                        }
                                    }
                                    if let _ = response.response{
                                        if response.response!.statusCode == 401 {
                                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                        }else{
                                            if let _ = response.data{
                                                success(response.data!)
                                            }else{
                                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                            }
                                        }
                                    }else{
                                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                    }
                                }
                            }else{
                                timeManage.request(url1, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in

                                    if isLoader == true {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                            ANLoader.hide()
                                        }
                                    }
                                    if let _ = response.response{
                                        if response.response!.statusCode == 401 {
                                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                        }else{
                                            if let _ = response.data{
                                                success(response.data!)
                                            }else{
                                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                            }
                                        }
                                    }else{
                                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                                    }
                                }
                            }
                        }catch{
                            if isLoader == true {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    ANLoader.hide()
                                }
                            }
                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                        }
                    }else{
                        if isLoader == true {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                ANLoader.hide()
                            }
                        }
                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                    }
              }
        }else{
            if isLoader == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
            onError(NoNetwork)
        }
    }
}
    
