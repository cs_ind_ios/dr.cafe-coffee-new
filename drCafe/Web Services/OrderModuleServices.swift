//
//  OrderModuleServices.swift
//  drCafeVendor
//
//  Created by RAVI on 01/07/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
@available(iOS 13.0, *)
class OrderModuleServices{
    //MARK:DashBoard Service
    class func DashBoardService(dic:[String:Any],success: @escaping (_ result:DashBoardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.dashBoardService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(DashBoardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DashBoardServiceWithoutLoader(dic:[String:Any],success: @escaping (_ result:DashBoardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.dashBoardService.path(), params: param, method: .post) { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(DashBoardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        } onError: { error in
            onError(error)
        }
    }
    //MARK:DashBoard Popup Service
    class func PopUpUserVisibilityUpdateService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.PopUpUserVisibilityUpdate.path(), params: param, method: .post, isLoader: false, isUIDisable: true, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Bulk Order Service
    class func BulkOrderService(dic:[String:Any],success: @escaping (_ result:BulkOrderModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.bulkOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(BulkOrderModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Catering Order Service
    class func CateringOrderService(dic:[String:Any],success: @escaping (_ result:BulkOrderModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.cateringOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
               //
                do{
                    let result1 = try JSONDecoder().decode(BulkOrderModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Address Module
    class func AddressListService(dic:[String:Any],success: @escaping (_ result:AddressListModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addressList.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(AddressListModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func AddressDeleteService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addressDelete.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func AddressEditService(dic:[String:Any],success: @escaping (_ result:SaveAddressModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addressEdit.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(SaveAddressModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func AddressSaveService(dic:[String:Any],success: @escaping (_ result:SaveAddressModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addressSave.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
               //
                do{
                    let result1 = try JSONDecoder().decode(SaveAddressModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func AddressUpdateService(dic:[String:Any],success: @escaping (_ result:UpdateAddressModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addressUpdate.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
               //
                do{
                    let result1 = try JSONDecoder().decode(UpdateAddressModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    
    //MARK:Store List
    class func GetStoreListService(dic:[String:Any],success: @escaping (_ result:StoresListModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getStoreList.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(StoresListModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Stores
    class func GetStoresService(dic:[String:Any],success: @escaping (_ result:StoresListModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getStores.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(StoresListModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Categories
    class func GetStoreCategoriesService(dic:[String:Any],success: @escaping (_ result:StoresCategoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.storeCategories.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
               //
                do{
                    let result1 = try JSONDecoder().decode(StoresCategoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Categories Delivery
    class func GetStoreCategoyDeliveryService(dic:[String:Any],success: @escaping (_ result:StoresCategoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.storeCategoryDelivery.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(StoresCategoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func GetSubScriptionCategoryService(dic:[String:Any],success: @escaping (_ result:SubScriptionCategoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.SubscriptionCategory.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(SubScriptionCategoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Items Additionals
    class func GetAdditionalItemsService(dic:[String:Any],success: @escaping (_ result:StoreItemsAdditionalsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.itemDetails.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(StoreItemsAdditionalsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Items Service
    class func GetStoreItemsService(dic:[String:Any],success: @escaping (_ result:StoreItemsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.storeItems.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(StoreItemsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Items Service
    class func GetDiscoverStoreItemsService(dic:[String:Any],success: @escaping (_ result:StoreItemsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.discoverStoreItems.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(StoreItemsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Information Service
    class func GetStoreInfoService(dic:[String:Any],success: @escaping (_ result:StoreInfoModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.storeInfo.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(StoreInfoModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Store Make Favourite
    class func GetStoreMakeFavouriteService(dic:[String:Any],success: @escaping (_ result:StoresFavouriteModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.storeMakeFavourite.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(StoresFavouriteModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK:Theme Color Update
    class func GetThemeColorUpdateService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.themeColoeUpdate.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Redeem Rewards
    class func ConvertPointsToPayBalanceService(dic:[String:Any],success: @escaping (_ result:ConvertPointsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.convertPointsBalanceService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ConvertPointsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DCSmileService(dic:[String:Any],success: @escaping (_ result:DCSmileModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.DCSmileService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(DCSmileModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DCSmileDealsService(dic:[String:Any],success: @escaping (_ result:DCSmileDealsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.DCSmileDealsService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(DCSmileDealsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DCSmileHistoryService(dic:[String:Any],success: @escaping (_ result:DCSmileHistoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.DCSmileHistoryService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(DCSmileHistoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func GetLoyaltyPointsService(dic:[String:Any],success: @escaping (_ result:LoyaltyPointsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.GetLoyaltyPointsService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //
                do{
                    let result1 = try JSONDecoder().decode(LoyaltyPointsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
}
