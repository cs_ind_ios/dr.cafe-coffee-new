//
//  URLS.swift
//  drCafeVendor
//
//  Created by RAVI on 27/06/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import Alamofire

let AppURL = "http://onelink.to/drcafeapp"

//Test URL
//let baseURL = "http://csadms.com/dcwebapi/"
//let imagePath = "http://csadms.com/dcweb/"
//let baseURL = "https://drcafestore.azurewebsites.net/api/"
//let imagePath = "https://drcafestore.azurewebsites.net/"

//Live
//let baseURL = "https://drcafeapi.azurewebsites.net/"
//let imagePath = "https://drcafeapi.azurewebsites.net/"

//Live 2.1
//let baseURL = "https://drcafeapi.azurewebsites.net/v2.1/"
//let imagePath = "https://drcafeapi.azurewebsites.net/v2.1/"

let baseURL = "https://drcafeapi.azurewebsites.net/v2.2/"
let imagePath = "https://drcafeapi.azurewebsites.net/v2.2/"

enum url: String{
    // Get JWT Token
    case getOAuthService = "api/UserAPIs/OAuth"
    
    //User Module
    case signInService = "api/User/Login"
    case forgetPassword = "api/User/ForgetPassword"
    case signUpService = "api/User/Registration"
    case changePassword = "api/User/ChangePassword"
    case verifyOTP = "api/User/VerifyOTP"
    case newPassword = "api/User/ResetPassword"
    case countryCode = "api/Country/GetCountryList"
    case profile = "api/User/EditProfile"
    case updateProfile = "api/User/UpdateProfile"
    case changeLanguage = "api/User/LanguageChange"
    case logout = "api/User/Logout"
    case deleteAccount = "api/User/DeleteAccount"
    
    //Address
    case addressList = "api/Address/GetAddress"
    case addressDelete = "api/Address/DeleteAddress"
    case addressEdit = "api/Address/EditAddress"
    case addressSave = "api/Address/SaveNewAddress"
    case addressUpdate = "api/Address/UpdateAddress"
    case updateOrderAddress = "api/Address/UpdateOrderAddress"

    //Main Module
    case dashBoardService = "api/Dashboard/GetDashboard"
    case PopUpUserVisibilityUpdate = "api/PopUps/PopUpUserVisibilityUpdate"
    case bulkOrderService = "api/Order/BulkOrder"
    case cateringOrderService = "api/Order/Catering"
    case getStoreList = "api/Store/GetStoreList"
    case getStores = "api/Store/StoreList"
    case storeCategories = "api/Store/StoreCategories"
    case storeCategoryDelivery = "api/Store/Delivery"
    case additionalItems = "api/Store/AdditionalItems"
    case itemDetails = "api/Store/ItemDetails"
    case storeItems = "api/Store/StoreItems"
    case discoverStoreItems = "api/Store/DiscoveryItems"
    case storeInfo = "api/Store/StoreInformation"
    case storeMakeFavourite = "api/Store/MakeFavourite"
    case themeColoeUpdate = "api/User/UpdateTheme"
    
    //Cart Module
    case addToCart = "api/Cart/AddToCart"
    case loadCart = "api/Cart/LoadCart"
    case updateCartItem = "api/Cart/UpdateCartItem"
    case emptyCart = "api/Cart/EmptyCart"
    case removeCartItem = "api/Cart/RemoveCartItem"
    case promotionUpdate = "api/Cart/ApplyPromotion"
    case frequencyUpdate = "api/Subscription/UpdateFrequency"
    case getCurrentTime = "api/Subscription/GetCurrentTime"
    case shippingMethod = "api/Cart/ShippingMethod"
    case promotionDetails = "api/Cart/Promotion"
    case cartDetails = "api/Cart/CartsInfo"
    case applyRewardPoints = "api/Cart/RewardBand"
    case getAppVersion = "Api/AppVersion/GetAppVersionByAppType"
    case addCustomerCar = "api/CustomerCars/NewAddCustomerCars"
    case deleteCustomerCar = "api/CustomerCars/NewDeleteCustomerCars"
    case subOrderDetails = "api/Order/NewGetSubOrderDetails"
    
    //Order Module
    case insertOrder = "api/Order/ConfirmOrder"
    case orderTracking = "api/Order/OrderTracking"
    case rateOrder = "api/Order/RateOrder"
    case deleteOrder = "api/Order/DeleteOrder"
    case toggleFevOrder = "api/Order/ToggleFavOrder"
    case orderHistory = "api/Order/OrderHistory"
    case cancelOrder = "api/Order/CancelOrder"
    case reOrderService = "api/Order/ReOrder"
    case orderDetails = "api/Order/"
    case pauseOrderService = "api/Subscription/SkipOrder"
    case resumeOrderService = "api/Subscription/ResumeOrder"
    case retryService = "api/HyperpayAPI/RecurringPayment"
    case rescheduleOrderService = "api/Subscription/Reschedule"
    
    //share
    case shareApp = "api/User/ShareApp"
    
    //Rewards
    case convertPointsBalanceService = "api/WalletAPI/ConvertPointsToPayBalance"
    case DCSmileService = "api/DrCafeSmileAPIs/NewGetData"
    case DCSmileDealsService = "api/DealsAPIs/GetDeals"
    case GetLoyaltyPointsService = "api/WalletAPI/GetWalletLoyalityPoints"
    case DCSmileHistoryService = "api/WalletAPI/GetWalletLoyalityTransactions"
    
    //Subscription
    case SubscriptionCategory = "api/Store/Subscription"
    case MySubscriptions = "api/Subscription/All"
    case CardUpdate = "api/Subscription/CardUpdate"
    case SubscriptionHistory = "api/Subscription/History"
    case SubscriptionTrackService = "api/Subscription/Tracking"
    case SubscriptionCancelService = "api/Subscription/Cancel"
    
    //Beta Module
    case BetaDefinationService = "api/beta/definations/beta/Getdefination"
    case BetaPublicEventsService = "api/beta/PublicEvents/beta/GetPublicEvents"
    case BetaBookingAreasService = "api/beta/bookingAreas/beta/GetbookingAreas"
    case EventBookService = "api/beta/bookEvent"
    case BookEnquiryService = "api/beta/bookEnquiry"
    case BookingHistoryService = "api/beta/MyBooking"
    case BookingEventCancelService = "api/beta/NewCancelEventBooking"

    func path() -> String {
        return baseURL+self.rawValue
    }

}
enum displayImages: String{
    case images = "admin/Uploads/"
    case profileImages = "UploadImages/"

    func path() -> String {
        return imagePath+self.rawValue
    }
    func ProfilePath() -> String {
        return baseURL+self.rawValue
    }
}
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
