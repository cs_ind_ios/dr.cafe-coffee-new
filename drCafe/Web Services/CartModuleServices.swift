//
//  CartModuleServices.swift
//  drCafe
//
//  Created by Devbox on 07/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
class CartModuleServices{
    //MARK:Cart Module
    class func AddToCartService(dic:[String:Any],success: @escaping (_ result:AddToCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.addToCart.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(AddToCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func CartDetailsService(dic:[String:Any],success: @escaping (_ result:MultipleOrdersCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.cartDetails.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(MultipleOrdersCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func CartDetailsWithOutLoaderService(dic:[String:Any],success: @escaping (_ result:MultipleOrdersCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.cartDetails.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(MultipleOrdersCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func LoadCartService(dic:[String:Any],success: @escaping (_ result:LoadCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.loadCart.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                print(json)
                do{
                    let result1 = try JSONDecoder().decode(LoadCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func LoadCartWithOutLoaderService(dic:[String:Any],success: @escaping (_ result:LoadCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.loadCart.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(LoadCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func UpdateCartItemService(dic:[String:Any],success: @escaping (_ result:LoadCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.updateCartItem.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(LoadCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func EmptyCartService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.emptyCart.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func RemoveCartItemService(dic:[String:Any],success: @escaping (_ result:LoadCartModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.removeCartItem.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                print(json)
                do{
                    let result1 = try JSONDecoder().decode(LoadCartModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ShippingMethodService(dic:[String:Any],success: @escaping (_ result:LoadCartShippingModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.shippingMethod.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(LoadCartShippingModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func OrderShippingMethodService(dic:[String:Any],success: @escaping (_ result:OrderDetailsShippingModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.shippingMethod.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(OrderDetailsShippingModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ApplyRewardPointsService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.applyRewardPoints.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func PomotionDetailsService(dic:[String:Any],success: @escaping (_ result:LoadCartPromotionsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.promotionDetails.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(LoadCartPromotionsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DeliveryFrequencyUpdateService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.frequencyUpdate.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func GetCurrentTimeService(dic:[String:Any],success: @escaping (_ result:CurrentTimeModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.getCurrentTime.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(CurrentTimeModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func PromotionAppliedService(dic:[String:Any],success: @escaping (_ result:PromotionModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.promotionUpdate.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(PromotionModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //MARK: Order Module
    class func ConfirmOrderService(dic:[String:Any],isLoader:Bool, isUIDisable:Bool, success: @escaping (_ result:ConfirmOrderModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.insertOrder.path(), params: param, method: .post, isLoader: isLoader, isUIDisable: isUIDisable, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ConfirmOrderModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ConfirmOrderService(dic:[String:Any],success: @escaping (_ result:ConfirmOrderModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.insertOrder.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ConfirmOrderModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func OrderDetailsService(dic:[String:Any],url:String,success: @escaping (_ result:OrderDetailsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        
        APIHandler.Request(url: url, params: param, method: .get, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(OrderDetailsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func OldOrderDetailsService(dic:[String:Any],url:String,success: @escaping (_ result:OldOrderDetailsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url, params: param, method: .get, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(OldOrderDetailsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func OrderTrackingService(dic:[String:Any],success: @escaping (_ result:OrderTrackingModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.orderTracking.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(OrderTrackingModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ToggleFevOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.toggleFevOrder.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DeleteOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.deleteOrder.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func RateOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.rateOrder.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func OrderHistoryService(dic:[String:Any],success: @escaping (_ result:OrderHistoryModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.orderHistory.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                print(json)
                do{
                    let result1 = try JSONDecoder().decode(OrderHistoryModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func CancelOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.cancelOrder.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func ReOrderService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.reOrderService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Getting time
    class func LocationGetService(url:String,success: @escaping (_ result:Any) -> Void, onError: @escaping (_ error:String) -> Void){
        APIHandler.GetRequestWithOptions(url:url, method: .get,isLoader:false,isDisableUI:true, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: JSONSerialization.ReadingOptions.allowFragments)
                success(json)
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Getting Map Image
    class func LocationMapGetService(url:String,success: @escaping (_ result:Any) -> Void, onError: @escaping (_ error:String) -> Void){
        APIHandler.GetRequestWithOptions(url:url, method: .get,isLoader:false,isDisableUI:true, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: JSONSerialization.ReadingOptions.allowFragments)
                success(json)
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func CardUpdateService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.CardUpdate.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func GetAppVersionService(dic:[String:Any],success: @escaping (_ result:AppVersionModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.getAppVersion.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(AppVersionModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func CarInformationService(dic:[String:Any],success: @escaping (_ result:SubOrderDetailsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.subOrderDetails.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(SubOrderDetailsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func AddCustomerCarService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.addCustomerCar.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func DeleteCustomerCarService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.deleteCustomerCar.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
}
