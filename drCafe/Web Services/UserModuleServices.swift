//
//  UserModuleServices.swift
//  drCafeVendor
//
//  Created by RAVI on 27/06/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
@available(iOS 13.0, *)
class UserModuleServices{
    class func signInService(dic:[String:Any],success: @escaping (_ result:SignInModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.signInService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(SignInModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func signUpService(dic:[String:Any],success: @escaping (_ result:SignUpModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.signUpService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(SignUpModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func forgetPasswordService(dic:[String:Any],success: @escaping (_ result:ForgetPasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.forgetPassword.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ForgetPasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func newPasswordService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.newPassword.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func changePasswordService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.changePassword.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func verifyOTPService(dic:[String:Any],success: @escaping (_ result:VerifyOTPModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.verifyOTP.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(VerifyOTPModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func getCountryCodesService(dic:[String:Any],success: @escaping (_ result:CountryCodeModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.countryCode.path(), params: param, method: .get, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(CountryCodeModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func profileService(dic:[String:Any],success: @escaping (_ result:ProfileModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.profile.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ProfileModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func updateProfileService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.updateProfile.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func shareAppService(dic:[String:Any],success: @escaping (_ result:ShareAppModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.shareApp.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ShareAppModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func updateOrderAddressService(dic:[String:Any],success: @escaping (_ result:ResponseModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.updateOrderAddress.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ResponseModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func changeLanguageService(dic:[String:Any],success: @escaping (_ result:SignInModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestWithoutLoader(url: url.changeLanguage.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(SignInModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func logoutService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.logout.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    class func deleteAccountService(dic:[String:Any],success: @escaping (_ result:ChangePasswordModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.deleteAccount.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(ChangePasswordModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
}
