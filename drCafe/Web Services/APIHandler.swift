//
//  APIHandler.swift
//  drCafeVendor
//
//  Created by RAVI on 27/06/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import Alamofire

class APIHandler {
    class func getJWTRequest(url:String,params:Parameters,method:HTTPMethod,success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
          let timeManage = Alamofire.SessionManager.default
          timeManage.session.configuration.timeoutIntervalForRequest = 60
          timeManage.session.configuration.timeoutIntervalForResource = 60
          let headers: HTTPHeaders = ["Authorization":authorization,"Content-Type":"application/json"]
          let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
          let url = URL(string:escapedString)!
          print(url)
          print(params)
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                  if let _ = response.data{
                      success(response.data!)
                  }else{
                      onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                  }
            }
        }else{
          onError(NoNetwork)
        }
    }
    class func RequestWithoutLoader(url:String,params:Parameters,method:HTTPMethod,success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
          let timeManage = Alamofire.SessionManager.default
          timeManage.session.configuration.timeoutIntervalForRequest = 60
          timeManage.session.configuration.timeoutIntervalForResource = 60
          let headers: HTTPHeaders = ["Authorization":getJWT(),"Content-Type":"application/json"]
          let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
          let url = URL(string:escapedString)!
          print(url)
          print(params)
          if method == .get {
              timeManage.request(url,headers: headers).responseJSON { (response) in
                  if let _ = response.response{
                      print(response.response!.statusCode)
                      if response.response!.statusCode == 401 {
                          JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: false, isUIDisable: false) { (result) in
                                  success(result)
                          } onError: { error in
                              onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                          }
                      }else{
                          if let _ = response.data{
                          success(response.data!)
                          }else{
                              onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                          }
                      }
                  }else{
                      onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                  }
              }
          }else{
              timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                  //success(response.data!)
                  if let _ = response.response{
                      print(response.response!.statusCode)
                      if response.response!.statusCode == 401 {
                          JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: false, isUIDisable: false) { (result) in
                                  success(result)
                          } onError: { error in
                              onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                          }
                      }else{
                          if let _ = response.data{
                              success(response.data!)
                          }else{
                              onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                          }
                      }
                  }else{
                      onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                  }
                }
            }
        }else{
            onError(NoNetwork)
        }
    }
    class func RequestOptions(url:String, params:Parameters, method:HTTPMethod, isLoader:Bool, isUIDisable:Bool, success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            if isLoader == true {
                ANLoader.showLoading("", disableUI: isUIDisable)
            }
            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
                         
            let headers: HTTPHeaders = ["Authorization":getJWT(),"Content-Type":"application/json"]
            let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
            let url = URL(string:escapedString)!
            //print(url)
            //print(params)
            if method == .get {
                timeManage.request(url,headers: headers).responseJSON { (response) in
                    if let _ = response.response{
                        print(response.response!.statusCode)
                        if response.response!.statusCode == 401 {
                            JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: isLoader, isUIDisable: isUIDisable) { (result) in
                                    success(result)
                            } onError: { error in
                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                            }
                        }else{
                            if isLoader == true {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    ANLoader.hide()
                                }
                            }
                            if let _ = response.data{
                                success(response.data!)
                            }else{
                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                            }
                        }
                    }else{
                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                    }
                }
            }else{
                timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                    if let _ = response.response{
                        print(response.response!.statusCode)
                        if response.response!.statusCode == 401 {
                            JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: isLoader, isUIDisable: isUIDisable) { (result) in
                                    success(result)
                            } onError: { error in
                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                            }
                        }else{
                            if isLoader == true {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    ANLoader.hide()
                                }
                            }
                            if let _ = response.data{
                                success(response.data!)
                            }else{
                                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                            }
                        }
                    }else{
                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                    }
                }
            }
        }else{
            if isLoader == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
            onError(NoNetwork)
        }
    }
    class func Request(url:String,params:Parameters,method:HTTPMethod,success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
      if(Connectivity.isConnectedToInternet() == true){
        ANLoader.showLoading("", disableUI: true)
        let timeManage = Alamofire.SessionManager.default
          timeManage.session.configuration.timeoutIntervalForRequest = 60
          timeManage.session.configuration.timeoutIntervalForResource = 60
   
        let headers: HTTPHeaders = ["Authorization":getJWT(),"Content-Type":"application/json"]
        let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
        let url = URL(string:escapedString)!
        print(url)
        print(params)
        if method == .get {
            timeManage.request(url,headers: headers).responseJSON { (response) in
                if let _ = response.response{
                    print(response.response!.statusCode)
                    if response.response!.statusCode == 401 {
                        JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: true, isUIDisable: true) { (result) in
                                success(result)
                        } onError: { error in
                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                        }
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            ANLoader.hide()
                        }
                        if let _ = response.data{
                            success(response.data!)
                        }else{
                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                        }
                    }
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }
        }else{
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                if let _ = response.response{
                    print(response.response!.statusCode)
                    if response.response!.statusCode == 401 {
                        JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: true, isUIDisable: true) { (result) in
                                success(result)
                        } onError: { error in
                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                        }
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            ANLoader.hide()
                        }
                        if let _ = response.data{
                            success(response.data!)
                        }else{
                            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                        }
                    }
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }
        }
      }else{
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        onError(NoNetwork)
      }
    }
    class func GetRequestWithOptions(url:String, method:HTTPMethod, isLoader:Bool, isDisableUI:Bool, success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if isLoader == true {
            ANLoader.showLoading("", disableUI: isDisableUI)
        }
        if(Connectivity.isConnectedToInternet() == true){
            
            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
            //print(url)
            timeManage.request(url, method: method).responseJSON { (response) in
                //success(response.data!)
                if isLoader == true {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                }
                if let _ = response.data{
                    success(response.data!)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }
        }else{
            if isLoader == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
            onError(NoNetwork)
        }
    }
}
