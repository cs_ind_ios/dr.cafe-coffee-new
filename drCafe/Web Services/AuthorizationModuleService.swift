//
//  AuthorizationModuleService.swift
//  drCafe
//
//  Created by Creative Solutions on 10/26/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation
class AuthorizationModuleService{
    class func getJWTService(dic:[String:Any],success: @escaping (_ result:String) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.getJWTRequest(url: url.getOAuthService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: [.allowFragments]) as! String
                do{
                    let result1 = try JSONDecoder().decode(String.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    
}
