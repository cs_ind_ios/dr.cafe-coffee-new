//
//  CateringOrderCreateVC.swift
//  drCafe
//
//  Created by Devbox on 09/12/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class CateringOrderCreateVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var messageTV: UITextView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var cityDropDownBtn: UIButton!
    @IBOutlet weak var countryCodeDropDownBtn: UIButton!
    @IBOutlet weak var countryCodeDropDownImg: UIImageView!
    
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailNameLbl: UILabel!
    @IBOutlet weak var mobileNumNameLbl: UILabel!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var messageNameLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var BGView: UIView!
    
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    var countryCodesArray = [CountryCodeDetailsModel]()
    
    var cityDropDownArray = ["Riyadh", "Jeddah", "Eastern region", "Qaseem"]
    var city = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mobileNumTF.delegate = self
        messageTV.delegate = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Catering", value: "", table: nil))!
        nameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Name", value: "", table: nil))!
        emailNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email Name", value: "", table: nil))!
        mobileNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!
        cityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "City Name", value: "", table: nil))!
        messageNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Message", value: "", table: nil))!
        submitBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Submit", value: "", table: nil))!, for: .normal)
        
        cityDropDownArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Riyadh", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Jeddah", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Eastern region", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Qaseem", value: "", table: nil))!]
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if SaveAddressClass.CountryCodesListArray.count > 0{
            let saudiCC = SaveAddressClass.CountryCodesListArray.filter({$0.MobileCode == 966})
            if saudiCC.count > 0{
                self.countryCodeLbl.text = "+966"
                self.selectCountryCode = 966
                self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
            }else{
                self.countryCodeLbl.text = "+966"
                self.selectCountryCode = 966
                self.selectCCMobileNumDigits = 9
                self.selectCCMinLength = 9
                self.selectCCMaxLength = 9
            }
            if SaveAddressClass.CountryCodesListArray.count == 1{
                countryCodeDropDownBtn.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "Arabic"{
                    countryCodeDropDownImg.isHidden = true
                }
            }else{
                countryCodeDropDownBtn.isHidden = false
                if AppDelegate.getDelegate().appLanguage == "Arabic"{
                    countryCodeDropDownImg.isHidden = false
                }
            }
        }else{
            self.getCountryCodesListService()
        }
        
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        submitBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Country Codes Get Service
    func getCountryCodesListService(){
        let dic:[String:Any] = [:]
        UserModuleServices.getCountryCodesService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.countryCodesArray = data.Data!
                SaveAddressClass.CountryCodesListArray = data.Data!
                let saudiCC = data.Data!.filter({$0.MobileCode == 966})
                if saudiCC.count > 0{
                    self.countryCodeLbl.text = "+966"
                    self.selectCountryCode = 966
                    self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
                }else{
                    self.countryCodeLbl.text = "+966"
                    self.selectCountryCode = 966
                    self.selectCCMobileNumDigits = 9
                    self.selectCCMinLength = 9
                    self.selectCCMaxLength = 9
                }
                if data.Data!.count == 1{
                    self.countryCodeDropDownBtn.isHidden = true
                    if AppDelegate.getDelegate().appLanguage == "Arabic"{
                        self.countryCodeDropDownImg.isHidden = true
                    }
                }else{
                    self.countryCodeDropDownBtn.isHidden = false
                    if AppDelegate.getDelegate().appLanguage == "Arabic"{
                        self.countryCodeDropDownImg.isHidden = false
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: City DropDown
    @IBAction func cityDropDownBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = cityDropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 10 , y: -18, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX , y: -18, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = cityDropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Country Drop Down
    @IBAction func countryCodeDropDownBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = SaveAddressClass.CountryCodesListArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.center.x+11 , y: -15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: countryCodeDropDownImg.frame.midX - 9 , y: -20, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 1
        obj.codeDropDownArray = SaveAddressClass.CountryCodesListArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Submit Button
    @IBAction func submitBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var yourMessage = ""
        var userId = 0
        if messageTV.text != nil || (messageTV.text != ""){
            yourMessage = messageTV.text!
        }
        if isUserLogIn() == true{
            userId = Int(UserDef.getUserId())!
        }
        let dic = ["EmailId": "\(emailTF.text!)",
                   "CountryCode":"\(selectCountryCode!)",
                   "PhoneNo": "\(mobileNumTF.text!)",
                   "City":"\(city)",
                   "UserId": userId,
                   "Message": yourMessage,
                   "RequestBy": 2] as [String : Any]
        OrderModuleServices.CateringOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToast(on: self, message: data.MessageEn)
                }else{
                    Alert.showToast(on: self, message: data.MessageAr)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if emailTF.text == nil || (emailTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isValidEmail(email: emailTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EmailValidation", value: "", table: nil))!)
                return false
            }
        }
        if mobileNumTF.text == nil || (mobileNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileNumTF.text!, minDigits: selectCCMinLength, maxDigits: selectCCMaxLength){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        if cityTF.text == nil || (cityTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CityNameAlert", value: "", table: nil))!)
            return false
        }
        return true
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
@available(iOS 13.0, *)
extension CateringOrderCreateVC : UITextFieldDelegate{
    //MARK: TextField Delegate Methods
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileNumTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: selectCCMaxLength)
        }
        return true
    }
}
//MARK: UITextView Delegate Methods
@available(iOS 13.0, *)
extension CateringOrderCreateVC:UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textView.textInputMode?.primaryLanguage == "emoji") || text.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension CateringOrderCreateVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if whereType == 1{
            countryCodeLbl.text = "+\(ID)"
            selectCountryCode = Int(ID)
            let selectDic = SaveAddressClass.CountryCodesListArray.filter({$0.CountryNameEn == name})
            self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
            mobileNumTF.text = ""
        }else{
            if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Riyadh", value: "", table: nil))!{
                city = "Riyadh"
            }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Jeddah", value: "", table: nil))!{
                city = "Jeddah"
            }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Eastern region", value: "", table: nil))!{
                city = "Eastern region"
            }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Qaseem", value: "", table: nil))!{
                city = "Qaseem"
            }else{
                city = name
            }
            cityTF.text = name
        }
    }
}
