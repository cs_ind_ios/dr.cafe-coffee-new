//
//  CateringVC.swift
//  drCafe
//
//  Created by Devbox on 25/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class CateringVC: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var descOneLbl: UILabel!
    @IBOutlet weak var descTwoNameLbl: UILabel!
    @IBOutlet weak var contactInfoLbl: UILabel!
    @IBOutlet weak var rocketBaristaNameLbl: UILabel!
    @IBOutlet weak var oneBaristaNameLbl: UILabel!
    @IBOutlet weak var patioNameLbl: UILabel!
    @IBOutlet weak var ventageCartNameLbl: UILabel!
    @IBOutlet weak var mobileCoffeeTruckNameLbl: UILabel!
    @IBOutlet weak var containerBoxNameLbl: UILabel!
    @IBOutlet weak var orderNowBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Catering", value: "", table: nil))!
        descOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "cateringDescOne", value: "", table: nil))!
        descTwoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "cateringDescTwo", value: "", table: nil))!
        contactInfoLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "cateringInfo", value: "", table: nil))!
        rocketBaristaNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rocket Barista", value: "", table: nil))!
        oneBaristaNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "One Barista Black", value: "", table: nil))!
        patioNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Patio", value: "", table: nil))!
        ventageCartNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ventage Cart", value: "", table: nil))!
        mobileCoffeeTruckNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Coffee Truck", value: "", table: nil))!
        containerBoxNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Container Box", value: "", table: nil))!
        orderNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Now", value: "", table: nil))!, for: .normal)
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        orderNowBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Order Now Button action
    @IBAction func orderNowBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CateringOrderCreateVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CateringOrderCreateVC") as! CateringOrderCreateVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Contact Button Action
    @IBAction func contactBtn_Tapped(_ sender: Any) {
        let mobile = "+966593007070"
        if let url = URL(string: "tel://\(mobile)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
