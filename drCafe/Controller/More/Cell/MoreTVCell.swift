//
//  MoreTVCell.swift
//  drCafe
//
//  Created by Devbox on 30/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class MoreTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var nextArowBtn: UIButton!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    @IBOutlet weak var imgTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
