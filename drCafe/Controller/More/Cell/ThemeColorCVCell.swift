//
//  ThemeColorCVCell.swift
//  drCafe
//
//  Created by Devbox on 14/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class ThemeColorCVCell: UICollectionViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var themeColorImg: UIImageView!
    @IBOutlet weak var themeSelectBtnBGView: CustomView!
    @IBOutlet weak var themeSelectBtn: UIButton!
    @IBOutlet weak var themeColorNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
