//
//  ContactUsTVCell.swift
//  drCafe
//
//  Created by Devbox on 12/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class ContactUsTVCell: UITableViewCell {

    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var contactNameLbl: UILabel!
    @IBOutlet weak var contactNumLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
