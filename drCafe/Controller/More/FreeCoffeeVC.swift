//
//  FreeCoffeeVC.swift
//  drCafe
//
//  Created by Mac2 on 29/03/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class FreeCoffeeVC: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var shareCodeLbl: UILabel!
    @IBOutlet weak var referNowBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var termsAndConditionsBtn: UIButton!
    @IBOutlet weak var termsAndConditionLineLbl: UILabel!
    
    var shareCode = ""
    var isLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        if isUserLogIn() == true{
            getDetailsService()
        }else{
            self.shareCodeLbl.text = ""
        }
        
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.referNowBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            titleLbl.textColor = .white
            descLbl.textColor = .white
            termsAndConditionsBtn.setTitleColor(.white, for: .normal)
            termsAndConditionLineLbl.backgroundColor = .white
        } else {
            // User Interface is Light
            titleLbl.textColor = .black
            descLbl.textColor = .darkGray
            termsAndConditionsBtn.setTitleColor(#colorLiteral(red: 0, green: 0.5764552355, blue: 0.7616670728, alpha: 0.8), for: .normal)
            termsAndConditionLineLbl.backgroundColor = #colorLiteral(red: 0, green: 0.5764552355, blue: 0.7616670728, alpha: 0.8)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isLogin == true{
            LoginVC.instance.whereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Copy Button Action
    @IBAction func copyBtn_Tapped(_ sender: Any) {
        UIPasteboard.general.string = shareCodeLbl.text!
        Alert.showToastDown(on: self, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Referal code copied", value: "", table: nil))!)
        //print(UIPasteboard.general.string!)
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async() {
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let shareUrl = URL.init(string: urlStr as String)
            var desc = ""
            if AppDelegate.getDelegate().appLanguage == "English"{
                desc = "I'm really enjoying a variety of Coffee at dr. Cafe Think you might enjoy it too.\n\nUse my Referal Code - \(self.shareCode) during the Registration & grab a free coffee on your first purchase.\n\ndownload the app at \(shareUrl!)"
            }else{
                desc = "أنا أستمتع حقا بمجموعة متنوعة من القهوة في د.كيف كافيه، وأعتقد أنك ستستمتع بها أيضا.\n\n استخدم رمز الدعوة الخاص بي - \(self.shareCode) خلال تسجيلك في التطبيق وأحصل على قهوة مجانية في أول عملية شراء لك الان!.\n\nقم بتحميل التطبيق من الرابط التالي \(shareUrl!)"
            }
            let shareAll = [desc] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    //MARK: Refer Now Button Action
    @IBAction func referNowBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async() {
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let shareUrl = URL.init(string: urlStr as String)
            
            var desc = ""
            if AppDelegate.getDelegate().appLanguage == "English"{
                desc = "I'm really enjoying a variety of Coffee at dr. Cafe Think you might enjoy it too.\n\nUse my Referal Code - \(self.shareCode) during the Registration & grab a free coffee on your first purchase.\n\ndownload the app at \(shareUrl!)"
            }else{
                desc = "أنا أستمتع حقا بمجموعة متنوعة من القهوة في د.كيف كافيه، وأعتقد أنك ستستمتع بها أيضا.\n\n استخدم رمز الدعوة الخاص بي - \(self.shareCode) خلال تسجيلك في التطبيق وأحصل على قهوة مجانية في أول عملية شراء لك الان!.\n\nقم بتحميل التطبيق من الرابط التالي \(shareUrl!)"
            }
            let shareAll = [desc] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    //MARK: Terms And Conditions Button Action
    @IBAction func termsAndConditionsBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Referral Terms & Conditions", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppReferalProgram"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppReferalProgram"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    func getDetailsService(){
        let dic:[String:Any] = ["Userid": UserDef.getUserId(), "RequestBy":2]
        UserModuleServices.shareAppService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.shareCodeLbl.text = "#\(data.Data!.ReferalCode)"
                self.shareCode = "\(data.Data!.ReferalCode)"
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
