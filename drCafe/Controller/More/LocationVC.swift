//
//  LocationVC.swift
//  drCafe
//
//  Created by Devbox on 30/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class LocationVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dropDownArray = [String]()
    
    let detailsArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Distance", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Radius", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location Display", value: "", table: nil))!]
    var distance:String!
    var radius:String!
    var location:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location", value: "", table: nil))!
        distance = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Kilometer", value: "", table: nil))!
        radius = "50 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)"
        if UserDefaults.standard.object(forKey: "mapType") != nil {
            let mapType = UserDefaults.standard.object(forKey: "mapType") as! [String:Any]
            location = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "\(mapType["MapType"]!)", value: "", table: nil))!
        }else{
            location = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "List", value: "", table: nil))!
        }
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            navigationBarTitleLbl.textColor = .white
            tableView.backgroundColor = .black
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
        } else {
            // User Interface is Light
            self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            navigationBarTitleLbl.textColor = .black
            tableView.backgroundColor = .white
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension LocationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, for: indexPath) as! MoreTVCell
        cell.nameLbl.text = detailsArray[indexPath.row]
        if indexPath.row == 0{
            cell.detailsLbl.text = distance!
        }else if indexPath.row == 1{
            cell.detailsLbl.text = radius!
        }else{
            cell.detailsLbl.text = location!
        }
        
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            cell.contentView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            cell.nameLbl.textColor = .white
            cell.detailsLbl.textColor = .white
        } else {
            // User Interface is Light
            cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.nameLbl.textColor = .black
            cell.detailsLbl.textColor = .darkGray
        }
        
        cell.nextArowBtn.isHidden = false
        cell.nextArowBtn.tag = indexPath.row
        cell.nextArowBtn.addTarget(self, action: #selector(dropDownBtn_Tapped(_:)), for: .touchUpInside)
        cell.selectBtn.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(detailsArray[indexPath.row])
    }
    //MARK: Drop Down Selection
    @objc func dropDownBtn_Tapped(_ sender: UIButton) {
        if sender.tag == 0{
            dropDownArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Kilometer", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mile", value: "", table: nil))!]
        }else if sender.tag == 1{
            dropDownArray = ["5 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)","10 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)","20 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)","40 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)","50 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "KM", value: "", table: nil))!)"]
        }else{
            dropDownArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Map", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "List", value: "", table: nil))!]
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 150, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        pVC?.sourceRect = CGRect(x: sender.center.x , y: 0, width: 0, height: sender.frame.size.height)
        obj.whereObj = sender.tag + 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
// MARK: Drop Down Delegate Methods
@available(iOS 13.0, *)
extension LocationVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if whereType == 2{
            distance = name
        }else if whereType == 3{
            radius = name
        }else{
            location = name
            var dic:[String:Any]! = ["MapType":"List"]
            if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Map", value: "", table: nil))!{
                dic = ["MapType":"Map"]
            }else{
                dic = ["MapType":"List"]
            }
            UserDefaults.standard.set(dic, forKey:"mapType")
            UserDefaults.standard.synchronize()
        }
        tableView.reloadData()
    }
}
