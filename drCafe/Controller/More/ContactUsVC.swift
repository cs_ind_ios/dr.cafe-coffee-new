//
//  ContactUsVC.swift
//  drCafe
//
//  Created by Devbox on 03/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ContactUsVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let contactNamesArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "General Inquiry", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Real Estate", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Catering and Events", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Recruitment", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Marketing & Promotions", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quality Assurance", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Franchising", value: "", table: nil))!]
    let contactNumbersArray = ["800-122-8222","+966593915888","+966593007070","+966593004040","+966593827657","+966595017691","+966581083356"]
    let contactImgsArray = ["General Inquiry","Real Estate","Catering and Events","Recruitment","Marketing & Promotions","Quality Assurance","Franchising"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Us", value: "", table: nil))!
        
        self.BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension ContactUsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactNamesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ContactUsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ContactUsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ContactUsTVCell", value: "", table: nil))!, for: indexPath) as! ContactUsTVCell
        cell.contactNameLbl.text = contactNamesArray[indexPath.row]
        cell.contactNumLbl.text = contactNumbersArray[indexPath.row]
        cell.contactImg.image = UIImage(named: contactImgsArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mobile = "\(contactNumbersArray[indexPath.row])"
        if let url = URL(string: "tel://\(mobile)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
