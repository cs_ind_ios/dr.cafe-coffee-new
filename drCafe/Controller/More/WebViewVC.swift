//
//  WebViewVC.swift
//  drCafe
//
//  Created by Devbox on 18/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class WebViewVC: UIViewController, UIWebViewDelegate {

    //MARK: IBOutlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    var titleStr = String()
    var url = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        navigationBarTitleLbl.text = titleStr
        webView.delegate = self
        if(Connectivity.isConnectedToInternet()){
           webView.loadRequest(URLRequest(url: URL(string: url)!))
        }else{
            Alert.showToast(on: self, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NoNetwork", value: "", table: nil))!)
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.dismiss(animated: false, completion: nil)
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          ANLoader.hide()
        }
        self.dismiss(animated: true, completion: nil)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
         ANLoader.showLoading("", disableUI: false)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          ANLoader.hide()
        }
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
    }
}

