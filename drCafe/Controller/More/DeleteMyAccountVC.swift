//
//  DeleteMyAccountVC.swift
//  drCafe
//
//  Created by Mac2 on 03/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class DeleteMyAccountVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var tableviewHeight: NSLayoutConstraint!
    @IBOutlet weak var deleteMyAccountBtn: UIButton!
    
    var warningDetailsArray = [""]
    var isDelete = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self
        tableview.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete My Account", value: "", table: nil))!
        deleteMyAccountBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete My Account", value: "", table: nil))!, for: .normal)
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Deleting your account will:", value: "", table: nil))!
        warningDetailsArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete your account from dr.Cafe Coffee", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Erase your order history", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete your payment history", value: "", table: nil))!]
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isDelete == true{
            ProfileVC.instance.backObj = 1
            self.navigationController?.popViewController(animated: false)
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        ProfileVC.instance.backObj = 0
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func deleteMyAccountBtn_Tapped(_ sender: Any) {
        let alert = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Do you want to delete this account", value: "", table: nil))!, preferredStyle: .alert)
        let action = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
        }
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            var language = "En"
            if AppDelegate.getDelegate().appLanguage == "English"{
                language = "En"
            }else{
                language = "Ar"
            }
//            let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//            let deviceInfo:[String:Any] = [
//                "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//                "applang":language,
//                "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//                "SDKversion":"",
//                "Modelname":"\(UIDevice.current.name)",
//                "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//                "AppVersion":"IOS(\(appVersion))",
//                "TimeZone":"\(TimeZone.current.abbreviation()!)",
//                "TimeZoneRegion":"\(TimeZone.current.identifier)"]
            let dic = ["UserId": UserDef.getUserId(),
                       "DeviceInfo": "\(getDeviceInfo())", "RequestBy":2] as [String : Any]
            UserModuleServices.deleteAccountService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = "Order"
                    }
                    self.isDelete = true
                    removeFromUserDefaultForKey(key:"userDic")
                    UserDef.removeFromUserDefaultForKey(key: "UserId")
                    SaveAddressClass.getDCSmileHistoryArray.removeAll()
                    self.tabBarController?.selectedIndex = 0
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
        alert.addAction(action)
        alert.addAction(action1)
        self.present(alert, animated: true, completion: nil)
    }
}
extension DeleteMyAccountVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return warningDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DeleteMyAccountVCCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DeleteMyAccountVCCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DeleteMyAccountVCCell", value: "", table: nil))!, for: indexPath) as! DeleteMyAccountVCCell
        cell.nameLbl.text = warningDetailsArray[indexPath.row]
        tableviewHeight.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
    }
}
