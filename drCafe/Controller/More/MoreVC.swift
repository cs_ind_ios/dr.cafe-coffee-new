//
//  MoreVC.swift
//  drCafe
//
//  Created by Devbox on 30/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@available(iOS 13.0, *)
class MoreVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var myOrderTitleLbl: UILabel!
    @IBOutlet weak var rateAppTitleLbl: UILabel!
    @IBOutlet weak var contactTitleLbl: UILabel!
    @IBOutlet weak var shareAppTitleLbl: UILabel!
    
    @IBOutlet weak var settingsTitleLbl: UILabel!
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var settingsTVHeight: NSLayoutConstraint!
    @IBOutlet weak var settingsBGView: UIView!
    @IBOutlet weak var DCBGView: UIView!
    
    @IBOutlet weak var drCafeCoffeeInfoTitleLbl: UILabel!
    @IBOutlet weak var drCafeCoffeeInfoTableView: UITableView!
    @IBOutlet weak var drCafeCoffeeInfoTVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var logOutBtn: UIButton!
    @IBOutlet weak var myOrderBtn: UIButton!
    @IBOutlet weak var rateAppBtn: UIButton!
    @IBOutlet weak var shareAppBtn: UIButton!
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fourSquareBtn: UIButton!
    @IBOutlet weak var snapChatBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var allRightsReservedNameLbl: UILabel!
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    @IBOutlet weak var termsOfUseBtn: UIButton!
    @IBOutlet weak var termsOfSaleBtn: UIButton!
    @IBOutlet weak var subscriptionTCBtn: UIButton!
    @IBOutlet weak var FAQBtn: UIButton!
    @IBOutlet weak var rewardsTCBtn: UIButton!
    @IBOutlet weak var bottomDetailsView: UIView!
    
    var settingsArray: [MoreModel] = []
    var drCafeInfoArray: [MoreModel] = []
    
    var language = "عربي"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        drCafeCoffeeInfoTableView.delegate = self
        drCafeCoffeeInfoTableView.dataSource = self
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "More", value: "", table: nil))!
        rateAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        rateAppTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        shareAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shareAppTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contactTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contactTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        myOrderTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myOrderTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "عربي"
        }else{
            language = "English"
        }
        self.settingsArray.removeAll()
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Profile", value: "", table: nil))!, details: "", image: "More_Profile"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Language", value: "", table: nil))!, details: language, image: "More_Language"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Password", value: "", table: nil))!, details: "", image: "More_Password"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Address", value: "", table: nil))!, details: "", image: "More_Address"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Cars", value: "", table: nil))!, details: "", image: "Manage Cars"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Cards", value: "", table: nil))!, details: "", image: "More_Manage Cards"))
        settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Theme", value: "", table: nil))!, details: "", image: "More_Themes"))
        self.drCafeInfoArray.removeAll()
        drCafeInfoArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!, details: "", image: "More_About"))
        drCafeInfoArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "News Room", value: "", table: nil))!, details: "", image: "More_News"))
        drCafeInfoArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FAQ", value: "", table: nil))!, details: "", image: "More_FAQ"))
        drCafeInfoArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Social Media Links", value: "", table: nil))!, details: "", image: ""))
        if !hasLocationPermission() {
            settingsArray.insert(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Allow Location", value: "", table: nil))!, details: "", image: "More_Allow Location"), at: 6)
        }
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = false
            self.settingsTableView.reloadData()
            self.drCafeCoffeeInfoTableView.reloadData()
        }
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        
        if isUserLogIn() == true{
            logOutBtn.isHidden = false
        }else{
            logOutBtn.isHidden = true
        }
        self.DCBGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.settingsBGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.bottomDetailsView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            settingsTitleLbl.textColor = .white
            drCafeCoffeeInfoTitleLbl.textColor = .white
            settingsTableView.backgroundColor = .white
            drCafeCoffeeInfoTableView.backgroundColor = .white
            termsOfUseBtn.setTitleColor(.white, for: .normal)
            termsOfSaleBtn.setTitleColor(.white, for: .normal)
            privacyPolicyBtn.setTitleColor(.white, for: .normal)
            versionLbl.textColor = .white
            allRightsReservedNameLbl.textColor = .white
        } else {
            // User Interface is Light
            settingsTitleLbl.textColor = .black
            drCafeCoffeeInfoTitleLbl.textColor = .black
            termsOfUseBtn.setTitleColor(.darkGray, for: .normal)
            termsOfSaleBtn.setTitleColor(.darkGray, for: .normal)
            privacyPolicyBtn.setTitleColor(.darkGray, for: .normal)
            versionLbl.textColor = .darkGray
            allRightsReservedNameLbl.textColor = .darkGray
        }
        
        versionLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Version", value: "", table: nil))!) \(AppUtility.methodForApplicationVersion().split(separator: "(")[0])"
        allRightsReservedNameLbl.text = "© \(DateConverts.convertDateToString(date: Date(), dateformatType: .year)) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE COFFEE, All Rights Reserved", value: "", table: nil))!)"
        privacyPolicyBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "PrivacyPolicy", value: "", table: nil))!, for: .normal)
        termsOfUseBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms of Use", value: "", table: nil))!, for: .normal)
        termsOfSaleBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms of Sale", value: "", table: nil))!, for: .normal)
        subscriptionTCBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription T&C", value: "", table: nil))!, for: .normal)
        FAQBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "FAQ", value: "", table: nil))!, for: .normal)
        rewardsTCBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards T&C", value: "", table: nil))!, for: .normal)
        
        tabBarColor()
    }
    //MARK: TabBar Theme Color
    func tabBarColor() {
        if let tabItems = self.tabBarController?.tabBar.items {
          //let tabItem = tabItems[2]
          if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
            //tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
            tabItems[2].image = UIImage(named: "Cart Tab")
            tabItems[2].selectedImage = UIImage(named: "Cart Select")
            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
          }else{
            //tabItem.badgeValue = nil
            tabItems[2].image = UIImage(named: "Tab Order")
            tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
          }
        }
        let appearance = UITabBarAppearance()
        //appearance.backgroundColor = .white
        //appearance.shadowImage = UIImage()
        // appearance.shadowColor = .white
        let color = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        appearance.stackedLayoutAppearance.normal.iconColor = color.withAlphaComponent(0.7)
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color.withAlphaComponent(0.7)]
        //appearance.stackedLayoutAppearance.normal.badgeBackgroundColor = .yellow
        appearance.stackedLayoutAppearance.selected.iconColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")]
        // set padding between tabbar item title and image
        //appearance.stackedLayoutAppearance.selected.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 4)
        //appearance.stackedLayoutAppearance.normal.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 4)
        self.tabBarController?.tabBar.standardAppearance = appearance
      }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Logout Button Action
    @IBAction func logOutBtn_Tapped(_ sender: Any) {
        let alert = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Do you want to logout from this account", value: "", table: nil))!, preferredStyle: .alert)
        let action = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
        }
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            let dic = ["UserId": UserDef.getUserId(),
             "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2] as [String : Any]
            UserModuleServices.logoutService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = "Order"
                    }
                    removeFromUserDefaultForKey(key:"userDic")
                    UserDef.removeFromUserDefaultForKey(key: "UserId")
                    SaveAddressClass.getDCSmileHistoryArray.removeAll()
                    self.tabBarController?.selectedIndex = 0
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
        alert.addAction(action)
        alert.addAction(action1)
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: RateApp Button Action
    @IBAction func myOrderBtn_Tapped(_ sender: Any) {
        myOrderTitleLbl.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        myOrderTitleLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        myOrderTitleLbl.layer.cornerRadius = 15
        shareAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shareAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        contactTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contactTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        rateAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        rateAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = NewOrderHistoryVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewOrderHistoryVC") as! NewOrderHistoryVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.whereObj = 11
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: RateApp Button Action
    @IBAction func rateAppBtn_Tapped(_ sender: Any) {
        rateAppTitleLbl.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        rateAppTitleLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        rateAppTitleLbl.layer.cornerRadius = 15
        shareAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shareAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        contactTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contactTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        myOrderTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myOrderTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/" + "dr.cafe-coffee/id1083344883?mt=8") else {
            return
        }
        let application = UIApplication.shared
        application.open(url as URL)
    }
    //MARK: Share App Button Action
    @IBAction func shareAppBtn_Tapped(_ sender: Any) {
        shareAppTitleLbl.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        shareAppTitleLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        shareAppTitleLbl.layer.cornerRadius = 15
        rateAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        rateAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        contactTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        contactTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let shareUrl = URL.init(string: urlStr as String)
        let Name = ""
        let Des = "dr.CAFE® COFFEE launches the newest and latest mobile ordering & pickup application in the coffee industry allowing you to order your favorite beverage and/or food and ready to deliver or pick up or enjoy at the store."
        let shareAll = [Name,Des,"\n\nDownload the app at ",shareUrl as Any] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    //MARK: Contacts Button Action
    @IBAction func contactBtn_Tapped(_ sender: Any) {
        contactTitleLbl.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        contactTitleLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        contactTitleLbl.layer.cornerRadius = 15
        rateAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        rateAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        shareAppTitleLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shareAppTitleLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 0.3019607843, alpha: 1)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ContactUsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Facebook Button Action
    @IBAction func facebookBtn_Tapped(_ sender: Any) {
        let Username = "drcafeksa"
        let appURL = NSURL(string: "fb://profile/\(Username)")!
        let webURL = NSURL(string: "https://fb.com/\(Username)")!
        let application = UIApplication.shared
        if application.canOpenURL(appURL as URL) {
            application.open(appURL as URL)
        } else {
            application.open(webURL as URL)
        }
    }
    //MARK: Twitter Button Action
    @IBAction func twitterBtn_Tapped(_ sender: Any) {
        let screenName =  "drcafeksa"
        let appURL = NSURL(string: "twitter://user?screen_name=\(screenName)")!
        let webURL = NSURL(string: "https://twitter.com/\(screenName)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL as URL) {
             application.open(appURL as URL)
        } else {
             application.open(webURL as URL)
        }
    }
    //MARK: FourSquare Button Action
    @IBAction func fourSquareBtn_Tapped(_ sender: Any) {
        let username =  "44581694"
        let webURL = NSURL(string: "https://foursquare.com/user/\(username)")!
        let application = UIApplication.shared
        application.open(webURL as URL)
    }
    //MARK: Snap Chat Button Action
    @IBAction func snapChatBtn_Tapped(_ sender: Any) {
        let username = "drcafeksa"
        let appURL = URL(string: "snapchat://add/\(username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            let webURL = URL(string: "https://www.snapchat.com/add/\(username)")!
            application.open(webURL)
        }
    }
    //MARK: Instagram Button Action
    @IBAction func instaBtn_Tapped(_ sender: Any) {
        let Username =  "drcafeksa"
        let appURL = URL(string: "instagram://user?username=\(Username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }
    }
    @IBAction func termsOfUseBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms of Use", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppTermsofUse"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppTermsofUse"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func termsOfSaleBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms of Sale", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppTermsofSale"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppTermsofSale"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func privacyPolicyBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PrivacyPolicy", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppPrivacyPolicy"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppPrivacyPolicy"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func subscriptionTCBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Terms & Conditions", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppSubscriptionTermsandCondition"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppSubscriptionTermsandCondition"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func FAQBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FAQ", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppFAQuestions"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppFAQuestions"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func rewardsTCBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Terms & Conditions", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppRewards"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppRewards"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension MoreVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == settingsTableView{
            return settingsArray.count
        }else{
            return drCafeInfoArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!)
        //Settings tableView
        if tableView == settingsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, for: indexPath) as! MoreTVCell
            cell.nameLbl.text = settingsArray[indexPath.row].name
            cell.detailsLbl.text = settingsArray[indexPath.row].details
            cell.img.image = UIImage(named: settingsArray[indexPath.row].image)
            cell.nextArowBtn.isHidden = false
            cell.nextArowBtn.tag = indexPath.row
            cell.nextArowBtn.addTarget(self, action: #selector(settingsNextArrowBtn_Tapped(_:)), for: .touchUpInside)
            cell.selectBtn.isHidden = true
            if settingsArray.count-1 == indexPath.row{
                cell.lineLbl.isHidden = true
            }else{
                cell.lineLbl.isHidden = false
            }
            cell.imgWidth.constant = 25
            cell.imgTrailing.constant = 15
            cell.img.isHidden = false
            settingsTVHeight.constant = CGFloat(60 * settingsArray.count)
            settingsTableView.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }else{
            //dr.Cafe Info TableView
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, for: indexPath) as! MoreTVCell
            cell.nameLbl.text = drCafeInfoArray[indexPath.row].name
            cell.detailsLbl.text = drCafeInfoArray[indexPath.row].details
            cell.img.image = UIImage(named: drCafeInfoArray[indexPath.row].image)
            cell.nextArowBtn.isHidden = false
            cell.selectBtn.isHidden = true
            if drCafeInfoArray.count-1 == indexPath.row{
                cell.lineLbl.isHidden = true
                cell.nextArowBtn.setImage(nil, for: .normal)
                cell.imgWidth.constant = 0
                cell.imgTrailing.constant = 0
                cell.img.isHidden = true
            }else{
                cell.imgWidth.constant = 25
                cell.imgTrailing.constant = 15
                cell.img.isHidden = false
                cell.lineLbl.isHidden = false
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.nextArowBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                }else{
                    cell.nextArowBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                }
            }
            cell.nextArowBtn.tag = indexPath.row
            cell.nextArowBtn.addTarget(self, action: #selector(drCafeInfoNextArrowBtn_Tapped(_:)), for: .touchUpInside)
            drCafeCoffeeInfoTVHeight.constant = CGFloat((60 * drCafeInfoArray.count) - 20)
            drCafeCoffeeInfoTitleLbl.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }
    }
    //MARK: Settings Next Arrow Buttonn Selection
    @objc func settingsNextArrowBtn_Tapped(_ sender: UIButton) {
//        if sender.tag == 0{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            var obj = LocationVC()
//            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
//            self.navigationController?.pushViewController(obj, animated: true)
//        }else
        if sender.tag == 0{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ProfileVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                obj.whereObj = 12
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if sender.tag == 1{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ChangeLanguageVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ChangeLanguageVC") as! ChangeLanguageVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if sender.tag == 2{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ChangePasswordVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                obj.whereObj = 13
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if sender.tag == 3{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ManageAddressVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
                obj.whereObj = 10
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                obj.whereObj = 14
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if sender.tag == 4{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = CarDetailsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarDetailsVC") as! CarDetailsVC
                obj.isMore = true
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                obj.whereObj = 21
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if sender.tag == 5{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ManageCardsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if settingsArray[sender.tag].name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Allow Location", value: "", table: nil))!{
            let vc = LocationAutocompleteVC(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LocationAutocompleteVC", value: "", table: nil))!, bundle: nil)
            vc.locationAutocompleteVCDelegate = self
            vc.whereObj = 3
            self.navigationController?.pushViewController(vc, animated: true)
        }else if settingsArray[sender.tag].name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Theme", value: "", table: nil))!{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ThemeColorVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ThemeColorVC") as! ThemeColorVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    //MARK: dr.Cafe Info Next Arrow Buttonn Selection
    @objc func drCafeInfoNextArrowBtn_Tapped(_ sender: UIButton) {
        if sender.tag == 0{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                Obj.url = "https://drcafe.com/en-sa/guest/about-us"
            }else{
                Obj.url = "https://drcafe.com/ar-sa/guest/about-us"
            }
            Obj.modalPresentationStyle = .overCurrentContext
            self.present(Obj, animated: true, completion: nil)
        }else if sender.tag == 1{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "News Room", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                Obj.url = "https://drcafe.com/en-sa/guest/news-room"
            }else{
                Obj.url = "https://drcafe.com/ar-sa/guest/news-room"
            }
            Obj.modalPresentationStyle = .overCurrentContext
            self.present(Obj, animated: true, completion: nil)
        }else if sender.tag == 2{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FAQ", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                Obj.url = "https://drcafestore.com/Shopping/AppFAQuestions"
            }else{
                Obj.url = "https://drcafestore.com/ar/Shopping/AppFAQuestions"
            }
            Obj.modalPresentationStyle = .overCurrentContext
            self.present(Obj, animated: true, completion: nil)
        }
    }
}
extension MoreVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String, Status: String) {
        if latitude == 0.0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if Status == "Allow"{
                    //Redirect to Settings app
                    self.settingsArray.removeAll()
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Profile", value: "", table: nil))!, details: "", image: "More_Profile"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Language", value: "", table: nil))!, details: self.language, image: "More_Language"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Password", value: "", table: nil))!, details: "", image: "More_Password"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Address", value: "", table: nil))!, details: "", image: "More_Address"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Cars", value: "", table: nil))!, details: "", image: "Manage Cars"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Cards", value: "", table: nil))!, details: "", image: "More_Manage Cards"))
                    self.settingsArray.append(MoreModel(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Theme", value: "", table: nil))!, details: "", image: "More_Themes"))
                    self.settingsTableView.reloadData()
                }
            }
        }else{
            print(MainAddress)
        }
    }
}
