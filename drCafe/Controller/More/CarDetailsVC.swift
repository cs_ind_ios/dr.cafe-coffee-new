//
//  CarDetailsVC.swift
//  drCafe
//
//  Created by mac3 on 10/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class CarDetailsVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var noRecordsFoundLbl: UILabel!

    var carDicArray = [CarsModel]()
    var isMore = false
    var isLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Details", value: "", table: nil))!
    }
    override func viewWillAppear(_ animated: Bool) {
//        if SaveAddressClass.carInformationArray.count > 0{
//            if let _ = SaveAddressClass.carInformationArray[0].Cars{
//                carDicArray = SaveAddressClass.carInformationArray[0].Cars!
//            }
//            self.tableView.reloadData()
//        }else{
//            self.getDetailsService()
//        }
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            addBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            addBtn.setTitleColor(.black, for: .normal)
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
            noRecordsFoundLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            addBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            addBtn.setTitleColor(.white, for: .normal)
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
            noRecordsFoundLbl.textColor = .darkGray
        }
        
        self.getDetailsService()
    }
    func getDetailsService(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId": 0]
        CartModuleServices.CarInformationService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    SaveAddressClass.carInformationArray = [data.Data!]
                    if let _ = data.Data!.Cars{
                        self.carDicArray = data.Data!.Cars!
                        self.tableView.reloadData()
                    }else{
                        self.noRecordsFoundLbl.isHidden = false
                    }
                }else{
                    self.noRecordsFoundLbl.isHidden = false
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isMore == true{
            self.navigationController?.popToRootViewController(animated: true)
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 4
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Add Button Action
    @IBAction func addBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CarInformationVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarInformationVC") as! CarInformationVC
        obj.whereObj = 3
        obj.carInformationVCDelegate = self
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
extension CarDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carDicArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!, for: indexPath) as! CarsListTVCell
        
        let dic = carDicArray[indexPath.row]
        cell.carNumberLbl.text = dic.VehicleNumber
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.carColorLbl.text = dic.ColorEn
            cell.brandLbl.text = dic.BrandEn
        }else{
            cell.carColorLbl.text = dic.ColorAr
            cell.brandLbl.text = dic.BrandAr
        }
        
        if dic.IsDefault == true{
            cell.defaultCheckBtn.setImage(UIImage(named: "car_check_box"), for: .normal)
        }else{
            cell.defaultCheckBtn.setImage(UIImage(named: "car_check_box_unselected"), for: .normal)
        }
        
        cell.carImg.setImageColor(color: UIColor(hexString: "\(dic.ColorCode)"))
        
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editBtn_Tapped(_:)), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtn_Tapped(_:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true)
    }
    @objc func editBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CarInformationVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarInformationVC") as! CarInformationVC
        obj.whereObj = 4
        obj.carDetailsDic = [carDicArray[sender.tag]]
        obj.carInformationVCDelegate = self
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func deleteBtn_Tapped(_ sender: UIButton){
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are you sure to delete car information?", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            return
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            let dic:[String:Any] = ["Id":self.carDicArray[sender.tag].Id]
            CartModuleServices.DeleteCustomerCarService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    SaveAddressClass.carInformationArray[0].Cars!.remove(at: sender.tag)
                    self.carDicArray.remove(at: sender.tag)
                    self.tableView.reloadData()
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }))
    }
}
extension CarDetailsVC: CarInformationVCDelegate{
    func didAddOrUpdateCar(status: Bool) {
        if status == true{
            if SaveAddressClass.carInformationArray.count > 0{
                if let _ = SaveAddressClass.carInformationArray[0].Cars{
                    carDicArray = SaveAddressClass.carInformationArray[0].Cars!
                }
            }
            self.tableView.reloadData()
        }
    }
}
extension UIImageView {//img.setImageColor(color: UIColor.purple) Use
  func setImageColor(color: UIColor) {
      let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
      self.image = templateImage
      self.tintColor = color
    }
}
extension UIImage {//let image = UIImage(named: "Image Name"), img.image = image?.maskWithColor(color: UIColor.red) Use
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}
