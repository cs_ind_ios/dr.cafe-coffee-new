//
//  ThemeColorVC.swift
//  drCafe
//
//  Created by Devbox on 28/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ThemeColorVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var setDefaultBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var themeCV: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    
    var themeColorArray = [themesDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        themeColorArray = SaveAddressClass.themeColorArray
        
        themeCV.delegate = self
        themeCV.dataSource = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Theme Color", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            themeCV.semanticContentAttribute = .forceLeftToRight
        }else{
            themeCV.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: Default Button
    @IBAction func setDefaultBtn_Tapped(_ sender: Any) {
        var themeId = 0
        let defaultArray = themeColorArray.filter({$0.DefaultSelection == true})
        if defaultArray.count > 0{
            themeId = defaultArray[0].Id
        }else{
            if themeColorArray.count > 0{
                themeId = themeColorArray[0].Id
            }
        }
        let dic = ["UserId":"\(UserDef.getUserId())", "ThemeId" : themeId,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2] as [String : Any]
        OrderModuleServices.GetThemeColorUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
//                        AppDelegate.getDelegate().selectColor = defaultArray[0].ForegroundColor!
//                        AppDelegate.getDelegate().selectBGColor = defaultArray[0].BackgroundColor!
//                        AppDelegate.getDelegate().selectColorname = defaultArray[0].ThemeName
//                        self.navigationController?.popViewController(animated: true)
//                        self.themeCV.reloadData()
                    self.getDashboardServiceWithoutLoader()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: ClectionView Delegate Methods
@available(iOS 13.0, *)
extension ThemeColorVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themeColorArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "ThemeColorCVCell", bundle: nil), forCellWithReuseIdentifier: "ThemeColorCVCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemeColorCVCell", for: indexPath) as! ThemeColorCVCell
        
        cell.themeColorNameLbl.text = themeColorArray[indexPath.row].ThemeName
        cell.themeSelectBtnBGView.backgroundColor = UIColor(hexString: "#\(themeColorArray[indexPath.row].BackgroundColor!)")
        if themeColorArray[indexPath.row].BackgroundColor! == AppDelegate.getDelegate().selectBGColor{
            cell.themeSelectBtn.setImage(UIImage(named: "Black Select"), for: .normal)
        }else{
            cell.themeSelectBtn.setImage(nil, for: .normal)
        }
        cell.BGView.backgroundColor = .clear
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            cell.themeColorNameLbl.textColor = .white
        }else{
            cell.themeColorNameLbl.textColor = .black
        }
        //cell.themeColorImg.image = UIImage(named: themeColorArray[indexPath.row].ThemeName)
        //Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(themeColorArray[indexPath.row].Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.themeColorImg.kf.setImage(with: url)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width/3, height: 270)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dic = ["UserId":"\(UserDef.getUserId())", "ThemeId" : themeColorArray[indexPath.row].Id,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2] as [String : Any]
        OrderModuleServices.GetThemeColorUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
//                    AppDelegate.getDelegate().selectColor = self.themeColorArray[indexPath.row].ForegroundColor!
//                    AppDelegate.getDelegate().selectBGColor = self.themeColorArray[indexPath.row].BackgroundColor!
//                    AppDelegate.getDelegate().selectColorname = self.themeColorArray[indexPath.row].ThemeName
//                    self.navigationController?.popViewController(animated: true)
//                    self.themeCV.reloadData()
                    self.getDashboardServiceWithoutLoader()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: DashBoard Details Get Service WithOut Loader
    func getDashboardServiceWithoutLoader(){
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        let dic = ["UserId": UserDef.getUserId(),"AppVersion" : "\(appversion)","AppType": "IOS","DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2, "IsSubscription":false, "Latitude":"","Longitude":"", "action" : 0] as [String : Any]
        OrderModuleServices.DashBoardServiceWithoutLoader(dic: dic, success: { (data) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    var themeColor = data.Data!.themes!.filter({$0.DefaultSelection == true})
                    if themeColor.count > 0{
                        if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                            AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                            AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                            AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                        }else{
                            AppDelegate.getDelegate().selectColor = "535353"
                            AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                            AppDelegate.getDelegate().selectColorname = ""
                        }
                    }else{
                        themeColor = data.Data!.themes!.filter({$0.IsSelected == true})
                        if themeColor.count > 0{
                            if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                                AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                                AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                                AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                            }else{
                                AppDelegate.getDelegate().selectColor = "535353"
                                AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                AppDelegate.getDelegate().selectColorname = ""
                            }
                        }else{
                            if data.Data!.themes!.count > 0{
                                if data.Data!.themes![0].ForegroundColor != nil && data.Data!.themes![0].BackgroundColor != nil{
                                    AppDelegate.getDelegate().selectColor = data.Data!.themes![0].ForegroundColor!
                                    AppDelegate.getDelegate().selectBGColor = data.Data!.themes![0].BackgroundColor!
                                    AppDelegate.getDelegate().selectColorname = data.Data!.themes![0].ThemeName
                                }else{
                                    AppDelegate.getDelegate().selectColor = "535353"
                                    AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                    AppDelegate.getDelegate().selectColorname = ""
                                }
                            }else{
                                AppDelegate.getDelegate().selectColor = "535353"
                                AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                AppDelegate.getDelegate().selectColorname = ""
                            }
                        }
                    }
                    self.themeCV.reloadData()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
