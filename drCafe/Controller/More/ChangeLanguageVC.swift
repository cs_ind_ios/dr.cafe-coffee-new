//
//  ChangeLanguageVC.swift
//  drCafe
//
//  Created by Devbox on 16/09/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class ChangeLanguageVC: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btmBGView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    
    var selectIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Language", value: "", table: nil))!
        
        self.btmBGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.doneBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            selectIndex = 0
        }else{
            selectIndex = 1
        }
        tableView.reloadData()
        
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Done Button
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        if AppDelegate.getDelegate().appLanguage == "English" && selectIndex == 0 || AppDelegate.getDelegate().appLanguage == "Arabic" && selectIndex == 1{
            navigationController?.popViewController(animated: true)
        }else{
            var language = "En"
            if AppDelegate.getDelegate().appLanguage == "English"{
                language = "En"
            }else{
                language = "Ar"
            }
//            let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//            let deviceInfo:[String:Any] = [
//                "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//                "applang":language,
//                "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//                "SDKversion":"",
//                "Modelname":"\(UIDevice.current.name)",
//                "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//                "AppVersion":"IOS(\(appVersion))",
//                "TimeZone":"\(TimeZone.current.abbreviation()!)",
//                "TimeZoneRegion":"\(TimeZone.current.identifier)"]
            if isUserLogIn() == true{
                let dic = ["UserId": UserDef.getUserId(),
                 "Language": language,
                 "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
                 "DeviceInfo": "\(getDeviceInfo())", "RequestBy":2] as [String : Any]
                UserModuleServices.changeLanguageService(dic: dic, success: { (data) in
//                    if(data.Status == false){
//                        if AppDelegate.getDelegate().appLanguage == "English"{
//                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                        }else{
//                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                        }
//                    }else{
//                        if AppDelegate.getDelegate().appLanguage == "English"{
//                            Alert.showToast(on: self, message: data.MessageEn)
//                        }else{
//                            Alert.showToast(on: self, message: data.MessageAr)
//                        }
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                            self.languageChange()
//                        }
//                    }
                }) { (error) in
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }else{
                //self.languageChange()
            }
            self.languageChange()
        }
    }
    //MARK: Language Change functionality
    func languageChange(){
        if(AppDelegate.getDelegate().myLanguage == "English"){
            var path = String()
            path = Bundle.main.path(forResource: "ar", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "Arabic"
            UserDefaults.standard.set("Arabic", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "Arabic"
            AppDelegate.getDelegate().tabbarViewController()
        }else{
            var path = String()
            path = Bundle.main.path(forResource: "en", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "English"
            UserDefaults.standard.set("English", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "English"
            AppDelegate.getDelegate().tabbarViewController()
        }
    }
}
//MARK: TableView Delegate Methods
extension ChangeLanguageVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
        if indexPath.row == 0{
            cell.nameLbl.text = "English"
            cell.img.image = UIImage(named: "FLAG")
        }else if indexPath.row == 1{
            cell.nameLbl.text = "عربي"
            cell.img.image = UIImage(named: "language")
        }
        if selectIndex == indexPath.row{
            cell.selectImg.image = UIImage(named: "Un Select")
        }else{
            cell.selectImg.image = nil
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectIndex = indexPath.row
        tableView.reloadData()
    }
}
