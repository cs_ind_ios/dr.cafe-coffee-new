//
//  LanguageVC.swift
//  drCafe
//
//  Created by Devbox on 30/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class LanguageVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var detailsArray = [LanguageArrayModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Language", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            detailsArray.append(LanguageArrayModel(Name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "English", value: "", table: nil))!, IsSelect: true))
            detailsArray.append(LanguageArrayModel(Name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Arabic", value: "", table: nil))!, IsSelect: false))
        }else{
            detailsArray.append(LanguageArrayModel(Name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "English", value: "", table: nil))!, IsSelect: false))
            detailsArray.append(LanguageArrayModel(Name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Arabic", value: "", table: nil))!, IsSelect: true))
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension LanguageVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MoreTVCell", value: "", table: nil))!, for: indexPath) as! MoreTVCell
        cell.nameLbl.text = detailsArray[indexPath.row].Name!
        cell.detailsLbl.text = ""
        cell.nextArowBtn.isHidden = true
        if detailsArray[indexPath.row].IsSelect! == true{
            cell.selectBtn.isHidden = false
        }else{
            cell.selectBtn.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0...detailsArray.count-1{
            detailsArray[i].IsSelect = false
        }
        detailsArray[indexPath.row].IsSelect = true
        self.laguageChangeFunc()
        tableView.reloadData()
    }
    //MARK: Language Change functnaty
    func laguageChangeFunc() {
        if(AppDelegate.getDelegate().myLanguage == "English"){
            var path = String()
            path = Bundle.main.path(forResource: "ar", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "Arabic"
            UserDefaults.standard.set("Arabic", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "Arabic"
            AppDelegate.getDelegate().tabbarViewController()
        }else{
            var path = String()
            path = Bundle.main.path(forResource: "en", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "English"
            UserDefaults.standard.set("English", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "English"
            AppDelegate.getDelegate().tabbarViewController()
        }
    }
}
