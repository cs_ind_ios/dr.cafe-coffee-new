//
//  BeverageVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 15/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

@available(iOS 13.0, *)
class BeverageVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var topDetailsView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var cartNumLbl: UILabel!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var navigationBarBGView: UIView!
    @IBOutlet weak var sizeImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var allDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sizesTVMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nutritionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noteViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var itemImg: UIImageView!
    
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    @IBOutlet weak var tagsCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tagsCollectionViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var pointsNameLbl: UILabel!
    @IBOutlet weak var rewardPointsLbl: UILabel!
    @IBOutlet weak var pointsBtn: UIButton!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var calView: UIView!
    @IBOutlet weak var calMainView: UIView!
    @IBOutlet weak var calMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calMainViewBtmLineLbl: UILabel!
    
    @IBOutlet weak var weightNameLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var weightViewHeight: NSLayoutConstraint!
    @IBOutlet weak var outOfStockLbl: UILabel!
    
    @IBOutlet weak var quantityAmountLbl: UILabel!
    @IBOutlet weak var successfullyAddedMsgLbl: UILabel!
    @IBOutlet weak var currecyNameLbl: UILabel!
    @IBOutlet weak var successfullyAddedMsgViewHeight: NSLayoutConstraint!
    
    //@IBOutlet weak var topSellerImage: UIImageView!
    //@IBOutlet weak var topSellerBtn: UIButton!
    @IBOutlet weak var sizeTableView: UITableView!
    @IBOutlet weak var sizeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeNameLbl: UILabel!
    @IBOutlet weak var sizeDescLbl: UILabel!
    @IBOutlet weak var calLbl: UILabel!
    @IBOutlet weak var sizeWtLbl: UILabel!
    @IBOutlet weak var sizeWtLblHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeWtLblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noteView: UIView!
    
    @IBOutlet weak var selectBeanMainView: UIView!
    @IBOutlet weak var selectBeanMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectBeanCV: UICollectionView!
    
    @IBOutlet weak var latteNameLbl: UILabel!
    @IBOutlet weak var latteMainView: UIView!
    @IBOutlet weak var latteMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var latteCV: UICollectionView!
    
    @IBOutlet weak var foamnameLbl: UILabel!
    @IBOutlet weak var foamMainView: UIView!
    @IBOutlet weak var foamMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foamCV: UICollectionView!
    
    @IBOutlet weak var noteTF: UITextField!
    @IBOutlet weak var noteLineLbl: UILabel!
    
    @IBOutlet weak var moreCoffeeTypeTableView: UITableView!
    @IBOutlet weak var moreCoffeeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var moreCoffeeTypeTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var coffeeTypeTableView: UITableView!
    @IBOutlet weak var coffeeTypeLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeBelowLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeBottomConstraint: NSLayoutConstraint!
    
    //Not Recommend
    @IBOutlet weak var customizedViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var latteNameNotRecommendLbl: UILabel!
    @IBOutlet weak var latteNotRecommendMainView: UIView!
    @IBOutlet weak var latteNotRecommendMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var latteNotRecommendCV: UICollectionView!
    
    @IBOutlet weak var foamNotRecommendnameLbl: UILabel!
    @IBOutlet weak var foamNotRecommendMainView: UIView!
    @IBOutlet weak var foamNotRecommendMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foamNotRecommendCV: UICollectionView!
    
    @IBOutlet weak var moreCoffeeTypeNotRecommendTableView: UITableView!
    @IBOutlet weak var moreCoffeeTypeNotRecommendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var moreCoffeeTypeNotRecommendTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var coffeeTypeNotRecommendTableView: UITableView!
    @IBOutlet weak var coffeeTypeNotRecommendTVHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeNotRecommendLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeNotRecommendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeNotRecommendBelowLineLbl: UILabel!
    
    @IBOutlet weak var recommendedNameLbl: UILabel!
    @IBOutlet weak var recommendedViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    @IBOutlet weak var RecommendedTableView: UITableView!
    @IBOutlet weak var RecommendedTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addBtnView: UIView!
    @IBOutlet weak var addMoreBtnView: UIView!
    @IBOutlet weak var noThanksView: UIView!
    @IBOutlet weak var noThanksBtn: UIButton!
    @IBOutlet weak var addMoreBtn: UIButton!
    @IBOutlet weak var viewCartBtn: UIButton!
    
    @IBOutlet weak var nutritionInfoBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var selectNutritionCalLbl: UILabel!
    @IBOutlet weak var allDetailsView: UIView!
    
    @IBOutlet weak var noRecordFoundView: UIView!
    @IBOutlet weak var noRecordFoundLbl: UILabel!
    
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    var quantity:Int = 1
    var totalAmount:Double = 1

    var popover = Popover()
    static var instance: BeverageVC!
    var backWhereObj = 0
    
    var whereObj = 1
    var sectionType = 0
    
    var addressDetailsArray = [AddressDetailsModel]()
    var dropDownArray = [DropDownModel]()
    var nutritionDetailsArray:[[String:Any]]!
    var selectNutrition:String!
    var dropWhereObj:Int!
    var selectIndex:Int!
    var itemID:Int!
    var storeId:Int!
    var selectOrderType:Int!
    var selectAddressId:Int!
    var isV12 = false
    var address:String!
    
    var selectSizeId:Int!
    var selectSizePrice:Double!
    var selectSizeIndex = 1
    var grinderItemId = 0
    var otherBeanSelect = false
    var otherBeanSelectId = 0
    
    var isSubscription = false
    var isCustomized = false
    
    var additionalsArray = [AdditionalsDetailsModel]()
    var latteDetailsArray = [AdditionalGroupsModel]()
    //var foamDetailsArray = [AdditionalGroupsModel]()
    var grinderDetailsArray = [AdditionalGroupsModel]()
    var additionalItemsArray = [AdditionalGroupsModel]()
    var minSelectAdditionalItemsArray = [AdditionalGroupsModel]()
    var titleLbl:String!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    var itemImagesArray = [ItemImagesModel]()
    @IBOutlet weak var bannerPageController: UIPageControl!
    var SwiftTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        BeverageVC.instance = self
        let width = UIScreen.main.bounds.width - 30
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: width, height: width * 0.7)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .horizontal
        bannerCollectionView.collectionViewLayout = layout
        bannerCollectionView.dataSource = self
        bannerCollectionView.delegate = self
        tabBarController?.tabBar.isHidden = true
        quantityLbl.text = "\(quantity)"
        
        RecommendedTableView.delegate = self
        RecommendedTableView.dataSource = self
        coffeeTypeTableView.delegate = self
        coffeeTypeTableView.dataSource = self
        moreCoffeeTypeTableView.delegate = self
        moreCoffeeTypeTableView.dataSource = self
        sizeTableView.delegate = self
        sizeTableView.dataSource = self
        noteTF.delegate = self
        
        coffeeTypeNotRecommendTableView.delegate = self
        coffeeTypeNotRecommendTableView.dataSource = self
        moreCoffeeTypeNotRecommendTableView.delegate = self
        moreCoffeeTypeNotRecommendTableView.dataSource = self
        latteNotRecommendCV.delegate = self
        latteNotRecommendCV.dataSource = self
        foamNotRecommendCV.delegate = self
        foamNotRecommendCV.dataSource = self
        
        selectBeanCV.delegate = self
        selectBeanCV.dataSource = self
        latteCV.delegate = self
        latteCV.dataSource = self
        foamCV.delegate = self
        foamCV.dataSource = self
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        RecommendedTableView.estimatedRowHeight = 130
        RecommendedTableView.rowHeight = UITableView.automaticDimension
        
        getStoreAdditionalsService()
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            bannerCollectionView.semanticContentAttribute = .forceLeftToRight
            selectBeanCV.semanticContentAttribute = .forceLeftToRight
            latteCV.semanticContentAttribute = .forceLeftToRight
            foamCV.semanticContentAttribute = .forceLeftToRight
            latteNotRecommendCV.semanticContentAttribute = .forceLeftToRight
            foamNotRecommendCV.semanticContentAttribute = .forceLeftToRight
            tagsCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            bannerCollectionView.semanticContentAttribute = .forceRightToLeft
            selectBeanCV.semanticContentAttribute = .forceRightToLeft
            latteCV.semanticContentAttribute = .forceRightToLeft
            foamCV.semanticContentAttribute = .forceRightToLeft
            latteNotRecommendCV.semanticContentAttribute = .forceRightToLeft
            foamNotRecommendCV.semanticContentAttribute = .forceRightToLeft
            tagsCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        //self.allDetailsView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
        //self.topDetailsView.roundCorners(corners: [.topLeft,.topRight], radius: 10)
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        
        if backWhereObj == 1{
            ItemsVC.instance.backWhereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                // User Interface is Dark
                if isV12 == true{
                    navigationBarBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    navigationBarTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cartNumLbl.textColor = #colorLiteral(red: 0.3568627451, green: 0.1803921569, blue: 0.1725490196, alpha: 1)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        backBtn.setImage(UIImage(named: "Back"), for: .normal)
                    }else{
                        backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                    }
                    cartBtn.setImage(UIImage(named: "Cart"), for: .normal)
                }else{
                    navigationBarBGView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    navigationBarTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cartNumLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        backBtn.setImage(UIImage(named: "back white"), for: .normal)
                    }else{
                        backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                    }
                    cartBtn.setImage(UIImage(named: "cart white"), for: .normal)
                }
            }else{
                // User Interface is Light
                if isV12 == true{
                    navigationBarBGView.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                    navigationBarTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    view.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                    cartNumLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        backBtn.setImage(UIImage(named: "back white"), for: .normal)
                    }else{
                        backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                    }
                    cartBtn.setImage(UIImage(named: "cart white"), for: .normal)
                }else{
                    navigationBarBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    navigationBarTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cartNumLbl.textColor = #colorLiteral(red: 0.3568627451, green: 0.1803921569, blue: 0.1725490196, alpha: 1)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        backBtn.setImage(UIImage(named: "Back"), for: .normal)
                    }else{
                        backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                    }
                    cartBtn.setImage(UIImage(named: "Cart"), for: .normal)
                }
            }
            
            navigationBarTitleLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
            outOfStockLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
            if whereObj == 2{
                self.navigationController?.popViewController(animated: true)
            }
            navigationBarTitleLbl.text! = titleLbl!
            if isSubscription == false{
                cartNumLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
            }else{
                cartNumLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
            }
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[2]
                if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                    tabItems[2].image = UIImage(named: "Cart Tab")
                    tabItems[2].selectedImage = UIImage(named: "Cart Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                }else{
                    tabItem.badgeValue = nil
                    tabItems[2].image = UIImage(named: "Tab Order")
                    tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                }
            }
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
                noRecordFoundView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
                noRecordFoundLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                recommendedNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                totalAmountLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                quantityLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                currecyNameLbl.textColor = .white
                noThanksBtn.setTitleColor(.white, for: .normal)
                addMoreBtn.setTitleColor(.white, for: .normal)
                viewCartBtn.setTitleColor(.white, for: .normal)
                addBtn.backgroundColor = .white
                addBtn.setTitleColor(.black, for: .normal)
                addBtnView.backgroundColor = .black
                noThanksView.backgroundColor = .black
                addMoreBtnView.backgroundColor = .black
            }else{
                myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
                noRecordFoundView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
                noRecordFoundLbl.textColor = .darkGray
                recommendedNameLbl.textColor = .darkGray
                totalAmountLbl.textColor = .black
                quantityLbl.textColor = .black
                currecyNameLbl.textColor = .darkGray
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
    }
    //MARK: Start Timer Banner Scrolling
    func startTimer() {
        SwiftTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(HomeVC.scrollToNextCell), userInfo: nil, repeats: true);
    }
    //MARK: Banner Scroll To next cell
    @objc func scrollToNextCell(){
        //get cell size
        let cellSize = CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = bannerCollectionView.contentOffset;
        if bannerCollectionView.contentSize.width <= bannerCollectionView.contentOffset.x + cellSize.width{
            bannerCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y  , width: cellSize.width, height: cellSize.height), animated: false)
        }else{
            bannerCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        if bannerPageController.currentPage == bannerPageController.numberOfPages - 1 {
            bannerPageController.currentPage = 0
        } else {
            bannerPageController.currentPage += 1
        }
    }
    func getStoreAdditionalsService(){
        let dic:[String:Any] = ["StoreId": storeId!, "ItemId": itemID!, "OrderTypeId": selectOrderType!, "RequestBy":2, "IsSubscription": isSubscription]
        OrderModuleServices.GetAdditionalItemsService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.mainView.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.additionalsArray = data.Data!
                if self.additionalsArray.count > 0 {
                    self.itemImagesArray = self.additionalsArray[0].ItemImages!
                    self.bannerPageController.numberOfPages = self.itemImagesArray.count
                    self.bannerCollectionView.reloadData()
                    //self.startTimer()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    // additionals section expand functionality
                    if self.additionalsArray.count > 0{
                        self.noRecordFoundView.isHidden = true
                        if self.additionalsArray[0].AdditionalGroups != nil{
                            print("Additionals Not Empty")
                            if self.additionalsArray[0].AdditionalGroups!.count > 0{
                                for i in 0...self.additionalsArray[0].AdditionalGroups!.count-1{
                                    if self.additionalsArray[0].AdditionalGroups![i].MinSelection! >= 1{
                                        self.additionalsArray[0].AdditionalGroups![i].sectionIsExpand = true
                                    }else{
                                        self.additionalsArray[0].AdditionalGroups![i].sectionIsExpand = false
                                    }
                                    
                                    if self.additionalsArray[0].AdditionalGroups![i].Additionals != nil{
                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![0].ItemSelect = false
                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![0].additionalQty = 1
                                        if self.additionalsArray[0].AdditionalGroups![i].Additionals!.count > 1{
                                            for j in 0...self.additionalsArray[0].AdditionalGroups![i].Additionals!.count-1{
                                                self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                            }
                                        }
                                        if self.additionalsArray[0].AdditionalGroups![i].Additionals!.count > 1{
                                            for j in 0...self.additionalsArray[0].AdditionalGroups![i].Additionals!.count-1{
                                                if self.additionalsArray[0].AdditionalGroups![i].Additionals![j].AdditionalPrices![0].IsRecommended != nil{
                                                    if self.additionalsArray[0].AdditionalGroups![i].Additionals![j].AdditionalPrices![0].IsRecommended! == true{
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = true
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 1
                                                    }else{
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                                    }
                                                }else{
                                                    self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                    self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("Additionals Empty")
                            }
                        }else{
                            print("Additionals Empty")
                        }
                        if self.additionalsArray[0].Grinders != nil{
                            if self.additionalsArray[0].Grinders!.count > 0{
                                for i in 0...self.additionalsArray[0].Grinders!.count-1{
                                    self.additionalsArray[0].Grinders![i].selectGrinder = false
                                }
                                self.additionalsArray[0].Grinders![0].selectGrinder = true
                                self.grinderItemId = self.additionalsArray[0].Grinders![0].Id
                            }
                        }
                        self.AllDetails()
                    }else{
                        self.noRecordFoundView.isHidden = false
                        print("No Item Details")
                    }
                    
//                    if self.additionalsArray[0].MoreBeans != nil{
//                        if self.additionalsArray[0].MoreBeans!.count > 0{
//                            for i in 0...self.additionalsArray[0].MoreBeans!.count-1{
//                                self.additionalsArray[0].MoreBeans![i].isSelect = false
//                            }
//                        }
//                    }
                    
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Details
    func AllDetails(){
        recommendedViewHeight.constant = 0
        
        //Tag Details
        if additionalsArray[0].Tags!.count > 0{
            tagsCollectionViewHeight.constant = 40
            tagsCollectionViewTop.constant = 10
            tagsCollectionView.reloadData()
        }else{
            tagsCollectionViewHeight.constant = 0
            tagsCollectionViewTop.constant = 0
        }
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            itemNameLbl.text = additionalsArray[0].NameEn
            itemDescLbl.text = additionalsArray[0].DescEn
            sizeDescLbl.text = additionalsArray[0].Prices![0].DescEn
            sizeNameLbl.text = additionalsArray[0].Prices![0].SizeEn
        }else{
            itemNameLbl.text = additionalsArray[0].NameAr
            itemDescLbl.text = additionalsArray[0].DescAr
            sizeDescLbl.text = additionalsArray[0].Prices![0].DescAr
            sizeNameLbl.text = additionalsArray[0].Prices![0].SizeAr
        }
    
        myScrollView.layoutIfNeeded()
        
        //Weight Functionality
        if additionalsArray[0].Prices!.count == 1{
            if additionalsArray[0].Prices![0].Weight! > 0.0{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    weightLbl.text = "\(additionalsArray[0].Prices![0].Weight!) \(additionalsArray[0].Prices![0].WeightUOM!)"
                }else{
                    weightLbl.text = "\(additionalsArray[0].Prices![0].WeightUOM!) \(additionalsArray[0].Prices![0].Weight!)"
                }
                if additionalsArray[0].Prices![0].IsOutOfStock! == 1{
                    outOfStockLbl.isHidden = false
                }else{
                    outOfStockLbl.isHidden = true
                }
                weightViewHeight.constant = 40
                calMainViewBtmLineLbl.isHidden = true
                weightNameLbl.isHidden = false
                weightLbl.isHidden = false
            }else{
                if additionalsArray[0].Prices![0].IsOutOfStock! == 1{
                    weightViewHeight.constant = 40
                    calMainViewBtmLineLbl.isHidden = true
                    outOfStockLbl.isHidden = false
                }else{
                    weightViewHeight.constant = 0
                    calMainViewBtmLineLbl.isHidden = false
                    outOfStockLbl.isHidden = true
                }
                weightNameLbl.isHidden = true
                weightLbl.isHidden = true
            }
        }else{
//            let priceDetails = additionalsArray[0].Prices!.filter({$0.IsOutOfStock! == 0})
//            if priceDetails.count == 0{
//                weightViewHeight.constant = 40
//                calMainViewBtmLineLbl.isHidden = true
//                outOfStockLbl.isHidden = false
//            }else{
//                weightViewHeight.constant = 0
//                calMainViewBtmLineLbl.isHidden = false
//                outOfStockLbl.isHidden = true
//            }
            weightViewHeight.constant = 0
            calMainViewBtmLineLbl.isHidden = false
            outOfStockLbl.isHidden = true
            weightNameLbl.isHidden = true
            weightLbl.isHidden = true
        }
        if additionalsArray[0].Prices![0].Weight! > 0.0{
            sizeWtLbl.text = "WT : \(additionalsArray[0].Prices![0].Weight!) \(additionalsArray[0].Prices![0].WeightUOM!)"
            sizeWtLblHeight.constant = 21
            sizeWtLblTopConstraint.constant = 5
        }else{
            sizeWtLbl.text = ""
            sizeWtLblHeight.constant = 0
            sizeWtLblTopConstraint.constant = 0
        }
        
        let priceDetails = additionalsArray[0].Prices!.filter({$0.IsOutOfStock! == 0})
        if priceDetails.count > 0{
            selectSizePrice = priceDetails[0].Price!
            selectSizeId = priceDetails[0].SizeId!
            addBtn.isEnabled = true
            plusBtn.isEnabled = true
            minusBtn.isEnabled = true
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                addBtn.backgroundColor = .white
                addBtn.setTitleColor(.black, for: .normal)
            }else{
                addBtn.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                addBtn.setTitleColor(.white, for: .normal)
            }
        }else{
            selectSizePrice = additionalsArray[0].Prices![0].Price!
            selectSizeId = additionalsArray[0].Prices![0].SizeId!
            addBtn.isEnabled = false
            plusBtn.isEnabled = false
            minusBtn.isEnabled = false
            addBtn.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 0.6957804699)
        }
        
        if additionalsArray[0].RewardEligibility != nil{
            if additionalsArray[0].Prices![0].Calories! != "" || additionalsArray[0].RewardEligibility == true{
                self.calMainViewHeight.constant = 50
                if additionalsArray[0].RewardEligibility == true{
                    pointsView.isHidden = false
                    //pointsBtn.setTitle("\(String(describing: additionalsArray[0].RewardBand))", for: .normal)
                    rewardPointsLbl.text = "\(String(describing: additionalsArray[0].RewardBand!))"
                }else{
                    pointsView.isHidden = true
                }
                if additionalsArray[0].Prices![0].Calories! != ""{
                    calLbl.text = "\(additionalsArray[0].Prices![0].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                    calView.isHidden = false
                }else{
                    calView.isHidden = true
                }
            }else{
                self.calMainViewHeight.constant = 0
            }
        }else{
            pointsView.isHidden = true
            if additionalsArray[0].Prices![0].Calories! != ""{
                calLbl.text = "\(additionalsArray[0].Prices![0].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                calView.isHidden = false
                self.calMainViewHeight.constant = 50
            }else{
                calView.isHidden = true
                self.calMainViewHeight.constant = 0
            }
        }
    
        selectNutritionCalLbl.text = "\(additionalsArray[0].Prices![0].TotalFat!) KL"
        nutritionDetailsArray = [["Name":"Calcium","Calories":additionalsArray[0].Prices![0].Calcium!], ["Name":"Iron","Calories":additionalsArray[0].Prices![0].Iron!], ["Name":"VitaminC","Calories":additionalsArray[0].Prices![0].VitaminC!], ["Name":"VitaminA","Calories":additionalsArray[0].Prices![0].VitaminA!], ["Name":"Protein","Calories":additionalsArray[0].Prices![0].Protein!], ["Name":"Sugars","Calories":additionalsArray[0].Prices![0].Sugars!], ["Name":"DietaryFibers","Calories":additionalsArray[0].Prices![0].DietaryFibers!],["Name":"Carbohydrates","Calories":additionalsArray[0].Prices![0].Carbohydrates!],["Name":"Sodium","Calories":additionalsArray[0].Prices![0].Sodium!], ["Name":"Cholestrol","Calories":additionalsArray[0].Prices![0].Cholestrol!],["Name":"TransFat","Calories":additionalsArray[0].Prices![0].TransFat!],["Name":"SaturatedFat","Calories":additionalsArray[0].Prices![0].SaturatedFat!],["Name":"UOM","Calories":additionalsArray[0].Prices![0].UOM!],["Name":"CaloriesFromFat","Calories":additionalsArray[0].Prices![0].CaloriesFromFat!],["Name":"Calories","Calories":additionalsArray[0].Prices![0].Calories!], ["Name":"TotalFat","Calories":additionalsArray[0].Prices![0].TotalFat!]]
        
        //Item Image
        let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        itemImg.kf.setImage(with: url)
        
        //Item Size Image
        let ImgStr1:NSString = "\(displayImages.images.path())\(additionalsArray[0].Prices![0].Image!)" as NSString
        let charSet1 = CharacterSet.urlFragmentAllowed
        let urlStr1 : NSString = ImgStr1.addingPercentEncoding(withAllowedCharacters: charSet1)! as NSString
        let url1 = URL.init(string: urlStr1 as String)
        sizeImage.kf.setImage(with: url1)
        
        additionalItemsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 1 && $0.MinSelection == 0})
        minSelectAdditionalItemsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 1 && $0.MinSelection != 0})
        latteDetailsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 2})
        grinderDetailsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 3})
        
        if additionalItemsArray.count == 0 && latteDetailsArray.count == 0 && grinderDetailsArray.count == 0 && minSelectAdditionalItemsArray.count == 0{
            coffeeTypeBelowLineLbl.isHidden = true
            coffeeTypeBottomConstraint.constant = 0
        }else{
            coffeeTypeBelowLineLbl.isHidden = false
            coffeeTypeBottomConstraint.constant = 15
        }
        
        //Grinders functionality
        if additionalsArray[0].Grinders!.count > 0{
            //let recommend = additionalsArray[0].Grinders!.filter({$0.Recommended == true})
//            if view.frame.width >= 375{
//                if recommend.count > 0{
//                    if additionalsArray[0].ManualBrew! == true{
//                        self.selectBeanMainViewHeight.constant = 285
//                    }else{
//                        self.selectBeanMainViewHeight.constant = 260
//                    }
//                }else{
//                    if additionalsArray[0].ManualBrew! == true{
//                        self.selectBeanMainViewHeight.constant = 255
//                    }else{
//                        self.selectBeanMainViewHeight.constant = 230
//                    }
//                }
//            }else{
                //if recommend.count > 0{
                    if additionalsArray[0].ManualBrew! == true{
                        self.selectBeanMainViewHeight.constant = 280
                    }else{
                        self.selectBeanMainViewHeight.constant = 255
                    }
                //}else{
                    //if additionalsArray[0].ManualBrew! == true{
                        //self.selectBeanMainViewHeight.constant = 250
                    //}else{
                        //self.selectBeanMainViewHeight.constant = 225
                   // }
                //}
            //}
            
            DispatchQueue.main.async {
                self.selectBeanCV.reloadData()
            }
            coffeeTypeLineLbl.isHidden = false
            myScrollView.layoutIfNeeded()
        }else{
            coffeeTypeLineLbl.isHidden = true
            self.selectBeanMainViewHeight.constant = 0
            myScrollView.layoutIfNeeded()
        }
        //Latte Functionality
        if additionalsArray[0].AdditionalGroups != nil{
            if latteDetailsArray.count > 0 {
                if latteDetailsArray.count == 1{
                    if latteDetailsArray[0].MinSelection == 0{
                        self.isCustomized = true
                        let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                        let maxQtyArray =  latteDetailsArray[0].Additionals!.filter({$0.MaxQty > 1})
                        if priceArray.count > 0{
                            if maxQtyArray.count > 0{
                                latteNotRecommendMainViewHeight.constant = 254
                            }else{
                                latteNotRecommendMainViewHeight.constant = 214
                            }
                        }else{
                            if maxQtyArray.count > 0{
                                latteNotRecommendMainViewHeight.constant = 222
                            }else{
                                latteNotRecommendMainViewHeight.constant = 182
                            }
                        }
                        //latteMainViewHeight.constant = 216
                        DispatchQueue.main.async {
                            self.latteNotRecommendCV.reloadData()
                        }
                    }else{
                        let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                        let maxQtyArray =  latteDetailsArray[0].Additionals!.filter({$0.MaxQty > 1})
                        if priceArray.count > 0{
                            if maxQtyArray.count > 0{
                                latteMainViewHeight.constant = 254
                            }else{
                                latteMainViewHeight.constant = 214
                            }
                        }else{
                            if maxQtyArray.count > 0{
                                latteMainViewHeight.constant = 222
                            }else{
                                latteMainViewHeight.constant = 182
                            }
                        }
                        //latteMainViewHeight.constant = 216
                        DispatchQueue.main.async {
                            self.latteCV.reloadData()
                        }
                    }
                }else{
                    for i in 1...2{
                        if i == 1{
                            if latteDetailsArray[0].MinSelection == 0{
                                self.isCustomized = true
                                let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                                let maxQtyArray =  latteDetailsArray[0].Additionals!.filter({$0.MaxQty > 1})
                                if priceArray.count > 0{
                                    if maxQtyArray.count > 0{
                                        latteNotRecommendMainViewHeight.constant = 254
                                    }else{
                                        latteNotRecommendMainViewHeight.constant = 214
                                    }
                                }else{
                                    if maxQtyArray.count > 0{
                                        latteNotRecommendMainViewHeight.constant = 222
                                    }else{
                                        latteNotRecommendMainViewHeight.constant = 182
                                    }
                                }
                                //latteMainViewHeight.constant = 216
                                DispatchQueue.main.async {
                                    self.latteNotRecommendCV.reloadData()
                                }
                            }else{
                                let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                                let maxQtyArray =  latteDetailsArray[0].Additionals!.filter({$0.MaxQty > 1})
                                if priceArray.count > 0{
                                    if maxQtyArray.count > 0{
                                        latteMainViewHeight.constant = 254
                                    }else{
                                        latteMainViewHeight.constant = 214
                                    }
                                }else{
                                    if maxQtyArray.count > 0{
                                        latteMainViewHeight.constant = 222
                                    }else{
                                        latteMainViewHeight.constant = 182
                                    }
                                }
                                //latteMainViewHeight.constant = 216
                                DispatchQueue.main.async {
                                    self.latteCV.reloadData()
                                }
                            }
                        }else{
                            if latteDetailsArray[1].MinSelection == 0{
                                self.isCustomized = true
                                let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                                let maxQtyArray =  latteDetailsArray[1].Additionals!.filter({$0.MaxQty > 1})
                                if priceArray.count > 0{
                                    if maxQtyArray.count > 0{
                                        foamNotRecommendMainViewHeight.constant = 254
                                    }else{
                                        foamNotRecommendMainViewHeight.constant = 214
                                    }
                                }else{
                                    if maxQtyArray.count > 0{
                                        foamNotRecommendMainViewHeight.constant = 222
                                    }else{
                                        foamNotRecommendMainViewHeight.constant = 182
                                    }
                                }
                                //foamMainViewHeight.constant = 216
                                DispatchQueue.main.async {
                                    self.foamNotRecommendCV.reloadData()
                                }
                            }else{
                                let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                                let maxQtyArray =  latteDetailsArray[1].Additionals!.filter({$0.MaxQty > 1})
                                if priceArray.count > 0{
                                    if maxQtyArray.count > 0{
                                        foamMainViewHeight.constant = 254
                                    }else{
                                        foamMainViewHeight.constant = 214
                                    }
                                }else{
                                    if maxQtyArray.count > 0{
                                        foamMainViewHeight.constant = 222
                                    }else{
                                        foamMainViewHeight.constant = 182
                                    }
                                }
                                //foamMainViewHeight.constant = 216
                                DispatchQueue.main.async {
                                    self.foamCV.reloadData()
                                }
                            }
                        }
                    }
                }
            }else{
                latteMainViewHeight.constant = 0
                foamMainViewHeight.constant = 0
                latteNotRecommendMainViewHeight.constant = 0
                foamNotRecommendMainViewHeight.constant = 0
            }
//            if foamDetailsArray.count > 0{
//                let priceArray =  foamDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
//                let maxQtyArray =  foamDetailsArray[0].Additionals!.filter({$0.MaxQty > 1})
//                if priceArray.count > 0{
//                    if maxQtyArray.count > 0{
//                        foamMainViewHeight.constant = 258
//                    }else{
//                        foamMainViewHeight.constant = 218
//                    }
//                }else{
//                    if maxQtyArray.count > 0{
//                        foamMainViewHeight.constant = 231
//                    }else{
//                        foamMainViewHeight.constant = 191
//                    }
//                }
//                //foamMainViewHeight.constant = 216
//                DispatchQueue.main.async {
//                    self.foamCV.reloadData()
//                }
//            }else{
//                foamMainViewHeight.constant = 0
//            }
            
            if grinderDetailsArray.count > 0{
                if grinderDetailsArray[0].MinSelection == 0{
                    isCustomized = true
                    moreCoffeeTypeNotRecommendTableViewHeight.constant = CGFloat(grinderDetailsArray.count * 50)
                    moreCoffeeTypeNotRecommendViewHeight.constant = CGFloat(grinderDetailsArray.count * 50) + 21
                    moreCoffeeTypeNotRecommendTableView.reloadData()
                }else{
                    moreCoffeeTypeTableViewHeight.constant = CGFloat(grinderDetailsArray.count * 50)
                    moreCoffeeTypeViewHeight.constant = CGFloat(grinderDetailsArray.count * 50) + 21
                    moreCoffeeTypeTableView.reloadData()
                }
            }else{
                moreCoffeeTypeTableViewHeight.constant = 0
                moreCoffeeTypeViewHeight.constant = 0
                moreCoffeeTypeNotRecommendTableViewHeight.constant = 0
                moreCoffeeTypeNotRecommendViewHeight.constant = 0
            }
            
            //if additionalsArray.count > 0{
//                let minSelection = additionalItemsArray.filter({$0.MinSelection == 0})
//                if minSelection.count > 0{
//                    coffeeTypeTableViewHeight.constant = CGFloat(minSelection.count * 50)
//                    if maxSelection.count > 0{
//                        coffeeTypeRecommendTVHeight.constant = CGFloat(maxSelection.count * 50)
//                        coffeeTypeViewHeight.constant = CGFloat(minSelection.count * 50) + CGFloat(maxSelection.count * 50) + 71
//                        coffeeTypeRecommendTableView.reloadData()
//                        coffeeTypeTableView.reloadData()
//                    }else{
//                        coffeeTypeRecommendTVHeight.constant = 0
//                        coffeeTypeViewHeight.constant = CGFloat(minSelection.count * 50) + 61
//                        coffeeTypeTableView.reloadData()
//                    }
//                    customizedViewHeight.constant = 30
//                }else{
//                    if maxSelection.count > 0{
//                        coffeeTypeRecommendTVHeight.constant = CGFloat(maxSelection.count * 50)
//                        coffeeTypeViewHeight.constant = CGFloat(maxSelection.count * 50) + 21
//                        coffeeTypeRecommendTableView.reloadData()
//                    }else{
//                        coffeeTypeRecommendTVHeight.constant = 0
//                        coffeeTypeViewHeight.constant = 21
//                    }
//                    coffeeTypeTableViewHeight.constant = 0
//                    coffeeTypeTableView.reloadData()
//                    customizedViewHeight.constant = 0
//                }
//            }else{
//                coffeeTypeTableViewHeight.constant = 0
//                coffeeTypeViewHeight.constant = 0
//                customizedViewHeight.constant = 0
//            }
            
            if additionalsArray.count > 0{
                if minSelectAdditionalItemsArray.count > 0{
                    coffeeTypeTableViewHeight.constant = CGFloat(minSelectAdditionalItemsArray.count * 50)
                    coffeeTypeViewHeight.constant = CGFloat(minSelectAdditionalItemsArray.count * 50) + 21
                    coffeeTypeTableView.reloadData()
                }else{
                    coffeeTypeTableViewHeight.constant = 0
                    coffeeTypeViewHeight.constant = 0
                }
                if additionalItemsArray.count > 0{
                    isCustomized = true
                    coffeeTypeNotRecommendTVHeight.constant = CGFloat(additionalItemsArray.count * 50)
                    coffeeTypeNotRecommendViewHeight.constant = CGFloat(additionalItemsArray.count * 50) + 21
                    coffeeTypeNotRecommendTableView.reloadData()
                }else{
                    coffeeTypeNotRecommendTVHeight.constant = 0
                    coffeeTypeNotRecommendViewHeight.constant = 0
                }
            }else{
                coffeeTypeTableViewHeight.constant = 0
                coffeeTypeViewHeight.constant = 0
                customizedViewHeight.constant = 0
                coffeeTypeNotRecommendTVHeight.constant = 0
            }
            myScrollView.layoutIfNeeded()
        }else{
            self.latteMainViewHeight.constant = 0
            self.foamMainViewHeight.constant = 0
            self.latteNotRecommendMainViewHeight.constant = 0
            self.foamNotRecommendMainViewHeight.constant = 0
            moreCoffeeTypeTableViewHeight.constant = 0
            moreCoffeeTypeTableViewHeight.constant = 0
            coffeeTypeTableViewHeight.constant = 0
            coffeeTypeViewHeight.constant = 0
            coffeeTypeNotRecommendTVHeight.constant = 0
            myScrollView.layoutIfNeeded()
        }
        
//        if additionalsArray[0].MoreBeans != nil{
//            moreCoffeeTypeTableViewHeight.constant = CGFloat(additionalsArray[0].MoreBeans!.count * 50)
//            moreCoffeeTypeViewHeight.constant = CGFloat(additionalsArray[0].MoreBeans!.count * 50) + 21
//            moreCoffeeTypeTableView.reloadData()
//        }else{
//            moreCoffeeTypeTableViewHeight.constant = 0
//            moreCoffeeTypeViewHeight.constant = 0
//        }

//        if additionalsArray[0].AdditionalGroups!.count == 0 && additionalsArray[0].Grinders!.count == 0 && self.additionalsArray[0].Prices!.count == 1{
//            noteLineLbl.isHidden = false
//        }else{
            noteLineLbl.isHidden = true
        //}
        
        DispatchQueue.main.async {
            if self.additionalsArray[0].Prices!.count > 1{
                self.sizesTVMainViewHeight.constant = CGFloat((30 * self.additionalsArray[0].Prices!.count) + 50)
                self.sizeTableView.reloadData()
            }else{
                self.sizesTVMainViewHeight.constant = 0
            }
            if self.isCustomized == true{
                self.customizedViewHeight.constant = 50
            }else{
                self.customizedViewHeight.constant = 0
            }
        }
        self.priceCalculation()
        self.mainView.isHidden = false
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Calories Info Button Action
    @IBAction func caloriesInfoBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "NutritionDetailsVC") as! NutritionDetailsVC
        obj.dropDownArray = nutritionDetailsArray as! [[String : String]]
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        present(obj, animated: false, completion: nil)
    }
    //MARK: Nutrition Info Button Action
    @IBAction func nutritionInfoBtn_Tapped(_ sender: UIButton) {
//        var dropDownArray = [DropDownModel]()
//        for dic in self.detailsArray{
//            dropDownArray.append(DropDownModel(ID: dic.BUSINESSTRIPID, Name: dic.FromDate))
//        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 300, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        pVC?.sourceRect = CGRect(x: sender.center.x , y: 0, width: 0, height: sender.frame.size.height)
        obj.whereObj = 2
        //obj.dropDownArray = nutritionArray
        obj.dropVCDelegate = self
        obj.isSingle = false
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.isSubscription = isSubscription
            obj.whereObj = 5
            obj.isRemove = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Top Seller Button Action
//    @IBAction func topSellerBtn_Tapped(_ sender: UIButton) {
//        if AppDelegate.getDelegate().appLanguage == "English"{
//            popUpNameLbl.text = additionalsArray[0].Tags![0].NameEn
//        }else{
//            popUpNameLbl.text = additionalsArray[0].Tags![0].NameAr
//        }
//        let startPoint = CGPoint(x: sender.frame.origin.x+37, y: 155 - myScrollView.contentOffset.y)
//        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 120, height: 40)
//        popover.show(self.popUpLblMainView, point: startPoint)
//    }
    //MARK: Plus Button Action
    @IBAction func plusBtn_Tapped(_ sender: Any) {
        if additionalsArray[0].MaxOrderQty > quantity{
            quantity = quantity + 1
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have exceeded max quantity for this item", value: "", table: nil))!)
        }
        priceCalculation()
    }
    //MARK: Minus Button Action
    @IBAction func minusBtn_Tapped(_ sender: Any) {
        if quantity > 1{
            quantity = quantity - 1
            priceCalculation()
        }else{
            quantity = 1
        }
    }
    //MARK: Add Button Action
    @IBAction func addBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            if self.addressDetailsArray.count > 0{
                AppDelegate.getDelegate().selectAddress = self.addressDetailsArray[0].Address
                AppDelegate.getDelegate().cartStoreName = ""
            }else{
                AppDelegate.getDelegate().selectAddress = ""
                AppDelegate.getDelegate().cartStoreName = self.address!
            }
            self.addToCart()
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Add To Cart function
    func addToCart(){
        AppDelegate.getDelegate().cartStoreId = storeId
        var additionals:[[String:Any]] = [[:]]
        var additionalName:[String] = [""]
        if additionalsArray[0].AdditionalGroups!.count > 0{
            var additionalArray:[String:Any]!
            if self.grinderDetailsArray.count > 0{
                for i in 0...grinderDetailsArray.count-1{
                    if grinderDetailsArray[i].MinSelection! > 0{
                        let minSelect = grinderDetailsArray[i].MinSelection!
                        let minSelectArray = grinderDetailsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                        if minSelect <= minSelectArray.count{
                            for j in 0...grinderDetailsArray[i].Additionals!.count - 1{
                                if grinderDetailsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": grinderDetailsArray[i].Id!,"MenuAdditionalId": grinderDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": grinderDetailsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }else{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additionalName.append(grinderDetailsArray[i].NameEn!)
                            }else{
                                additionalName.append(grinderDetailsArray[i].NameAr!)
                            }
                        }
                    }else{
                        for j in 0...grinderDetailsArray[i].Additionals!.count - 1{
                            if grinderDetailsArray[i].Additionals![j].ItemSelect! == true{
                                additionalArray = ["AdditionalGroupId": grinderDetailsArray[i].Id!,"MenuAdditionalId": grinderDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": grinderDetailsArray[i].Additionals![j].additionalQty!]
                                additionals.append(additionalArray!)
                            }
                        }
                    }
                }
            }
            if self.latteDetailsArray.count > 0{
                for i in 0...latteDetailsArray.count-1{
                    if latteDetailsArray[i].MinSelection! > 0{
                        let minSelect = latteDetailsArray[i].MinSelection!
                        let minSelectArray = latteDetailsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                        if minSelect == minSelectArray.count{
                            for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                                if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": latteDetailsArray[i].Id!,"MenuAdditionalId": latteDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteDetailsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }else{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additionalName.append(latteDetailsArray[i].NameEn!)
                            }else{
                                additionalName.append(latteDetailsArray[i].NameAr!)
                            }
                        }
                    }else{
                        for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                            if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                                additionalArray = ["AdditionalGroupId": latteDetailsArray[i].Id!,"MenuAdditionalId": latteDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteDetailsArray[i].Additionals![j].additionalQty!]
                                additionals.append(additionalArray!)
                            }
                        }
                    }
                }
            }
            if self.additionalItemsArray.count > 0{
                for i in 0...additionalItemsArray.count - 1{
                    if additionalItemsArray[i].MinSelection! > 0{
                        let minSelect = additionalItemsArray[i].MinSelection!
                        let minSelectArray = additionalItemsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                        if minSelect == minSelectArray.count{
                            for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                                if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": additionalItemsArray[i].Id!,"MenuAdditionalId": additionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": additionalItemsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }else{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additionalName.append(additionalItemsArray[i].NameEn!)
                            }else{
                                additionalName.append(additionalItemsArray[i].NameAr!)
                            }
                        }
                    }else{
                        for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                            if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                additionalArray = ["AdditionalGroupId": additionalItemsArray[i].Id!,"MenuAdditionalId": additionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": additionalItemsArray[i].Additionals![j].additionalQty!]
                                additionals.append(additionalArray!)
                            }
                        }
                    }
                }
            }
            if self.minSelectAdditionalItemsArray.count > 0{
                for i in 0...minSelectAdditionalItemsArray.count - 1{
                    if minSelectAdditionalItemsArray[i].MinSelection! > 0{
                        let minSelect = minSelectAdditionalItemsArray[i].MinSelection!
                        let minSelectArray = minSelectAdditionalItemsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                        if minSelect == minSelectArray.count{
                            for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                                if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": minSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": minSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }else{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additionalName.append(minSelectAdditionalItemsArray[i].NameEn!)
                            }else{
                                additionalName.append(minSelectAdditionalItemsArray[i].NameAr!)
                            }
                        }
                    }else{
                        for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                            if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                additionalArray = ["AdditionalGroupId": minSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": minSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                additionals.append(additionalArray!)
                            }
                        }
                    }
                }
            }
            if additionals.count > 1{
                additionals.remove(at: 0)
            }
            if additionalName.count > 1{
                additionalName.remove(at: 0)
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Additionals in", value: "", table: nil))!) \(additionalName.joined(separator: ","))")
                return
            }
        }else{
            additionals = [[:]]
        }
        
        let dic:[String:Any] = ["userId": UserDef.getUserId(),"StoreId": storeId!,"OrderType": selectOrderType!,"MainCategoryId": additionalsArray[0].MainCategoryId,"CategoryId": additionalsArray[0].CategoryId,"SubCategoryId": additionalsArray[0].SubCategoryId,"MenuItemId": additionalsArray[0].Id,"GrinderItemId":"\(grinderItemId)","SizeId": selectSizeId!,"Quantity": quantity,"Comment": "\(noteTF.text!)","AddressId" : selectAddressId!,"OrderStatusId" : 1,"Additionals": additionals, "RequestBy":2, "SectionType":sectionType, "IsSubscription" : isSubscription]
        CartModuleServices.AddToCartService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.cartNumLbl.text = "\(data.Data!.CartItemsQuantity!)"
                if self.isSubscription == true{
                    AppDelegate.getDelegate().subCartQuantity = data.Data!.CartItemsQuantity!
                    AppDelegate.getDelegate().subCartAmount = data.Data!.TotalPrice!
                }else{
                    AppDelegate.getDelegate().cartQuantity = data.Data!.CartItemsQuantity!
                    AppDelegate.getDelegate().cartAmount = data.Data!.TotalPrice!
                }
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
                AppDelegate.getDelegate().selectSectionType = self.sectionType
                DispatchQueue.main.async {
                    self.successfullyAddedMsgViewHeight.constant = 35
                    self.allDetailsViewHeight.constant = 0
                    self.sizesTVMainViewHeight.constant = 0
                    self.nutritionViewHeight.constant = 0
                    self.noteViewHeight.constant = 0
                    self.selectBeanMainViewHeight.constant = 0
                    self.latteMainViewHeight.constant = 0
                    self.latteNotRecommendMainViewHeight.constant = 0
                    self.foamNotRecommendMainViewHeight.constant = 0
                    self.foamMainViewHeight.constant = 0
                    self.coffeeTypeViewHeight.constant = 0
                    self.coffeeTypeNotRecommendTVHeight.constant = 0
                    self.coffeeTypeNotRecommendViewHeight.constant = 0
                    self.weightViewHeight.constant = 0
                    self.calMainViewHeight.constant = 0
                    self.coffeeTypeLineLbl.isHidden = true
                    self.coffeeTypeNotRecommendLineLbl.isHidden = true
                    self.moreCoffeeTypeViewHeight.constant = 0
                    self.moreCoffeeTypeTableViewHeight.constant = 0
                    self.moreCoffeeTypeNotRecommendViewHeight.constant = 0
                    self.moreCoffeeTypeNotRecommendTableViewHeight.constant = 0
                    self.customizedViewHeight.constant = 0
                    //self.allDetailsView.isHidden = true
                    self.recommendedViewHeight.constant = 75 + self.RecommendedTableView.contentSize.height
                    DispatchQueue.main.async {
                        self.RecommendedTableView.reloadData()
                    }
                    self.quantityAmountLbl.text = "\(self.quantity) X \(self.selectSizePrice!) = \(self.totalAmount.withCommas())"
                    if self.additionalsArray[0].SuggestiveItems != nil{
//                        if self.additionalsArray[0].SuggestiveItems!.count > 0{
//                            self.noThanksView.isHidden = false
//                            self.addMoreBtnView.isHidden = true
//                            self.addBtnView.isHidden = true
//                        }else{
                            self.noThanksView.isHidden = true
                            self.addMoreBtnView.isHidden = false
                            self.addBtnView.isHidden = true
                        //}
                    }else{
                        self.noThanksView.isHidden = true
                        self.addMoreBtnView.isHidden = false
                        self.addBtnView.isHidden = true
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: No thanks Button Action
    @IBAction func noThanksBtn_Tapped(_ sender: Any) {
        noThanksView.isHidden = true
        addMoreBtnView.isHidden = false
        addBtnView.isHidden = true
    }
    //MARK: Add More Button Action
    @IBAction func addMoreBtn_Tapped(_ sender: Any) {
        ItemsVC.instance.backWhereObj = 1
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func itemImageBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.imageName = additionalsArray[0].Image
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    //MARK: View Cart Button Action
    @IBAction func viewCartBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CartVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        obj.isSubscription = isSubscription
        obj.whereObj = 5
        obj.isRemove = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Price calculation
    func priceCalculation(){
        quantityLbl.text = "\(quantity)"
        var itemPrice = selectSizePrice
        var additionalPrice = 0
        //additional Select Price Calculation
        if additionalItemsArray.count > 0{
            for i in 0...additionalItemsArray.count - 1{
                for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                    if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                        additionalPrice = Int(additionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(additionalItemsArray[i].Additionals![j].additionalQty!))
                        itemPrice = itemPrice! + Double(additionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        var minAdditionalPrice = 0
        //additional Select Price Calculation
        if minSelectAdditionalItemsArray.count > 0{
            for i in 0...minSelectAdditionalItemsArray.count - 1{
                for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                    if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                        minAdditionalPrice = Int(minSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!))
                        itemPrice = itemPrice! + Double(minAdditionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        //Grinder Select Price Calculation
        if additionalsArray[0].ManualBrew! == true{
            let grinderDetails =  additionalsArray[0].Grinders!.filter({$0.selectGrinder! == true})
            if grinderDetails.count > 0{
                if grinderDetails[0].Price > 0{
                    let price = grinderDetails[0].Price + itemPrice!
                    itemPrice = price
                }
            }
        }
        
        //Additional Price
        if latteDetailsArray.count > 0{
            for i in 0...latteDetailsArray.count - 1{
                for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                    if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                        additionalPrice = Int(latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(latteDetailsArray[i].Additionals![j].additionalQty!))
                        itemPrice = itemPrice! + Double(additionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        
        totalAmount = Double(itemPrice! * Double(quantity))
        totalAmountLbl.text = "\(totalAmount.withCommas())"
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension BeverageVC:UITableViewDelegate, UITableViewDataSource, BeveragesListTVCellDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == coffeeTypeNotRecommendTableView{
            if additionalsArray.count > 0{
                return additionalItemsArray.count
            }else{
                return 0
            }
        }else if tableView == coffeeTypeTableView{
            if additionalsArray.count > 0{
                return minSelectAdditionalItemsArray.count
            }else{
                return 0
            }
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if grinderDetailsArray.count > 0{
                return grinderDetailsArray.count
            }else{
                return 0
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == coffeeTypeNotRecommendTableView{
            if additionalsArray.count > 0{
                if additionalItemsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return additionalItemsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == coffeeTypeTableView{
            if additionalsArray.count > 0{
                if minSelectAdditionalItemsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return minSelectAdditionalItemsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if grinderDetailsArray.count > 0{
                if grinderDetailsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return grinderDetailsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == sizeTableView{
            if additionalsArray.count > 0{
                return additionalsArray[0].Prices!.count + 1
            }else{
                return 0
            }
        }else{
            if additionalsArray.count > 0{
                return additionalsArray[0].SuggestiveItems!.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == coffeeTypeNotRecommendTableView {
            return 50
        }else if tableView == coffeeTypeTableView{
            return 50
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if indexPath.row == 0{
                return 50
            }else{
                return 80
            }
        }else if tableView == sizeTableView{
            if additionalsArray[0].ManualBrew! == true && additionalsArray[0].Grinders!.count > 0{
                return 40
            }else{
                return 30
            }
        }else{
            return 140
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == coffeeTypeNotRecommendTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Coffee Type Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                //headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameEn!
                if additionalItemsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(additionalItemsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(additionalItemsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(additionalItemsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(additionalItemsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(dropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Coffee Type Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesListTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].HasVariant == false{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0 || additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0.0{
                        subCell.itemCalLbl.isHidden = true
                    }else{
                        if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                            var addPrice = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price
                            addPrice = addPrice * Double(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                            subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                        }else{
                            subCell.itemCalLbl.text = "\(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price.withCommas())"
                        }
                        subCell.itemCalLbl.isHidden = false
                        subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }else{
                    let priceDetails = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        if priceDetails[0].Price == 0 || priceDetails[0].Price == 0.0{
                            subCell.itemCalLbl.isHidden = true
                        }else{
                            if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                                var addPrice = priceDetails[0].Price
                                addPrice = addPrice * Double(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                                subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                            }else{
                                subCell.itemCalLbl.text = "\(priceDetails[0].Price.withCommas())"
                            }
                            subCell.itemCalLbl.isHidden = false
                            subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                        }
                    }else{
                        subCell.itemCalLbl.isHidden = true
                    }
                }
                
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 1{
                    subCell.itemCalLbl.isHidden = false
                    subCell.itemCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                    subCell.itemCalLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                }

                if additionalItemsArray[indexPath.section].MaxSelection! > 1{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                subCell.delegate = self
                subCell.tableView = coffeeTypeNotRecommendTableView
                
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                    subCell.quantityViewWidth.constant = 80
                    subCell.quantityLbl.text = "\(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)"
                }else{
                    subCell.quantityViewWidth.constant = 0
                }
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.coffeeTypeNotRecommendTableView.layoutIfNeeded()
                self.coffeeTypeNotRecommendTVHeight.constant = self.coffeeTypeNotRecommendTableView.contentSize.height
                self.coffeeTypeNotRecommendViewHeight.constant = self.coffeeTypeNotRecommendTableView.contentSize.height + 21
            }
//                let minSelection = self.additionalItemsArray.filter({$0.MinSelection == 0})
//                if minSelection.count > 0{
//                    self.coffeeTypeViewHeight.constant = self.coffeeTypeTableView.contentSize.height + 61
//                }else{
//                    self.coffeeTypeViewHeight.constant = self.coffeeTypeTableView.contentSize.height + 21
//                }
            
            cell.selectionStyle = .none
            return cell
        }else if tableView == coffeeTypeTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Coffee Type Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                //headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameEn!
                if minSelectAdditionalItemsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(minSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(minSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(minDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Coffee Type Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesListTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].HasVariant == false{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0 || minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0.0{
                        subCell.itemCalLbl.isHidden = true
                    }else{
                        if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                            var addPrice = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price
                            addPrice = addPrice * Double(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                            subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                        }else{
                            subCell.itemCalLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price.withCommas())"
                        }
                        subCell.itemCalLbl.isHidden = false
                        subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }else{
                    let priceDetails = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        if priceDetails[0].Price == 0 || priceDetails[0].Price == 0.0{
                            subCell.itemCalLbl.isHidden = true
                        }else{
                            if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                                var addPrice = priceDetails[0].Price
                                addPrice = addPrice * Double(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                                subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                            }else{
                                subCell.itemCalLbl.text = "\(priceDetails[0].Price.withCommas())"
                            }
                            subCell.itemCalLbl.isHidden = false
                            subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                        }
                    }else{
                        subCell.itemCalLbl.isHidden = true
                    }
                }
                
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 1{
                    subCell.itemCalLbl.isHidden = false
                    subCell.itemCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                    subCell.itemCalLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                }

                if minSelectAdditionalItemsArray[indexPath.section].MaxSelection! > 1{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                subCell.tableView = coffeeTypeTableView
                subCell.delegate = self

                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                    subCell.quantityViewWidth.constant = 80
                    subCell.quantityLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)"
                }else{
                    subCell.quantityViewWidth.constant = 0
                }
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.coffeeTypeTableView.layoutIfNeeded()
                self.coffeeTypeTableViewHeight.constant = self.coffeeTypeTableView.contentSize.height
                self.coffeeTypeViewHeight.constant = self.coffeeTypeTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
        } else if tableView == sizeTableView{
            if additionalsArray[0].ManualBrew! == true && additionalsArray[0].Grinders!.count > 0{
                //MARK: Size TableView Details
                sizeTableView.register(UINib(nibName: "SettingsVCTVCell", bundle: nil), forCellReuseIdentifier: "SettingsVCTVCell")
                let cell = sizeTableView.dequeueReusableCell(withIdentifier: "SettingsVCTVCell", for: indexPath) as! SettingsVCTVCell
                
                if indexPath.row == 0{
                    cell.nameLbl.text = "  \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Size", value: "", table: nil))!)"
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.nameLbl.font = UIFont(name: "Avenir Medium", size: 16)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.nameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeEn!)"
                    }else{
                        cell.nameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeAr!)"
                    }
                    cell.nameLbl.font = UIFont(name: "Avenir Book", size: 14)
                }
                
                if indexPath.row == selectSizeIndex{
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.862745098, blue: 0.8980392157, alpha: 1)
                }else{
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                cell.nameLbl.layer.cornerRadius = cell.nameLbl.frame.height/2
                cell.nameLbl.clipsToBounds = true
                
                cell.selectionStyle = .none
                return cell
            }else{
                //MARK: Size TableView Details
                sizeTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!)
                let cell = sizeTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesVCSizeTVCell
                
                if indexPath.row == 0{
                    cell.sizeNameLbl.text = "   \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Size", value: "", table: nil))!)"
                    cell.sizePriceLbl.text = "   \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!)"
                    cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizeNameLbl.font = UIFont(name: "Avenir Medium", size: 16)
                    cell.sizePriceLbl.font = UIFont(name: "Avenir Medium", size: 16)
                    cell.sizePriceLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.sizeNameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeEn!)"
                    }else{
                        cell.sizeNameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeAr!)"
                    }
                    if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 0{
                        cell.sizePriceLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].Price!.withCommas())"
                        cell.sizePriceLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }else{
                        cell.sizePriceLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                        cell.sizePriceLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                    }
                    cell.sizeNameLbl.font = UIFont(name: "Avenir Book", size: 14)
                    cell.sizePriceLbl.font = UIFont(name: "Avenir Book", size: 14)
                }
                
                if indexPath.row == selectSizeIndex{
                    if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 1{
                        if selectSizeIndex <= additionalsArray[0].Prices!.count{
                            selectSizeIndex = selectSizeIndex + 1
                            sizeTableView.reloadData()
                        }else{
                            selectSizeIndex = 0
                            cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        }
                    }else{
                        cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.862745098, blue: 0.8980392157, alpha: 1)
                        cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.9215686275, blue: 0.9607843137, alpha: 1)
                    }
                }else{
                    cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                sizeTableViewHeight.constant = sizeTableView.contentSize.height
                
                cell.selectionStyle = .none
                return cell
            }
        }else if tableView == moreCoffeeTypeTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Grinders Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                
                if grinderDetailsArray[indexPath.section].MinSelection! > 0{
                    
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(grinderDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Grinders Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, for: indexPath) as! GrindingMethodTVCell
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if grinderDetailsArray[indexPath.section].MaxSelection! > 1{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                //Image
                let ImgStr:NSString = "\(displayImages.images.path())\(grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                subCell.grindImg.kf.setImage(with: url)
                
                subCell.grindImg.layer.cornerRadius = 25
                subCell.grindImg.clipsToBounds = true
                
                cell = subCell
            }
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.moreCoffeeTypeTableView.layoutIfNeeded()
                self.moreCoffeeTypeTableViewHeight.constant = self.moreCoffeeTypeTableView.contentSize.height
                self.moreCoffeeTypeViewHeight.constant = self.moreCoffeeTypeTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
            
        }else if tableView == moreCoffeeTypeNotRecommendTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Grinders Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                
                if grinderDetailsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(grinderNotRecommendDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Grinders Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, for: indexPath) as! GrindingMethodTVCell
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if grinderDetailsArray[indexPath.section].MaxSelection! > 1{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                //Image
                let ImgStr:NSString = "\(displayImages.images.path())\(grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                subCell.grindImg.kf.setImage(with: url)
                
                subCell.grindImg.layer.cornerRadius = 25
                subCell.grindImg.clipsToBounds = true
                
                cell = subCell
            }
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.moreCoffeeTypeNotRecommendTableView.layoutIfNeeded()
                self.moreCoffeeTypeNotRecommendTableViewHeight.constant = self.moreCoffeeTypeNotRecommendTableView.contentSize.height
                self.moreCoffeeTypeNotRecommendViewHeight.constant = self.moreCoffeeTypeNotRecommendTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
            
        }else{
            //MARK: Recommended TableView Details
            RecommendedTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!)
            let cell = RecommendedTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!, for: indexPath) as! ItemsTVCell

            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
                cell.itemDiscLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].DescEn
            }else{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
                cell.itemDiscLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].DescAr
            }

            if additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories! == "0" || additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories! == "0.0"{
                cell.itemCalLbl.text = ""
                cell.itemCalLbl.isHidden = true
            }else{
                cell.itemCalLbl.text = "\(additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                cell.itemCalLbl.isHidden = false
            }
            
            cell.itemPriceLbl.text = "\(additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Price!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"

            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(addBtnTapped(_:)), for: .touchUpInside)

            cell.itemQuantityLbl.isHidden = true
            //Image
            let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].SuggestiveItems![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)

            myScrollView.layoutIfNeeded()
            RecommendedTableView.layoutIfNeeded()
            recommendedViewHeight.constant = 75 + RecommendedTableView.contentSize.height
            RecommendedTableViewHeight.constant = RecommendedTableView.contentSize.height + 10

            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == coffeeTypeNotRecommendTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.additionalItemsArray[indexPath.section].sectionIsExpand = !additionalItemsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = additionalItemsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = additionalItemsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(additionalItemsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(additionalItemsArray[indexPath.section].NameEn!)")
                                }else{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(additionalItemsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(additionalItemsArray[indexPath.section].NameAr!)")
                                }
                            }
                        }else{
                            if self.additionalItemsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.additionalItemsArray[indexPath.section].Additionals!.count-1{
                                    self.additionalItemsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1

                }
            }
            self.priceCalculation()
        }else if tableView == coffeeTypeTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.minSelectAdditionalItemsArray[indexPath.section].sectionIsExpand = !minSelectAdditionalItemsArray[indexPath.section].sectionIsExpand!
                
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = minSelectAdditionalItemsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = minSelectAdditionalItemsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath.section].NameEn!)")
                                }else{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath.section].NameAr!)")
                                }
                            }
                        }else{
                            if self.minSelectAdditionalItemsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.minSelectAdditionalItemsArray[indexPath.section].Additionals!.count-1{
                                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                }
            }
            self.priceCalculation()
        }else if tableView == moreCoffeeTypeTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.grinderDetailsArray[indexPath.section].sectionIsExpand = !grinderDetailsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = grinderDetailsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = grinderDetailsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].NameEn!)")
                                }else{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].NameAr!)")
                                }
                            }
                        }else{
                            if self.grinderDetailsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.grinderDetailsArray[indexPath.section].Additionals!.count-1{
                                    self.grinderDetailsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                }
            }
            self.priceCalculation()
        }else if tableView == moreCoffeeTypeNotRecommendTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.grinderDetailsArray[indexPath.section].sectionIsExpand = !grinderDetailsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = grinderDetailsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = grinderDetailsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].NameEn!)")
                                }else{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "in", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].NameAr!)")
                                }
                            }
                        }else{
                            if self.grinderDetailsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.grinderDetailsArray[indexPath.section].Additionals!.count-1{
                                    self.grinderDetailsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                }
            }
            self.priceCalculation()
        }else if tableView == sizeTableView{
            if indexPath.row != 0{
                if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 1{
                    print("Out Of Stock")
                }else{
                    selectSizeIndex = indexPath.row
                    selectSizeId = additionalsArray[0].Prices![indexPath.row-1].SizeId
                    selectSizePrice = additionalsArray[0].Prices![indexPath.row-1].Price
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        sizeDescLbl.text = additionalsArray[0].Prices![indexPath.row-1].DescEn
                        sizeNameLbl.text = additionalsArray[0].Prices![indexPath.row-1].SizeEn
                    }else{
                        sizeDescLbl.text = additionalsArray[0].Prices![indexPath.row-1].DescAr
                        sizeNameLbl.text = additionalsArray[0].Prices![indexPath.row-1].SizeAr
                    }
                    if additionalsArray[0].RewardEligibility != nil{
                        if additionalsArray[0].Prices![indexPath.row-1].Calories! != "" || additionalsArray[0].RewardEligibility == true{
                            self.calMainViewHeight.constant = 50
                            if additionalsArray[0].Prices![indexPath.row-1].Calories! != ""{
                                calLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                                calView.isHidden = false
                            }else{
                                calView.isHidden = true
                            }
                        }else{
                            self.calMainViewHeight.constant = 0
                        }
                    }else{
                        pointsView.isHidden = true
                        if additionalsArray[0].Prices![indexPath.row-1].Calories! != ""{
                            calLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                            calView.isHidden = false
                            self.calMainViewHeight.constant = 50
                        }else{
                            calView.isHidden = true
                            self.calMainViewHeight.constant = 0
                        }
                    }
                    
                    if additionalsArray[0].Prices![indexPath.row-1].Weight! > 0.0{
                        sizeWtLbl.text = "WT : \(additionalsArray[0].Prices![indexPath.row-1].Weight!) \(additionalsArray[0].Prices![indexPath.row-1].WeightUOM!)"
                        sizeWtLblHeight.constant = 21
                        sizeWtLblTopConstraint.constant = 5
                    }else{
                        sizeWtLbl.text = ""
                        sizeWtLblHeight.constant = 0
                        sizeWtLblTopConstraint.constant = 0
                    }
                    nutritionDetailsArray = [["Name":"Calcium","Calories":additionalsArray[0].Prices![indexPath.row-1].Calcium!], ["Name":"Iron","Calories":additionalsArray[0].Prices![indexPath.row-1].Iron!], ["Name":"VitaminC","Calories":additionalsArray[0].Prices![indexPath.row-1].VitaminC!], ["Name":"VitaminA","Calories":additionalsArray[0].Prices![indexPath.row-1].VitaminA!], ["Name":"Protein","Calories":additionalsArray[0].Prices![indexPath.row-1].Protein!], ["Name":"Sugars","Calories":additionalsArray[0].Prices![indexPath.row-1].Sugars!], ["Name":"DietaryFibers","Calories":additionalsArray[0].Prices![indexPath.row-1].DietaryFibers!],["Name":"Carbohydrates","Calories":additionalsArray[0].Prices![indexPath.row-1].Carbohydrates!],["Name":"Sodium","Calories":additionalsArray[0].Prices![indexPath.row-1].Sodium!], ["Name":"Cholestrol","Calories":additionalsArray[0].Prices![indexPath.row-1].Cholestrol!],["Name":"TransFat","Calories":additionalsArray[0].Prices![indexPath.row-1].TransFat!],["Name":"SaturatedFat","Calories":additionalsArray[0].Prices![indexPath.row-1].SaturatedFat!],["Name":"UOM","Calories":additionalsArray[0].Prices![indexPath.row-1].UOM!],["Name":"CaloriesFromFat","Calories":additionalsArray[0].Prices![indexPath.row-1].CaloriesFromFat!],["Name":"Calories","Calories":additionalsArray[0].Prices![indexPath.row-1].Calories!], ["Name":"TotalFat","Calories":additionalsArray[0].Prices![indexPath.row-1].TotalFat!]]
                    
                    //Item Size Image
                    let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].Prices![indexPath.row-1].Image!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    sizeImage.kf.setImage(with: url)
                    
                    selectNutritionCalLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].TotalFat!) KL"
                    self.priceCalculation()
                    self.sizeTableView.reloadData()
                    self.selectBeanCV.reloadData()
                    self.coffeeTypeTableView.reloadData()
                    self.coffeeTypeNotRecommendTableView.reloadData()
                }
            }else{
                print(indexPath.row)
            }
        }else if tableView == RecommendedTableView{ //MARK:Recomended TableView
            itemID = additionalsArray[0].SuggestiveItems![indexPath.row].Id
            storeId = storeId!
            if AppDelegate.getDelegate().appLanguage == "English"{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
            }else{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
            }
            self.successfullyAddedMsgViewHeight.constant = 0
            self.noteViewHeight.constant = 75
            self.quantity = 1
            self.quantityLbl.text = "\(quantity)"
            self.noThanksView.isHidden = true
            self.addMoreBtnView.isHidden = true
            self.addBtnView.isHidden = false
            self.coffeeTypeLineLbl.isHidden = false
            self.getStoreAdditionalsService()
        } else{
            print(indexPath.row)
        }
    }
    @objc func grinderDropDownBtn_Tapped(_ sender: UIButton){
        self.grinderDetailsArray[sender.tag].sectionIsExpand = !grinderDetailsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        moreCoffeeTypeTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func grinderNotRecommendDropDownBtn_Tapped(_ sender: UIButton){
        self.grinderDetailsArray[sender.tag].sectionIsExpand = !grinderDetailsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        moreCoffeeTypeNotRecommendTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func dropDownBtn_Tapped(_ sender: UIButton){
        self.additionalItemsArray[sender.tag].sectionIsExpand = !additionalItemsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        coffeeTypeNotRecommendTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func minDropDownBtn_Tapped(_ sender: UIButton){
        self.minSelectAdditionalItemsArray[sender.tag].sectionIsExpand = !minSelectAdditionalItemsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        coffeeTypeTableView.reloadSections([sender.tag], with: .automatic)
    }
    //MARK: Coffee Type cell Button Action
    func additionalButtonsTapped(cell: BeveragesListTVCell, plus: Bool, minus: Bool, tableView: UITableView) {
        if tableView == coffeeTypeNotRecommendTableView{
            let indexPath = self.coffeeTypeNotRecommendTableView.indexPath(for: cell)
            if plus == true{
                if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty > additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty!{
                        additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! + 1
                        priceCalculation()
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty)")
                    }
                }else{
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeNotRecommendTableView.reloadData()
            }else if minus == true{
                if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! == 1{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                    }else{
                        additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! - 1
                        priceCalculation()
                    }
                }else{
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeNotRecommendTableView.reloadData()
            }else{
                print(indexPath!.section)
                print(indexPath!.row)
            }
        }else{
            let indexPath = self.coffeeTypeTableView.indexPath(for: cell)
            if plus == true{
                if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty > minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty!{
                        minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! + 1
                        priceCalculation()
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty)")
                    }
                }else{
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeTableView.reloadData()
            }else if minus == true{
                if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! == 1{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                    }else{
                        minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! - 1
                        priceCalculation()
                    }
                }else{
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeTableView.reloadData()
            }else{
                print(indexPath!.section)
                print(indexPath!.row)
            }
        }
    }
    //MARK: Recommended cell Button Actions
    @objc func addBtnTapped(_ sender: UIButton){
        itemID = additionalsArray[0].SuggestiveItems![sender.tag].Id
        storeId = storeId!
        if AppDelegate.getDelegate().appLanguage == "English"{
            navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![sender.tag].NameEn
        }else{
            navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![sender.tag].NameAr
        }
        self.successfullyAddedMsgViewHeight.constant = 0
        self.noteViewHeight.constant = 75
        self.quantity = 1
        self.quantityLbl.text = "\(quantity)"
        self.noThanksView.isHidden = true
        self.addMoreBtnView.isHidden = true
        self.addBtnView.isHidden = false
        self.coffeeTypeLineLbl.isHidden = false
        self.getStoreAdditionalsService()
    }
}
@available(iOS 13.0, *)
extension BeverageVC:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == selectBeanCV{
            if additionalsArray.count > 0{
                return additionalsArray[0].Grinders!.count
            }else{
                return 0
            }
        }else if collectionView == latteCV || collectionView == latteNotRecommendCV{
            if latteDetailsArray.count > 0{
                return latteDetailsArray[0].Additionals!.count
            }else{
                return 0
            }
        }else if collectionView == foamCV || collectionView == foamNotRecommendCV{
            if latteDetailsArray.count > 0{
                return latteDetailsArray[1].Additionals!.count
            }else{
                return 0
            }
        }else if collectionView == bannerCollectionView{
            return itemImagesArray.count
        }else if collectionView == tagsCollectionView{
            if additionalsArray.count > 0{
                return additionalsArray[0].Tags!.count
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == selectBeanCV{
            selectBeanCV.register(UINib(nibName: "RecommendedBeansCVCell", bundle: nil), forCellWithReuseIdentifier: "RecommendedBeansCVCell")
            let cell = selectBeanCV.dequeueReusableCell(withReuseIdentifier: "RecommendedBeansCVCell", for: indexPath) as! RecommendedBeansCVCell
            
            //Top Recommended Image Functionality
            if additionalsArray[0].Grinders![indexPath.row].Recommended == true{
                cell.recommendedImg.isHidden = false
            }else{
                cell.recommendedImg.isHidden = true
            }
            let recommend = additionalsArray[0].Grinders!.filter({$0.Recommended == true})
            if recommend.count > 0{
                cell.recommendedImgHeight.constant = 15
            }else{
                cell.recommendedImgHeight.constant = 0
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameEn
            }else{
                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameAr
            }
            
            if additionalsArray[0].ManualBrew! == true{
                cell.priceLblHeight.constant = 21
                cell.priceBottomConstraint.constant = 5
                cell.priceLbl.isHidden = false
                if additionalsArray[0].Grinders![indexPath.row].Price > 0{
                    let price = additionalsArray[0].Grinders![indexPath.row].Price + selectSizePrice
                    cell.priceLbl.text = "\(price.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                }else{
                    cell.priceLbl.text = "\(selectSizePrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                }
            }else{
                cell.priceLbl.isHidden = true
                cell.priceLblHeight.constant = 0
                cell.priceBottomConstraint.constant = 0
            }
            
            //Image
            let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].Grinders![indexPath.row].Icon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.beanTypeImg.kf.setImage(with: url)
            
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(beanInfoBtn_Tapped(_:)), for: .touchUpInside)
            
            if additionalsArray[0].Grinders![indexPath.row].selectGrinder == true{
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            
            return cell
            
//            selectBeanCV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalBeansCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalBeansCVCell", value: "", table: nil))!)
//            let cell = selectBeanCV.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalBeansCVCell", value: "", table: nil))!, for: indexPath) as! AdditionalBeansCVCell
//
//            //Top Recommended Image Functionality
//            let recommend = additionalsArray[0].Grinders!.filter({$0.Recommended == true})
//            if recommend.count > 0{
//                cell.recommendedTopConstraint.constant = 10
//                cell.recommendedImgHeight.constant = 20
//                if additionalsArray[0].Grinders![indexPath.row].Recommended == true{
//                    cell.recommendedImg.isHidden = false
//                }else{
//                    cell.recommendedImg.isHidden = true
//                }
//            }else{
//                cell.recommendedTopConstraint.constant = 0
//                cell.recommendedImgHeight.constant = 0
//            }
//
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameEn
//            }else{
//                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameAr
//            }
            
            //Price Details Functionality based on Manual Brew
//            if additionalsArray[0].ManualBrew! == true{
//                cell.priceLblHeight.constant = 21
//                cell.priceBottomConstraint.constant = 5
//                cell.priceLbl.isHidden = false
//                if additionalsArray[0].Grinders![indexPath.row].Price > 0{
//                    let price = additionalsArray[0].Grinders![indexPath.row].Price + selectSizePrice
//                    cell.priceLbl.text = "\(price.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//                }else{
//                    cell.priceLbl.text = "\(selectSizePrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//                }
//            }else{
//                cell.priceLbl.isHidden = true
//                cell.priceLblHeight.constant = 0
//                cell.priceBottomConstraint.constant = 0
//            }
            
//            //Image
//            let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].Grinders![indexPath.row].Icon)" as NSString
//            let charSet = CharacterSet.urlFragmentAllowed
//            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
//            let url = URL.init(string: urlStr as String)
//            cell.beanTypeImg.kf.setImage(with: url)
            
//            cell.beanSelectBtn.tag = indexPath.row
//            cell.beanSelectBtn.addTarget(self, action: #selector(beanSelectBtn_Tapped(_:)), for: .touchUpInside)
//
//            cell.infoBtn.tag = indexPath.row
//            cell.infoBtn.addTarget(self, action: #selector(beanInfoBtn_Tapped(_:)), for: .touchUpInside)
//
//            //Selection functionality
//            if additionalsArray[0].Grinders![indexPath.row].selectGrinder == true{
//                cell.beanSelectBtn.setImage(UIImage(named: "Select"), for: .normal)
//            }else{
//                cell.beanSelectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
//            }
//
//            return cell
        }else if collectionView == latteCV{
            latteCV.register(UINib(nibName: "AdditionalLatteCVCell", bundle: nil), forCellWithReuseIdentifier: "AdditionalLatteCVCell")
            let cell = latteCV.dequeueReusableCell(withReuseIdentifier: "AdditionalLatteCVCell", for: indexPath) as! AdditionalLatteCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.latteNameLbl.text = latteDetailsArray[0].NameEn!
                cell.nameLbl.text = latteDetailsArray[0].Additionals![indexPath.row].NameEn
            }else{
                self.latteNameLbl.text = latteDetailsArray[0].NameAr!
                cell.nameLbl.text = latteDetailsArray[0].Additionals![indexPath.row].NameAr
            }
            
            let ImgStr:NSString = "\(displayImages.images.path())\(latteDetailsArray[0].Additionals![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)

            let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                let pricearray =  latteDetailsArray[0].Additionals![indexPath.row].AdditionalPrices!.filter({$0.Price > 0})
                if pricearray.count > 0{
                    cell.priceLbl.text = "\(latteDetailsArray[0].Additionals![indexPath.row].AdditionalPrices![0].Price.withCommas())"
                }else{
                    cell.priceLbl.text = "0.00"
                }
                cell.priceLblHeight.constant = 22
                cell.priceLblBottomConstraint.constant = 5
            }else{
                cell.priceLbl.text = ""
                cell.priceLblHeight.constant = 0
                cell.priceLblBottomConstraint.constant = 0
            }
            
            cell.selectBtn.tag = indexPath.row
            cell.selectBtn.addTarget(self, action: #selector(latteSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            if latteDetailsArray[0].Additionals![indexPath.row].ItemSelect! == true{
                cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
            }else{
                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            
            //Quantity Functionality
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(lattePlusBtnTapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(latteMinusBtnTapped(_:)), for: .touchUpInside)
            if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                cell.quantityViewHeight.constant = 30
                cell.quantityViewTopConstraint.constant = 10
                cell.quantityLbl.text = "\(latteDetailsArray[0].Additionals![indexPath.row].additionalQty!)"
            }else{
                cell.quantityViewHeight.constant = 0
                cell.quantityViewTopConstraint.constant = 0
            }
            
            return cell
        }else if collectionView == latteNotRecommendCV{
            latteNotRecommendCV.register(UINib(nibName: "AdditionalLatteCVCell", bundle: nil), forCellWithReuseIdentifier: "AdditionalLatteCVCell")
            let cell = latteNotRecommendCV.dequeueReusableCell(withReuseIdentifier: "AdditionalLatteCVCell", for: indexPath) as! AdditionalLatteCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.latteNameNotRecommendLbl.text = latteDetailsArray[0].NameEn!
                cell.nameLbl.text = latteDetailsArray[0].Additionals![indexPath.row].NameEn
            }else{
                self.latteNameNotRecommendLbl.text = latteDetailsArray[0].NameAr!
                cell.nameLbl.text = latteDetailsArray[0].Additionals![indexPath.row].NameAr
            }
            
            let ImgStr:NSString = "\(displayImages.images.path())\(latteDetailsArray[0].Additionals![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)

            let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                let pricearray =  latteDetailsArray[0].Additionals![indexPath.row].AdditionalPrices!.filter({$0.Price > 0})
                if pricearray.count > 0{
                    cell.priceLbl.text = "\(latteDetailsArray[0].Additionals![indexPath.row].AdditionalPrices![0].Price.withCommas())"
                }else{
                    cell.priceLbl.text = "0.00"
                }
                cell.priceLblHeight.constant = 22
                cell.priceLblBottomConstraint.constant = 5
            }else{
                cell.priceLbl.text = ""
                cell.priceLblHeight.constant = 0
                cell.priceLblBottomConstraint.constant = 0
            }
            
            cell.selectBtn.tag = indexPath.row
            cell.selectBtn.addTarget(self, action: #selector(latteSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            if latteDetailsArray[0].Additionals![indexPath.row].ItemSelect! == true{
                cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
            }else{
                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            
            //Quantity Functionality
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(latteNotRecommendPlusBtnTapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(latteNotRecommendMinusBtnTapped(_:)), for: .touchUpInside)
            if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                cell.quantityViewHeight.constant = 30
                cell.quantityViewTopConstraint.constant = 10
                cell.quantityLbl.text = "\(latteDetailsArray[0].Additionals![indexPath.row].additionalQty!)"
            }else{
                cell.quantityViewHeight.constant = 0
                cell.quantityViewTopConstraint.constant = 0
            }
            
            return cell
        }else if collectionView == bannerCollectionView{
            bannerCollectionView.register(UINib(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
            let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
            let name = itemImagesArray[indexPath.row].Image
            cell.img.image = UIImage(named: "")
            let fullNameArr = name.split(separator:".")
            if fullNameArr.count > 1 {
            if fullNameArr[1] == "mp4" {
                cell.playBtn.tag = indexPath.row
                cell.playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
                cell.playBtn.isHidden = false
                cell.playerView.isHidden = false
                let ImgStr:NSString = "\(displayImages.images.path())\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                let avPlayer = AVPlayer(url: url! as URL);
                cell.playerView!.playerLayer.player = avPlayer;
                cell.contentView.bringSubviewToFront(cell.playBtn)
            }else{
                cell.playBtn.isHidden = true
                cell.playerView.isHidden = true
                let ImgStr:NSString = "\(displayImages.images.path())\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            }else{
                cell.playBtn.isHidden = true
                cell.playerView.isHidden = true
                let ImgStr:NSString = "\(displayImages.images.path())\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            
            return cell
        }else if collectionView == tagsCollectionView{
            tagsCollectionView.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
            let cell = tagsCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
            cell.cellBGImg.contentMode = .scaleAspectFit
            cell.cellBGImg.clipsToBounds = true
            //Image
            let ImgStr:NSString = "\(displayImages.images.path())\(additionalsArray[0].Tags![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.cellBGImg.kf.setImage(with: url)
            
            return cell
        }else if collectionView == foamCV{
            foamCV.register(UINib(nibName: "AdditionalLatteCVCell", bundle: nil), forCellWithReuseIdentifier: "AdditionalLatteCVCell")
            let cell = foamCV.dequeueReusableCell(withReuseIdentifier: "AdditionalLatteCVCell", for: indexPath) as! AdditionalLatteCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.foamnameLbl.text = latteDetailsArray[1].NameEn!
                cell.nameLbl.text = latteDetailsArray[1].Additionals![indexPath.row].NameEn
            }else{
                self.foamnameLbl.text = latteDetailsArray[1].NameAr!
                cell.nameLbl.text = latteDetailsArray[1].Additionals![indexPath.row].NameAr
            }
            
            let ImgStr:NSString = "\(displayImages.images.path())\(latteDetailsArray[1].Additionals![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                let pricearray =  latteDetailsArray[1].Additionals![indexPath.row].AdditionalPrices!.filter({$0.Price > 0})
                if pricearray.count > 0{
                    cell.priceLbl.text = "\(latteDetailsArray[1].Additionals![indexPath.row].AdditionalPrices![0].Price.withCommas())"
                }else{
                    cell.priceLbl.text = "0.00"
                }
                cell.priceLblHeight.constant = 22
                cell.priceLblBottomConstraint.constant = 5
            }else{
                cell.priceLbl.text = ""
                cell.priceLblHeight.constant = 0
                cell.priceLblBottomConstraint.constant = 0
            }
            
            cell.selectBtn.tag = indexPath.row
            cell.selectBtn.addTarget(self, action: #selector(latteSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            if latteDetailsArray[1].Additionals![indexPath.row].ItemSelect! == true{
                cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
            }else{
                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            //Quantity Functionality
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(foamPlusBtnTapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(foamMinusBtnTapped(_:)), for: .touchUpInside)
            if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                cell.quantityViewHeight.constant = 30
                cell.quantityViewTopConstraint.constant = 10
                cell.quantityLbl.text = "\(latteDetailsArray[1].Additionals![indexPath.row].additionalQty!)"
            }else{
                cell.quantityViewHeight.constant = 0
                cell.quantityViewTopConstraint.constant = 0
            }
            
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                self.foamnameLbl.text = foamDetailsArray[0].NameEn!
//            }else{
//                self.foamnameLbl.text = foamDetailsArray[0].NameAr!
//            }
//
//            //Image
//            let ImgStr:NSString = "\(displayImages.images.path())\(foamDetailsArray[0].Additionals![indexPath.row].Image)" as NSString
//            let charSet = CharacterSet.urlFragmentAllowed
//            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
//            let url = URL.init(string: urlStr as String)
//            cell.itemImg.kf.setImage(with: url)
//
//            let pricearray =  foamDetailsArray[0].Additionals![indexPath.row].AdditionalPrices!.filter({$0.Price > 0})
//            if pricearray.count > 0{
//                cell.priceLbl.text = "\(foamDetailsArray[0].Additionals![indexPath.row].AdditionalPrices![0].Price.withCommas())"
//                cell.priceLblHeight.constant = 22
//                cell.priceLblBottomConstraint.constant = 5
//            }else{
//                cell.priceLbl.text = ""
//                cell.priceLblHeight.constant = 0
//                cell.priceLblBottomConstraint.constant = 0
//            }
//
//            cell.selectBtn.tag = indexPath.row
//            cell.selectBtn.addTarget(self, action: #selector(foamSelectBtn_Tapped(_:)), for: .touchUpInside)
//
//            if foamDetailsArray[0].Additionals![indexPath.row].ItemSelect! == true{
//                cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
//            }else{
//                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
//            }
//
//            //Quantity Functionality
//            cell.plusBtn.tag = indexPath.row
//            cell.plusBtn.addTarget(self, action: #selector(foamPlusBtnTapped(_:)), for: .touchUpInside)
//            cell.minusBtn.tag = indexPath.row
//            cell.minusBtn.addTarget(self, action: #selector(foamMinusBtnTapped(_:)), for: .touchUpInside)
//            if foamDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
//                cell.quantityViewHeight.constant = 30
//                cell.quantityViewTopConstraint.constant = 10
//                cell.quantityLbl.text = "\(foamDetailsArray[0].Additionals![indexPath.row].additionalQty!)"
//            }else{
//                cell.quantityViewHeight.constant = 0
//                cell.quantityViewTopConstraint.constant = 0
//            }
            
            return cell
        }else{
            foamNotRecommendCV.register(UINib(nibName: "AdditionalLatteCVCell", bundle: nil), forCellWithReuseIdentifier: "AdditionalLatteCVCell")
            let cell = foamNotRecommendCV.dequeueReusableCell(withReuseIdentifier: "AdditionalLatteCVCell", for: indexPath) as! AdditionalLatteCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.foamNotRecommendnameLbl.text = latteDetailsArray[1].NameEn!
                cell.nameLbl.text = latteDetailsArray[1].Additionals![indexPath.row].NameEn
            }else{
                self.foamNotRecommendnameLbl.text = latteDetailsArray[1].NameAr!
                cell.nameLbl.text = latteDetailsArray[1].Additionals![indexPath.row].NameAr
            }
            
            let ImgStr:NSString = "\(displayImages.images.path())\(latteDetailsArray[1].Additionals![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                let pricearray =  latteDetailsArray[1].Additionals![indexPath.row].AdditionalPrices!.filter({$0.Price > 0})
                if pricearray.count > 0{
                    cell.priceLbl.text = "\(latteDetailsArray[1].Additionals![indexPath.row].AdditionalPrices![0].Price.withCommas())"
                }else{
                    cell.priceLbl.text = "0.00"
                }
                cell.priceLblHeight.constant = 22
                cell.priceLblBottomConstraint.constant = 5
            }else{
                cell.priceLbl.text = ""
                cell.priceLblHeight.constant = 0
                cell.priceLblBottomConstraint.constant = 0
            }
            
            cell.selectBtn.tag = indexPath.row
            cell.selectBtn.addTarget(self, action: #selector(latteSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            if latteDetailsArray[1].Additionals![indexPath.row].ItemSelect! == true{
                cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
            }else{
                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            //Quantity Functionality
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(foamNotRecommendPlusBtnTapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(foamNotRecommendMinusBtnTapped(_:)), for: .touchUpInside)
            if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                cell.quantityViewHeight.constant = 30
                cell.quantityViewTopConstraint.constant = 10
                cell.quantityLbl.text = "\(latteDetailsArray[1].Additionals![indexPath.row].additionalQty!)"
            }else{
                cell.quantityViewHeight.constant = 0
                cell.quantityViewTopConstraint.constant = 0
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bannerCollectionView{
          let name = itemImagesArray[indexPath.row].Image
           let fullNameArr = name.split(separator:".")
           if fullNameArr.count > 1 {
           if fullNameArr[1] == "mp4" {
               let indexPath = NSIndexPath(row: indexPath.row, section: 0)
               let cell = bannerCollectionView.cellForItem(at: indexPath as IndexPath) as! ImagesCell?
               if cell!.playBtn.isHidden == true {
                   cell!.playBtn.isHidden = false
                   cell!.playerView!.player!.pause()
                   cell!.contentView.bringSubviewToFront(cell!.playBtn)
               }
           }
           //else{
   //            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
   //            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
   //            obj.modalPresentationStyle = .overCurrentContext
   //            obj.imageName = name
   //            obj.modalPresentationStyle = .overFullScreen
   //            present(obj, animated: false, completion: nil)
           //}
           }
           //else{
               let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
               let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
               //obj.modalPresentationStyle = .overCurrentContext
               if AppDelegate.getDelegate().appLanguage == "English"{
                   obj.itemName = additionalsArray[0].NameEn
               }else{
                   obj.itemName = additionalsArray[0].NameAr
               }
               obj.itemImagesArray = itemImagesArray
               //obj.modalPresentationStyle = .overFullScreen
               present(obj, animated: false, completion: nil)
           //}
        }else if collectionView == tagsCollectionView{
            if AppDelegate.getDelegate().appLanguage == "English"{
                popUpNameLbl.text = additionalsArray[0].Tags![indexPath.row].NameEn
            }else{
                popUpNameLbl.text = additionalsArray[0].Tags![indexPath.row].NameAr
            }
            let startPoint = CGPoint(x: tagsCollectionView.frame.origin.x+37, y: 220 - myScrollView.contentOffset.y)
            self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 120, height: 40)
            popover.show(self.popUpLblMainView, point: startPoint)
        }else if collectionView == selectBeanCV{
            if additionalsArray[0].Grinders!.count > 1{
                for i in 0...additionalsArray[0].Grinders!.count-1{
                    self.additionalsArray[0].Grinders![i].selectGrinder = false
                }
            }
            grinderItemId = additionalsArray[0].Grinders![indexPath.row].Id
            self.additionalsArray[0].Grinders![indexPath.row].selectGrinder = !additionalsArray[0].Grinders![indexPath.row].selectGrinder!
            selectBeanCV.reloadData()
            if additionalsArray[0].ManualBrew! == true{
                if additionalsArray[0].Grinders![indexPath.row].Price > 0{
                    priceCalculation()
                }
            }
        }else if collectionView == latteCV{
            if self.latteDetailsArray[0].Additionals![indexPath.row].IsOutOfStock! == 0{
                if latteDetailsArray[0].Additionals!.count > 1{
                    for i in 0...latteDetailsArray[0].Additionals!.count-1{
                        latteDetailsArray[0].Additionals![i].ItemSelect = false
                    }
                }
                self.latteDetailsArray[0].Additionals![indexPath.row].ItemSelect = !latteDetailsArray[0].Additionals![indexPath.row].ItemSelect!
                self.latteDetailsArray[0].Additionals![indexPath.row].additionalQty! = 1
                self.priceCalculation()
                latteCV.reloadData()
            }else{
                print("Item is Out Of Stock")
            }
        }else if collectionView == latteNotRecommendCV{
            if self.latteDetailsArray[0].Additionals![indexPath.row].IsOutOfStock! == 0{
                if latteDetailsArray[0].Additionals!.count > 1{
                    for i in 0...latteDetailsArray[0].Additionals!.count-1{
                        latteDetailsArray[0].Additionals![i].ItemSelect = false
                    }
                }
                self.latteDetailsArray[0].Additionals![indexPath.row].ItemSelect = !latteDetailsArray[0].Additionals![indexPath.row].ItemSelect!
                self.latteDetailsArray[0].Additionals![indexPath.row].additionalQty! = 1
                self.priceCalculation()
                latteNotRecommendCV.reloadData()
            }else{
                print("Item is Out Of Stock")
            }
        }else if collectionView == foamCV{
            if self.latteDetailsArray[1].Additionals![indexPath.row].IsOutOfStock! == 0{
                if latteDetailsArray[1].Additionals!.count > 1{
                    for i in 0...latteDetailsArray[1].Additionals!.count-1{
                        latteDetailsArray[1].Additionals![i].ItemSelect = false
                    }
                }
                self.latteDetailsArray[1].Additionals![indexPath.row].ItemSelect = !latteDetailsArray[1].Additionals![indexPath.row].ItemSelect!
                self.latteDetailsArray[1].Additionals![indexPath.row].additionalQty! = 1
                self.priceCalculation()
                foamCV.reloadData()
            }else{
                print("Item is Out Of Stock")
            }
        }else if collectionView == foamNotRecommendCV{
            if self.latteDetailsArray[1].Additionals![indexPath.row].IsOutOfStock! == 0{
                if latteDetailsArray[1].Additionals!.count > 1{
                    for i in 0...latteDetailsArray[1].Additionals!.count-1{
                        latteDetailsArray[1].Additionals![i].ItemSelect = false
                    }
                }
                self.latteDetailsArray[1].Additionals![indexPath.row].ItemSelect = !latteDetailsArray[1].Additionals![indexPath.row].ItemSelect!
                self.latteDetailsArray[1].Additionals![indexPath.row].additionalQty! = 1
                self.priceCalculation()
                foamNotRecommendCV.reloadData()
            }else{
                print("Item is Out Of Stock")
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == selectBeanCV{
            let CVWidth = selectBeanCV.frame.width
            var width = 150.0
            if CVWidth > 330{
                width = Double((CVWidth/4)-5)
            }else{
                width = Double((CVWidth/3)-10)
            }
            //let recommend = additionalsArray[0].Grinders!.filter({$0.Recommended == true})
            if view.frame.width <= 375{
                if additionalsArray[0].ManualBrew! == true{
                    return CGSize(width: width, height: 223)
                }else{
                    return CGSize(width: width, height: 198)
                }
            }else{
                if additionalsArray[0].ManualBrew! == true{
                    return CGSize(width: width, height: 220)
                }else{
                    return CGSize(width: width, height: 195)
                }
            }
            //if recommend.count > 0{
//                if additionalsArray[0].ManualBrew! == true{
//                    return CGSize(width: width, height: 220)
//                }else{
//                    return CGSize(width: width, height: 195)
//                }
//            }else{
//                if additionalsArray[0].ManualBrew! == true{
//                    return CGSize(width: width, height: 192)
//                }else{
//                    return CGSize(width: width, height: 162)
//                }
//            }
        }else if collectionView == foamCV{
            let width = foamMainView.frame.width
            let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 193)
                }else{
                    return CGSize(width: width/4, height: 155)
                }
            }else{
                if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 161)
                }else{
                    return CGSize(width: width/4, height: 121)
                }
            }
        }else if collectionView == foamNotRecommendCV{
            let width = foamNotRecommendMainView.frame.width
            let priceArray =  latteDetailsArray[1].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 193)
                }else{
                    return CGSize(width: width/4, height: 155)
                }
            }else{
                if latteDetailsArray[1].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 161)
                }else{
                    return CGSize(width: width/4, height: 121)
                }
            }
        }else if collectionView == bannerCollectionView{
            print("Width:\(bannerCollectionView.frame.width)")
              return CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
        }else if collectionView == tagsCollectionView{
            return CGSize(width: 40, height: tagsCollectionView.frame.height)
        }else if collectionView == latteCV{
            let width = latteMainView.frame.width
            let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 193)
                }else{
                    return CGSize(width: width/4, height: 153)
                }
            }else{
                if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 161)
                }else{
                    return CGSize(width: width/4, height: 121)
                }
            }
        }else {
            let width = latteNotRecommendMainView.frame.width
            let priceArray =  latteDetailsArray[0].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 193)
                }else{
                    return CGSize(width: width/4, height: 153)
                }
            }else{
                if latteDetailsArray[0].Additionals![indexPath.row].MaxQty > 1{
                    return CGSize(width: width/4, height: 161)
                }else{
                    return CGSize(width: width/4, height: 121)
                }
            }
        }
    }
    @objc func beanSelectBtn_Tapped(_ sender:UIButton){
        if additionalsArray[0].Grinders!.count > 1{
            for i in 0...additionalsArray[0].Grinders!.count-1{
                self.additionalsArray[0].Grinders![i].selectGrinder = false
            }
        }
        grinderItemId = additionalsArray[0].Grinders![sender.tag].Id
        self.additionalsArray[0].Grinders![sender.tag].selectGrinder = !additionalsArray[0].Grinders![sender.tag].selectGrinder!
        selectBeanCV.reloadData()
        if additionalsArray[0].ManualBrew! == true{
            if additionalsArray[0].Grinders![sender.tag].Price > 0{
                priceCalculation()
            }
        }
    }
    @objc func beanInfoBtn_Tapped(_ sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpTwoVC") as! ImagePopUpTwoVC
        obj.modalPresentationStyle = .overCurrentContext
        if additionalsArray[0].Grinders!.count > 0{
            if additionalsArray[0].Grinders![sender.tag].Image != ""{
                obj.imageName = additionalsArray[0].Grinders![sender.tag].Image
            }else{
                obj.imageName = ""
            }
        }else{
            obj.imageName = ""
        }
        
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    @objc func latteSelectBtn_Tapped(_ sender:UIButton){
        if self.latteDetailsArray[0].Additionals![sender.tag].IsOutOfStock! == 0{
            if latteDetailsArray[0].Additionals!.count > 1{
                for i in 0...latteDetailsArray[0].Additionals!.count-1{
                    latteDetailsArray[0].Additionals![i].ItemSelect = false
                }
            }
            self.latteDetailsArray[0].Additionals![sender.tag].ItemSelect = !latteDetailsArray[0].Additionals![sender.tag].ItemSelect!
            self.latteDetailsArray[0].Additionals![sender.tag].additionalQty! = 1
            self.priceCalculation()
            latteCV.reloadData()
        }else{
            print("Item is Out Of Stock")
        }
    }
    @objc func foamSelectBtn_Tapped(_ sender:UIButton){
//        if foamDetailsArray[0].Additionals!.count > 1{
//             for i in 0...foamDetailsArray[0].Additionals!.count-1{
//                 foamDetailsArray[0].Additionals![i].ItemSelect = false
//             }
//         }
//        self.foamDetailsArray[0].Additionals![sender.tag].ItemSelect = !foamDetailsArray[0].Additionals![sender.tag].ItemSelect!
        if self.latteDetailsArray[1].Additionals![sender.tag].IsOutOfStock! == 0{
            if latteDetailsArray[1].Additionals!.count > 1{
                for i in 0...latteDetailsArray[1].Additionals!.count-1{
                    latteDetailsArray[1].Additionals![i].ItemSelect = false
                }
            }
            self.latteDetailsArray[1].Additionals![sender.tag].ItemSelect = !latteDetailsArray[1].Additionals![sender.tag].ItemSelect!
            self.latteDetailsArray[1].Additionals![sender.tag].additionalQty! = 1
            self.priceCalculation()
            foamCV.reloadData()
        }else{
            print("Item is Out Of Stock")
        }
    }
    //MARK: Latte cell Button Actions
    @objc func lattePlusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[0].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[0].Additionals![sender.tag].MaxQty > latteDetailsArray[0].Additionals![sender.tag].additionalQty!{
                latteDetailsArray[0].Additionals![sender.tag].additionalQty! = latteDetailsArray[0].Additionals![sender.tag].additionalQty! + 1
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].MaxQty)")
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].NameEn)")
        }
        latteCV.reloadData()
    }
    @objc func latteMinusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[0].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[0].Additionals![sender.tag].additionalQty! == 1{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
            }else{
                latteDetailsArray[0].Additionals![sender.tag].additionalQty! = latteDetailsArray[0].Additionals![sender.tag].additionalQty! - 1
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].NameEn)")
        }
        latteCV.reloadData()
    }
    //MARK: Latte Not Recommend cell Button Actions
    @objc func latteNotRecommendPlusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[0].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[0].Additionals![sender.tag].MaxQty > latteDetailsArray[0].Additionals![sender.tag].additionalQty!{
                latteDetailsArray[0].Additionals![sender.tag].additionalQty! = latteDetailsArray[0].Additionals![sender.tag].additionalQty! + 1
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].MaxQty)")
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].NameEn)")
        }
        latteNotRecommendCV.reloadData()
    }
    @objc func latteNotRecommendMinusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[0].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[0].Additionals![sender.tag].additionalQty! == 1{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
            }else{
                latteDetailsArray[0].Additionals![sender.tag].additionalQty! = latteDetailsArray[0].Additionals![sender.tag].additionalQty! - 1
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[0].Additionals![sender.tag].NameEn)")
        }
        latteNotRecommendCV.reloadData()
    }
    //MARK: Foam cell Button Actions
    @objc func foamNotRecommendPlusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[1].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[1].Additionals![sender.tag].MaxQty > latteDetailsArray[1].Additionals![sender.tag].additionalQty!{
                latteDetailsArray[1].Additionals![sender.tag].additionalQty! = latteDetailsArray[1].Additionals![sender.tag].additionalQty! + 1
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].MaxQty)")
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].NameEn)")
        }
        foamNotRecommendCV.reloadData()
    }
    @objc func foamNotRecommendMinusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[1].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[1].Additionals![sender.tag].additionalQty! == 1{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
            }else{
                latteDetailsArray[1].Additionals![sender.tag].additionalQty! = latteDetailsArray[1].Additionals![sender.tag].additionalQty! - 1
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].NameEn)")
        }
        foamNotRecommendCV.reloadData()
    }
    //MARK: Foam cell Button Actions
    @objc func foamPlusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[1].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[1].Additionals![sender.tag].MaxQty > latteDetailsArray[1].Additionals![sender.tag].additionalQty!{
                latteDetailsArray[1].Additionals![sender.tag].additionalQty! = latteDetailsArray[1].Additionals![sender.tag].additionalQty! + 1
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].MaxQty)")
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].NameEn)")
        }
        foamCV.reloadData()
    }
    @objc func foamMinusBtnTapped(_ sender: UIButton){
        if latteDetailsArray[1].Additionals![sender.tag].ItemSelect! == true{
            if latteDetailsArray[1].Additionals![sender.tag].additionalQty! == 1{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
            }else{
                latteDetailsArray[1].Additionals![sender.tag].additionalQty! = latteDetailsArray[1].Additionals![sender.tag].additionalQty! - 1
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(latteDetailsArray[1].Additionals![sender.tag].NameEn)")
        }
        foamCV.reloadData()
    }
    @objc func playBtnTapped(_ sender: UIButton){
        print(sender.tag)
        let name = itemImagesArray[sender.tag].Image
         let fullNameArr = name.split(separator:".")
         if fullNameArr.count > 1 {
         if fullNameArr[1] == "mp4" {
             let indexPath = NSIndexPath(row: sender.tag, section: 0)
             let cell = bannerCollectionView.cellForItem(at: indexPath as IndexPath) as! ImagesCell?
             if cell!.playBtn.isHidden == true {
                 cell!.playBtn.isHidden = false
                 cell!.playerView!.player!.pause()
                 cell!.contentView.bringSubviewToFront(cell!.playBtn)
             }else{
                 cell!.playBtn.isHidden = true
                 cell?.playerView!.player!.play()
                 cell!.contentView.bringSubviewToFront(cell!.playBtn)
             }
         }
      }
    }
    //Banner auto scrool
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == bannerCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.bannerCollectionView.indexPathsForVisibleItems
            indexes.sort()
            var index = indexes.first!
            let name = itemImagesArray[index.row].Image
             let fullNameArr = name.split(separator:".")
             if fullNameArr.count > 1 {
             if fullNameArr[1] == "mp4" {
                let cell = bannerCollectionView.cellForItem(at: index as IndexPath) as! ImagesCell?
                 if cell!.playBtn.isHidden == true {
                     cell!.playBtn.isHidden = false
                     cell!.playerView!.player!.pause()
                    cell!.contentView.bringSubviewToFront(cell!.playBtn)

                 }
             }
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row - 1
                }else{
                    index.row = index.row + 1
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
            }
            
            if itemImagesArray.count > index.row {
                DispatchQueue.main.async {
                    self.bannerCollectionView.scrollToItem(at: index, at: .right, animated: true )
                }
            }
            DispatchQueue.main.async {
                self.bannerPageController.currentPage = index.row
            }
        }
    }
    }
}
@available(iOS 13.0, *)
extension BeverageVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        self.selectNutrition = name
    }
}
@available(iOS 13.0, *)
extension BeverageVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if (string == " ") && range.location == 0
        {
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")

        if (textField.textInputMode?.primaryLanguage == "emoji") {
            return false
        }
        if string.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.safelyLimitedTo(length: 250)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
@available(iOS 13.0, *)
extension String {
var containsValidCharacter: Bool {
    guard self != "" else { return true }
    let hexSet = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ")
    let newSet = CharacterSet(charactersIn: self)
    return hexSet.isSuperset(of: newSet)
  }
}
