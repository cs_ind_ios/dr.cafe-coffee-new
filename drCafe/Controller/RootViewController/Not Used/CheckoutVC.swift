//
//  CheckoutVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 19/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class CheckoutVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var addMoreBtn: UIButton!
    @IBOutlet weak var checkoutBtn: UIButton!
    
    var isfirst:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func checkoutBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ConfirmationVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func addMoreBtn_Tapped(_ sender: Any) {
    }
}
@available(iOS 13.0, *)
extension CheckoutVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "CheckOutTVCell", bundle: nil), forCellReuseIdentifier: "CheckOutTVCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckOutTVCell", for: indexPath) as! CheckOutTVCell
        cell.layoutIfNeeded()
        tableViewHeight.constant = tableView.contentSize.height
        return cell
    }
}
