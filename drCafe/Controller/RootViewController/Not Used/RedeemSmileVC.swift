//
//  RedeemSmileVC.swift
//  drCafe
//
//  Created by Devbox on 03/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

//MARK: RedeemSmile Delegate Protocol
protocol RedeemSmileVCDelegate {
    func didTapReedem()
}

@available(iOS 13.0, *)
class RedeemSmileVC: UIViewController {

    @IBOutlet weak var smileBadgeImg: UIImageView!
    @IBOutlet weak var redeemSmileTitleLbl: UILabel!
    @IBOutlet weak var redeemDicLbl: UILabel!
    @IBOutlet weak var redeemPointsLbl: UILabel!
    @IBOutlet weak var mySmileNameLbl: UILabel!
    @IBOutlet weak var redeemAmountLbl: UILabel!
    @IBOutlet weak var redeemNowBtn: UIButton!
    
    var redeemPoints:Double!
    var redeemSmiles:Double!
    var redeemSmileVCDelegate:RedeemSmileVCDelegate!
    var badgeNum:Int!
    var minWalletAmount:Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        mySmileNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Points", value: "", table: nil))!
        redeemSmileTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem Smiles", value: "", table: nil))!
        redeemDicLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem Smile Disc", value: "", table: nil))!
        redeemNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem Now", value: "", table: nil))!, for: .normal)
        
        redeemPointsLbl.text = "\(redeemSmiles!.withCommas())"
        redeemAmountLbl.text = "\(redeemPoints!.withCommas())"
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            if badgeNum == 1{
                smileBadgeImg.image = UIImage(named: "smile_redeem_standard")
            }else if badgeNum == 2{
                smileBadgeImg.image = UIImage(named: "smile_redeem_platinum")
            }else if badgeNum == 3{
                smileBadgeImg.image = UIImage(named: "smile_redeem_gold")
            }else{
                smileBadgeImg.image = UIImage(named: "smile_redeem_elite")
            }
        }else{
            if badgeNum == 1{
                self.smileBadgeImg.image = UIImage(named: "smile_standard")
            }else if badgeNum == 2{
                self.smileBadgeImg.image = UIImage(named: "smile_platinum")
            }else if badgeNum == 3{
                self.smileBadgeImg.image = UIImage(named: "smile_gold")
            }else{
                self.smileBadgeImg.image = UIImage(named: "smile_elite")
            }
        }
    }
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func redeemNowBtn_Tapped(_ sender: Any) {
        if redeemSmiles >= minWalletAmount{
            let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2]
            OrderModuleServices.ConvertPointsToPayBalanceService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.redeemSmileVCDelegate.didTapReedem()
                    self.dismiss(animated: true, completion: nil)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "Loyalty points are less")
        }
    }
}
