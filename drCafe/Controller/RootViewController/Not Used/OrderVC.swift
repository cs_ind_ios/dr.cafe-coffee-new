//
//  OrderVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 17/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class OrderVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var whereObj:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if whereObj == 1{
            backBtn.isHidden = false
            tabBarController?.tabBar.isHidden = true
        }else{
            backBtn.isHidden = true
            self.tabBarController?.tabBar.isHidden = false
        }
        view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Order Button Action
    @IBAction func orderBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Schedule Order Button Action
    @IBAction func scheduleBtn_Tapped(_ sender: Any) {
    }
    //MARK: Favourite Order Button Action
    @IBAction func favouriteBtn_Tapped(_ sender: Any) {
    }
    //MARK: Order History Button Action
    @IBAction func orderHistoryBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = TrackOrderVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension OrderVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "OrderVCCVCell", bundle: nil), forCellWithReuseIdentifier: "OrderVCCVCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderVCCVCell", for: indexPath) as! OrderVCCVCell
        cell.bannerImg.image = UIImage(named: "iced")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize(width: width, height: height)
    }
}
