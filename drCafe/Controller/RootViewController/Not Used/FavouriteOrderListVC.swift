//
//  FavouriteOrderListVC.swift
//  drCafe
//
//  Created by Devbox on 03/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class FavouriteOrderListVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var pageNumber:Int = 1
    var totalRows:Int = 0
    private let refreshControl = UIRefreshControl()
    var mainArray = [OrderHistoryDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 120
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        navigationBarTitleLbl.text = FavoriteOrder
         
        DispatchQueue.main.async {
            SaveAddressClass.getOrderHistoryArray.removeAll()
            self.getFavOrderHistoryService()
        }
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Refresh Data
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        self.getFavOrderHistoryService()
    }
    //MARK: Get Favourite Order History Service
    func getFavOrderHistoryService(){
        if pageNumber == 1 {
            ANLoader.showLoading("", disableUI: false)
        }
        let dic:[String : Any] = ["userId": UserDef.getUserId(),
        "Pageno": "\(pageNumber)",
        "Pagesize": "10", "Favourite": true, "RequestBy":2]

        CartModuleServices.OrderHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.navigationController?.popViewController(animated: true)
                return
            }else{
                if self.pageNumber == 1 {
                    SaveAddressClass.getOrderHistoryArray.removeAll()
                }
                
                if data.Orders!.count > 0{
                    for i in 0...data.Orders!.count - 1{
                        SaveAddressClass.getOrderHistoryArray.append(data.Orders![i])
                    }
                }
                    
                self.totalRows = data.TotalOrders
                self.mainArray.removeAll()
                self.mainArray = SaveAddressClass.getOrderHistoryArray
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension FavouriteOrderListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if totalRows > self.mainArray.count {
            return self.mainArray.count + 1
        }else{
            return self.mainArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.mainArray.count == indexPath.row {
           return 50
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
            self.tableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
            cell.activityIndicatorView.startAnimating()
            cell.selectionStyle = .none
            return cell
        }else{
            self.tableView.register(UINib(nibName:"FavouriteOrderListTVCell", bundle: nil), forCellReuseIdentifier:"FavouriteOrderListTVCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteOrderListTVCell", for: indexPath) as! FavouriteOrderListTVCell
            
            let dic = self.mainArray[indexPath.row]
            if dic.SectionType == 2{
                let expectDate = dic.ExpectedTime.components(separatedBy: "T")[0]
                cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .dateR)
            }else{
                let expectDate = dic.ExpectedTime.components(separatedBy: ".")[0]
                cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
            }
            
            cell.favouriteBtn.setImage(UIImage(named: "fillStar"), for: .normal)
            cell.nameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Test", value: "", table: nil))!
            cell.amountLbl.text = "\(dic.NetPrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"

            if dic.DeliveryAddress != nil{
                cell.addressLbl.text = dic.DeliveryAddress![0].Details
            }else{
                cell.addressLbl.text = dic.Store![0].StoreAddressEn
            }

            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                if self.mainArray.count == indexPath.row+1 {
                    if totalRows >  self.mainArray.count {
                        pageNumber = pageNumber + 1
                        self.getFavOrderHistoryService()
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = TrackOrderVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
            obj.orderId = mainArray[indexPath.row].OrderId
            obj.orderFrom = 2
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
