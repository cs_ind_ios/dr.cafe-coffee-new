//
//  QuickShareVC.swift
//  drCafe
//
//  Created by Devbox on 03/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class QuickShareVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var shareCodeLbl: UILabel!
    @IBOutlet weak var shareAndGetCoffeeNameLbl: UILabel!
    @IBOutlet weak var shareOurAppNameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    var imgsArray = ["Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center","Call Center"]
    var shareCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //collectionView.delegate = self
        //collectionView.dataSource = self
        self.tabBarController?.tabBar.isHidden = true
        if isUserLogIn() == true{
            getDetailsService()
        }else{
            self.shareCodeLbl.text = ""
        }
        
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            shareOurAppNameLbl.textColor = .white
            shareAndGetCoffeeNameLbl.textColor = .white
            descLbl.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        } else {
            // User Interface is Light
            shareOurAppNameLbl.textColor = .black
            shareAndGetCoffeeNameLbl.textColor = .black
            descLbl.textColor = .darkGray
        }
    }
    func getDetailsService(){
        let dic:[String:Any] = ["Userid": UserDef.getUserId(), "RequestBy":2]
        UserModuleServices.shareAppService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
//                let activityViewController = UIActivityViewController(activityItems: [data.Data!.Message, "Referal Code :\(data.Data!.ReferalCode)"], applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = self.view
//                self.present(activityViewController, animated: true, completion: nil)
                self.shareCodeLbl.text = "#\(data.Data!.ReferalCode)"
                self.shareCode = "\(data.Data!.ReferalCode)"
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async() {
            let Desc = "I'm really enjoying variety of Coffee's at dr.Cafe Think you might enjoy it too.\nأنا أستمتع حقًا بمجموعة متنوعة من القهوة في dr.Cafe أعتقد أنك قد تست\n ReferalCode : \(self.shareCode)"
            let shareAll = [Desc] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    //MARK: Invite Friend Button Action
    @IBAction func inviteBtn_Tapped(_ sender: Any) {
//        if let urlStr = NSURL(string: "https://itunes.apple.com/in/app/dr.cafe-coffee/id1083344883?mt=8") {
//            let objectsToShare = [urlStr]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    //MARK: How Invitw works Button Action
    @IBAction func howInvitesWorksBtn_Tapped(_ sender: Any) {
    }
    //MARK: FAQ Button Action
    @IBAction func FAQBtn_Tapped(_ sender: Any) {
    }
    //MARK: Terms and Conditions Button Action
    @IBAction func termsAndCondBtn_Tapped(_ sender: Any) {
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension QuickShareVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "StoreImagesCVCell", bundle: nil), forCellWithReuseIdentifier: "StoreImagesCVCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreImagesCVCell", for: indexPath) as! StoreImagesCVCell
        cell.storeImg.image = UIImage(named: imgsArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 64, height: 64)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}
