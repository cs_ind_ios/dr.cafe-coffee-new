//
//  CateringItemDetailsVC.swift
//  drCafe
//
//  Created by Devbox on 21/05/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class CateringItemDetailsVC: UIViewController {

    @IBOutlet weak var rocketBaristaTitleLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var servesLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var coffeeCollectionView: UICollectionView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var sunDateLbl: UILabel!
    @IBOutlet weak var monDateLbl: UILabel!
    @IBOutlet weak var tueDateLbl: UILabel!
    @IBOutlet weak var wedDateLbl: UILabel!
    @IBOutlet weak var thuDateLbl: UILabel!
    @IBOutlet weak var friDateLbl: UILabel!
    @IBOutlet weak var satDateLbl: UILabel!
    @IBOutlet weak var fromTimeLbl: UILabel!
    @IBOutlet weak var toTimeLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var productCountLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coffeeCollectionView.delegate = self
        coffeeCollectionView.dataSource = self
        coffeeCollectionView.register(UINib(nibName: "CoffeeBlendCVCell", bundle: nil), forCellWithReuseIdentifier: "CoffeeBlendCVCell")
        
        tabBarController?.tabBar.isHidden = true
    }
    @IBAction func backBtnTapped(_ sender: Any) {
    }
    @IBAction func addInfoBtnTapped(_ sender: Any) {
    }
    @IBAction func rocketBaristaBackBtn(_ sender: Any) {
    }
    @IBAction func incrementBtnTapped(_ sender: Any) {
    }
    @IBAction func decrementBtnTapped(_ sender: Any) {
    }
    @IBAction func addBtnTapped(_ sender: Any) {
    }
    @IBAction func checkOutBtnTapped(_ sender: Any) {
    }
}
extension CateringItemDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = coffeeCollectionView.dequeueReusableCell(withReuseIdentifier: "CoffeeBlendCVCell", for: indexPath) as! CoffeeBlendCVCell
        cell.coffeeImageView.image = UIImage(named: "coffee")
        cell.coffeeTypeLbl.text = "Colombian"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionwidth = coffeeCollectionView.bounds.width
        return CGSize(width: collectionwidth/4, height: collectionwidth/4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 2, bottom: 0, right: 2)
    }
}
