//
//  FeedBackVC.swift
//  drCafe
//
//  Created by Devbox on 15/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class FeedBackVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dicLbl: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var overallExpQualityNameLbl: UILabel!
    @IBOutlet weak var overallExpQualityRateView: FloatRatingView!
    
    @IBOutlet weak var noteDicLbl: UILabel!
    @IBOutlet weak var noteTV: UITextView!
    @IBOutlet weak var BGView: UIView!
    
    var selectionArray = [false,false,false,false]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackHeaderTVCell", value: "", table: nil))!)
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackSubTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackSubTVCell", value: "", table: nil))!)
        
        tableView.delegate = self
        tableView.dataSource = self
        self.noteTV.delegate = self
        
        self.noteTV.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.noteTV.layer.borderWidth = 1.0
        self.noteTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!
        self.noteTV.textColor = UIColor.lightGray
        
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            self.BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            navigationBarTitleLbl.textColor = .white
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
        } else {
            // User Interface is Light
            self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.BGView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9490196078, alpha: 1)
            navigationBarTitleLbl.textColor = .black
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension FeedBackVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectionArray[section] == false{
            return 1
        }else{
            return 2
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 55
        }else{
            return 190
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0{
             //MARK: Items Header Details
            let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackHeaderTVCell", value: "", table: nil))!, for: indexPath) as! FeedBackHeaderTVCell
            //headerCell.headerNameLbl.text = ""
            if selectionArray[indexPath.section] == false{
                headerCell.downArrowBtn.setImage(UIImage(named: "Path 3 Copy 6"), for: .normal)
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    headerCell.downArrowBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                }else{
                    headerCell.downArrowBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                }
            }
            cell = headerCell
        }else{
             //MARK: Items Details
            let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FeedBackSubTVCell", value: "", table: nil))!, for: indexPath) as! FeedBackSubTVCell
            cell = subCell
        }
        
        DispatchQueue.main.async {
            self.tableView.layoutIfNeeded()
            self.tableViewHeight.constant = self.tableView.contentSize.height
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //Section Expand code
            selectionArray[indexPath.section] = !selectionArray[indexPath.section]
            tableView.reloadSections([indexPath.section], with: .automatic)
        }else{
            print(indexPath.row)
        }
    }
}
//MARK: TextView Delegate
@available(iOS 13.0, *)
extension FeedBackVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
