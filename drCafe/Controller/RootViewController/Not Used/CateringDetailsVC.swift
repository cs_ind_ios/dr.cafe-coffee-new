//
//  CateringDetailsVC.swift
//  drCafe
//
//  Created by Devbox on 25/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class CateringDetailsVC: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var standardPkgBtn: UILabel!
    @IBOutlet weak var itsAlwaysLbl: UILabel!
    @IBOutlet weak var standardPkgImg: UIImageView!
    
    @IBOutlet weak var eventPreparationLbl: UILabel!
    @IBOutlet weak var electricityLbl: UILabel!
    @IBOutlet weak var phaseLbl: UILabel!
    @IBOutlet weak var AMPLbl: UILabel!
    @IBOutlet weak var setupTimeLbl: UILabel!
    @IBOutlet weak var hoursLbl: UILabel!
    @IBOutlet weak var setupSpaceLbl: UILabel!
    @IBOutlet weak var multiplyLbl: UILabel!
    @IBOutlet weak var meterLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var reservedLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var khuraisRoadLbl: UILabel!
    @IBOutlet weak var abiAlArabLbl: UILabel!
    @IBOutlet weak var noOfGuestLbl: UILabel!
    
    @IBOutlet weak var thisPkgLbl: UILabel!
    @IBOutlet weak var noOfGuestCountLbl: UILabel!
    
     // no of guest view elements
    @IBOutlet weak var noOfGuestLbl1: UILabel!
    @IBOutlet weak var noOfGuestsNumberLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    
    @IBOutlet weak var servingTimeLbl: UILabel!
    @IBOutlet weak var seringHoursLbl: UILabel!

    @IBOutlet weak var noOfBaristaLbl: UILabel!
    @IBOutlet weak var indoorLbl: UILabel!
    @IBOutlet weak var ootdoorLbl: UILabel!

    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var coffeeLatteLbl: UILabel!
    
    @IBOutlet weak var cappuccinoLbl: UILabel!
    @IBOutlet weak var coffeeMochaLbl: UILabel!
    @IBOutlet weak var caramelMacchiatoLatteLbl: UILabel!
    @IBOutlet weak var coffeeAmericanoLbl: UILabel!
    @IBOutlet weak var flatWhiteLbl: UILabel!
    @IBOutlet weak var extentCountLbl: UILabel!
    
    // TableView OutLets
    @IBOutlet weak var pastryTable: UITableView!
    @IBOutlet weak var baristaTable: UITableView!
    @IBOutlet weak var accessoriesTable: UITableView!
    @IBOutlet weak var complementaryImageView: UIImageView!
    @IBOutlet weak var rollUpBannerAmountLbl: UILabel!
    @IBOutlet weak var snapChatAmountLbl: UILabel!
    @IBOutlet weak var snapChatImage1: UIImageView!
    @IBOutlet weak var snapChatImage2: UIImageView!
    @IBOutlet weak var noteTextview: UITextView!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var delivertChargeLbl: UILabel!
    @IBOutlet weak var vatLbl: UIView!
    @IBOutlet weak var couponDiscounLbl: UIView!
    @IBOutlet weak var netAmountLbl: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pastryTable.delegate = self
        pastryTable.dataSource = self
        
        baristaTable.delegate = self
        baristaTable.dataSource = self
        
        accessoriesTable.delegate = self
        accessoriesTable.dataSource = self
        
        pastryTable.register(UINib(nibName: "AddPastryTVCell", bundle: nil), forCellReuseIdentifier: "AddPastryTVCell")
        baristaTable.register(UINib(nibName: "AddBaristaTVCell", bundle: nil), forCellReuseIdentifier: "AddBaristaTVCell")
        accessoriesTable.register(UINib(nibName: "AddAccessoriesTVCell", bundle: nil), forCellReuseIdentifier: "AddAccessoriesTVCell")
        
        tabBarController?.tabBar.isHidden = true
    }
    @IBAction func titleButtonBtn(_ sender: Any) {
    }
    @IBAction func reservedDotBtn(_ sender: Any) {
    }
    @IBAction func mapBtn(_ sender: Any) {
    }
    @IBAction func noOfGuestAddBtn(_ sender: Any) {
    }
    @IBAction func noOfGuestMinusBtn(_ sender: Any) {
    }
    @IBAction func indoorBtnTapped(_ sender: Any) {
    }
    @IBAction func outdoorBtnTapped(_ sender: Any) {
    }
    @IBAction func uploadBtnTapped(_ sender: Any) {
    }
    @IBAction func menuDownloadBtnTapped(_ sender: Any) {
    }
    @IBAction func extentHoursIncrementTapped(_ sender: Any) {
    }
    @IBAction func extentHoursDecrementTapped(_ sender: Any) {
    }
    @IBAction func complementaryBtnTapped(_ sender: Any) {
    }
    @IBAction func complementaryAttachmentTapped(_ sender: Any) {
    }
    @IBAction func complementaryNoteTapped(_ sender: Any) {
    }
    @IBAction func rollUpBannerBtnTapped(_ sender: Any) {
    }
    @IBAction func rollUpBannerAttachementTapped(_ sender: Any) {
    }
    @IBAction func rollUpBannerNoteBtnTapped(_ sender: Any) {
    }
    @IBAction func snapChatFilterBtnTapped(_ sender: Any) {
    }
    @IBAction func snapChatFilterAttachTapped(_ sender: Any) {
    }
    @IBAction func snapChatFilterNoteTapped(_ sender: Any) {
    }
    @IBAction func billAmountBtnTapped(_ sender: Any) {
    }
    @IBAction func addBtnTapped(_ sender: Any) {
    }
    @IBAction func checkOutBtnTapped(_ sender: Any) {
    }
}
extension CateringDetailsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == pastryTable{
            return 2
        }else if tableView == baristaTable{
            return 2
        }else{
            return 2
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == pastryTable{
            let cell  = pastryTable.dequeueReusableCell(withIdentifier: "AddPastryTVCell", for: indexPath) as! AddPastryTVCell
            cell.priceLbl.text = "200"
            
            return cell
        }else if tableView == baristaTable{
            let cell1  = baristaTable.dequeueReusableCell(withIdentifier: "AddBaristaTVCell", for: indexPath) as! AddBaristaTVCell
            cell1.baristaCountLbl.text = "300"
            return cell1
        }else{
            let cell2  = accessoriesTable.dequeueReusableCell(withIdentifier: "AddAccessoriesTVCell", for: indexPath) as! AddAccessoriesTVCell
            cell2.itemPriceLbl.text = "500"
            return cell2
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == pastryTable{
            return 121
        }else if tableView == baristaTable{
            return 66
        }else{
            return 121
        }
    }
}
