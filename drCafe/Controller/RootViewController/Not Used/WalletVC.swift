//
//  WalletVC.swift
//  drCafe
//
//  Created by Devbox on 11/01/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class WalletVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var walletImg: UIImageView!
    @IBOutlet weak var myRewardAmountLbl: UILabel!
    @IBOutlet weak var myRewardNameLbl: UILabel!
    @IBOutlet weak var cardAmountLbl: UILabel!
    @IBOutlet weak var drCafeCardNameLbl: UILabel!
    @IBOutlet var SARLbl: [UILabel]!
    @IBOutlet weak var myGiftAmountLbl: UILabel!
    @IBOutlet weak var myGiftNameLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var loyaltyPointsView: UIView!
    @IBOutlet weak var loyaltyPointsNameLbl: UILabel!
    @IBOutlet weak var loyaltyPointsImg: UIImageView!
    @IBOutlet weak var drCafeCardView: UIView!
    @IBOutlet weak var drCafeNameLbl: UILabel!
    @IBOutlet weak var drCafeCardImg: UIImageView!
    @IBOutlet weak var walletHistoryView: UIView!
    @IBOutlet weak var walletHistoryNameLbl: UILabel!
    @IBOutlet weak var walletHistoryImg: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var detailsArray:[[String:Any]]!
    var cardsArray:[[String:Any]]!
    var coffeeRewardsArray:[[String:Any]] = [["Name":"Achive 750 points 10ASR","isExpand":false,"Image":"Online Payment"]]
    var giftsArray:[[String:Any]] = [["Name":"dr.CAFE-222 'Free Drink'","isExpand":false,"Image":"Online Payment"]]
    var partnerRewardsArray:[[String:Any]] = [["Name":"Al Rajhi Bank Bonus","isExpand":false,"Image":"Online Payment"], ["Name":"Emirate Skywards","isExpand":false,"Image":"Online Payment"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        detailsArray = [["Name":"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Cards", value: "", table: nil))!)","isExpand":false,"Image":"Online Payment"], ["Name":"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Gift", value: "", table: nil))!)","isExpand":false,"Image":"Online Payment"], ["Name":"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE COFFEE Rewards", value: "", table: nil))!)","isExpand":false,"Image":"Online Payment"], ["Name":"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Our Partner Rewards", value: "", table: nil))!)","isExpand":false,"Image":"Online Payment"]]
        cardsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE CARD", value: "", table: nil))!,"isExpand":false,"Image":"Online Payment"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Credit Card", value: "", table: nil))!,"isExpand":false,"Image":"Online Payment"]]
        
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            navigationBarTitleLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = .white
            myScrollView.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
            navigationBarTitleLbl.textColor = .black
        }
    }
    @IBAction func loyaltyPointsBtn_Tapped(_ sender: Any) {
    }
    @IBAction func drCafeCardBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ManageCardsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func walletHistoryBtn_Tapped(_ sender: Any) {
    }
}
@available(iOS 13.0, *)
extension WalletVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return detailsArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if detailsArray[section]["isExpand"] as! Bool == false{
            return 1
        }else{
            if section == 0{
                return cardsArray.count + 1
            }else if section == 1{
                return giftsArray.count + 1
            }else if section == 2{
                return coffeeRewardsArray.count + 1
            }else if section == 3{
                return partnerRewardsArray.count + 1
            }else{
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0{
             //MARK: Items Header Details
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!)
            let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
            
            headerCell.selectImg.isHidden = true
            headerCell.selectBtn.isHidden = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                headerCell.nameLbl.text = (detailsArray[indexPath.section]["Name"] as! String)
            }else{
                headerCell.nameLbl.text = (detailsArray[indexPath.section]["Name"] as! String)
            }
            headerCell.img.image = UIImage(named: (detailsArray[indexPath.section]["Image"] as! String))
            if detailsArray[indexPath.section]["isExpand"] as! Bool == false{
                headerCell.selectBtn.setImage(UIImage(named: "plus Ash"), for: .normal)
            }else{
                headerCell.selectBtn.setImage(UIImage(named: "minus Ash"), for: .normal)
            }
            headerCell.BGView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell = headerCell
        }else{
             //MARK: Items Details
            let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
            subCell.selectImg.isHidden = true
            subCell.selectBtn.isHidden = false
            if indexPath.section == 0{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = (cardsArray[indexPath.row - 1]["Name"] as! String)
                }else{
                    subCell.nameLbl.text = (cardsArray[indexPath.row - 1]["Name"] as! String)
                }
                subCell.img.image = UIImage(named: (cardsArray[indexPath.row - 1]["Image"] as! String))
            }else if indexPath.section == 1{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = (giftsArray[indexPath.row - 1]["Name"] as! String)
                }else{
                    subCell.nameLbl.text = (giftsArray[indexPath.row - 1]["Name"] as! String)
                }
                subCell.img.image = UIImage(named: (giftsArray[indexPath.row - 1]["Image"] as! String))
            }else if indexPath.section == 2{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = (coffeeRewardsArray[indexPath.row - 1]["Name"] as! String)
                }else{
                    subCell.nameLbl.text = (coffeeRewardsArray[indexPath.row - 1]["Name"] as! String)
                }
                subCell.img.image = UIImage(named: (coffeeRewardsArray[indexPath.row - 1]["Image"] as! String))
            }else if indexPath.section == 3{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = (partnerRewardsArray[indexPath.row - 1]["Name"] as! String)
                }else{
                    subCell.nameLbl.text = (partnerRewardsArray[indexPath.row - 1]["Name"] as! String)
                }
                subCell.img.image = UIImage(named: (partnerRewardsArray[indexPath.row - 1]["Image"] as! String))
            }
            subCell.BGView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                subCell.selectBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                subCell.selectBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
            
            cell = subCell
        }
        DispatchQueue.main.async {
            self.tableViewHeight.constant = tableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //Section Expand code
            if detailsArray[indexPath.section]["isExpand"] as! Bool == true{
                detailsArray[indexPath.section]["isExpand"] = false
                tableView.reloadData()
            }else{
                detailsArray[indexPath.section]["isExpand"] = true
                tableView.reloadData()
            }
        }else{
            if indexPath.section == 0{
                if indexPath.row == 2{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = ManageCardsVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }
    }
}
