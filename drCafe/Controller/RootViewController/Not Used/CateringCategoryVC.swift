//
//  CateringCategoryVC.swift
//  drCafe
//
//  Created by Devbox on 21/05/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class CateringCategoryVC: UIViewController {

    @IBOutlet var cateringLBL: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
    
    var array = ["Bitmap","Bitmap1"]
    var labelArray = ["Rocket Barista","Delivery","Station","Truck"]
    var titleArray = ["Smile More","For coffee lovers"]
    var STArray = ["It's always coffee time","It's always coffee time"]
    var orderArray = ["Order Now","Order Now"]
    var cupArray = ["100 cupof 7ozfor 2 hours","Serving 2 hours of coffee"]
    var priceArray = ["800.00SAR","1200.00SAR"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       tableView.rowHeight = 350
       tableView.delegate = self
       tableView.dataSource = self
       collectionView.delegate = self
       collectionView.dataSource = self
        
        tabBarController?.tabBar.isHidden = true
    }
}
extension CateringCategoryVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CateringCategoryTVCell", for: indexPath) as! CateringCategoryTVCell
        
        cell.imageIMG.image=UIImage(named: array[indexPath.row])
        cell.titleLBL.text = titleArray[indexPath.row]
        cell.subTitleLBL.text = STArray[indexPath.row]
        cell.orderNowLBL.text = orderArray[indexPath.row]
        cell.cupLBL.text = cupArray[indexPath.row]
        cell.priceLBL.text = priceArray[indexPath.row]
        cell.priceLBL.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        
        return cell
    }
}
extension CateringCategoryVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CateringCategoryCVCell", for: indexPath) as! CateringCategoryCVCell
        cell.contentView.backgroundColor = .blue
        cell.nameLbl.text = labelArray[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width/4-6, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(labelArray[indexPath.row])
    }
}
