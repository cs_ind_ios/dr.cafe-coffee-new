//
//  FeaturedListTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 19/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class FeaturedListTVCell: UITableViewCell {

    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var titleDescLbl: UILabel!
    @IBOutlet weak var orderNowBtn: UIButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var calLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var itemDescLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
