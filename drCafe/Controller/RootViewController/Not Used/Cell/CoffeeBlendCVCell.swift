//
//  CoffeeBlendCVCell.swift
//  samplePractice
//
//  Created by mac on 05/05/21.
//

import UIKit

class CoffeeBlendCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var coffeeImageView: UIImageView!
    
    
    @IBOutlet weak var infoBtn: UIButton!
    
    @IBOutlet weak var coffeeTypeLbl: UILabel!
    
    @IBOutlet weak var selectCoffeeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
