//
//  SubScriptionMyOrdersTVCell.swift
//  drCafe
//
//  Created by Devbox on 15/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SubScriptionMyOrdersTVCell: UITableViewCell {

    
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
