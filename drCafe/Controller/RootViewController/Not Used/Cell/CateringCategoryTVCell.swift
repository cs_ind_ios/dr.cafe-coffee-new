//
//  CateringCategoryTVCell.swift
//  drCafe
//
//  Created by Devbox on 21/05/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class CateringCategoryTVCell: UITableViewCell {

    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var subTitleLBL: UILabel!
    @IBOutlet var orderNowLBL: UILabel!
    @IBOutlet var imageIMG: UIImageView!
    @IBOutlet var cupLBL: UILabel!
    @IBOutlet var priceLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
