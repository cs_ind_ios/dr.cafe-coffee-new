//
//  FavouriteOrderListTVCell.swift
//  drCafe
//
//  Created by Devbox on 03/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class FavouriteOrderListTVCell: UITableViewCell {

    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
