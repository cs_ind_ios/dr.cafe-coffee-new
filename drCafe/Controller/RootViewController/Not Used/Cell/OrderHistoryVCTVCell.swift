//
//  OrderHistoryVCTVCell.swift
//  drCafe
//
//  Created by Devbox on 21/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class OrderHistoryVCTVCell: UITableViewCell {

    @IBOutlet weak var topDetailsView: UIView!
    @IBOutlet weak var orderNumNameLbl: UILabel!
    @IBOutlet weak var orderStatusNameLbl: UILabel!
    @IBOutlet weak var quantityNameLbl: UILabel!
    @IBOutlet weak var amountNameLbl: UILabel!
    @IBOutlet weak var invoiceNumLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var priceLblLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var rattingBtn: UIButton!
    @IBOutlet weak var rateView: FloatRatingView!
    @IBOutlet weak var ratingMainView: UIView!
    @IBOutlet weak var ratingMainViewWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
