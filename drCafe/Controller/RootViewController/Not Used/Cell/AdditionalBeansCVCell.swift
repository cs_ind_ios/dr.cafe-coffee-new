//
//  AdditionalBeansCVCell.swift
//  drCafe
//
//  Created by Devbox on 05/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class AdditionalBeansCVCell: UICollectionViewCell {

    @IBOutlet weak var recommendedImg: UIImageView!
    @IBOutlet weak var recommendedImgHeight: NSLayoutConstraint!
    @IBOutlet weak var recommendedTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var beanTypeImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var beanSelectBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceLblHeight: NSLayoutConstraint!
    @IBOutlet weak var priceBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
