//
//  FeedBackHeaderTVCell.swift
//  drCafe
//
//  Created by Devbox on 15/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class FeedBackHeaderTVCell: UITableViewCell {

    @IBOutlet weak var downArrowBtn: UIButton!
    @IBOutlet weak var headerNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
