//
//  AddBaristaTVCell.swift
//  CateringScreen
//
//  Created by mac on 30/04/21.
//

import UIKit

class AddBaristaTVCell: UITableViewCell {
    

    @IBOutlet weak var baristaLbl: UILabel!
    
    @IBOutlet weak var pricePerHourLbl: UILabel!
    @IBOutlet weak var baristaCheckBtn: UIButton!
    
    @IBOutlet weak var baristaPlusBtn: UIButton!
    
    @IBOutlet weak var baristaMinusBtn: UIButton!
    
    @IBOutlet weak var baristaCountLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func baristaCheckBtn_Tapped(_ sender: Any) {
    }
    
    @IBAction func baristaMinusBtn_Tapped(_ sender: Any) {
    }
    
    @IBAction func baristaPlusBtn_Tapped(_ sender: Any) {
    }
    
    
    
    
}
