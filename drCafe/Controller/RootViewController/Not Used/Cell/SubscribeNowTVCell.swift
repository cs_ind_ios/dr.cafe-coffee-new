//
//  SubscribeNowTVCell.swift
//  drCafe
//
//  Created by Devbox on 19/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SubscribeNowTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dicLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var subscribeNowBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
