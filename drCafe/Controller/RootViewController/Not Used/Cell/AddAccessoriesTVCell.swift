//
//  AddAccessoriesTVCell.swift
//  CateringScreen
//
//  Created by mac on 29/04/21.
//

import UIKit

class AddAccessoriesTVCell: UITableViewCell {
    
    @IBOutlet weak var itemSetLbl: UILabel!
    
    @IBOutlet weak var itemPriceLbl: UILabel!
    
    @IBOutlet weak var itemImgLbl: UIImageView!
    
    @IBOutlet weak var itemPlusBtn: UIButton!
    
    @IBOutlet weak var itemMinusBtn: UIButton!
    
    @IBOutlet weak var itemCountLbl: UILabel!
    
    @IBOutlet weak var pricePerSetLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func itemMinusBtn_Tapped(_ sender: Any) {
    }
    
    @IBAction func itemPlusBtn_Tapped(_ sender: Any) {
    }
    
    
}
