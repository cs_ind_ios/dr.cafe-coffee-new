//
//  OrderHistoryVC.swift
//  drCafe
//
//  Created by Devbox on 21/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class OrderHistoryVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var filterDetailsView: UIView!
    @IBOutlet weak var filterDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var dineInBtn: UIButton!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var deliveredBtn: UIButton!
    @IBOutlet weak var CancelledBtn: UIButton!
    @IBOutlet weak var subscriptionBtn: UIButton!
    @IBOutlet weak var filterView: CardView!
    
    static var instance: OrderHistoryVC!
    var pageNumber:Int = 1
    var totalRows:Int = 0
    private let refreshControl = UIRefreshControl()
    var mainArray = [OrderHistoryDetailsModel]()
    var filterArray = [OrderHistoryDetailsModel]()
    var orderType = 0
    var orderStatus = 0
    var favourite = false
    var whereObj = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        OrderHistoryVC.instance = self
        self.tabBarController?.tabBar.isHidden = true
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 200
        self.tableView.delegate = self
        self.tableView.dataSource = self
       
        DispatchQueue.main.async {
            SaveAddressClass.getOrderHistoryArray.removeAll()
            self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
        }
       
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            tableView.semanticContentAttribute = .forceLeftToRight
        }else{
            tableView.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if whereObj == 1{
            self.navigationController?.popViewController(animated: true)
        }
    
        filterDetailsView.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            // User Interface is Dark
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            tableView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
            filterDetailsView.shadowColor1 = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.tableView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9490196078, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
            filterView.backgroundColor = .white
            favouriteBtn.setTitleColor(.darkGray, for: .normal)
            dineInBtn.setTitleColor(.darkGray, for: .normal)
            pickupBtn.setTitleColor(.darkGray, for: .normal)
            deliveredBtn.setTitleColor(.darkGray, for: .normal)
            CancelledBtn.setTitleColor(.darkGray, for: .normal)
            subscriptionBtn.setTitleColor(.darkGray, for: .normal)
        }
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order History", value: "", table: nil))!
        favouriteBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!, for: .normal)
        dineInBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Dine In", value: "", table: nil))!, for: .normal)
        pickupBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!, for: .normal)
        deliveredBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!, for: .normal)
        CancelledBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancelled", value: "", table: nil))!, for: .normal)
        subscriptionBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!, for: .normal)
        
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 11{
            LoginVC.instance.whereObj = 1
        }
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Filter Button Action
    @IBAction func filterBtn_Tapped(_ sender: Any) {
        if filterDetailsView.isHidden == true{
            self.viewSlideInFromTopToBottom(view: filterDetailsView)
            filterDetailsView.isHidden = false
        }else{
            self.viewSlideInFromBottomToTop(view: filterDetailsView)
            filterDetailsView.isHidden = true
        }
    }
    //MARK: Filter Buttons Action
    @IBAction func favouriteBtn_Tapped(_ sender: Any) {
        filterArray = mainArray.filter({$0.Favourite == true})
        self.viewSlideInFromBottomToTop(view: filterDetailsView)
        filterDetailsView.isHidden = true
        pageNumber = 1
        orderType = 0
        orderStatus = 0
        favourite = true
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
    }
    @IBAction func dineInBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromBottomToTop(view: filterDetailsView)
        filterDetailsView.isHidden = true
        pageNumber = 1
        orderType = 1
        orderStatus = 0
        favourite = false
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
    }
    @IBAction func pickupBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromBottomToTop(view: filterDetailsView)
        filterDetailsView.isHidden = true
        pageNumber = 1
        orderType = 3
        orderStatus = 0
        favourite = false
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
    }
    @IBAction func deliveredBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromBottomToTop(view: filterDetailsView)
        filterDetailsView.isHidden = true
        pageNumber = 1
        orderType = 4
        orderStatus = 0
        favourite = false
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
    }
    @IBAction func cancelledBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromBottomToTop(view: filterDetailsView)
        filterDetailsView.isHidden = true
        pageNumber = 1
        orderType = 0
        orderStatus = 4
        favourite = false
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
    }
    @IBAction func subscriptionBtn_Tapped(_ sender: Any) {
        
    }
    //MARK: Refresh Data
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        orderType = 0
        orderStatus = 0
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: false)
    }
    //MARK: Get Order History Data
    func getOrderHistoryService(orderType:Int, orderStatus:Int, favourite:Bool){
        if pageNumber == 1 {
            ANLoader.showLoading("", disableUI: false)
        }
        let dic:[String : Any]!
        if favourite == false{
            dic = ["userId":UserDef.getUserId(),
            "Pageno": "\(pageNumber)",
                "Pagesize": "10", "OrderType":orderType, "OrderStatusId":orderStatus, "RequestBy":2]
        }else{
            dic = ["userId":UserDef.getUserId(),
            "Pageno": "\(pageNumber)",
                "Pagesize": "10", "Favourite": true, "RequestBy":2]
        }
        CartModuleServices.OrderHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
                self.navigationController?.popViewController(animated: true)
                return
            }else{
                if self.pageNumber == 1 {
                    SaveAddressClass.getOrderHistoryArray.removeAll()
                }
            
                if data.Orders!.count > 0{
                    for i in 0...data.Orders!.count - 1{
                        SaveAddressClass.getOrderHistoryArray.append(data.Orders![i])
                    }
                }
                
                self.totalRows = data.TotalOrders
                self.mainArray.removeAll()
                self.mainArray = SaveAddressClass.getOrderHistoryArray
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //View Animation
    func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        view.layer.add(transition, forKey: kCATransition)
    }
    func viewSlideInFromBottomToTop(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        view.layer.add(transition, forKey: kCATransition)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension OrderHistoryVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if totalRows > self.mainArray.count {
            return self.mainArray.count + 1
        }else{
            return self.mainArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.mainArray.count == indexPath.row {
           return 50
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
            self.tableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
            cell.activityIndicatorView.startAnimating()
            cell.selectionStyle = .none
            return cell
        }else{
            self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderHistoryVCTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderHistoryVCTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderHistoryVCTVCell", value: "", table: nil))!, for: indexPath) as! OrderHistoryVCTVCell
            
            cell.rattingBtn.tag = indexPath.row
            cell.rattingBtn.addTarget(self, action: #selector(rateBtn_Tapped(sender:)), for: .touchUpInside)
            
            let dic = self.mainArray[indexPath.row]
            cell.invoiceNumLbl.text = dic.InvoiceNo
            if dic.SectionType == 2{
                let expectDate = dic.ExpectedTime.components(separatedBy: "T")[0]
                let shipTime = dic.ShippingTime.components(separatedBy: " - ")
                if shipTime.count > 0 && dic.ShippingTime != ""{
                    cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)) \(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                }else{
                    cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                }
            }else{
                let expectDate = dic.ExpectedTime.components(separatedBy: ".")[0]
                cell.orderDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
            }
            cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: dic.OrderStatus, value: "", table: nil))!
            cell.quantityLbl.text = "\(dic.TotalItems)"
            cell.priceLblLbl.text = "\(dic.NetPrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"

            if dic.OrderType != nil{
                if dic.OrderType! == "4"{
                    if dic.DeliveryAddress != nil{
                        cell.addressLbl.text = dic.DeliveryAddress![0].Details
                    }else{
                        cell.addressLbl.text = ""
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.addressLbl.text = dic.Store![0].StoreAddressEn
                    }else{
                        cell.addressLbl.text = dic.Store![0].StoreAr
                    }
                }
            }else{
                cell.addressLbl.text = ""
            }
            
            cell.orderNumNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order No", value: "", table: nil))!) :"
            cell.orderStatusNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Status", value: "", table: nil))!)"
            cell.quantityNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quantity", value: "", table: nil))!)"
            cell.amountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Amount", value: "", table: nil))!)"
            
            if self.mainArray[indexPath.row].Favourite == false {
                cell.favouriteBtn.setImage(UIImage(named: "fav_unselected"), for: .normal)
            }else{
                cell.favouriteBtn.setImage(UIImage(named: "heart_fill"), for: .normal)
            }
            
            if dic.OrderStatus == "Close"{
                cell.ratingMainViewWidthConstraint.constant = 100
                if dic.OrderRatings != nil{
                    cell.rateView.rating = Double(dic.OrderRatings![0].Value)
                    cell.rattingBtn.isHidden = true
                    cell.rateView.isHidden = false
                }else{
                    cell.rattingBtn.isHidden = false
                    cell.rateView.isHidden = true
                }
            }else{
                cell.ratingMainViewWidthConstraint.constant = 0
                cell.rattingBtn.isHidden = true
                cell.rateView.isHidden = true
            }
        
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                if self.mainArray.count == indexPath.row+1 {
                    if totalRows >  self.mainArray.count {
                        pageNumber = pageNumber + 1
                        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favouriteAction = UIContextualAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!) { (action, view, completion) in
            if self.mainArray[indexPath.row].Favourite == false{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.textPopUpVCDelegate = self
                obj.id = indexPath.row
                obj.modalPresentationStyle = .overFullScreen
                self.present(obj, animated: true, completion: nil)
            }else{
                self.favouriteOrder(FavouriteName: "", index: indexPath.row)
            }
            completion(true)
        }

        let reOrderInfoAction = UIContextualAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))!) { (action, view, completion) in
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ReOrderVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
            obj.orderId = self.mainArray[indexPath.row].OrderId
            obj.isArchieve = self.mainArray[indexPath.row].IsArchive
            self.navigationController?.pushViewController(obj, animated: true)
            completion(true)
        }

        favouriteAction.image = UIImage(named: "heart_fill")
        favouriteAction.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        reOrderInfoAction.image = UIImage(named: "reOrder")
        reOrderInfoAction.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

        return UISwipeActionsConfiguration(actions: [reOrderInfoAction,favouriteAction])
    }
    //MARK: Favourite Service Integration
    func favouriteOrder(FavouriteName: String, index:Int){
        var dic:[String:Any] = [:]
        if self.favouriteBtn.currentImage == UIImage(named: "heart_fill"){
            dic = ["OrderId": self.mainArray[index].OrderId, "FavouriteName":FavouriteName, "RequestBy":2]
        }else{
            dic = ["OrderId": self.mainArray[index].OrderId, "RequestBy":2]
        }
        CartModuleServices.ToggleFevOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                var message = data.MessageEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    message = data.MessageEn
                }else{
                    message = data.MessageAr
                }
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                    //Favourite functionality
                    if self.mainArray[index].Favourite == false{
                        self.mainArray[index].Favourite = true
                    }else{
                        self.mainArray[index].Favourite = false
                    }
                    self.tableView.reloadData()
                }))
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
        }else{
            if mainArray[indexPath.row].OrderStatus == "Close" || mainArray[indexPath.row].OrderStatus == "Cancel" || mainArray[indexPath.row].OrderStatus == "Reject"{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ReOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                obj.orderId = self.mainArray[indexPath.row].OrderId
                obj.isArchieve = self.mainArray[indexPath.row].IsArchive
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = TrackOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                obj.orderId = mainArray[indexPath.row].OrderId
                obj.orderFrom = 2
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    //MARK: rate Button Tapped
    @objc func rateBtn_Tapped(sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = self.mainArray[sender.tag].OrderId
        obj.orderType = Int(self.mainArray[sender.tag].OrderType!)!
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.StoreName = self.mainArray[sender.tag].Store![0].StoreNameEn.capitalized
        }else{
            obj.StoreName = self.mainArray[sender.tag].Store![0].StoreAr.capitalized
        }
        obj.rateVCDelegate = self
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
}
//MARK: Rate VC Delegate
@available(iOS 13.0, *)
extension OrderHistoryVC:RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
        }
    }
}
//MARK: TextPopUp Delegate
@available(iOS 13.0, *)
extension OrderHistoryVC : TextPopUpVCDelegate{
    func didTapAction(ID: Int, name: String) {
        self.favouriteOrder(FavouriteName: name, index: ID)
    }
}
