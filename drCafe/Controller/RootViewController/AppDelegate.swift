//
//  AppDelegate.swift
//  Dr.Cafe
//
//  Created by Devbox on 16/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import UserNotifications // Push Notification
import Firebase

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var selectColor:String = "535353"
    var selectBGColor:String = "D8D8D8"
    var selectColorname:String = ""
    var appLanguage:String?
    var myLanguage:String?
    var filePath: Bundle?
    var cartQuantity = 0
    var cartAmount = 0.0
    var subCartQuantity = 0
    var subCartAmount = 0.0
    var cartStoreId = 0
    var cartStoreName = ""
    var selectAddress = ""
    var selectSectionType = 0
    var isCartAnimationFirst:Bool = false
    var isStoreAnimationFirst:Bool = false
    var imagePathURL = ""
    var siriShort = ""
    var isDashboardFirst = true
    
    //Beta Info
    var BetaInfoTitle1 = ""
    var BetaInfoMobile1 = ""
    var BetaInfoTitle2 = ""
    var BetaInfoMobile2 = ""
    var BetaInfoTitleAr1 = ""
    var BetaInfoTitleAr2 = ""

    //Current Location
    var currentLatitude:Double!
    var currentLongitude:Double!
    
    //Manual Location
    var manualLocationStr = ""
    var currentlocation:CLLocation!

    static func getDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Make sure that URL scheme is identical to the registered one
        if url.scheme?.caseInsensitiveCompare("com.creativesols.drcafecoffee.payments") == .orderedSame {
            // Send notification to handle result in the view controller.
            NotificationCenter.default.post(name: Notification.Name(rawValue:"AsyncPaymentCompletedNotificationKey"), object: nil)
            return true
        }
        return false
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Use the Firebase library to configure APIs.
        FirebaseApp.configure()
        
//        // Jail Broken
//        if UIDevice.current.isJailBroken == true {
//            /* Start Crashlytics Logs */
//            Crashlytics.crashlytics().setCustomValue("JailBroken", forKey: "Device")
//            /* End Crashlytics Logs */
//            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
//            return true
//        }
                
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //IQKeyboardManager.shared.enableAutoToolbar = false
        self.clearDefaultsData()
        self.getDevcieLangauge()
        GMSPlacesClient.provideAPIKey("AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM")
        GMSServices.provideAPIKey("AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM")
        
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        // fresh chat
        let freshchatConfig:FreshchatConfig = FreshchatConfig.init(appID: "c2362c09-e69d-469b-9d43-fa61be9e42da", andAppKey: "8cc1c2f6-aec7-4d6d-9193-577acfd6e263")
        freshchatConfig.cameraCaptureEnabled = true
        freshchatConfig.gallerySelectionEnabled = true
        freshchatConfig.notificationSoundEnabled = true
        freshchatConfig.showNotificationBanner = true
        freshchatConfig.teamMemberInfoVisible = true
          //config.domain = "YOUR-DOMAIN"
          Freshchat.sharedInstance().initWith(freshchatConfig)
        
        if(isDeviceToken() == false){
            UserDef.methodForSaveStringObjectValue("-1", andKey: "DeviceToken")
        }
        registerForPushNotifications() // Push Notification
       
        
//        if UITraitCollection.current.userInterfaceStyle == UIUserInterfaceStyle.dark {
//            selectColor = "000000"
//            selectBGColor = "000000"
//        }else{
            //selectColor = "535353"
            //selectBGColor = "D8D8D8"
        //}
        
        // Override point for customization after application launch.
        return true
    }
    // MARK: Clear Defaults Data
    func clearDefaultsData() {
        if getAppverson() == "0" {
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
                dictionary.keys.forEach
                { key in   defaults.removeObject(forKey: key) }
        }
//        let appVersion = Double(getAppverson())
//        if appVersion! < 2.0 {
//            let defaults = UserDefaults.standard
//            let dictionary = defaults.dictionaryRepresentation()
//            dictionary.keys.forEach
//            { key in   defaults.removeObject(forKey: key)
//            }
//        }
        UserDef.methodForSaveStringObjectValue("\(AppUtility.getApplicationVersion())", andKey: "AppVerson")
    }
    // MARK: Push Notifications
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            DispatchQueue.main.async(execute: {
                guard settings.authorizationStatus == .authorized else { return }
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        print("Device Token: \(token)")
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    func tabbarViewController(){
        if appLanguage == "English"{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let mainVC : UITabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let navigationController = UINavigationController(rootViewController: mainVC)
            self.window!.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: "MainAr", bundle: nil)
            let mainVC : UITabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let navigationController = UINavigationController(rootViewController: mainVC)
            
            self.window!.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }
    }
    func getDevcieLangauge() {
        let app = UIApplication.shared.delegate as? AppDelegate
        let defaults = UserDefaults.standard
        var language = Bundle.main.preferredLocalizations[0]
        language = language.components(separatedBy: "-")[0]
        if !UserDefaults.standard.bool(forKey: "HasLaunchedOnce") {
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
            
            if (language == "en") {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "en", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "English"
                appLanguage = "English"
                defaults.set("English", forKey: "Language")
                defaults.synchronize()
            } else {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "ar", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "Arabic"
                appLanguage = "Arabic"
                defaults.set("Arabic", forKey: "Language")
                defaults.synchronize()
            }
            tabbarViewController()
        }else{
            let laguageSelect = UserDefaults.standard.string(forKey: "Language")
            if (laguageSelect == "Arabic") {
                
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "ar", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "Arabic"
                appLanguage = "Arabic"
                defaults.set("Arabic", forKey: "Language")
                defaults.synchronize()
            } else {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "en", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "English"
                appLanguage = "English"
                defaults.set("English", forKey: "Language")
                defaults.synchronize()
            }
            tabbarViewController()
        }
    }
}

// MARK: Push Notifications
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // Tracking Order Callback
        NotificationCenter.default.post(name: Notification.Name(rawValue:"getOrderTrackingDetails"), object: nil)

        
        let alert = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: Title, value: "", table: nil))!, message: ((notification.request.content.userInfo["aps"] as? [AnyHashable : Any])?["alert"] as! String), preferredStyle: .alert)
        let action = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .cancel, handler: nil)
        alert.addAction(action)
        window?.rootViewController?.present(alert, animated: true)
        
//        print(notification.request.content.userInfo)
//        print(notification.request.content.userInfo["PnType"] as Any)
//
//        let PnType : Int? = notification.request.content.userInfo["PnType"] as? Int
//        switch PnType {
//        case 1:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedNotificationData"), object: nil )
//
//        case 2:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 3:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 4:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 5:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 6:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 7:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedHistoryData"), object: nil )
//
//        default:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedNotificationData"), object: nil )
//
//        }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
       /* if(isUserLogIn() == true){
            // 1
            let userInfo = response.notification.request.content.userInfo
            let aps = userInfo["aps"] as! [String: AnyObject]
            print(aps)
            let identifier = "NotificationVC"
            
            //            let PnType: Int = response.notification.request.content.userInfo["PnType"] as! Int
            //            switch PnType {
            //            case 1:
            //                identifier = "NotificationVC"
            //
            //            case 2:
            //                identifier = "RequestsListVC"
            //
            //            case 3:
            //                identifier = "RequestsListVC"
            //
            //            case 4:
            //                identifier = "RequestsListVC"
            //
            //            case 5:
            //                identifier = "RequestsListVC"
            //
            //            case 6:
            //                identifier = "RequestsListVC"
            //
            //            case 7:
            //                identifier = "HistoryVC"
            //
            //            default:
            //                identifier = "NotificationVC"
            //            }
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftMenuVC: LeftMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
            let centerVC: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: identifier)
            let centerNavVC = UINavigationController(rootViewController: centerVC)
            
            //  Set the Panel controllers with just two lines of code
            let rootController: FAPanelController = window?.rootViewController as! FAPanelController
            rootController.center(centerNavVC).left(leftMenuVC)
            
        }*/
        
        
        
        //        PN TYPES
        //        1    Registration Successful    التسجيل ناجح
        //        2    New Request    طلب جديد
        //        3    Bid Placed    عرض السعر
        //        4    Bid Accepted    قبلت المزايدة
        //        5    Final Price Placed    السعر النهائي يوضع
        //        6    Final Price Accepted    السعر النهائي مقبول
        //        7    Invoice Created    الفاتورة التي تم إنشاؤها
        
        completionHandler()
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        switch userActivity.activityType {
        case UserActivityType.TrackOrder:
           // print(userActivity.userInfo as Any)
            self.siriShort = UserActivityType.TrackOrder
            self.tabbarViewController()
            
        case UserActivityType.OrderHistory:
            self.siriShort = UserActivityType.OrderHistory
            self.tabbarViewController()
//            if let homeVC = window?.rootViewController as? HomeVC {
//                homeVC.goToTrackOrder()
//            }
        default:
            print("No such user activity")
        }
        return false
    }
    
}
