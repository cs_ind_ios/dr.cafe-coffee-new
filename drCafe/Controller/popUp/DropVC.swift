//
//  DropVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 29/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: DropVC Delegate Protocol
protocol DropVCDelegate {
    func didTapAction(ID:String,name:String,whereType:Int)
}

@available(iOS 13.0, *)
class DropVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var titleLblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLblHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dropVCDelegate:DropVCDelegate!
    var whereObj = 0
    var isSingle = false
    var isBeta = false
    
    var dropDownArray = [""]
    var codeDropDownArray = [CountryCodeDetailsModel]()
    
    //var carColorsArray = [CarColorsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if isBeta == false{
            if isSingle == true{
                titleLblHeight.constant = 0
                titleLblTopConstraint.constant = 0
            }else{
                if whereObj == 1{
                    titleLblHeight.constant = 0
                    titleLblTopConstraint.constant = 0
                }else{
                    titleLblHeight.constant = 50
                    titleLblTopConstraint.constant = 10
                }
            }
        }else{
            titleLblHeight.constant = 0
            titleLblTopConstraint.constant = 0
            self.tableView.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
        }
    }
}
//MARK: TableView Delegate Method
@available(iOS 13.0, *)
extension DropVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if whereObj == 1{
            return codeDropDownArray.count
        }else{
            return dropDownArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSingle == true{
            tableView.register(UINib(nibName: "SettingsVCTVCell", bundle: nil), forCellReuseIdentifier: "SettingsVCTVCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsVCTVCell", for: indexPath) as! SettingsVCTVCell
            if whereObj == 1{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.nameLbl.text = "\(codeDropDownArray[indexPath.row].CountryNameEn)(+\(codeDropDownArray[indexPath.row].MobileCode))"
                }else{
                    cell.nameLbl.text = "(+\(codeDropDownArray[indexPath.row].MobileCode))\(codeDropDownArray[indexPath.row].CountryNameAr)"
                }
            }else{
                cell.nameLbl.text = dropDownArray[indexPath.row]
            }
            if isBeta == false{
                self.tableView.separatorStyle = .singleLine
            }else{
                self.tableView.separatorStyle = .none
                cell.nameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
            }
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: "DropVCTVCell", bundle: nil), forCellReuseIdentifier: "DropVCTVCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropVCTVCell", for: indexPath) as! DropVCTVCell
            cell.nameLbl.text = dropDownArray[indexPath.row]
            cell.calariesLbl.text = ""
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if whereObj == 1{
            dropVCDelegate.didTapAction(ID: "\(codeDropDownArray[indexPath.row].MobileCode)", name: codeDropDownArray[indexPath.row].CountryNameEn, whereType: whereObj)
        }else{
            dropVCDelegate.didTapAction(ID: "1", name: dropDownArray[indexPath.row], whereType: whereObj)
        }
        dismiss(animated: false, completion: nil)
    }
}
