//
//  ImagePopUpVC.swift
//  drCafe
//
//  Created by Devbox on 13/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ImagePopUpVC: UIViewController, UIScrollViewDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var noOfImagesLbl: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    var itemImagesArray = [ItemImagesModel]()
    var itemName = String()
    var imageName = String()
    @IBOutlet weak var fullViewBtn: UIButton!
    
    @IBOutlet weak var imagePageController: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myScrollView.minimumZoomScale = 1.0
        myScrollView.maximumZoomScale = 10.0
        myScrollView.delegate = self
        itemNameLbl.text = itemName
        //playerView!.transform = CGAffineTransform(scaleX: 2, y: 2)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            layout.itemSize = CGSize(width: 100, height: 100)
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal
        imagesCollectionView.collectionViewLayout = layout
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
        self.selectImage(index: 0)
        
        if itemImagesArray.count > 1{
            imagePageController.isHidden = false
            imagePageController.numberOfPages = itemImagesArray.count
            imagePageController.currentPage = 0
        }else{
            imagePageController.isHidden = true
        }
        
        imagePageController.addTarget(self, action: #selector(pageControllerSelection(_:)), for: .valueChanged)
    }
    //MARK: Pagination Selection
    @objc func pageControllerSelection(_ sender: UIPageControl){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let page: Int? = sender.currentPage
            let nextItem: IndexPath = IndexPath(item: page!, section: 0)
            if nextItem.row < self.itemImagesArray.count {
                self.selectImage(index: page!)
            }
        }
    }
    //MARK: Full View
    @IBAction func fullViewBtn_Tapped(_ sender: Any) {
        if  playerView!.transform == .identity {
            playerView!.transform = CGAffineTransform(rotationAngle: .pi/2)
            playerView!.frame = CGRect(x: self.myScrollView!.frame.minX , y: self.myScrollView!.frame.minY, width: self.myScrollView!.frame.width, height: self.myScrollView!.frame.height)
        }else{
            playerView!.transform = .identity
            playerView!.frame = CGRect(x: 10 , y: self.myScrollView!.frame.minY, width: self.playerView!.frame.height, height: self.playerView!.frame.width)
        }
    }
    //MARK: Play Button Action
    @IBAction func playBtn_Tapped(_ sender: Any) {
        fullViewBtn.isHidden = false
        if let image = playBtn.currentImage {
            if image.isEqual(UIImage(named: "play")) {
                playBtn.isHidden = false
                playBtn.setImage(UIImage(named: ""), for: .normal)
                playerView!.player!.play()
                self.view.bringSubviewToFront(playBtn)
            }else{
                playBtn.isHidden = false
                playBtn.setImage(UIImage(named: "play"), for: .normal)
                playerView!.player!.pause()
                self.view.bringSubviewToFront(playBtn)
            }
        }else{
            playBtn.isHidden = false
            playBtn.setImage(UIImage(named: "play"), for: .normal)
            playerView!.player!.pause()
            self.view.bringSubviewToFront(playBtn)
        }
    }
    func selectImage(index:Int){
        if itemImagesArray.count > 0{
            noOfImagesLbl.text = "\(index+1) of \(itemImagesArray.count)"
            imageName =  itemImagesArray[index].Image
            imageView.image = UIImage(named: "")
            let fullNameArr = imageName.split(separator:".")
            if fullNameArr.count > 1 {
            if fullNameArr[1] == "mp4" {
                playBtn.isHidden = false
                playerView.isHidden = false
                myScrollView.isHidden = true
                fullViewBtn.isHidden = false
                playerView!.transform = .identity
                playerView!.frame = CGRect(x: 10 , y: self.myScrollView!.frame.minY, width: self.playerView!.frame.height, height: self.playerView!.frame.width)
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(imageName)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                let avPlayer = AVPlayer(url: url! as URL);
                playerView!.playerLayer.player = avPlayer;
                playBtn.setImage(UIImage(named: ""), for: .normal)
                playerView!.player!.play()
                self.view.bringSubviewToFront(playBtn)
                return
            }
            }
            if playBtn.isHidden == false {
                playerView!.player!.pause()
                playBtn.setImage(UIImage(named: "play"), for: .normal)
            }
            playBtn.isHidden = true
            playerView.isHidden = true
            myScrollView.isHidden = false
            fullViewBtn.isHidden = true

            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(imageName)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            imageView.kf.setImage(with: url)
        }else{
            if imageName != ""{
                noOfImagesLbl.text = "1 of 1"
                playBtn.isHidden = true
                playerView.isHidden = true
                myScrollView.isHidden = false
                fullViewBtn.isHidden = true
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(imageName)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                imageView.kf.setImage(with: url)
            }else{
                noOfImagesLbl.text = "0 of 0"
            }
        }
    }
    //MARK: View Zoom
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    //MARK: Cancel Button Action
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
//MARK: Collectionview delegates
extension ImagePopUpVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        imagesCollectionView.register(UINib(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
        let cell = imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
        let name = itemImagesArray[indexPath.row].Image
        cell.img.image = UIImage(named: "")
        let fullNameArr = name.split(separator:".")
        if fullNameArr.count > 1 {
        if fullNameArr[1] == "mp4" {
            cell.playBtn.isHidden = false
            cell.playerView.isHidden = false
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            let avPlayer = AVPlayer(url: url! as URL);
            cell.playerView!.playerLayer.player = avPlayer;
            cell.contentView.bringSubviewToFront(cell.playBtn)
        }else{
            cell.playBtn.isHidden = true
            cell.playerView.isHidden = true
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
        }
        }else{
            cell.playBtn.isHidden = true
            cell.playerView.isHidden = true
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
        }
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.darkGray.cgColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectImage(index: indexPath.row)
        imagePageController.currentPage = indexPath.row
    }
}
