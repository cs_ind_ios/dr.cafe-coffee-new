//
//  CategoryPopUpVC.swift
//  drCafe
//
//  Created by Devbox on 09/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: Category PopUp Delegate Protocol
protocol CategoryPopUpVCDelegate {
    func didTapAction(dic:CategoryDetailsModel)
}

@available(iOS 13.0, *)
class CategoryPopUpVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var popUpMainView: UIView!
    @IBOutlet weak var popUpCollectionView: UICollectionView!
    @IBOutlet weak var popUpDismissBtn: UIButton!
    @IBOutlet weak var popUpMainViewHeight: NSLayoutConstraint!
    
    var categoryPopUpVCDelegate:CategoryPopUpVCDelegate!
    var categoryArray = [CategoryDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryArray.removeAll()
        for dic in SaveAddressClass.discoverCategoryArray {
            categoryArray.append(dic)
        }
        for dic in SaveAddressClass.newCategoryArray {
            for dic1 in dic.Category! {
                categoryArray.append(dic1)
            }
        }
        popUpCollectionView.delegate = self
        popUpCollectionView.dataSource = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            popUpCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            popUpCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        
    }
    //MARK: ViewWill Appear
    override func viewWillAppear(_ animated: Bool) {
        popUpCollectionView.reloadData()
    }
    //MARK: PopUp Dismiss
    @IBAction func popUpDismissBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension CategoryPopUpVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        popUpCollectionView.register(UINib(nibName: "CategoryPopUpCVCell", bundle: nil), forCellWithReuseIdentifier: "CategoryPopUpCVCell")
        let cell = popUpCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryPopUpCVCell", for: indexPath) as! CategoryPopUpCVCell

        let dic = categoryArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.categoryNameLbl.text = dic.NameEn
        }else{
            cell.categoryNameLbl.text = dic.NameAr
        }
        //Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.categoryImg.kf.setImage(with: url)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            if indexPath.row%2 == 0{
                cell.leftLineLbl.isHidden = true
                cell.rightLineLbl.isHidden = false
            }else{
                cell.leftLineLbl.isHidden = false
                cell.rightLineLbl.isHidden = true
            }
        }else{
            if indexPath.row%2 == 0{
                cell.leftLineLbl.isHidden = false
                cell.rightLineLbl.isHidden = true
            }else{
                cell.leftLineLbl.isHidden = true
                cell.rightLineLbl.isHidden = false
            }
        }
        if indexPath.row == categoryArray.count-1{
            if indexPath.row%2 == 0{
                cell.leftLineLbl.isHidden = true
                cell.rightLineLbl.isHidden = true
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = popUpMainView.bounds.width
        return CGSize(width: width/2, height: 110)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryPopUpVCDelegate.didTapAction(dic: categoryArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}
