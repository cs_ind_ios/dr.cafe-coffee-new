//
//  ImagePopUpTwoVC.swift
//  drCafe
//
//  Created by Devbox on 28/10/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class ImagePopUpTwoVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    var imageName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myScrollView.minimumZoomScale = 1.0
        myScrollView.maximumZoomScale = 10.0
        myScrollView.delegate = self
        
        //Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(imageName)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        imageView.kf.setImage(with: url)
        
    }
    //MARK: View Zoom
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    //MARK: Cancel Button Action
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
