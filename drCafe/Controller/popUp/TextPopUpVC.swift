//
//  TextPopUpVC.swift
//  drCafe
//
//  Created by Devbox on 22/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: Drop Down Delegate Protocal
protocol TextPopUpVCDelegate {
    func didTapAction(ID:Int,name:String)
}

@available(iOS 13.0, *)
class TextPopUpVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var titleOneLbl: UILabel!
    @IBOutlet weak var titleTwoLbl: UILabel!
    @IBOutlet weak var favouriteTF: UITextField!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var popUpCancelBtn: UIButton!
    
    var textPopUpVCDelegate:TextPopUpVCDelegate!
    var id = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTwoLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Save the order to favourite", value: "", table: nil))!
        favouriteTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter Favourite order Name", value: "", table: nil))!
        noBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, for: .normal)
        yesBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, for: .normal)
        
        favouriteTF.delegate = self
        
    }
    //MARK: PopUp Cancel Button Action
    @IBAction func popUpCancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: No Button Action
    @IBAction func noBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Yes Button Action
    @IBAction func yesBtn_Tapped(_ sender: Any) {
        if favouriteTF.text != ""{
            textPopUpVCDelegate.didTapAction(ID: id, name: favouriteTF.text!)
            dismiss(animated: true, completion: nil)
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter favourite name", value: "", table: nil))!)
        }
    }
}
//MARK: TextField Delegate Method
@available(iOS 13.0, *)
extension TextPopUpVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textField.textInputMode?.primaryLanguage == "emoji") || string.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.safelyLimitedTo(length: 250)
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
