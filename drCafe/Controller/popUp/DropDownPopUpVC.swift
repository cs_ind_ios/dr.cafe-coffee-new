//
//  DropDownPopUpVC.swift
//  drCafe
//
//  Created by Devbox on 05/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: Drop Down Delegate Protocal
protocol DropDownPopUpVCDelegate {
    func didTapAction(ID:String,name:String,selectBannerDetailsArray:[BannerDetailsModel]!)
}
@available(iOS 13.0, *)
class DropDownPopUpVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var titleOneLbl: UILabel!
    @IBOutlet weak var titleTwoLbl: UILabel!
    @IBOutlet weak var selectTypeNameLbl: UILabel!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var popUpCancelBtn: UIButton!
    
    var bannerDetailsArray = [BannerDetailsModel]()
    var dropDownPopUpVCDelegate:DropDownPopUpVCDelegate!
    var orderTypeArray = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    //MARK: PopUp Cancel Button Action
    @IBAction func popUpCancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Drop Down
    @IBAction func dropDownBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = orderTypeArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        pVC?.sourceRect = CGRect(x: sender.center.x , y: 0, width: 0, height: sender.frame.size.height)
        obj.whereObj = 2
        obj.dropDownArray = orderTypeArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Done Button Action
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        if selectTypeNameLbl.text != PleaseSelectOrderType{
            dropDownPopUpVCDelegate.didTapAction(ID: "1", name: selectTypeNameLbl.text!, selectBannerDetailsArray: bannerDetailsArray)
            dismiss(animated: true, completion: nil)
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Order Type", value: "", table: nil))!)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
//MARK: Drop Down Delegate
@available(iOS 13.0, *)
extension DropDownPopUpVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        self.selectTypeNameLbl.text = name
    }
}
