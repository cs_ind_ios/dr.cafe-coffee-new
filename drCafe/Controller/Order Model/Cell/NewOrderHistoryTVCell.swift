//
//  NewOrderHistoryTVCell.swift
//  drCafe
//
//  Created by Devbox on 06/07/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class NewOrderHistoryTVCell: UITableViewCell {
    
    @IBOutlet weak var invoiceNumLbl: UILabel!
    @IBOutlet weak var favouriteNameLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountCurrencyLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var orderTimeLbl: UILabel!
    @IBOutlet weak var favourireNameViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rateViewWidth: NSLayoutConstraint!
    @IBOutlet weak var rattingView: FloatRatingView!
    @IBOutlet weak var rateBtnView: CustomView!
    @IBOutlet weak var rateMainView: CustomView!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var rateOrderNameLbl: UILabel!
    
    @IBOutlet weak var subscriptionImgWidth: NSLayoutConstraint!
    @IBOutlet weak var subscriptionImgLeading: NSLayoutConstraint!
    @IBOutlet weak var subscriptionImg: UIImageView!
    @IBOutlet weak var invoiceSelectBtn: UIButton!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var orderTypeImg: UIImageView!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var paymentTypeImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
