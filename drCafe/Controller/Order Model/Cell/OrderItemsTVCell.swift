//
//  OrderItemsTVCell.swift
//  drCafe
//
//  Created by Devbox on 23/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class OrderItemsTVCell: UITableViewCell {

    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemWtLbl: UILabel!
    @IBOutlet weak var itemQuantityLbl: UILabel!
    @IBOutlet weak var itemImgWidth: NSLayoutConstraint!
    @IBOutlet weak var itemImgLeading: NSLayoutConstraint!
    @IBOutlet weak var itemImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var additionalNameLbl: UILabel!
    @IBOutlet weak var additionalsLbl: UILabel!
    @IBOutlet weak var additionalsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var BGView: UIView!
    
    @IBOutlet weak var grinderNameLbl: UILabel!
    @IBOutlet weak var grinderViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var redeemPointsMsgLbl: UILabel!
    @IBOutlet weak var redeemPointsLbl: UILabel!
    @IBOutlet weak var redeemPointsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemPointsView: UIView!
    
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var noteViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
