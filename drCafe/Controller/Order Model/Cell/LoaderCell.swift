//
//  LoaderCell.swift
//  drCafe
//
//  Created by Devbox on 21/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class LoaderCell: UITableViewCell {

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
