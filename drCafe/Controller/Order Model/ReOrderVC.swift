//
//  ReOrderVC.swift
//  drCafe
//
//  Created by Devbox on 24/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ReOrderVC: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var orderNumLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var priceCurrrencyLbl: UILabel!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var orderTypeImg: UIImageView!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var paymentTypeImg: UIImageView!
    @IBOutlet weak var favouriteNameLbl: UILabel!
    @IBOutlet weak var favouriteViewHeight: NSLayoutConstraint!

    @IBOutlet weak var deliveryFrequencyNameLbl: UILabel!
    @IBOutlet weak var topFrequencyNameLbl: UILabel!
    @IBOutlet weak var topDFViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var orderDetailsNameLbl: UILabel!
    @IBOutlet weak var orderItemsTableView: UITableView!
    @IBOutlet weak var orderItemsTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var priceDetailsMainView: UIView!
    @IBOutlet weak var totalAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountNameLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLbl: UILabel!
    @IBOutlet weak var deliveryChargesLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var netAmountNameLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var walletDiscountNameLbl: UILabel!
    @IBOutlet weak var walletDiscountHeight: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountBottom: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryChargesNameLblBottom: NSLayoutConstraint!
    @IBOutlet weak var couponDiscountNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var couponDiscountNameLblBottom: NSLayoutConstraint!
    @IBOutlet weak var discountNameLblTop: NSLayoutConstraint!
    @IBOutlet weak var vatLineLbl: UILabel!
    
    @IBOutlet weak var netTotalView: UIView!
    @IBOutlet weak var btmNetAmountNameLbl: UILabel!
    @IBOutlet weak var btmNetAmountLbl: UILabel!
    
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var orderAgainBtn: UIButton!
    @IBOutlet weak var resumeBtn: UIButton!
    @IBOutlet weak var bottomBtnsSepLineLbl: UILabel!
    @IBOutlet weak var bottomBtnsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomBtnsMainView: UIView!
    
    @IBOutlet weak var rateBtnView: CustomView!
    @IBOutlet weak var rateOrderNameLbl: UILabel!
    @IBOutlet weak var rattingBtn: UIButton!
    @IBOutlet weak var rateView: FloatRatingView!
    @IBOutlet weak var ratingMainView: UIView!
    
    @IBOutlet weak var DFPopUpMainView: UIView!
    @IBOutlet weak var deliveryFrequencyTableView: UITableView!
    @IBOutlet weak var frequencyNameLbl: UILabel!
    
    //SubOrder Details
    @IBOutlet weak var subOrderTypeNameLbl: UILabel!
    @IBOutlet weak var subOrderTypeLbl: UILabel!
    @IBOutlet weak var carDetailsNameLbl: UILabel!
    @IBOutlet weak var carNumberNameLbl: UILabel!
    @IBOutlet weak var carNumberLbl: UILabel!
    @IBOutlet weak var carColorNameLbl: UILabel!
    @IBOutlet weak var carColorLbl: UILabel!
    @IBOutlet weak var carBrandNameLbl: UILabel!
    @IBOutlet weak var carBrandLbl: UILabel!
    @IBOutlet weak var carDetailsViewHeight: NSLayoutConstraint!//188,41
    @IBOutlet weak var carDetailsViewTop: NSLayoutConstraint!
    
    var orderId = 0
    var orderDetailsArray = [OrderDetailsDataModel]()
    var isArchieve:Bool!
    var isSubscription = false
    
    var shippingMethodsArray = [ShippingMethodDetailsModel]()
    var selectOrderId = 0
    var selectFrequency = 0
    var selectSubscriptionId = 0
    var selectDate = ""
    var selectSlotId = 0
    var firstShipDate = ""
    var firstSelectShipId = 0
    var shipTimeSlot = ""
    var shipTimeSlotDate = ""
    var currentDate : Date = Date()
    var paymentIcon = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        
        orderItemsTableView.delegate = self
        orderItemsTableView.dataSource = self
        deliveryFrequencyTableView.delegate = self
        deliveryFrequencyTableView.dataSource = self
        
        if isArchieve == false{
            getOrderDetailsService()
            self.orderAgainBtn.isHidden = false
            self.bottomBtnsMainView.isHidden = false
            self.bottomBtnsViewHeight.constant = 74
        }else if isArchieve == true{
            getOldOrderDetailsService()
            self.orderAgainBtn.isHidden = true
            self.bottomBtnsMainView.isHidden = true
            self.bottomBtnsViewHeight.constant = 0
        }else{
            getOrderDetailsService()
            self.orderAgainBtn.isHidden = false
            self.bottomBtnsMainView.isHidden = false
            self.bottomBtnsViewHeight.constant = 74
        }
        
        currentTimeGetService()
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        //Theme Color
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
        }else{
            self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
        }
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        orderAgainBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        orderAgainBtn.setTitleColor(.white, for: .normal)
        resumeBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        resumeBtn.setTitleColor(.white, for: .normal)
        
        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Details", value: "", table: nil))!
        self.orderDetailsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Details", value: "", table: nil))!
        self.totalAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) :"
        self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
        self.discountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
        self.netAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!) :"
        self.btmNetAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!) :"
        self.walletDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE PAY", value: "", table: nil))!) :"
        self.rateOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Order", value: "", table: nil))!
        self.deliveryFrequencyNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Frequency", value: "", table: nil))!) :"
        self.frequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Frequency Options", value: "", table: nil))!
        self.subOrderTypeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Order Type", value: "", table: nil))!) :"
        self.carDetailsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Details", value: "", table: nil))!
        self.carNumberNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vehicle Number", value: "", table: nil))!) :"
        self.carColorNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Color", value: "", table: nil))!) :"
        self.carBrandNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Brand", value: "", table: nil))!) :"
        
        // Subscription Functionality
        if isSubscription == false{
            self.topDFViewHeightConstraint.constant = 0
        }else{
            self.topDFViewHeightConstraint.constant = 40
        }
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Order Again Button
    @IBAction func orderAgainBtn_Tapped(_ sender: Any) {
        if orderAgainBtn.currentTitle == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))! {
            let dic:[String:Any] = ["OrderId": orderId, "RequestBy":2]
            CartModuleServices.ReOrderService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    DispatchQueue.main.async {
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = CartVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                        obj.isRemove = true
                        obj.whereObj = 6
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else if orderAgainBtn.currentTitle == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Retry", value: "", table: nil))!{
            let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
            let dic:[String:Any] = ["amount": orderDetailsArray[0].NetTotal, "merchantTransactionId":orderDetailsArray[0].Invoice, "customerEmail":"\((userDic["Email"]! as! String))", "customerPhone":"\((userDic["Mobile"]! as! String))", "customerName":"\((userDic["FullName"]! as! String))","OrderId":orderId, "registrationId":orderDetailsArray[0].registrationId!, "UserId":UserDef.getUserId(), "SubscriptionId":orderDetailsArray[0].SubscriptionId!, "isVisaMaster":true]
            SubscriptionModuleServices.RetryService(dic: dic, success: { (data) in
                if(data.Status == false){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                }else{
                    DispatchQueue.main.async {
                        self.getOrderDetailsService()
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            if shippingMethodsArray.count > 0{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "PauseVC") as! PauseVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.pauseVCDelegate = self
                obj.minimumDate = DateConverts.convertStringToDate(date: "\(self.shipTimeSlotDate)", dateformatType: .date)
                obj.maxDays = self.shippingMethodsArray[0].FutureDays
                obj.obj_where = 2
                obj.shipDetails = self.shippingMethodsArray
                if self.orderDetailsArray[0].FrequencyOptions != nil{
                    obj.pauseFrequencyArray = self.orderDetailsArray[0].FrequencyOptions!
                }
                obj.selectSlotId = self.selectSlotId
                obj.modalPresentationStyle = .overFullScreen
                self.present(obj, animated: true, completion: nil)
            }
        }
    }
    //MARK: Get Order Details Service
    func getOrderDetailsService(){
        let dic:[String:Any] = [:]
        let url = "\(baseURL)api/Order/\(orderId)/Details"
        CartModuleServices.OrderDetailsService(dic: dic, url: url, success: { (data) in
            if(data.Status == false){
                self.myScrollView.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                self.myScrollView.isHidden = false
                self.orderDetailsArray = data.Data!
                DispatchQueue.main.async {
                    self.allDetails()
                    self.shippingMethodDetailsGetService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Get Old Order Details Service
    func getOldOrderDetailsService(){
        let dic:[String:Any] = [:]
        let url = "\(baseURL)api/Order/\(orderId)/Details"
        CartModuleServices.OldOrderDetailsService(dic: dic, url: url, success: { (data) in
            if(data.Status == false){
                self.myScrollView.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                self.myScrollView.isHidden = false
                if data.Data != nil{
                    if data.Data!.count > 0{
                        self.orderDetailsArray = data.Data![0].OrderJson!
                    }
                }
                DispatchQueue.main.async {
                    self.allDetails()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func allDetails(){
        //Order Details
        if orderDetailsArray[0].IsSubscription != nil{
            if orderDetailsArray[0].IsSubscription! == true{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!)(\(Int(orderDetailsArray[0].FrequencyDiscount!))%) :"
            }else{
                if orderDetailsArray[0].ConsumedPoints! != 0{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Discount", value: "", table: nil))!)(\(orderDetailsArray[0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!)) :"
                }else if orderDetailsArray[0].PromoCode! != ""{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!)(\(orderDetailsArray[0].PromoCode!)) :"
                }else{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
                    self.couponDiscountNameLblHeight.constant = 0
                    self.couponDiscountNameLblBottom.constant = 0
                    self.discountNameLblTop.constant = 0
                    self.vatLineLbl.isHidden = true
                }
            }
        }else{
            if orderDetailsArray[0].ConsumedPoints != nil{
                if orderDetailsArray[0].ConsumedPoints! != 0{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Discount", value: "", table: nil))!)(\(orderDetailsArray[0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!)) :"
                }else if orderDetailsArray[0].PromoCode! != ""{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!)(\(orderDetailsArray[0].PromoCode!)) :"
                }else{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
                    self.couponDiscountNameLblHeight.constant = 0
                    self.couponDiscountNameLblBottom.constant = 0
                    self.discountNameLblTop.constant = 0
                    self.vatLineLbl.isHidden = true
                }
            }else{
                if orderDetailsArray[0].PromoCode! != ""{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!)(\(orderDetailsArray[0].PromoCode!)) :"
                }else{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
                    self.couponDiscountNameLblHeight.constant = 0
                    self.couponDiscountNameLblBottom.constant = 0
                    self.discountNameLblTop.constant = 0
                    self.vatLineLbl.isHidden = true
                }
            }
        }
        
        self.orderNumLbl.text = "#\(orderDetailsArray[0].Invoice)"
        if orderDetailsArray[0].SectionType != nil{
            if orderDetailsArray[0].SectionType! == 2{
                self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Shipping Charges", value: "", table: nil))!) :"
                let expectDate = orderDetailsArray[0].ExpectedDate.components(separatedBy: "T")[0]
                let shipTime = orderDetailsArray[0].ShippingTime.components(separatedBy: "-")
                if orderDetailsArray[0].ExpectedDate != ""{
                    if shipTime.count > 0 && orderDetailsArray[0].ShippingTime != ""{
                        self.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        self.timeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                        timeLbl.font = UIFont(name: "Avenir Book", size: 12.0)
                    }else{
                        self.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)
                        self.timeLbl.text = ""
                        timeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
                    }
                }else{
                    self.dateLbl.text = ""
                    self.timeLbl.text = ""
                }
            }else{
                self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
                let expectDate = orderDetailsArray[0].ExpectedDate.components(separatedBy: ".")[0]
                if orderDetailsArray[0].ExpectedDate != ""{
                    self.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    self.timeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                }else{
                    self.dateLbl.text = ""
                    self.timeLbl.text = ""
                }
                timeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            }
        }else{
            self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
            let orderDate = orderDetailsArray[0].OrderDate.components(separatedBy: " ")[0]
            let expectDate = orderDetailsArray[0].ExpectedDate.components(separatedBy: ".")[0]
            if orderDetailsArray[0].OrderDate != ""{
                self.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: orderDate, inputDateformatType: .MMddyyyy, outputDateformatType: .date))"
            }else{
                self.dateLbl.text = ""
            }
            if orderDetailsArray[0].ExpectedDate != ""{
                self.timeLbl.text = "\(expectDate)"
            }else{
                self.timeLbl.text = ""
            }
            timeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
        }
        
        self.priceLbl.text = "\(orderDetailsArray[0].NetTotal.withCommas())"
        self.priceCurrrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
        
        if orderDetailsArray[0].OrderStatus == "Close"{
            if orderDetailsArray[0].orderType == "Delivery"{
                self.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
            }else{
                self.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served", value: "", table: nil))!
            }
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: orderDetailsArray[0].OrderStatus, value: "", table: nil))!
            }else{
                self.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: orderDetailsArray[0].OrderStatusAr, value: "", table: nil))!
            }
        }
        
        if orderDetailsArray[0].orderType == "Delivery"{
            self.addressLbl.text = orderDetailsArray[0].CAddress
            self.deliveryChargesNameLblHeight.constant = 21
            self.deliveryChargesNameLblBottom.constant = 10
            self.discountNameLblTop.constant = 10
            self.vatLineLbl.isHidden = false
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.addressLbl.text = orderDetailsArray[0].StoreAddressEn
            }else{
                self.addressLbl.text = orderDetailsArray[0].StoreAddressAr
            }
            self.deliveryChargesNameLblHeight.constant = 0
            self.deliveryChargesNameLblBottom.constant = 0
        }
        
        orderTypeLbl.textColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.orderTypeLbl.text = orderDetailsArray[0].orderType
        }else{
            self.orderTypeLbl.text = orderDetailsArray[0].orderTypeAr
        }
        if orderDetailsArray[0].orderType == "DineIn"{
            orderTypeImg.image = UIImage(named: "dinein")
        }else if orderDetailsArray[0].orderType == "DriveThru"{
            orderTypeImg.image = UIImage(named: "Drive Thru")
        }else if orderDetailsArray[0].orderType == "PickUp"{
            orderTypeImg.image = UIImage(named: "Home Pick up")
            if orderDetailsArray[0].SubOrderNameEn == "" || orderDetailsArray[0].SubOrderNameEn == nil{
                self.carDetailsViewTop.constant = 0
                self.carDetailsViewHeight.constant = 0
            }else{
                self.carDetailsViewTop.constant = 10
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if let _ = orderDetailsArray[0].SubOrderNameEn{
                        self.subOrderTypeLbl.text = orderDetailsArray[0].SubOrderNameEn!
                    }
                }else{
                    if let _ = orderDetailsArray[0].SubOrderNameAr{
                        self.subOrderTypeLbl.text = orderDetailsArray[0].SubOrderNameAr!
                    }
                }
                if orderDetailsArray[0].CarId != 0{
                    self.carDetailsViewHeight.constant = 188
                    if let _ = orderDetailsArray[0].CarJson{
                        let carDetails = orderDetailsArray[0].CarJson!.convertToDictionary()
                        if carDetails != nil{
                            self.carNumberNameLbl.text = "\(carDetails!["CarVehicleNo"]!)"
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                self.carColorLbl.text = "\(carDetails!["CarColorEn"]!)"
                                self.carBrandLbl.text = "\(carDetails!["CarBrandEn"]!)"
                            }else{
                                self.carColorLbl.text = "\(carDetails!["CarColorAr"]!)"
                                self.carBrandLbl.text = "\(carDetails!["CarBrandAr"]!)"
                            }
                        }else{
                            self.carNumberLbl.text = ""
                            self.carColorLbl.text = ""
                            self.carBrandLbl.text = ""
                            self.carDetailsViewHeight.constant = 41
                        }
                    }else{
                        self.carNumberLbl.text = ""
                        self.carColorLbl.text = ""
                        self.carBrandLbl.text = ""
                        self.carDetailsViewHeight.constant = 41
                    }
                }else{
                    self.carDetailsViewHeight.constant = 41
                }
            }
        }else if orderDetailsArray[0].orderType == "Delivery"{
            if orderDetailsArray[0].IsSubscription! == true{
                orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!
                orderTypeImg.image = UIImage(named: "Subscription")
            }else{
                orderTypeImg.image = UIImage(named: "delivery")
            }
        }

        //Payment Details
        //Image
        if orderDetailsArray[0].PaymentIcon != nil{
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(orderDetailsArray[0].PaymentIcon!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            paymentTypeImg.kf.setImage(with: url)
        }else{
            if isArchieve! == true{
                paymentTypeImg.image = UIImage(named: "")
            }else{
                if paymentIcon != ""{
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(paymentIcon)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    paymentTypeImg.kf.setImage(with: url)
                }else{
                    paymentTypeImg.image = UIImage(named: "")
                }
            }
        }
        
        paymentTypeImg.isHidden = false
        paymentTypeLbl.isHidden = false
        if AppDelegate.getDelegate().appLanguage == "English"{
            if orderDetailsArray[0].OrderPaymentDetails != nil{
                if orderDetailsArray[0].OrderPaymentDetails!.count > 0{
                    paymentTypeLbl.text = "\(orderDetailsArray[0].PaymentType) ****\(orderDetailsArray[0].OrderPaymentDetails![0].LastDigits)"
                }else{
                    paymentTypeLbl.text = orderDetailsArray[0].PaymentType
                }
            }else{
                paymentTypeLbl.text = orderDetailsArray[0].PaymentType
            }
        }else{
            if orderDetailsArray[0].OrderPaymentDetails != nil{
                if orderDetailsArray[0].OrderPaymentDetails!.count > 0{
                    paymentTypeLbl.text = "\(orderDetailsArray[0].PaymentTypeAr) ****\(orderDetailsArray[0].OrderPaymentDetails![0].LastDigits)"
                }else{
                    paymentTypeLbl.text = orderDetailsArray[0].PaymentTypeAr
                }
            }else{
                paymentTypeLbl.text = orderDetailsArray[0].PaymentTypeAr
            }
        }
        
//        let additionals = orderDetailsArray[0].Items!.filter({$0.Additionals!.count > 0})
//        if additionals.count > 0{
//            self.lineLbl.isHidden = true
//        }else{
//            self.lineLbl.isHidden = false
//        }

        //Price Details
        //if AppDelegate.getDelegate().appLanguage == "English"{
            self.totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].TotalPrice.withCommas())"
            self.discountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        if orderDetailsArray[0].DeliveryCharge > 0{
            self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].DeliveryCharge.withCommas())"
        }else if orderDetailsArray[0].DeliveryCharge == 0{
            self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Free", value: "", table: nil))!)"
        }else{
            self.deliveryChargesLbl.text = ""
        }
            self.vatLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].Vat.withCommas())"
            self.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].NetTotal.withCommas())"
            self.btmNetAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].NetTotal.withCommas())"
            if orderDetailsArray[0].Discount! == 0 || orderDetailsArray[0].Discount! == 0.0{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].Discount!.withCommas())"
            }else{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].Discount!.withCommas())"
            }
//        }else{
//            self.totalAmountLbl.text = "\(orderDetailsArray[0].TotalPrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            self.discountLbl.text = "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            self.deliveryChargesLbl.text = "\(orderDetailsArray[0].DeliveryCharge.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            self.vatLbl.text = "\(orderDetailsArray[0].Vat.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            self.netAmountLbl.text = "\(orderDetailsArray[0].NetTotal.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            if orderDetailsArray[0].Discount! == 0 || orderDetailsArray[0].Discount! == 0.0{
//                couponDiscountLbl.text = "\(orderDetailsArray[0].Discount!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            }else{
//                couponDiscountLbl.text = "\(orderDetailsArray[0].Discount!.withCommas()) - \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//            }
//        }
        self.vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(orderDetailsArray[0].VatPercentage)%) :"
        
        if orderDetailsArray[0].TotalPrice == 0 && orderDetailsArray[0].Vat == 0{
            self.priceDetailsMainView.isHidden = true
            self.netTotalView.isHidden = false
        }else{
            self.priceDetailsMainView.isHidden = false
            self.netTotalView.isHidden = true
        }
        
        if orderDetailsArray[0].Wallet != nil{
            if orderDetailsArray[0].Wallet == 0 || orderDetailsArray[0].Wallet == 0.0{
                walletDiscountHeight.constant = 0
                walletDiscountBottom.constant = 0
            }else{
                self.discountNameLblTop.constant = 10
                self.vatLineLbl.isHidden = false
                walletDiscountHeight.constant = 21
                walletDiscountBottom.constant = 10
                //if AppDelegate.getDelegate().appLanguage == "English"{
                    walletDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].Wallet!.withCommas())"
//                }else{
//                    walletDiscountLbl.text = "\(orderDetailsArray[0].Wallet!.withCommas()) - \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//                }
            }
        }else{
            walletDiscountHeight.constant = 0
            walletDiscountBottom.constant = 0
        }
        
        if orderDetailsArray[0].OrderStatus == "Cancel" || orderDetailsArray[0].OrderStatus == "Reject"{
            self.orderStatusLbl.textColor = #colorLiteral(red: 0.7522415519, green: 0.1044703498, blue: 0.1641884744, alpha: 1)
        }else{
            self.orderStatusLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.6196078431, blue: 0.262745098, alpha: 1)
        }
        
        //SubScription
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.topFrequencyNameLbl.text = orderDetailsArray[0].FrequencyNameEn
        }else{
            self.topFrequencyNameLbl.text = orderDetailsArray[0].FrequencyNameAr
        }
        
        if isArchieve == true{
            self.bottomBtnsMainView.isHidden = true
            self.bottomBtnsViewHeight.constant = 0
        }else{
            if orderDetailsArray[0].IsSubscription != nil{
                if orderDetailsArray[0].IsSubscription! == true{
                    self.bottomBtnsMainView.isHidden = true
                    self.bottomBtnsViewHeight.constant = 0
        //            if orderDetailsArray[0].Upcoming! == 1{
        //                if self.orderDetailsArray[0].IsSuspended! == 1 {
        //                    if self.orderDetailsArray[0].PaymentProcess! == true {
        //                        self.resumeBtn.isHidden = false
        //                        self.orderAgainBtn.isHidden = false
        //                        self.orderAgainBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Retry", value: "", table: nil))!, for: .normal)
        //                        self.resumeBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Card", value: "", table: nil))!, for: .normal)
        //                    } else {
        //                        if orderDetailsArray[0].IsPause! == 1{
        //                            self.orderAgainBtn.isHidden = true
        //                            self.resumeBtn.isHidden = true
        //                            //self.resumeBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resume", value: "", table: nil))!, for: .normal)
        //                            self.bottomBtnsMainView.isHidden = true
        //                            self.bottomBtnsViewHeight.constant = 0
        //                        }else{
        //                            self.resumeBtn.isHidden = true
        //                            self.orderAgainBtn.isHidden = false
        //                            self.orderAgainBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pause", value: "", table: nil))!, for: .normal)
        //                        self.bottomBtnsMainView.isHidden = false
        //                        self.bottomBtnsViewHeight.constant = 74
        //                        }
        //                    }
        //                } else {
        //                    if orderDetailsArray[0].IsPause! == 1{
        //                        self.orderAgainBtn.isHidden = true
        //                        self.resumeBtn.isHidden = true
        //                        //self.resumeBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resume", value: "", table: nil))!, for: .normal)
        //                        self.bottomBtnsMainView.isHidden = true
        //                        self.bottomBtnsViewHeight.constant = 0
        //                    }else{
        //                        self.resumeBtn.isHidden = true
        //                        self.orderAgainBtn.isHidden = false
        //                        self.orderAgainBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pause", value: "", table: nil))!, for: .normal)
        //                    self.bottomBtnsMainView.isHidden = false
        //                    self.bottomBtnsViewHeight.constant = 74
        //                    }
        //                }
        //            }else{
        //                self.bottomBtnsMainView.isHidden = true
        //                self.bottomBtnsViewHeight.constant = 0
        //            }
                }else{
                    resumeBtn.isHidden = true
                    self.orderAgainBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))!, for: .normal)
                    self.bottomBtnsMainView.isHidden = false
                    self.bottomBtnsViewHeight.constant = 74
                }
            }else{
                resumeBtn.isHidden = true
                self.orderAgainBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))!, for: .normal)
                self.bottomBtnsMainView.isHidden = false
                self.bottomBtnsViewHeight.constant = 74
            }
        }

        if isArchieve == true{
            self.rateBtnView.isHidden = true
            self.ratingMainView.isHidden = true
            favouriteViewHeight.constant = 0
            favouriteBtn.isHidden = true
        }else{
            //Favourite Functionality
            if orderDetailsArray[0].Favourite == false{
                favouriteBtn.isHidden = false
                favouriteBtn.setImage(UIImage(named: "heart_fill"), for: .normal)
                favouriteViewHeight.constant = 0
            }else{
                favouriteBtn.isHidden = true
                if orderDetailsArray[0].FavouriteName != nil{
                    if orderDetailsArray[0].FavouriteName! == ""{
                        favouriteViewHeight.constant = 0
                    }else{
                        favouriteViewHeight.constant = 40
                        favouriteNameLbl.text = orderDetailsArray[0].FavouriteName!
                    }
                }else{
                    favouriteViewHeight.constant = 0
                }
            }
            //Rating Functionality
            if orderDetailsArray[0].OrderStatus == "Close"{
                if orderDetailsArray[0].OrderRatings != nil{
                    if orderDetailsArray[0].OrderRatings!.count > 0{
                        self.ratingMainView.isHidden = false
                        self.rateBtnView.isHidden = true
                        self.rateView.rating = Double(orderDetailsArray[0].OrderRatings![0].Value)
                        if orderDetailsArray[0].OrderRatings![0].Value == 1{
                            self.rateView.fullImage = UIImage(named: "Rate 1")
                        }else if orderDetailsArray[0].OrderRatings![0].Value == 2{
                            self.rateView.fullImage = UIImage(named: "Rate 2")
                        }else if orderDetailsArray[0].OrderRatings![0].Value == 3{
                            self.rateView.fullImage = UIImage(named: "Rate 3")
                        }else if orderDetailsArray[0].OrderRatings![0].Value == 4{
                            self.rateView.fullImage = UIImage(named: "Rate 4")
                        }else if orderDetailsArray[0].OrderRatings![0].Value == 5{
                            self.rateView.fullImage = UIImage(named: "Rate 5")
                        }else{
                            self.rateView.fullImage = UIImage(named: "empStar")
                            self.ratingMainView.isHidden = true
                            self.rateBtnView.isHidden = true
                        }
                    }else{
                        self.ratingMainView.isHidden = true
                        self.rateBtnView.isHidden = false
                    }
                }else{
                    self.ratingMainView.isHidden = true
                    self.rateBtnView.isHidden = false
                }
            }else{
                self.rateBtnView.isHidden = true
                self.ratingMainView.isHidden = true
            }
        }
        
        DispatchQueue.main.async {
            self.orderItemsTableView.reloadData()
        }
    }
//    func convertToDictionary(from text: String) throws -> [String: String] {
//        guard let data = text.data(using: .utf8) else { return [:] }
//        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
//        return anyResult as? [String: String] ?? [:]
//    }
    //MARK: rate Button Tapped
    @IBAction func rattingBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = orderId
        var orderType = 0
        if self.orderDetailsArray[0].orderType == "Delivery"{
            orderType = 4
        }else if self.orderDetailsArray[0].orderType == "DineIn"{
            orderType = 1
        }else if self.orderDetailsArray[0].orderType == "PickUp"{
            orderType = 3
        }else if self.orderDetailsArray[0].orderType == "DriveThru"{
            orderType = 2
        }
        obj.orderType = orderType
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.StoreName = self.orderDetailsArray[0].StoreName.capitalized
        }else{
            obj.StoreName = self.orderDetailsArray[0].storeNameAr.capitalized
        }
        obj.rateVCDelegate = self
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    //MARK: Favourite Button Tapped
    @IBAction func favouriteBtn_Tapped(_ sender: Any) {
        if self.favouriteBtn.currentImage == UIImage(named: "heart_fill"){
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.textPopUpVCDelegate = self
            obj.id = 0
            obj.modalPresentationStyle = .overFullScreen
            self.present(obj, animated: true, completion: nil)
        }else{
            self.favouriteOrder(FavouriteName: "")
        }
    }
    //MARK: Favourite Service Integration
    func favouriteOrder(FavouriteName: String){
        var dic:[String:Any] = [:]
        if self.favouriteBtn.currentImage == UIImage(named: "heart_fill"){
            dic = ["OrderId": orderId, "FavouriteName":FavouriteName, "RequestBy":2]
        }else{
            dic = ["OrderId": orderId, "RequestBy":2]
        }
        CartModuleServices.ToggleFevOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                var message = data.MessageEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    message = data.MessageEn
                }else{
                    message = data.MessageAr
                }
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                    //Favourite functionality
                    if self.favouriteBtn.currentImage == UIImage(named: "fav_unselected"){
                        self.favouriteBtn.isHidden = true
                    }else{
                        self.favouriteBtn.isHidden = false
                    }
                    if self.isArchieve == false{
                        self.getOrderDetailsService()
                    }else if self.isArchieve == true{
                        self.getOldOrderDetailsService()
                    }else{
                        self.getOrderDetailsService()
                    }
                }))
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Delivery Frequency Button Tapped
    @IBAction func DFPopUpDismissBtn_Tapped(_ sender: Any) {
        self.DFPopUpMainView.isHidden = true
    }
    //MARK: Resume Button Tapped
    @IBAction func resumeBtn_Tapped(_ sender: Any) {
        if resumeBtn.currentTitle == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Card", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageCardsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
            obj.isSubscription = orderDetailsArray[0].IsSubscription!
            obj.orderId = orderId
            
            if orderDetailsArray[0].SubscriptionId != nil{
                obj.subscriptionId = orderDetailsArray[0].SubscriptionId!
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TimeSlotVC") as! TimeSlotVC
//            obj.modalPresentationStyle = .overCurrentContext
//            obj.timeSlotVCDelegate = self
//            obj.whereObj = 1
//            obj.shipTimeSlotDate = DateConverts.convertDateToString(date: Date(), dateformatType: .date)
//            obj.selectSlotId = 0
//            obj.modalPresentationStyle = .overFullScreen
//            self.present(obj, animated: true, completion: nil)
        }
    }
    @objc func currentTimeGetService(){
        let dic = ["RequestBy":2] as [String : Any]
        CartModuleServices.GetCurrentTimeService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                //print(data)
                DispatchQueue.main.async {
                    var time = data.Data
                    time = time.components(separatedBy: "T")[0]
                    self.currentDate = DateConverts.convertStringToDate(date: time, dateformatType: .dateR)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Shipping Method
    func shippingMethodDetailsGetService(){
        let dic:[String : Any] = ["OrderId": orderId, "RequestBy":2]
        CartModuleServices.OrderShippingMethodService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if data.MessageEn != nil{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn!)
                    }
                }else{
                    if data.MessageAr != nil{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr!)
                    }
                }
            }else{
                self.shippingMethodsArray = data.Data!.filter({$0.DisplayNameEn == self.orderDetailsArray[0].ShippingMethodEn})
                if self.shippingMethodsArray.count > 0{
                    self.shippingMethodsArray[0].isSelect = true
                    self.slotIdSelect()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func slotIdSelect(){
        if shippingMethodsArray.count == 1{
            let shipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: ".")[0]
            firstShipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0]
            firstSelectShipId = shippingMethodsArray[0].Id
            var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
            var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
            let currentShipDate = DateConverts.convertStringToDate(date: shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0], dateformatType: .dateR)
            if currentShipDate > currentDate{
                let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                if time.count > 0{
                    shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                    shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                    selectSlotId = time[0].Id
                    for i in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                        if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
                            shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                        }
                    }
                    print("Match")
                }else{
                    print("Not Match")
                    for i in 1...shippingMethodsArray[0].FutureDays{
                        nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                        let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                        if time.count > 0{
                            shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                            shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                            selectSlotId = time[0].Id
                            for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                }
                            }
//                            if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                            }
                            print("Match")
                            break
                        }else{
                            print("Not Match")
                        }
                    }
                }
            }else{
                let timeSlot = expectTimeSlot(presentDate: shipDate, index: 0)
                if timeSlot == "NotMatch"{
                    let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: 0)
                    if firstTimeSlot == "NotMatch"{
                        for i in 1...shippingMethodsArray[0].FutureDays{
                            nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                            let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                            if time.count > 0{
                                shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                selectSlotId = time[0].Id
                                for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                    if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                        shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                    }
                                }
//                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                                }
                                print("Match")
                                break
                            }else{
                                print("Not Match")
                            }
                        }
                    }else{
                        print("Match")
                    }
                }else{
                    print("Match")
                }
            }
        }
    }
    func expectTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        var status = ""
        for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
            let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
            let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
            if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: startTime, dateformatType: .dateNTimeA) as NSDate, endTime: DateConverts.convertStringToDate(date: endTime, dateformatType: .dateNTimeA) as NSDate) == true{
                if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                    shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                  shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                    selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                    status = "Match"
                    break
                }else{
                    status = "NotMatch"
                }
            }else{
                status = "NotMatch"
            }
        }
        print("Time Slot Status \(status)")
        return status
    }
    func selectFirstTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        let currentDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
        var status = ""
        let start = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![0].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        let end = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: start, dateformatType: .dateNTime) as NSDate, endTime: DateConverts.convertStringToDate(date: end, dateformatType: .dateNTime) as NSDate) == true || currentDate < start{
            for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
                let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
                let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                if status == "NotMatch"{
                    if currentDate < startTime && currentDate < endTime{
                        if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                            shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                            shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                            shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                            selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                            status =  "Match"
                            break
                        }else{
                            status =  "NotMatch"
                        }
                    }else{
                        status = "NotMatch"
                    }
                }else{
                    status = "NotMatch"
                }
            }
        }else{
            status = "NotMatch"
        }
        print("First Time Slot Status \(status)")
        return status
    }
    func isTimeStampCurrent(timeStamp:NSDate, startTime:NSDate, endTime:NSDate)->Bool{
        timeStamp.earlierDate(endTime as Date) == timeStamp as Date && timeStamp.laterDate(startTime as Date) == timeStamp as Date
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension ReOrderVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == orderItemsTableView{
            if orderDetailsArray.count > 0{
                return self.orderDetailsArray[0].Items!.count
            }else{
                return 0
            }
        }else{
            if orderDetailsArray.count > 0{
                return self.orderDetailsArray[0].FrequencyOptions!.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == orderItemsTableView{
            var additional = [""]
            if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != nil{
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }
            
            var cellHeight = 90
            if isArchieve == true{
                cellHeight = 75
            }else{
                cellHeight = 90
            }
            
            var rewardPoints = 0
            if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints != nil{
                if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints! > 0{
                    rewardPoints = 31
                }else{
                    rewardPoints = 0
                }
            }
            
            var noteHeight = 0
            //Comments
            if let _ = orderDetailsArray[0].Items![indexPath.row].ItemComments{
                if orderDetailsArray[0].Items![indexPath.row].ItemComments! != ""{
                    let height = heightForView(text: "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                    noteHeight = Int(10 + height)
                }else{
                    noteHeight = 0
                }
            }else{
                noteHeight = 0
            }
            
            if orderDetailsArray[0].Items![indexPath.row].Additionals != nil{
                if orderDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...orderDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalEn) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty)")
                        }else{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalAr) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty)")
                        }
                    }
                }
                if additional.count > 1{
                    additional.remove(at: 0)
                    var height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                    height = CGFloat(cellHeight + 5) + height
                    print(height)
                    return height + CGFloat(rewardPoints + noteHeight)
                }else{
                    return CGFloat(cellHeight + rewardPoints + noteHeight)
                }
            }else{
                return CGFloat(cellHeight + rewardPoints + noteHeight)
            }
        }else{
            return 50
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == orderItemsTableView{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!) as! OrderItemsTVCell
            
            if isArchieve == true{
                cell.itemImgWidth.constant = 0
                cell.itemImgLeading.constant = 0
                cell.itemImgHeight.constant = 55
            }else{
                cell.itemImgWidth.constant = 47
                cell.itemImgLeading.constant = 10
                cell.itemImgHeight.constant = 70
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                if orderDetailsArray[0].Items![indexPath.row].itemType == "Regular" || orderDetailsArray[0].Items![indexPath.row].itemType == "No Item Type"{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn)"
                }else{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn) (\(orderDetailsArray[0].Items![indexPath.row].itemType))"
                }
                cell.itemQuantityLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].quantity) X \(orderDetailsArray[0].Items![indexPath.row].ItemPrice.withCommas())"
            }else{
                if orderDetailsArray[0].Items![indexPath.row].itemType == "Regular" || orderDetailsArray[0].Items![indexPath.row].itemType == "No Item Type"{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr)"
                }else{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr) (\(orderDetailsArray[0].Items![indexPath.row].itemTypeAr))"
                }
                cell.itemQuantityLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemPrice.withCommas()) X \(orderDetailsArray[0].Items![indexPath.row].quantity)"
            }
            
            if isArchieve == false{
                cell.itemPriceLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].TotalPrice.withCommas())"
            }else{
                var itemPrice = 0.0
                if orderDetailsArray[0].Items![indexPath.row].Additionals != nil{
                    if orderDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                        for i in 0...orderDetailsArray[0].Items![indexPath.row].Additionals!.count  - 1{
                            itemPrice = itemPrice + (Double(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty) * orderDetailsArray[0].Items![indexPath.row].Additionals![i].additionalprice)
                        }
                    }
                }
                itemPrice = itemPrice + orderDetailsArray[0].Items![indexPath.row].ItemPrice
                itemPrice = itemPrice * Double(orderDetailsArray[0].Items![indexPath.row].quantity)
                cell.itemPriceLbl.text = "\(itemPrice.withCommas())"
            }
            
            cell.additionalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Additional", value: "", table: nil))!
            
            if indexPath.row%2 == 0{
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.7647058824, green: 0.7647058824, blue: 0.7647058824, alpha: 1)
            }
            
            var additional = [""]
            if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != nil{
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }
            if orderDetailsArray[0].Items![indexPath.row].Additionals != nil{
                if orderDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...orderDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                        if orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty == 0{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalEn) x 1")
                            }else{
                                additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalAr) x 1")
                            }
                        }else{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalEn) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty)")
                            }else{
                                additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].AdditionalAr) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Qty)")
                            }
                        }
                        
                    }
                }
            }
            
            if additional.count > 1{
                additional.remove(at: 0)
                cell.additionalsLbl.text = additional.joined(separator: ",")
                let height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
                cell.additionalsViewHeight.constant = 5 + height
            }else{
                cell.additionalsLbl.text = ""
                cell.additionalsViewHeight.constant = 0
            }
            
            //Comments
            if let _ = orderDetailsArray[0].Items![indexPath.row].ItemComments{
                if orderDetailsArray[0].Items![indexPath.row].ItemComments! != ""{
                    cell.noteLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)"
                    let height = heightForView(text: "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
                    cell.noteViewHeight.constant = 10 + height
                }else{
                    cell.noteLbl.text = ""
                    cell.noteViewHeight.constant = 0
                }
            }else{
                cell.noteLbl.text = ""
                cell.noteViewHeight.constant = 0
            }
            
            if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints != nil{
                if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints! > 0{
                    cell.redeemPointsViewHeight.constant = 31
                    cell.redeemPointsMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "It’s on us!", value: "", table: nil))!) \(orderDetailsArray[0].Items![indexPath.row].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied.", value: "", table: nil))!)"
                    cell.redeemPointsLbl.text = "- \(orderDetailsArray[0].Items![indexPath.row].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!)"
                }else{
                    cell.redeemPointsViewHeight.constant = 0
                }
            }else{
                cell.redeemPointsViewHeight.constant = 0
            }

            cell.grinderViewHeight.constant = 0
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(orderDetailsArray[0].Items![indexPath.row].ItemImage)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
                    
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.orderItemsTableView.layoutIfNeeded()
                self.orderItemsTableViewHeight.constant = self.orderItemsTableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!, for: indexPath) as! NutritionDetailsTVCell
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nutritionNameLbl.text = orderDetailsArray[0].FrequencyOptions![indexPath.row].OptionEn!
            }else{
                cell.nutritionNameLbl.text = orderDetailsArray[0].FrequencyOptions![indexPath.row].OptionAr!
            }
            cell.nutritionNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            cell.nutritionCalLbl.text = ""
            cell.BGView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == orderItemsTableView{
            print("Select")
        }else{
            let dic:[String:Any] = ["isActive": orderDetailsArray[0].FrequencyOptions![indexPath.row].IsActive!, "Orderid": orderId, "Frequency":orderDetailsArray[0].FrequencyOptions![indexPath.row].Id!,"RequestBy":2]
            SubscriptionModuleServices.PauseOrderService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.DFPopUpMainView.isHidden = true
                        if self.isArchieve == false{
                            self.getOrderDetailsService()
                        }else if self.isArchieve == true{
                            self.getOldOrderDetailsService()
                        }else{
                            self.getOrderDetailsService()
                        }
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
//MARK: Rate VC Delegate
@available(iOS 13.0, *)
extension ReOrderVC:RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            if self.isArchieve == false{
                self.getOrderDetailsService()
            }else{
                self.getOldOrderDetailsService()
            }
        }
    }
}
//MARK: TextPopUp Delegate
@available(iOS 13.0, *)
extension ReOrderVC : TextPopUpVCDelegate{
    func didTapAction(ID: Int, name: String) {
        self.favouriteOrder(FavouriteName: name)
    }
}
// MARK: Date Delegate Methods
@available(iOS 13.0, *)
extension ReOrderVC:TimeSlotVCDelegate{
    func didTapAction(expectDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateDots))
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionPauseVC") as! SubscriptionPauseVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.subscriptionID = self.orderDetailsArray[0].SubscriptionId!
        if self.orderDetailsArray[0].OrderId != nil{
            obj.orderId = self.orderDetailsArray[0].OrderId!
        }
        obj.frequency = self.orderDetailsArray[0].FrequencyId!
        obj.shippingTime = "\(startTime) - \(endTime)"
        obj.slotId = SlotId
        obj.startDate = "\(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateR)) 00:00:00.000"
        obj.shippingMethod = self.orderDetailsArray[0].ShippingMethodEn!
        obj.modalPresentationStyle = .overFullScreen
        obj.subscriptionPauseVCDelegate = self
        self.present(obj, animated: true, completion: nil)
    }
}
extension ReOrderVC : SubscriptionPauseVCDelegate{
    func didTapAction(status: String) {
        if status == "Done"{
            self.getOrderDetailsService()
        }
    }
}
// MARK: Calender Delegate Methods
extension ReOrderVC:PauseVCDelegate{
    func didTapAction(getDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertStringToStringDates(inputDateStr: "\(getDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR))
        print("\(startTime) \(endTime)")
        let date = DateConverts.convertStringToStringDates(inputDateStr: "\(getDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionPauseVC") as! SubscriptionPauseVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.subscriptionID = self.orderDetailsArray[0].SubscriptionId!
        if self.orderDetailsArray[0].OrderId != nil{
            obj.orderId = self.orderDetailsArray[0].OrderId!
        }
        obj.frequency = self.orderDetailsArray[0].FrequencyId!
        obj.shippingTime = "\(startTime) - \(endTime)"
        obj.slotId = SlotId
        obj.startDate = "\(date) 00:00:00.000"
        obj.shippingMethod = self.orderDetailsArray[0].ShippingMethodEn!
        obj.modalPresentationStyle = .overFullScreen
        obj.subscriptionPauseVCDelegate = self
        self.present(obj, animated: true, completion: nil)
    }
}
extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
}
