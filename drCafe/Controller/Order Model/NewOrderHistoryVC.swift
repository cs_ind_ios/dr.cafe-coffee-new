//
//  NewOrderHistoryVC.swift
//  drCafe
//
//  Created by Devbox on 06/07/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class NewOrderHistoryVC: UIViewController {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var noRecordsFoundLbl: UILabel!
    
    static var instance: NewOrderHistoryVC!
    var pageNumber:Int = 1
    var totalRows:Int = 0
    private let refreshControl = UIRefreshControl()
    var mainArray = [OrderHistoryDetailsModel]()
    var filterArray = [OrderHistoryDetailsModel]()
    var orderType = 0
    var orderStatus = 0
    var favourite = false
    var whereObj = 0
    var isSubScription = false
    
    var CategoryDetailsArray:[[String:String]] = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Order", value: "", table: nil))!,"Image":"history","isSelect":"1"],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!,"Image":"heart_fill","isSelect":"0"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!,"Image":"delivery","isSelect":"0"],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!,"Image":"Home Pick up","isSelect":"0"],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Dine In", value: "", table: nil))!,"Image":"dinein","isSelect":"0"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!,"Image":"Subscription","isSelect":"0"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancelled", value: "", table: nil))!,"Image":"validation-cross","isSelect":"0"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NewOrderHistoryVC.instance = self
        self.tabBarController?.tabBar.isHidden = true
        //self.ordersTableView.rowHeight = UITableView.automaticDimension
        //self.ordersTableView.estimatedRowHeight = 200
        self.ordersTableView.delegate = self
        self.ordersTableView.dataSource = self
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
       
        DispatchQueue.main.async {
            SaveAddressClass.getOrderHistoryArray.removeAll()
            self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
        }
       
        ordersTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            categoryCollectionView.semanticContentAttribute = .forceLeftToRight
            ordersTableView.semanticContentAttribute = .forceLeftToRight
        }else{
            categoryCollectionView.semanticContentAttribute = .forceRightToLeft
            ordersTableView.semanticContentAttribute = .forceRightToLeft
        }
        self.createUserActivity()
    }
    // Siri Shortcts
    func createUserActivity(){
        let activity = NSUserActivity(activityType:UserActivityType.OrderHistory)
        activity.title = "Order History"
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        self.userActivity = activity
        self.userActivity?.becomeCurrent()
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if whereObj == 1{
            self.navigationController?.popViewController(animated: true)
        }

        self.tabBarController?.tabBar.isHidden = true
        if let index = self.ordersTableView.indexPathForSelectedRow{
            self.ordersTableView.deselectRow(at: index, animated: true)
        }
        
        self.BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            noRecordsFoundLbl.textColor = .white
        }else{
            noRecordsFoundLbl.textColor = .darkGray
        }
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
        }else{
            self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
        }
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order History", value: "", table: nil))!
        noRecordsFoundLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
        if favourite == true{
            for i in 0...CategoryDetailsArray.count-1{
                self.CategoryDetailsArray[i]["isSelect"] = "0"
            }
            CategoryDetailsArray[1]["isSelect"]! = "1"
            categoryCollectionView.reloadData()
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 11{
            LoginVC.instance.whereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Refresh Data
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        categoryCollectionView.reloadData()
        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: favourite)
    }
    //MARK: Get Order History Data
    func getOrderHistoryService(orderType:Int, orderStatus:Int, favourite:Bool){
        if pageNumber == 1 {
            ANLoader.showLoading("", disableUI: false)
        }
        let dic:[String : Any]!
        if isSubScription == false{
            if favourite == false{
                dic = ["userId":UserDef.getUserId(),
                "Pageno": "\(pageNumber)",
                    "Pagesize": "10", "OrderType":orderType, "OrderStatusId":orderStatus,"IsSubscription": false, "RequestBy":2]
            }else{
                dic = ["userId":UserDef.getUserId(),
                "Pageno": "\(pageNumber)",
                    "Pagesize": "10", "Favourite": true, "RequestBy":2]
            }
        }else{
            dic = ["userId":UserDef.getUserId(),
            "Pageno": "\(pageNumber)",
                "Pagesize": "10", "IsSubscription": true, "RequestBy":2]
        }
//        dic = ["userId":UserDef.getUserId(),
//        "Pageno": "3",
//            "Pagesize": "100", "OrderType":orderType, "OrderStatusId":orderStatus,"IsSubscription": false, "RequestBy":2]
        CartModuleServices.OrderHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
                self.navigationController?.popViewController(animated: true)
                return
            }else{
                if self.pageNumber == 1 {
                    SaveAddressClass.getOrderHistoryArray.removeAll()
                }
            
                if data.Orders!.count > 0{
                    for i in 0...data.Orders!.count - 1{
                        SaveAddressClass.getOrderHistoryArray.append(data.Orders![i])
                    }
                }
                
                self.totalRows = data.TotalOrders
                self.mainArray.removeAll()
                self.mainArray = SaveAddressClass.getOrderHistoryArray
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    if self.mainArray.count > 0{
                        self.noRecordsFoundLbl.isHidden = true
                        self.ordersTableView.isHidden = false
                        self.ordersTableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }else{
                        self.noRecordsFoundLbl.isHidden = false
                        self.ordersTableView.isHidden = true
                    }
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
@available(iOS 13.0, *)
extension NewOrderHistoryVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if CategoryDetailsArray.count > 0{
            return CategoryDetailsArray.count
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        categoryCollectionView.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
        
        cell.filterNameLbl.text = "\(String(describing: CategoryDetailsArray[indexPath.row]["Name"]!))"
        cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14)
        
        if CategoryDetailsArray[indexPath.row]["isSelect"]! == "1"{
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
        }else{
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
        }
        cell.filterImg.image = UIImage(named: "\(String(describing: CategoryDetailsArray[indexPath.row]["Image"]!))")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0...CategoryDetailsArray.count-1{
            self.CategoryDetailsArray[i]["isSelect"] = "0"
        }
        DispatchQueue.main.async { [self] in
            self.CategoryDetailsArray[indexPath.row]["isSelect"] = "1"
            if indexPath.row == 0{
                self.pageNumber = 1
                self.orderType = 0
                self.orderStatus = 0
                self.favourite = false
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 1{
                self.filterArray = self.mainArray.filter({$0.Favourite == true})
                self.pageNumber = 1
                self.orderType = 0
                self.orderStatus = 0
                self.favourite = true
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 2{
                self.pageNumber = 1
                self.orderType = 4
                self.orderStatus = 0
                self.favourite = false
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 3{
                self.pageNumber = 1
                self.orderType = 3
                self.orderStatus = 0
                self.favourite = false
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 4{
                self.pageNumber = 1
                self.orderType = 1
                self.orderStatus = 0
                self.favourite = false
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 5{
                self.pageNumber = 1
                self.orderType = 0
                self.orderStatus = 0
                self.favourite = false
                self.isSubScription = true
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }else if indexPath.row == 6{
                self.pageNumber = 1
                self.orderType = 0
                self.orderStatus = 4
                self.favourite = false
                self.isSubScription = false
                self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
            }
            self.categoryCollectionView.reloadData()
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension NewOrderHistoryVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if totalRows > self.mainArray.count {
            return self.mainArray.count + 1
        }else{
            return self.mainArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.mainArray.count == indexPath.row {
           return 50
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
            self.ordersTableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
            cell.activityIndicatorView.startAnimating()
            cell.selectionStyle = .none
            return cell
        }else{
            self.ordersTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewOrderHistoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewOrderHistoryTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewOrderHistoryTVCell", value: "", table: nil))!, for: indexPath) as! NewOrderHistoryTVCell
            
            let dic = self.mainArray[indexPath.row]
            cell.invoiceNumLbl.text = "#\(dic.InvoiceNo)"
            if dic.SectionType == 2{
                let expectDate = dic.ExpectedTime.components(separatedBy: "T")[0]
                var shipTime = dic.ShippingTime.components(separatedBy: " - ")
                if shipTime.count == 1{
                    shipTime = dic.ShippingTime.components(separatedBy: "-")
                }
                if dic.ExpectedTime != ""{
                    if shipTime.count > 0 && dic.ShippingTime != ""{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = "\(shipTime[0]) to \(shipTime[1])"
                        cell.orderTimeLbl.font = UIFont(name: "Avenir Book", size: 12.0)
                    }else{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = ""
                        cell.orderTimeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
                    }
                }else{
                    cell.orderDateLbl.text = ""
                    cell.orderTimeLbl.text = ""
                }
            }else{
                if dic.ExpectedTime != ""{
                    let expectDate = dic.ExpectedTime.components(separatedBy: ".")[0]
                    cell.orderDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    cell.orderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                }else{
                    cell.orderDateLbl.text = ""
                    cell.orderTimeLbl.text = ""
                }
                cell.orderTimeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            }
            cell.amountLbl.text = "\(dic.NetPrice.withCommas())"
            cell.amountCurrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
            
            if dic.OrderStatus == "Cancel" || dic.OrderStatus == "Reject"{
                cell.orderStatusLbl.textColor = #colorLiteral(red: 0.7522415519, green: 0.1044703498, blue: 0.1641884744, alpha: 1)
            }else{
                cell.orderStatusLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.6196078431, blue: 0.262745098, alpha: 1)
            }
            
            //Order Status Functionality
            if dic.OrderStatus == "Close"{
                if dic.OrderType == "4"{
                    cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
                }else{
                    cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served", value: "", table: nil))!
                }
            }else{
                cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: dic.OrderStatus, value: "", table: nil))!
            }
            
            cell.orderTypeLbl.textColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            if let _ = dic.OrderType{
                if dic.OrderType! == "1"{
                    cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DineIn", value: "", table: nil))!
                    cell.orderTypeImg.image = UIImage(named: "dinein")
                }else if dic.OrderType! == "2"{
                    cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DriveThru", value: "", table: nil))!
                    cell.orderTypeImg.image = UIImage(named: "Drive Thru")
                }else if dic.OrderType! == "3"{
                    cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
                    cell.orderTypeImg.image = UIImage(named: "Home Pick up")
                }else if dic.OrderType! == "4"{
                    if let _ = dic.IsSubscription{
                        if dic.IsSubscription! == true{
                            cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!
                            cell.orderTypeImg.image = UIImage(named: "Subscription")
                            //cell.orderTypeLbl.textColor = .darkGray
                        }else{
                            cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
                            cell.orderTypeImg.image = UIImage(named: "delivery")
                        }
                    }else{
                        cell.orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
                        cell.orderTypeImg.image = UIImage(named: "delivery")
                    }
                }
                if dic.OrderType! == "4"{
                    if dic.DeliveryAddress != nil{
                        cell.addressLbl.text = dic.DeliveryAddress![0].Details
                    }else{
                        cell.addressLbl.text = ""
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.addressLbl.text = dic.Store![0].StoreAddressEn
                    }else{
                        cell.addressLbl.text = dic.Store![0].StoreAr
                    }
                }
            }
            
            //if dic.IsSubscription! == false{
                cell.subscriptionImgWidth.constant = 0
                cell.subscriptionImgLeading.constant = 0
            //}else{
                //cell.subscriptionImgWidth.constant = 15
                //cell.subscriptionImgLeading.constant = 10
            //}
            
            //Payment Details
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.PaymentIcon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.paymentTypeImg.kf.setImage(with: url)
            
            cell.paymentTypeImg.isHidden = false
            cell.paymentTypeLbl.isHidden = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                if dic.OrderPaymentDetails != nil{
                    if dic.OrderPaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(dic.PaymentType) ****\(dic.OrderPaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = dic.PaymentType
                    }
                }else{
                    cell.paymentTypeLbl.text = dic.PaymentType
                }
            }else{
                if dic.OrderPaymentDetails != nil{
                    if dic.OrderPaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(dic.PaymentTypeAr) ****\(dic.OrderPaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = dic.PaymentTypeAr
                    }
                }else{
                    cell.paymentTypeLbl.text = dic.PaymentTypeAr
                }
            }
            
            if self.mainArray[indexPath.row].IsArchive == true{
                cell.favouriteBtn.isHidden = true
                cell.rateViewWidth.constant = 0
                cell.rateMainView.isHidden = true
                cell.rateBtnView.isHidden = true
            }else{
                //Favourite functionality
                if dic.Favourite == true{
                    if dic.FavouriteName! == ""{
                        cell.favouriteNameLbl.text = ""
                        cell.favourireNameViewHeight.constant = 0
                    }else{
                        cell.favouriteNameLbl.text = dic.FavouriteName!
                        cell.favourireNameViewHeight.constant = 40
                    }
                    cell.favouriteBtn.isHidden = true
                }else{
                    cell.favouriteNameLbl.text = ""
                    cell.favourireNameViewHeight.constant = 0
                    cell.favouriteBtn.isHidden = false
                }
                
                //Rating functionality
                if dic.OrderStatus == "Close"{
                    cell.rateViewWidth.constant = 110
                    if dic.OrderRatings != nil{
                        cell.rattingView.rating = Double(dic.OrderRatings![0].Value)
                        cell.rateBtnView.isHidden = true
                        cell.rateMainView.isHidden = false
                        if dic.OrderRatings![0].Value == 1{
                            cell.rattingView.fullImage = UIImage(named: "Rate 1")
                        }else if dic.OrderRatings![0].Value == 2{
                            cell.rattingView.fullImage = UIImage(named: "Rate 2")
                        }else if dic.OrderRatings![0].Value == 3{
                            cell.rattingView.fullImage = UIImage(named: "Rate 3")
                        }else if dic.OrderRatings![0].Value == 4{
                            cell.rattingView.fullImage = UIImage(named: "Rate 4")
                        }else if dic.OrderRatings![0].Value == 5{
                            cell.rattingView.fullImage = UIImage(named: "Rate 5")
                        }else{
                            cell.rattingView.fullImage = UIImage(named: "empStar")
                            cell.rateBtnView.isHidden = true
                            cell.rateMainView.isHidden = true
                        }
                    }else{
                        cell.rateBtnView.isHidden = false
                        cell.rateMainView.isHidden = true
                    }
                }else{
                    cell.rateViewWidth.constant = 0
                    cell.rateMainView.isHidden = true
                    cell.rateBtnView.isHidden = true
                }
            }
            
            cell.favouriteBtn.tag = indexPath.row
            cell.favouriteBtn.addTarget(self, action: #selector(favouriteBtn_Tapped(sender:)), for: .touchUpInside)
            
            cell.rateOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Order", value: "", table: nil))!
            cell.rateBtn.tag = indexPath.row
            cell.rateBtn.addTarget(self, action: #selector(rateBtn_Tapped(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                if self.mainArray.count == indexPath.row+1 {
                    if totalRows >  self.mainArray.count {
                        pageNumber = pageNumber + 1
                        self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favouriteAction = UIContextualAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!) { (action, view, completion) in
            view.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.9058823529, blue: 0.9450980392, alpha: 1)
            if self.mainArray[indexPath.row].Favourite == false{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.textPopUpVCDelegate = self
                obj.id = indexPath.row
                obj.modalPresentationStyle = .overFullScreen
                self.present(obj, animated: true, completion: nil)
            }else{
                self.favouriteOrder(FavouriteName: "", index: indexPath.row)
            }
            completion(true)
        }
        var name = ""
        var image = ""
        if self.mainArray[indexPath.row].IsSubscription == true{
            name = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "View Details", value: "", table: nil))!
            image = "view details"
        }else{
            name = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))!
            image = "reOrder"
        }
        
        let reOrderInfoAction = UIContextualAction(style: .normal, title: name) { (action, view, completion) in
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ReOrderVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
            obj.orderId = self.mainArray[indexPath.row].OrderId
            obj.isSubscription = self.mainArray[indexPath.row].IsSubscription!
            obj.isArchieve = self.mainArray[indexPath.row].IsArchive
            obj.paymentIcon = self.mainArray[indexPath.row].PaymentIcon
            self.navigationController?.pushViewController(obj, animated: true)
            completion(true)
        }
        
        favouriteAction.image = UIImage(named: "heart_fill")
        favouriteAction.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.9058823529, blue: 0.9450980392, alpha: 1)
        reOrderInfoAction.image = UIImage(named: image)
        reOrderInfoAction.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.9058823529, blue: 0.9450980392, alpha: 1)

        if mainArray[indexPath.row].OrderStatus == "Close" || mainArray[indexPath.row].OrderStatus == "Cancel" || mainArray[indexPath.row].OrderStatus == "Reject"{
            return UISwipeActionsConfiguration(actions: [favouriteAction])
        }else{
            return UISwipeActionsConfiguration(actions: [reOrderInfoAction,favouriteAction])
        }
    }
    //MARK: rate Button Tapped
    @objc func rateBtn_Tapped(sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = self.mainArray[sender.tag].OrderId
        obj.orderType = Int(self.mainArray[sender.tag].OrderType!)!
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.StoreName = self.mainArray[sender.tag].Store![0].StoreNameEn.capitalized
        }else{
            obj.StoreName = self.mainArray[sender.tag].Store![0].StoreAr.capitalized
        }
        obj.rateVCDelegate = self
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    //MARK: Favourite Button Tapped
    @objc func favouriteBtn_Tapped(sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.textPopUpVCDelegate = self
        obj.id = sender.tag
        obj.modalPresentationStyle = .overFullScreen
        self.present(obj, animated: true, completion: nil)
    }
    //MARK: Favourite Service Integration
    func favouriteOrder(FavouriteName: String, index:Int){
        var dic:[String:Any] = [:]
        if self.mainArray[index].Favourite == false{
            dic = ["OrderId": self.mainArray[index].OrderId, "FavouriteName":FavouriteName, "RequestBy":2]
        }else{
            dic = ["OrderId": self.mainArray[index].OrderId, "RequestBy":2]
        }
        CartModuleServices.ToggleFevOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                var message = data.MessageEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    message = data.MessageEn
                }else{
                    message = data.MessageAr
                }
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                    //Favourite functionality
                    if self.mainArray[index].Favourite == false{
                        self.mainArray[index].Favourite = true
                    }else{
                        self.mainArray[index].Favourite = false
                    }
                    self.ordersTableView.reloadData()
                }))
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
        }else{
            if self.mainArray[indexPath.row].IsArchive == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ReOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                obj.orderId = self.mainArray[indexPath.row].OrderId
                obj.isSubscription = self.mainArray[indexPath.row].IsSubscription!
                obj.isArchieve = self.mainArray[indexPath.row].IsArchive
                obj.paymentIcon = self.mainArray[indexPath.row].PaymentIcon
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                if mainArray[indexPath.row].Upcoming != nil{
                    if mainArray[indexPath.row].Upcoming! == 1{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = ReOrderVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                        obj.orderId = self.mainArray[indexPath.row].OrderId
                        obj.isSubscription = self.mainArray[indexPath.row].IsSubscription!
                        obj.isArchieve = self.mainArray[indexPath.row].IsArchive
                        obj.paymentIcon = self.mainArray[indexPath.row].PaymentIcon
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        if mainArray[indexPath.row].OrderStatus == "Close" || mainArray[indexPath.row].OrderStatus == "Cancel" || mainArray[indexPath.row].OrderStatus == "Reject"{
                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                            var obj = ReOrderVC()
                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                            obj.orderId = self.mainArray[indexPath.row].OrderId
                            obj.isSubscription = self.mainArray[indexPath.row].IsSubscription!
                            obj.isArchieve = self.mainArray[indexPath.row].IsArchive
                            obj.paymentIcon = self.mainArray[indexPath.row].PaymentIcon
                            self.navigationController?.pushViewController(obj, animated: true)
                        }else{
                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                            var obj = TrackOrderVC()
                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                            obj.orderId = mainArray[indexPath.row].OrderId
                            obj.orderFrom = 2
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }else{
                    if mainArray[indexPath.row].OrderStatus == "Close" || mainArray[indexPath.row].OrderStatus == "Cancel" || mainArray[indexPath.row].OrderStatus == "Reject"{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = ReOrderVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                        obj.orderId = self.mainArray[indexPath.row].OrderId
                        obj.isSubscription = self.mainArray[indexPath.row].IsSubscription!
                        obj.isArchieve = self.mainArray[indexPath.row].IsArchive
                        obj.paymentIcon = self.mainArray[indexPath.row].PaymentIcon
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = TrackOrderVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                        obj.orderId = mainArray[indexPath.row].OrderId
                        obj.orderFrom = 2
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
    }
}
//MARK: TextPopUp Delegate
@available(iOS 13.0, *)
extension NewOrderHistoryVC : TextPopUpVCDelegate{
    func didTapAction(ID: Int, name: String) {
        self.favouriteOrder(FavouriteName: name, index: ID)
    }
}
//MARK: Rate VC Delegate
@available(iOS 13.0, *)
extension NewOrderHistoryVC:RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            self.getOrderHistoryService(orderType: self.orderType, orderStatus: self.orderStatus, favourite: self.favourite)
        }
    }
}
