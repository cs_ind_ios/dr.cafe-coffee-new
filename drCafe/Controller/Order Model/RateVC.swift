//
//  RateVC.swift
//  drCafe
//
//  Created by Devbox on 21/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: RateVC Delegate Protocol
protocol RateVCDelegate {
    func didTapRating(vendorRating: Double,vendorComment:String,driverRating: Double,driverComment:String)
}

@available(iOS 13.0, *)
class RateVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var vendorRatingTitleLbl: UILabel!
    @IBOutlet weak var vendorRatingView: FloatRatingView!
    @IBOutlet weak var vendorcommentsTV: UITextView!
    
    @IBOutlet weak var driverRatingTitleLbl: UILabel!
    @IBOutlet weak var driverRatingView: FloatRatingView!
    @IBOutlet weak var drivercommentsTV: UITextView!
    @IBOutlet weak var driverMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var noThanksBtn: UIButton!
    
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var rateDriverNameLbl: UILabel!
    
    var rateVCDelegate: RateVCDelegate!
    var vendorRate = Double()
    var driverRate = Double()
    var OrderId = Int()
    var StoreName = String()
    var StoreImage = String()
    var orderType = Int()
    var driverComment = ""
    var vendorComment = ""
    var isHomeVC = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
                
        if orderType == 1 || orderType == 2 || orderType == 3{
            driverMainViewHeight.constant = 0
        }else{
            driverMainViewHeight.constant = 273
        }
        
        // Rating View
        vendorRatingView.delegate = self
        vendorRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        vendorRatingView.type = .wholeRatings
        vendorRatingView.rating = vendorRate
        
        driverRatingView.delegate = self
        driverRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        driverRatingView.type = .wholeRatings
        driverRatingView.rating = driverRate
        
        self.vendorcommentsTV.delegate = self
        self.drivercommentsTV.delegate = self
        
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.vendorcommentsTV.layer.borderColor = borderGray.cgColor
        self.vendorcommentsTV.layer.borderWidth = 1.0
        self.vendorcommentsTV.layer.cornerRadius = 5.0
        self.vendorcommentsTV.textColor = UIColor.lightGray

        self.drivercommentsTV.layer.borderColor = borderGray.cgColor
        self.drivercommentsTV.layer.borderWidth = 1.0
        self.drivercommentsTV.layer.cornerRadius = 5.0
        self.drivercommentsTV.textColor = UIColor.lightGray
        
        vendorcommentsTV.textAlignment = .left
        vendorcommentsTV.semanticContentAttribute = .forceLeftToRight
        drivercommentsTV.textAlignment = .left
        drivercommentsTV.semanticContentAttribute = .forceLeftToRight
        
        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!
        self.rateDriverNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Driver", value: "", table: nil))!
        self.submitBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Submit", value: "", table: nil))!, for: .normal)
        self.noThanksBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Thanks!", value: "", table: nil))!, for: .normal)
        
        // Text View
        if vendorComment == ""{
            self.vendorcommentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Suggestions? Let us Know....", value: "", table: nil))!
        }else{
            self.vendorcommentsTV.text = vendorComment
        }
        
        if vendorRate == 1 || vendorRate == 2{
            vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
        }else if vendorRate == 3 || vendorRate == 4 {
            vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
        }else if vendorRate == 5 {
            vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
        }else{
            vendorRatingTitleLbl.text = ""
        }

        if driverComment == ""{
            self.drivercommentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Suggestions? Let us Know....", value: "", table: nil))!
        }else{
            self.drivercommentsTV.text = driverComment
        }

        if driverRate == 1 || driverRate == 2{
            driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
        }else if driverRate == 3 || driverRate == 4 {
            driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
        }else if driverRate == 5 {
            driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
        }else{
            driverRatingTitleLbl.text = ""
        }
        
        if orderType == 2 || orderType == 3{
            storeName.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!) - \(StoreName.capitalized)"
        }else{
            storeName.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!)"
        }
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            vendorcommentsTV.textAlignment = .left
            vendorcommentsTV.semanticContentAttribute = .forceLeftToRight
            drivercommentsTV.textAlignment = .left
            drivercommentsTV.semanticContentAttribute = .forceLeftToRight
        }else{
            vendorcommentsTV.textAlignment = .right
            vendorcommentsTV.semanticContentAttribute = .forceRightToLeft
            drivercommentsTV.textAlignment = .right
            drivercommentsTV.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        noThanksBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        submitBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isHomeVC == true{
            HomeVC.instance.bottomPopUpBGView.isHidden = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: No Thanks Button Action
    @IBAction func noThanksBtn_Tapped(_ sender: Any) {
        if isHomeVC == true{
            HomeVC.instance.bottomPopUpBGView.isHidden = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Submit Button Action
    @IBAction func submitBtn_Tapped(_ sender: Any) {
        var vendorComment:String = self.vendorcommentsTV.text!
        if vendorComment == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Suggestions? Let us Know....", value: "", table: nil))! {
            vendorComment = ""
        }
        var driverComment:String = self.drivercommentsTV.text!
        if driverComment == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Suggestions? Let us Know....", value: "", table: nil))! {
            driverComment = ""
        }
        
        let orderRatingDic:[String : Any] = ["Value":Int(self.vendorRatingView.rating),
        "Comment":"\(vendorComment)"]
        let driverRatingDic:[String : Any] = ["Value":Int(self.driverRatingView.rating),
        "Comment":"\(driverComment)"]
        let dic:[String : Any] = ["OrderId":"\(String(describing: OrderId))",
            "OrderRating":orderRatingDic,
            "DriverRating":driverRatingDic, "RequestBy":2]
        CartModuleServices.RateOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.rateVCDelegate.didTapRating(vendorRating: self.vendorRatingView.rating, vendorComment: "\(vendorComment)", driverRating: self.driverRatingView.rating, driverComment: "\(driverComment)")
                self.dismiss(animated: true, completion: nil)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
@available(iOS 13.0, *)
extension RateVC: FloatRatingViewDelegate {
// MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        if ratingView == vendorRatingView{
            vendorRate = vendorRatingView.rating
            if vendorRate == 1 || vendorRate == 2{
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if vendorRate == 3 || vendorRate == 4 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if vendorRate == 5 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }else{
                vendorRatingTitleLbl.text = ""
            }
        }else{
            driverRate = driverRatingView.rating
            if driverRate == 1 || driverRate == 2{
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if driverRate == 3 || driverRate == 4 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if driverRate == 5 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }else{
                driverRatingTitleLbl.text = ""
            }
        }
    }
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        if ratingView == vendorRatingView{
            if vendorRate == 1{
                vendorRatingView.fullImage = UIImage(named: "Rate 1")
            }else if vendorRate == 2{
                vendorRatingView.fullImage = UIImage(named: "Rate 2")
            }else if vendorRate == 3{
                vendorRatingView.fullImage = UIImage(named: "Rate 3")
            }else if vendorRate == 4{
                vendorRatingView.fullImage = UIImage(named: "Rate 4")
            }else if vendorRate == 5 {
                vendorRatingView.fullImage = UIImage(named: "Rate 5")
            }else{
                vendorRatingView.fullImage = UIImage(named: "empStar")
            }
            vendorRate = vendorRatingView.rating
            if vendorRate == 1 || vendorRate == 2{
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if vendorRate == 3 || vendorRate == 4 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if vendorRate == 5 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }else{
                vendorRatingTitleLbl.text = ""
            }
        }else{
            if driverRate == 1 {
                driverRatingView.fullImage = UIImage(named: "Rate 1")
            }else if driverRate == 2{
                driverRatingView.fullImage = UIImage(named: "Rate 2")
            }else if driverRate == 3{
                driverRatingView.fullImage = UIImage(named: "Rate 3")
            }else if driverRate == 4{
                driverRatingView.fullImage = UIImage(named: "Rate 4")
            }else if driverRate == 5 {
                driverRatingView.fullImage = UIImage(named: "Rate 5")
            }else{
                driverRatingView.fullImage = UIImage(named: "empStar")
            }
            driverRate = driverRatingView.rating
            if driverRate == 1 || driverRate == 2{
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if driverRate == 3 || driverRate == 4 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if driverRate == 5 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }else{
                driverRatingTitleLbl.text = ""
            }
        }
    }
}
//MARK: TextView Delegate
@available(iOS 13.0, *)
extension RateVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray || textView.textColor == Color.init(cgColor: CGColor(red: 205.0, green: 205.0, blue: 205.0, alpha: 1)){
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Suggestions? Let us Know....", value: "", table: nil))!
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textView.textInputMode?.primaryLanguage == "emoji") || text.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}

