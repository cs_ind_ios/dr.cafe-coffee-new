//
//  TrackOrderVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 15/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@available(iOS 13.0, *)
class TrackOrderVC: UIViewController, CLLocationManagerDelegate {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var estimateTimeView: UIView!
    @IBOutlet weak var estimateDeliveryTimeLbl: UILabel!
    @IBOutlet weak var estimateDeliveryDateLbl: UILabel!
    @IBOutlet weak var estimateDeliveryTimeNameLbl: UILabel!
    @IBOutlet weak var orderNumNameLbl: UILabel!
    @IBOutlet weak var orderNumLbl: UILabel!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var favouriteImg: UIImageView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var shareOrderBtn: UIButton!
    @IBOutlet weak var cancelOrderBtn: UIButton!
    @IBOutlet weak var favouriteNameLbl: UILabel!
    @IBOutlet weak var callNameLbl: UILabel!
    @IBOutlet weak var helpNameLbl: UILabel!
    @IBOutlet weak var shareNameLbl: UILabel!
    @IBOutlet weak var cancelNameLbl: UILabel!
    @IBOutlet weak var favouriteView: UIView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var shareOrderView: UIView!
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var favouriteMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var trackingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trackingView: UIView!
    
    @IBOutlet weak var orderConfirmedView: UIView!
    @IBOutlet weak var orderConfirmedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderConfirmedImgBGView: UIView!
    @IBOutlet weak var orderConfirmedImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var OrderConfirmedLbl: UILabel!
    @IBOutlet weak var OrderConfirmedDescLbl: UILabel!
    @IBOutlet weak var orderConfirmedTimeLbl: UILabel!
    @IBOutlet weak var orderConfirmedLineLbl: UILabel!
    @IBOutlet weak var orderConfirmedLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var orderConfirmedSepLineLbl: UILabel!
    @IBOutlet weak var orderConfirmedImage: UIImageView!
    
    @IBOutlet weak var prepareOrderView: UIView!
    @IBOutlet weak var prepareOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var prepareOrderImgBGView: UIView!
    @IBOutlet weak var prepareOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var preparingOrderNameLbl: UILabel!
    @IBOutlet weak var preparingOrderDescLbl: UILabel!
    @IBOutlet weak var preparingOrderTimeLbl: UILabel!
    @IBOutlet weak var preparingOrderLineLbl: UILabel!
    @IBOutlet weak var preparingOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var preparingOrderSepLineLbl: UILabel!
    @IBOutlet weak var prepareOrderImage: UIImageView!
    
    @IBOutlet weak var readyOrderView: UIView!
    @IBOutlet weak var readyOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderImgBGView: UIView!
    @IBOutlet weak var readyOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderNameLbl: UILabel!
    @IBOutlet weak var readyOrderDescLbl: UILabel!
    @IBOutlet weak var readyOrderTimeLbl: UILabel!
    @IBOutlet weak var readyOrderLineLbl: UILabel!
    @IBOutlet weak var readyOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderSepLineLbl: UILabel!
    @IBOutlet weak var readyOrderImage: UIImageView!
    
    @IBOutlet weak var pickingOrderView: UIView!
    @IBOutlet weak var pickingOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pickingOrderImgBGView: UIView!
    @IBOutlet weak var pickingOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pickingOrderNameLbl: UILabel!
    @IBOutlet weak var pickingOrderDescLbl: UILabel!
    @IBOutlet weak var pickingOrderTimeLbl: UILabel!
    @IBOutlet weak var pickingOrderLineLbl: UILabel!
    @IBOutlet weak var pickingOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var pickingOrderSepLineLbl: UILabel!
    @IBOutlet weak var pickingOrderImage: UIImageView!
    
    @IBOutlet weak var riderNearbyView: UIView!
    @IBOutlet weak var riderNearbyViewHeight: NSLayoutConstraint!
    @IBOutlet weak var riderNearbyImgBGView: UIView!
    @IBOutlet weak var riderNearbyImgBGHeight: NSLayoutConstraint!
    @IBOutlet weak var riderIsNearByNameLbl: UILabel!
    @IBOutlet weak var riderIsNearByDescLbl: UILabel!
    @IBOutlet weak var riderNearByTimeLbl: UILabel!
    @IBOutlet weak var riderNearbyImage: UIImageView!
    
    @IBOutlet weak var addressDetailsView: UIView!
    @IBOutlet weak var getDirectionsBtn: UIButton!
    @IBOutlet weak var getDirectionsLbl: UILabel!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var estimateTimeLbl: UILabel!
    @IBOutlet weak var getDirectionsMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var getDirectionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var getDirectionsViewWidth: NSLayoutConstraint!
    @IBOutlet weak var getDirectionsViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var orderDetailsNameLbl: UILabel!
    @IBOutlet weak var orderDetailsDropDownBtn: UIButton!
    @IBOutlet weak var orderItemsTableView: UITableView!
    @IBOutlet weak var orderItemsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderDetailsMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var billAmountDropDownBtn: UIButton!
    @IBOutlet weak var priceDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var billAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountNameLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLbl: UILabel!
    @IBOutlet weak var deliveryChargesLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var walletDiscountNameLbl: UILabel!
    @IBOutlet weak var walletDiscountHeight: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountBottom: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var netAmountNameLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryChargesNameLblBottom: NSLayoutConstraint!
    
    @IBOutlet weak var ratingMainView: UIView!
    @IBOutlet weak var ratingMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rateYourOrderNameLbl: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var rattingBtn: UIButton!
    @IBOutlet weak var rattingBtnHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var newOrderBtn: UIButton!
    @IBOutlet weak var reOrderBtn: UIButton!
    @IBOutlet weak var bottomBtnsSepLineLbl: UILabel!
    
    @IBOutlet weak var DFMainView: UIView!
    @IBOutlet weak var deliveryFrequencyNameLbl: UILabel!
    @IBOutlet weak var frequencyNameLbl: UILabel!
    @IBOutlet weak var DFViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DFViewBottomConstraint: NSLayoutConstraint!
    
    //SubOrder Details
    @IBOutlet weak var subOrderTypeNameLbl: UILabel!
    @IBOutlet weak var subOrderTypeLbl: UILabel!
    @IBOutlet weak var carDetailsNameLbl: UILabel!
    @IBOutlet weak var carNumberNameLbl: UILabel!
    @IBOutlet weak var carNumberLbl: UILabel!
    @IBOutlet weak var carColorNameLbl: UILabel!
    @IBOutlet weak var carColorLbl: UILabel!
    @IBOutlet weak var carBrandNameLbl: UILabel!
    @IBOutlet weak var carBrandLbl: UILabel!
    @IBOutlet weak var carDetailsViewHeight: NSLayoutConstraint!//188,41
    @IBOutlet weak var carDetailsViewTop: NSLayoutConstraint!
    
    var orderId = 0
    var orderFrom = Int()
    var orderDetailsArray = [OrderTrackingDetailsModel]()
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.tabBar.isHidden = true
        ratingView.delegate = self
        
        orderItemsTableView.delegate = self
        orderItemsTableView.dataSource = self
        
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        
        myScrollView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    //MARK: View will Appear
    override func viewWillAppear(_ animated: Bool) {
        self.getOrderDetailsService()
        
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
        }else{
            self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
        }
        navigationBarTitleLbl.textColor = .black
        newOrderBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        reOrderBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        
        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Track Order", value: "", table: nil))!
        self.favouriteNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!
        self.callNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Call", value: "", table: nil))!
        self.helpNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Help", value: "", table: nil))!
        self.shareNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share", value: "", table: nil))!
        self.cancelNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!
        self.orderDetailsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Details", value: "", table: nil))!
        self.billAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bill Amount", value: "", table: nil))!
        self.totalAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) :"
        self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
        self.discountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
        self.netAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!) :"
        self.skipBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Skip", value: "", table: nil))!, for: .normal)
        self.rateYourOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!
        self.newOrderBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "New Order", value: "", table: nil))!, for: .normal)
        self.reOrderBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re Order", value: "", table: nil))!, for: .normal)
        //self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!) :"
        self.walletDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE PAY", value: "", table: nil))!) :"
        self.orderNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Number", value: "", table: nil))!
        self.deliveryFrequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Frequency", value: "", table: nil))!
        self.subOrderTypeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Order Type", value: "", table: nil))!) :"
        self.carDetailsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Details", value: "", table: nil))!
        self.carNumberLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vehicle Number", value: "", table: nil))!) :"
        self.carColorNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Color", value: "", table: nil))!) :"
        self.carBrandNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Brand", value: "", table: nil))!) :"
        
        if view.frame.width > 380{
            self.orderNumNameLbl.font = UIFont(name: "Avenir Book", size: 16.0)
            self.estimateDeliveryTimeNameLbl.font = UIFont(name: "Avenir Book", size: 16.0)
            self.orderNumLbl.font = UIFont(name: "Avenir Book", size: 20.0)
            self.estimateDeliveryTimeLbl.font = UIFont(name: "Avenir Book", size: 16.0)
            self.estimateDeliveryDateLbl.font = UIFont(name: "Avenir Book", size: 16.0)
        }else{
            self.orderNumNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            self.estimateDeliveryTimeNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            self.orderNumLbl.font = UIFont(name: "Avenir Book", size: 16.0)
            self.estimateDeliveryTimeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            self.estimateDeliveryDateLbl.font = UIFont(name: "Avenir Book", size: 14.0)
        }
        
        // Notification Center
        NotificationCenter.default.removeObserver(self, name:Notification.Name(rawValue:"getOrderTrackingDetails"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getOrderTrackingDetails),
                                                      name:Notification.Name(rawValue:"getOrderTrackingDetails"),
                                                      object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidEnterBackground),
                                                      name: UIApplication.didEnterBackgroundNotification,
                                                      object: nil)
               
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnteredForeground),
                                                      name: UIApplication.willEnterForegroundNotification,
                                                      object: nil)
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        DispatchQueue.main.async {
            self.getOrderDetailsService()
        }

    }
    @objc func getOrderTrackingDetails(application:UIApplication)  {
        DispatchQueue.main.async {
            self.getOrderDetailsService()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name:Notification.Name(rawValue:"getOrderTrackingDetails"), object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    //MARK: Refresh Data
    @objc private func refreshWeatherData(_ sender: Any) {
        self.getOrderDetailsService()
    }
    //MARK: Get Order Details Service
    func getOrderDetailsService(){
        let dic:[String:Any] = ["UserId": Int(UserDef.getUserId())!, "OrderId": orderId, "RequestBy":2]
        CartModuleServices.OrderTrackingService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.myScrollView.isHidden = true
                if data.MessageEn == "No details for the given order!"{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }else{
                        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }
            }else{
                self.myScrollView.isHidden = false
                self.orderDetailsArray = data.Data!
                DispatchQueue.main.async {
                    self.allDetails()
                    self.refreshControl.endRefreshing()
                    self.orderItemsTableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Details
    func allDetails(){
        var expectDate = ""
        var sectionType = 0
        if let _ = orderDetailsArray[0].SectionType{
            sectionType = orderDetailsArray[0].SectionType!
        }
        if sectionType == 2{
            self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Shipping Charges", value: "", table: nil))!) :"
            if orderDetailsArray[0].ExpecttedDateTime != ""{
                expectDate = orderDetailsArray[0].ExpecttedDateTime.components(separatedBy: "T")[0]
                let shipTime = orderDetailsArray[0].ShippingTime.components(separatedBy: "-")
                if shipTime.count > 0 && orderDetailsArray[0].ShippingTime != ""{
                    estimateDeliveryTimeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                    estimateDeliveryDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                    estimateTimeLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)) \(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                }else{
                    estimateDeliveryTimeLbl.text = ""
                    estimateDeliveryDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)
                    estimateTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)
                }
            }else{
                estimateDeliveryTimeLbl.text = ""
                estimateDeliveryDateLbl.text = ""
                estimateTimeLbl.text = ""
            }
        }else{
            self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
            if orderDetailsArray[0].ExpecttedDateTime != ""{
                expectDate = orderDetailsArray[0].ExpecttedDateTime.components(separatedBy: ".")[0]
                estimateDeliveryTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                estimateTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
                estimateDeliveryDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
            }else{
                estimateDeliveryTimeLbl.text = ""
                estimateDeliveryDateLbl.text = ""
                estimateTimeLbl.text = ""
            }
        }
        
        if let _ = orderDetailsArray[0].InvoiceNo{
            orderNumLbl.text = orderDetailsArray[0].InvoiceNo
        }else{
            orderNumLbl.text = ""
        }
        
        //Favourite functionality
        if let _ = orderDetailsArray[0].Favourite{
            if orderDetailsArray[0].Favourite == true{
                self.favouriteImg.image = UIImage(named: "heart_fill")
            }else{
                self.favouriteImg.image = UIImage(named: "fav_unselected")
            }
        }else{
            self.favouriteImg.image = UIImage(named: "fav_unselected")
        }
        
        if let _ = orderDetailsArray[0].IsSubscription{
            if orderDetailsArray[0].IsSubscription! == true{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
            }else{
                if let _ = orderDetailsArray[0].ConsumedPoints{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Discount", value: "", table: nil))!) :"
                }else if let _ = orderDetailsArray[0].PromoCode{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!) :"
                }else{
                    self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
                }
            }
        }else{
            if let _ = orderDetailsArray[0].ConsumedPoints{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Discount", value: "", table: nil))!) :"
            }else if let _ = orderDetailsArray[0].PromoCode{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!) :"
            }else{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
            }
        }
        
        if orderDetailsArray[0].OrderTypeId == "4"{
            self.getDirectionsBtn.isEnabled = false
            self.getDirectionsLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Track Order", value: "", table: nil))!
            pickingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ON THE WAY", value: "", table: nil))!
            estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Delivery", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
            if let _ = orderDetailsArray[0].DeliveryAddress{
                if orderDetailsArray[0].DeliveryAddress!.count > 0{
                    self.addressLbl.text = orderDetailsArray[0].DeliveryAddress![0].Details!
                }else{
                    self.addressLbl.text = ""
                }
            }else{
                self.addressLbl.text = ""
            }
            
            pickingOrderSepLineLbl.isHidden = false
            pickingOrderLineLbl.isHidden = false
            riderNearbyImgBGView.isHidden = false
            riderNearbyView.isHidden = false
            orderConfirmedLineLblHeight.constant = 25
            preparingOrderLineLblHeight.constant = 25
            readyOrderLineLblHeight.constant = 25
            pickingOrderLineLblHeight.constant = 25
            orderConfirmedViewHeight.constant = 55
            prepareOrderViewHeight.constant = 60
            readyOrderViewHeight.constant = 60
            pickingOrderViewHeight.constant = 60
            riderNearbyViewHeight.constant = 60
            riderNearbyImgBGHeight.constant = 50
            deliveryChargesNameLblHeight.constant = 21
            deliveryChargesNameLblBottom.constant = 10
            getDirectionsViewWidth.constant = 0
            getDirectionsViewLeading.constant = 0
        }else{
            self.getDirectionsBtn.isEnabled = true
            self.getDirectionsLbl .text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get Directions", value: "", table: nil))!
            pickingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SERVED", value: "", table: nil))!
            estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Pickup", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up at", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                if let _ = orderDetailsArray[0].Store![0].StoreAddressEn{
                    addressLbl.text = orderDetailsArray[0].Store![0].StoreAddressEn!
                }else{
                    addressLbl.text = ""
                }
            }else{
                if let _ = orderDetailsArray[0].Store![0].StoreAddressAr{
                    addressLbl.text = orderDetailsArray[0].Store![0].StoreAddressAr!
                }else{
                    addressLbl.text = ""
                }
            }
            pickingOrderSepLineLbl.isHidden = true
            pickingOrderLineLbl.isHidden = true
            riderNearbyImgBGView.isHidden = true
            riderNearbyView.isHidden = true
            orderConfirmedLineLblHeight.constant = 45
            preparingOrderLineLblHeight.constant = 45
            readyOrderLineLblHeight.constant = 45
            orderConfirmedViewHeight.constant = 65
            prepareOrderViewHeight.constant = 65
            readyOrderViewHeight.constant = 65
            pickingOrderViewHeight.constant = 65
            riderNearbyViewHeight.constant = 0
            riderNearbyImgBGHeight.constant = 0
            deliveryChargesNameLblHeight.constant = 0
            deliveryChargesNameLblBottom.constant = 0
            getDirectionsViewWidth.constant = 75
            getDirectionsViewLeading.constant = 10
        }
        
        if orderDetailsArray[0].OrderTypeId == "3"{
            if orderDetailsArray[0].SubOrderNameEn == "" || orderDetailsArray[0].SubOrderNameEn == nil{
                self.carDetailsViewTop.constant = 0
                self.carDetailsViewHeight.constant = 0
            }else{
                self.carDetailsViewTop.constant = 10
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if let _ = orderDetailsArray[0].SubOrderNameEn{
                        self.subOrderTypeLbl.text = orderDetailsArray[0].SubOrderNameEn!
                    }
                }else{
                    if let _ = orderDetailsArray[0].SubOrderNameAr{
                        self.subOrderTypeLbl.text = orderDetailsArray[0].SubOrderNameAr!
                    }
                }
                if orderDetailsArray[0].CarId != 0{
                    self.carDetailsViewHeight.constant = 188
                    if let _ = orderDetailsArray[0].CarJson{
                        let carDetails = orderDetailsArray[0].CarJson!.convertToDictionary()
                        if carDetails != nil{
                            self.carNumberNameLbl.text = "\(carDetails!["CarVehicleNo"]!)"
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                self.carColorLbl.text = "\(carDetails!["CarColorEn"]!)"
                                self.carBrandLbl.text = "\(carDetails!["CarBrandEn"]!)"
                            }else{
                                self.carColorLbl.text = "\(carDetails!["CarColorAr"]!)"
                                self.carBrandLbl.text = "\(carDetails!["CarBrandAr"]!)"
                            }
                        }else{
                            self.carNumberLbl.text = ""
                            self.carColorLbl.text = ""
                            self.carBrandLbl.text = ""
                            self.carDetailsViewHeight.constant = 41
                        }
                    }else{
                        self.carNumberLbl.text = ""
                        self.carColorLbl.text = ""
                        self.carBrandLbl.text = ""
                        self.carDetailsViewHeight.constant = 41
                    }
                }else{
                    self.carDetailsViewHeight.constant = 41
                }
            }
        }else{
            self.carDetailsViewTop.constant = 0
            self.carDetailsViewHeight.constant = 0
        }
        
        //Price Details
        if let _ =  orderDetailsArray[0].ItemTotal{
            self.totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].ItemTotal!.withCommas())"
        }else{
            self.totalAmountLbl.text = ""
        }
        self.discountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        if let _ =  orderDetailsArray[0].NetTotal{
            self.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].NetTotal!.withCommas())"
        }else{
            self.netAmountLbl.text = ""
        }
        if let _ =  orderDetailsArray[0].Vatcharges{
            self.vatLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].Vatcharges!.withCommas())"
        }else{
            self.vatLbl.text = ""
        }
        if let _ =  orderDetailsArray[0].DeliveryCharge{
            if orderDetailsArray[0].DeliveryCharge! > 0{
                self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].DeliveryCharge!.withCommas())"
            }else if orderDetailsArray[0].DeliveryCharge! == 0{
                self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Free", value: "", table: nil))!)"
            }else{
                self.deliveryChargesLbl.text = ""
            }
        }else{
            self.deliveryChargesLbl.text = ""
        }
        if let _ =  orderDetailsArray[0].Discount{
            if orderDetailsArray[0].Discount! == 0 || orderDetailsArray[0].Discount! == 0.0{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].Discount!.withCommas())"
            }else{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].Discount!.withCommas())"
            }
        }else{
            self.couponDiscountLbl.text = ""
        }
        if let _ =  orderDetailsArray[0].VatPecentage{
            self.vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(orderDetailsArray[0].VatPecentage!)%) :"
        }else{
            self.vatNameLbl.text = ""
        }
        
        if let _ =  orderDetailsArray[0].Wallet{
            if orderDetailsArray[0].Wallet! == 0 || orderDetailsArray[0].Wallet! == 0.0{
                walletDiscountHeight.constant = 0
                walletDiscountBottom.constant = 0
            }else{
                walletDiscountHeight.constant = 21
                walletDiscountBottom.constant = 10
                walletDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].Wallet!.withCommas())"
            }
        }else{
            walletDiscountHeight.constant = 0
            walletDiscountBottom.constant = 0
        }
        
        if let _ =  orderDetailsArray[0].OrderStatusEn{
            if orderDetailsArray[0].OrderStatusEn! == "Cancel" || orderDetailsArray[0].OrderStatusEn! == "Close" || orderDetailsArray[0].OrderStatusEn! == "Reject"{
                cancelOrderBtn.isEnabled = false
            }else{
                cancelOrderBtn.isEnabled = true
            }
            if orderDetailsArray[0].OrderStatusEn! == "Cancel" || orderDetailsArray[0].OrderStatusEn! == "Reject"{
                callView.isHidden = true
                getDirectionsViewHeight.constant = 0
                getDirectionsMainViewHeight.constant = 0
                favouriteMainViewHeight.constant = 413
                trackingViewHeight.constant = 305
            }else{
                callView.isHidden = false
                getDirectionsViewHeight.constant = 95
                getDirectionsMainViewHeight.constant = 105
                favouriteMainViewHeight.constant = 501
                trackingViewHeight.constant = 393
            }
        }else{
            cancelOrderBtn.isEnabled = false
            callView.isHidden = false
            getDirectionsViewHeight.constant = 95
            getDirectionsMainViewHeight.constant = 105
            favouriteMainViewHeight.constant = 501
            trackingViewHeight.constant = 393
        }
        
        //Order Track Details
        //OrderConfirmedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Confirmed", value: "", table: nil))!
        OrderConfirmedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ORDER", value: "", table: nil))!
        let newOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 2})
        if newOrderArray.count > 0{
            if let _ =  newOrderArray[0].Time{
                if newOrderArray[0].Time! != ""{
                    let orderTime = newOrderArray[0].Time!.components(separatedBy: ".")[0]
                    if sectionType == 2{
                        let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(orderTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(orderTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                        orderConfirmedTimeLbl.attributedText = time
                    }else{
                        orderConfirmedTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(orderTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                    }
                }else{
                    orderConfirmedTimeLbl.text = ""
                }
            }else{
                orderConfirmedTimeLbl.text = ""
            }
            orderConfirmedImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
            OrderConfirmedDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is placed", value: "", table: nil))!
        }else{
            orderConfirmedImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
            orderConfirmedTimeLbl.text = ""
            OrderConfirmedDescLbl.text = ""
        }
        
        var cancelOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 4})
        if cancelOrderArray.count == 0{
            cancelOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 5})
        }
        
        //preparingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Preparing Your Order", value: "", table: nil))!
        preparingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ACCEPTED", value: "", table: nil))!
        let acceptOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 6})
        if acceptOrderArray.count > 0{
            if let _ =  acceptOrderArray[0].Time{
                if acceptOrderArray[0].Time! != ""{
                    let acceptTime = acceptOrderArray[0].Time!.components(separatedBy: ".")[0]
                    if sectionType == 2{
                        let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(acceptTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(acceptTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                        preparingOrderTimeLbl.attributedText = time
                    }else{
                        preparingOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(acceptTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                    }
                }else{
                    preparingOrderTimeLbl.text = ""
                }
            }else{
                preparingOrderTimeLbl.text = ""
            }
            
            prepareOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
            preparingOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is accepted", value: "", table: nil))!
        }else{
            prepareOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
            preparingOrderTimeLbl.text = ""
            preparingOrderDescLbl.text = ""
        }
        
        //readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order is Ready", value: "", table: nil))!
        readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "READY", value: "", table: nil))!
        let readyOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 7})
        if readyOrderArray.count > 0{
            if let _ =  readyOrderArray[0].Time{
                if readyOrderArray[0].Time! != ""{
                    let readyTime = readyOrderArray[0].Time!.components(separatedBy: ".")[0]
                    if sectionType == 2{
                        let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(readyTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(readyTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                        readyOrderTimeLbl.attributedText = time
                    }else{
                        readyOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(readyTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                    }
                }else{
                    readyOrderTimeLbl.text = ""
                }
            }else{
                readyOrderTimeLbl.text = ""
            }
            readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
            readyOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is ready, the driver has picked the order", value: "", table: nil))!
        }else{
            readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
            readyOrderTimeLbl.text = ""
            readyOrderDescLbl.text = ""
        }
        
        let onTheWayOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 9})
        if onTheWayOrderArray.count > 0{
            if let _ =  onTheWayOrderArray[0].Time{
                if onTheWayOrderArray[0].Time! != ""{
                    let onTheWayTime = onTheWayOrderArray[0].Time!.components(separatedBy: ".")[0]
                    if sectionType == 2{
                        let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(onTheWayTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(onTheWayTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                        pickingOrderTimeLbl.attributedText = time
                    }else{
                        pickingOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(onTheWayTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                    }
                }else{
                    pickingOrderTimeLbl.text = ""
                }
            }else{
                pickingOrderTimeLbl.text = ""
            }
            pickingOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
            pickingOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is on the way, will be delivered soon", value: "", table: nil))!
        }else{
            pickingOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
            pickingOrderTimeLbl.text = ""
            pickingOrderDescLbl.text = ""
        }
        
        //riderIsNearByNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Delivered", value: "", table: nil))!
        riderIsNearByNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DELIVERED", value: "", table: nil))!
        let deliveredOrderArray = orderDetailsArray[0].Tracking!.filter({$0.OrderStatusId == 10})
        if deliveredOrderArray.count > 0{
            if orderDetailsArray[0].OrderTypeId == "4"{
                if let _ =  deliveredOrderArray[0].Time{
                    if deliveredOrderArray[0].Time! != ""{
                        let deliveredTime = deliveredOrderArray[0].Time!.components(separatedBy: ".")[0]
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            riderNearByTimeLbl.attributedText = time
                        }else{
                            riderNearByTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        riderNearByTimeLbl.text = ""
                    }
                }else{
                    riderNearByTimeLbl.text = ""
                }
                riderNearbyImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                riderIsNearByDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been delivered", value: "", table: nil))!
            }else{
                if let _ = deliveredOrderArray[0].Time{
                    if deliveredOrderArray[0].Time! != ""{
                        let deliveredTime = deliveredOrderArray[0].Time!.components(separatedBy: ".")[0]
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            pickingOrderTimeLbl.attributedText = time
                        }else{
                            pickingOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(deliveredTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        pickingOrderTimeLbl.text = ""
                    }
                }else{
                    pickingOrderTimeLbl.text = ""
                }
                pickingOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                pickingOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been served.", value: "", table: nil))!
            }
        }else{
            if orderDetailsArray[0].OrderTypeId == "4"{
                riderNearbyImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                riderNearByTimeLbl.text = ""
                riderIsNearByDescLbl.text = ""
            }else{
                pickingOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                pickingOrderTimeLbl.text = ""
                pickingOrderDescLbl.text = ""
            }
        }
        
        if cancelOrderArray.count > 0{
            var cancelTime = ""
            if let _ = cancelOrderArray[0].Time{
                cancelTime = cancelOrderArray[0].Time!.components(separatedBy: ".")[0]
            }
            if acceptOrderArray.count == 0{
                if let _ = cancelOrderArray[0].Time{
                    if cancelOrderArray[0].Time! != ""{
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            preparingOrderTimeLbl.attributedText = time
                        }else{
                            preparingOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        preparingOrderTimeLbl.text = ""
                    }
                }else{
                    preparingOrderTimeLbl.text = ""
                }
                
                //preparingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order has been cancelled", value: "", table: nil))!
                preparingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                preparingOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                prepareOrderImage.image = UIImage(named: "track_order_cancel")
                prepareOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                readyOrderView.isHidden = true
                readyOrderImgBGView.isHidden = true
                pickingOrderView.isHidden = true
                pickingOrderImgBGView.isHidden = true
                riderNearbyView.isHidden = true
                riderNearbyImgBGView.isHidden = true
                preparingOrderLineLbl.isHidden = true
                readyOrderLineLbl.isHidden = true
                pickingOrderLineLbl.isHidden = true
                preparingOrderSepLineLbl.isHidden = true
            }else if acceptOrderArray.count > 0 && readyOrderArray.count == 0{
                if let _ = cancelOrderArray[0].Time{
                    if cancelOrderArray[0].Time! != ""{
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            readyOrderTimeLbl.attributedText = time
                        }else{
                            readyOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        readyOrderTimeLbl.text = ""
                    }
                }else{
                    readyOrderTimeLbl.text = ""
                }
                
                //readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order has been cancelled", value: "", table: nil))!
                readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                readyOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                readyOrderImage.image = UIImage(named: "track_order_cancel")
                readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                pickingOrderView.isHidden = true
                pickingOrderImgBGView.isHidden = true
                riderNearbyView.isHidden = true
                riderNearbyImgBGView.isHidden = true
                readyOrderLineLbl.isHidden = true
                pickingOrderLineLbl.isHidden = true
                readyOrderSepLineLbl.isHidden = true
            }else if acceptOrderArray.count > 0 && readyOrderArray.count > 0 && onTheWayOrderArray.count == 0{
                if let _ = cancelOrderArray[0].Time{
                    if cancelOrderArray[0].Time! != ""{
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            pickingOrderTimeLbl.attributedText = time
                        }else{
                            pickingOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        pickingOrderTimeLbl.text = ""
                    }
                }else{
                    pickingOrderTimeLbl.text = ""
                }
                
                //pickingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order has been cancelled", value: "", table: nil))!
                pickingOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                pickingOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                pickingOrderImage.image = UIImage(named: "track_order_cancel")
                pickingOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                riderNearbyView.isHidden = true
                riderNearbyImgBGView.isHidden = true
                pickingOrderLineLbl.isHidden = true
                pickingOrderSepLineLbl.isHidden = true
            }else if acceptOrderArray.count > 0 && readyOrderArray.count > 0 && onTheWayOrderArray.count > 0 && deliveredOrderArray.count == 0{
                if let _ = cancelOrderArray[0].Time{
                    if cancelOrderArray[0].Time! != ""{
                        if sectionType == 2{
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            riderNearByTimeLbl.attributedText = time
                        }else{
                            riderNearByTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(cancelTime)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        riderNearByTimeLbl.text = ""
                    }
                }else{
                    riderNearByTimeLbl.text = ""
                }
                
                //riderIsNearByNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order has been cancelled", value: "", table: nil))!
                riderIsNearByNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                riderIsNearByDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                riderNearbyImage.image = UIImage(named: "track_order_cancel")
                riderNearByTimeLbl.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.9215686275, blue: 0.9568627451, alpha: 1)
            }
        }
        
        //Rating Functionality
        if let _ =  orderDetailsArray[0].OrderStatusEn{
            if orderDetailsArray[0].OrderStatusEn! == "Close" {
                if let _ =  orderDetailsArray[0].OrderRating {
                    if orderDetailsArray[0].OrderRating!.count > 0{
                        rattingBtn.isHidden = true
                        rattingBtnHeightConstraint.constant = 0
                    }else{
                        rattingBtn.isHidden = false
                        rattingBtnHeightConstraint.constant = 55
                    }
                }else{
                    rattingBtn.isHidden = false
                    rattingBtnHeightConstraint.constant = 55
                }
            }else{
                rattingBtn.isHidden = true
                rattingBtnHeightConstraint.constant = 0
            }
        }else{
            rattingBtn.isHidden = true
            rattingBtnHeightConstraint.constant = 0
        }
        if let _ =  orderDetailsArray[0].IsSubscription{
            if orderDetailsArray[0].IsSubscription! == false{
                if let _ =  orderDetailsArray[0].EnableCancel{
                    if orderDetailsArray[0].EnableCancel == false{
                        cancelOrderView.isHidden = true
                    }
                }else{
                    cancelOrderView.isHidden = true
                }
            }else{
                cancelOrderView.isHidden = true
            }
        }else{
            if let _ =  orderDetailsArray[0].EnableCancel{
                if orderDetailsArray[0].EnableCancel == false{
                    cancelOrderView.isHidden = true
                }
            }else{
                cancelOrderView.isHidden = true
            }
        }
        
//        if orderDetailsArray[0].EnableCancel == false{
//            cancelOrderView.isHidden = true
//        }
        
        //if orderDetailsArray[0].OrderStatusEn == "Close" {
            //if orderDetailsArray[0].OrderRating != nil {
              //  ratingMainView.isHidden = true
               // ratingMainViewHeight.constant = 0
           // }else{
               // ratingMainView.isHidden = false
               // ratingMainViewHeight.constant = 100
            //}
       // }else{
           // ratingMainView.isHidden = true
           // ratingMainViewHeight.constant = 0
        //}
        
        //Subscription
        if let _ =  orderDetailsArray[0].IsSubscription{
            if orderDetailsArray[0].IsSubscription! == true{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if let _ =  orderDetailsArray[0].FrequencyNameEn{
                        self.frequencyNameLbl.text = orderDetailsArray[0].FrequencyNameEn!
                    }else{
                        self.frequencyNameLbl.text = ""
                    }
                }else{
                    if let _ =  orderDetailsArray[0].FrequencyNameAr{
                        self.frequencyNameLbl.text = orderDetailsArray[0].FrequencyNameAr!
                    }else{
                        self.frequencyNameLbl.text = ""
                    }
                }
                self.DFViewHeightConstraint.constant = 70
                self.DFViewBottomConstraint.constant = 10
                //self.bottomBtnsSepLineLbl.isHidden = true
                self.reOrderBtn.isHidden = true
                self.newOrderBtn.isHidden = false
            }else{
                self.DFViewHeightConstraint.constant = 0
                self.DFViewBottomConstraint.constant = 0
                //self.bottomBtnsSepLineLbl.isHidden = false
                self.reOrderBtn.isHidden = false
                self.newOrderBtn.isHidden = false
            }
        }else{
            self.DFViewHeightConstraint.constant = 0
            self.DFViewBottomConstraint.constant = 0
           // self.bottomBtnsSepLineLbl.isHidden = false
            self.reOrderBtn.isHidden = false
            self.newOrderBtn.isHidden = false
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if orderFrom == 1{
            HomeVC.instance.dashBoardArray.removeAll()
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Favourite Button Action
    @IBAction func favouriteBtn_Tapped(_ sender: Any) {
        if self.favouriteImg.image == UIImage(named: "fav_unselected"){
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.textPopUpVCDelegate = self
            obj.id = 0
            obj.modalPresentationStyle = .overFullScreen
            present(obj, animated: true, completion: nil)
        }else{
            self.favouriteOrder(FavouriteName: "")
        }
    }
    //MARK: Favourite Service Integration
    func favouriteOrder(FavouriteName: String){
        var dic:[String:Any] = [:]
        if self.favouriteImg.image == UIImage(named: "fav_unselected"){
            dic = ["OrderId": orderId, "FavouriteName":FavouriteName, "RequestBy":2]
        }else{
            dic = ["OrderId": orderId, "RequestBy":2]
        }
        CartModuleServices.ToggleFevOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                var message = data.MessageEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    message = data.MessageEn
                }else{
                    message = data.MessageAr
                }
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                    //Favourite functionality
                    if self.favouriteImg.image == UIImage(named: "heart_fill"){
                        self.favouriteImg.image = UIImage(named: "fav_unselected")
                    }else{
                        self.favouriteImg.image = UIImage(named: "heart_fill")
                    }
                }))
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Call Button Action
    @IBAction func callBtn_Tapped(_ sender: Any) {
        var mobile = ""
        if let _ =  orderDetailsArray[0].Store![0].PhoneNumber{
            mobile = orderDetailsArray[0].Store![0].PhoneNumber!
        }
        //print(mobile.removeWhitespace())
        if let url = URL(string: "tel://\(mobile.removeWhitespace())") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func getUserDetails(key:String) -> String {
        return UserDefaults.standard.value(forKey: key) == nil ? "" : "\(UserDefaults.standard.value(forKey: key)!)"
    }
    //MARK: Help Button Action
    @IBAction func helpBtn_Tapped(_ sender: Any) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        // Create a user object
        let user = FreshchatUser.sharedInstance();
        user.firstName = "\((userDic["FullName"]! as! String))"
        user.lastName = "\((userDic["NickName"]! as! String))"
        user.email = "\((userDic["Email"]! as! String))"
        user.phoneCountryCode="966"
        user.phoneNumber = "\((userDic["Mobile"]! as! String))"
        Freshchat.sharedInstance().setUser(user)
        Freshchat.sharedInstance().showConversations(self)
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        if orderDetailsArray.count > 0{
            DispatchQueue.main.async() {
                let Name = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!
                var OrderId = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order No", value: "", table: nil))!) : "
                if let _ =  self.orderDetailsArray[0].InvoiceNo{
                    OrderId = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order No", value: "", table: nil))!) : \(self.orderDetailsArray[0].InvoiceNo!)"
                }
                var ExpectedTime = ""
                var sectionType = 0
                if let _ =  self.orderDetailsArray[0].SectionType{
                    sectionType = self.orderDetailsArray[0].SectionType!
                }
                if self.orderDetailsArray[0].ExpecttedDateTime != ""{
                    let expectTime = self.orderDetailsArray[0].ExpecttedDateTime.components(separatedBy: ".")[0]
                    if sectionType == 2{
                        let shipTime = self.orderDetailsArray[0].ShippingTime.components(separatedBy: " - ")
                        if shipTime.count > 0 && self.orderDetailsArray[0].ShippingTime != ""{
                            ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyy)) \(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                        }else{
                            ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyyTimeA))"
                        }
                    }else{
                        ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectTime)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyyTimeA))"
                    }
                }
                
                var OrderType = ""
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if let _ =  self.orderDetailsArray[0].OrderTypeEn{
                        OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderTypeEn!)"
                    }else{
                        OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) :"
                    }
                }else{
                    if let _ =  self.orderDetailsArray[0].OrderTypeAr{
                        OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderTypeAr!)"
                    }else{
                        OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) :"
                    }
                }
                var ItemQuantity = ""
                if let _ =  self.orderDetailsArray[0].CartItemsQuantity{
                    ItemQuantity = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Items", value: "", table: nil))!) : \(self.orderDetailsArray[0].CartItemsQuantity!)"
                }else{
                    ItemQuantity = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Items", value: "", table: nil))!) :"
                }
                var OrderAmount = ""
                if let _ =  self.orderDetailsArray[0].NetTotal{
                    OrderAmount = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) : \(self.orderDetailsArray[0].NetTotal!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                }else{
                    OrderAmount = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) : 0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                }
                var paymentMode = ""
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if let _ =  self.orderDetailsArray[0].PaymentTypeEn{
                        paymentMode = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Mode", value: "", table: nil))!) : \(self.orderDetailsArray[0].PaymentTypeEn!)"
                    }else{
                        paymentMode = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Mode", value: "", table: nil))!) :"
                    }
                }else{
                    if let _ =  self.orderDetailsArray[0].PaymentTypeAr{
                        paymentMode = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Mode", value: "", table: nil))!) : \(self.orderDetailsArray[0].PaymentTypeAr!)"
                    }else{
                        paymentMode = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Mode", value: "", table: nil))!) :"
                    }
                }
                let Des = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Thank you for ordering at dr.CAFE Coffee. It has been a pleasure serving you. We hope to serve you again soon!", value: "", table: nil))!

                let shareAll = [Name,OrderId,ExpectedTime,OrderType,ItemQuantity,OrderAmount, paymentMode,Des as Any ] as [Any]
                print(shareAll)
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    //MARK: Cancel Button Action
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Do you want to cancel this Order", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            return
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            let dic:[String:Any] = ["UserId": UserDef.getUserId(), "OrderId":self.orderId, "RequestBy":2]
            CartModuleServices.CancelOrderService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.getOrderDetailsService()
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }))
    }
    //MARK: Get Directions Button Action
    @IBAction func getDirectionsBtn_Tapped(_ sender: Any) {
        if let _ =  orderDetailsArray[0].OrderTypeEn{
            if orderDetailsArray[0].OrderTypeEn! == "Delivery"{
                
            }else{
                if orderDetailsArray[0].Store![0].StoreLat != nil && orderDetailsArray[0].Store![0].StoreLong != nil{
                    if orderDetailsArray[0].Store![0].StoreLat != "" && orderDetailsArray[0].Store![0].StoreLong != ""{
                        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                            UIApplication.shared.open((URL(string:
                                "comgooglemaps://?saddr=\(latitude),\(longitude)&daddr=\(orderDetailsArray[0].Store![0].StoreLat!),\(orderDetailsArray[0].Store![0].StoreLong!)&directionsmode=driving")!), options: [:], completionHandler: nil)
                        } else {
                            print("Can't use comgooglemaps://");
                            print("https://www.google.co.in/maps/dir/?saddr=\(latitude),\(longitude)&daddr=\(String(describing: orderDetailsArray[0].Store![0].StoreLat!)),\(String(describing: orderDetailsArray[0].Store![0].StoreLong!))&directionsmode=driving")
                            UIApplication.shared.open((URL(string:
                                "https://www.google.co.in/maps/dir/?saddr=\(latitude),\(longitude)&daddr=\(String(describing: orderDetailsArray[0].Store![0].StoreLat!)),\(String(describing: orderDetailsArray[0].Store![0].StoreLong!))&directionsmode=driving")!), options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        }
    }
    //MARK: New Order Button Action
    @IBAction func newOrderBtn_Tapped(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    //MARK: Re Order Button Action
    @IBAction func reOrderBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["OrderId": orderId, "RequestBy":2]
        CartModuleServices.ReOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = CartVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                    obj.whereObj = 6
                    obj.isRemove = true
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Rate Skip Button Action
    @IBAction func skipBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromTopToBottom(view: ratingMainView)
        ratingMainView.isHidden = true
        self.ratingMainViewHeight.constant = 0
    }
    //MARK: rate Button Tapped
    @IBAction func rattingBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = orderId
        if let _ =  self.orderDetailsArray[0].DeliveryAddress{
            obj.orderType = 1
        }else{
            obj.orderType = 2
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let _ =  self.orderDetailsArray[0].Store![0].StoreNameEn{
                obj.StoreName = self.orderDetailsArray[0].Store![0].StoreNameEn!.capitalized
            }else{
                obj.StoreName = ""
            }
        }else{
            if let _ =  self.orderDetailsArray[0].Store![0].StoreNameAr{
                obj.StoreName = self.orderDetailsArray[0].Store![0].StoreNameAr!.capitalized
            }else{
                obj.StoreName = ""
            }
        }
        obj.rateVCDelegate = self
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    //MARK: Order Details Drop Down Button Action
    @IBAction func orderDetailsDropDownBtn_Tapped(_ sender: Any) {
        if orderDetailsMainViewHeight.constant == 44{
            self.orderItemsTableView.reloadData()
            self.orderDetailsMainViewHeight.constant = self.orderItemsTableView.contentSize.height + 44
            if AppDelegate.getDelegate().appLanguage == "English"{
                orderDetailsDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                orderDetailsDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            orderDetailsMainViewHeight.constant = 44
            orderDetailsDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Amount Details Drop Down Button Action
    @IBAction func billAmountDropDownBtn_Tapped(_ sender: Any) {
        if priceDetailsViewHeight.constant == 45{
            if self.walletDiscountHeight.constant == 0{
                priceDetailsViewHeight.constant = 233
            }else{
                priceDetailsViewHeight.constant = 260
            }
            if AppDelegate.getDelegate().appLanguage == "English"{
                billAmountDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                billAmountDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            priceDetailsViewHeight.constant = 45
            billAmountDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //View Top to Bottom Animation Function
    func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        view.layer.add(transition, forKey: kCATransition)
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
    }
    //Get Label Height Based on content
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()

        return label.frame.height
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String, text3:String) -> NSMutableAttributedString{
        let boldAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 12.0)!
           ]
        let regularAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 12.0)!
           ]
        let boldText1 = NSAttributedString(string: text1, attributes: boldAttribute)
        let boldText2 = NSAttributedString(string: text2, attributes: boldAttribute)
        let regularText = NSAttributedString(string: text3, attributes: regularAttribute)
        let newString = NSMutableAttributedString()
        newString.append(boldText1)
        newString.append(boldText2)
        newString.append(regularText)
        return newString
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension TrackOrderVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderDetailsArray.count > 0{
            return self.orderDetailsArray[0].Items!.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var additional = [""]
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].GrinderNameEn {
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                }
            }
        }else{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].GrinderNameAr {
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameAr != ""{
                    additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                }
            }
        }
        
        var rewardPoints = 0
        if let _ =  orderDetailsArray[0].Items![indexPath.row].ConsumedPoints {
            if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints! > 0{
                rewardPoints = 31
            }else{
                rewardPoints = 0
            }
        }
        
        var noteHeight = 0
        //Comments
        if let _ = orderDetailsArray[0].Items![indexPath.row].ItemComments{
            if orderDetailsArray[0].Items![indexPath.row].ItemComments! != ""{
                let height = heightForView(text: "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                noteHeight = Int(10 + height)
            }else{
                noteHeight = 0
            }
        }else{
            noteHeight = 0
        }
        
        if orderDetailsArray[0].Items![indexPath.row].Additionals != nil{
            if orderDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                for i in 0...orderDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        if orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn != nil && orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity != nil{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn!) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity!)")
                        }
                    }else{
                        if orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn != nil && orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity != nil{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameAr!) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity!)")
                        }
                    }
                }
            }
            if additional.count > 1{
                additional.remove(at: 0)
                var height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                height = 95 + height
                print(height)
                return height + CGFloat(rewardPoints + noteHeight)
            }else{
                return CGFloat(90 + rewardPoints + noteHeight)
            }
        }else{
            return CGFloat(90 + rewardPoints + noteHeight)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, value: "", table: nil))!) as! OrderItemsTVCell
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].itemTypeEn{
                if orderDetailsArray[0].Items![indexPath.row].itemTypeEn! == "Regular"{
                    if let _ =  orderDetailsArray[0].Items![indexPath.row].ItemNameEn{
                        cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn!)"
                    }else{
                        cell.itemNameLbl.text = ""
                    }
                }else{
                    if let _ =  orderDetailsArray[0].Items![indexPath.row].ItemNameEn{
                        cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn!) (\(orderDetailsArray[0].Items![indexPath.row].itemTypeEn!))"
                    }else{
                        cell.itemNameLbl.text = ""
                    }
                }
            }else{
                if let _ =  orderDetailsArray[0].Items![indexPath.row].ItemNameEn{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn!)"
                }else{
                    cell.itemNameLbl.text = ""
                }
            }
        }else{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].itemTypeEn{
                if orderDetailsArray[0].Items![indexPath.row].itemTypeEn! == "Regular"{
                    if let _ =  orderDetailsArray[0].Items![indexPath.row].ItemNameAr{
                        cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr!)"
                    }else{
                        cell.itemNameLbl.text = ""
                    }
                }else{
                    if orderDetailsArray[0].Items![indexPath.row].ItemNameAr != nil && orderDetailsArray[0].Items![indexPath.row].itemTypeAr != nil{
                        cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr!) (\(orderDetailsArray[0].Items![indexPath.row].itemTypeAr!))"
                    }else{
                        cell.itemNameLbl.text = ""
                    }
                }
            }else{
                if let _ =  orderDetailsArray[0].Items![indexPath.row].ItemNameAr{
                    cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr!)"
                }else{
                    cell.itemNameLbl.text = ""
                }
            }
        }
//        if AppDelegate.getDelegate().appLanguage == "English"{
//            if orderDetailsArray[0].Items![indexPath.row].itemTypeEn == "Regular"{
//                cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn)"
//            }else{
//                cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameEn) (\(orderDetailsArray[0].Items![indexPath.row].itemTypeEn))"
//            }
//        }else{
//            if orderDetailsArray[0].Items![indexPath.row].itemTypeEn == "Regular"{
//                cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr)"
//            }else{
//                cell.itemNameLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemNameAr) (\(orderDetailsArray[0].Items![indexPath.row].itemTypeAr))"
//            }
//        }
        if orderDetailsArray[0].Items![indexPath.row].ItemSubTotal != nil && orderDetailsArray[0].Items![indexPath.row].Quantity != nil{
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.itemQuantityLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].Quantity!) X \(orderDetailsArray[0].Items![indexPath.row].ItemSubTotal!.withCommas())"
            }else{
                cell.itemQuantityLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemSubTotal!.withCommas()) X \(orderDetailsArray[0].Items![indexPath.row].Quantity!)"
            }
        }else{
            cell.itemQuantityLbl.text = ""
        }
        if let _ =  orderDetailsArray[0].Items![indexPath.row].TotalPrice{
            cell.itemPriceLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].TotalPrice!.withCommas())"
        }else{
            cell.itemPriceLbl.text = ""
        }
        cell.additionalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Additional", value: "", table: nil))!
        if indexPath.row%2 == 0{
            cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else{
            cell.BGView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        }
        var additional = [""]
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].GrinderNameEn{
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                }
            }
        }else{
            if let _ =  orderDetailsArray[0].Items![indexPath.row].GrinderNameAr {
                if orderDetailsArray[0].Items![indexPath.row].GrinderNameAr != ""{
                    additional.append("\(orderDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                }
            }
        }
        if orderDetailsArray[0].Items![indexPath.row].Additionals != nil{
            if orderDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                for i in 0...orderDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        if orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn != nil && orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity != nil{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn!) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity!)")
                        }
                    }else{
                        if orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn != nil && orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity != nil{
                            additional.append("\(orderDetailsArray[0].Items![indexPath.row].Additionals![i].NameAr!) x \(orderDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity!)")
                        }
                    }
                }
            }
        }
        
        if additional.count > 1{
            additional.remove(at: 0)
            cell.additionalsLbl.text = additional.joined(separator: ",")
            let height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
            cell.additionalsViewHeight.constant = 5 + height
        }else{
            cell.additionalsLbl.text = ""
            cell.additionalsViewHeight.constant = 0
        }
        
        //Comments
        if let _ = orderDetailsArray[0].Items![indexPath.row].ItemComments{
            if orderDetailsArray[0].Items![indexPath.row].ItemComments! != ""{
                cell.noteLbl.text = "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)"
                let height = heightForView(text: "\(orderDetailsArray[0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
                cell.noteViewHeight.constant = 10 + height
            }else{
                cell.noteLbl.text = ""
                cell.noteViewHeight.constant = 0
            }
        }else{
            cell.noteLbl.text = ""
            cell.noteViewHeight.constant = 0
        }
        
        if let _ = orderDetailsArray[0].Items![indexPath.row].ConsumedPoints{
            if orderDetailsArray[0].Items![indexPath.row].ConsumedPoints! > 0{
                cell.redeemPointsViewHeight.constant = 31
                cell.redeemPointsMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "It’s on us!", value: "", table: nil))!) \(orderDetailsArray[0].Items![indexPath.row].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied.", value: "", table: nil))!)"
                cell.redeemPointsLbl.text = "- \(orderDetailsArray[0].Items![indexPath.row].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!)"
            }else{
                cell.redeemPointsViewHeight.constant = 0
            }
        }else{
            cell.redeemPointsViewHeight.constant = 0
        }
        cell.grinderViewHeight.constant = 0
        
        //Item Image
        var image = ""
        if let _ = orderDetailsArray[0].Items![indexPath.row].Image{
            image = orderDetailsArray[0].Items![indexPath.row].Image!
        }
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.itemImg.kf.setImage(with: url)
        
        DispatchQueue.main.async {
            self.myScrollView.layoutIfNeeded()
            self.orderItemsTableView.layoutIfNeeded()
            self.orderItemsTableViewHeight.constant = self.orderItemsTableView.contentSize.height
            self.orderDetailsMainViewHeight.constant = self.orderItemsTableView.contentSize.height + 44
        }
        cell.selectionStyle = .none
        return cell
    }
}
//MARK: Float Rating View
@available(iOS 13.0, *)
extension TrackOrderVC : FloatRatingViewDelegate{
   @objc func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double){
        ratingView.rating = rating
    }
    @objc func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double){
        self.viewSlideInFromTopToBottom(view: ratingMainView)
        if ratingView.rating == 1{
            ratingView.fullImage = UIImage(named: "Rate 1")
        }else if ratingView.rating == 2{
            ratingView.fullImage = UIImage(named: "Rate 2")
        }else if ratingView.rating == 3{
            ratingView.fullImage = UIImage(named: "Rate 3")
        }else if ratingView.rating == 4{
            ratingView.fullImage = UIImage(named: "Rate 4")
        }else if ratingView.rating == 5 {
            ratingView.fullImage = UIImage(named: "Rate 5")
        }else{
            ratingView.fullImage = UIImage(named: "empStar")
        }
        ratingMainView.isHidden = true
        self.ratingMainViewHeight.constant = 0
        ratingView.rating = rating
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = orderId
        obj.orderType = Int(self.orderDetailsArray[0].OrderTypeId)!
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let _ = self.orderDetailsArray[0].Store![0].StoreNameEn{
                obj.StoreName = self.orderDetailsArray[0].Store![0].StoreNameEn!.capitalized
            }else{
                obj.StoreName = ""
            }
        }else{
            if let _ = self.orderDetailsArray[0].Store![0].StoreNameAr{
                obj.StoreName = self.orderDetailsArray[0].Store![0].StoreNameAr!.capitalized
            }else{
                obj.StoreName = ""
            }
        }
        obj.rateVCDelegate = self
        obj.vendorRate = rating
        obj.driverRate = rating
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: false, completion: nil)
    }
}
//MARK: RateVC Delegate
@available(iOS 13.0, *)
extension TrackOrderVC : RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            self.rattingBtn.isHidden = true
            self.rattingBtnHeightConstraint.constant = 0
        }
    }
}
//MARK: TextPopUp Delegate
@available(iOS 13.0, *)
extension TrackOrderVC : TextPopUpVCDelegate{
    func didTapAction(ID: Int, name: String) {
        self.favouriteOrder(FavouriteName: name)
    }
}
