//
//  LoginVC.swift
//  drCafe
//
//  Created by Devbox on 09/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase
@available(iOS 13.0, *)
class LoginVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var loginTitleNameLbl: UILabel!
    @IBOutlet weak var dontHaveAnAccountNameLbl: UILabel!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var mobileNumNameLbl: UILabel!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var confirmPasswordNameLbl: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var resetPasswordBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet var otpView: UIView!
    @IBOutlet weak var otpSubView: UIView!
    @IBOutlet weak var otpVerificationNameLbl: UILabel!
    @IBOutlet weak var enterOTPHereNameLbl: UILabel!
    @IBOutlet weak var otpDicLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!
    @IBOutlet weak var firstDigitTF: UITextField!
    @IBOutlet weak var secondDigitTF: UITextField!
    @IBOutlet weak var thirdDigitTF: UITextField!
    @IBOutlet weak var fourthDigitTF: UITextField!
    @IBOutlet weak var otpCancelBtn: UIButton!
    @IBOutlet weak var verifyOTPBtn: UIButton!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    static var instance: LoginVC!
    var otpString = String()
    var timer:Timer?
    var countDown = 120
    var backSpaceTF = UITextField()
    var userID:Int!
    var whereObj = 0
    var mobNum:String!
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    
    var countryCodesArray = [CountryCodeDetailsModel]()
    
    var storeId = 0
    var orderTypeObj = 0
    var storeLatitude:String!
    var storeLongitude:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.tabBar.isHidden = true

        LoginVC.instance = self
        self.mobileNumTF.delegate = self
        firstDigitTF.delegate = self
        secondDigitTF.delegate = self
        thirdDigitTF.delegate = self
        fourthDigitTF.delegate = self
        firstDigitTF.textContentType = .oneTimeCode
        secondDigitTF.textContentType = .oneTimeCode
        thirdDigitTF.textContentType = .oneTimeCode
        fourthDigitTF.textContentType = .oneTimeCode
        
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        // changing  OTP text fields auto matically one by one
        firstDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        secondDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirdDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourthDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        loginTitleNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Log In", value: "", table: nil))!
        dontHaveAnAccountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Don't have an account", value: "", table: nil))!
        mobileNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!
        confirmPasswordNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password Name", value: "", table: nil))!
        signUpBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sign Up", value: "", table: nil))!, for: .normal)
        resetPasswordBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Forgot Password?", value: "", table: nil))!, for: .normal)
        loginBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Log In", value: "", table: nil))!, for: .normal)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            mobileNumTF.textAlignment = .left
            mobileNumTF.semanticContentAttribute = .forceLeftToRight
            confirmPasswordTF.textAlignment = .left
            confirmPasswordTF.semanticContentAttribute = .forceLeftToRight
        }else{
            mobileNumTF.textAlignment = .right
            mobileNumTF.semanticContentAttribute = .forceRightToLeft
            confirmPasswordTF.textAlignment = .right
            confirmPasswordTF.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: View will Appear
    override func viewWillAppear(_ animated: Bool) {
        if whereObj == 1{
            self.navigationController?.popViewController(animated: false)
        }
        self.getCountryCodesListService()
    }
    //MARK: Country Codes Get Service
    func getCountryCodesListService(){
        let dic:[String:Any] = [:]
        UserModuleServices.getCountryCodesService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.countryCodesArray = data.Data!
                SaveAddressClass.CountryCodesListArray = data.Data!
                let saudiCC = data.Data!.filter({$0.MobileCode == 966})
                if saudiCC.count > 0{
                    self.countryCodeLbl.text = "+966"
                    self.selectCountryCode = 966
                    self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
                    //self.selectCCMinLength = saudiCC[0].MinLength
                    //self.selectCCMaxLength = saudiCC[0].MaxLength
                }else{
                    self.countryCodeLbl.text = "+966"
                    self.selectCountryCode = 966
                    self.selectCCMobileNumDigits = 9
                    self.selectCCMinLength = 9
                    self.selectCCMaxLength = 9
                }
                if data.Data!.count == 1{
                    self.countryBtn.isHidden = true
                }else{
                    self.countryBtn.isHidden = false
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Country Code Drop Down
    @IBAction func countryBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = countryCodesArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.center.x+27 , y: -15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.center.x-27 , y: -15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 1
        obj.codeDropDownArray = countryCodesArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 2{
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 0
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Reset Password Button
    @IBAction func resetPasswordBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ForgotPasswordVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Show Password Button Action
    @IBAction func showPasswordBtn_Tapped(_ sender: Any) {
        if confirmPasswordTF.isSecureTextEntry == false{
            confirmPasswordTF.isSecureTextEntry = true
            showPasswordBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
        }else{
            confirmPasswordTF.isSecureTextEntry = false
            showPasswordBtn.setImage(UIImage(named: "show Password"), for: .normal)
        }
    }
    //MARK: SignUp Button Action
    @IBAction func signUpBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = SignUpVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Login Button Action
    @IBAction func loginBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var language = "Ar"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }
        let dic = ["CountryCode":"\(selectCountryCode!)","Mobile":"\(mobileNumTF.text!)", "Password":"\(confirmPasswordTF.text!)", "DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "DeviceInfo":"\(getDeviceInfo())", "Language":"\(language)", "RequestBy":2] as [String : Any]
        
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("signInService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        //Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        UserModuleServices.signInService(dic: dic, success: { (data) in
            if(data.Status == false){
                if data.Data!.OTP == nil || data.Data!.OTP! == ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    var alertController = UIAlertController()
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, preferredStyle: .alert)
                    }else{
                        alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, preferredStyle: .alert)
                    }
                    let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        DispatchQueue.main.async {
                            print("OTP \(String(describing: data.Data!.OTP))")
                            ANLoader.hide()
                            self.countDown = 120
                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                            self.otpString = "\(String(describing: data.Data!.OTP!))"
                            self.userID = data.Data!.Id
                            UIView.animate(withDuration: 0.6, animations: { () -> Void in
                                self.otpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                self.otpSubView.isHidden = false
                                self.view.addSubview(self.otpView)
                                self.firstDigitTF.becomeFirstResponder()

                            })
                        }
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: false, completion: nil)
                }
            }else{
                UserDef.saveToUserDefault(value: data.Data!.Id!, key: "UserId")
                var nickName = ""
                if data.Data!.NickName != nil{
                    nickName = data.Data!.NickName!
                }
                var gender = ""
                if data.Data!.Gender != nil{
                    gender = data.Data!.Gender!
                }
                let dic = ["UserId":data.Data!.Id!,
                           "FullName":data.Data!.FullName!,
                            "NickName":nickName,
                            "Email":data.Data!.Email!,
                            "Gender":gender,
                            "Mobile":data.Data!.Mobile!,
                            "isActive":data.Data!.isActive!,
                            "ModifiedOn":data.Data!.ModifiedOn!] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"userDic")
                UserDefaults.standard.synchronize()
                AppDelegate.getDelegate().cartStoreId = data.Data!.StoreId!
                if AppDelegate.getDelegate().appLanguage == "English"{
                    AppDelegate.getDelegate().cartStoreName = data.Data!.StoreNameEn!
                }else{
                    AppDelegate.getDelegate().cartStoreName = data.Data!.StoreNameAr!
                }
                AppDelegate.getDelegate().subCartQuantity = data.Data!.SubCartItem!
                AppDelegate.getDelegate().cartQuantity = Int(data.Data!.CartItemsQuantity!)!
                
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
                var themeColor = data.Data!.Themes!.filter({$0.IsSelected == true})
                if themeColor.count > 0{
                    AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                    AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                    AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                }else{
                    themeColor = data.Data!.Themes!.filter({$0.DefaultSelection == true})
                    if themeColor.count > 0{
                        AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                        AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                        AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                    }else{
                        AppDelegate.getDelegate().selectColor = "535353"
                        AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                        AppDelegate.getDelegate().selectColorname = ""
                    }
                }
                AppDelegate.getDelegate().cartAmount = data.Data!.TotalPrice!
                AppDelegate.getDelegate().subCartAmount = data.Data!.subTotalPrice!
                self.nextViewController()
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func nextViewController(){
        if whereObj == 10{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = OrderTypeVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderTypeVC") as! OrderTypeVC
            obj.whereObj = 1
            obj.isLogin = true
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 11{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = NewOrderHistoryVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewOrderHistoryVC") as! NewOrderHistoryVC
            obj.whereObj = 11
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 12{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ProfileVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            obj.whereObj = 1
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 13{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ChangePasswordVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            obj.whereObj = 1
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 14{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            obj.whereObj = 11
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 15{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            obj.whereObj = 11
            self.navigationController?.pushViewController(obj, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }else if whereObj == 16{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = SubScriptionVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubScriptionVC") as! SubScriptionVC
            obj.isLogin = true
            self.navigationController?.pushViewController(obj, animated: true)
        } else if whereObj == 17{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            if whereObj == 2{
                obj.storeLatitude = storeLatitude!
                obj.storeLongitude = storeLongitude!
            }
            obj.whereObj = orderTypeObj
            obj.storeId = storeId
            obj.sectionType = 1
            obj.isOrderType = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else if whereObj == 18{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = FreeCoffeeVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "FreeCoffeeVC") as! FreeCoffeeVC
            obj.isLogin = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else if whereObj == 19{
            SelectFeedbackTypeVC.instance.isUserLogin = true
            self.navigationController?.popViewController(animated: true)
        }else if whereObj == 21{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CarDetailsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarDetailsVC") as! CarDetailsVC
            obj.isMore = true
            self.navigationController?.pushViewController(obj, animated: true)
        } else{
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Update CountDown Time
    @objc func updateCountDown() {
        if(countDown > 0) {
            let minute = String(countDown/60)
            let seconds = String(countDown % 60)
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            self.resendOTPBtn.isEnabled = false
            var str = "Resend OTP 0\(minute):\(seconds)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                if(UInt8(seconds)! < 10){
                    str = "Resend OTP 0\(minute):0\(seconds)"
                }else{
                    str = "Resend OTP 0\(minute):\(seconds)"
                }
            }else{
                if(UInt8(seconds)! < 10){
                    str = "0إعادة ارسال كلمة مرور صالحه لمره واحده\(minute):0\(seconds)"
                }else{
                    str = "إعادة ارسال كلمة مرور صالحه لمره واحده0\(minute):\(seconds)"
                }
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            countDown = countDown - 1
        } else {
            self.resendOTPBtn.isEnabled = true
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            var str = "Resend OTP"
            if AppDelegate.getDelegate().appLanguage == "English"{
                str = "Resend OTP"
            }else{
                str = "إعادة ارسال كلمة مرور صالحه لمره واحده"
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if mobileNumTF.text == nil || (mobileNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileNumTF.text!, minDigits: selectCCMinLength, maxDigits: selectCCMaxLength){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        if confirmPasswordTF.text == nil || (confirmPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: confirmPasswordTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    //MARK: OTP Cancel Button Action
    @IBAction func otpCancelBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.otpSubView.isHidden = true
            self.view.addSubview(self.otpView)
        })
    }
    //MARK: Verify OTP Button Action
    @IBAction func verifyOTPBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        let otpFromTF = "\(firstDigitTF.text!)\(secondDigitTF.text!)\(thirdDigitTF.text!)\(fourthDigitTF.text!)"
        if(firstDigitTF.hasText == false || secondDigitTF.hasText == false || thirdDigitTF.hasText == false || fourthDigitTF.hasText == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidOTP", value: "", table: nil))!)
            return
        }
        if(otpString != otpFromTF){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "InvalidOTP", value: "", table: nil))!)
            return
        }else{
            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.otpSubView.isHidden = true
                self.firstDigitTF.text = ""; self.secondDigitTF.text = "";
                self.thirdDigitTF.text = "";  self.fourthDigitTF.text = "";
            })
            let dic:[String:Any] = ["UserId": userID!,
                      "OTP": "\(otpFromTF)", "RequestBy":2]
            UserModuleServices.verifyOTPService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    UserDef.saveToUserDefault(value: data.Data!.Id!, key: "UserId")
                    let dic = ["UserId":data.Data!.Id!,
                               "FullName":data.Data!.FullName!,
                                "NickName":data.Data!.NickName!,
                                "Email":data.Data!.Email!,
                                "Gender":data.Data!.Gender!,
                                "Mobile":data.Data!.Mobile!,
                                "isActive":data.Data!.isActive!,
                                "CreatedOn":data.Data!.CreatedOn!, "ModifiedOn":data.Data!.ModifiedOn!] as [String : Any]
                    UserDefaults.standard.set(dic, forKey:"userDic")
                    UserDefaults.standard.synchronize()
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    //MARK: Resend OTP Button Action
    @IBAction func resendOTPBtn_Tapped(_ sender: Any) {
        firstDigitTF.text = ""; secondDigitTF.text = ""; thirdDigitTF.text = ""; fourthDigitTF.text = "";
        let dic = ["UserId":"\(UserDef.getUserId())", "RequestBy":2, "Mobile":mobileNumTF.text! as Any ]
        UserModuleServices.forgetPasswordService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    print("OTP \(String(describing: data.Data!.OTP))")
                    ANLoader.hide()
                    self.countDown = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                    self.otpString = "\(String(describing: data.Data!.OTP))"
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
@available(iOS 13.0, *)
extension LoginVC : UITextFieldDelegate{
    //MARK: TextField Delegate Methods
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case firstDigitTF:
                secondDigitTF.becomeFirstResponder()
            case secondDigitTF:
                thirdDigitTF.becomeFirstResponder()
            case thirdDigitTF:
                fourthDigitTF.becomeFirstResponder()
            case fourthDigitTF:
                fourthDigitTF.resignFirstResponder()
            default:
                break
            }
        }
    }
    @objc func otpTextFieldChangeBackSpace(){
        if(backSpaceTF == firstDigitTF){
            if(fourthDigitTF.hasText == true){
                fourthDigitTF.becomeFirstResponder()
            }else{
                if(thirdDigitTF.hasText == true){
                    thirdDigitTF.becomeFirstResponder()
                }else{
                    if(secondDigitTF.hasText == true){
                        secondDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if(backSpaceTF == secondDigitTF){
            if(firstDigitTF.hasText == true){
                firstDigitTF.becomeFirstResponder()
            }else{
                if(fourthDigitTF.hasText == true){
                    fourthDigitTF.becomeFirstResponder()
                }else{
                    if(thirdDigitTF.hasText == true){
                        thirdDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == thirdDigitTF){
            if(secondDigitTF.hasText == true){
                secondDigitTF.becomeFirstResponder()
            }else{
                if(firstDigitTF.hasText == true){
                    firstDigitTF.becomeFirstResponder()
                }else{
                    if(thirdDigitTF.hasText == true){
                        thirdDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == fourthDigitTF){
            
            if(thirdDigitTF.hasText == true){
                thirdDigitTF.becomeFirstResponder()
            }else{
                if(secondDigitTF.hasText == true){
                    secondDigitTF.becomeFirstResponder()
                }else{
                    if(firstDigitTF.hasText == true){
                        firstDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileNumTF {
            if(self.mobileNumTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                updatedText.safelyLimitedTo(length: selectCCMaxLength)
        }
        if textField == self.firstDigitTF || textField == self.secondDigitTF || textField == self.thirdDigitTF || textField == self.fourthDigitTF {
            if(range.length == 1){
                backSpaceTF = textField
                self.perform(#selector(otpTextFieldChangeBackSpace), with: nil, afterDelay: 0.2)
                return true
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 1
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
//MARK: String Extension Mobile Number
extension String{
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    func removeStringValue() -> String {
        return self.replace(string: "+966 ", replacement: "")
    }
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    func removeCountryCode() -> String {
        return self.replace(string: "+966", replacement: "")
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension LoginVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        countryCodeLbl.text = "+\(ID)"
        selectCountryCode = Int(ID)
        let selectDic = countryCodesArray.filter({$0.CountryNameEn == name})
        self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
        //self.selectCCMinLength = selectDic[0].MinLength
        //self.selectCCMaxLength = selectDic[0].MaxLength
        mobileNumTF.text = ""
    }
}
