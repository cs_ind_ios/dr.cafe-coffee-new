//
//  ForgotPasswordVC.swift
//  drCafe
//
//  Created by Devbox on 09/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase
@available(iOS 13.0, *)
class ForgotPasswordVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var forgotPwdTitleNameLbl: UILabel!
    @IBOutlet weak var mobileNumNameLbl: UILabel!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var sendLinkBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginBtnLineLbl: UILabel!
    @IBOutlet weak var mobileNumTFView: UIView!
    
    @IBOutlet var otpView: UIView!
    @IBOutlet weak var otpSubView: UIView!
    @IBOutlet weak var otpVerificationNameLbl: UILabel!
    @IBOutlet weak var enterOTPHereNameLbl: UILabel!
    @IBOutlet weak var otpDicLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!
    @IBOutlet weak var firstDigitTF: UITextField!
    @IBOutlet weak var secondDigitTF: UITextField!
    @IBOutlet weak var thirdDigitTF: UITextField!
    @IBOutlet weak var fourthDigitTF: UITextField!
    @IBOutlet weak var otpCancelBtn: UIButton!
    @IBOutlet weak var verifyOTPBtn: UIButton!
    @IBOutlet weak var resendOTPBtn: UIButton!
    
    @IBOutlet var newPasswordView: UIView!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confNewPasswordTF: UITextField!
    @IBOutlet weak var newPwdCancelBtn: UIButton!
    @IBOutlet weak var newPwdSubmitBtn: UIButton!
    @IBOutlet weak var enterNewPwdNameLblLbl: UILabel!
    
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    var otpString = String()
    var timer:Timer?
    var countDown = 120
    var backSpaceTF = UITextField()
    var mobNum:String!
    var userId:Int!
    
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        self.newPasswordView.frame = CGRect(x: 10, y: self.view.frame.size.height+10, width: self.view.frame.size.width-20, height: 0)
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        
        mobileNumTF.delegate = self
        firstDigitTF.delegate = self
        secondDigitTF.delegate = self
        thirdDigitTF.delegate = self
        fourthDigitTF.delegate = self
        firstDigitTF.textContentType = .oneTimeCode
        secondDigitTF.textContentType = .oneTimeCode
        thirdDigitTF.textContentType = .oneTimeCode
        fourthDigitTF.textContentType = .oneTimeCode

        mobileNumTF.keyboardType = .asciiCapableNumberPad
        firstDigitTF.keyboardType = .asciiCapableNumberPad
        secondDigitTF.keyboardType = .asciiCapableNumberPad
        thirdDigitTF.keyboardType = .asciiCapableNumberPad
        fourthDigitTF.keyboardType = .asciiCapableNumberPad
        
        // changing  OTP text fields auto matically one by one
        firstDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        secondDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirdDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourthDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        forgotPwdTitleNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Forgot Password", value: "", table: nil))!
        mobileNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!
        loginBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Back to Log In", value: "", table: nil))!, for: .normal)
        sendLinkBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Send OTP", value: "", table: nil))!, for: .normal)

        if AppDelegate.getDelegate().appLanguage == "English"{
            mobileNumTF.textAlignment = .left
            mobileNumTF.semanticContentAttribute = .forceLeftToRight
        }else{
            mobileNumTF.textAlignment = .right
            mobileNumTF.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        let saudiCC = SaveAddressClass.CountryCodesListArray.filter({$0.MobileCode == 966})
        if saudiCC.count > 0{
            self.countryCodeLbl.text = "+966"
            self.selectCountryCode = 966
            self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
            //self.selectCCMinLength = saudiCC[0].MinLength
            //self.selectCCMaxLength = saudiCC[0].MaxLength
        }else{
            self.countryCodeLbl.text = "+966"
            self.selectCountryCode = 966
            self.selectCCMobileNumDigits = 9
            self.selectCCMinLength = 9
            self.selectCCMaxLength = 9
        }
        
        if SaveAddressClass.CountryCodesListArray.count == 1{
            countryBtn.isHidden = true
        }else{
            countryBtn.isHidden = false
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    //MARK: Forget Password Link Send Button
    @IBAction func sendLinkBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        mobNum = "\(self.mobileNumTF.text!)"
        let dic = ["RequestBy":2, "Mobile":mobNum! as Any ]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("forgetPasswordService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
       // Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        UserModuleServices.forgetPasswordService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.countDown = 120
                self.mobileNumLbl.text = "+\(self.selectCountryCode!) \(self.mobileNumTF.text!)"
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                self.otpString = "\(String(describing: data.Data!.OTP!))"
                self.userId = data.Data!.Id!
                print("OTP \(String(describing: data.Data!.OTP))")
                 self.firstDigitTF.text = nil
                self.secondDigitTF.text = nil
                self.thirdDigitTF.text = nil
                self.fourthDigitTF.text = nil
                UIView.animate(withDuration: 0.6, animations: { () -> Void in
                    self.otpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                    self.otpSubView.isHidden = false
                    self.view.addSubview(self.otpView)
                    self.firstDigitTF.becomeFirstResponder()
                })
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back To Login Button Action
    @IBAction func loginBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Country Code Drop Down
    @IBAction func countryBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = SaveAddressClass.CountryCodesListArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.center.x+25 , y: -15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.center.x-25 , y: -15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 1
        obj.codeDropDownArray = SaveAddressClass.CountryCodesListArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if mobileNumTF.text == nil || (mobileNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileNumTF.text!, minDigits: selectCCMinLength, maxDigits: selectCCMaxLength){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    //MARK: OTP View Close Button Action
    @IBAction func closeBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.otpSubView.isHidden = true
            self.view.addSubview(self.otpView)
        })
    }
    //MARK: Verify OTP Button Action
    @IBAction func verifyOTPBtn_Tapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let otpFromTF = "\(firstDigitTF.text!)\(secondDigitTF.text!)\(thirdDigitTF.text!)\(fourthDigitTF.text!)"
        if(firstDigitTF.hasText == false || secondDigitTF.hasText == false || thirdDigitTF.hasText == false || fourthDigitTF.hasText == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidOTP", value: "", table: nil))!)
            return
        }
        if(otpString != otpFromTF){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "InvalidOTP", value: "", table: nil))!)
            return
        }else{
            self.newPasswordTF!.text = nil
            self.confNewPasswordTF!.text = nil

            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.newPasswordView.frame = CGRect(x: 10, y: self.otpSubView.frame.origin.y+70, width: self.view.frame.size.width-20, height: 230)
                self.newPasswordView.layer.cornerRadius = 8.0
                self.newPasswordView.layer.masksToBounds = true
                self.otpSubView.isHidden = true
                self.view.addSubview(self.newPasswordView)
                self.firstDigitTF.text = ""; self.secondDigitTF.text = "";
                self.thirdDigitTF.text = "";  self.fourthDigitTF.text = "";
            })
        }
    }
    //MARK: Resend OTP Button Action
    @IBAction func resendOTPBtn_Tapped(_ sender: UIButton) {
        firstDigitTF.text = ""; secondDigitTF.text = ""; thirdDigitTF.text = ""; fourthDigitTF.text = "";
        let dic = ["RequestBy":2, "Mobile":mobileNumTF.text! as Any ]
        UserModuleServices.forgetPasswordService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    print("OTP \(String(describing: data.Data!.OTP))")
                    ANLoader.hide()
                    self.countDown = 120
                    self.userId = data.Data!.Id!
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                    self.otpString = "\(String(describing: data.Data!.OTP!))"
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: New Password Submit Button Action
    @IBAction func newPasswordSubmitBtn_Tapped(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        self.view.endEditing(true)
        if(validPasswordInputParams() == false){
            return
        }
        var language = "En"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }else{
            language = "Ar"
        }
//        let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//        let deviceInfo:[String:Any] = [
//            "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//            "applang":language,
//            "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//            "SDKversion":"",
//            "Modelname":"\(UIDevice.current.name)",
//            "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//            "AppVersion":"IOS(\(appVersion))",
//            "TimeZone":"\(TimeZone.current.abbreviation()!)",
//            "TimeZoneRegion":"\(TimeZone.current.identifier)"]
        let dic = ["UserId":"\(userId!)", "DeviceInfo": "\(getDeviceInfo())",  "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "Language": "\(language)", "RequestBy":2, "NewPassword":newPasswordTF.text! as Any ]
        UserModuleServices.newPasswordService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                UIView.animate(withDuration: 1.0, animations: { () -> Void in
                    self.newPasswordView.frame = CGRect(x: 10, y: self.view.frame.size.height+10, width: self.view.frame.size.width-20, height: 0)
                    self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
                    self.otpSubView.isHidden = false
                    self.newPasswordView.removeFromSuperview()
                    self.otpView.removeFromSuperview()
                })
                let alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordSuccess", value: "", table: nil))!, preferredStyle: .alert)
                let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.mobileNumTF.text = ""
                    self.confNewPasswordTF.text = ""
                    self.newPasswordTF.text = ""
                    self.firstDigitTF.text = ""; self.secondDigitTF.text = ""; self.thirdDigitTF.text = ""; self.fourthDigitTF.text = "";
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: false, completion: nil)
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: New Password Cancel Button Action
    @IBAction func newPasswordCancelBtn_Tapped(_ sender: UIButton) {
        self.newPasswordView.frame = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height+10, width: 0, height: 0)
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        self.otpSubView.isHidden = false
        self.newPasswordView.removeFromSuperview()
        self.otpView.removeFromSuperview()
        self.mobileNumTF.text = ""
        firstDigitTF.text = ""; secondDigitTF.text = ""; thirdDigitTF.text = ""; fourthDigitTF.text = "";
    }
    //MARK: CountDown Time Update
    @objc func updateCountDown() {
        if(countDown > 0) {
            let minute = String(countDown/60)
            let seconds = String(countDown % 60)
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            self.resendOTPBtn.isEnabled = false
            var str = "Resend OTP 0\(minute):\(seconds)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                if(UInt8(seconds)! < 10){
                    str = "Resend OTP 0\(minute):0\(seconds)"
                }else{
                    str = "Resend OTP 0\(minute):\(seconds)"
                }
            }else{
                if(UInt8(seconds)! < 10){
                    str = "0إعادة ارسال كلمة مرور صالحه لمره واحده\(minute):0\(seconds)"
                }else{
                    str = "إعادة ارسال كلمة مرور صالحه لمره واحده0\(minute):\(seconds)"
                }
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            countDown = countDown - 1
        } else {
            self.resendOTPBtn.isEnabled = true
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2352941176, blue: 0.2392156863, alpha: 1)
            var str = "Resend OTP"
            if AppDelegate.getDelegate().appLanguage == "English"{
                str = "Resend OTP"
            }else{
                str = "إعادة ارسال كلمة مرور صالحه لمره واحده"
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    //MARK: Password Validation
    func validPasswordInputParams() -> Bool {
        if newPasswordTF.text == nil || (newPasswordTF.text == ""){
            Alert.showAlert(on: self, title:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password Name", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewPassword", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: newPasswordTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "InvalidPassword", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if confNewPasswordTF.text == nil || (confNewPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmPassword", value: "", table: nil))!)
            return false
        }else{
            if !(newPasswordTF.text == confNewPasswordTF.text){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VerifyPassword", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
}
//MARK: TextField Delegate Method
@available(iOS 13.0, *)
extension ForgotPasswordVC : UITextFieldDelegate{
     @objc func textFieldDidChange(textField: UITextField){
            let text = textField.text
            if text?.count == 1 {
                switch textField{
                case firstDigitTF:
                    secondDigitTF.becomeFirstResponder()
                case secondDigitTF:
                    thirdDigitTF.becomeFirstResponder()
                case thirdDigitTF:
                    fourthDigitTF.becomeFirstResponder()
                case fourthDigitTF:
                    fourthDigitTF.resignFirstResponder()
                default:
                    break
                }
            }
        }
        @objc func otpTextFieldChangeBackSpace(){
            if(backSpaceTF == firstDigitTF){
                if(fourthDigitTF.hasText == true){
                    fourthDigitTF.becomeFirstResponder()
                }else{
                    if(thirdDigitTF.hasText == true){
                        thirdDigitTF.becomeFirstResponder()
                    }else{
                        if(secondDigitTF.hasText == true){
                            secondDigitTF.becomeFirstResponder()
                        }else{
                            self.view.endEditing(true)
                        }
                    }
                }
            }else if(backSpaceTF == secondDigitTF){
                if(firstDigitTF.hasText == true){
                    firstDigitTF.becomeFirstResponder()
                }else{
                    if(fourthDigitTF.hasText == true){
                        fourthDigitTF.becomeFirstResponder()
                    }else{
                        if(thirdDigitTF.hasText == true){
                            thirdDigitTF.becomeFirstResponder()
                        }else{
                            self.view.endEditing(true)
                        }
                    }
                }
            }else if (backSpaceTF == thirdDigitTF){
                if(secondDigitTF.hasText == true){
                    secondDigitTF.becomeFirstResponder()
                }else{
                    if(firstDigitTF.hasText == true){
                        firstDigitTF.becomeFirstResponder()
                    }else{
                        if(thirdDigitTF.hasText == true){
                            thirdDigitTF.becomeFirstResponder()
                        }else{
                            self.view.endEditing(true)
                        }
                    }
                }
            }else if (backSpaceTF == fourthDigitTF){
                if(thirdDigitTF.hasText == true){
                    thirdDigitTF.becomeFirstResponder()
                }else{
                    if(secondDigitTF.hasText == true){
                        secondDigitTF.becomeFirstResponder()
                    }else{
                        if(firstDigitTF.hasText == true){
                            firstDigitTF.becomeFirstResponder()
                        }else{
                            self.view.endEditing(true)
                        }
                    }
                }
            }
        }
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            return true
        }
        public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let whitespaceSet = NSCharacterSet.whitespaces
            let range1 = string.rangeOfCharacter(from: whitespaceSet)
            if ((textField.text?.count)! == 0 && range1 != nil)
                  || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
                return false
            }
            if textField == self.mobileNumTF {
                if(self.mobileNumTF.text?.count == 0){
                    let num = "123456789"
                    let checNum = CharacterSet.init(charactersIn: num)
                    if (string.rangeOfCharacter(from: checNum) != nil){
                        return true
                    }else{
                        return false
                    }
                }
                let currentText = textField.text ?? ""
                guard let stringRange = Range(range, in: currentText) else { return false }
                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                    updatedText.safelyLimitedTo(length: selectCCMaxLength)
            }
            if textField == self.firstDigitTF || textField == self.secondDigitTF || textField == self.thirdDigitTF || textField == self.fourthDigitTF {
                if(range.length == 1){
                    backSpaceTF = textField
                    self.perform(#selector(otpTextFieldChangeBackSpace), with: nil, afterDelay: 0.2)
                    return true
                }
                let currentText = textField.text ?? ""
                guard let stringRange = Range(range, in: currentText) else { return false }
                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                return updatedText.count <= 1
            }
            return true
        }
        // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true;
        }
}
//MARK: Country Code DropDown Delegate
@available(iOS 13.0, *)
extension ForgotPasswordVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        countryCodeLbl.text = "+\(ID)"
        selectCountryCode = Int(ID)
        let selectDic = SaveAddressClass.CountryCodesListArray.filter({$0.CountryNameEn == name})
        self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
        //self.selectCCMinLength = selectDic[0].MinLength
        //self.selectCCMaxLength = selectDic[0].MaxLength
        mobileNumTF.text = ""
    }
}
