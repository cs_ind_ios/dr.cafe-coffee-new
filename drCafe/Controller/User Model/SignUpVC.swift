//
//  SignUpVC.swift
//  drCafe
//
//  Created by Devbox on 09/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase
@available(iOS 13.0, *)
class SignUpVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var signUpTitleNameLbl: UILabel!
    @IBOutlet weak var alreadyHaveAnAccountNameLbl: UILabel!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var loginBtnLineLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailNameLbl: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumNameLbl: UILabel!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var passwordNameLbl: UILabel!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordShowBtn: UIButton!
    @IBOutlet weak var confirmPasswordNameLbl: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordShowBtn: UIButton!
    @IBOutlet weak var confirmPasswordMsgLbl: UILabel!
    @IBOutlet weak var termsCheckBtn: UIButton!
    @IBOutlet weak var iAgreeToTheNameLbl: UILabel!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var andNameLbl: UILabel!
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var fullNameTFView: UIView!
    @IBOutlet weak var mobileNumTFView: UIView!
    @IBOutlet weak var emailTFView: UIView!
    @IBOutlet weak var passwordTFView: UIView!
    @IBOutlet weak var ConfiramPwdTFView: UIView!
    
    @IBOutlet var otpView: UIView!
    @IBOutlet weak var otpSubView: UIView!
    @IBOutlet weak var otpVerificationNameLbl: UILabel!
    @IBOutlet weak var enterOTPHereNameLbl: UILabel!
    @IBOutlet weak var otpDicLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!
    @IBOutlet weak var firstDigitTF: UITextField!
    @IBOutlet weak var secondDigitTF: UITextField!
    @IBOutlet weak var thirdDigitTF: UITextField!
    @IBOutlet weak var fourthDigitTF: UITextField!
    @IBOutlet weak var verifyOTPBtn: UIButton!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var otpCancelBtn: UIButton!
    
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    @IBOutlet weak var referrelCodeBtn: UIButton!
    @IBOutlet weak var referrelCodeLineLbl: UILabel!
    @IBOutlet weak var referrelCodeTF: UITextField!
    @IBOutlet weak var referrelCodeTFViewHeight: NSLayoutConstraint!
    @IBOutlet weak var referrelCodeTFView: UIView!
    
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    
    var otpString = String()
    var timer:Timer?
    var countDown = 120
    var backSpaceTF = UITextField()
    var userID:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        fullNameTF.delegate = self
        mobileNumTF.delegate = self
        firstDigitTF.delegate = self
        secondDigitTF.delegate = self
        thirdDigitTF.delegate = self
        fourthDigitTF.delegate = self
        
        firstDigitTF.textContentType = .oneTimeCode
        secondDigitTF.textContentType = .oneTimeCode
        thirdDigitTF.textContentType = .oneTimeCode
        fourthDigitTF.textContentType = .oneTimeCode
        
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        
        // changing  OTP text fields auto matically one by one
        firstDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        secondDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirdDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourthDigitTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        signUpTitleNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sign Up", value: "", table: nil))!
        alreadyHaveAnAccountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Already have an account", value: "", table: nil))!
        fullNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Full Name", value: "", table: nil))!
        emailNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email Name", value: "", table: nil))!
        mobileNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!
        passwordNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password Name", value: "", table: nil))!
        confirmPasswordNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirm Password", value: "", table: nil))!
        signUpBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sign Up", value: "", table: nil))!, for: .normal)
        logInBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Log In", value: "", table: nil))!, for: .normal)
        iAgreeToTheNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "I agree to the", value: "", table: nil))!
        andNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "and", value: "", table: nil))!
        termsBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms", value: "", table: nil))!, for: .normal)
        privacyPolicyBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Privacy Policy", value: "", table: nil))!, for: .normal)
            
        if AppDelegate.getDelegate().appLanguage == "English"{
            fullNameTF.textAlignment = .left
            fullNameTF.semanticContentAttribute = .forceLeftToRight
            emailTF.textAlignment = .left
            emailTF.semanticContentAttribute = .forceLeftToRight
            mobileNumTF.textAlignment = .left
            mobileNumTF.semanticContentAttribute = .forceLeftToRight
            passwordTF.textAlignment = .left
            passwordTF.semanticContentAttribute = .forceLeftToRight
            confirmPasswordTF.textAlignment = .left
            confirmPasswordTF.semanticContentAttribute = .forceLeftToRight
        }else{
            fullNameTF.textAlignment = .right
            fullNameTF.semanticContentAttribute = .forceRightToLeft
            emailTF.textAlignment = .right
            emailTF.semanticContentAttribute = .forceRightToLeft
            mobileNumTF.textAlignment = .right
            mobileNumTF.semanticContentAttribute = .forceRightToLeft
            passwordTF.textAlignment = .right
            passwordTF.semanticContentAttribute = .forceRightToLeft
            confirmPasswordTF.textAlignment = .right
            confirmPasswordTF.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        let saudiCC = SaveAddressClass.CountryCodesListArray.filter({$0.MobileCode == 966})
        if saudiCC.count > 0{
            self.countryCodeLbl.text = "+966"
            self.selectCountryCode = 966
            self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
            //self.selectCCMinLength = saudiCC[0].MinLength
            //self.selectCCMaxLength = saudiCC[0].MaxLength
        }else{
            self.countryCodeLbl.text = "+966"
            self.selectCountryCode = 966
            self.selectCCMobileNumDigits = 9
            self.selectCCMinLength = 9
            self.selectCCMaxLength = 9
        }
        
        if SaveAddressClass.CountryCodesListArray.count == 1{
            countryBtn.isHidden = true
        }else{
            countryBtn.isHidden = false
        }
        termsCheckBtn.setImage(UIImage(named: "unchecked-checkbox"), for: .normal)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    //MARK: Login Button Action
    @IBAction func loginBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Password Show Button Action
    @IBAction func passwordShowBtn_Tapped(_ sender: Any) {
        if passwordTF.isSecureTextEntry == false{
            passwordTF.isSecureTextEntry = true
            passwordShowBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
        }else{
            passwordTF.isSecureTextEntry = false
            passwordShowBtn.setImage(UIImage(named: "show Password"), for: .normal)
        }
    }
    //MARK: Confirm Password Show Button Action
    @IBAction func ConfirmPasswordShowBtn_Tapped(_ sender: Any) {
        if confirmPasswordTF.isSecureTextEntry == false{
            confirmPasswordTF.isSecureTextEntry = true
            confirmPasswordShowBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
        }else{
            confirmPasswordTF.isSecureTextEntry = false
            confirmPasswordShowBtn.setImage(UIImage(named: "show Password"), for: .normal)
        }
    }
    //MARK: ReferrelCode Button
    @IBAction func referrelCodeBtn_Tapped(_ sender: Any) {
        referrelCodeTFViewHeight.constant = 101
        referrelCodeBtn.isHidden = true
        referrelCodeLineLbl.isHidden = true
        referrelCodeTFView.isHidden = false
    }
    @IBAction func referrelCodeCancelBtn_Tapped(_ sender: Any) {
        referrelCodeTFViewHeight.constant = 41
        referrelCodeBtn.isHidden = false
        referrelCodeLineLbl.isHidden = false
        referrelCodeTFView.isHidden = true
    }
    //MARK: Terms check Button
    @IBAction func termsCheckBtn_Tapped(_ sender: Any) {
        if termsCheckBtn.currentImage == UIImage(named: "unchecked-checkbox") || termsCheckBtn.currentImage == UIImage(named: "Box Un Select"){
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                termsCheckBtn.setImage(UIImage(named: "Box Selected"), for: .normal)
            }else{
                termsCheckBtn.setImage(UIImage(named: "checked-checkbox"), for: .normal)
            }
        }else{
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                termsCheckBtn.setImage(UIImage(named: "Box Un Select"), for: .normal)
            }else{
                termsCheckBtn.setImage(UIImage(named: "unchecked-checkbox"), for: .normal)
            }
        }
    }
    //MARK: Country Code Drop Down
    @IBAction func countryBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = SaveAddressClass.CountryCodesListArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.center.x+25 , y: -15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.center.x-25 , y: -15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 1
        obj.codeDropDownArray = SaveAddressClass.CountryCodesListArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Terms Button Action
    @IBAction func termsBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms and Conditions", value: "", table: nil))!
        //Obj.url = "https://drcafestore.azurewebsites.net/Shopping/AppTermsandConditions"
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppTermsandConditions"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppTermsandConditions"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    //MARK: Privsacy Policy Button Action
    @IBAction func privacyPolicyBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms and Conditions", value: "", table: nil))!
        //Obj.url = "https://drcafestore.azurewebsites.net/Shopping/AppTermsandConditions"
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppTermsandConditions"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppTermsandConditions"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    //MARK: SignUp Button Action
    @IBAction func signUpBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var referrelCode = ""
        if referrelCodeTF.text == nil || referrelCodeTF.text! == ""{
            referrelCode = ""
        }else{
            referrelCode = referrelCodeTF.text!
        }
        var language = "En"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }else{
            language = "Ar"
        }
//        let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//        let deviceInfo:[String:Any] = [
//            "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//            "applang":language,
//            "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//            "SDKversion":"",
//            "Modelname":"\(UIDevice.current.name)",
//            "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//            "AppVersion":"IOS(\(appVersion))",
//            "TimeZone":"\(TimeZone.current.abbreviation()!)",
//            "TimeZoneRegion":"\(TimeZone.current.identifier)"]
        let dic = [
            "FullName": "\(fullNameTF.text!)",
            "Mobile": "\(mobileNumTF.text!)",
            "Password": "\(passwordTF.text!)",
            "Email": "\(emailTF.text!)",
            "CountryCode": "\(selectCountryCode!)",
            "ReferralCode": "\(referrelCode)",
            "Language": "En",
            "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
            "DeviceInfo": "\(getDeviceInfo())", "RequestBy":2
            ] as [String : Any]
        
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("signUpService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
       // Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        UserModuleServices.signUpService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                print("OTP \(data.Data!.OTP!)")
                DispatchQueue.main.async() {
                    self.userID = data.Data!.Id!
                    self.otpString = "\(String(describing: data.Data!.OTP!))"
                    self.mobileNumLbl.text = "+\(self.selectCountryCode!) \(self.mobileNumTF.text!)"
                    self.countDown = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                    UIView.animate(withDuration: 0.6, animations: { () -> Void in
                        self.otpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        self.otpSubView.isHidden = false
                        self.view.addSubview(self.otpView)
                        self.firstDigitTF.becomeFirstResponder()
                    })
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: count down timer
    @objc func updateCountDown() {
        if(countDown > 0) {
            let minute = String(countDown/60)
            let seconds = String(countDown % 60)
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            self.resendOTPBtn.isEnabled = false
            var str = "Resend OTP 0\(minute):\(seconds)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                if(UInt8(seconds)! < 10){
                    str = "Resend OTP 0\(minute):0\(seconds)"
                }else{
                    str = "Resend OTP 0\(minute):\(seconds)"
                }
            }else{
                if(UInt8(seconds)! < 10){
                    str = "0إعادة ارسال كلمة مرور صالحه لمره واحده\(minute):0\(seconds)"
                }else{
                    str = "إعادة ارسال كلمة مرور صالحه لمره واحده0\(minute):\(seconds)"
                }
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            countDown = countDown - 1
        } else {
            self.resendOTPBtn.isEnabled = true
            self.resendOTPBtn.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2352941176, blue: 0.2392156863, alpha: 1)
            var str = "Resend OTP"
            if AppDelegate.getDelegate().appLanguage == "English"{
                str = "Resend OTP"
            }else{
                str = "إعادة ارسال كلمة مرور صالحه لمره واحده"
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if fullNameTF.text == nil || (fullNameTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EnterFullName", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isCharCount(name: fullNameTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                return false
            }
        }
        if emailTF.text == nil || (emailTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isValidEmail(email: emailTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EmailValidation", value: "", table: nil))!)
                return false
            }
        }
        if mobileNumTF.text == nil || (mobileNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileNumTF.text!, minDigits: selectCCMinLength, maxDigits: selectCCMaxLength){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        if passwordTF.text == nil || (passwordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: passwordTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if confirmPasswordTF.text == nil || (confirmPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmPassword", value: "", table: nil))!)
            return false
        }else{
            if passwordTF.text != confirmPasswordTF.text {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VerifyPassword", value: "", table: nil))!)
                return false
            }
        }
        if termsCheckBtn.currentImage == UIImage(named: "unchecked-checkbox") || termsCheckBtn.currentImage == UIImage(named: "Box Un Select"){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "TermsAlert", value: "", table: nil))!)
            return false
        }
        return true
    }
    //MARK: OTP Cancel Button Action
    @IBAction func otpCancelBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.otpSubView.isHidden = true
            self.view.addSubview(self.otpView)
        })
    }
    //MARK: Verify OTP Button Action
    @IBAction func verifyOTPBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        let otpFromTF = "\(firstDigitTF.text!)\(secondDigitTF.text!)\(thirdDigitTF.text!)\(fourthDigitTF.text!)"
        if(firstDigitTF.hasText == false || secondDigitTF.hasText == false || thirdDigitTF.hasText == false || fourthDigitTF.hasText == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidOTP", value: "", table: nil))!)
            return
        }
        if(otpString != otpFromTF){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "InvalidOTP", value: "", table: nil))!)
            return
        }else{
            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.otpSubView.isHidden = true
                self.firstDigitTF.text = ""; self.secondDigitTF.text = "";
                self.thirdDigitTF.text = "";  self.fourthDigitTF.text = "";
            })
            let dic:[String:Any] = ["UserId": userID!,
                      "OTP": "\(otpFromTF)", "RequestBy":2]
            UserModuleServices.verifyOTPService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    UserDef.saveToUserDefault(value: data.Data!.Id!, key: "UserId")
                    var nickName = ""
                    if data.Data!.NickName != nil{
                        nickName = data.Data!.NickName!
                    }
                    var gender = ""
                    if data.Data!.Gender != nil{
                        gender = data.Data!.Gender!
                    }
                    let dic = ["UserId":data.Data!.Id!,
                               "FullName":data.Data!.FullName!,
                                "NickName":nickName,
                                "Email":data.Data!.Email!,
                                "Gender":gender,
                                "Mobile":data.Data!.Mobile!,
                                "isActive":data.Data!.isActive!,
                                "CreatedOn":data.Data!.CreatedOn!, "ModifiedOn":data.Data!.ModifiedOn!] as [String : Any]
                    UserDefaults.standard.set(dic, forKey:"userDic")
                    UserDefaults.standard.synchronize()
                    LoginVC.instance.whereObj = 1
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    //MARK: Resend OTP Button Action
    @IBAction func resendOTPBtn_Tapped(_ sender: Any) {
        firstDigitTF.text = ""; secondDigitTF.text = ""; thirdDigitTF.text = ""; fourthDigitTF.text = "";
        var language = "En"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }else{
            language = "Ar"
        }
//        let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//        let deviceInfo:[String:Any] = [
//            "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//            "applang":language,
//            "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//            "SDKversion":"",
//            "Modelname":"\(UIDevice.current.name)",
//            "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//            "AppVersion":"IOS(\(appVersion))",
//            "TimeZone":"\(TimeZone.current.abbreviation()!)",
//            "TimeZoneRegion":"\(TimeZone.current.identifier)"]
        let dic = [
            "FullName": "\(fullNameTF.text!)",
            "Mobile": "\(mobileNumTF.text!)",
            "Password": "\(passwordTF.text!)",
            "Email": "\(emailTF.text!)",
            "CountryCode": "\(selectCountryCode!)",
            "Language": "En",
            "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
            "DeviceInfo": "\(getDeviceInfo())", "RequestBy":2
            ] as [String : Any]
        UserModuleServices.signUpService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                print("OTP \(String(describing: data.Data!.OTP!))")
                DispatchQueue.main.async {
                    ANLoader.hide()
                    self.countDown = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                    self.otpString = "\(String(describing: data.Data!.OTP!))"
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
@available(iOS 13.0, *)
extension SignUpVC : UITextFieldDelegate{
    //MARK: TextField Delegate Methods
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case firstDigitTF:
                secondDigitTF.becomeFirstResponder()
            case secondDigitTF:
                thirdDigitTF.becomeFirstResponder()
            case thirdDigitTF:
                fourthDigitTF.becomeFirstResponder()
            case fourthDigitTF:
                fourthDigitTF.resignFirstResponder()
            default:
                break
            }
        }
    }
    @objc func otpTextFieldChangeBackSpace(){
        if(backSpaceTF == firstDigitTF){
            if(fourthDigitTF.hasText == true){
                fourthDigitTF.becomeFirstResponder()
            }else{
                if(thirdDigitTF.hasText == true){
                    thirdDigitTF.becomeFirstResponder()
                }else{
                    if(secondDigitTF.hasText == true){
                        secondDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if(backSpaceTF == secondDigitTF){
            if(firstDigitTF.hasText == true){
                firstDigitTF.becomeFirstResponder()
            }else{
                if(fourthDigitTF.hasText == true){
                    fourthDigitTF.becomeFirstResponder()
                }else{
                    if(thirdDigitTF.hasText == true){
                        thirdDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == thirdDigitTF){
            if(secondDigitTF.hasText == true){
                secondDigitTF.becomeFirstResponder()
            }else{
                if(firstDigitTF.hasText == true){
                    firstDigitTF.becomeFirstResponder()
                }else{
                    if(thirdDigitTF.hasText == true){
                        thirdDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == fourthDigitTF){
            
            if(thirdDigitTF.hasText == true){
                thirdDigitTF.becomeFirstResponder()
            }else{
                if(secondDigitTF.hasText == true){
                    secondDigitTF.becomeFirstResponder()
                }else{
                    if(firstDigitTF.hasText == true){
                        firstDigitTF.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileNumTF {
            if(self.mobileNumTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: selectCCMaxLength)
        }
        if textField == self.firstDigitTF || textField == self.secondDigitTF || textField == self.thirdDigitTF || textField == self.fourthDigitTF {
            if(range.length == 1){
                backSpaceTF = textField
                self.perform(#selector(otpTextFieldChangeBackSpace), with: nil, afterDelay: 0.2)
                return true
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 1
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension SignUpVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        countryCodeLbl.text = "+\(ID)"
        selectCountryCode = Int(ID)
        let selectDic = SaveAddressClass.CountryCodesListArray.filter({$0.CountryNameEn == name})
        self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
        //self.selectCCMinLength = selectDic[0].MinLength
        //self.selectCCMaxLength = selectDic[0].MaxLength
        mobileNumTF.text = ""
    }
}
