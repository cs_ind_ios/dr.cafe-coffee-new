//
//  ChangePasswordVC.swift
//  drCafe
//
//  Created by Devbox on 09/09/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase

@available(iOS 13.0, *)
class ChangePasswordVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var changePwdTitleNameLbl: UILabel!
    @IBOutlet weak var oldPwdNameLbl: UILabel!
    @IBOutlet weak var oldPasswordTF: UITextField!
    @IBOutlet weak var newPwdNameLbl: UILabel!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPwdNameLbl: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var newPwdMsgNameLbl: UILabel!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var oldPwdShowBtn: UIButton!
    @IBOutlet weak var newPwdShowBtn: UIButton!
    @IBOutlet weak var conirmNewPwdShowBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    var whereObj = 0
    var showPassword = "show Password"
    var hidePassword = "Hide Password"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        oldPasswordTF.delegate = self
        newPasswordTF.delegate = self
        confirmPasswordTF.delegate = self
        
        navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        changePwdTitleNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Password", value: "", table: nil))!
        oldPwdNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Old Password", value: "", table: nil))!
        newPwdNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "New Password", value: "", table: nil))!
        confirmPwdNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirm Password", value: "", table: nil))!
        changePasswordBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Password", value: "", table: nil))!, for: .normal)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            oldPasswordTF.textAlignment = .left
            oldPasswordTF.semanticContentAttribute = .forceLeftToRight
            newPasswordTF.textAlignment = .left
            newPasswordTF.semanticContentAttribute = .forceLeftToRight
            confirmPasswordTF.textAlignment = .left
            confirmPasswordTF.semanticContentAttribute = .forceLeftToRight
        }else{
            oldPasswordTF.textAlignment = .right
            oldPasswordTF.semanticContentAttribute = .forceRightToLeft
            newPasswordTF.textAlignment = .right
            newPasswordTF.semanticContentAttribute = .forceRightToLeft
            confirmPasswordTF.textAlignment = .right
            confirmPasswordTF.semanticContentAttribute = .forceRightToLeft
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            // User Interface is Dark
            //self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            changePasswordBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            changePasswordBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            changePwdTitleNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            oldPwdNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            newPwdNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            confirmPwdNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            oldPasswordTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            newPasswordTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            confirmPasswordTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            showPassword = "show Password Ar"
            hidePassword = "Hide Password Ar"
            oldPwdShowBtn.setImage(UIImage(named: "Hide Password Ar"), for: .normal)
            newPwdShowBtn.setImage(UIImage(named: "Hide Password Ar"), for: .normal)
            conirmNewPwdShowBtn.setImage(UIImage(named: "Hide Password Ar"), for: .normal)
        }else{
            changePasswordBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            showPassword = "show Password"
            hidePassword = "Hide Password"
            oldPwdShowBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
            newPwdShowBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
            conirmNewPwdShowBtn.setImage(UIImage(named: "Hide Password"), for: .normal)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj != 1{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 4
        }
    }
    //MARK: Change Password Button Action
    @IBAction func changePasswordBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        let dic = ["UserId":"\(UserDef.getUserId())", "OldPassword":"\(oldPasswordTF.text!)", "NewPassword":"\(newPasswordTF.text!)", "RequestBy":2] as [String : Any]
        
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("changePasswordService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        //Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        UserModuleServices.changePasswordService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                var alertController = UIAlertController()
                if AppDelegate.getDelegate().appLanguage == "English"{
                    alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, preferredStyle: .alert)
                }else{
                    alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, preferredStyle: .alert)
                }
                
                let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: false)
                    }
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: false, completion: nil)
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Old Password Show Button Actions
    @IBAction func oldPasswordShowBtn_Tapped(_ sender: UIButton) {
        if oldPasswordTF.isSecureTextEntry == false{
            oldPasswordTF.isSecureTextEntry = true
            oldPwdShowBtn.setImage(UIImage(named: hidePassword), for: .normal)
        }else{
            oldPasswordTF.isSecureTextEntry = false
            oldPwdShowBtn.setImage(UIImage(named: showPassword), for: .normal)
        }
    }
    //MARK: New Password Show Button Actions
    @IBAction func newPasswordShowBtn_Tapped(_ sender: UIButton) {
        if newPasswordTF.isSecureTextEntry == false{
            newPasswordTF.isSecureTextEntry = true
            newPwdShowBtn.setImage(UIImage(named: hidePassword), for: .normal)
        }else{
            newPasswordTF.isSecureTextEntry = false
            newPwdShowBtn.setImage(UIImage(named: showPassword), for: .normal)
        }
    }
    //MARK: Confirm Password Show Button Actions
    @IBAction func confirmPasswordShowBtn_Tapped(_ sender: UIButton) {
        if confirmPasswordTF.isSecureTextEntry == false{
            confirmPasswordTF.isSecureTextEntry = true
            conirmNewPwdShowBtn.setImage(UIImage(named: hidePassword), for: .normal)
        }else{
            confirmPasswordTF.isSecureTextEntry = false
            conirmNewPwdShowBtn.setImage(UIImage(named: showPassword), for: .normal)
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if oldPasswordTF.text == nil || (oldPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EnterOldPwd", value: "", table: nil))!)
            return false
        }
        if newPasswordTF.text == nil || (newPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewPassword", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: newPasswordTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if confirmPasswordTF.text == nil || (confirmPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmPassword", value: "", table: nil))!)
            return false
        }else{
            if newPasswordTF.text != confirmPasswordTF.text {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VerifyPassword", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
}
//MARK: TextField Delegate
@available(iOS 13.0, *)
extension ChangePasswordVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if(string == "#"){
            return false
        }else{
            return true
        }
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.returnKeyType == .default)
        {
            textField.resignFirstResponder()
            return true
        }
        if (textField == oldPasswordTF)
        {
            newPasswordTF.becomeFirstResponder()
        }
        if (textField==newPasswordTF)
        {
            confirmPasswordTF.becomeFirstResponder()
        }
        return true;
    }
}
