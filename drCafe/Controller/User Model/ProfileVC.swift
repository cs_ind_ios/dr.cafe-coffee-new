//
//  ProfileVC.swift
//  drCafe
//
//  Created by Devbox on 06/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase
@available(iOS 13.0, *)
class ProfileVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var editProfileLineLbl: UILabel!
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var emailTitleLbl: UILabel!
    @IBOutlet weak var mobileNumTitleLbl: UILabel!
    @IBOutlet weak var genderTitleLbl: UILabel!
    @IBOutlet weak var dateOfBirthTitleLbl: UILabel!
    @IBOutlet weak var professionTitleLbl: UILabel!
    @IBOutlet weak var interestsTitleLbl: UILabel!
    @IBOutlet weak var nickNameTitleLbl: UILabel!
    @IBOutlet weak var nickNameTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var dateOfBirthTF: UITextField!
    @IBOutlet weak var professionTF: UITextField!
    @IBOutlet weak var interestsTF: UITextField!
    @IBOutlet weak var nameTFView: UIView!
    @IBOutlet weak var nickNameTFView: UIView!
    @IBOutlet weak var emailTFView: UIView!
    @IBOutlet weak var mobileNumTFView: UIView!
    @IBOutlet weak var genderTFView: UIView!
    @IBOutlet weak var DOBTFView: UIView!
    @IBOutlet weak var professionTFView: UIView!
    @IBOutlet weak var interestsTFView: UIView!
    @IBOutlet weak var deleteMyAccountBtn: UIButton!
    
    static var instance: ProfileVC!
    var profileDetailsArray = [ProfileDetailsModel]()
    var whereObj = 0
    var backObj = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProfileVC.instance = self
        self.tabBarController?.tabBar.isHidden = true
        titleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Profile", value: "", table: nil))!)"
        editProfileBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Edit Profile", value: "", table: nil))!)", for: .normal)
        nameTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Name", value: "", table: nil))!)"
        nickNameTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Nick Name", value: "", table: nil))!)"
        emailTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email Name", value: "", table: nil))!)"
        mobileNumTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!)"
        genderTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gender", value: "", table: nil))!)"
        dateOfBirthTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date Of Birth", value: "", table: nil))!)"
        professionTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Profession", value: "", table: nil))!)"
        interestsTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Interests", value: "", table: nil))!)"
        deleteMyAccountBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete My Account", value: "", table: nil))!, for: .normal)
        
    }
    //MARK: View will Appear
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileDetailsService()
        
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            // User Interface is Dark
            //self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            editProfileBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            editProfileLineLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nickNameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nickNameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            emailTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            emailTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mobileNumTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mobileNumTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            genderTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            genderTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            dateOfBirthTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            dateOfBirthTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            professionTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            professionTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            interestsTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            interestsTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            nickNameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            emailTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            mobileNumTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            genderTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            DOBTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            professionTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            interestsTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if backObj == 1{
            self.navigationController?.popViewController(animated: false)
        }
    }
    //MARK: Edit Profile Button
    @IBAction func editProfileBtn_Tapped(_ sender: Any) {
        if profileDetailsArray.count > 0{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = EditProfileVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            obj.profileDetailsArray = profileDetailsArray
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj != 1{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 4
        }
    }
    //MARK: Delete My Account Button
    @IBAction func deleteMyAccountBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = DeleteMyAccountVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "DeleteMyAccountVC") as! DeleteMyAccountVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Profile Details Service
    func getProfileDetailsService(){
        let dic:[String:Any] = ["Userid": UserDef.getUserId(), "RequestBy":2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("profileService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
       // Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        UserModuleServices.profileService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.profileDetailsArray = [data.Data!]
                DispatchQueue.main.async {
                    self.AllData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Data
    func AllData(){
        nameTF.text = profileDetailsArray[0].User!.FullName
        nickNameTF.text = profileDetailsArray[0].User!.NickName.removeWhitespace()
        emailTF.text = profileDetailsArray[0].User!.Email
        mobileNumTF.text = "+\(profileDetailsArray[0].User!.CountryCode)\(profileDetailsArray[0].User!.Mobile)"
        genderTF.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: profileDetailsArray[0].User!.Gender, value: "", table: nil))!
        if profileDetailsArray[0].User!.DateOfBirth != ""{
            dateOfBirthTF.text = DateConverts.convertStringToStringDates(inputDateStr: profileDetailsArray[0].User!.DateOfBirth, inputDateformatType: .dateTtime, outputDateformatType: .date)
        }else{
            dateOfBirthTF.text = ""
        }
        
        let profession = profileDetailsArray[0].StandardProfessions!.filter({$0.Id == profileDetailsArray[0].User!.Profession})
        if profession.count > 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                professionTF.text = profession[0].NameEn
            }else{
                professionTF.text = profession[0].NameAr
            }
        }else{
            professionTF.text = ""
        }
        
        var userInterests = [String]()
        if profileDetailsArray[0].User!.Interests!.count > 0{
            for i in 0...profileDetailsArray[0].User!.Interests!.count - 1{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    userInterests.append(profileDetailsArray[0].User!.Interests![i].NameEn)
                }else{
                    userInterests.append(profileDetailsArray[0].User!.Interests![i].NameAr)
                }
            }
            interestsTF.text = userInterests.joined(separator: ",")
        }else{
            interestsTF.text = ""
        }
        
        //Profile
        if profileDetailsArray[0].User!.ProfilePic == ""{
            profileImg.image = UIImage(named: "Profile")
        }else{
            //Image
            let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(profileDetailsArray[0].User!.ProfilePic)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            profileImg.kf.setImage(with: url)
        }
    }
}
