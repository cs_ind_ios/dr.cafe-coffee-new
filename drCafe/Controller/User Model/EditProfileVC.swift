//
//  EditProfileVC.swift
//  drCafe
//
//  Created by Devbox on 06/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase

@available(iOS 13.0, *)
class EditProfileVC: UIViewController, UIPopoverPresentationControllerDelegate, UIDocumentPickerDelegate {

    //MARK: Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var profileImgChangeBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var dateOfBirthTF: UITextField!
    @IBOutlet weak var professionTF: UITextField!
    @IBOutlet weak var interestsTF: UITextField!
    
    @IBOutlet weak var DOBCalBtn: UIButton!
    
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var emailTitleLbl: UILabel!
    @IBOutlet weak var mobileNumTitleLbl: UILabel!
    @IBOutlet weak var genderTitleLbl: UILabel!
    @IBOutlet weak var dateOfBirthTitleLbl: UILabel!
    @IBOutlet weak var professionTitleLbl: UILabel!
    @IBOutlet weak var interestsTitleLbl: UILabel!
    @IBOutlet weak var nickNameTitleLbl: UILabel!
    @IBOutlet weak var nickNameTF: UITextField!
    @IBOutlet weak var updateProfileBtn: UIButton!
    @IBOutlet weak var nameTFView: UIView!
    @IBOutlet weak var nickNameTFView: UIView!
    @IBOutlet weak var emailTFView: UIView!
    @IBOutlet weak var mobileNumTFView: UIView!
    @IBOutlet weak var genderTFView: UIView!
    @IBOutlet weak var DOBTFView: UIView!
    @IBOutlet weak var professionTFView: UIView!
    @IBOutlet weak var interestsTFView: UIView!
    
    var profileDetailsArray = [ProfileDetailsModel]()
    var dropDownArray = [String]()
    var selectProfessionId = ""
    var interests = [String]()
    
    var profileImgDetails = [AttachementArrayModel]()
    var pickerControll = UIImagePickerController()
    var selectDate:String!
    
    var profilePic = ""
    var ProfilePicBase64 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        pickerControll.delegate = self
        nameTF.delegate = self
        nickNameTF.delegate = self
        emailTF.delegate = self
        mobileNumTF.delegate = self
        
        titleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Edit Profile", value: "", table: nil))!)"
        updateProfileBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Update Profile", value: "", table: nil))!)", for: .normal)
        nameTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Name", value: "", table: nil))!)"
        nickNameTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Nick Name", value: "", table: nil))!)"
        emailTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email Name", value: "", table: nil))!)"
        mobileNumTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!)"
        genderTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gender", value: "", table: nil))!)"
        dateOfBirthTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date Of Birth", value: "", table: nil))!)"
        professionTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Profession", value: "", table: nil))!)"
        interestsTitleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Interests", value: "", table: nil))!)"
        dateOfBirthTF.placeholder = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select DOB", value: "", table: nil))!)"
        genderTF.placeholder = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Gender", value: "", table: nil))!)"
        professionTF.placeholder = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Profession", value: "", table: nil))!)"
        interestsTF.placeholder = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Interests", value: "", table: nil))!)"
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            nameTF.textAlignment = .left
            nameTF.semanticContentAttribute = .forceLeftToRight
            emailTF.textAlignment = .left
            emailTF.semanticContentAttribute = .forceLeftToRight
            nickNameTF.textAlignment = .left
            nickNameTF.semanticContentAttribute = .forceLeftToRight
        }else{
            nameTF.textAlignment = .right
            nameTF.semanticContentAttribute = .forceRightToLeft
            emailTF.textAlignment = .right
            emailTF.semanticContentAttribute = .forceRightToLeft
            nickNameTF.textAlignment = .right
            nickNameTF.semanticContentAttribute = .forceRightToLeft
        }
    }
    //MARK: ViewWill Appear
    override func viewWillAppear(_ animated: Bool) {
        if profileDetailsArray.count > 0{
            selectProfessionId = "\(profileDetailsArray[0].User!.Profession)"
            if profileDetailsArray[0].User!.DateOfBirth != ""{
                selectDate = profileDetailsArray[0].User!.DateOfBirth
                dateOfBirthTF.text = DateConverts.convertStringToStringDates(inputDateStr: profileDetailsArray[0].User!.DateOfBirth, inputDateformatType: .dateTtime, outputDateformatType: .date)
            }else{
                selectDate = ""
                dateOfBirthTF.text = ""
            }
            
            nameTF.text = profileDetailsArray[0].User!.FullName
            nickNameTF.text = profileDetailsArray[0].User!.NickName
            emailTF.text = profileDetailsArray[0].User!.Email
            mobileNumTF.text = "+\(profileDetailsArray[0].User!.CountryCode)\(profileDetailsArray[0].User!.Mobile)"
            genderTF.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: profileDetailsArray[0].User!.Gender, value: "", table: nil))!
            
            
            let profession = profileDetailsArray[0].StandardProfessions!.filter({$0.Id == profileDetailsArray[0].User!.Profession})
            if profession.count > 0{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    professionTF.text = profession[0].NameEn
                }else{
                    professionTF.text = profession[0].NameAr
                }
            }else{
                professionTF.text = ""
            }
            
            var userInterests = [String]()
            if profileDetailsArray[0].User!.Interests!.count > 0{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    for i in 0...profileDetailsArray[0].User!.Interests!.count - 1{
                        userInterests.append(profileDetailsArray[0].User!.Interests![i].NameEn)
                    }
                }else{
                    for i in 0...profileDetailsArray[0].User!.Interests!.count - 1{
                        userInterests.append(profileDetailsArray[0].User!.Interests![i].NameAr)
                    }
                }
                
                interestsTF.text = userInterests.joined(separator: ",")
                interests = userInterests
            }else{
                interestsTF.text = ""
            }
            
            //Profile
            if profileDetailsArray[0].User!.ProfilePic == ""{
                profileImg.image = UIImage(named: "Profile")
            }else{
                //Image
                let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(profileDetailsArray[0].User!.ProfilePic)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                profileImg.kf.setImage(with: url)
                profilePic = profileDetailsArray[0].User!.ProfilePic
                ProfilePicBase64 = convertUrlToBase64(url: url!)
            }
            
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                // User Interface is Dark
                //self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
                }else{
                    self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                }
                updateProfileBtn.backgroundColor = .white
                updateProfileBtn.setTitleColor(.black, for: .normal)
                titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                nameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                nameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                nickNameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                nickNameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                emailTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                emailTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                mobileNumTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                mobileNumTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                genderTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                genderTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                dateOfBirthTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                dateOfBirthTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                professionTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                professionTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                interestsTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                interestsTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                nameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                nickNameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                emailTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                mobileNumTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                genderTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                DOBTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                professionTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                interestsTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                DOBCalBtn.setImage(UIImage(named: "calendar White"), for: .normal)
            }else{
                updateProfileBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                DOBCalBtn.setImage(UIImage(named: "calendarDate"), for: .normal)
            }
        }
        selectProfessionId = "\(profileDetailsArray[0].User!.Profession)"
        if profileDetailsArray[0].User!.DateOfBirth != ""{
            selectDate = profileDetailsArray[0].User!.DateOfBirth
            dateOfBirthTF.text = DateConverts.convertStringToStringDates(inputDateStr: profileDetailsArray[0].User!.DateOfBirth, inputDateformatType: .dateTtime, outputDateformatType: .date)
        }else{
            selectDate = ""
            dateOfBirthTF.text = ""
        }
        
        nameTF.text = profileDetailsArray[0].User!.FullName
        nickNameTF.text = profileDetailsArray[0].User!.NickName
        emailTF.text = profileDetailsArray[0].User!.Email
        mobileNumTF.text = "+\(profileDetailsArray[0].User!.CountryCode)\(profileDetailsArray[0].User!.Mobile)"
        genderTF.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: profileDetailsArray[0].User!.Gender, value: "", table: nil))!
        
        
        let profession = profileDetailsArray[0].StandardProfessions!.filter({$0.Id == profileDetailsArray[0].User!.Profession})
        if profession.count > 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                professionTF.text = profession[0].NameEn
            }else{
                professionTF.text = profession[0].NameAr
            }
        }else{
            professionTF.text = ""
        }
        
        var userInterests = [String]()
        if profileDetailsArray[0].User!.Interests!.count > 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                for i in 0...profileDetailsArray[0].User!.Interests!.count - 1{
                    userInterests.append(profileDetailsArray[0].User!.Interests![i].NameEn)
                }
            }else{
                for i in 0...profileDetailsArray[0].User!.Interests!.count - 1{
                    userInterests.append(profileDetailsArray[0].User!.Interests![i].NameAr)
                }
            }
            
            interestsTF.text = userInterests.joined(separator: ",")
            interests = userInterests
        }else{
            interestsTF.text = ""
        }
        
        //Profile
        if profileDetailsArray[0].User!.ProfilePic == ""{
            profileImg.image = UIImage(named: "Profile")
        }else{
            //Image
            let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(profileDetailsArray[0].User!.ProfilePic)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            profileImg.kf.setImage(with: url)
            profilePic = profileDetailsArray[0].User!.ProfilePic
            ProfilePicBase64 = convertUrlToBase64(url: url!)
        }
        
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            // User Interface is Dark
            //self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            updateProfileBtn.backgroundColor = .white
            updateProfileBtn.setTitleColor(.black, for: .normal)
            titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nickNameTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nickNameTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            emailTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            emailTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mobileNumTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mobileNumTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            genderTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            genderTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            dateOfBirthTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            dateOfBirthTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            professionTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            professionTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            interestsTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            interestsTF.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            nickNameTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            emailTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            mobileNumTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            genderTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            DOBTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            professionTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            interestsTFView.borderColor1 = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            DOBCalBtn.setImage(UIImage(named: "calendar White"), for: .normal)
        }else{
            updateProfileBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            DOBCalBtn.setImage(UIImage(named: "calendarDate"), for: .normal)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Update Profile
    @IBAction func updateProfileBtn_Tapped(_ sender: Any) {
        var interestIds = [String]()
        if interests.count > 0{
            for j in 0...interests.count-1{
                let interest:[StandardInterestsModel]!
                if AppDelegate.getDelegate().appLanguage == "English"{
                    interest = profileDetailsArray[0].StandardInterests!.filter({$0.NameEn == interests[j]})
                }else{
                    interest = profileDetailsArray[0].StandardInterests!.filter({$0.NameAr == interests[j]})
                }
                interestIds.append("\(interest[0].Id)")
            }
        }else{
            interestIds = [""]
        }
        if interestIds.count == 0{
            interestIds = [""]
        }
        
        if profileImgDetails.count > 0{
            profilePic = "\(profileImgDetails[0].FileName!).\(profileImgDetails[0].FileType!)"
            ProfilePicBase64 = "\(profileImgDetails[0].Base64!)"
        }
        var gender = ""
        if genderTF.text! == "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Male", value: "", table: nil))!)"{
            gender = "Male"
        }else if genderTF.text! == "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Female", value: "", table: nil))!)"{
            gender = "Female"
        }else{
            gender = ""
        }
        
        let dic:[String:Any] = ["UserId":UserDef.getUserId(), "FullName":"\(nameTF.text!)", "NickName":"\(nickNameTF.text!)", "CountryCode":"\(profileDetailsArray[0].User!.CountryCode)", "ProfilePic": profilePic, "ProfilePicBase64": ProfilePicBase64, "Gender":"\(gender)", "Language":"En", "DateOfBirth":"\(selectDate!)", "Profession":selectProfessionId, "Interests":"\(interestIds.joined(separator: ","))", "RequestBy":2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("profileService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        //Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        UserModuleServices.updateProfileService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
                let dic = ["UserId":UserDef.getUserId(),
                           "FullName":"\(self.nameTF.text!)",
                           "NickName":"\(self.nickNameTF.text!)",
                            "Email":"\((userDic["Email"]! as! String))",
                            "Gender":gender,
                            "Mobile":"\((userDic["Mobile"]! as! String))",
                            "isActive":userDic["isActive"]! as! Bool,
                            "ModifiedOn":"\((userDic["ModifiedOn"]! as! String))"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"userDic")
                UserDefaults.standard.synchronize()
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }))
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Profile Image Change
    @IBAction func profileImgChangeBtn_Tapped(_ sender: Any) {
        pickerControll.allowsEditing = true
        pickerControll.sourceType = .photoLibrary
        pickerControll.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        pickerControll.modalPresentationStyle = .overCurrentContext
        present(pickerControll, animated: true, completion: nil)
    }
    //MARK: Image Set
    func setImage(){
        if self.profileImgDetails[0].FileType == "jpeg" || self.profileImgDetails[0].FileType == "png"{
            profileImg.image = self.convertBase64ToImage(strBase64: self.profileImgDetails[0].Base64)
        }else{
            profileImg.image = UIImage(named: "Profile")
        }
    }
    //MARK: DOB Button
    @IBAction func DOBCalBtn_Tapped(_ sender: UIButton) {
        let obj : CalenderVC! = (self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC)
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 300, height: 300)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 25 , y: -5, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 25 , y: -5, width: 0, height: sender.frame.size.height)
        }
        obj.obj_where = 1
        obj.calenderVCDelegate = self
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Gender Selection
    @IBAction func genderDropDownBtn_Tapped(_ sender: UIButton) {
        dropDownArray = ["\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Male", value: "", table: nil))!)","\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Female", value: "", table: nil))!)"]
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 20 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Profession Selection
    @IBAction func professionDropDownBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        for i in 0...profileDetailsArray[0].StandardProfessions!.count - 1{
            if AppDelegate.getDelegate().appLanguage == "English"{
                dropDownArray.append(profileDetailsArray[0].StandardProfessions![i].NameEn)
            }else{
                dropDownArray.append(profileDetailsArray[0].StandardProfessions![i].NameAr)
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 20 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 3
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Interests Selection
    @IBAction func interestsDropDownBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        for i in 0...profileDetailsArray[0].StandardProfessions!.count - 1{
            if AppDelegate.getDelegate().appLanguage == "English"{
                dropDownArray.append(profileDetailsArray[0].StandardInterests![i].NameEn)
            }else{
                dropDownArray.append(profileDetailsArray[0].StandardInterests![i].NameAr)
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 20 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 4
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
// MARK: Drop Down Delegate Methods
@available(iOS 13.0, *)
extension EditProfileVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if whereType == 2{
            genderTF.text = name
        }else if whereType == 3{
            let profession:[StandardInterestsModel]!
            if AppDelegate.getDelegate().appLanguage == "English"{
                profession = profileDetailsArray[0].StandardProfessions!.filter({$0.NameEn == name})
            }else{
                profession = profileDetailsArray[0].StandardProfessions!.filter({$0.NameAr == name})
            }
            selectProfessionId = "\(profession[0].Id)"
            professionTF.text = name
        }else{
            interests.append(name)
            interestsTF.text = interests.joined(separator: ",")
        }
    }
}
// MARK: Calender Delegate Methods
@available(iOS 13.0, *)
extension EditProfileVC:CalenderVCDelegate{
    func didTapAction(getDate:Date,whereType:Int){
       // print(DateConverts.convertDateToString(date: getDate, dateformatType: .dateDots))
        selectDate = "\(getDate)"
        dateOfBirthTF.text = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .date))"
    }
}
//MARK: Select Image Delegates
@available(iOS 13.0, *)
extension EditProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var proflePic = UIImage()
        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            proflePic = img
            let name = "Profile"
            let base64 = "\(self.convertImageToBase64(image: proflePic))"
                    
            profileImgDetails.removeAll()
            profileImgDetails.append(AttachementArrayModel(FileName: name, FileType: "jpeg", Base64: base64))
            
            setImage()
        }else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            proflePic =  img
            let name = "Profile"
            let base64 = "\(self.convertImageToBase64(image: proflePic))"
                        
            profileImgDetails.removeAll()
            profileImgDetails.append(AttachementArrayModel(FileName: name, FileType: "jpeg", Base64: base64))
            setImage()
        }
        dismiss(animated:true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    // Convert Image to String
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.jpegData(compressionQuality: 0.3)
        let base64String =  imageData?.base64EncodedString()
        return base64String!
    }
    // Convert Image to String
    func convertUrlToBase64(urlStr: String) -> String {
         // print(urlStr)
         let escapedString = urlStr.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
         let url = URL(string:escapedString)!
         do {
             let imageData = try Data(contentsOf: url as URL)
             let base64String =  imageData.base64EncodedString()
             return base64String
         } catch {
             print("Unable to load data: \(error)")
              return ""
         }
     }
     func convertUrlToBase64(url: URL) -> String {
        do {
            let imageData = try Data(contentsOf: url as URL)
            let base64String =  imageData.base64EncodedString()
            return base64String
        } catch {
            print("Unable to load data: \(error)")
                 return ""
        }
    }
    func convertBase64ToImage(strBase64: String) -> UIImage {
        var strBase = strBase64.replacingOccurrences(of: "\n", with: "")
            strBase = strBase.replacingOccurrences(of: "'\'", with: "")
        if let dataDecoded:NSData = NSData(base64Encoded: strBase, options: NSData.Base64DecodingOptions(rawValue: 0)) {
            let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
                return decodedimage
        }else{
            return UIImage()
        }
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS" //Specify your format that you want
        return dateFormatter.string(from: date)
    }
}
//MARK: TextField Delegate Method
@available(iOS 13.0, *)
extension EditProfileVC : UITextFieldDelegate{
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
