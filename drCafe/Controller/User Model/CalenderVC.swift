//
//  CalenderVC.swift
//  drCafe
//
//  Created by Devbox on 06/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import FSCalendar

protocol CalenderVCDelegate {
    func didTapAction(getDate:Date,whereType:Int)
}

class CalenderVC: UIViewController {

    @IBOutlet weak var calenderView: FSCalendar!
    
    var obj_where:Int!
    var calenderVCDelegate:CalenderVCDelegate!
    var minimumDate:Date!
    var maxDays:Int!

    fileprivate lazy var dateFormatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "EN")
        formatter.dateFormat = "EEEE"
        return formatter
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        calenderView.delegate = self
        calenderView.dataSource = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            calenderView.locale = Locale(identifier: "en")
        }else{
            calenderView.locale = Locale(identifier: "ar")
        }
        if obj_where == 3{
            calenderView.today = nil
        }
    }
}
extension CalenderVC:FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance{
    func minimumDate(for calendar: FSCalendar) -> Date {
        if obj_where == 1{
            let date = Calendar.current.date(byAdding: .year, value: -120, to: Date())
            return date!
        }else{
            return minimumDate!
        }
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        if obj_where == 1{
            let date = Calendar.current.date(byAdding: .day, value: 0, to: Date())
            return date!
        }else{
            let date = Calendar.current.date(byAdding: .day, value: maxDays!, to: Date())
            return date!
        }
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        if obj_where == 1 || obj_where == 3 {
            return true
        }else{
            let key = self.dateFormatter3.string(from: date)
            if key.lowercased() == "saturday" {
                return false
            }else{
                return true
            }
        }
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        calenderVCDelegate.didTapAction(getDate: date, whereType: obj_where)
        dismiss(animated: false, completion: nil)
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        return nil
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return UIColor.white
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        return nil
    }
}
