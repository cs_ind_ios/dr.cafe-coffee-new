//
//  BetaBookingTypeCVCell.swift
//  drCafe
//
//  Created by mac3 on 17/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaBookingTypeCVCell: UICollectionViewCell {

    @IBOutlet weak var bookingTypeNameLbl: UILabel!
    @IBOutlet weak var bookingTypeImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
