//
//  BetaArenaTVCell.swift
//  drCafe
//
//  Created by mac3 on 25/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaArenaTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var hoursBookingLbl: UILabel!
    @IBOutlet weak var basePriceNameLbl: UILabel!
    @IBOutlet weak var basePriceLbl: UILabel!
    @IBOutlet weak var numOfGuestsNameLbl: UILabel!
    @IBOutlet weak var numOfGuestsLbl: UILabel!
    @IBOutlet weak var pricePerPersonNameLbl: UILabel!
    @IBOutlet weak var pricePerPersonLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var moreDetailsBtn: UIButton!
    @IBOutlet weak var totalNameLbl: UILabel!
    @IBOutlet weak var currencyNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
