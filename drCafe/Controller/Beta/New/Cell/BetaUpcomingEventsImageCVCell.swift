//
//  BetaUpcomingEventsImageCVCell.swift
//  drCafe
//
//  Created by mac3 on 17/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaUpcomingEventsImageCVCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
