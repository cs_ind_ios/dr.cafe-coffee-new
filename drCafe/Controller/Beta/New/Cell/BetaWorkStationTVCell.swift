//
//  BetaWorkStationTVCell.swift
//  drCafe
//
//  Created by mac3 on 24/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaWorkStationTVCell: UITableViewCell {

    @IBOutlet weak var hoursBookingLbl: UILabel!
    @IBOutlet weak var numOfHoursNameLbl: UILabel!
    @IBOutlet weak var numOfHoursLbl: UILabel!
    @IBOutlet weak var priceNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var featuresNameLbl: UILabel!
    @IBOutlet weak var featuresLbl: UILabel!
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var timeNameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var timeBtn: UIButton!
    @IBOutlet weak var currencyNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var bookBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
