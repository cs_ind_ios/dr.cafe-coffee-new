//
//  BetaEventDetailsTVCell.swift
//  drCafe
//
//  Created by mac3 on 16/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaEventDetailsTVCell: UITableViewCell {

    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventDescLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var beginTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet var guestView: UIView!
    @IBOutlet weak var guestLbl: UILabel!
    @IBOutlet var netAmountView: UIView!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var topVenueLbl: UILabel!
    @IBOutlet weak var topVenueViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topVenueViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var instructionsNameLbl: UILabel!
    @IBOutlet weak var instructionsLbl: UILabel!
    @IBOutlet weak var instructionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var instructionsViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var topStatusLbl: UILabel!
    @IBOutlet weak var eventMsgLbl: UILabel!
    @IBOutlet weak var countDownTimeLbl: UILabel!
    @IBOutlet weak var getDirectionBtn: UIButton!
    @IBOutlet weak var getDirectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var getDirectionBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var cancelBtnTop: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var detailsBtnTop: NSLayoutConstraint!
    @IBOutlet weak var detailsBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var bookBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var bookBtnBtm: NSLayoutConstraint!
    
    @IBOutlet weak var btmVenueLbl: UILabel!
    @IBOutlet weak var btmVenueViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btmVenueViewBtm: NSLayoutConstraint!
    @IBOutlet weak var btmStatusLbl: UILabel!
    @IBOutlet weak var btmStatusHeight: NSLayoutConstraint!//25
    @IBOutlet weak var btmStatusBtm: NSLayoutConstraint!//15
    @IBOutlet weak var btmVenueBtn: UIButton!
    
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var beginsNameLbl: UILabel!
    @IBOutlet weak var endsNameLbl: UILabel!
    @IBOutlet weak var guestsNameLbl: UILabel!
    @IBOutlet weak var netAmountNameLbl: UILabel!
    @IBOutlet weak var topVenueNameLbl: UILabel!
    @IBOutlet weak var btmVenueNameLbl: UILabel!
    @IBOutlet weak var topVenueBtn: UIButton!
    
    @IBOutlet weak var OTPBtn: UIButton!
    @IBOutlet weak var invoiceNumLbl: UILabel!
    @IBOutlet weak var OTPBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var OTPBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var OTPBtnBtm: NSLayoutConstraint!
    
    var SwiftTimer = Timer()
    var eventDate = Date()
    var totalTime = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func updateTimer() {
        //print(self.totalTime)
        self.countDownTimeLbl.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            SwiftTimer.invalidate()
            countDownTimeLbl.text = ""
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = Int(totalSeconds) / 3600
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    func configure(with eventdate: Date, totalTime: Int) {
        SwiftTimer.invalidate()
        self.eventDate = eventdate
        self.totalTime = totalTime
        //SwiftTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateTime), userInfo: nil, repeats: true)
        self.SwiftTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    @objc func UpdateTime() {
        let userCalendar = Calendar.current
        // Set Current Date
        let date = Date()
        let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
        let currentDate = userCalendar.date(from: components)!
        let components1 = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: self.eventDate)
        let eventDate1 = userCalendar.date(from: components1)!
        
        if currentDate >= eventDate1 {
            SwiftTimer.invalidate()
            countDownTimeLbl.text = ""
        }else{
            // Change the seconds to days, hours, minutes and seconds
            let timeLeft = userCalendar.dateComponents([.hour, .minute, .second], from: currentDate, to: eventDate1)

            // Display Countdown
            countDownTimeLbl.text = "\(timeLeft.hour!)h \(timeLeft.minute!)m \(timeLeft.second!)s"
        }
    }
}
