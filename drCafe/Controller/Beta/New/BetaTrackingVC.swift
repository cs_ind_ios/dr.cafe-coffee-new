//
//  BetaTrackingVC.swift
//  drCafe
//
//  Created by mac3 on 16/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class BetaTrackingVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bookAnotherEventBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btmViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var shareBtn: UIButton!
    
    var historyDetailsArray = [BetaHistoryBookingsModel]()
    var id = 0
    var bookingType = ""
    var whereObj = 0//Hostory:1,checkout:2
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var isLocation:Bool = false
    var selectLocation:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        self.shareBtn.isHidden = true
        if whereObj == 1{
            self.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Bookings", value: "", table: nil))!
            self.btmViewHeight.constant = 0
            self.backBtn.isHidden = false
        }else{
            self.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Track", value: "", table: nil))!
            self.tableView.reloadData()
            self.btmViewHeight.constant = 70
            bookAnotherEventBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Another Event", value: "", table: nil))!, for: .normal)
            self.backBtn.isHidden = true
        }
        self.getBetaDetailsService()
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = ["UserId":UserDef.getUserId(),"Id":"\(id)","BookingType":bookingType]
        BetaModuleServices.NewBookHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    if let _ = data.Data!.MyBookings{
                        self.historyDetailsArray = data.Data!.MyBookings!
                        if data.Data!.MyBookings!.count > 0{
                            for i in 0...data.Data!.MyBookings!.count - 1{
                                self.historyDetailsArray[i].OTPOpen = false
                            }
                        }
                    }
                }
                self.tableView.reloadData()
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        ANLoader.showLoading("", disableUI: true)
        DispatchQueue.main.async() {
            if self.historyDetailsArray.count > 0 {
            if let _ = self.historyDetailsArray[0].ShareableMsg {
                let Desc = self.historyDetailsArray[0].ShareableMsg!
                let shareAll = [Desc] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Book Another Event Button Action
    @IBAction func bookAnotherEventBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = NewBetaDetailsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewBetaDetailsVC") as! NewBetaDetailsVC
        obj.whereObj = 1
        self.navigationController?.pushViewController(obj, animated: true)
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
        var storelatitude = ""
        var storeLongitude = ""
        storelatitude = self.historyDetailsArray[selectLocation].Latitude!
        storeLongitude = self.historyDetailsArray[selectLocation].Longitude!
        if isLocation == false {
            isLocation = true
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open((URL(string:
                "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(storelatitude),\(storeLongitude)&directionsmode=driving")!), options: [:], completionHandler: nil)
            
            } else {
                print("Can't use comgooglemaps://");
                UIApplication.shared.open((URL(string:
                "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storelatitude)),\(String(describing: storeLongitude))")!), options: [:], completionHandler: nil)
            
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
extension BetaTrackingVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, for: indexPath) as! BetaEventDetailsTVCell
        
        let dic = self.historyDetailsArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.eventNameLbl.text = dic.TitleEn!
            cell.eventDescLbl.text = dic.DescEn!
            if let _ = dic.AddressEn{
                cell.topVenueLbl.text = dic.AddressEn!
            }else{
                cell.topVenueLbl.text = ""
            }
            //Image
            if let _ = dic.ImageEn{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
        }else{
            cell.eventNameLbl.text = dic.TitleAr!
            cell.eventDescLbl.text = dic.DescAr!
            if let _ = dic.AddressAr{
                cell.topVenueLbl.text = dic.AddressAr!
            }else{
                cell.topVenueLbl.text = ""
            }
            //Image
            if let _ = dic.ImageAr{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
        }
        cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate!, inputDateformatType: .dateTtime, outputDateformatType: .EEEEddYear)
        cell.beginTimeLbl.text = "\(dic.StartTime!)"
        cell.endTimeLbl.text = "\(dic.EndTime!)"
        if let _ = dic.GrossTotal{
            cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(dic.GrossTotal!.withCommas())"
        }else{
            cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        }
        cell.guestLbl.text = "\(dic.GuestsReserved!)"
        
        cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        cell.beginsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Begins", value: "", table: nil))!
        cell.endsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ends", value: "", table: nil))!
        cell.netAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!
        cell.topVenueNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Venue", value: "", table: nil))!
        cell.guestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Guest", value: "", table: nil))!
        cell.instructionsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Instructions", value: "", table: nil))!
        if dic.BookingType == "Event"{//countdown
            cell.eventMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event gate closes 30 mins before", value: "", table: nil))!
            cell.getDirectionViewHeight.constant = 500
            cell.btmStatusHeight.constant = 0
            cell.btmStatusBtm.constant = 0
            cell.topVenueBtn.tag = indexPath.row
            cell.topVenueBtn.addTarget(self, action: #selector(getDirectionBtn_Tapped(_:)), for: .touchUpInside)
            cell.getDirectionBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get Direction", value: "", table: nil))!, for: .normal)
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirectionBtn_Tapped(_:)), for: .touchUpInside)
            
            //Count Down Time
            let eventDateStr = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate!, inputDateformatType: .dateTtime, outputDateformatType: .date)
            let eventDate = DateConverts.convertStringToDate(date: "\(eventDateStr) \(dic.StartTime!)", dateformatType: .dateNTimeA)
            if dic.MinutesLeft! > 0{
                let countSec = dic.MinutesLeft! * 60
                cell.configure(with: eventDate, totalTime: countSec)
            }else{
                cell.countDownTimeLbl.isHidden = true
            }
            if let _ = dic.MinutesLeft{
                if dic.MinutesLeft! != 0{
                    cell.countDownTimeLbl.isHidden = false
                }else{
                    cell.countDownTimeLbl.isHidden = true
                }
            }else{
                cell.countDownTimeLbl.isHidden = true
            }
            if dic.MinutesLeft! == 0 && dic.IsCancellable! == false || dic.MinutesLeft! < 0 && dic.IsCancellable! == false{
                cell.cancelBtnHeight.constant = 0
                cell.cancelBtnTop.constant = 0
            }else{
                cell.cancelBtnHeight.constant = 44
                cell.cancelBtnTop.constant = 15
            }
            
            //Share Button
            cell.shareBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share Ticket", value: "", table: nil))!, for: .normal)
            cell.shareBtn.tag = indexPath.row
            cell.shareBtn.addTarget(self, action: #selector(sharBtn_Tapped(_:)), for: .touchUpInside)
            cell.detailsBtnHeight.constant = 0
            cell.detailsBtnTop.constant = 0
            
            //Cancel Button
            if let _ = dic.IsCancellable{
                if dic.IsCancellable! == true{
                    cell.cancelBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, for: .normal)
                    cell.cancelBtn.tag = indexPath.row
                    cell.cancelBtn.addTarget(self, action: #selector(cancelBtn_Tapped(_:)), for: .touchUpInside)
                }else{
                    cell.cancelBtn.isHidden = true
                }
            }else{
                cell.cancelBtn.isHidden = true
            }
            
            //Instruction
            cell.instructionsViewHeight.constant = 250
            cell.instructionsViewTop.constant = 10
            if AppDelegate.getDelegate().appLanguage == "English"{
                if let _ = dic.InstructionEn{
                    if dic.InstructionEn! != ""{
                        cell.instructionsLbl.text = dic.InstructionEn!
                    }else{
                        cell.instructionsViewHeight.constant = 0
                        cell.instructionsViewTop.constant = 0
                    }
                }else{
                    cell.instructionsViewHeight.constant = 0
                    cell.instructionsViewTop.constant = 0
                }
            }else{
                if let _ = dic.InstructionAr{
                    if dic.InstructionEn! != ""{
                        cell.instructionsLbl.text = dic.InstructionAr!
                    }else{
                        cell.instructionsViewHeight.constant = 0
                        cell.instructionsViewTop.constant = 0
                    }
                }else{
                    cell.instructionsViewHeight.constant = 0
                    cell.instructionsViewTop.constant = 0
                }
            }
            cell.OTPBtnHeight.constant = 30
            cell.OTPBtnBtm.constant = 10
            cell.OTPBtn.tag = indexPath.row
            cell.OTPBtn.addTarget(self, action: #selector(OTPBtn_Tapped(_:)), for: .touchUpInside)
            if let _ = dic.OTPOpen{
                if dic.OTPOpen == true{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.OTPBtnWidth.constant = 80
                    }else{
                        cell.OTPBtnWidth.constant = 120

                    }
                    if let _ = dic.OTP{
                        cell.OTPBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!): \(dic.OTP!)", for: .normal)
                    }else{
                        cell.OTPBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!)", for: .normal)
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.OTPBtnWidth.constant = 50
                    }else{
                        cell.OTPBtnWidth.constant = 80
                    }
                    cell.OTPBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!, for: .normal)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.OTPBtnWidth.constant = 50
                }else{
                    cell.OTPBtnWidth.constant = 80
                }
                cell.OTPBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!, for: .normal)
            }
            if dic.OrderStatusId == 2{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed Booking", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.262745098, green: 0.7764705882, blue: 0.1764705882, alpha: 1)
            }else if dic.OrderStatusId == 10{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }else if dic.OrderStatusId == 4{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ticket Cancel", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
                cell.OTPBtnWidth.constant = 0
                cell.cancelBtnHeight.constant = 0
                cell.cancelBtnTop.constant = 0
                cell.getDirectionBtnHeight.constant = 0
            }else{
                cell.topStatusLbl.text = ""
            }
        }else{
            cell.OTPBtnWidth.constant = 0
            cell.getDirectionViewHeight.constant = 0
            cell.btmStatusHeight.constant = 25
            cell.btmStatusBtm.constant = 15
            if dic.RequestStatus == "Review Pending"{
                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
            }else if dic.RequestStatus == "Accept"{
                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.07058823529, green: 0.4745098039, blue: 0.3333333333, alpha: 1)
            }else{
                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }
            cell.btmStatusLbl.text = dic.RequestStatus!
            cell.btmStatusLbl.textAlignment = .center
        }
        if let _ = dic.InvoiceNo{
            cell.invoiceNumLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invoice No", value: "", table: nil))!): \(dic.InvoiceNo!)"
        }else{
            cell.invoiceNumLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invoice No", value: "", table: nil))!):"
        }
        cell.topVenueViewHeight.constant = 150
        cell.topVenueViewTop.constant = 15
        cell.bookBtnHeight.constant = 0
        cell.bookBtnBtm.constant = 0
        cell.btmVenueViewHeight.constant = 0
        cell.btmVenueViewBtm.constant = 0

        cell.selectionStyle = .none
        return cell
    }
    @objc func cancelBtn_Tapped(_ sender: UIButton){
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are you sure you want to cancel Booking?", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            return
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            let dic:[String:Any] = ["Id":"\(self.id)","BookingType":self.bookingType]
            BetaModuleServices.BookingEventCancelService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }))
    }
    @objc func getDirectionBtn_Tapped(_ sender: UIButton){
        selectLocation = sender.tag
        isLocation = false
        ANLoader.showLoading("", disableUI: true)
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
    @objc func sharBtn_Tapped(_ sender: UIButton){
        ANLoader.showLoading("", disableUI: true)
        DispatchQueue.main.async() {
            if self.historyDetailsArray.count > 0 {
            if let _ = self.historyDetailsArray[0].ShareableMsg {
                let Desc = self.historyDetailsArray[0].ShareableMsg!
                let shareAll = [Desc] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
    }
    @objc func OTPBtn_Tapped(_ sender: UIButton){
        if self.historyDetailsArray[sender.tag].OTPOpen! == true{
            self.historyDetailsArray[sender.tag].OTPOpen = false
        }else{
            self.historyDetailsArray[sender.tag].OTPOpen = true
        }
        tableView.reloadData()
    }
}
