//
//  BookFacilitiesVC.swift
//  drCafe
//
//  Created by mac3 on 25/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BookFacilitiesVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var facilityTypeImgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var facilityTypeImg: UIImageView!
    
    @IBOutlet weak var packageLevelLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    @IBOutlet weak var descOneLbl: UILabel!
    @IBOutlet weak var descOneLblHeight: NSLayoutConstraint!
    @IBOutlet weak var descOneLblBtm: NSLayoutConstraint!
    @IBOutlet weak var descOneBtmLineImg: UIImageView!
    
    @IBOutlet weak var basePriceNameLbl: UILabel!
    @IBOutlet weak var basePriceCurrencyLbl: UILabel!
    @IBOutlet weak var basePriceLbl: UILabel!
    @IBOutlet weak var topBasePriceView: UIView!
    @IBOutlet weak var durationNameLbl: UILabel!
    @IBOutlet weak var durationHoursLbl: UILabel!
    @IBOutlet weak var durationHoursNameLbl: UILabel!
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var numOfGuestNameLbl: UILabel!
    @IBOutlet weak var numOfGuestLbl: UILabel!
    @IBOutlet weak var pricePerPersonNameLbl: UILabel!
    @IBOutlet weak var pricePerPersonLbl: UILabel!
    
    @IBOutlet weak var totalNameLbl: UILabel!
    @IBOutlet weak var totalCurrencyLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var descTwoLbl: UILabel!
    @IBOutlet weak var descTwoView: UIView!
    @IBOutlet weak var btmBasePriceNameLbl: UILabel!
    @IBOutlet weak var btmBasePriceCurrencyLbl: UILabel!
    @IBOutlet weak var btmBasePriceLbl: UILabel!
    @IBOutlet weak var btmBasePriceView: UIView!
    @IBOutlet weak var featuresNameLbl: UILabel!
    @IBOutlet weak var featuresLbl: UILabel!
    @IBOutlet weak var addMoreGuestsNameLbl: UILabel!
    @IBOutlet weak var addMoreGuestsTF: UITextField!
    @IBOutlet weak var addMoreGuestsBtn: UIButton!
    @IBOutlet weak var addMoreGuestsPriceNameLbl: UILabel!
    @IBOutlet weak var addMoreGuestsPriceLbl: UILabel!
    @IBOutlet weak var addAdditionalHoursNameLbl: UILabel!
    @IBOutlet weak var addAdditionalHoursTF: UITextField!
    @IBOutlet weak var addAdditionalHoursBtn: UIButton!
    @IBOutlet weak var addAdditionalPriceNameLbl: UILabel!
    @IBOutlet weak var addAdditionalPriceLbl: UILabel!
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var dateCollectionView: UICollectionView!
    @IBOutlet weak var timeNameLbl: UILabel!
    @IBOutlet weak var timeBtn: UIButton!
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var timeCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var currencyNameLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var bookBtn: UIButton!
    
    let timesArray = ["10:00 AM", "11:00 AM", "12:00 PM", "01:00 PM", "02:00 PM", "03:00 PM", "04:00 PM", "05:00 PM", "06:00 PM"]
    var datesArray = [ExpectDates]()
    var timeSelectIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateCollectionView.delegate = self
        dateCollectionView.dataSource = self
        timeCollectionView.delegate = self
        timeCollectionView.dataSource = self
        
        datesArray.removeAll()
        datesArray.append(ExpectDates(id: 0, date: DateConverts.convertStringToStringDates(inputDateStr: "\(Date())", inputDateformatType: .dateTimeZ, outputDateformatType: .date), isSelect: false))
        for i in 1...30{
            let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: DateConverts.convertStringToDate(date: datesArray[i-1].date, dateformatType: .date))
            datesArray.append(ExpectDates(id: i, date: DateConverts.convertStringToStringDates(inputDateStr: "\(nextDay!)", inputDateformatType: .dateTimeZ, outputDateformatType: .date), isSelect: false))
        }
        DispatchQueue.main.async {
            self.dateCollectionView.reloadData()
            self.timeCollectionView.reloadData()
        }
        
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Arena", value: "", table: nil))!
        basePriceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Base Price", value: "", table: nil))!
        btmBasePriceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Base Price", value: "", table: nil))!
        numOfGuestNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No of Guest", value: "", table: nil))!
        pricePerPersonNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price per person", value: "", table: nil))!
        totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!
        durationNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Duration", value: "", table: nil))!
        featuresNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Features", value: "", table: nil))!
        addMoreGuestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add more Guest", value: "", table: nil))!
        addMoreGuestsPriceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!
        addAdditionalHoursNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add additional hours", value: "", table: nil))!
        addAdditionalPriceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!
        dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        timeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Time", value: "", table: nil))!
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            dateCollectionView.semanticContentAttribute = .forceLeftToRight
            timeCollectionView.semanticContentAttribute = .forceLeftToRight
            addMoreGuestsTF.textAlignment = .left
            addAdditionalHoursTF.textAlignment = .left
        }else{
            dateCollectionView.semanticContentAttribute = .forceRightToLeft
            timeCollectionView.semanticContentAttribute = .forceRightToLeft
            addMoreGuestsTF.textAlignment = .right
            addAdditionalHoursTF.textAlignment = .right
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addMoreGuestsBtn(_ sender: Any) {
    }
    @IBAction func addAdditionalHoursBtn(_ sender: Any) {
    }
    @IBAction func dateBtn_Tapped(_ sender: Any) {
    }
    @IBAction func timeBtn_Tapped(_ sender: Any) {
    }
    @IBAction func bookBtn_Tapped(_ sender: Any) {
    }
}
extension BookFacilitiesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == dateCollectionView{
            return datesArray.count
        }else{
            return timesArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dateCollectionView{
            collectionView.register(UINib(nibName: "dayWithDateTVCell", bundle: nil), forCellWithReuseIdentifier: "dayWithDateTVCell")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayWithDateTVCell", for: indexPath) as! dayWithDateTVCell

            cell.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: datesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .dd))"
            cell.dayNameLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: datesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .E))"
            
            if datesArray[indexPath.row].isSelect == true{
                cell.dateLbl.backgroundColor = #colorLiteral(red: 0, green: 0.4196078431, blue: 0.6862745098, alpha: 1)
                cell.dateLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.dateLbl.layer.cornerRadius = 10
                cell.dateLbl.clipsToBounds = true
            }else{
                cell.dateLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.dateLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            return cell
        }else{
            collectionView.register(UINib(nibName: "BetaTimeCVCell", bundle: nil), forCellWithReuseIdentifier: "BetaTimeCVCell")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BetaTimeCVCell", for: indexPath) as! BetaTimeCVCell

            cell.nameLbl.text = "\(timesArray[indexPath.row])"
            if timeSelectIndex == indexPath.row{
                cell.BGView.backgroundColor = #colorLiteral(red: 0, green: 0.4196078431, blue: 0.6862745098, alpha: 1)
                cell.nameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.layer.cornerRadius = 10
                cell.BGView.clipsToBounds = true
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
                cell.nameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.BGView.layer.cornerRadius = 10
                cell.BGView.clipsToBounds = true
            }
            
            self.timeCollectionViewHeight.constant = self.timeCollectionView.contentSize.height
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dateCollectionView{
            print(datesArray[indexPath.row].date)
            for i in 0...datesArray.count-1{
                self.datesArray[i].isSelect = false
            }
            self.datesArray[indexPath.row].isSelect = true
            self.dateCollectionView.reloadData()
        }else{
            print(timesArray[indexPath.row])
            timeSelectIndex = indexPath.row
            self.timeCollectionView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == dateCollectionView{
            return CGSize(width: 40, height: self.dateCollectionView.frame.height)
        }else{
            let width = self.timeCollectionView.frame.width
            return CGSize(width: 90, height: 50)
        }
    }
}
