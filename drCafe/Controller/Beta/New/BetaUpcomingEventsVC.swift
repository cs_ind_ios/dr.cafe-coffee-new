//
//  BetaUpcomingEventsVC.swift
//  drCafe
//
//  Created by mac3 on 16/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class BetaUpcomingEventsVC: UIViewController, UIPopoverPresentationControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var eventsListTableView: UITableView!
    @IBOutlet weak var noRecordsFoundLbl: UILabel!

    var upcomingEventsArray = [BetaEventsModel]()
    var filterDetailsArray: [BetaEventFilterModel] = []
    var dropDownArray = [String]()
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var selectIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        eventsListTableView.delegate = self
        eventsListTableView.dataSource = self
        if SaveAddressClass.upcomingEventDetailsArray.count > 0{
            if let _ = SaveAddressClass.upcomingEventDetailsArray[0].Filters{
                filterDetailsArray = SaveAddressClass.upcomingEventDetailsArray[0].Filters!
            }
            if let _ = SaveAddressClass.upcomingEventDetailsArray[0].Events{
                upcomingEventsArray = SaveAddressClass.upcomingEventDetailsArray[0].Events!
            }
            self.eventsListTableView.reloadData()
        }
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upcoming Events", value: "", table: nil))!
        noRecordsFoundLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
        if self.upcomingEventsArray.count > 0{
            self.noRecordsFoundLbl.isHidden = true
            self.eventsListTableView.isHidden = false
        }else{
            self.noRecordsFoundLbl.isHidden = false
            self.eventsListTableView.isHidden = true
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Filter Button Action
    @IBAction func filterBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        for dic in filterDetailsArray{
            if AppDelegate.getDelegate().appLanguage == "English"{
                dropDownArray.append(dic.NameEn)
            }else{
                dropDownArray.append(dic.NameAr)
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 180, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView?.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
        pVC?.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        obj.isBeta = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    func getBetaDetailsService(filterId:String){
        let dic:[String:Any] = ["UserId":UserDef.getUserId(),"FilterId":filterId]
        BetaModuleServices.BetaPublicEventsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    self.upcomingEventsArray = data.Data!.Events!
                    self.eventsListTableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        ANLoader.hide()
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //ANLoader.hide()
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        let storeLocation = self.upcomingEventsArray[selectIndex].GeoCoordinates.components(separatedBy: ",")
        if storeLocation.count == 2{
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open((URL(string:
                                                "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(storeLocation[0]),\(storeLocation[1])&directionsmode=driving")!), options: [:], completionHandler: nil)
                
            } else {
                print("Can't use comgooglemaps://");
                UIApplication.shared.open((URL(string:
                    "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storeLocation[0])),\(String(describing: storeLocation[1]))")!), options: [:], completionHandler: nil)
                
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        ANLoader.hide()
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
extension BetaUpcomingEventsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.upcomingEventsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        eventsListTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!)
        let cell = eventsListTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, for: indexPath) as! BetaEventDetailsTVCell
        let dic = self.upcomingEventsArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.eventNameLbl.text = dic.TitleEn
            cell.eventDescLbl.text = dic.DescEn
            cell.topVenueLbl.text = dic.StoreAddressEn
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.eventImage.kf.setImage(with: url)
        }else{
            cell.eventNameLbl.text = dic.TitleAr
            cell.eventDescLbl.text = dic.DescAr
            cell.topVenueLbl.text = dic.StoreAddressAr
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.eventImage.kf.setImage(with: url)
        }
        cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEEddYear)
        cell.beginTimeLbl.text = "\(dic.StartTime)"
        cell.endTimeLbl.text = "\(dic.EndTime)"
        cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(dic.PricePerSeat.withCommas())"
        
        cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        cell.beginsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Begins", value: "", table: nil))!
        cell.endsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ends", value: "", table: nil))!
        cell.netAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!
        cell.topVenueNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Venue", value: "", table: nil))!
        
        cell.guestView.isHidden = true
        cell.topVenueViewHeight.constant = 150
        cell.topVenueViewTop.constant = 10
        
        cell.bookBtnHeight.constant = 44
        cell.bookBtnBtm.constant = 15
        cell.bookBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book", value: "", table: nil))!, for: .normal)
        cell.bookBtn.tag = indexPath.row
        cell.bookBtn.addTarget(self, action: #selector(bookBtn_Tapped(_:)), for: .touchUpInside)
        cell.topVenueBtn.tag = indexPath.row
        cell.topVenueBtn.addTarget(self, action: #selector(venueBtn_Tapped(_:)), for: .touchUpInside)
        
        cell.getDirectionViewHeight.constant = 0
        cell.btmVenueViewHeight.constant = 0
        cell.btmVenueViewBtm.constant = 0
        cell.btmStatusHeight.constant = 0
        cell.btmStatusBtm.constant = 0
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaEventBookNowVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaEventBookNowVC") as! BetaEventBookNowVC
        obj.upcomingEventArray = [upcomingEventsArray[indexPath.row]]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func bookBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaEventBookNowVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaEventBookNowVC") as! BetaEventBookNowVC
        obj.upcomingEventArray = [upcomingEventsArray[sender.tag]]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func venueBtn_Tapped(_ sender: UIButton){
        selectIndex = sender.tag
        ANLoader.showLoading("", disableUI: true)
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension BetaUpcomingEventsVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        var eventsArray = [BetaEventFilterModel]()
        if AppDelegate.getDelegate().appLanguage == "English"{
            eventsArray = SaveAddressClass.upcomingEventDetailsArray[0].Filters!.filter({$0.NameEn == name})
        }else{
            eventsArray = SaveAddressClass.upcomingEventDetailsArray[0].Filters!.filter({$0.NameAr == name})
        }
        if eventsArray.count > 0{
            self.getBetaDetailsService(filterId: "\(eventsArray[0].Id)")
        }
    }
}
