//
//  BetaEventCheckoutVC.swift
//  drCafe
//
//  Created by mac3 on 16/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMaps
import GooglePlaces

class BetaEventCheckoutVC: UIViewController, SFSafariViewControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var eventDetailstableView: UITableView!
    @IBOutlet weak var eventDetailstableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var paymentMethodBtn: UIButton!
    @IBOutlet weak var paymentMethodNameLbl: UILabel!
    @IBOutlet weak var paymentDetailstableView: UITableView!
    @IBOutlet weak var paymentDetailstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentDetailsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var billAmountBtn: UIButton!
    @IBOutlet weak var billAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var netAmountNameLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var billAmountViewHeight: NSLayoutConstraint!
    @IBOutlet weak var payViewHeight: NSLayoutConstraint!

    @IBOutlet weak var btmTotalAmountLbl: UILabel!
    @IBOutlet weak var btmCurrencyLbl: UILabel!
    @IBOutlet weak var payBtn: UIButton!
    
    var paymentDetailsArray = [PayementMethodsModel]()
    var bookingDetailsArray = [BetaBookingDataModel]()
    var paymentSelectIndex:Int!
    var paymentSelectId = 0
    var totalSecond = Int()
    var timer:Timer?
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var selectIndex = 0
    
    var isOnlinePaymeny:Bool = false
    var isBackApplePay:Bool = false
    var isAppleDevice:Bool = false
    
    //Payment
    var isApplePay:Bool = false
    var isSTCPay:Bool = false
    var isMadaPay:Bool = false
    var isVisa:Bool = false
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.drcafecoffee.payments"
    let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    // MARK: - The payment brands for Ready-to-use UI "APPLEPAY"
    static let checkoutPaymentBrands = ["VISA", "MASTER", "AMEX"]
    static let STCPAYPaymentBrands = ["STC_PAY"]
    static let MADAPAYPaymentBrands = ["MADA"]
    
    var netAmount:Double = 0.0
    var InvoiceNoStr:String = ""
    var stcPayNumber:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.netAmount = bookingDetailsArray[0].NetTotal!
        self.InvoiceNoStr = bookingDetailsArray[0].InvoiceNo!
        payBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pay", value: "", table: nil))!, for: .normal)
        // Apple Play Support
        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
        request.currencyCode = "SAR"
        let amount = NSDecimalNumber(value: 10)
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
            if let _ = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                // Apple Pay Support
                isAppleDevice = true
            }else{
                // Apple Pay not support
                isAppleDevice = false
            }
        }else{
            // Apple Pay not support
            isAppleDevice = false
        }
        if self.isAppleDevice == false {
            if let row =   self.paymentDetailsArray.firstIndex(where: {$0.Id == 5}) {
                self.paymentDetailsArray.remove(at: row)
            }
        }
        if self.paymentDetailsArray.count > 0{
            for i in 0...self.paymentDetailsArray.count-1{
                self.paymentDetailsArray[i].isSelect = false
            }
        }
        if self.paymentDetailsArray.count == 1{
            self.paymentDetailsArray[0].isSelect = true
        }
        
        paymentDetailstableView.delegate = self
        paymentDetailstableView.dataSource = self
        eventDetailstableView.delegate = self
        eventDetailstableView.dataSource = self
        self.allData()
    }
    //MARK: All Details
    func allData(){
        let dic = bookingDetailsArray[0]
        self.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Check Out", value: "", table: nil))!
        self.paymentMethodNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Methods", value: "", table: nil))!
        self.billAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bill Amount", value: "", table: nil))!
        self.totalAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!
        self.vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(dic.Vatpercentage!)%) :"
        self.netAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!
        self.btmCurrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
        
        self.totalAmountLbl.text = "\(dic.ItemTotal!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
        self.vatLbl.text = "\(dic.Vat!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
        self.netAmountLbl.text = "\(dic.NetTotal!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
        self.btmTotalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!) \(dic.NetTotal!.withCommas())"
        self.totalSecond = dic.MinutesLeft! * 60
        startTimer()
    }
    @objc func countdown() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        if totalSecond <= 0 {
            timer?.invalidate()
            payViewHeight.constant = 0
            if self.isOnlinePaymeny == false {
                self.navigationController?.popViewController(animated: true)
            }
        }
        totalSecond = totalSecond - 1
        hours = totalSecond / 3600
        minutes = (totalSecond % 3600) / 60
        seconds = (totalSecond % 3600) % 60
        timeLbl.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        if timeLbl.text == "00:00:00"{
            timer?.invalidate()
            payViewHeight.constant = 0
        }
    }
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Payment Method Button Button Action
    @IBAction func paymentMethodBtn_Tapped(_ sender: UIButton) {
        if paymentDetailsViewHeight.constant == 49{
            paymentDetailstableView.isHidden = false
            paymentDetailsViewHeight.constant = CGFloat(60 * paymentDetailsArray.count + 49)
            paymentDetailstableViewHeight.constant = CGFloat(60 * paymentDetailsArray.count)
            if AppDelegate.getDelegate().appLanguage == "English"{
                paymentMethodBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                paymentMethodBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
            paymentDetailstableView.reloadData()
        }else{
            paymentDetailsViewHeight.constant = 49
            paymentDetailstableViewHeight.constant = 0
            paymentDetailstableView.isHidden = true
            paymentMethodBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Bill Amount Button Action
    @IBAction func billAmountBtn_Tapped(_ sender: UIButton) {
        if billAmountViewHeight.constant == 45{
            billAmountViewHeight.constant = 173
            if AppDelegate.getDelegate().appLanguage == "English"{
                billAmountBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                billAmountBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            billAmountViewHeight.constant = 45
            billAmountBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Pay Button Button Action
    @IBAction func payBtn_Tapped(_ sender: UIButton) {
        if paymentSelectId == 0 {
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Payment Method", value: "", table: nil))!)
        }else{
            DispatchQueue.main.async {
                self.getCheckoutId(amount: self.netAmount)
            }
        }
    }
    //MARK: Payment SDK
    func getCheckoutId(amount:Double) {
        let userDic = getUserDetails()
        self.isOnlinePaymeny = true
        var customerPhone = userDic.Mobile
        if self.isSTCPay == true{
            customerPhone = stcPayNumber
        }
        let dic:[String:Any] = [
            "amount":String(format: "%.2f", amount),
            "shopperResultUrl": "\(self.urlScheme)://result" ,
            "isCardRegistration": "false",
            "merchantTransactionId": self.InvoiceNoStr,
            "customerEmail":userDic.Email,
            "userId":userDic.UserId,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "isMada":isMadaPay,
            "isVisaMaster":isVisa,
            "customerPhone":customerPhone,
            "customerName":userDic.FullName,
            "IsSubscription":false,
            "PaymentMethodId":paymentSelectId
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
            }else{
                DispatchQueue.main.async {
                   // print(data.Data![0].id)
                    self.checkoutID = data.Data![0].id
                    if self.isApplePay == true {
                        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
                        request.currencyCode = "SAR"
                        let amount = NSDecimalNumber(value: amount)
                        //print("ApplePay:\(amount)")
                        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
                        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
                            if let vc = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                            }else{
                                self.isOnlinePaymeny = false
                                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Unable to present Apple Pay authorization.", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                                    NSLog("Apple Pay not supported.");
                                    
                                }))
                            }
                        }else{
                            
                        }
                    }else{
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                        self.checkoutProvider?.delegate = self
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: nil)
                    }
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    func TrackOrderScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaTrackingVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTrackingVC") as! BetaTrackingVC
        obj.whereObj = 2
        obj.id = bookingDetailsArray[0].Id!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //ANLoader.hide()
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        let storeLocation = self.bookingDetailsArray[selectIndex].GeoCoordinates!.components(separatedBy: ",")
        if storeLocation.count == 2{
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open((URL(string:
                                                "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(storeLocation[0]),\(storeLocation[1])&directionsmode=driving")!), options: [:], completionHandler: nil)
                
            } else {
                print("Can't use comgooglemaps://");
                UIApplication.shared.open((URL(string:
                    "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storeLocation[0])),\(String(describing: storeLocation[1]))")!), options: [:], completionHandler: nil)
                
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        ANLoader.hide()
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension BetaEventCheckoutVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == eventDetailstableView {
            return bookingDetailsArray.count
        }else{
            if paymentDetailsArray.count > 0{
                let paymentArray = paymentDetailsArray.filter({$0.IsActive! == true})
                return paymentArray.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == eventDetailstableView {
            return UITableView.automaticDimension
        }else{
            return 60
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == eventDetailstableView {
            eventDetailstableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!)
            let cell = eventDetailstableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, for: indexPath) as! BetaEventDetailsTVCell
            let dic = self.bookingDetailsArray[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.eventNameLbl.text = dic.TitleEn!
                cell.eventDescLbl.text = dic.DescEn!
                cell.btmVenueLbl.text = dic.StoreAddressEn!
                if let _ = dic.ImageEn{
                    //Image
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.eventImage.kf.setImage(with: url)
                }
            }else{
                cell.eventNameLbl.text = dic.TitleAr!
                cell.eventDescLbl.text = dic.DescAr!
                cell.btmVenueLbl.text = dic.StoreAddressAr!
                if let _ = dic.ImageAr{
                    //Image
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.eventImage.kf.setImage(with: url)
                }
            }
            cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate!, inputDateformatType: .dateTtime, outputDateformatType: .EEEEddYear)
            cell.beginTimeLbl.text = "\(dic.StartTime!)"
            cell.endTimeLbl.text = "\(dic.EndTime!)"
            cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(dic.NetTotal!.withCommas())"
            cell.guestLbl.text = "\(dic.Guests!)"
            
            cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
            cell.beginsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Begins", value: "", table: nil))!
            cell.endsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ends", value: "", table: nil))!
            cell.netAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!
            cell.btmVenueNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Venue", value: "", table: nil))!
            cell.guestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Guest", value: "", table: nil))!
            cell.btmVenueViewHeight.constant = 150
            cell.btmVenueViewBtm.constant = 15
            
            cell.bookBtnHeight.constant = 0
            cell.bookBtnBtm.constant = 0
            cell.topVenueViewHeight.constant = 0
            cell.topVenueViewTop.constant = 0
            cell.getDirectionViewHeight.constant = 0
            cell.btmStatusHeight.constant = 0
            cell.btmStatusBtm.constant = 0
            cell.btmVenueBtn.tag = indexPath.row
            cell.btmVenueBtn.addTarget(self, action: #selector(venueBtn_Tapped(_:)), for: .touchUpInside)
            
            DispatchQueue.main.async {
                self.eventDetailstableView.layoutIfNeeded()
                self.eventDetailstableViewHeight.constant = tableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
            
            if paymentDetailsArray[indexPath.row].IsActive! == true{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameEn!
                }else{
                    cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameAr!
                }
                
                if paymentDetailsArray[indexPath.row].isSelect! == true{
                    cell.selectImg.image = UIImage(named: "Box Selected")
                }else{
                    cell.selectImg.image = UIImage(named: "Box Un Select")
                }
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(paymentDetailsArray[indexPath.row].Icon!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            
            self.paymentDetailstableView.layoutIfNeeded()
            self.paymentDetailstableViewHeight.constant = tableView.contentSize.height
            self.paymentDetailsViewHeight.constant = tableView.contentSize.height + 49
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        paymentSelectIndex = indexPath.row
        for i in 0...paymentDetailsArray.count-1{
            paymentDetailsArray[i].isSelect! = false
        }
        paymentDetailsArray[indexPath.row].isSelect! = true
        paymentSelectId = paymentDetailsArray[indexPath.row].Id!
        if paymentDetailsArray[indexPath.row].Id! == 2{
            paymentSelectId = 2
            isApplePay = false
            isSTCPay = false
            isMadaPay = false
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 3{
            paymentSelectId = 3
            isApplePay = false
            isSTCPay = false
            isMadaPay = false
            isVisa = true
        }else if paymentDetailsArray[indexPath.row].Id! == 4{
            paymentSelectId = 4
            isApplePay = false
            isSTCPay = false
            isMadaPay = true
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 5{
            paymentSelectId = 5
            isApplePay = true
            isSTCPay = false
            isMadaPay = false
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 6{
            paymentSelectId = 6
            isApplePay = false
            isSTCPay = true
            isMadaPay = false
            isVisa = false
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "STCPayVC") as! STCPayVC
//            obj.modalPresentationStyle = .overCurrentContext
//            obj.modalTransitionStyle = .crossDissolve
//            obj.stcPayVCDelegate =  self
//            self.present(obj, animated: false, completion: nil)
        }
        self.paymentDetailstableView.reloadData()
    }
    @objc func venueBtn_Tapped(_ sender: UIButton){
        selectIndex = sender.tag
        ANLoader.showLoading("", disableUI: true)
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
}
@available(iOS 13.0, *)
extension BetaEventCheckoutVC:PKPaymentAuthorizationViewControllerDelegate{
   func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
       controller.dismiss(animated: true, completion: nil)
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
           ANLoader.hide()
       }
       self.isOnlinePaymeny = false
        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            controller.dismiss(animated: true, completion: nil)
        }))
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    if let params = try? OPPApplePayPaymentParams(checkoutID: self.checkoutID, tokenData: payment.token.paymentData) as OPPApplePayPaymentParams? {
        self.transaction = OPPTransaction(paymentParams: params)
        self.provider.submitTransaction(OPPTransaction(paymentParams: params), completionHandler: { (transaction, error) in
        if (error != nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            controller.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                
                }))
            }
        }else{
            controller.dismiss(animated: true, completion: nil)
            self.provider.requestCheckoutInfo(withCheckoutID: self.checkoutID) { (checkoutInfo, error) in
            DispatchQueue.main.async {
            guard let resourcePath = checkoutInfo?.resourcePath else {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)..", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
                return
            }
            ANLoader.showLoading("", disableUI: true)
            self.transaction = nil
            let dic:[String:Any] = [
                "amount":String(format: "%.2f", self.netAmount),
                "resourcePath": resourcePath,
                "userId":"\(UserDef.getUserId())",
                "isApplePay":self.isApplePay,
                "isSTCPay":self.isSTCPay,
                "isMada":self.isMadaPay,
                "InvoiceNo":self.InvoiceNoStr,
                "isVisaMaster":self.isVisa,
                "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))",
                "ExpectedDate":"",
                "KitchenStartTime":"",
                "OrderStatus":"New",
                "PaymentMethodId":self.paymentSelectId
            ]
            PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                    self.isOnlinePaymeny = false
                    self.TrackOrderScreen()
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                    self.isOnlinePaymeny = false
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }
            }){(error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
            }
            }
        }
        }
        })
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
}
@available(iOS 13.0, *)
extension BetaEventCheckoutVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    // MARK: - Payment helpers
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        self.transaction = transaction
        if transaction.type == .synchronous {
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        } else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = OPPCheckoutSettings.init()
        if self.isSTCPay == true{
            checkoutSettings.paymentBrands = ConfirmationVC.STCPAYPaymentBrands
        }else if self.isMadaPay == true {
            checkoutSettings.paymentBrands = ConfirmationVC.MADAPAYPaymentBrands
        }else{
            checkoutSettings.paymentBrands = ConfirmationVC.checkoutPaymentBrands
        }
        checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
        checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
        checkoutSettings.theme.confirmationButtonColor = self.mainColor
        checkoutSettings.theme.accentColor = self.mainColor
        checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
        checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resource path is invalid", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        ANLoader.showLoading("", disableUI: true)
        self.transaction = nil
        let dic:[String:Any] = [
            "amount":String(format: "%.2f", self.netAmount),
            "resourcePath": resourcePath,
                                "userId":"\(UserDef.getUserId())",
                                "isApplePay":self.isApplePay,
                                "isSTCPay":self.isSTCPay,
                                "isMada":self.isMadaPay,
                                "isVisaMaster" : self.isVisa,
                                "InvoiceNo":self.InvoiceNoStr,
                                "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))",
                                "ExpectedDate":"",
                                "KitchenStartTime":"",
                                "OrderStatus":"New",
                                "PaymentMethodId":self.paymentSelectId
        ]
        
        PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    self.isOnlinePaymeny = false
                    self.TrackOrderScreen()
                }else{
                    self.isOnlinePaymeny = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
    // MARK: - Async payment callback
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
}
@available(iOS 13.0, *)
extension BetaEventCheckoutVC: STCPayVCDelegate{
    func didTapAction(mobileNumber: String) {
        stcPayNumber = mobileNumber
     }
}
