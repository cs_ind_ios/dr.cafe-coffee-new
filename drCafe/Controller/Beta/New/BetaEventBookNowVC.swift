//
//  BetaEventBookNowVC.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaEventBookNowVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var eventsListTableView: UITableView!
    @IBOutlet weak var eventsListTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var numOfGuestsTF: UITextField!
    @IBOutlet weak var numberOfGuestsDropBtn: UIButton!
    @IBOutlet weak var bookBtn: UIButton!
    
    @IBOutlet weak var selfBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var maxbookigLbl: UILabel!
    @IBOutlet weak var selfNameLbl: UILabel!
    
    @IBOutlet weak var totalAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var btmTotalAmountLbl: UILabel!
    @IBOutlet weak var btmCurrencyLbl: UILabel!

    var upcomingEventArray = [BetaEventsModel]()
    var bookingFor = 1
    var paymentDetailsArray = [PayementMethodsModel]()
    var bookingDetailsArray = [BetaBookingDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        eventsListTableView.delegate = self
        eventsListTableView.dataSource = self
        mobileTF.delegate = self
        if upcomingEventArray[0].MaxSeatsPerbook > 1 {
            maxbookigLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "NUMBER OF GUEST(Maximum", value: "", table: nil))!) \(upcomingEventArray[0].MaxSeatsPerbook) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Guests)", value: "", table: nil))!)"
        }else{
            maxbookigLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "NUMBER OF GUEST(Maximum", value: "", table: nil))!) \(upcomingEventArray[0].MaxSeatsPerbook) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Guest)", value: "", table: nil))!)"
        }
        totalAmountLbl.text = "(\(upcomingEventArray[0].PricePerSeat.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) / \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "per guest", value: "", table: nil))!))"
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        
        bookBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book", value: "", table: nil))!, for: .normal)
        selfNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Self", value: "", table: nil))!
        totalAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!
        btmCurrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
    }
    override func viewWillAppear(_ animated: Bool) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        emailTF.isEnabled = false
        mobileTF.isEnabled = false
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Self Button Action
    @IBAction func selfBtn_Tapped(_ sender: Any) {
        bookingFor = 1
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        emailTF.isEnabled = false
        mobileTF.isEnabled = false
        numOfGuestsTF.text = ""
    }
    //MARK: Other Button Action
    @IBAction func otherBtn_Tapped(_ sender: Any) {
        bookingFor = 2
        selfBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        nameTF.text = ""
        emailTF.text = ""
        mobileTF.text = ""
        nameTF.isEnabled = true
        emailTF.isEnabled = true
        mobileTF.isEnabled = true
        numOfGuestsTF.text = ""
    }
    @IBAction func numberOfGuestsDropBtn_Tapped(_ sender: UIButton) {
        var dropDownArray:[String]!
        if upcomingEventArray[0].MaxSeatsPerbook > 1{
            dropDownArray = ["1"]
            for i in 2...upcomingEventArray[0].MaxSeatsPerbook{
                dropDownArray.append("\(i)")
            }
        }else{
            dropDownArray = ["1"]
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 19 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 3
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    @IBAction func bookBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaEventRulesPopUpVC") as! BetaEventRulesPopUpVC
        obj.betaEventRulesPopUpVCDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.rules = upcomingEventArray[0].InstructionEn
        }else{
            obj.rules = upcomingEventArray[0].InstructionAr
        }
        present(obj, animated: false, completion: nil)
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if bookingFor == 2{
            if nameTF.text == nil || (nameTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EnterFullName", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isCharCount(name: nameTF.text!){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                    return false
                }
            }
            if mobileTF.text == nil || (mobileTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileTF.text!, minDigits: 9, maxDigits: 9){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                    return false
                }
            }
        }
        if numOfGuestsTF.text == nil || (numOfGuestsTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Number Of Guests", value: "", table: nil))!)
            return false
        }
        return true
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
extension BetaEventBookNowVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.upcomingEventArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        eventsListTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!)
        let cell = eventsListTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, for: indexPath) as! BetaUpcomingEventsTVCell
        let dic = self.upcomingEventArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.eventNameLbl.text = dic.TitleEn
            cell.titleLbl.text = dic.DescEn
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.eventImage.kf.setImage(with: url)
        }else{
            cell.eventNameLbl.text = dic.TitleAr
            cell.titleLbl.text = dic.DescAr
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.eventImage.kf.setImage(with: url)
        }
        //Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Icon)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.img.kf.setImage(with: url)
        cell.img.layer.cornerRadius = cell.img.frame.width / 2
        cell.img.clipsToBounds = true
        
        cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
        cell.timeLbl.text = "\(dic.StartTime) to \(dic.EndTime)"
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.storeDetailsLbl.text = "\(dic.StoreNameEn)\n\(dic.StoreAddressEn)"
        }else{
            cell.storeDetailsLbl.text = "\(dic.StoreNameAr)\n\(dic.StoreAddressAr)"
        }
        cell.storeDetailsViewHeight.constant = 300
        cell.storeAddressBtn.tag = indexPath.row
        cell.storeAddressBtn.addTarget(self, action: #selector(storeAddressBtn_Tapped(_:)), for: .touchUpInside)
        
        cell.subTitleLbl.isHidden = true
        cell.bookNowBtnHeight.constant = 0
        cell.bookNowBtnBtm.constant = 0
        cell.bookBtnHeight.constant = 0
        cell.bookBtnBtm.constant = 0
        cell.bookNowBtn.isHidden = true
        cell.bookBtnStackView.isHidden = true
        DispatchQueue.main.async {
            self.myScrollView.layoutIfNeeded()
            self.eventsListTableView.layoutIfNeeded()
            self.eventsListTableViewHeight.constant = self.eventsListTableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
    @objc func storeAddressBtn_Tapped(_ sender: UIButton){
        
    }
}
// MARK: Drop Down Delegate Methods
@available(iOS 13.0, *)
extension BetaEventBookNowVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        numOfGuestsTF.text = name
        let total = upcomingEventArray[0].PricePerSeat * Double(name)!
        btmTotalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!) \(total.withCommas())"
    }
}
@available(iOS 13.0, *)
extension BetaEventBookNowVC : UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileTF {
            if(self.mobileTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: 9)
            
        }else if textField == self.nameTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return  updatedText.safelyLimitedTo(length: 50)
        }
        return true
    }
}
extension BetaEventBookNowVC: BetaEventRulesPopUpVCDelegate{
    func didTapAction(isAgree: Bool) {
        if isAgree == true{
            let price = upcomingEventArray[0].PricePerSeat * Double(numOfGuestsTF.text!)!
            var mobileNum = mobileTF.text!
            if mobileTF.text!.count > 9{
                mobileNum = mobileTF.text!.removeCountryCode()
            }
            //let deviceInfoStr = "\(getDeviceInfo())"
            let dic = ["UserId":UserDef.getUserId(), "EventId":upcomingEventArray[0].Id, "ContactPerson":"\(nameTF.text!)", "ContactPhone":"\(mobileNum)", "Guests":Int(numOfGuestsTF.text!)!, "deviceinfo":"", "price":price.withCommas(), "Bookingfor":bookingFor] as [String : Any]
            BetaModuleServices.EventBookService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.bookingDetailsArray = data.Data!
                    // Payment Array
                    self.paymentDetailsArray = data.PaymentMethods!
                    if self.bookingDetailsArray.count > 0{
                        if self.bookingDetailsArray[0].GrossTotal != 0{
                            if self.bookingDetailsArray[0].InvoiceNo != ""{
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaEventCheckoutVC") as! BetaEventCheckoutVC
                                obj.bookingDetailsArray = self.bookingDetailsArray
                                obj.paymentDetailsArray = self.paymentDetailsArray
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                        }else{
                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                            var obj = BetaTrackingVC()
                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTrackingVC") as! BetaTrackingVC
                            obj.whereObj = 2
                            obj.id = self.bookingDetailsArray[0].Id!
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
}
