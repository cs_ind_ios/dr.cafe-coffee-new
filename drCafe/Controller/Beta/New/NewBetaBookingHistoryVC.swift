//
//  NewBetaBookingHistoryVC.swift
//  drCafe
//
//  Created by mac3 on 16/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NewBetaBookingHistoryVC: UIViewController, UIPopoverPresentationControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRecordsFoundLbl: UILabel!
    
    var historyDetailsArray = [BetaHistoryBookingsModel]()
    var filterDetailsArray = [BetaHistoryFilterModel]()
    var dropDownArray = [String]()
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var selectIndex = 0
    
    var totalSecond = Int()
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        self.getBetaDetailsService(bookingType: "", FilterId: "")
        
        noRecordsFoundLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking History", value: "", table: nil))!
    }
    func getBetaDetailsService(bookingType:String, FilterId:String){
        let dic:[String:Any] = ["UserId":UserDef.getUserId(),"Id":"","BookingType":bookingType,"FilterId":FilterId]
        BetaModuleServices.NewBookHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    if let _ = data.Data!.MyBookings{
                        self.historyDetailsArray = data.Data!.MyBookings!
                        if data.Data!.MyBookings!.count > 0{
                            for i in 0...data.Data!.MyBookings!.count - 1{
                                self.historyDetailsArray[i].OTPOpen = false
                            }
                        }
                    }
                    if let _ = data.Data!.Filters{
                        self.filterDetailsArray = data.Data!.Filters!
                    }
                    self.tableView.reloadData()
                }
                if self.historyDetailsArray.count > 0{
                    self.noRecordsFoundLbl.isHidden = true
                    self.tableView.isHidden = false
                }else{
                    self.noRecordsFoundLbl.isHidden = false
                    self.tableView.isHidden = true
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Filter Button Action
    @IBAction func filterBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        for dic in filterDetailsArray{
            if AppDelegate.getDelegate().appLanguage == "English"{
                dropDownArray.append(dic.NameEn)
            }else{
                dropDownArray.append(dic.NameAr)
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 180, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView?.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
        pVC?.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.5176470588, blue: 0.7960784314, alpha: 1)
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        obj.isBeta = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        ANLoader.hide()
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //ANLoader.hide()
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open((URL(string:
                                            "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(self.historyDetailsArray[selectIndex].Latitude!),\(self.historyDetailsArray[selectIndex].Longitude!)&directionsmode=driving")!), options: [:], completionHandler: nil)
            
        } else {
            print("Can't use comgooglemaps://");
            UIApplication.shared.open((URL(string:
                "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: historyDetailsArray[selectIndex].Latitude!)),\(String(describing: historyDetailsArray[selectIndex].Longitude!))")!), options: [:], completionHandler: nil)
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        ANLoader.hide()
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
extension NewBetaBookingHistoryVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaEventDetailsTVCell", value: "", table: nil))!, for: indexPath) as! BetaEventDetailsTVCell
        cell.SwiftTimer.invalidate()
        let dic = self.historyDetailsArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.eventNameLbl.text = dic.TitleEn!
            cell.eventDescLbl.text = dic.DescEn!
            if let _ = dic.AddressEn{
                cell.topVenueLbl.text = dic.AddressEn!
            }else{
                cell.topVenueLbl.text = ""
            }
            //Image
            if let _ = dic.ImageEn{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
        }else{
            cell.eventNameLbl.text = dic.TitleAr!
            cell.eventDescLbl.text = dic.DescAr!
            if let _ = dic.AddressAr{
                cell.topVenueLbl.text = dic.AddressAr!
            }else{
                cell.topVenueLbl.text = ""
            }
            //Image
            if let _ = dic.ImageAr{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
        }
        cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate!, inputDateformatType: .dateTtime, outputDateformatType: .EEEEddYear)
        cell.beginTimeLbl.text = "\(dic.StartTime!)"
        cell.endTimeLbl.text = "\(dic.EndTime!)"
        if let _ = dic.GrossTotal{
            cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(dic.GrossTotal!.withCommas())"
        }else{
            cell.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        }
        
        cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        cell.beginsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Begins", value: "", table: nil))!
        cell.endsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ends", value: "", table: nil))!
        cell.netAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!
        cell.topVenueNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Venue", value: "", table: nil))!
        cell.guestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Guest", value: "", table: nil))!
        if let _ = dic.InvoiceNo {
            cell.invoiceNumLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invoice No", value: "", table: nil))!): \(dic.InvoiceNo!)"
        }else{
            cell.invoiceNumLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invoice No", value: "", table: nil))!):"
        }
        
        cell.OTPBtnHeight.constant = 30
        cell.OTPBtnBtm.constant = 10
        cell.OTPBtn.tag = indexPath.row
        cell.OTPBtn.addTarget(self, action: #selector(OTPBtn_Tapped(_:)), for: .touchUpInside)
        if let _ = dic.OTPOpen{
            if dic.OTPOpen == true{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.OTPBtnWidth.constant = 80
                }else{
                    cell.OTPBtnWidth.constant = 120

                }
                if let _ = dic.OTP{
                    cell.OTPBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!): \(dic.OTP!)", for: .normal)
                }else{
                    cell.OTPBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!)", for: .normal)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.OTPBtnWidth.constant = 50
                }else{
                    cell.OTPBtnWidth.constant = 80
                }
                cell.OTPBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!, for: .normal)
            }
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.OTPBtnWidth.constant = 50
            }else{
                cell.OTPBtnWidth.constant = 80
            }
            cell.OTPBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OTP name", value: "", table: nil))!, for: .normal)
        }
        cell.guestLbl.text = "\(dic.GuestsReserved!)"
        cell.getDirectionViewHeight.constant = 0
        
        if dic.BookingType == "Event"{//countdown
            cell.eventMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event gate closes 30 mins before", value: "", table: nil))!
            cell.getDirectionViewHeight.constant = 500
            cell.btmStatusHeight.constant = 0
            cell.btmStatusBtm.constant = 0
            cell.topVenueBtn.tag = indexPath.row
            cell.topVenueBtn.addTarget(self, action: #selector(getDirectionBtn_Tapped(_:)), for: .touchUpInside)
            cell.getDirectionBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get Direction", value: "", table: nil))!, for: .normal)
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirectionBtn_Tapped(_:)), for: .touchUpInside)
            
            cell.shareBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share Ticket", value: "", table: nil))!, for: .normal)
            cell.shareBtn.tag = indexPath.row
            cell.shareBtn.addTarget(self, action: #selector(sharBtn_Tapped(_:)), for: .touchUpInside)
            
            cell.cancelBtnHeight.constant = 0
            cell.cancelBtnTop.constant = 0
            cell.detailsBtnHeight.constant = 44
            cell.detailsBtnTop.constant = 15
            cell.detailsBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Details", value: "", table: nil))!, for: .normal)
            cell.detailsBtn.tag = indexPath.row
            cell.detailsBtn.addTarget(self, action: #selector(detailsBtn_Tapped(_:)), for: .touchUpInside)
            
            //Count Down Time
            cell.countDownTimeLbl.isHidden = true
            
            if dic.OrderStatusId == 2{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed Booking", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.262745098, green: 0.7764705882, blue: 0.1764705882, alpha: 1)
            }else if dic.OrderStatusId == 10{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }else if dic.OrderStatusId == 4{
                cell.topStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ticket Cancel", value: "", table: nil))!
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
                cell.OTPBtnWidth.constant = 0
                cell.getDirectionBtnHeight.constant = 0
                cell.detailsBtnTop.constant = 0
            }else{
                cell.topStatusLbl.text = ""
            }
        }else{
            cell.getDirectionViewHeight.constant = 0
            //cell.btmStatusHeight.constant = 25
            //cell.btmStatusBtm.constant = 15
//            if dic.RequestStatus == "Review Pending"{
//                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
//            }else if dic.RequestStatus == "Accept"{
//                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.07058823529, green: 0.4745098039, blue: 0.3333333333, alpha: 1)
//            }else{
//                cell.btmStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
//            }
//            cell.btmStatusLbl.text = dic.RequestStatus!
//            cell.btmStatusLbl.textAlignment = .center
            cell.OTPBtnWidth.constant = 0
            
            cell.getDirectionViewHeight.constant = 200
            if dic.RequestStatus == "Review Pending"{
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
            }else if dic.RequestStatus == "Accept"{
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.07058823529, green: 0.4745098039, blue: 0.3333333333, alpha: 1)
            }else{
                cell.topStatusLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }
            cell.topStatusLbl.text = dic.RequestStatus!
            cell.detailsBtnHeight.constant = 44
            cell.detailsBtnTop.constant = 0
            cell.getDirectionBtnHeight.constant = 0
            cell.cancelBtnHeight.constant = 0
            cell.cancelBtnTop.constant = 0
            cell.btmStatusHeight.constant = 0
            cell.btmStatusBtm.constant = 0
        }

        cell.topVenueViewHeight.constant = 150
        cell.topVenueViewTop.constant = 10
        cell.bookBtnHeight.constant = 0
        cell.bookBtnBtm.constant = 0
        cell.btmVenueViewHeight.constant = 0
        cell.btmVenueViewBtm.constant = 0
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaTrackingVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTrackingVC") as! BetaTrackingVC
        obj.whereObj = 1
        obj.id = historyDetailsArray[indexPath.row].Id
        obj.bookingType = historyDetailsArray[indexPath.row].BookingType!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func detailsBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaTrackingVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTrackingVC") as! BetaTrackingVC
        obj.whereObj = 1
        obj.id = historyDetailsArray[sender.tag].Id
        obj.bookingType = historyDetailsArray[sender.tag].BookingType!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func getDirectionBtn_Tapped(_ sender: UIButton){
        selectIndex = sender.tag
        ANLoader.showLoading("", disableUI: true)
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
    @objc func sharBtn_Tapped(_ sender: UIButton){
        ANLoader.showLoading("", disableUI: true)
        DispatchQueue.main.async() {
            if self.historyDetailsArray.count > 0 {
                if let _ = self.historyDetailsArray[sender.tag].ShareableMsg {
                let Desc = self.historyDetailsArray[sender.tag].ShareableMsg!
                let shareAll = [Desc] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            ANLoader.hide()
        }
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension NewBetaBookingHistoryVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        var details = [BetaHistoryFilterModel]()
        if AppDelegate.getDelegate().appLanguage == "English"{
            details = filterDetailsArray.filter({$0.NameEn == name})
        }else{
            details = filterDetailsArray.filter({$0.NameAr == name})
        }
        if details.count > 0{
            self.getBetaDetailsService(bookingType: details[0].BookingType, FilterId: "\(details[0].Id)")
        }
    }
    @objc func OTPBtn_Tapped(_ sender: UIButton){
        if self.historyDetailsArray[sender.tag].OTPOpen! == true{
            self.historyDetailsArray[sender.tag].OTPOpen = false
        }else{
            self.historyDetailsArray[sender.tag].OTPOpen = true
        }
        tableView.reloadData()
    }
}
