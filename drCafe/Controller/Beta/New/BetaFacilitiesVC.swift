//
//  BetaFacilitiesVC.swift
//  drCafe
//
//  Created by mac3 on 25/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaFacilitiesVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var facilityTypeImg: UIImageView!
    @IBOutlet weak var facilityDescLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var whereObj = 1
    var detailsArray = [BetaAreasModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if detailsArray.count > 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                titleLbl.text = detailsArray[0].TitleEn
                facilityDescLbl.text = detailsArray[0].SubTitleEn
            }else{
                titleLbl.text = detailsArray[0].TitleEn
                facilityDescLbl.text = detailsArray[0].SubTitleAr
            }
            if detailsArray[0].TitleEn == "Arena"{
                whereObj = 2
            }
            self.tableView.reloadData()
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BetaFacilitiesVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if whereObj == 1{
            return 5
        }else{
            return 5
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if whereObj == 1{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaWorkStationTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaWorkStationTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaWorkStationTVCell", value: "", table: nil))!, for: indexPath) as! BetaWorkStationTVCell
            
            cell.numOfHoursNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No of Hours", value: "", table: nil))!
            cell.priceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!
            cell.featuresNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Features", value: "", table: nil))!
            cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
            cell.timeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Time", value: "", table: nil))!
            cell.currencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
            cell.bookBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book", value: "", table: nil))!, for: .normal)
            
            cell.hoursBookingLbl.text = "For 3 hours booking"
            cell.numOfHoursLbl.text = "3"
            cell.priceLbl.text = "50.00"
            cell.descLbl.text = "The Base price will be 3,000 with a minimum pax confirmation of 25 which will be included in the final package"
            cell.featuresLbl.text = "1. Unlimited servings of Food and beverage.\n2. Utilisation of the complete arena facilities.\n3. Video and photography facility included"
            cell.dateLbl.text = "00-00-0000"
            cell.timeLbl.text = "00:00"
            cell.totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!) 50.00"
            
            cell.dateBtn.tag = indexPath.row
            cell.dateBtn.addTarget(self, action: #selector(dateBtn_Tapped(_:)), for: .touchUpInside)
            cell.timeBtn.tag = indexPath.row
            cell.timeBtn.addTarget(self, action: #selector(timeBtn_Tapped(_:)), for: .touchUpInside)
            
            DispatchQueue.main.async {
                self.tableView.layoutIfNeeded()
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaArenaTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaArenaTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaArenaTVCell", value: "", table: nil))!, for: indexPath) as! BetaArenaTVCell
            
            cell.basePriceNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Base Price", value: "", table: nil))!
            cell.numOfGuestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No of Guest", value: "", table: nil))!
            cell.pricePerPersonNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price per person", value: "", table: nil))!
            cell.totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!
            cell.currencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
            cell.moreDetailsBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "More details", value: "", table: nil))!, for: .normal)
            
            cell.titleLbl.text = "Standard Package"
            cell.hoursBookingLbl.text = "Duration 5 Hours"
            cell.basePriceLbl.text = "3,000.00"
            cell.numOfGuestsLbl.text = "25"
            cell.pricePerPersonLbl.text = "100.00"
            cell.descLbl.text = "The Base price will be 3,000 with a minimum pax confirmation of 25 which will be included in the final package"
            cell.totalAmountLbl.text = "5,500.00"
            
            DispatchQueue.main.async {
                self.tableView.layoutIfNeeded()
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BookFacilitiesVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BookFacilitiesVC") as! BookFacilitiesVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func dateBtn_Tapped(_ sender: UIButton){
        
    }
    @objc func timeBtn_Tapped(_ sender: UIButton){
        
    }
}
