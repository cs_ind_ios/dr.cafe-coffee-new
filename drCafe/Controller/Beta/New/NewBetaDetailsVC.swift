//
//  NewBetaDetailsVC.swift
//  drCafe
//
//  Created by mac3 on 17/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NewBetaDetailsVC: UIViewController, CLLocationManagerDelegate,GMSMapViewDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var mapMainView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var storeInfoPopUpView: UIView!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var downArrowImg: UIImageView!
    @IBOutlet weak var mapPinImg: UIImageView!
    @IBOutlet weak var storeInfoBtn: UIButton!
    @IBOutlet weak var storeDirectionBtn: UIButton!
    
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var upcomingEventsNameLbl: UILabel!
    @IBOutlet weak var upcomingEventsCollectionView: UICollectionView!
    @IBOutlet weak var upcomingEventsPageController: UIPageControl!
    @IBOutlet weak var upcomingEventsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bookFacilitiesNameLbl: UILabel!
    @IBOutlet weak var  bookFacilitiesCollectionView: UICollectionView!
    @IBOutlet weak var  bookFacilitiesViewHeight: NSLayoutConstraint!//197
    
    var categoryDetailsArray = [["Name":"About BETA","Desc":"Business, Event, Training, Arena","Image":"About Beta"]]
    var upcomingEventsArray = [BetaEventsModel]()
    var bookingArray = [BetaEventBookingsModel]()
    var storesArray = [BetaEventStoresModel]()
    var whereObj = 0
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        upcomingEventsCollectionView.delegate = self
        upcomingEventsCollectionView.dataSource = self
        //bookFacilitiesCollectionView.delegate = self
        //bookFacilitiesCollectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        mapView.delegate = self
        self.initGoogleMaps()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.getBetaDetailsService()
        }
        
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        titleLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Welcome", value: "", table: nil))!) \((userDic["FullName"]! as! String))"
        upcomingEventsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upcoming Events", value: "", table: nil))!
        viewAllBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "View All", value: "", table: nil))!, for: .normal)
        
        if let _ = AppDelegate.getDelegate().currentLatitude{
            latitude = AppDelegate.getDelegate().currentLatitude
            longitude = AppDelegate.getDelegate().currentLongitude
        }else{
            //location Delegates
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            upcomingEventsCollectionView.semanticContentAttribute = .forceLeftToRight
            upcomingEventsPageController.semanticContentAttribute = .forceLeftToRight
            bookFacilitiesCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            upcomingEventsCollectionView.semanticContentAttribute = .forceRightToLeft
            upcomingEventsPageController.semanticContentAttribute = .forceRightToLeft
            bookFacilitiesCollectionView.semanticContentAttribute = .forceRightToLeft
        }
    }
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:24.6871594, longitude:46.6817154 , zoom: 14.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = false
        //mapView.settings.myLocationButton = true
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = false
        self.mapView.settings.myLocationButton = false
    }
    override func viewWillAppear(_ animated: Bool) {
        categoryDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking History", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discover BETA and More", value: "", table: nil))!,"Image":"ic_history_beta"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book our facilities", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event, Workspace, Meeting Room", value: "", table: nil))!,"Image":"Beta Book Now"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose your Channel", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose your favourite channel Audio", value: "", table: nil))!,"Image":"Beta Listen Audio"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upcoming Events", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event, Workspace, Meeting Room", value: "", table: nil))!,"Image":"About Beta"]]
//        DispatchQueue.main.async {
          // self.tableView.reloadData()
//        }
    }
    override func viewDidDisappear(_ animated: Bool) {
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = ["UserId":UserDef.getUserId()]
        BetaModuleServices.BetaPublicEventsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.tableView.reloadData()
            }else{
                if let _ = data.Data{
                    SaveAddressClass.upcomingEventDetailsArray = [data.Data!]
                    self.upcomingEventsArray = data.Data!.Events!
                    if data.Data!.Events!.count > 0{
                        self.upcomingEventsViewHeight.constant = 1000
                        self.upcomingEventsCollectionView.reloadData()
                        self.upcomingEventsPageController.numberOfPages = data.Data!.Events!.count
                        self.upcomingEventsPageController.currentPage = 0
                        if data.Data!.Events!.count > 1{
                            if AppDelegate.getDelegate().appLanguage != "English"{
                                let index = IndexPath(item: 0, section: 0)
                                self.upcomingEventsCollectionView.scrollToItem(at: index, at: .right, animated: false)
                            }
                        }
                    }else{
                        self.upcomingEventsViewHeight.constant = 0
                    }
                    if let _ = data.Data!.Bookings{
                        self.bookingArray = data.Data!.Bookings!
                    }
                    if let _ = data.Data!.BetaStores{
                        self.storesArray = data.Data!.BetaStores!
                        if self.storesArray.count > 0{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                self.storeNameLbl.text = self.storesArray[0].NameEn
                                self.storeAddressLbl.text = self.storesArray[0].AddressEn
                            }else{
                                self.storeNameLbl.text = self.storesArray[0].NameAr
                                self.storeAddressLbl.text = self.storesArray[0].AddressAr
                            }
                        }else{
                            self.storeInfoPopUpView.isHidden = true
                            self.mapPinImg.isHidden = true
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 0{
            navigationController?.popViewController(animated: true)
        }else{
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
   
    //MARK: View All Button Action
    @IBAction func viewAllBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaUpcomingEventsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaUpcomingEventsVC") as! BetaUpcomingEventsVC
        obj.upcomingEventsArray = upcomingEventsArray
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Store Info Button Action
    @IBAction func storeInfoBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = NewAboutBetaVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewAboutBetaVC") as! NewAboutBetaVC
        obj.storeId = self.storesArray[0].Id
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Store Directions Button Action
    @IBAction func storeDirectionBtn_Tapped(_ sender: UIButton) {
        if storesArray.count > 0{
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open((URL(string:
                                                "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(self.storesArray[0].Latitude),\(self.storesArray[0].Longitude)&directionsmode=driving")!), options: [:], completionHandler: nil)
                
            } else {
                print("Can't use comgooglemaps://");
                UIApplication.shared.open((URL(string:
                    "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storesArray[0].Latitude)),\(String(describing: storesArray[0].Longitude))")!), options: [:], completionHandler: nil)
                
            }
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
extension NewBetaDetailsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, for: indexPath) as! BetaTypeTVCell
        
        cell.nameLbl.text = categoryDetailsArray[indexPath.row]["Name"]
        cell.descLbl.text = categoryDetailsArray[indexPath.row]["Desc"]
        if indexPath.row == 0{
            cell.descLblTop.constant = 0
            cell.descLblHeight.constant = 0
        }else{
        cell.descLblTop.constant = 5
        cell.descLblHeight.constant = 21
        }
        if bookingArray.count > 0{
            if indexPath.row == 0{
                cell.orderDetailsViewHeight.constant = 500
                cell.orderDetailsView.isHidden = false
                var bookingTitle = ""
                if AppDelegate.getDelegate().appLanguage == "English"{
                    bookingTitle = bookingArray[0].TitleEn
                }else{
                    bookingTitle = bookingArray[0].TitleAr
                }
                if bookingArray[0].OrderStatusId == 2{
                    cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed", value: "", table: nil))!
                    cell.addressLbl.text = "\(bookingTitle)\n\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Request is", value: "", table: nil))!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed", value: "", table: nil))!)"
                }else if bookingArray[0].OrderStatusId == 10{
                    cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                    cell.addressLbl.text = "\(bookingTitle)\n\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Request is", value: "", table: nil))!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!)"
                }else{
                    cell.statusLbl.text =  bookingArray[0].OrderStatus
                    cell.addressLbl.text = ""
                }
                cell.countDownTimeLbl.text = ""
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    cell.addressLbl.text = "\(bookingArray[0].TitleEn)\n\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Request is", value: "", table: nil))!) \(bookingArray[0].RequestStatus)"
//                }else{
//                    cell.addressLbl.text = "\(bookingArray[0].TitleAr)\n\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Request is", value: "", table: nil))!) \(bookingArray[0].RequestStatus)"
//                }
                let eventDateStr = DateConverts.convertStringToStringDates(inputDateStr: bookingArray[0].EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                
                cell.dateLbl.text = eventDateStr
                cell.timeLbl.text = "\(bookingArray[0].StartTime) to \(bookingArray[0].EndTime)"
                let eventDate = DateConverts.convertStringToDate(date: "\(eventDateStr) \(bookingArray[0].StartTime)", dateformatType: .dateNTimeA)
                cell.configure(with: eventDate)
               //cell.countDownTimeLbl.text = self.UpdateTime(eventdate: eventDate)
                
                cell.orderStatusImage.image = UIImage(named: "beta_order_details_icon")
                cell.orderArrowBtn.tag = indexPath.row
                cell.orderArrowBtn.addTarget(self, action: #selector(orderBtn_Tapped(_:)), for: .touchUpInside)
                cell.descLbl.isHidden = true
            }else{
                cell.orderDetailsViewHeight.constant = 0
                cell.orderDetailsView.isHidden = true
                cell.descLbl.isHidden = false
            }
        }else{
            cell.orderDetailsViewHeight.constant = 0
            cell.orderDetailsView.isHidden = true
            cell.descLbl.isHidden = false
        }
        
        if indexPath.row > 2{
            cell.imgBGView.isHidden = false
            cell.typeImage.isHidden = true
            cell.imgTwo.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
        }else{
            cell.typeImage.isHidden = false
            cell.imgBGView.isHidden = true
            cell.typeImage.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
        }
        DispatchQueue.main.async {
            self.tableView.layoutIfNeeded()
            self.tableViewHeight.constant = self.tableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = NewBetaBookingHistoryVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewBetaBookingHistoryVC") as! NewBetaBookingHistoryVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 1 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BetaBookNowVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaBookNowVC") as! BetaBookNowVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 2 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:"Main_Audio", bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 3 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BetaUpcomingEventsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaUpcomingEventsVC") as! BetaUpcomingEventsVC
            obj.upcomingEventsArray = upcomingEventsArray
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    @objc func orderBtn_Tapped(_ sender: UIButton){
//        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//        var obj = BetaTrackingVC()
//        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTrackingVC") as! BetaTrackingVC
//        obj.whereObj = 1
//        obj.id = historyDetailsArray[indexPath.row].Id
//        obj.bookingType = bookingArray[0].BookingType
//        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func countdownTimer() {
        tableView.reloadData()
    }
    func UpdateTime(eventdate: Date) -> String {
        let userCalendar = Calendar.current
        // Set Current Date
        let date = Date()
        let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
        let currentDate = userCalendar.date(from: components)!
        
        let components1 = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: eventdate)
        let eventDate1 = userCalendar.date(from: components1)!
        if currentDate >= eventDate1 {
            return ""
        }else{
        // Change the seconds to days, hours, minutes and seconds
        let timeLeft = userCalendar.dateComponents([.hour, .minute, .second], from: currentDate, to: eventDate1)
        
        // Display Countdown
        return "\(timeLeft.hour!)h \(timeLeft.minute!)m \(timeLeft.second!)s"
        //timerLabel.text = "\(timeLeft.day!)d \(timeLeft.hour!)h \(timeLeft.minute!)m \(timeLeft.second!)s"
        }
    }
}
extension NewBetaDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == upcomingEventsCollectionView{
            return self.upcomingEventsArray.count
        }else{
            return 3
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == upcomingEventsCollectionView{
            upcomingEventsCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsImageCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsImageCVCell", value: "", table: nil))!)
            let cell = upcomingEventsCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsImageCVCell", value: "", table: nil))!, for: indexPath) as! BetaUpcomingEventsImageCVCell
            
            let dic = self.upcomingEventsArray[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nameLbl.text = dic.TitleEn
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }else{
                cell.nameLbl.text = dic.TitleAr
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            cell.img.layer.cornerRadius = 10
            cell.img.clipsToBounds = true
            cell.img.contentMode = .scaleAspectFill
            
            return cell
        }else{
            bookFacilitiesCollectionView.register(UINib(nibName: "BetaBookingTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "BetaBookingTypeCVCell")
            let cell = bookFacilitiesCollectionView.dequeueReusableCell(withReuseIdentifier: "BetaBookingTypeCVCell", for: indexPath) as! BetaBookingTypeCVCell
//            let betaAreasArr = storeInfoArray[0].BetaAreas!
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                cell.bookingTypeNameLbl.text = betaAreasArr[indexPath.row].TitleEn
//            }else{
//                cell.bookingTypeNameLbl.text = betaAreasArr[indexPath.row].TitleAr
//            }
//
//            //Item Image
//            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(betaAreasArr[indexPath.row].Image)" as NSString
//            let charSet = CharacterSet.urlFragmentAllowed
//            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
//            let url = URL.init(string: urlStr as String)
//            cell.bookingTypeImg.kf.setImage(with: url)
            
            if indexPath.row == 0{
                cell.bookingTypeNameLbl.text = "Arena"
            }else if indexPath.row == 1{
                cell.bookingTypeNameLbl.text = "WorkStation"
            }else{
                cell.bookingTypeNameLbl.text = "Conference"
            }
            
            //self.bookFacilitiesViewHeight.constant = 197
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == upcomingEventsCollectionView{
            let width = upcomingEventsCollectionView.frame.width
            return CGSize(width: width - 70, height: upcomingEventsCollectionView.frame.height)
        }else{
            let width = bookFacilitiesCollectionView.frame.width
            return CGSize(width: width/3, height: bookFacilitiesCollectionView.frame.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == upcomingEventsCollectionView{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BetaEventBookNowVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaEventBookNowVC") as! BetaEventBookNowVC
            obj.upcomingEventArray = [upcomingEventsArray[indexPath.row]]
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaFacilitiesVC") as! BetaFacilitiesVC
//            //obj.detailsArray = [storeInfoArray[0].BetaAreas![indexPath.row]]
//            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //Banner auto scrool
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == upcomingEventsCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.upcomingEventsCollectionView.indexPathsForVisibleItems
            indexes.sort()
            guard let _ = indexes.first else { /* Handle nil case */ return }
            var index = indexes.first!
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row
                }else{
                    index.row = index.row + 1
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
            }
            
            if upcomingEventsArray.count > index.row {
                DispatchQueue.main.async {
                    self.upcomingEventsCollectionView.scrollToItem(at: index, at: .right, animated: true )
                }
            }
            DispatchQueue.main.async {
                self.upcomingEventsPageController.currentPage = index.row
            }
        }
    }
}
