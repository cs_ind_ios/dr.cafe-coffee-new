//
//  BetaEventRulesPopUpVC.swift
//  drCafe
//
//  Created by mac3 on 17/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

//MARK: BetaEventRules PopUp Delegate Protocol
protocol BetaEventRulesPopUpVCDelegate {
    func didTapAction(isAgree:Bool)
}

class BetaEventRulesPopUpVC: UIViewController {

    @IBOutlet weak var iAgreeBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    var rules = ""
    var betaEventRulesPopUpVCDelegate:BetaEventRulesPopUpVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descLbl.text = rules
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking Instructions", value: "", table: nil))!
        iAgreeBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "I Agree", value: "", table: nil))!, for: .normal)
        cancelBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, for: .normal)

    }
    //MARK: Back Button Action
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        betaEventRulesPopUpVCDelegate.didTapAction(isAgree: false)
        self.dismiss(animated: true)
    }
    //MARK: Filter Button Action
    @IBAction func iAgreeBtn_Tapped(_ sender: UIButton) {
        betaEventRulesPopUpVCDelegate.didTapAction(isAgree: true)
        self.dismiss(animated: false)
    }
}
//extension UILabel {
//    func setHTML(html: String) {
//        do {
//            let attributedString: NSAttributedString = try NSAttributedString(data: html.data(using: .utf8)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
//            self.attributedText = attributedString
//        } catch {
//            self.text = html
//        }
//    }
//}
