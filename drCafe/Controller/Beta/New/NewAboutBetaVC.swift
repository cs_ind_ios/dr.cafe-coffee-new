//
//  NewAboutBetaVC.swift
//  drCafe
//
//  Created by mac3 on 17/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NewAboutBetaVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var imagesCountLbl: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var imageBtn: UIButton!
    
    @IBOutlet weak var bookingTypeNameLbl: UILabel!
    @IBOutlet weak var bookingTypeCollectionView: UICollectionView!
    @IBOutlet weak var bookingTypeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var orderTypeCollectionView: UICollectionView!
    @IBOutlet weak var orderTypeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var storeTypeNameLbl: UILabel!
    @IBOutlet weak var storeTypeCollectionView: UICollectionView!
    @IBOutlet weak var storeTypeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var facilityNameLbl: UILabel!
    @IBOutlet weak var facilityCollectionView: UICollectionView!
    @IBOutlet weak var facilityViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var segmentNameLbl: UILabel!
    @IBOutlet weak var segmentCollectionView: UICollectionView!
    @IBOutlet weak var segmentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var categoryDetailsArray = [["Name":"About BETA","Desc":"Business, Event, Training, Arena","Image":"About Beta"]]
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var isLocation:Bool = false
    var where_Location:Int = 0

    var storeId:Int!
    var storeInfoArray = [StoreInfoDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        self.bookingTypeCollectionView.delegate = self
        self.bookingTypeCollectionView.dataSource = self
        self.orderTypeCollectionView.delegate = self
        self.orderTypeCollectionView.dataSource = self
        self.storeTypeCollectionView.delegate = self
        self.storeTypeCollectionView.dataSource = self
        self.facilityCollectionView.delegate = self
        self.facilityCollectionView.dataSource = self
        self.segmentCollectionView.delegate = self
        self.segmentCollectionView.dataSource = self
        
        orderTypeCollectionView.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        storeTypeCollectionView.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        facilityCollectionView.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        segmentCollectionView.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        
        self.getStoreDetailsService()
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            imagesCollectionView.semanticContentAttribute = .forceLeftToRight
            orderTypeCollectionView.semanticContentAttribute = .forceLeftToRight
            storeTypeCollectionView.semanticContentAttribute = .forceLeftToRight
            facilityCollectionView.semanticContentAttribute = .forceLeftToRight
            segmentCollectionView.semanticContentAttribute = .forceLeftToRight
            bookingTypeCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            imagesCollectionView.semanticContentAttribute = .forceRightToLeft
            orderTypeCollectionView.semanticContentAttribute = .forceRightToLeft
            storeTypeCollectionView.semanticContentAttribute = .forceRightToLeft
            facilityCollectionView.semanticContentAttribute = .forceRightToLeft
            segmentCollectionView.semanticContentAttribute = .forceRightToLeft
            bookingTypeCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        self.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!
        self.storeTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Type", value: "", table: nil))!
        self.facilityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Facility", value: "", table: nil))!
        self.segmentNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Segment", value: "", table: nil))!
        self.bookingTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking Type", value: "", table: nil))!
        self.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Beta", value: "", table: nil))!
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Image Button Action
    @IBAction func imageBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.itemName = storeInfoArray[0].NameEn
        }else{
            obj.itemName = storeInfoArray[0].NameAr
        }
        for images in storeInfoArray[0].StoreImages!{
            obj.itemImagesArray.append(ItemImagesModel(Image: images.Image, IsFeatured: false))
        }
        present(obj, animated: false, completion: nil)
    }
    //MARK: Get Store Details Service
    func getStoreDetailsService(){
        let dic = ["UserId":"\(UserDef.getUserId())", "StoreId" : storeId!, "RequestBy":2] as [String : Any]
        OrderModuleServices.GetStoreInfoService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeInfoArray = [data.Data!]
                DispatchQueue.main.async {
                    if self.storeInfoArray[0].StoreImages!.count > 0{
                        //self.storeImgsCollectionViewHeight.constant = 200
                        self.imagesCollectionView.reloadData()
                        self.imagesCountLbl.text = "\(self.storeInfoArray[0].StoreImages!.count) +"
                    }else{
                        //self.storeImgsCollectionViewHeight.constant = 0
                    }
                    self.AllData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Data
    func AllData(){
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.locationLbl.text = "\(storeInfoArray[0].NameEn)\n\(storeInfoArray[0].AddressEn)"
        }else{
            self.locationLbl.text = "\(storeInfoArray[0].NameAr)\n\(storeInfoArray[0].AddressAr)"
        }
        categoryDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upcoming Events", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event, workspace, Meeting Room", value: "", table: nil))!,"Image":"About Beta"],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Call", value: "", table: nil))!,"Desc":storeInfoArray[0].MobileNo,"Image":"Beta Call"],  ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Direction", value: "", table: nil))!,"Image":"Beta Get"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invite", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Friends", value: "", table: nil))!,"Image":"About Beta 1"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Location", value: "", table: nil))!,"Image":"About Beta 1"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "About BETA", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Business, Event, Training, Arena", value: "", table: nil))!,"Image":"About Beta 1"]]
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        //self.storeTypeCollectionView.reloadData()
        //self.orderTypeCollectionView.reloadData()
        //self.facilityCollectionView.reloadData()
        //self.bookingTypeCollectionView.reloadData()

        if storeInfoArray[0].Segments!.count > 0{
            self.segmentCollectionView.reloadData()
        }else{
            self.segmentViewHeight.constant = 0
        }
        if storeInfoArray[0].Facilities!.count > 0{
            self.facilityCollectionView.reloadData()
        }else{
            self.facilityViewHeight.constant = 0
        }
        if storeInfoArray[0].OrderTypes!.count > 0{
            self.orderTypeCollectionView.reloadData()
        }else{
            self.orderTypeViewHeight.constant = 0
        }
        if storeInfoArray[0].StoreTypes!.count > 0{
            self.storeTypeCollectionView.reloadData()
        }else{
            self.storeTypeViewHeight.constant = 0
        }
        if storeInfoArray[0].BetaAreas!.count > 0{
            self.bookingTypeCollectionView.reloadData()
        }else{
            self.bookingTypeViewHeight.constant = 0
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
        if isLocation == false {
            isLocation = true
            if where_Location == 1 {
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open((URL(string:
                    "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(storeInfoArray[0].Latitude),\(storeInfoArray[0].Longitude)&directionsmode=driving")!), options: [:], completionHandler: nil)
                
            } else {
                print("Can't use comgooglemaps://");
                UIApplication.shared.open((URL(string:
                    "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storeInfoArray[0].Latitude)),\(String(describing: storeInfoArray[0].Longitude))")!), options: [:], completionHandler: nil)
                
            }
            }else if where_Location == 2 {
                DispatchQueue.main.async() {
                    var URLString = "https://www.google.com/maps/search/?api=1&query="
                    if self.storeInfoArray[0].Latitude != "" && self.storeInfoArray[0].Longitude != ""{
                        URLString = "https://www.google.com/maps/search/?api=1&query=\(self.storeInfoArray[0].Latitude),\(self.storeInfoArray[0].Longitude)"
                    }
                    let url = NSURL(string: URLString)
                    var storeName = self.storeInfoArray[0].NameEn
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        storeName = self.storeInfoArray[0].NameEn
                    }else{
                        storeName = self.storeInfoArray[0].NameAr
                    }
                    let Desc = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "I would love to have a Cup of coffee with you at dr.CAFE Coffee in", value: "", table: nil))!) \(storeName) \(url!)"
                    let shareAll = [Desc] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }else if where_Location == 3 {
                var URLString = "https://www.google.com/maps/search/?api=1&query="
                if storeInfoArray[0].Latitude != "" && storeInfoArray[0].Longitude != ""{
                    URLString = "https://www.google.com/maps/search/?api=1&query=\(self.storeInfoArray[0].Latitude),\(self.storeInfoArray[0].Longitude)"
                }
                let url = NSURL(string: URLString)
                var Desc = "\(self.storeInfoArray[0].NameEn) , \(self.storeInfoArray[0].AddressEn) \(url!)"
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Desc = "\(self.storeInfoArray[0].NameEn) , \(self.storeInfoArray[0].AddressEn) \(url!)"
                }else{
                    Desc = "\(self.storeInfoArray[0].NameAr) , \(self.storeInfoArray[0].AddressAr) \(url!)"
                }
                let shareAll = [Desc] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
        //rotation = heading.magneticHeading
    }
}
//MARK: Collectionview delegate methods
@available(iOS 13.0, *)
extension NewAboutBetaVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if storeInfoArray.count > 0{
            if collectionView == imagesCollectionView{
                return storeInfoArray[0].StoreImages!.count
            }else if collectionView == bookingTypeCollectionView{
                return storeInfoArray[0].BetaAreas!.count
            }else if collectionView == orderTypeCollectionView{
                let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
                return orderTypeArr.count
            }else if collectionView == storeTypeCollectionView{
                return storeInfoArray[0].StoreTypes!.count
            }else if collectionView == facilityCollectionView{
                return storeInfoArray[0].Facilities!.count
            }else{
                return storeInfoArray[0].Segments!.count
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == imagesCollectionView{
            imagesCollectionView.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
            let cell = imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
            cell.percentNumLbl.isHidden = true
            cell.nameLbl.isHidden = true
            cell.percentNumBGView.isHidden = true
            
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(storeInfoArray[0].StoreImages![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.cellBGImg.kf.setImage(with: url)
            
            cell.cellBGView.layer.cornerRadius = 0
            return cell
        }else if collectionView == bookingTypeCollectionView{
            bookingTypeCollectionView.register(UINib(nibName: "BetaBookingTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "BetaBookingTypeCVCell")
            let cell = bookingTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "BetaBookingTypeCVCell", for: indexPath) as! BetaBookingTypeCVCell
            let betaAreasArr = storeInfoArray[0].BetaAreas!
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.bookingTypeNameLbl.text = betaAreasArr[indexPath.row].TitleEn
            }else{
                cell.bookingTypeNameLbl.text = betaAreasArr[indexPath.row].TitleAr
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(betaAreasArr[indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.bookingTypeImg.kf.setImage(with: url)
            
            bookingTypeViewHeight.constant = 192
            return cell
        }else if collectionView == orderTypeCollectionView{
            let cell = orderTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = orderTypeArr[indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = orderTypeArr[indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1)
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(orderTypeArr[indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            orderTypeViewHeight.constant = orderTypeCollectionView.contentSize.height + 42
            return cell
        }else if collectionView == storeTypeCollectionView{
            let cell = storeTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].StoreTypes![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].StoreTypes![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1)
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(storeInfoArray[0].StoreTypes![indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            storeTypeViewHeight.constant = storeTypeCollectionView.contentSize.height + 42
            return cell
        }else if collectionView == facilityCollectionView{
            let cell = facilityCollectionView.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].Facilities![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].Facilities![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1)
            
            //Item Image
            let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(storeInfoArray[0].Facilities![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            facilityViewHeight.constant = facilityCollectionView.contentSize.height + 42
            return cell
        }else{
            let cell = segmentCollectionView.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].Segments![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].Segments![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1)
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(String(describing: storeInfoArray[0].Segments![indexPath.row].Image!))" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            segmentViewHeight.constant = segmentCollectionView.contentSize.height + 42
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == imagesCollectionView{
            let width = imagesCollectionView.bounds.width
            let height = imagesCollectionView.bounds.height
            return CGSize(width: width, height: height)
        }else if collectionView == bookingTypeCollectionView{
            let width = bookingTypeCollectionView.frame.width
            return CGSize(width: width/3, height: bookingTypeCollectionView.frame.height)
        }else{
            let width = collectionView.frame.width
            return CGSize(width: width/4 - 20, height: 110)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == imagesCollectionView{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.itemName = storeInfoArray[0].NameEn
            }else{
                obj.itemName = storeInfoArray[0].NameAr
            }
            for images in storeInfoArray[0].StoreImages!{
                obj.itemImagesArray.append(ItemImagesModel(Image: images.Image, IsFeatured: false))
            }
            present(obj, animated: false, completion: nil)
        }
//        else if collectionView == bookingTypeCollectionView{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaFacilitiesVC") as! BetaFacilitiesVC
//            obj.detailsArray = [storeInfoArray[0].BetaAreas![indexPath.row]]
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
    }
}
extension NewAboutBetaVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, for: indexPath) as! BetaTypeTVCell
        
        cell.orderDetailsViewHeight.constant = 0
        cell.nameLbl.text = categoryDetailsArray[indexPath.row]["Name"]
        cell.descLbl.text = categoryDetailsArray[indexPath.row]["Desc"]
        if indexPath.row > 2{
            cell.imgBGView.isHidden = false
            cell.typeImage.isHidden = true
            cell.imgTwo.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
        }else{
            cell.typeImage.isHidden = false
            cell.imgBGView.isHidden = true
            cell.typeImage.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
        }
        DispatchQueue.main.async {
            //self.myScrollView.layoutIfNeeded()
            self.tableView.layoutIfNeeded()
            self.tableViewHeight.constant = self.tableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BetaUpcomingEventsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaUpcomingEventsVC") as! BetaUpcomingEventsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 1 {
            if storeInfoArray.count > 0{
                let mobile = storeInfoArray[0].MobileNo
                print(mobile.removeWhitespace())
                if let url = URL(string: "tel://\(mobile.removeWhitespace())") {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }else if indexPath.row == 2 {
            if storeInfoArray.count > 0{
                if storeInfoArray[0].Latitude != "" && storeInfoArray[0].Longitude != ""{
                    where_Location = 1
                    self.isLocation = false
                    //location Delegates
                    self.locationManager = CLLocationManager()
                    self.locationManager.delegate = self
                    self.locationManager.requestWhenInUseAuthorization()
                    self.locationManager.startUpdatingLocation()
                    self.locationManager.startMonitoringSignificantLocationChanges()
                    
                }
            }
        }else if indexPath.row == 3 {
            if storeInfoArray.count > 0{
                where_Location = 2
                self.isLocation = false
                //location Delegates
                self.locationManager = CLLocationManager()
                self.locationManager.delegate = self
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
                self.locationManager.startMonitoringSignificantLocationChanges()
            }
        }else if indexPath.row == 4 {
            if storeInfoArray.count > 0{
                where_Location = 3
                self.isLocation = false
                //location Delegates
                self.locationManager = CLLocationManager()
                self.locationManager.delegate = self
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
                self.locationManager.startMonitoringSignificantLocationChanges()
               
            }
        }else if indexPath.row == 5 {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = AboutBetaVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AboutBetaVC") as! AboutBetaVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
