//
//  BetaMoreInfoVC.swift
//  drCafe
//
//  Created by Mac2 on 29/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaMoreInfoVC: UIViewController {
    
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var moreDetailsArray:[[String:String]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Info", value: "", table: nil))!
        if AppDelegate.getDelegate().BetaInfoTitle1 != "" && AppDelegate.getDelegate().BetaInfoMobile1 != ""{
            if AppDelegate.getDelegate().BetaInfoTitleAr1 != "" {
                moreDetailsArray = [["Name":AppDelegate.getDelegate().BetaInfoTitle1,"Mobile":AppDelegate.getDelegate().BetaInfoMobile1, "NameAr":AppDelegate.getDelegate().BetaInfoTitleAr1]]
            }else{
                moreDetailsArray = [["Name":AppDelegate.getDelegate().BetaInfoTitle1,"Mobile":AppDelegate.getDelegate().BetaInfoMobile1, "NameAr":AppDelegate.getDelegate().BetaInfoTitle1]]
            }
           
        }
        if AppDelegate.getDelegate().BetaInfoTitle2 != "" && AppDelegate.getDelegate().BetaInfoMobile2 != ""{
            if moreDetailsArray.count > 0{
                if AppDelegate.getDelegate().BetaInfoTitleAr2 != "" {
                    moreDetailsArray.append(["Name":AppDelegate.getDelegate().BetaInfoTitle2,"Mobile":AppDelegate.getDelegate().BetaInfoMobile2,"NameAr":AppDelegate.getDelegate().BetaInfoTitleAr2])
                }else{
                    moreDetailsArray.append(["Name":AppDelegate.getDelegate().BetaInfoTitle2,"Mobile":AppDelegate.getDelegate().BetaInfoMobile2,"NameAr":AppDelegate.getDelegate().BetaInfoTitle2])
                }
            }else{
                if AppDelegate.getDelegate().BetaInfoTitleAr2 != "" {
                    moreDetailsArray = [["Name":AppDelegate.getDelegate().BetaInfoTitle2,"Mobile":AppDelegate.getDelegate().BetaInfoMobile2,"NameAr":AppDelegate.getDelegate().BetaInfoTitleAr2]]
                }else{
                    moreDetailsArray = [["Name":AppDelegate.getDelegate().BetaInfoTitle2,"Mobile":AppDelegate.getDelegate().BetaInfoMobile2,"NameAr":AppDelegate.getDelegate().BetaInfoTitle2]]
                }
            }
        }
        tableView.reloadData()
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BetaMoreInfoVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if moreDetailsArray != nil{
            return self.moreDetailsArray.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaMoreInfoTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaMoreInfoTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaMoreInfoTVCell", value: "", table: nil))!, for: indexPath) as! BetaMoreInfoTVCell

        if AppDelegate.getDelegate().appLanguage == "English" {
            cell.nameLbl.text = moreDetailsArray[indexPath.row]["Name"]
        }else{
            cell.nameLbl.text = moreDetailsArray[indexPath.row]["NameAr"]
        }
        cell.mobileNumLbl.text = moreDetailsArray[indexPath.row]["Mobile"]

        cell.selectionStyle = .none
        return cell
    }
}
