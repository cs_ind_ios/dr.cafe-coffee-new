//
//  BetaBookingHistoryVC.swift
//  drCafe
//
//  Created by Mac2 on 29/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaBookingHistoryVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categorySegment: UISegmentedControl!
    @IBOutlet weak var noBookingsLbl: UILabel!
    
    var historyDetailsArray = [BetaHistoryDetailsModel]()
    var isBooking = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking History", value: "", table: nil))!
        categorySegment.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bookings", value: "", table: nil))!, forSegmentAt: 0)
        categorySegment.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Events", value: "", table: nil))!, forSegmentAt: 1)
        noBookingsLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No bookings found!", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            categorySegment.semanticContentAttribute = .forceLeftToRight
        }else{
            categorySegment.semanticContentAttribute = .forceRightToLeft
        }
        categorySegment.addTarget(self, action: #selector(categorySegment_Tapped(_:)), for: .valueChanged)
        getBetaDetailsService()
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func categorySegment_Tapped(_ sender: UISegmentedControl){
        if categorySegment.selectedSegmentIndex == 0{
            isBooking = true
            if let _ = historyDetailsArray[0].Enquiry{
                if historyDetailsArray[0].Enquiry!.count > 0{
                    self.noBookingsLbl.isHidden = true
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else{
                    self.noBookingsLbl.isHidden = false
                    self.tableView.isHidden = true
                }
            }else{
                self.noBookingsLbl.isHidden = false
                self.tableView.isHidden = true
            }
        }else{
            isBooking = false
            if let _ = historyDetailsArray[0].Events{
                if historyDetailsArray[0].Events!.count > 0{
                    self.noBookingsLbl.isHidden = true
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else{
                    self.noBookingsLbl.isHidden = false
                    self.tableView.isHidden = true
                }
            }else{
                self.noBookingsLbl.isHidden = false
                self.tableView.isHidden = true
            }
        }
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = [:]
        let url = "\(url.BookingHistoryService.path())\(UserDef.getUserId())"
        BetaModuleServices.BetaBookingHistoryService(url: url, dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    self.historyDetailsArray = [data.Data!]
                }
                self.tableView.reloadData()
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
extension BetaBookingHistoryVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if historyDetailsArray.count > 0{
            if isBooking == true{
                return historyDetailsArray[0].Enquiry!.count
            }else{
                return historyDetailsArray[0].Events!.count
            }
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaBookingHistoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaBookingHistoryTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaBookingHistoryTVCell", value: "", table: nil))!, for: indexPath) as! BetaBookingHistoryTVCell
        
        if isBooking == true{
            let dic = historyDetailsArray[0].Enquiry![indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.eventNameLbl.text = dic.TitleEn
            }else{
                cell.eventNameLbl.text = dic.TitleAr
            }
            cell.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date))"
            cell.timeLbl.text = "\(dic.StartTime) to\n\(dic.EndTime)"
            if dic.RequestStatus == "Review Pending"{
                cell.reviewCommentViewTop.constant = 0
                cell.reviewCommentViewHeight.constant = 0
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
            }else if dic.RequestStatus == "Accept"{
                cell.reviewCommentViewTop.constant = 10
                cell.reviewCommentViewHeight.constant = 49
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.07058823529, green: 0.4745098039, blue: 0.3333333333, alpha: 1)
            }else{
                cell.reviewCommentViewTop.constant = 10
                cell.reviewCommentViewHeight.constant = 49
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }
            cell.statusLbl.text = dic.RequestStatus
//            if dic.Reviewed == false{
//                cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pending", value: "", table: nil))!
//            }else{
//                cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed", value: "", table: nil))!
//            }
            cell.img.image = UIImage(named: "Beta History")
            cell.img.layer.cornerRadius = cell.img.frame.width/2
            cell.img.clipsToBounds =  true
        }else{
            let dic = historyDetailsArray[0].Events![indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.eventNameLbl.text = dic.EventTitleEn
            }else{
                cell.eventNameLbl.text = dic.EventTitleAr
            }
            cell.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date))"
            cell.timeLbl.text = "\(dic.StartTime) to\n\(dic.EndTime)"
            if dic.RequestStatus == "Review Pending"{
                cell.reviewCommentViewTop.constant = 0
                cell.reviewCommentViewHeight.constant = 0
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
            }else if dic.RequestStatus == "Accept"{
                cell.reviewCommentViewTop.constant = 10
                cell.reviewCommentViewHeight.constant = 49
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.07058823529, green: 0.4745098039, blue: 0.3333333333, alpha: 1)
            }else{
                cell.reviewCommentViewTop.constant = 10
                cell.reviewCommentViewHeight.constant = 49
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
            }
            cell.statusLbl.text = dic.RequestStatus
            cell.reviewCommentLbl.text = dic.ReviewComment
//            if dic.Reviewed == false{
//                cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pending", value: "", table: nil))!
//            }else{
//                cell.statusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirmed", value: "", table: nil))!
//            }
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Icon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
            cell.img.layer.cornerRadius = cell.img.frame.width/2
            cell.img.clipsToBounds =  true
        }
        
        cell.statusNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Status", value: "", table: nil))!
        cell.dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        cell.timeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Time", value: "", table: nil))!
        cell.reviewCommentNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Review Comment", value: "", table: nil))!
        
        cell.reBookBtn.tag = indexPath.row
        cell.reBookBtn.addTarget(self, action: #selector(reBookBtn_Tapped(_:)), for: .touchUpInside)

        cell.selectionStyle = .none
        return cell
    }
    @objc func reBookBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaBookNowVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaBookNowVC") as! BetaBookNowVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
