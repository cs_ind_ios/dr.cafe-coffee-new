//
//  BetaMoreInfoTVCell.swift
//  drCafe
//
//  Created by Mac2 on 29/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaMoreInfoTVCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
