//
//  BetaNameDescriptionCVCell.swift
//  drCafe
//
//  Created by Mac2 on 05/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaNameDescriptionCVCell: UICollectionViewCell {
    
   // @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var numLbl: UILabel!
    @IBOutlet weak var arrowLeading: NSLayoutConstraint!
    @IBOutlet weak var arrawImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
