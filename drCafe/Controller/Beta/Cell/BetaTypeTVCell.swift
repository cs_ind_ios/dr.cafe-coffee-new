//
//  BetaTypeTVCell.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaTypeTVCell: UITableViewCell {
    
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var descLblTop: NSLayoutConstraint!
    @IBOutlet weak var descLblHeight: NSLayoutConstraint!

    @IBOutlet weak var typeImage: UIImageView!
    
    @IBOutlet weak var imgBGView: UIView!
    @IBOutlet weak var imgTwo: UIImageView!
    
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var orderArrowBtn: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var countDownTimeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var orderStatusImage: UIImageView!
    @IBOutlet weak var orderDetailsViewHeight: NSLayoutConstraint!
    var SwiftTimer = Timer()
    var eventDate = Date()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configure(with eventdate: Date) {
        SwiftTimer.invalidate()
        self.eventDate = eventdate        
        SwiftTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateTime), userInfo: nil, repeats: true)
    }
    @objc func UpdateTime() {
        let userCalendar = Calendar.current
        // Set Current Date
        let date = Date()
        let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
        let currentDate = userCalendar.date(from: components)!
        let components1 = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: self.eventDate)
        let eventDate1 = userCalendar.date(from: components1)!
        
        if currentDate >= eventDate1 {
            SwiftTimer.invalidate()
            countDownTimeLbl.text = ""
        }else{
            // Change the seconds to days, hours, minutes and seconds
            let timeLeft = userCalendar.dateComponents([.hour, .minute, .second], from: currentDate, to: eventDate1)

            // Display Countdown
            countDownTimeLbl.text = "\(timeLeft.hour!)h \(timeLeft.minute!)m \(timeLeft.second!)s"
        }
    }
}
