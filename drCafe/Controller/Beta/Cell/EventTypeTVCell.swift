//
//  EventTypeTVCell.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class EventTypeTVCell: UITableViewCell {

    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var selectImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
