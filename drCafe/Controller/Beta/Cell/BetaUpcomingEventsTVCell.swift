//
//  BetaUpcomingEventsTVCell.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaUpcomingEventsTVCell: UITableViewCell {
    
    @IBOutlet weak var bookNowBtn: UIButton!
    @IBOutlet weak var bookNowBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var bookNowBtnBtm: NSLayoutConstraint!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var subTitleLblHeight: NSLayoutConstraint!
    @IBOutlet weak var subTitleLblBtm: NSLayoutConstraint!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateLblHeight: NSLayoutConstraint!
    @IBOutlet weak var dateLblBtm: NSLayoutConstraint!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var timeLblWidth: NSLayoutConstraint!//150
    @IBOutlet weak var timeLblLeading: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var calendarBtn: UIButton!
    @IBOutlet weak var bookBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var bookBtnBtm: NSLayoutConstraint!//15
    @IBOutlet weak var bookBtnStackView: UIStackView!

    @IBOutlet weak var storeDetailsLbl: UILabel!
    @IBOutlet weak var storeDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var storeAddressBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
