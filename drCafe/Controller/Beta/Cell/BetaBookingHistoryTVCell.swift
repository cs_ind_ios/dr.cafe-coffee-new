//
//  BetaBookingHistoryTVCell.swift
//  drCafe
//
//  Created by Mac2 on 29/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaBookingHistoryTVCell: UITableViewCell {

    @IBOutlet weak var reBookBtn: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var reviewCommentLbl: UILabel!
    @IBOutlet weak var reviewCommentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var reviewCommentViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var timeNameLbl: UILabel!
    @IBOutlet weak var statusNameLbl: UILabel!
    @IBOutlet weak var reviewCommentNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
