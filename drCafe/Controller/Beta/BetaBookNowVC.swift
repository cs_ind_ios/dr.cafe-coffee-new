//
//  BetaBookNowVC.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaBookNowVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var aboutYourEventTV: UITextView!
    @IBOutlet weak var eventDateTF: UITextField!
    @IBOutlet weak var startTimeTF: UITextField!
    @IBOutlet weak var endTimeTF: UITextField!
    @IBOutlet weak var numOfGuestsTF: UITextField!
    @IBOutlet weak var bookBtn: UIButton!
    
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    @IBOutlet weak var popUpDescLbl: UILabel!
    @IBOutlet weak var popUpImg: UIImageView!
    
    @IBOutlet weak var selfBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet var changeTimeView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var changeTimeCancelBtn: UIButton!
    @IBOutlet weak var changeTimeDoneBtn: UIButton!
    
    @IBOutlet weak var selfNameLbl: UILabel!
    @IBOutlet weak var tellUsAboutEventNameLbl: UILabel!
    @IBOutlet weak var eventDateNameLbl: UILabel!
    @IBOutlet weak var startTimeNameLbl: UILabel!
    @IBOutlet weak var endTimeNameLbl: UILabel!
    @IBOutlet weak var numOfGuestsNameLbl: UILabel!
    
    var bookingAreaDetailsArray = [BetaBookingAreasDataModel]()
    var selectIndex:Int!
    var popover = Popover()
    var bookingFor = 1
    var selectDate:String!
    var selectStartTime:String!
    var selectEndTime:String!
    var startTime = false
    var endTime = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        mobileTF.delegate = self
        nameTF.delegate = self
        aboutYourEventTV.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.getBetaDetailsService()
        }
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        
        bookBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book", value: "", table: nil))!, for: .normal)
        selfNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Self", value: "", table: nil))!
        tellUsAboutEventNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "TELL US ABOUT YOUR EVENT", value: "", table: nil))!
        eventDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EVENT DATE", value: "", table: nil))!
        startTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "START TIME", value: "", table: nil))!
        endTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "END TIME", value: "", table: nil))!
        numOfGuestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NUMBER OF GUESTS", value: "", table: nil))!
        
        let myString:NSString = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BOOK AN EVENT, TRAINING, TALK SHOW, LAUNCHING, OF PRODUCT SPORTS LIVE TELECAST, PRIVATE TOUR OR EXPERIENCE AT THE BETA ", value: "", table: nil))! as NSString
        if AppDelegate.getDelegate().appLanguage == "English"{
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir Medium", size: 16.0)!])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:4))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:5,length:84))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:88,length:25))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location:114,length:4))
            myMutableString.addAttribute(NSAttributedString.Key.backgroundColor, value: #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1), range: NSRange(location:114,length:5))
            titleLbl.attributedText = myMutableString
        }else{
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir Medium", size: 16.0)!])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:4))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:5,length:86))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:91,length:16))
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location:108,length:4))
            myMutableString.addAttribute(NSAttributedString.Key.backgroundColor, value: #colorLiteral(red: 0.1098039216, green: 0.4588235294, blue: 0.7333333333, alpha: 1), range: NSRange(location:108,length:4))
            titleLbl.attributedText = myMutableString
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        emailTF.isEnabled = false
        mobileTF.isEnabled = false
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = [:]
        BetaModuleServices.BetaBookingAreasService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.bookingAreaDetailsArray = data.Data!
                self.tableView.reloadData()
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Self Button Action
    @IBAction func selfBtn_Tapped(_ sender: Any) {
        bookingFor = 1
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        emailTF.isEnabled = false
        mobileTF.isEnabled = false
        numOfGuestsTF.text = ""
    }
    //MARK: Other Button Action
    @IBAction func otherBtn_Tapped(_ sender: Any) {
        bookingFor = 2
        selfBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        nameTF.text = ""
        emailTF.text = ""
        mobileTF.text = ""
        nameTF.isEnabled = true
        emailTF.isEnabled = true
        mobileTF.isEnabled = true
        numOfGuestsTF.text = ""
    }
    @IBAction func bookBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var mobileNum = mobileTF.text!
        if mobileTF.text!.count > 9{
            mobileNum = mobileTF.text!.removeCountryCode()
        }
        //let deviceInfoStr = "\(getDeviceInfo())"
        let dic = ["UserId":UserDef.getUserId(), "bookingAreaId":bookingAreaDetailsArray[selectIndex].Id, "EventDescription":"\(aboutYourEventTV.text!)", "EventDate":"\(selectDate!)", "StartTime":"\(selectStartTime!)", "EndTime":"\(selectEndTime!)", "Guests":Int(numOfGuestsTF.text!)!, "ContactPerson":"\(nameTF.text!)", "ContactMobile":"\(mobileNum)", "deviceinfo":"", "Bookingfor":bookingFor] as [String : Any]
        BetaModuleServices.BookEnquiryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if selectIndex == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Event Type", value: "", table: nil))!)
            return false
        }
        if bookingFor == 2{
            if nameTF.text == nil || (nameTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EnterFullName", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isCharCount(name: nameTF.text!){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                    return false
                }
            }
            if mobileTF.text == nil || (mobileTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileTF.text!, minDigits: 9, maxDigits: 9){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                    return false
                }
            }
        }
        if aboutYourEventTV.text == nil || (aboutYourEventTV.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Enter About Your Event", value: "", table: nil))!)
            return false
        }
        if selectDate == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Event Date", value: "", table: nil))!)
            return false
        }
        if selectStartTime == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Start Time", value: "", table: nil))!)
            return false
        }
        if selectEndTime == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select End Time", value: "", table: nil))!)
            return false
        }
        if numOfGuestsTF.text == nil || (numOfGuestsTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Number Of Guests", value: "", table: nil))!)
            return false
        }
        return true
    }
    //MARK: Number Of Guests
    @IBAction func numberOfGuestsDropBtn_Tapped(_ sender: UIButton) {
        if selectIndex != nil{
            var dropDownArray:[String]!
            if bookingAreaDetailsArray[selectIndex].MaxGuestCapacity > 1{
                dropDownArray = ["1"]
                for i in 2...bookingAreaDetailsArray[selectIndex].MaxGuestCapacity{
                    dropDownArray.append("\(i)")
                }
            }else{
                dropDownArray = ["1"]
            }
            let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
            let height = dropDownArray.count * 50
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .popover
            obj.preferredContentSize = CGSize(width: 200, height: height)
            let pVC = obj.popoverPresentationController
            pVC?.permittedArrowDirections = .down
            pVC?.sourceView = sender
            pVC?.delegate = self
            if AppDelegate.getDelegate().appLanguage == "English"{
                pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
            }else{
                pVC?.sourceRect = CGRect(x: sender.frame.minX + 19 , y: +15, width: 0, height: sender.frame.size.height)
            }
            obj.whereObj = 3
            obj.dropDownArray = dropDownArray
            obj.dropVCDelegate = self
            obj.isSingle = true
            self.present(obj, animated: false, completion: nil)
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Event Type", value: "", table: nil))!)
        }
    }
    @IBAction func dateBtn_Tapped(_ sender: UIButton) {
        let obj : CalenderVC! = (self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC)
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 300, height: 300)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 25 , y: 5, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 25 , y: 5, width: 0, height: sender.frame.size.height)
        }
        var dateComponents = DateComponents()
        dateComponents.day = 1
        let minimumDate = Calendar.current.date(byAdding: dateComponents, to: Date())
        obj.minimumDate = minimumDate
        obj.maxDays = 365
        obj.obj_where = 3
        obj.calenderVCDelegate = self
        self.present(obj, animated: false, completion: nil)
    }
    @IBAction func startTimeBtn_Tapped(_ sender: UIButton) {
        if selectDate == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Event Date", value: "", table: nil))!)
            return
        }
        startTime = true
        endTime = false
        let seltDate = DateConverts.convertStringToDate(date: "\(selectDate!) 12:00 AM", dateformatType: .dateTimeA)
        self.datePicker.setDate(seltDate, animated: true)
        self.datePicker.minimumDate = seltDate
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        self.datePicker.setValue(UIColor.black, forKey: "textColor")
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        self.datePicker.datePickerMode = .time
        var startPoint = CGPoint(x: sender.frame.maxX, y: 1000 - myScrollView.contentOffset.y)
        if AppDelegate.getDelegate().appLanguage == "English"{
            startPoint = CGPoint(x: sender.frame.maxX - 2, y: 1000 - myScrollView.contentOffset.y)
        }else{
            startPoint = CGPoint(x: sender.frame.maxX + 70, y: 1000 - myScrollView.contentOffset.y)
        }
        self.changeTimeView.frame = CGRect(x: 0, y: 0, width: 280, height: 280)
        popover.popoverType = .up
        popover.show(self.changeTimeView, point: startPoint)
    }
    @IBAction func endTimeBtn_Tapped(_ sender: UIButton) {
        if selectStartTime == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Start Time", value: "", table: nil))!)
            return
        }
        startTime = false
        endTime = true
        let dateSelect = DateConverts.convertStringToDate(date: selectDate, dateformatType: .dateR)
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        self.datePicker.setDate(dateSelect, animated: true)
        self.datePicker.setValue(UIColor.black, forKey: "textColor")
        var dateComponents = DateComponents()
        dateComponents.day = 1
        let maximumDays = Calendar.current.date(byAdding: dateComponents, to: dateSelect)
        self.datePicker.maximumDate = maximumDays
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let dateSelect2 = DateConverts.convertStringToDate(date: "\(selectDate!) \(selectStartTime!)", dateformatType: .dateTimeA)
        let minimumDays = Calendar.current.date(byAdding: .minute, value: 1, to: dateSelect2)
        self.datePicker.datePickerMode = .time
        self.datePicker.minimumDate = minimumDays
        var startPoint = CGPoint(x: self.view.bounds.width - 40, y: 1000 - myScrollView.contentOffset.y)
        if AppDelegate.getDelegate().appLanguage == "English"{
            startPoint = CGPoint(x: self.view.bounds.width - 40, y: 1000 - myScrollView.contentOffset.y)
        }else{
            startPoint = CGPoint(x: 40, y: 1000 - myScrollView.contentOffset.y)
        }
        self.changeTimeView.frame = CGRect(x: 0, y: 0, width: 280, height: 280)
        popover.popoverType = .up
        popover.show(self.changeTimeView, point: startPoint)
    }
    @IBAction func changeTimeCancelBtn_Tapped(_ sender: Any) {
        popover.dismiss()
    }
    @IBAction func changeTimeDoneBtn_Tapped(_ sender: Any) {
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        timeFormatter.locale = Locale(identifier: "EN")
        if startTime == true{
            self.startTimeTF.text = timeFormatter.string(from:self.datePicker.date)
            selectStartTime = timeFormatter.string(from:self.datePicker.date)
            self.endTimeTF.text = ""
            selectEndTime = ""
        }else{
            self.endTimeTF.text = timeFormatter.string(from:self.datePicker.date)
            selectEndTime = timeFormatter.string(from:self.datePicker.date)
        }
        popover.dismiss()
    }
    @IBAction func popUpDismissBtn_Tapped(_ sender: Any) {
        popover.dismiss()
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    func htmlView(htmlString:String){
        // works even without <html><body> </body></html> tags, BTW
        let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        // suppose we have an UILabel, but any element with NSAttributedString will do
        self.popUpDescLbl.attributedText = attrStr
    }
}
extension BetaBookNowVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookingAreaDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EventTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EventTypeTVCell", value: "", table: nil))!)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EventTypeTVCell", value: "", table: nil))!, for: indexPath) as! EventTypeTVCell
        let dic = self.bookingAreaDetailsArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.nameLbl.text = dic.TitleEn
        }else{
            cell.nameLbl.text = dic.TitleAr
        }
        cell.descLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Gusets", value: "", table: nil))!) \(dic.MaxGuestCapacity)"
        if selectIndex != nil{
            if selectIndex == indexPath.row{
                cell.selectImage.image = UIImage(named: "Beta Select 1")
            }else{
                cell.selectImage.image = UIImage(named: "Beta UnSelect 1")
            }
        }else{
            cell.selectImage.image = UIImage(named: "Beta UnSelect 1")
        }
        
        cell.infoBtn.tag = indexPath.row
        cell.infoBtn.addTarget(self, action: #selector(infoBtn_Tapped(_:)), for: .touchUpInside)
        
        DispatchQueue.main.async {
            self.myScrollView.layoutIfNeeded()
            self.tableView.layoutIfNeeded()
            self.tableViewHeight.constant = self.tableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectIndex = indexPath.row
        tableView.reloadData()
    }
    @objc func infoBtn_Tapped(_ sender: UIButton){
        var desc = self.bookingAreaDetailsArray[sender.tag].DescEn
        if AppDelegate.getDelegate().appLanguage == "English"{
            popUpNameLbl.text = "\(self.bookingAreaDetailsArray[sender.tag].TitleEn)"
            //popUpDescLbl.text = "\(self.bookingAreaDetailsArray[sender.tag].DescEn)"
            desc = self.bookingAreaDetailsArray[sender.tag].DescEn
            htmlView(htmlString: self.bookingAreaDetailsArray[sender.tag].DescEn)
        }else{
            popUpNameLbl.text = "\(self.bookingAreaDetailsArray[sender.tag].TitleAr)"
            //popUpDescLbl.text = "\(self.bookingAreaDetailsArray[sender.tag].DescAr)"
            desc = self.bookingAreaDetailsArray[sender.tag].DescAr
            htmlView(htmlString: self.bookingAreaDetailsArray[sender.tag].DescAr)
        }
        //Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(self.bookingAreaDetailsArray[sender.tag].Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        popUpImg.kf.setImage(with: url)
        
        let yPosition = 225 - myScrollView.contentOffset.y
        let startPoint = CGPoint(x: self.view.frame.width - 42, y: yPosition + CGFloat((sender.tag * 70)))
        let height = heightForView(text: desc, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: self.view.frame.width - 60)
        let popUpHeight = ((self.view.frame.width-40)/2.19) + 46 + height
        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: popUpHeight)
        popover.arrowSize = .zero
        popover.cornerRadius = 10
        popover.show(self.popUpLblMainView, point: startPoint)
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
// MARK: Drop Down Delegate Methods
@available(iOS 13.0, *)
extension BetaBookNowVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        numOfGuestsTF.text = name
    }
}
@available(iOS 13.0, *)
extension BetaBookNowVC : UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileTF {
            if(self.mobileTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: 9)
        }else if textField == self.nameTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return  updatedText.safelyLimitedTo(length: 50)
        }
        return true
    }
}
// MARK: Calender Delegate Methods
@available(iOS 13.0, *)
extension BetaBookNowVC:CalenderVCDelegate{
    func didTapAction(getDate:Date,whereType:Int){
        selectDate = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .dateR))"
        eventDateTF.text = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .ddMMyyyy))"
    }
}
extension BetaBookNowVC:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
       
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = text.rangeOfCharacter(from: whitespaceSet)
        if ((textView.text?.count)! == 0 && range1  != nil)
            || ((textView.text?.count)! > 0 && textView.text?.last  == " " && range1 != nil)  {
            return false
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            
        return numberOfChars < 250
    }
}
