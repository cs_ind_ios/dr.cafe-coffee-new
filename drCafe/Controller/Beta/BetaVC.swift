//
//  BetaVC.swift
//  drCafe
//
//  Created by Mac2 on 22/03/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaVC: UIViewController {

    @IBOutlet weak var categoryNameLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        let categoryName = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LIVE Stock, World NEWS", value: "", table: nil))!, text2: " \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "and", value: "", table: nil))!) ", text3: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Real Estate", value: "", table: nil))!, text4: " \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Broadcasting", value: "", table: nil))!)")
        if AppDelegate.getDelegate().appLanguage == "English"{
            categoryName.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], range: NSMakeRange(0,4))
        }else{
            categoryName.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], range: NSMakeRange(0,7))
        }
        categoryNameLbl.attributedText = categoryName
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Start Button Action
    @IBAction func startBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:"Main_Audio", bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(obj, animated: true)
        
//        if AppDelegate.getDelegate().appLanguage == "English"{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            var obj = BetaDetailsVC()
//            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaDetailsVC") as! BetaDetailsVC
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String, text3:String, text4:String) -> NSMutableAttributedString{
        let boldAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 16.0)!
           ]
        let regularAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 16.0)!
           ]
        let boldText1 = NSAttributedString(string: text1, attributes: boldAttribute)
        let regularText1 = NSAttributedString(string: text2, attributes: regularAttribute)
        let boldText2 = NSAttributedString(string: text3, attributes: boldAttribute)
        let regularText2 = NSAttributedString(string: text4, attributes: regularAttribute)
        let newString = NSMutableAttributedString()
        newString.append(boldText1)
        newString.append(regularText1)
        newString.append(boldText2)
        newString.append(regularText2)
        return newString
    }
}
