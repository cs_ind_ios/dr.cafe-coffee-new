//
//  BetaEventBookConfirmationVC.swift
//  drCafe
//
//  Created by mac3 on 15/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import SafariServices

class BetaEventBookConfirmationVC: UIViewController,SFSafariViewControllerDelegate{
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var invoiceNumNameLbl: UILabel!
    @IBOutlet weak var invoiceNumLbl: UILabel!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventDateLbl: UILabel!
    @IBOutlet weak var eventTimeLbl: UILabel!
    @IBOutlet weak var pricePerSeatNameLbl: UILabel!
    @IBOutlet weak var pricePerSeatLbl: UILabel!
    @IBOutlet weak var numOfGuestsNameLbl: UILabel!
    @IBOutlet weak var numOfGuestsLbl: UILabel!
    @IBOutlet weak var subTotalNameLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var totalNameLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var netTotalNameLbl: UILabel!
    @IBOutlet weak var netTotalLbl: UILabel!
    @IBOutlet weak var btmMessageLbl: UILabel!
    @IBOutlet weak var paymentMethodsNameLbl: UILabel!
    @IBOutlet weak var billAmountNameLbl: UILabel!
    @IBOutlet weak var confirmPaymentBtn: UIButton!
    
    var paymentDetailsArray = [PayementMethodsModel]()
    var bookingDetailsArray = [BetaBookingDataModel]()
    var paymentSelectIndex:Int!
    var paymentSelectId = 0
    
    var isOnlinePaymeny:Bool = false
    var isBackApplePay:Bool = false
    var isAppleDevice:Bool = false
    
    //Payment
    var isApplePay:Bool = false
    var isSTCPay:Bool = false
    var isMadaPay:Bool = false
    var isVisa:Bool = false
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.drcafecoffee.payments"
    let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    // MARK: - The payment brands for Ready-to-use UI "APPLEPAY"
    static let checkoutPaymentBrands = ["VISA", "MASTER", "AMEX"]
    static let STCPAYPaymentBrands = ["STC_PAY"]
    static let MADAPAYPaymentBrands = ["MADA"]
    
    var netAmount:Double = 0.0
    var InvoiceNoStr:String = ""
    var stcPayNumber:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Apple Play Support
        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
        request.currencyCode = "SAR"
        let amount = NSDecimalNumber(value: 10)
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
            if let _ = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                // Apple Pay Support
                isAppleDevice = true
            }else{
                // Apple Pay not support
                isAppleDevice = false
            }
        }else{
            // Apple Pay not support
            isAppleDevice = false
        }
        if self.isAppleDevice == false {
            if let row =   self.paymentDetailsArray.firstIndex(where: {$0.Id == 5}) {
                self.paymentDetailsArray.remove(at: row)
            }
        }
        if self.paymentDetailsArray.count > 0{
            for i in 0...self.paymentDetailsArray.count-1{
                self.paymentDetailsArray[i].isSelect = false
            }
        }
        if self.paymentDetailsArray.count == 1{
            self.paymentDetailsArray[0].isSelect = true
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        allData()
    }
    func allData(){
        let dic = bookingDetailsArray[0]
//        self.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking Details", value: "", table: nil))!
//        self.invoiceNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invoice No", value: "", table: nil))!
//        self.pricePerSeatNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price per seat", value: "", table: nil))!
//        self.numOfGuestsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Number of guests", value: "", table: nil))!
//        self.subTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Total", value: "", table: nil))!
//        self.totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!
//        self.vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(dic.Vatpercentage)%) :"
//        self.netTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Total", value: "", table: nil))!
//        self.paymentMethodsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Methods", value: "", table: nil))!
//        self.billAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bill Amount", value: "", table: nil))!
//        self.confirmPaymentBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Continue Payment", value: "", table: nil))!, for: .normal)
//
//        self.invoiceNumLbl.text = ": \(dic.InvoiceNo)"
//        if AppDelegate.getDelegate().appLanguage == "English"{
//            self.eventNameLbl.text = dic.TitleEn
//        }else{
//            self.eventNameLbl.text = dic.TitleAr
//        }
//        eventDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
//        eventTimeLbl.text = "\(dic.StartTime) to \(dic.EndTime)"
//        pricePerSeatLbl.text = ": \(dic.PricePerSeat.withCommas()) SAR"
//        numOfGuestsLbl.text = ": \(dic.Guests)"
//        self.subTotalLbl.text = ": \(dic.GrossTotal.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//        self.totalLbl.text = ": \(dic.ItemTotal.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//        self.vatLbl.text = ": \(dic.Vat.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//        self.netTotalLbl.text = ": \(dic.NetTotal.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
//        self.btmMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please complete payment at the earliest to confirm the seats.", value: "", table: nil))!
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func confirmPaymentBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        //self.getCheckoutId(amount: <#T##Double#>)
        
    }
    //MARK: Validations
    func validInputParams() -> Bool {
        if paymentSelectId == 0{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Payment Method", value: "", table: nil))!)
            return false
        }
        return true
    }
    
    
    //MARK: Payment SDK
    func getCheckoutId(amount:Double) {
        let userDic = getUserDetails()
        self.isOnlinePaymeny = true
        var customerPhone = userDic.Mobile
        if self.isSTCPay == true{
            customerPhone = stcPayNumber
        }
        let dic:[String:Any] = [
            "amount":String(format: "%.2f", amount),
            "shopperResultUrl": "\(self.urlScheme)://result" ,
            "isCardRegistration": "false",
            "merchantTransactionId": self.InvoiceNoStr,
            "customerEmail":userDic.Email,
            "userId":userDic.UserId,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "isMada":isMadaPay,
            "isVisaMaster":isVisa,
            "customerPhone":customerPhone,
            "customerName":userDic.FullName,
            "IsSubscription":false,
            "PaymentMethodId":paymentSelectId
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
            }else{
                DispatchQueue.main.async {
                   // print(data.Data![0].id)
                    self.checkoutID = data.Data![0].id
                    if self.isApplePay == true {
                        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
                        request.currencyCode = "SAR"
                        let amount = NSDecimalNumber(value: amount)
                        //print("ApplePay:\(amount)")
                        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
                        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
                            if let vc = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                            }else{
                                self.isOnlinePaymeny = false
                                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Unable to present Apple Pay authorization.", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                                    NSLog("Apple Pay not supported.");
                                    
                                }))
                            }
                        }else{
                            
                        }
                    }else{
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                        self.checkoutProvider?.delegate = self
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: nil)
                    }
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    func TrackOrderScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension BetaEventBookConfirmationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paymentDetailsArray.count > 0{
            let paymentArray = paymentDetailsArray.filter({$0.IsActive! == true})
            return paymentArray.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
        
        if paymentDetailsArray[indexPath.row].IsActive! == true{
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameEn!
            }else{
                cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameAr!
            }
            
            if paymentDetailsArray[indexPath.row].isSelect! == true{
                cell.selectImg.image = UIImage(named: "Box Selected")
            }else{
                cell.selectImg.image = UIImage(named: "Box Un Select")
            }
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(paymentDetailsArray[indexPath.row].Icon!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
        }
        
        self.tableView.layoutIfNeeded()
        self.tableViewHeight.constant = tableView.contentSize.height
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        paymentSelectIndex = indexPath.row
        for i in 0...paymentDetailsArray.count-1{
            paymentDetailsArray[i].isSelect! = false
        }
        paymentDetailsArray[indexPath.row].isSelect! = true
        paymentSelectId = paymentDetailsArray[indexPath.row].Id!
        if paymentDetailsArray[indexPath.row].Id! == 2{
            paymentSelectId = 2
            isApplePay = false
            isSTCPay = false
            isMadaPay = false
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 3{
            paymentSelectId = 3
            isApplePay = false
            isSTCPay = false
            isMadaPay = false
            isVisa = true
        }else if paymentDetailsArray[indexPath.row].Id! == 4{
            paymentSelectId = 4
            isApplePay = false
            isSTCPay = false
            isMadaPay = true
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 5{
            paymentSelectId = 5
            isApplePay = true
            isSTCPay = false
            isMadaPay = false
            isVisa = false
        }else if paymentDetailsArray[indexPath.row].Id! == 6{
            paymentSelectId = 6
            isApplePay = false
            isSTCPay = true
            isMadaPay = false
            isVisa = false
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "STCPayVC") as! STCPayVC
//            obj.modalPresentationStyle = .overCurrentContext
//            obj.modalTransitionStyle = .crossDissolve
//            obj.stcPayVCDelegate =  self
//            self.present(obj, animated: false, completion: nil)
        }
        self.tableView.reloadData()
    }
}
@available(iOS 13.0, *)
extension BetaEventBookConfirmationVC:PKPaymentAuthorizationViewControllerDelegate{
   func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
       controller.dismiss(animated: true, completion: nil)
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
           ANLoader.hide()
       }
       self.isOnlinePaymeny = false
        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            controller.dismiss(animated: true, completion: nil)
        }))
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    if let params = try? OPPApplePayPaymentParams(checkoutID: self.checkoutID, tokenData: payment.token.paymentData) as OPPApplePayPaymentParams? {
        self.transaction = OPPTransaction(paymentParams: params)
        self.provider.submitTransaction(OPPTransaction(paymentParams: params), completionHandler: { (transaction, error) in
        if (error != nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            controller.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                
                }))
            }
        }else{
            controller.dismiss(animated: true, completion: nil)
            self.provider.requestCheckoutInfo(withCheckoutID: self.checkoutID) { (checkoutInfo, error) in
            DispatchQueue.main.async {
            guard let resourcePath = checkoutInfo?.resourcePath else {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)..", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
                return
            }
            ANLoader.showLoading("", disableUI: true)
            self.transaction = nil
            let dic:[String:Any] = [
                "resourcePath": resourcePath,
                "userId":"\(UserDef.getUserId())",
                "isApplePay":self.isApplePay,
                "isSTCPay":self.isSTCPay,
                "isMada":self.isMadaPay,
                "InvoiceNo":self.InvoiceNoStr,
                "isVisaMaster":self.isVisa,
                "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))",
                "ExpectedDate":"",
                "KitchenStartTime":"",
                "OrderStatus":"New",
                "PaymentMethodId":self.paymentSelectId
            ]
            PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    //self.TrackOrderScreen()
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                    self.isOnlinePaymeny = false
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }
            }){(error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
            }
            }
        }
        }
        })
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
}
@available(iOS 13.0, *)
extension BetaEventBookConfirmationVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    // MARK: - Payment helpers
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        self.transaction = transaction
        if transaction.type == .synchronous {
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        } else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = OPPCheckoutSettings.init()
        if self.isSTCPay == true{
            checkoutSettings.paymentBrands = ConfirmationVC.STCPAYPaymentBrands
        }else if self.isMadaPay == true {
            checkoutSettings.paymentBrands = ConfirmationVC.MADAPAYPaymentBrands
        }else{
            checkoutSettings.paymentBrands = ConfirmationVC.checkoutPaymentBrands
        }
        checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
        checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
        checkoutSettings.theme.confirmationButtonColor = self.mainColor
        checkoutSettings.theme.accentColor = self.mainColor
        checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
        checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resource path is invalid", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        ANLoader.showLoading("", disableUI: true)
        self.transaction = nil
        let dic:[String:Any] = ["resourcePath": resourcePath,
                                "userId":"\(UserDef.getUserId())",
                                "isApplePay":self.isApplePay,
                                "isSTCPay":self.isSTCPay,
                                "isMada":self.isMadaPay,
                                "isVisaMaster" : self.isVisa,
                                "InvoiceNo":self.InvoiceNoStr,
                                "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))",
                                "ExpectedDate":"",
                                "KitchenStartTime":"",
                                "OrderStatus":"New",
                                "PaymentMethodId":self.paymentSelectId
        ]
        
        PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    self.TrackOrderScreen()
                }else{
                    self.isOnlinePaymeny = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    }))
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
    // MARK: - Async payment callback
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
}
@available(iOS 13.0, *)
extension BetaEventBookConfirmationVC: STCPayVCDelegate{
    func didTapAction(mobileNumber: String) {
        stcPayNumber = mobileNumber
     }
}
