//
//  BetaDetailsVC.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaDetailsVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var categoryTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upcomingEventsInfoBtn: UIButton!
    @IBOutlet weak var upcomingEventsTitleViewHeight: NSLayoutConstraint!//71
    @IBOutlet weak var upcomingEventsTableView: UITableView!
    @IBOutlet weak var upcomingEventsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upcomingEventsNameLbl: UILabel!
    
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    var categoryDetailsArray = [["Name":"About BETA","Desc":"Business, Event, Training, Arena","Image":"About Beta"], ["Name":"Choose your Channel","Desc":"Choose your favourite channel Audio","Image":"Beta Listen Audio"], ["Name":"Book Now","Desc":"Event, Workstation, Conference room","Image":"Beta Book Now"], ["Name":"Booking History","Desc":"Discover BETA and More","Image":"Beta History-1"], ["Name":"Contact Info","Desc":"Choose your favourite channel Audio","Image":"Beta Info"]]
    var upcomingEventsArray = [BetaEventsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        upcomingEventsTableView.delegate = self
        upcomingEventsTableView.dataSource = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.getBetaDetailsService()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        upcomingEventsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Up coming Events", value: "", table: nil))!
        categoryDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "About BETA", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Business, Event, Training, Arena", value: "", table: nil))!,"Image":"About Beta"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose your Channel", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose your favourite channel Audio", value: "", table: nil))!,"Image":"Beta Listen Audio"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Now", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Event, Workstation, Conference room", value: "", table: nil))!,"Image":"Beta Book Now"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "BETA Tour", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Explore the BETA Business Model", value: "", table: nil))!,"Image":"BETA Tour"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booking History", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discover BETA and More", value: "", table: nil))!,"Image":"Beta History-1"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Info", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "For any inquiries contact us", value: "", table: nil))!,"Image":"Beta Info"]]
        DispatchQueue.main.async {
            self.categoryTableView.reloadData()
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func upcomingEventsInfoBtn_Tapped(_ sender: Any) {
        
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = ["UserId":UserDef.getUserId()]
        BetaModuleServices.BetaPublicEventsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data{
                    SaveAddressClass.upcomingEventDetailsArray = [data.Data!]
                    self.upcomingEventsArray = data.Data!.Events!
                    if data.Data!.Events!.count > 0{
                        self.upcomingEventsTitleViewHeight.constant = 71
                        self.upcomingEventsTableView.reloadData()
                    }else{
                        self.upcomingEventsTitleViewHeight.constant = 0
                        self.upcomingEventsTableViewHeight.constant = 0
                    }
                }
                
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
extension BetaDetailsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == categoryTableView{
            return categoryDetailsArray.count
        }else{
            return self.upcomingEventsArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == categoryTableView{
            return 80
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == categoryTableView{
            categoryTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!)
            let cell = categoryTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaTypeTVCell", value: "", table: nil))!, for: indexPath) as! BetaTypeTVCell
            cell.nameLbl.text = categoryDetailsArray[indexPath.row]["Name"]
            cell.descLbl.text = categoryDetailsArray[indexPath.row]["Desc"]
            if indexPath.row > 2{
                cell.imgBGView.isHidden = false
                cell.typeImage.isHidden = true
                cell.imgTwo.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
            }else{
                cell.typeImage.isHidden = false
                cell.imgBGView.isHidden = true
                cell.typeImage.image = UIImage(named: categoryDetailsArray[indexPath.row]["Image"]!)
            }
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.categoryTableView.layoutIfNeeded()
                self.categoryTableViewHeight.constant = self.categoryTableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }else{
            upcomingEventsTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!)
            let cell = upcomingEventsTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, for: indexPath) as! BetaUpcomingEventsTVCell
            let dic = self.upcomingEventsArray[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.eventNameLbl.text = dic.TitleEn
                cell.titleLbl.text = dic.DescEn
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }else{
                cell.eventNameLbl.text = dic.TitleAr
                cell.titleLbl.text = dic.DescAr
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Icon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
            cell.img.layer.cornerRadius = cell.img.frame.width / 2
            cell.img.clipsToBounds = true
            
            cell.bookNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Now", value: "", table: nil))!, for: .normal)
            
//            cell.pricePerSeatLbl.text = ": \(dic.PricePerSeat.withCommas()) SAR"
//            cell.availableSeatsLbl.text = ": \(dic.AvailableSeats)"
//            cell.cityLbl.text = ": \(dic.StoreCity)"
//            cell.maxSeatsPerBookLblHeight.constant = 0
//            cell.maxSeatsPerBookLblTop.constant = 0
            cell.storeDetailsViewHeight.constant = 0
            
            cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: dic.EventDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
            cell.timeLbl.text = "\(dic.StartTime) to \(dic.EndTime)"
            
            cell.bookNowBtn.tag = indexPath.row
            cell.bookNowBtn.addTarget(self, action: #selector(bookNowBtn_Tapped(_:)), for: .touchUpInside)
            
            cell.subTitleLbl.isHidden = true
            cell.bookNowBtn.isHidden = false
            cell.bookBtnStackView.isHidden = true
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.upcomingEventsTableView.layoutIfNeeded()
                self.upcomingEventsTableViewHeight.constant = self.upcomingEventsTableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView{
            if indexPath.row == 0 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = AboutBetaVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "AboutBetaVC") as! AboutBetaVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 1 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:"Main_Audio", bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 2 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = BetaBookNowVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaBookNowVC") as! BetaBookNowVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 3 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = BetaTourVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaTourVC") as! BetaTourVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 4 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewBetaBookingHistoryVC") as! NewBetaBookingHistoryVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 5 {
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaMoreInfoVC") as! BetaMoreInfoVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BetaUpcomingEventsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaUpcomingEventsVC") as! BetaUpcomingEventsVC
            obj.upcomingEventsArray = upcomingEventsArray
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    @objc func bookNowBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaUpcomingEventsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaUpcomingEventsVC") as! BetaUpcomingEventsVC
        obj.upcomingEventsArray = upcomingEventsArray
        self.navigationController?.pushViewController(obj, animated: true)
    }
}


