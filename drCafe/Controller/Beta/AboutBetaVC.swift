//
//  AboutBetaVC.swift
//  drCafe
//
//  Created by Mac2 on 04/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class AboutBetaVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var popArrowView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var BtableView: UITableView!
    @IBOutlet weak var EtableView: UITableView!
    @IBOutlet weak var TtableView: UITableView!
    @IBOutlet weak var AtableView: UITableView!
    @IBOutlet var nameBtn: [UIButton]!
    
    @IBOutlet weak var BMainView: UIView!
    @IBOutlet weak var EMainView: UIView!
    @IBOutlet weak var TMainView: UIView!
    @IBOutlet weak var AMainView: UIView!
    
    var nameSelectIndex = 0
    var allDetailsArray = [BetaDefinationDataModel]()
    var filterArray = [BetaDefinationDataModel]()
    var BfilterArray = [BetaDefinationDataModel]()
    var EfilterArray = [BetaDefinationDataModel]()
    var TfilterArray = [BetaDefinationDataModel]()
    var AfilterArray = [BetaDefinationDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BtableView.delegate = self
        BtableView.dataSource = self
        EtableView.delegate = self
        EtableView.dataSource = self
        TtableView.delegate = self
        TtableView.dataSource = self
        AtableView.delegate = self
        AtableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.getBetaDetailsService()
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            collectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            collectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        //Gesture Fnctionality
        self.addSwipe()
    }
    func addSwipe(){//View with Gestures
        for direction in [UISwipeGestureRecognizer.Direction.left, .right]{
            let swipeGest = UISwipeGestureRecognizer(target: self, action: #selector(BViewswipeAction(_:)))
            swipeGest.direction = direction
            self.BtableView.addGestureRecognizer(swipeGest)
        }
        for direction in [UISwipeGestureRecognizer.Direction.left, .right]{
            let swipeGest = UISwipeGestureRecognizer(target: self, action: #selector(EViewswipeAction(_:)))
            swipeGest.direction = direction
            self.EtableView.addGestureRecognizer(swipeGest)
        }
        for direction in [UISwipeGestureRecognizer.Direction.left,.right]{
            let swipeGest = UISwipeGestureRecognizer(target: self, action: #selector(TViewswipeAction(_:)))
            swipeGest.direction = direction
            self.TtableView.addGestureRecognizer(swipeGest)
        }
        for direction in [UISwipeGestureRecognizer.Direction.left, .right]{
            let swipeGest = UISwipeGestureRecognizer(target: self, action: #selector(AViewswipeAction(_:)))
            swipeGest.direction = direction
            self.AtableView.addGestureRecognizer(swipeGest)
        }
    }
    //Views Swipe functionality
    @objc func BViewswipeAction(_ gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                if AppDelegate.getDelegate().appLanguage != "English"{
                    let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                    let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row+1, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                if AppDelegate.getDelegate().appLanguage == "English"{
                    let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                    let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row+1, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            default: break
        }
    }
    @objc func EViewswipeAction(_ gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                var index = 0
                let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    index = visibleIndexPath!.row-1
                    self.nameSelectIndex = visibleIndexPath!.row - 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }else{
                    index = visibleIndexPath!.row+1
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                var index = 0
                let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    index = visibleIndexPath!.row+1
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }else{
                    index = visibleIndexPath!.row-1
                    self.nameSelectIndex = visibleIndexPath!.row - 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            default: break
        }
    }
    @objc func TViewswipeAction(_ gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                var index = 0
                let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    index = visibleIndexPath!.row-1
                    self.nameSelectIndex = visibleIndexPath!.row - 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }else{
                    index = visibleIndexPath!.row+1
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                var index = 0
                let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    index = visibleIndexPath!.row+1
                    self.nameSelectIndex = visibleIndexPath!.row + 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }else{
                    index = visibleIndexPath!.row-1
                    self.nameSelectIndex = visibleIndexPath!.row - 1
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
                    self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                }
            default: break
        }
    }
    @objc func AViewswipeAction(_ gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                if AppDelegate.getDelegate().appLanguage == "English"{
                    let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                    let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                    if 4 > visibleIndexPath!.row - 1 {
                        self.nameSelectIndex = visibleIndexPath!.row - 1
                        self.collectionView.reloadData()
                        self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row-1, section: 0), at: .right, animated: true)
                        self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: false, back: true, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }
                }
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                if AppDelegate.getDelegate().appLanguage != "English"{
                    let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
                    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                    let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
                    if 4 > visibleIndexPath!.row - 1 {
                        self.nameSelectIndex = visibleIndexPath!.row - 1
                        self.collectionView.reloadData()
                        self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row-1, section: 0), at: .right, animated: true)
                        self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: true, back: false, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }
                }
            default: break
        }
    }
    func getBetaDetailsService(){
        let dic:[String:Any] = [:]
        BetaModuleServices.BetaDefinationService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.allDetailsArray = data.Data!
                self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                self.BfilterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                self.EfilterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                self.TfilterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                self.AfilterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                self.collectionView.reloadData()
                self.BtableView.reloadData()
                self.EtableView.reloadData()
                self.TtableView.reloadData()
                self.AtableView.reloadData()
                if AppDelegate.getDelegate().appLanguage != "English"{
                    self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
                }
                self.BMainView.isHidden = false
                self.EMainView.isHidden = true
                self.TMainView.isHidden = true
                self.AMainView.isHidden = true
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    
    //MARK:
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:
    //MARK: Next Arrow Button Action
    @IBAction func nextArrowBtn_Tapped(_ sender: Any) {
        if self.allDetailsArray.count > 0{
            let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
            var front = false
            var back = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                front = true
                back = false
            }else{
                front = false
                back = true
            }
            if allDetailsArray.count > 4{
                if 4 > visibleIndexPath!.row + 1 {
                    DispatchQueue.main.async {
                        self.nameSelectIndex = visibleIndexPath!.row + 1
                        if self.nameSelectIndex == 0{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                            self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else if self.nameSelectIndex == 1{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                            self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else if self.nameSelectIndex == 2{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                            self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else {
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                            self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }
                        self.collectionView.reloadData()
                        self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row+1, section: 0), at: .right, animated: true)
                    }
                }else{
                    nameSelectIndex = 0
                    self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                    self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
                }
            }else{
                if allDetailsArray.count > visibleIndexPath!.row + 1 {
                    DispatchQueue.main.async {
                        self.nameSelectIndex = visibleIndexPath!.row + 1
                        if self.nameSelectIndex == 0{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                            self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else if self.nameSelectIndex == 1{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                            self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else if self.nameSelectIndex == 2{
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                            self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }else {
                            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                            self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                        }
                        self.collectionView.reloadData()
                        self.collectionView.scrollToItem(at: IndexPath(item: visibleIndexPath!.row+1, section: 0), at: .right, animated: true)
                    }
                }else{
                    nameSelectIndex = 0
                    self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                    self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
                }
            }
        }
    }
    func bottomDisplayViews(B:Bool, E:Bool, T:Bool, A:Bool, front:Bool, back:Bool, appLanguage:String){
        if appLanguage == "English"{
            if front == true{
                if B == false{
                    self.viewSlideInFromRightToLeft(view: BMainView)
                }
                if E == false{
                    self.viewSlideInFromLeftToRight(view: BMainView)
                    self.viewSlideInFromLeftToRight(view: EMainView)
                }
                if T == false{
                    self.viewSlideInFromLeftToRight(view: EMainView)
                    self.viewSlideInFromLeftToRight(view: TMainView)
                }
                if A == false{
                    self.viewSlideInFromLeftToRight(view: TMainView)
                    self.viewSlideInFromLeftToRight(view: AMainView)
                }
            }else if back == true{
                if B == false{
                    self.viewSlideInFromRightToLeft(view: EMainView)
                    self.viewSlideInFromRightToLeft(view: BMainView)
                }
                if E == false{
                    self.viewSlideInFromRightToLeft(view: TMainView)
                    self.viewSlideInFromRightToLeft(view: EMainView)
                }
                if T == false{
                    self.viewSlideInFromRightToLeft(view: AMainView)
                    self.viewSlideInFromRightToLeft(view: TMainView)
                }
                if A == false{
                    self.viewSlideInFromRightToLeft(view: AMainView)
                }
            }
        }else{
            if back == true{
                if B == false{
                    self.viewSlideInFromRightToLeft(view: BMainView)
                }
                if E == false{
                    self.viewSlideInFromRightToLeft(view: BMainView)
                    self.viewSlideInFromRightToLeft(view: EMainView)
                }
                if T == false{
                    self.viewSlideInFromRightToLeft(view: EMainView)
                    self.viewSlideInFromRightToLeft(view: TMainView)
                }
                if A == false{
                    self.viewSlideInFromRightToLeft(view: TMainView)
                    self.viewSlideInFromRightToLeft(view: AMainView)
                }
            }else if front == true{
                if B == false{
                    self.viewSlideInFromLeftToRight(view: EMainView)
                    self.viewSlideInFromLeftToRight(view: BMainView)
                }
                if E == false{
                    self.viewSlideInFromLeftToRight(view: TMainView)
                    self.viewSlideInFromLeftToRight(view: EMainView)
                }
                if T == false{
                    self.viewSlideInFromLeftToRight(view: AMainView)
                    self.viewSlideInFromLeftToRight(view: TMainView)
                }
                if A == false{
                    self.viewSlideInFromLeftToRight(view: AMainView)
                }
            }
        }
        self.BMainView.isHidden = B
        self.EMainView.isHidden = E
        self.TMainView.isHidden = T
        self.AMainView.isHidden = A
    }
    func viewSlideInFromLeftToRight(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        view.layer.add(transition, forKey: kCATransition)
    }
    func viewSlideInFromRightToLeft(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        view.layer.add(transition, forKey: kCATransition)
    }
    //MARK: Name Button Action
    @IBAction func nameBtn_Tapped(_ sender: UIButton) {
        if self.allDetailsArray.count > 0{
            if sender.tag == 101{
                nameSelectIndex = 0
                self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                collectionView.reloadData()
                self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
                self.BMainView.isHidden = false
                self.EMainView.isHidden = true
                self.TMainView.isHidden = true
                self.AMainView.isHidden = true
            }else if sender.tag == 102{
                nameSelectIndex = 1
                self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                collectionView.reloadData()
                self.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .right, animated: true)
                self.BMainView.isHidden = true
                self.EMainView.isHidden = false
                self.TMainView.isHidden = true
                self.AMainView.isHidden = true
            }else if sender.tag == 103{
                nameSelectIndex = 2
                self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                collectionView.reloadData()
                self.collectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .right, animated: true)
                self.BMainView.isHidden = true
                self.EMainView.isHidden = true
                self.TMainView.isHidden = false
                self.AMainView.isHidden = true
            }else {
                nameSelectIndex = 3
                self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                collectionView.reloadData()
                self.collectionView.scrollToItem(at: IndexPath(item: 3, section: 0), at: .right, animated: true)
                self.BMainView.isHidden = true
                self.EMainView.isHidden = true
                self.TMainView.isHidden = true
                self.AMainView.isHidden = false
            }
        }
    }
}
extension AboutBetaVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.allDetailsArray.count > 4{
            return 4
        }else{
            return self.allDetailsArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaNameDescriptionCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaNameDescriptionCVCell", value: "", table: nil))!)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaNameDescriptionCVCell", value: "", table: nil))!, for: indexPath) as! BetaNameDescriptionCVCell
        let dic = self.allDetailsArray[indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.titleLbl.text = dic.NameEn
            cell.descLbl.text = dic.DefinationEn
        }else{
            cell.titleLbl.text = dic.NameAr
            cell.descLbl.text = dic.DefinationAr
        }
        cell.numLbl.text = "/\(dic.SequenceNo)"
        if indexPath.row == nameSelectIndex{
            cell.arrawImg.isHidden = true
            if AppDelegate.getDelegate().appLanguage == "English"{
                if nameSelectIndex == 0{
                    cell.arrowLeading.constant = 13
                    cell.arrawImg.isHidden = false
                    //width = 13
                }else if nameSelectIndex == 1{
                    cell.arrowLeading.constant = 24
                    cell.arrawImg.isHidden = false
                    //width = 25
                }else if nameSelectIndex == 2{
                    cell.arrowLeading.constant = 58
                    cell.arrawImg.isHidden = false
                    //width = 60
                }else{
                    cell.arrowLeading.constant = 90
                    cell.arrawImg.isHidden = false
                    //width = 90
                }
            }else{
                cell.arrawImg.isHidden = true
//                if nameSelectIndex == 0{
//                    cell.arrowLeading.constant = cell.titleLbl.frame.maxX - 6
//                    cell.arrawImg.isHidden = false
//                    //width = 13
//                }else if nameSelectIndex == 1{
//                    cell.arrowLeading.constant = cell.titleLbl.frame.maxX - 45
//                    cell.arrawImg.isHidden = false
//                    //width = 25
//                }else if nameSelectIndex == 2{
//                    cell.arrowLeading.constant = cell.titleLbl.frame.maxX - 78
//                    cell.arrawImg.isHidden = false
//                    //width = 60
//                }else{
//                    cell.arrowLeading.constant = cell.titleLbl.frame.maxX - 87
//                    cell.arrawImg.isHidden = false
//                    //width = 90
//                }
            }
            DispatchQueue.main.async {
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if self.nameSelectIndex == 0{
                        cell.arrowLeading.constant = 13
                        cell.arrawImg.isHidden = false
                        //width = 13
                    }
                }else{
                    //if self.nameSelectIndex == 0{
                        //cell.arrowLeading.constant = cell.titleLbl.frame.maxX - 6
                        cell.arrawImg.isHidden = true
                        //width = 13
                    //}
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        nameSelectIndex = indexPath.row
        if indexPath.row == 0{
            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
        }else if indexPath.row == 1{
            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .right, animated: true)
        }else if indexPath.row == 2{
            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .right, animated: true)
        }else {
            self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: 3, section: 0), at: .right, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.collectionView.bounds
        return CGSize(width: screenSize.width - 30 , height: screenSize.height)
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == collectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.collectionView.indexPathsForVisibleItems
            indexes.sort()
            var index = indexes.first!
            var front = false
            var back = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row
                    if indexes.last!.row == 0 && indexes.first!.row == 0{
                        front = false
                        back = false
                    }else{
                        front = false
                        back = true
                    }
                }else{
                    index.row = index.row + 1
                    front = true
                    back = false
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                    front = false
                    back = true
                }else{
                    index.row = index.row
                    if indexes.last!.row == 0 && indexes.first!.row == 0{
                        front = false
                        back = false
                    }else{
                        front = true
                        back = false
                    }
                }
            }
            if allDetailsArray.count > 4{
                if 4 > index.row {
                    nameSelectIndex = index.row
                    if index.row == 0{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                        self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else if index.row == 1{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                        self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else if index.row == 2{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                        self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else {
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                        self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }
                    collectionView.reloadData()
                    DispatchQueue.main.async {
                        self.collectionView.scrollToItem(at: index, at: .right, animated: true )
                    }
                }
            }else{
                if allDetailsArray.count > index.row {
                    nameSelectIndex = index.row
                    if index.row == 0{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "b"})
                        self.bottomDisplayViews(B: false, E: true, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else if index.row == 1{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "e"})
                        self.bottomDisplayViews(B: true, E: false, T: true, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else if index.row == 2{
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "t"})
                        self.bottomDisplayViews(B: true, E: true, T: false, A: true, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }else {
                        self.filterArray = self.allDetailsArray.filter({$0.KeyEn == "a"})
                        self.bottomDisplayViews(B: true, E: true, T: true, A: false, front: front, back: back, appLanguage: AppDelegate.getDelegate().appLanguage!)
                    }
                    collectionView.reloadData()
                    DispatchQueue.main.async {
                        self.collectionView.scrollToItem(at: index, at: .right, animated: true )
                    }
                }
            }
        }
    }
}
extension AboutBetaVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == BtableView{
            if BfilterArray.count > 0{
                return self.BfilterArray[0].Programs!.count
            }else{
                return 0
            }
        }else if tableView == EtableView{
            if EfilterArray.count > 0{
                return self.EfilterArray[0].Programs!.count
            }else{
                return 0
            }
        }else if tableView == TtableView{
            if TfilterArray.count > 0{
                return self.TfilterArray[0].Programs!.count
            }else{
                return 0
            }
        }else{
            if AfilterArray.count > 0{
                return self.AfilterArray[0].Programs!.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, for: indexPath) as! BetaUpcomingEventsTVCell
        var dic1 = [BetaDefinationDataModel]()
        if tableView == BtableView{
            dic1 = BfilterArray
        }else if tableView == EtableView{
            dic1 = EfilterArray
        }else if tableView == TtableView{
            dic1 = TfilterArray
        }else{
            dic1 = AfilterArray
        }
        
        if dic1.count > 0{
            let dic = dic1[0].Programs![indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.eventNameLbl.text = dic.TitleEn
                cell.titleLbl.text = dic.DescEn
                cell.subTitleLbl.text = dic.SubTitleEn
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
                cell.img.layer.cornerRadius = cell.img.frame.width / 2
                cell.img.clipsToBounds = true
            }else{
                cell.eventNameLbl.text = dic.TitleAr
                cell.titleLbl.text = dic.DescAr
                cell.subTitleLbl.text = dic.SubTitleAr
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.eventImage.kf.setImage(with: url)
            }
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.iCon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
            cell.img.layer.cornerRadius = cell.img.frame.width / 2
            cell.img.clipsToBounds = true
        }
        
        cell.dateLbl.isHidden = true
        cell.timeLbl.isHidden = true
        cell.bookNowBtn.isHidden = false
        cell.bookBtnStackView.isHidden = true
        cell.bookNowBtn.isUserInteractionEnabled = false
        cell.bookNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Now", value: "", table: nil))!, for: .normal)

        //cell.bookNowBtnHeight.constant = 0 //44
        //cell.bookNowBtnBtm.constant = 0 //10
        //cell.bookBtnHeight.constant = 0
        //cell.bookBtnBtm.constant = 0
        //cell.bookNowBtn.isHidden = true
        //cell.bookBtnStackView.isHidden = true
        //cell.btmDetailHeight.constant = 0
        
        DispatchQueue.main.async {
            tableView.layoutIfNeeded()
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BetaBookNowVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BetaBookNowVC") as! BetaBookNowVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
