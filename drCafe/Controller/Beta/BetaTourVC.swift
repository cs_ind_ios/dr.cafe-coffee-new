//
//  BetaTourVC.swift
//  drCafe
//
//  Created by Mac2 on 07/09/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class BetaTourVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var eventDateTF: UITextField!
    @IBOutlet weak var startTimeTF: UITextField!
    @IBOutlet weak var bookBtn: UIButton!
    
    @IBOutlet weak var selfBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet var changeTimeView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var changeTimeCancelBtn: UIButton!
    @IBOutlet weak var changeTimeDoneBtn: UIButton!
    
    @IBOutlet weak var selfNameLbl: UILabel!
    @IBOutlet weak var eventDateNameLbl: UILabel!
    @IBOutlet weak var startTimeNameLbl: UILabel!
    @IBOutlet var timeView: UIView!

    var popover = Popover()
    var bookingFor = 1
    var selectDate:String!
    var selectStartTime:String!
    var selectEndTime:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        mobileTF.delegate = self
        nameTF.delegate = self
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        bookBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book", value: "", table: nil))!, for: .normal)
        selfNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Self", value: "", table: nil))!
        eventDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "TOUR DATE", value: "", table: nil))!
        startTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ARRIVAL TIME", value: "", table: nil))!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        mobileTF.isEnabled = false
    }
    
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Self Button Action
    @IBAction func selfBtn_Tapped(_ sender: Any) {
        bookingFor = 1
        selfBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        nameTF.text = "\((userDic["FullName"]! as! String))"
        emailTF.text = "\((userDic["Email"]! as! String))"
        mobileTF.text = "+966\((userDic["Mobile"]! as! String))"
        nameTF.isEnabled = false
        emailTF.isEnabled = false
        mobileTF.isEnabled = false
    }
    
    //MARK: Other Button Action
    @IBAction func otherBtn_Tapped(_ sender: Any) {
        bookingFor = 2
        selfBtn.setImage(UIImage(named: "Beta UnSelect 1"), for: .normal)
        otherBtn.setImage(UIImage(named: "Beta Select 1"), for: .normal)
        nameTF.text = ""
        emailTF.text = ""
        mobileTF.text = ""
        nameTF.isEnabled = true
        emailTF.isEnabled = true
        mobileTF.isEnabled = true
    }
    
    @IBAction func bookBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var mobileNum = mobileTF.text!
        if mobileTF.text!.count > 9{
            mobileNum = mobileTF.text!.removeCountryCode()
        }
        //let deviceInfoStr = "\(getDeviceInfo())"
        let dic = ["UserId":UserDef.getUserId(), "bookingAreaId":1, "EventDescription":"Request for BETA Tour", "EventDate":"\(selectDate!)", "StartTime":"\(selectStartTime!)", "EndTime":"\(selectEndTime!)", "Guests":2, "ContactPerson":"\(nameTF.text!)", "ContactMobile":"\(mobileNum)", "deviceinfo":"", "Bookingfor":bookingFor] as [String : Any]
        BetaModuleServices.BookEnquiryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if bookingFor == 2{
            if nameTF.text == nil || (nameTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EnterFullName", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isCharCount(name: nameTF.text!){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                    return false
                }
            }
            if mobileTF.text == nil || (mobileTF.text == ""){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
                return false
            }else{
                if !Validations.isMobileNumberValidWithNumOfDigits(mobile: mobileTF.text!, minDigits: 9, maxDigits: 9){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                    return false
                }
            }
        }
        if selectDate == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Tour Date", value: "", table: nil))!)
            return false
        }
        if selectStartTime == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Arrival Time", value: "", table: nil))!)
            return false
        }
        return true
    }
    
    @IBAction func dateBtn_Tapped(_ sender: UIButton) {
        let obj : CalenderVC! = (self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC)
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 300, height: 300)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .down
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 25 , y: 5, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 25 , y: 5, width: 0, height: sender.frame.size.height)
        }
        var dateComponents = DateComponents()
        dateComponents.day = 1
        let minimumDate = Calendar.current.date(byAdding: dateComponents, to: Date())
        obj.minimumDate = minimumDate
        obj.maxDays = 365
        obj.obj_where = 3
        obj.calenderVCDelegate = self
        self.present(obj, animated: false, completion: nil)
    }
    @IBAction func startTimeBtn_Tapped(_ sender: UIButton) {
        if selectDate == nil{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Event Date", value: "", table: nil))!)
            return
        }
        let seltDate = DateConverts.convertStringToDate(date: "\(selectDate!) 12:00 AM", dateformatType: .dateTimeA)
        self.datePicker.setDate(seltDate, animated: true)
        self.datePicker.minimumDate = seltDate
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        self.datePicker.setValue(UIColor.black, forKey: "textColor")
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        self.datePicker.datePickerMode = .time
        let buttonFrame = myScrollView.superview?.convert(sender.frame, from: timeView)
        var startPoint = CGPoint(x: sender.frame.maxX, y: buttonFrame!.midY)
        if AppDelegate.getDelegate().appLanguage == "English"{
            startPoint = CGPoint(x: sender.frame.maxX - 2, y:  buttonFrame!.midY)
        }else{
            startPoint = CGPoint(x: sender.frame.maxX + 70, y: buttonFrame!.midY)
        }
        self.changeTimeView.frame = CGRect(x: 0, y: 0, width: 280, height: 280)
        popover.popoverType = .up
        popover.show(self.changeTimeView, point: startPoint)
    }
    @IBAction func changeTimeCancelBtn_Tapped(_ sender: Any) {
        popover.dismiss()
    }
    @IBAction func changeTimeDoneBtn_Tapped(_ sender: Any) {
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        timeFormatter.locale = Locale(identifier: "EN")
        self.startTimeTF.text = timeFormatter.string(from:self.datePicker.date)
        selectStartTime = timeFormatter.string(from:self.datePicker.date)
        let endTime = self.datePicker.date.addingTimeInterval(TimeInterval(20 * 60))
        selectEndTime = timeFormatter.string(from:endTime)
        popover.dismiss()
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
extension BetaTourVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BetaUpcomingEventsTVCell", value: "", table: nil))!, for: indexPath) as! BetaUpcomingEventsTVCell
        
        cell.eventNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BETA Tour", value: "", table: nil))!
        cell.titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Explore the BETA Business Model", value: "", table: nil))!
        cell.eventImage.image = UIImage(named: "betaTour")
        cell.img.image = UIImage(named: "beta_tour_logo")
        cell.img.layer.cornerRadius = cell.img.frame.width/2
        cell.img.clipsToBounds =  true
        
        cell.subTitleLbl.isHidden = true
        cell.dateLbl.isHidden = true
        cell.timeLbl.isHidden = true
        cell.dateLblHeight.constant = 0
        cell.dateLblBtm.constant = 5
        cell.subTitleLblHeight.constant = 0
        cell.subTitleLblBtm.constant = 5
        cell.bookNowBtnHeight.constant = 0
        cell.bookNowBtnBtm.constant = 0
        cell.bookBtnHeight.constant = 0
        cell.bookBtnBtm.constant = 0
        //cell.btmDetailHeight.constant = 0
        cell.bookNowBtn.isHidden = true
        cell.bookBtnStackView.isHidden = true
        DispatchQueue.main.async {
            self.myScrollView.layoutIfNeeded()
            self.tableView.layoutIfNeeded()
            self.tableViewHeight.constant = self.tableView.contentSize.height
        }
        cell.selectionStyle = .none
        return cell
    }
}
@available(iOS 13.0, *)
extension BetaTourVC : UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.mobileTF {
            if(self.mobileTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: 9)
        }else if textField == self.nameTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.safelyLimitedTo(length: 50)
        }
        return true
    }
}

// MARK: Calender Delegate Methods
@available(iOS 13.0, *)
extension BetaTourVC:CalenderVCDelegate{
    func didTapAction(getDate:Date,whereType:Int){
        selectDate = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .dateR))"
        eventDateTF.text = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .ddMMyyyy))"
    }
}
