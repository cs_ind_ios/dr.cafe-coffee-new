//
//  BulkOrderDetailsVC.swift
//  drCafe
//
//  Created by Devbox on 31/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BulkOrderDetailsVC: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descOneLbl: UILabel!
    @IBOutlet weak var descTwoLbl: UILabel!
    @IBOutlet weak var descThreeLbl: UILabel!
    @IBOutlet weak var descFourLbl: UILabel!
    @IBOutlet weak var descFiveLbl: UILabel!
    @IBOutlet weak var descSixLbl: UILabel!
    @IBOutlet weak var contactMsgLbl: UILabel!
    @IBOutlet weak var bookNowBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk Order", value: "", table: nil))!
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BulkOrderTitleNName", value: "", table: nil))!
        descTwoLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BulkOrderDescTwo", value: "", table: nil))!
        descThreeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BulkOrderDescThree", value: "", table: nil))!
        descFourLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BulkOrderDescFour", value: "", table: nil))!
        descSixLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BulkOrderDescSix", value: "", table: nil))!
        contactMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk Order Contact Msg", value: "", table: nil))!
        bookNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk Order Now", value: "", table: nil))!, for: .normal)
        
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        bookNowBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Book Now Button
    @IBAction func bookNowBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = BulkOrderCreateVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "BulkOrderCreateVC") as! BulkOrderCreateVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Contact Button Action
    @IBAction func contactBtn_Tapped(_ sender: Any) {
        let mobile = "+966581570462"
        if let url = URL(string: "tel://\(mobile)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
