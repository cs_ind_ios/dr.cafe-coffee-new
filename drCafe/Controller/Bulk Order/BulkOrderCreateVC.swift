//
//  BulkOrderCreateVC.swift
//  drCafe
//
//  Created by Devbox on 31/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BulkOrderCreateVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: IBOutlet
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    
    @IBOutlet weak var companyNameTF: UITextField!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var cityNameTF: UITextField!
    @IBOutlet weak var countryNameTF: UITextField!
    @IBOutlet weak var commercialRegistrationNumTF: UITextField!
    @IBOutlet weak var vatNumTF: UITextField!
    @IBOutlet weak var noteTV: UITextView!
    
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var emailNameLbl: UILabel!
    @IBOutlet weak var phoneNumNameLbl: UILabel!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var commercialRegistrationNumNameLbl: UILabel!
    @IBOutlet weak var vatNumNameLbl: UILabel!
    @IBOutlet weak var yourMessageNameLbl: UILabel!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var countryCodeDropBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var countryCodeDropDownImg: UIImageView!
    
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    var countryCodesArray = [CountryCodeDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteTV.delegate = self
        phoneNumTF.delegate = self
        tabBarController?.tabBar.isHidden = true
        
        if SaveAddressClass.CountryCodesListArray.count == 0{
            self.getCountryCodesListService()
        }else{
            self.countryCodeLbl.text = "+966"
            self.selectCountryCode = 966
            self.selectCCMobileNumDigits = 9
            if SaveAddressClass.CountryCodesListArray.count == 1{
                countryCodeDropBtn.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "Arabic"{
                    countryCodeDropDownImg.isHidden = true
                }
            }else{
                countryCodeDropBtn.isHidden = false
                if AppDelegate.getDelegate().appLanguage == "Arabic"{
                    countryCodeDropDownImg.isHidden = false
                }
            }
        }
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk Order", value: "", table: nil))!
        companyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Company Name", value: "", table: nil))!
        emailNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email Name", value: "", table: nil))!
        phoneNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Phone Number", value: "", table: nil))!
        cityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "City Name", value: "", table: nil))!
        countryNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Country Name", value: "", table: nil))!
        commercialRegistrationNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Commercial Registration Number", value: "", table: nil))!
        vatNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT Number", value: "", table: nil))!
        yourMessageNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Message", value: "", table: nil))!
        submitBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Submit", value: "", table: nil))!, for: .normal)
        noteTV.textColor = .lightGray
        
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        submitBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Country Codes Get Service
    func getCountryCodesListService(){
        let dic:[String:Any] = [:]
        UserModuleServices.getCountryCodesService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                SaveAddressClass.CountryCodesListArray = data.Data!
                if data.Data!.count == 1{
                    self.countryCodeDropBtn.isHidden = true
                    if AppDelegate.getDelegate().appLanguage == "Arabic"{
                        self.countryCodeDropDownImg.isHidden = true
                    }
                }else{
                    self.countryCodeDropBtn.isHidden = false
                    if AppDelegate.getDelegate().appLanguage == "Arabic"{
                        self.countryCodeDropDownImg.isHidden = false
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    let saudiCC = data.Data!.filter({$0.MobileCode == 966})
                    if saudiCC.count > 0{
                        self.countryCodeLbl.text = "+966"
                        self.selectCountryCode = 966
                        self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
                    }else{
                        self.countryCodeLbl.text = "+966"
                        self.selectCountryCode = 966
                        self.selectCCMobileNumDigits = 9
                        self.selectCCMinLength = 9
                        self.selectCCMaxLength = 9
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Country Code Drop Down
    @IBAction func countryCodeDropBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = SaveAddressClass.CountryCodesListArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.center.x+12 , y: -15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: countryCodeDropDownImg.frame.midX - 9 , y: -15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 1
        obj.codeDropDownArray = SaveAddressClass.CountryCodesListArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Submit Button
    @IBAction func submitBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        var vatNum = ""
        var registrationNum = ""
        var yourMessage = ""
        var userId = 0
        if vatNumTF.text != nil || (vatNumTF.text != ""){
            vatNum = vatNumTF.text!
        }
        if commercialRegistrationNumTF.text != nil || (commercialRegistrationNumTF.text != ""){
            registrationNum = commercialRegistrationNumTF.text!
        }
        if noteTV.text != nil || (noteTV.text != "") || (noteTV.text! != (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Message", value: "", table: nil))!){
            yourMessage = noteTV.text!
        }
        if isUserLogIn() == true{
            userId = Int(UserDef.getUserId())!
        }
        let dic = ["CompanyName": "\(companyNameTF.text!)",
        "EmailId": "\(emailIDTF.text!)",
        "PhoneNo": "\(phoneNumTF.text!)",
        "City":" \(cityNameTF.text!)",
        "Country": "\(countryNameTF.text!)",
        "CommercialRegistrationNo": registrationNum,
        "VATNumber": vatNum,
        "RequestBy": 2,
        "UserId": userId,
        "Message": yourMessage, "CountryCode":"\(selectCountryCode!)"] as [String : Any]
        print(dic)
        OrderModuleServices.BulkOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToast(on: self, message: data.MessageEn)
                }else{
                    Alert.showToast(on: self, message: data.MessageAr)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Validations For textfields
    func validInputParams() -> Bool {
        if companyNameTF.text == nil || (companyNameTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CompanyNameAlert", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isCharCount(name: companyNameTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                return false
            }
        }
        if emailIDTF.text == nil || (emailIDTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isValidEmail(email: emailIDTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EmailValidation", value: "", table: nil))!)
                return false
            }
        }
        if phoneNumTF.text == nil || (phoneNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNumberValid(mobile: phoneNumTF.text!, numOfDifits: 9){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        if countryNameTF.text == nil || (countryNameTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CountryNameAlert", value: "", table: nil))!)
            return false
        }
        if cityNameTF.text == nil || (cityNameTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CityNameAlert", value: "", table: nil))!)
            return false
        }
        if commercialRegistrationNumTF.text != nil && (commercialRegistrationNumTF.text != "") && (commercialRegistrationNumTF.text!.count != 10){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter minimum and maximum 10 characters for Commercial Registration Number", value: "", table: nil))!)
            return false
        }
        if vatNumTF.text != nil && (vatNumTF.text != "") && (vatNumTF.text!.count != 15){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter minimum and maximum 15 characters for VAT Number", value: "", table: nil))!)
            return false
        }
        return true
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
@available(iOS 13.0, *)
extension BulkOrderCreateVC : UITextFieldDelegate{
    //MARK: TextField Delegate Methods
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.phoneNumTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") && updatedText.safelyLimitedTo(length: 9)
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
//MARK: TextView Delegate Methods
@available(iOS 13.0, *)
extension BulkOrderCreateVC:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textView.textInputMode?.primaryLanguage == "emoji") || text.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
//MARK: Country Code Selection Delegate
@available(iOS 13.0, *)
extension BulkOrderCreateVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        countryCodeLbl.text = "+\(ID)"
        selectCountryCode = Int(ID)
        let selectDic = SaveAddressClass.CountryCodesListArray.filter({$0.CountryNameEn == name})
        self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
        phoneNumTF.text = ""
    }
}
