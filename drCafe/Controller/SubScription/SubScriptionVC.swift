//
//  SubScriptionVC.swift
//  drCafe
//
//  Created by Devbox on 15/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SubScriptionVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: IBOutlet
    @IBOutlet weak var cartCountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var myOrdersView: UIView!
    @IBOutlet weak var howItWorksView: UIView!
    @IBOutlet weak var mySubscriptionView: UIView!
    @IBOutlet weak var myOrdersBtn: UIButton!
    @IBOutlet weak var mySubscriptionBtn: UIButton!
    @IBOutlet weak var howItWorksBtn: UIButton!
    @IBOutlet weak var categoryViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var makeNewSubscriptionNameLbl: UILabel!
    @IBOutlet weak var trackMyOrderNameLbl: UILabel!
    @IBOutlet weak var trackMyOrderViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var detailsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var BGView: UIView!
    
    @IBOutlet weak var makeNewSubscrptionView: UIView!
    @IBOutlet weak var dontHaveSubMessageView: UIView!
    @IBOutlet weak var dontHaveSubMessageLbl: UILabel!
    @IBOutlet weak var makeNewSubNameLbl: UILabel!
    
    @IBOutlet weak var currentSubScriptionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var currentSubScriptionViewTop: NSLayoutConstraint!
    @IBOutlet weak var currentSubScriptionNameLbl: UILabel!
    
    @IBOutlet weak var subscriptionTCBtn: UIButton!
    @IBOutlet weak var subscriptionTCBtnDotLbl: CustomLabel!
    @IBOutlet weak var subscriptionTCBtnViewHeight: NSLayoutConstraint!
    
    
    var mainArray = [MySubscriptionDetailsModel]()
    var shippingMethodsArray = [ShippingMethodDetailsModel]()
    var selectOrderId = 0
    var selectFrequency = 0
    var selectSubscriptionId = 0
    var selectDate = ""
    var selectSlotId = 0
    var firstShipDate = ""
    var firstSelectShipId = 0
    var shipTimeSlot = ""
    var shipTimeSlotDate = ""
    var maxDays = 0
    var orderStartDate = ""
    var selectFrequencyArray = [FrequencyOptionsModel]()
    var getShippingTime = ""
    var getSlotId = 0
    var currentDate : Date = Date()
    
    var myOrders = true
    var isLogin = false
    var isReschedule = false
    
    var howItWorksDetailsArray = [["Title":"Coffee Selection","Desc":"Access a handpicked selection of 20+ Coffees roasted around the country and personalized just for you.","Image":"CoffeeSelection"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailsTableView.delegate = self
        detailsTableView.dataSource = self
        
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!
        myOrdersBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Subscription", value: "", table: nil))!, for: .normal)
        mySubscriptionBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Subscription", value: "", table: nil))!, for: .normal)
        howItWorksBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "How It Works", value: "", table: nil))!, for: .normal)
        makeNewSubscriptionNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make A New Subscription", value: "", table: nil))!
        makeNewSubNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make A New Subscription", value: "", table: nil))!
        dontHaveSubMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You do not have any subscription, add one", value: "", table: nil))!
        currentSubScriptionNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Current Subscription", value: "", table: nil))!
        subscriptionTCBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Terms & Conditions", value: "", table: nil))!, for: .normal)
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        cartCountLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
        categoryViewWidth.constant = 300
        
        howItWorksDetailsArray = [["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Selection", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Selection Desc", value: "", table: nil))!,"Image":"Coffee Selection"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Exclusive Subscriber Saving", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Exclusive Subscriber Saving Desc", value: "", table: nil))!,"Image":"Exclusive subscriber"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Flexibility", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Flexibility Desc", value: "", table: nil))!,"Image":"Total Flexibility"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Fresh & Easy", value: "", table: nil))!,"Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Fresh & Easy Desc", value: "", table: nil))!,"Image":"Fresh & Easy"]]
        
        if isUserLogIn() == true{
            //categoryViewWidth.constant = 300
            ANLoader.showLoading("", disableUI: true)
            self.getMySubscriptionsService()
            detailsTableView.isHidden = false
            makeNewSubscrptionView.isHidden = false
            dontHaveSubMessageView.isHidden = true
        }else{
            //self.categoryViewWidth.constant = 135
            self.myOrdersView.isHidden = true
            self.mySubscriptionView.isHidden = false
            self.myOrders = false
            self.trackMyOrderViewHeight.constant = 0
            self.howItWorksView.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
            self.mySubscriptionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.subscriptionTCBtnViewHeight.constant = 43
            DispatchQueue.main.async {
                self.detailsTableView.reloadData()
            }
        }
        
        //Theme Color
        self.BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            subscriptionTCBtn.setTitleColor(.white, for: .normal)
            subscriptionTCBtnDotLbl.backgroundColor = .white
            dontHaveSubMessageLbl.textColor = .white
        }else{
            subscriptionTCBtn.setTitleColor(.darkGray, for: .normal)
            subscriptionTCBtnDotLbl.backgroundColor = .darkGray
            dontHaveSubMessageLbl.textColor = .black
        }
    }
    //MARK: Back Button
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isLogin == true{
            LoginVC.instance.whereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Cart Button
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.whereObj = 4
            obj.isSubscription = true
            obj.isRemove = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: My Orders Button
    @IBAction func myOrdersBtn_Tapped(_ sender: UIButton) {
        myOrders = true
        self.trackMyOrderViewHeight.constant = 0
        //detailsTableViewHeight.constant = 0
        myOrdersView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8470588235)
        howItWorksView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        DispatchQueue.main.async {
            if self.mainArray.count == 0{
                ANLoader.showLoading("", disableUI: true)
                self.getMySubscriptionsService()
            }else{
                self.myScrollView.layoutIfNeeded()
                self.detailsTableView.reloadData()
                self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
            }
        }
        self.currentSubScriptionViewTop.constant = 15
        self.currentSubScriptionViewHeight.constant = 50
        self.subscriptionTCBtnViewHeight.constant = 0
    }
    //MARK: how It Works
    @IBAction func howItWorksBtn_Tapped(_ sender: Any) {
        myOrders = false
        self.currentSubScriptionViewTop.constant = 0
        self.currentSubScriptionViewHeight.constant = 0
        self.trackMyOrderViewHeight.constant = 0
        howItWorksView.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
        myOrdersView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mySubscriptionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dontHaveSubMessageView.isHidden = true
        detailsTableView.isHidden = false
        makeNewSubscrptionView.isHidden = false
        self.subscriptionTCBtnViewHeight.constant = 43
        DispatchQueue.main.async {
            //self.detailsTableView.reloadData()
            self.myScrollView.layoutIfNeeded()
            self.detailsTableView.reloadData()
            self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
        }
    }
    //MARK: My Subscription
    @IBAction func mySubscriptionBtn_Tapped(_ sender: Any) {
        self.mySubscriptionView.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
        self.howItWorksView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.subscriptionTCBtnViewHeight.constant = 0
        if isUserLogIn() == true{
            detailsTableView.isHidden = true
            makeNewSubscrptionView.isHidden = true
            dontHaveSubMessageView.isHidden = false
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Make New Subscription
    @IBAction func makeNewSubscriptionBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        obj.whereObj = 4
        obj.sectionType = 2
        obj.storeID = 190
        obj.selectOrderType = 4
        obj.isSubscription = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Subscription Terms and Conditions
    @IBAction func subscriptionTCBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Terms & Conditions", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppSubscriptionTermsandCondition"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppSubscriptionTermsandCondition"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    //MARK: Get Order History Data
    func getMySubscriptionsService(){
        let dic:[String : Any] = ["userId":UserDef.getUserId(), "RequestBy":2]
        SubscriptionModuleServices.MySubscriptionServiceWithOutLoader(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                self.mainArray = data.Data!
                if data.Data!.count > 0{
                    self.myOrders = true
                    self.myOrdersView.isHidden = false
                    self.mySubscriptionView.isHidden = true
                    self.myOrdersView.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
                    self.mySubscriptionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.howItWorksView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.categoryViewWidth.constant = 300
                    self.currentSubScriptionViewTop.constant = 15
                    self.currentSubScriptionViewHeight.constant = 50
                    self.subscriptionTCBtnViewHeight.constant = 0
                    DispatchQueue.main.async {
                        //ANLoader.hide()
                        self.myScrollView.layoutIfNeeded()
                        self.detailsTableView.reloadData()
                        self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
                        //self.myOrdersBtn_Tapped(self.myOrdersBtn)
                        let indexPath = IndexPath(item: 0, section: 0)
                        self.detailsTableView.reloadRows(at: [indexPath], with: .none)
                        self.detailsTableView.reloadData()
                    }
                }else{
                    self.currentTimeGetService()
                    //self.categoryViewWidth.constant = 135
                    self.currentSubScriptionViewTop.constant = 0
                    self.currentSubScriptionViewHeight.constant = 0
                    self.myOrdersView.isHidden = true
                    self.mySubscriptionView.isHidden = false
                    self.myOrders = false
                    self.trackMyOrderViewHeight.constant = 0
                    self.howItWorksView.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
                    self.mySubscriptionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.subscriptionTCBtnViewHeight.constant = 43
                    DispatchQueue.main.async {
                        //self.detailsTableView.reloadData()
                        self.myScrollView.layoutIfNeeded()
                        self.detailsTableView.reloadData()
                        self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Current time Get Service
    @objc func currentTimeGetService(){
        let dic = ["RequestBy":2] as [String : Any]
        CartModuleServices.GetCurrentTimeService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    if data.Data != ""{
                        if data.Data.contains("."){
                            var time = data.Data
                            time = time.components(separatedBy: ".")[0]
                            let currentTime = DateConverts.convertStringToStringDates(inputDateStr: time, inputDateformatType: .dateTtime, outputDateformatType: .dateR)
                            self.currentDate = DateConverts.convertStringToDate(date: currentTime, dateformatType: .dateR)
                        }else{
                            let currentTime = DateConverts.convertStringToStringDates(inputDateStr: data.Data, inputDateformatType: .dateTtime, outputDateformatType: .dateR)
                            self.currentDate = DateConverts.convertStringToDate(date: currentTime, dateformatType: .dateR)
                        }
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Shipping Method
    func shippingMethodDetailsGetService(orderId:Int,shippingMethod:String){
        let dic:[String : Any] = ["OrderId": orderId, "RequestBy":2]
        CartModuleServices.ShippingMethodService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.shippingMethodsArray = data.Data!.filter({$0.DisplayNameEn == shippingMethod})
                if self.shippingMethodsArray.count > 0{
                    self.shippingMethodsArray[0].isSelect = true
                    self.slotIdSelect()
                }
                if self.shippingMethodsArray.count > 0{
                    if self.isReschedule == true{
                        let date = self.selectDate.components(separatedBy: ".")[0]
                        if self.getShippingTime != ""{
                            let weekDay = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
                            let fromTime = self.getShippingTime.components(separatedBy: " - ")[0]
                            let time = self.shippingMethodsArray[0].ShippingSlots![weekDay-1].Slot!.filter({$0.StartTime == "\(fromTime)"})
                            if time.count > 0{
                                self.getSlotId = time[0].Id
                            }
                        }
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TimeSlotVC") as! TimeSlotVC
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.timeSlotVCDelegate = self
                        obj.rescheduleTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                        obj.rescheduleSelectSlotId = self.getSlotId
                        obj.shipDetails = self.shippingMethodsArray
                        obj.shipTimeSlotDate = self.shipTimeSlotDate
                        obj.selectSlotId = self.selectSlotId
                        obj.maxDays = self.maxDays
                        obj.reschedule = true
                        obj.modalPresentationStyle = .overFullScreen
                        self.present(obj, animated: true, completion: nil)
                    }else{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "PauseVC") as! PauseVC
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.pauseVCDelegate = self
                        //let upcomingDate = self.shipTimeSlotDate.split(separator: ".")[0]
                        obj.minimumDate = DateConverts.convertStringToDate(date: "\(self.shipTimeSlotDate)", dateformatType: .date)
                        obj.maxDays = self.maxDays
                        obj.obj_where = 2
                        obj.shipDetails = self.shippingMethodsArray
                        obj.pauseFrequencyArray = self.selectFrequencyArray
                        obj.selectSlotId = self.selectSlotId
                        obj.modalPresentationStyle = .overFullScreen
                        self.present(obj, animated: true, completion: nil)
                    }
                }else{
                    print("No Data")
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func slotIdSelect(){
        if shippingMethodsArray.count == 1{
            var shipDate = ""
            var currentShipDate:Date = Date()
            if orderStartDate != ""{
                shipDate = orderStartDate.components(separatedBy: ".")[0]
                firstShipDate = orderStartDate.components(separatedBy: "T")[0]
                currentShipDate = DateConverts.convertStringToDate(date: orderStartDate.components(separatedBy: "T")[0], dateformatType: .dateR)
            }else{
                shipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: ".")[0]
                firstShipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0]
                currentShipDate = DateConverts.convertStringToDate(date: shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0], dateformatType: .dateR)
            }
            firstSelectShipId = shippingMethodsArray[0].Id
            var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
            var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
            if currentShipDate > currentDate{
                let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                if time.count > 0{
                    shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                    shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                    selectSlotId = time[0].Id
                    for i in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                        if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
                            shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                        }
                    }
                    print("Match")
                }else{
                    print("Not Match")
                    for i in 1...shippingMethodsArray[0].FutureDays{
                        nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                        let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                        if time.count > 0{
                            shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                            shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                            selectSlotId = time[0].Id
                            for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                }
                            }
//                            if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                            }
                            print("Match")
                            break
                        }else{
                            print("Not Match")
                        }
                    }
                }
            }else{
                let timeSlot = expectTimeSlot(presentDate: shipDate, index: 0)
                if timeSlot == "NotMatch"{
                    let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: 0)
                    if firstTimeSlot == "NotMatch"{
                        for i in 1...shippingMethodsArray[0].FutureDays{
                            nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                            let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                            if time.count > 0{
                                shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                selectSlotId = time[0].Id
                                for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                    if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                        shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                    }
                                }
//                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                                }
                                print("Match")
                                break
                            }else{
                                print("Not Match")
                            }
                        }
                    }else{
                        print("Match")
                    }
                }else{
                    print("Match")
                }
            }
        }
    }
    func expectTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        var status = ""
        for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
            let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
            let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
            if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: startTime, dateformatType: .dateNTimeA) as NSDate, endTime: DateConverts.convertStringToDate(date: endTime, dateformatType: .dateNTimeA) as NSDate) == true{
                if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                    shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                  shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                    selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                    status = "Match"
                    break
                }else{
                    status = "NotMatch"
                }
            }else{
                status = "NotMatch"
            }
        }
        print("Time Slot Status \(status)")
        return status
    }
    func selectFirstTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        let currentDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
        var status = ""
        let start = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![0].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        let end = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: start, dateformatType: .dateNTime) as NSDate, endTime: DateConverts.convertStringToDate(date: end, dateformatType: .dateNTime) as NSDate) == true || currentDate < start{
            for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
                let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
                let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                if status == "NotMatch"{
                    if currentDate < startTime && currentDate < endTime{
                        if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                            shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                            shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                            shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                            selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                            status =  "Match"
                            break
                        }else{
                            status =  "NotMatch"
                        }
                    }else{
                        status = "NotMatch"
                    }
                }else{
                    status = "NotMatch"
                }
            }
        }else{
            status = "NotMatch"
        }
        print("First Time Slot Status \(status)")
        return status
    }
    func isTimeStampCurrent(timeStamp:NSDate, startTime:NSDate, endTime:NSDate)->Bool{
        timeStamp.earlierDate(endTime as Date) == timeStamp as Date && timeStamp.laterDate(startTime as Date) == timeStamp as Date
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension SubScriptionVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if myOrders == true{
            return self.mainArray.count
        }else{
            return howItWorksDetailsArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if myOrders == true{
            return UITableView.automaticDimension
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if myOrders == true{
            self.detailsTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MySubscriptionTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MySubscriptionTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MySubscriptionTVCell", value: "", table: nil))!, for: indexPath) as! MySubscriptionTVCell
            
            cell.rescheduleBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reschedule", value: "", table: nil))!, for: .normal)
            cell.pauseBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pause", value: "", table: nil))!, for: .normal)
            cell.viewDetailsBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "View Details", value: "", table: nil))!, for: .normal)
            cell.startDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Start Date", value: "", table: nil))!
            cell.deliveryFrequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Frequency", value: "", table: nil))!
            cell.upcomingDeliveryDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upcoming Delivery", value: "", table: nil))!
            cell.itemsInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Items Info", value: "", table: nil))!
            cell.subNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription No", value: "", table: nil))!
            cell.deliveryAddressNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Address", value: "", table: nil))!
            cell.paymentsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payments", value: "", table: nil))!
            cell.changeCardBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Card", value: "", table: nil))!, for: .normal)
            cell.retryPaymentBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Retry Payment", value: "", table: nil))!, for: .normal)
            
            let dic = self.mainArray[indexPath.row]
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.deliveryFrequencyLbl.text = dic.FrequencyNameEn
            }else{
                cell.deliveryFrequencyLbl.text = dic.FrequencyNameAr
            }
            cell.deliveryAddressLbl.text = dic.DeliveryAddress
            cell.orderNumLbl.text = "#\(dic.InvoiceNo)"
            if dic.InitiateDate != ""{
                let date = dic.InitiateDate.split(separator: ".")[0]
                cell.startDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
            }else{
                cell.startDateLbl.text = ""
            }
            if dic.RunningOrderDate != ""{
                let upcomingDate = dic.RunningOrderDate.split(separator: ".")[0]
                cell.upcomingDeliveryDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(upcomingDate)", inputDateformatType: .dateTtime, outputDateformatType: .date)
            }else{
                cell.upcomingDeliveryDateLbl.text = ""
            }
            cell.itemDetailsArray = dic.ItemDetails!
            cell.do_table_refresh()
            //cell.itemsTableView.reloadData()
            cell.itemsTableView.layoutIfNeeded()
            cell.itemsTableViewHeight.constant = CGFloat(90 * dic.ItemDetails!.count)
            
            cell.topStatusMessageImg.image = UIImage(named: "Active")
            cell.topStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Active", value: "", table: nil))!
            
            //Payment Details
            cell.paymentsViewHeight.constant = 130
            //Image
            if dic.PaymentBrandIcon != nil{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.PaymentBrandIcon!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.paymentTypeImg.kf.setImage(with: url)
            }else{
                cell.paymentTypeImg.image = nil
            }
            if AppDelegate.getDelegate().appLanguage == "English"{
                if dic.PaymentDetails != nil{
                    if dic.PaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(dic.PaymentMethod) ****\(dic.PaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = dic.PaymentMethod
                    }
                }else{
                    cell.paymentTypeLbl.text = dic.PaymentMethod
                }
            }else{
                var paymentName = ""
                if dic.PaymentMethodAr != nil{
                    paymentName = dic.PaymentMethodAr!
                }else{
                    paymentName = dic.PaymentMethod
                }
                if dic.PaymentDetails != nil{
                    if dic.PaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(paymentName) ****\(dic.PaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = paymentName
                    }
                }else{
                    cell.paymentTypeLbl.text = paymentName
                }
            }
            
            cell.amountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(dic.NetTotal.withCommas())"
            
            if dic.IsSuspended == true{
                cell.pauseBtn.isEnabled = false
                cell.cancelBtnView.isHidden = true
                cell.cancelDescViewHeight.constant = 0
                cell.pauseBtn.isHidden = true
                cell.upcomingDeliveryDateLbl.isHidden = true
                cell.upcomingDeliveryDateNameLbl.isHidden = true
                cell.upcomingDeliveryDateLblHeight.constant = 0
                cell.upcomingDeliveryDateNameLblHeight.constant = 0
                if dic.RetryPayment! == 1{
                    cell.topStatusMessageImg.image = UIImage(named: "Failed")
                    cell.topStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Failed", value: "", table: nil))!
                    cell.retryPaymentBtn.isHidden = false
                    cell.changeCardBtn.isHidden = false
                }else{
                    cell.topStatusMessageImg.image = UIImage(named: "Canceled")
                    cell.topStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancelled", value: "", table: nil))!
                    cell.paymentsViewHeight.constant = 0
                }
            }else{
                cell.pauseBtn.isEnabled = true
                cell.cancelBtnView.isHidden = false
                cell.pauseBtn.isHidden = false
                cell.upcomingDeliveryDateLbl.isHidden = false
                cell.upcomingDeliveryDateNameLbl.isHidden = false
                cell.upcomingDeliveryDateLblHeight.constant = 22
                cell.upcomingDeliveryDateNameLblHeight.constant = 22
                cell.cancelDescViewHeight.constant = 0
                cell.retryPaymentBtn.isHidden = true
                cell.changeCardBtn.isHidden = false
            }
            cell.pauseBtnViewHeight.constant = 59
            
            if dic.OrderStatusEn == "Reject" || dic.OrderStatusEn == "Cancel" || dic.OrderStatusEn == "Upcoming" && dic.IsSuspended == true{
                cell.rescheduleBtn.isHidden = true
                cell.rescheduleBtnHeight.constant = 0
            }else if dic.OrderStatusEn == "Close"{
                cell.rescheduleBtn.isHidden = true
                cell.rescheduleBtnHeight.constant = 0
            }else{
                if dic.IsReschedule != nil{
                    if dic.IsReschedule! == false{
                        cell.rescheduleBtn.isHidden = false
                        cell.rescheduleBtnHeight.constant = 44
                    }else{
                        cell.rescheduleBtn.isHidden = true
                        cell.rescheduleBtnHeight.constant = 0
                    }
                }else{
                    cell.rescheduleBtn.isHidden = true
                    cell.rescheduleBtnHeight.constant = 0
                }
            }
            
            cell.rescheduleBtn.tag = indexPath.row
            cell.rescheduleBtn.addTarget(self, action: #selector(rescheduleBtn_Tapped(_:)), for: .touchUpInside)
            cell.pauseBtn.tag = indexPath.row
            cell.pauseBtn.addTarget(self, action: #selector(pauseBtn_Tapped(_:)), for: .touchUpInside)
            cell.cancelBtn.tag = indexPath.row
            cell.cancelBtn.addTarget(self, action: #selector(cancelBtn_Tapped(_:)), for: .touchUpInside)
            cell.viewDetailsBtn.tag = indexPath.row
            cell.viewDetailsBtn.addTarget(self, action: #selector(viewDetailsBtn_Tapped(_:)), for: .touchUpInside)
            cell.retryPaymentBtn.tag = indexPath.row
            cell.retryPaymentBtn.addTarget(self, action: #selector(retryPaymentBtn_Tapped(_:)), for: .touchUpInside)
            cell.changeCardBtn.tag = indexPath.row
            cell.changeCardBtn.addTarget(self, action: #selector(changeCardBtn_Tapped(_:)), for: .touchUpInside)
            
            if indexPath.row%2 == 0{
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.detailsTableView.layoutIfNeeded()
                self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
            }
            
            cell.selectionStyle = .none
            return cell
        }else{
            detailsTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubScriptionHowItWorksTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubScriptionHowItWorksTVCell", value: "", table: nil))!)
            let cell = detailsTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubScriptionHowItWorksTVCell", value: "", table: nil))!, for: indexPath) as! SubScriptionHowItWorksTVCell
            cell.nameLbl.text = howItWorksDetailsArray[indexPath.row]["Title"]
            cell.descLbl.text = howItWorksDetailsArray[indexPath.row]["Desc"]
            cell.img.image = UIImage(named: howItWorksDetailsArray[indexPath.row]["Image"]!)
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.detailsTableView.layoutIfNeeded()
                self.detailsTableViewHeight.constant = self.detailsTableView.contentSize.height
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    //MARK: Reschedule Button Action
    @objc func rescheduleBtn_Tapped(_ sender: UIButton){
        isReschedule = true
        selectOrderId = self.mainArray[sender.tag].SubOrderId
        selectFrequency = self.mainArray[sender.tag].Frequency
        selectSubscriptionId = self.mainArray[sender.tag].SubscriptionId
        selectDate = self.mainArray[sender.tag].RunningOrderDate
        getShippingTime = self.mainArray[sender.tag].SlotTiming
        if self.mainArray[sender.tag].RescheduleStartDate != nil{
            orderStartDate = self.mainArray[sender.tag].RescheduleStartDate!
        }
        maxDays = self.mainArray[sender.tag].RescheduleMaxDays
        shippingMethodDetailsGetService(orderId: self.mainArray[sender.tag].SubOrderId, shippingMethod: self.mainArray[sender.tag].ShippingMethodEn)
    }
    //MARK: Pause Button Action
    @objc func pauseBtn_Tapped(_ sender: UIButton){
        isReschedule = false
        selectOrderId = self.mainArray[sender.tag].OrderId
        selectFrequency = self.mainArray[sender.tag].Frequency
        selectSubscriptionId = self.mainArray[sender.tag].SubscriptionId
        selectDate = self.mainArray[sender.tag].RunningOrderDate
        if self.mainArray[sender.tag].PauseStartDate != nil{
            orderStartDate = self.mainArray[sender.tag].PauseStartDate!
        }
        maxDays = self.mainArray[sender.tag].PauseMaxDays
        if self.mainArray[sender.tag].FrequencyOptions != nil{
            selectFrequencyArray = self.mainArray[sender.tag].FrequencyOptions!
        }
        shippingMethodDetailsGetService(orderId: self.mainArray[sender.tag].OrderId, shippingMethod: self.mainArray[sender.tag].ShippingMethodEn)
    }
    //MARK: Cancel Button Action
    @objc func cancelBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionCancelVC") as! SubscriptionCancelVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.subscriptionID = self.mainArray[sender.tag].SubscriptionId
        obj.modalPresentationStyle = .overFullScreen
        obj.VCTitle = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!
        obj.subscriptionCancelVCDelegate = self
        self.present(obj, animated: true, completion: nil)
    }
    //MARK: View Details Button Action
    @objc func viewDetailsBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = SubscriptionTrackingVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionTrackingVC") as! SubscriptionTrackingVC
        obj.subscriptionID = self.mainArray[sender.tag].SubscriptionId
        obj.currentTime = currentDate
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Retry Payment Button Action
    @objc func retryPaymentBtn_Tapped(_ sender: UIButton){
        var registrationId = ""
        if self.mainArray[sender.tag].RegistrationId != nil{
            registrationId = self.mainArray[sender.tag].RegistrationId!
        }
        var invoiceNo = ""
        if self.mainArray[sender.tag].SubInvoiceNo != nil{
            invoiceNo = self.mainArray[sender.tag].SubInvoiceNo!
        }
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        let dic:[String:Any] = ["amount": self.mainArray[sender.tag].NetTotal, "merchantTransactionId": invoiceNo, "customerEmail":"\((userDic["Email"]! as! String))", "customerPhone":"\((userDic["Mobile"]! as! String))", "customerName":"\((userDic["FullName"]! as! String))","OrderId":self.mainArray[sender.tag].SubOrderId, "registrationId":registrationId, "UserId":UserDef.getUserId(), "SubscriptionId":self.mainArray[sender.tag].SubscriptionId,  "isVisaMaster":true]
        SubscriptionModuleServices.RetryService(dic: dic, success: { (data) in
            if(data.Status == false){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
            }else{
                DispatchQueue.main.async {
                    self.myOrders = true
                    ANLoader.showLoading("", disableUI: true)
                    self.getMySubscriptionsService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Change Card Button Action
    @objc func changeCardBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ManageCardsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
        obj.isSubscription = true
        obj.orderId = self.mainArray[sender.tag].SubOrderId
        obj.subscriptionId = self.mainArray[sender.tag].SubscriptionId
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if myOrders == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = SubscriptionTrackingVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionTrackingVC") as! SubscriptionTrackingVC
            obj.subscriptionID = self.mainArray[indexPath.row].SubscriptionId
            obj.currentTime = currentDate
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
//MARK: TimeSlotVCDelegate
@available(iOS 13.0, *)
extension SubScriptionVC:TimeSlotVCDelegate{
    func didTapAction(expectDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateDots))
        DispatchQueue.main.async {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionReschedulePopUpVC") as! SubscriptionReschedulePopUpVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.subscriptionId = self.selectSubscriptionId
            obj.orderId = self.selectOrderId
            obj.slotTime = "\(startTime) - \(endTime)"
            obj.slotId = SlotId
            obj.orderExpected = expectDate
            obj.subscriptionReschedulePopUpVCDelegate = self
            obj.modalPresentationStyle = .overFullScreen
            self.present(obj, animated: true, completion: nil)
        }
//        let dic:[String:Any] = ["ShippingTime": "\(startTime) - \(endTime)", "SlotId": SlotId, "StartDate": "\(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateR)) 00:00:00.000", "Frequency": selectFrequency, "OrderId": selectOrderId, "SubscriptionId": selectSubscriptionId, "Action": 5, "RequestBy": 2]
//        SubscriptionModuleServices.PauseOrderService(dic: dic, success: { (data) in
//            if(data.Status == false){
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                }else{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                }
//            }else{
//                Alert.showToastDown(on: self, message: data.MessageEn)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    self.getMySubscriptionsService()
//                }
//            }
//        }) { (error) in
//            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
//        }
    }
}
//MARK: Cancel Delegate
extension SubScriptionVC : SubscriptionCancelVCDelegate, SubscriptionPauseVCDelegate, SubscriptionReschedulePopUpVCDelegate{
    func didTapAction(status: String) {
        if status == "Done"{
            ANLoader.showLoading("", disableUI: true)
            self.getMySubscriptionsService()
        }
    }
}
// MARK: Calender Delegate Methods
extension SubScriptionVC:PauseVCDelegate{
    func didTapAction(getDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertStringToStringDates(inputDateStr: "\(getDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR))
        print("\(startTime) \(endTime)")
        let date = DateConverts.convertStringToStringDates(inputDateStr: "\(getDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR)
        DispatchQueue.main.async {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionPauseVC") as! SubscriptionPauseVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.subscriptionID = self.selectSubscriptionId
            obj.orderId = self.selectOrderId
            obj.frequency = self.selectFrequency
            obj.shippingTime = "\(startTime) - \(endTime)"
            obj.slotId = SlotId
            obj.startDate = "\(date) 00:00:00.000"
            obj.subscriptionPauseVCDelegate = self
            obj.modalPresentationStyle = .overFullScreen
            self.present(obj, animated: true, completion: nil)
        }
//        let dic:[String:Any] = ["ShippingTime": "\(startTime) - \(endTime)", "SlotId": SlotId, "StartDate": "\(date) 00:00:00.000", "Frequency": selectFrequency, "OrderId": selectOrderId, "SubscriptionId": selectSubscriptionId, "Action": 5, "RequestBy": 2]
//        SubscriptionModuleServices.PauseOrderService(dic: dic, success: { (data) in
//            if(data.Status == false){
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                }else{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                }
//            }else{
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    self.getMySubscriptionsService()
//                }
//            }
//        }) { (error) in
//            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
//        }
    }
}
