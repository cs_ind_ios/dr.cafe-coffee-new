//
//  SubscriptionReschedulePopUpVC.swift
//  drCafe
//
//  Created by Devbox on 15/12/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

protocol SubscriptionReschedulePopUpVCDelegate {
    func didTapAction(status:String)
}

class SubscriptionReschedulePopUpVC: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var onlyThisOrderBtn: UIButton!
    @IBOutlet weak var allOrderBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var slotTime = ""
    var slotId = 0
    var orderExpected:Date!
    var orderId = 0
    var subscriptionId = 0
    var subscriptionReschedulePopUpVCDelegate:SubscriptionReschedulePopUpVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reschedule which order.", value: "", table: nil))!
        onlyThisOrderBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Only this order", value: "", table: nil))!, for: .normal)
        allOrderBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "All future order", value: "", table: nil))!, for: .normal)
        cancelBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, for: .normal)
        
    }
    //MARK: Cancel Button
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Only this Order Button
    @IBAction func onlyThisOrderBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["SlotTime": slotTime, "SlotId": slotId, "OrderExpected": "\(DateConverts.convertDateToString(date: orderExpected, dateformatType: .dateR)) 00:00:00.000", "RescheduleType": 0, "Orderid": orderId, "SubscriptionId": subscriptionId, "RequestBy": 2]
        SubscriptionModuleServices.RescheduleOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.subscriptionReschedulePopUpVCDelegate.didTapAction(status: "Done")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Orders Button
    @IBAction func allOrderBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["SlotTime": slotTime, "SlotId": slotId, "OrderExpected": "\(DateConverts.convertDateToString(date: orderExpected, dateformatType: .dateR)) 00:00:00.000", "RescheduleType": 1, "Orderid": orderId, "SubscriptionId": subscriptionId, "RequestBy": 2]
        SubscriptionModuleServices.RescheduleOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.subscriptionReschedulePopUpVCDelegate.didTapAction(status: "Done")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
