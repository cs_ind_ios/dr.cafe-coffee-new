//
//  SubScriptionHowItWorksTVCell.swift
//  drCafe
//
//  Created by Devbox on 15/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SubScriptionHowItWorksTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
