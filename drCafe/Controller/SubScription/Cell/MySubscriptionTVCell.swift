//
//  MySubscriptionTVCell.swift
//  drCafe
//
//  Created by Devbox on 15/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class MySubscriptionTVCell: UITableViewCell {
    
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var orderNumLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var deliveryAddressLbl: UILabel!
    @IBOutlet weak var deliveryFrequencyLbl: UILabel!
    @IBOutlet weak var upcomingDeliveryDateLbl: UILabel!
    @IBOutlet weak var upcomingDeliveryDateNameLbl: UILabel!
    @IBOutlet weak var itemDetailsLbl: UILabel!
    @IBOutlet weak var rescheduleBtn: UIButton!
    @IBOutlet weak var rescheduleBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var cancelBtnView: UIView!
    @IBOutlet weak var viewDetailsBtn: UIButton!
    @IBOutlet weak var pauseBtnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cancelDescViewHeight: NSLayoutConstraint!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var itemsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upcomingDeliveryDateLblHeight: NSLayoutConstraint!
    @IBOutlet weak var upcomingDeliveryDateNameLblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var subCancelMessageLbl: UILabel!
    @IBOutlet weak var subCancelMessageImg: UIImageView!
    
    @IBOutlet weak var startDateNameLbl: UILabel!
    @IBOutlet weak var deliveryFrequencyNameLbl: UILabel!
    @IBOutlet weak var subNumNameLbl: UILabel!
    @IBOutlet weak var deliveryAddressNameLbl: UILabel!
    @IBOutlet weak var itemsInfoNameLbl: UILabel!
    
    @IBOutlet weak var topStatusMessageLbl: UILabel!
    @IBOutlet weak var topStatusMessageImg: UIImageView!
    
    @IBOutlet weak var paymentsNameLbl: UILabel!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var paymentTypeImg: UIImageView!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var retryPaymentBtn: UIButton!
    @IBOutlet weak var changeCardBtn: UIButton!
    @IBOutlet weak var paymentsViewHeight: NSLayoutConstraint!
    
    var itemDetailsArray = [MySubscriptionItemsModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        itemsTableView.delegate = self
        itemsTableView.dataSource = self

        // Configure the view for the selected state
    }
    func do_table_refresh()
    {
        DispatchQueue.main.async {
            self.itemsTableView.reloadData()
            return
        }
    }
}
extension MySubscriptionTVCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, value: "", table: nil))!) as! OrderItemsTVCell
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.itemNameLbl.text = itemDetailsArray[indexPath.row].ItemNameEn
        }else{
            cell.itemNameLbl.text = itemDetailsArray[indexPath.row].ItemNameAr
        }
        cell.itemQuantityLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quantity", value: "", table: nil))!): \(self.itemDetailsArray[indexPath.row].Qty)"
        cell.itemPriceLbl.text = ""
        cell.BGView.backgroundColor = .clear

        cell.grinderViewHeight.constant = 0
        cell.additionalsLbl.text = ""
        cell.additionalsViewHeight.constant = 0
        
        //Item Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(self.itemDetailsArray[indexPath.row].Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.itemImg.kf.setImage(with: url)
        
        DispatchQueue.main.async {
            //cell.layoutIfNeeded()
            self.itemsTableView.layoutIfNeeded()
            self.itemsTableViewHeight.constant = CGFloat(90 * self.itemDetailsArray.count)
        }
        cell.selectionStyle = .none
        return cell
    }
}
