//
//  SubscriptionHistoryTVCell.swift
//  drCafe
//
//  Created by Devbox on 03/02/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

protocol SubscriptionHistoryTVCellDelegate: AnyObject {
    func subscriptionButtonsTapped(cell: SubscriptionHistoryTVCell, invoice:Bool, rate:Bool, retryPayment:Bool, changeCard:Bool, tableView: UITableView)
}

class SubscriptionHistoryTVCell: UITableViewCell {
    
    @IBOutlet weak var invoiceNumLbl: UILabel!
    @IBOutlet weak var favouriteNameLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountCurrencyLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var orderTimeLbl: UILabel!
    @IBOutlet weak var favourireNameViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rattingView: FloatRatingView!
    @IBOutlet weak var rateBtnView: CustomView!
    @IBOutlet weak var rateMainView: CustomView!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var rateOrderNameLbl: UILabel!
    
    @IBOutlet weak var subscriptionImgWidth: NSLayoutConstraint!
    @IBOutlet weak var subscriptionImgLeading: NSLayoutConstraint!
    @IBOutlet weak var subscriptionImg: UIImageView!
    @IBOutlet weak var invoiceSelectBtn: UIButton!
    
    @IBOutlet weak var retryPaymentBtn: UIButton!
    @IBOutlet weak var changeCardBtn: UIButton!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var paymentTypeImg: UIImageView!

    weak var delegate: SubscriptionHistoryTVCellDelegate?
    var tableView:UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func invoiceBtn_Tapped(_ sender: Any) {
        delegate?.subscriptionButtonsTapped(cell: self, invoice: true, rate: false, retryPayment: false, changeCard: false, tableView: tableView)
    }
    @IBAction func rateBtn_Tapped(_ sender: Any) {
        delegate?.subscriptionButtonsTapped(cell: self, invoice: false, rate: true, retryPayment: false, changeCard: false, tableView: tableView)
    }
    @IBAction func retryPaymentBtn_Tapped(_ sender: Any) {
        delegate?.subscriptionButtonsTapped(cell: self, invoice: false, rate: false, retryPayment: true, changeCard: false, tableView: tableView)
    }
    @IBAction func changeCardBtn_Tapped(_ sender: Any) {
        delegate?.subscriptionButtonsTapped(cell: self, invoice: false, rate: false, retryPayment: false, changeCard: true, tableView: tableView)
    }
}
