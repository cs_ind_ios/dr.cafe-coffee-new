//
//  SubscriptionTrackingVC.swift
//  drCafe
//
//  Created by Devbox on 23/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SubscriptionTrackingVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var navigationBGView: UIView!
    @IBOutlet weak var emptyLoadView: UIView!
    
    @IBOutlet weak var estimateTimeView: UIView!
    @IBOutlet weak var estimateDeliveryTimeLbl: UILabel!
    @IBOutlet weak var estimateDeliveryDateLbl: UILabel!
    @IBOutlet weak var estimateDeliveryTimeNameLbl: UILabel!
    @IBOutlet weak var orderNumNameLbl: UILabel!
    @IBOutlet weak var orderNumLbl: UILabel!
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var shareOrderBtn: UIButton!
    @IBOutlet weak var callNameLbl: UILabel!
    @IBOutlet weak var helpNameLbl: UILabel!
    @IBOutlet weak var shareNameLbl: UILabel!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var shareOrderView: UIView!
    @IBOutlet weak var rescheduleView: UIView!
    @IBOutlet weak var rescheduleBtn: UIButton!
    @IBOutlet weak var rescheduleNameLbl: UILabel!
    
    @IBOutlet weak var upcomingMsgLbl: UILabel!
    @IBOutlet weak var upcomingMsgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upcomingMsgViewBtm: NSLayoutConstraint!
    
    @IBOutlet weak var trackingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trackingView: UIView!
    
    @IBOutlet weak var orderConfirmedView: UIView!
    @IBOutlet weak var orderConfirmedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderConfirmedImgBGView: UIView!
    @IBOutlet weak var orderConfirmedImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var OrderConfirmedLbl: UILabel!
    @IBOutlet weak var OrderConfirmedDescLbl: UILabel!
    @IBOutlet weak var orderConfirmedTimeLbl: UILabel!
    @IBOutlet weak var orderConfirmedLineLbl: UILabel!
    @IBOutlet weak var orderConfirmedLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var orderConfirmedSepLineLbl: UILabel!
    @IBOutlet weak var orderConfirmedImage: UIImageView!
    
    @IBOutlet weak var acceptedOrderView: UIView!
    @IBOutlet weak var acceptedOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var acceptedOrderImgBGView: UIView!
    @IBOutlet weak var acceptedOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var acceptedOrderNameLbl: UILabel!
    @IBOutlet weak var acceptedOrderDescLbl: UILabel!
    @IBOutlet weak var acceptedOrderTimeLbl: UILabel!
    @IBOutlet weak var acceptedOrderLineLbl: UILabel!
    @IBOutlet weak var acceptedOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var acceptedOrderSepLineLbl: UILabel!
    @IBOutlet weak var acceptedOrderImage: UIImageView!
    
    @IBOutlet weak var readyOrderView: UIView!
    @IBOutlet weak var readyOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderImgBGView: UIView!
    @IBOutlet weak var readyOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderNameLbl: UILabel!
    @IBOutlet weak var readyOrderDescLbl: UILabel!
    @IBOutlet weak var readyOrderTimeLbl: UILabel!
    @IBOutlet weak var readyOrderLineLbl: UILabel!
    @IBOutlet weak var readyOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var readyOrderSepLineLbl: UILabel!
    @IBOutlet weak var readyOrderImage: UIImageView!
    
    @IBOutlet weak var onTheWayOrderView: UIView!
    @IBOutlet weak var onTheWayOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var onTheWayOrderImgBGView: UIView!
    @IBOutlet weak var onTheWayOrderImgBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var onTheWayOrderNameLbl: UILabel!
    @IBOutlet weak var onTheWayOrderDescLbl: UILabel!
    @IBOutlet weak var onTheWayOrderTimeLbl: UILabel!
    @IBOutlet weak var onTheWayOrderLineLbl: UILabel!
    @IBOutlet weak var onTheWayOrderLineLblHeight: NSLayoutConstraint!
    @IBOutlet weak var onTheWayOrderSepLineLbl: UILabel!
    @IBOutlet weak var onTheWayOrderImage: UIImageView!
    
    @IBOutlet weak var deliveredOrderView: UIView!
    @IBOutlet weak var deliveredOrderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveredOrderImgBGView: UIView!
    @IBOutlet weak var deliveredOrderImgBGHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveredOrderNameLbl: UILabel!
    @IBOutlet weak var deliveredOrderDescLbl: UILabel!
    @IBOutlet weak var deliveredOrderTimeLbl: UILabel!
    @IBOutlet weak var deliveredOrderImage: UIImageView!
    
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var orderDetailsNameLbl: UILabel!
    @IBOutlet weak var orderDetailsDropDownBtn: UIButton!
    @IBOutlet weak var orderItemsTableView: UITableView!
    @IBOutlet weak var orderItemsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderDetailsMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderDetailsMainViewBtm: NSLayoutConstraint!
    
    @IBOutlet weak var billAmountDropDownBtn: UIButton!
    @IBOutlet weak var priceDetailsView: UIView!
    @IBOutlet weak var priceDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var priceDetailsViewBtm: NSLayoutConstraint!
    @IBOutlet weak var billAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountNameLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountNameLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLbl: UILabel!
    @IBOutlet weak var deliveryChargesLbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var walletDiscountNameLbl: UILabel!
    @IBOutlet weak var walletDiscountHeight: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountBottom: NSLayoutConstraint!
    @IBOutlet weak var walletDiscountLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var netAmountNameLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryChargesNameLblBottom: NSLayoutConstraint!
    
    @IBOutlet weak var rejectedOrdersNameLbl: UILabel!
    @IBOutlet weak var rejectedOrdersTableView: UITableView!
    @IBOutlet weak var rejectedOrdersMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var deliveredNameLbl: UILabel!
    @IBOutlet weak var deliveredNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var orderHistoryTableView: UITableView!
    @IBOutlet weak var orderHistoryTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var noRecordsFoundView: UIView!
    
    @IBOutlet weak var deliveryMainView: UIView!
    @IBOutlet weak var deliveryNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var deliveryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var deliveryViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var DFMainView: UIView!
    @IBOutlet weak var deliveryFrequencyNameLbl: UILabel!
    @IBOutlet weak var frequencyNameLbl: UILabel!
    @IBOutlet weak var DFViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DFViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var estimatedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var estimatedViewBtm: NSLayoutConstraint!
    @IBOutlet weak var trackingViewBtm: NSLayoutConstraint!
    
    @IBOutlet weak var retryPaymentBtn: UIButton!
    @IBOutlet weak var changeCardBtn: UIButton!
    @IBOutlet weak var retryPaymentViewHeight: NSLayoutConstraint!

    var selectOrderId = 0
    var selectFrequency = 0
    var selectSubscriptionId = 0
    var selectDate = ""
    var selectSlotId = 0
    var firstShipDate = ""
    var firstSelectShipId = 0
    var shipTimeSlot = ""
    var shipTimeSlotDate = ""
    var shippingMethodsArray = [ShippingMethodDetailsModel]()
    var getShippingTime = ""
    var getSlotId = 0
    var currentDate = ""
    var orderStartDate = ""
    var currentTime : Date = Date()
    
    var subscriptionID = 0
    var pageNumber = 1
    var totalRows:Int = 0
    private let refreshControl = UIRefreshControl()
    var orderDetailsArray = [SubscriptionTrackDetailsModel]()
    var orderHistoryDetailsArray = [SubscriptionHistoryDetailsModel]()
    var rejectedOrderDetailsArray = [SubscriptionHistoryDetailsModel]()
    var tableViewHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        orderHistoryTableView.delegate = self
        orderHistoryTableView.dataSource = self
        orderItemsTableView.delegate = self
        orderItemsTableView.dataSource = self
        rejectedOrdersTableView.delegate = self
        rejectedOrdersTableView.dataSource = self
        
        emptyLoadView.isHidden = false
        
        getOrderDetailsService()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            SaveAddressClass.getSubscriptionOrderHistoryArray.removeAll()
            self.getOrderHistoryService()
        }
        
        orderHistoryTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
    }
    //MARK: View will Appear
    override func viewWillAppear(_ animated: Bool) {
        //Theme Color
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            deliveredNameLbl.textColor = .white
            rejectedOrdersNameLbl.textColor = .white
        } else {
            deliveredNameLbl.textColor = .black
            rejectedOrdersNameLbl.textColor = .black
        }
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")

        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Details", value: "", table: nil))!
        self.callNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Call", value: "", table: nil))!
        self.helpNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Help", value: "", table: nil))!
        self.shareNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share", value: "", table: nil))!
        self.orderDetailsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Details", value: "", table: nil))!
        self.billAmountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bill Amount", value: "", table: nil))!
        self.totalAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) :"
        self.deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
        self.discountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
        self.netAmountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Amount", value: "", table: nil))!) :"
        self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
        self.walletDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE PAY", value: "", table: nil))!) :"
        self.orderNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Number", value: "", table: nil))!
        self.retryPaymentBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Retry", value: "", table: nil))!, for: .normal)
        self.changeCardBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Card", value: "", table: nil))!, for: .normal)
        self.deliveredNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered Orders", value: "", table: nil))!
        self.deliveryNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
        self.deliveryFrequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Frequency", value: "", table: nil))!
        self.rejectedOrdersNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rejected Orders", value: "", table: nil))!
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        self.getOrderHistoryService()
    }
    //MARK: Get Order Details Service
    func getOrderDetailsService(){
        let dic:[String:Any] = ["SubscriptionId" : subscriptionID, "RequestBy":2]
        SubscriptionModuleServices.SubscriptionTrackService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.orderDetailsArray = [data.Data!]
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.allDetails()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Get Order History Service
    func getOrderHistoryService(){
        let dic:[String:Any] = ["SubscriptionId" : subscriptionID, "PageNo" : pageNumber, "RequestBy": 2, "PageSize":10, "IsSubscription":true]
        SubscriptionModuleServices.SubscriptionHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.emptyLoadView.isHidden = true
                print(data.Orders!)
                if self.pageNumber == 1 {
                    SaveAddressClass.getSubscriptionOrderHistoryArray.removeAll()
                }
                if data.Orders!.count > 0{
                    for i in 0...data.Orders!.count - 1{
                        SaveAddressClass.getSubscriptionOrderHistoryArray.append(data.Orders![i])
                    }
                }
                self.totalRows = data.TotalOrders
                self.orderHistoryDetailsArray.removeAll()
                self.orderHistoryDetailsArray = SaveAddressClass.getSubscriptionOrderHistoryArray
                
                if data.ReJectedOrders != nil{
                    self.rejectedOrderDetailsArray = data.ReJectedOrders!
                }
                
//                self.orderHistoryDetailsArray.removeAll()
//                self.orderHistoryDetailsArray = SaveAddressClass.getSubscriptionOrderHistoryArray.filter({$0.OrderStatus != "Reject"})
//                self.rejectedOrderDetailsArray.removeAll()
//                self.rejectedOrderDetailsArray = SaveAddressClass.getSubscriptionOrderHistoryArray.filter({$0.OrderStatus == "Reject"})
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    if self.orderHistoryDetailsArray.count > 0{
                        self.deliveredNameLblHeight.constant = 22
                        self.orderHistoryTableView.reloadData()
                    }else{
                        self.deliveredNameLblHeight.constant = 0
                        self.orderHistoryTableViewHeight.constant = 0
                    }
                    if self.rejectedOrderDetailsArray.count > 0{
                        self.rejectedOrdersMainViewHeight.constant = 150
                        self.rejectedOrdersTableView.reloadData()
                    }else{
                        self.rejectedOrdersMainViewHeight.constant = 0
                    }
                    self.refreshControl.endRefreshing()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Details
    func allDetails(){
        if self.orderDetailsArray[0].OrderDetails!.count == 0{
            self.noRecordsFoundView.isHidden = false
        }else{
            self.noRecordsFoundView.isHidden = true
        }
        
        if orderDetailsArray[0].OrderDetails!.count > 0{
            var expectDate = ""
            if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                if orderDetailsArray[0].OrderDetails![0].ExpectedDate != ""{
                    expectDate = orderDetailsArray[0].OrderDetails![0].ExpectedDate.components(separatedBy: "T")[0]
                    let shipTime = orderDetailsArray[0].OrderDetails![0].ShippingTime.components(separatedBy: "-")
                    if shipTime.count > 0 && orderDetailsArray[0].OrderDetails![0].ShippingTime != ""{
                        estimateDeliveryTimeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                        estimateDeliveryDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                    }else{
                        estimateDeliveryTimeLbl.text = ""
                        estimateDeliveryDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date)
                    }
                }else{
                    estimateDeliveryTimeLbl.text = ""
                    estimateDeliveryDateLbl.text = ""
                }
            }else{
                if orderDetailsArray[0].OrderDetails![0].ExpectedDate != ""{
                    expectDate = orderDetailsArray[0].OrderDetails![0].ExpectedDate.components(separatedBy: ".")[0]
                    estimateDeliveryTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                    estimateDeliveryDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                }else{
                    estimateDeliveryTimeLbl.text = ""
                    estimateDeliveryDateLbl.text = ""
                }
            }
            
            orderNumLbl.text = orderDetailsArray[0].OrderDetails![0].Invoice
            estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Delivery", value: "", table: nil))!
            if orderDetailsArray[0].OrderDetails![0].OrderStatus == "Reject" || orderDetailsArray[0].OrderDetails![0].OrderStatus == "Cancel" || orderDetailsArray[0].OrderDetails![0].OrderStatus == "Upcoming" && orderDetailsArray[0].OrderDetails![0].IsSuspended == 1{
                DispatchQueue.main.async {
                    //self.estimatedViewHeight.constant = 0
                    //self.estimatedViewBtm.constant = 0
                    self.trackingViewHeight.constant = 0
                    self.trackingViewBtm.constant = 0
                    self.DFViewHeightConstraint.constant = 0
                    self.DFViewBottomConstraint.constant = 0
                    self.priceDetailsViewHeight.constant = 0
                    self.priceDetailsViewBtm.constant = 0
                    self.orderDetailsMainViewHeight.constant = 0
                    self.orderDetailsMainViewBtm.constant = 0
                    self.deliveryViewHeightConstraint.constant = 0
                    self.deliveryViewBottomConstraint.constant = 0
                    self.billAmountNameLbl.isHidden = true
                    self.billAmountDropDownBtn.isHidden = true
                    self.priceDetailsView.isHidden = true
                    
//                    self.upcomingMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All orders delivered successfully.", value: "", table: nil))!
//                    self.upcomingMsgViewHeight.constant = 40
//                    self.upcomingMsgViewBtm.constant = 10
                    self.upcomingMsgLbl.text = ""
                    self.upcomingMsgViewHeight.constant = 0
                    self.upcomingMsgViewBtm.constant = 0
                }
                
            }else if orderDetailsArray[0].OrderDetails![0].OrderStatus == "Close"{
                //self.estimatedViewHeight.constant = 0
                //self.estimatedViewBtm.constant = 0
                self.trackingViewHeight.constant = 0
                self.trackingViewBtm.constant = 0
                self.DFViewHeightConstraint.constant = 0
                self.DFViewBottomConstraint.constant = 0
                self.priceDetailsViewHeight.constant = 0
                self.priceDetailsViewBtm.constant = 0
                self.orderDetailsMainViewHeight.constant = 0
                self.orderDetailsMainViewBtm.constant = 0
                self.deliveryViewHeightConstraint.constant = 0
                self.deliveryViewBottomConstraint.constant = 0
                self.billAmountNameLbl.isHidden = true
                self.billAmountDropDownBtn.isHidden = true
                self.priceDetailsView.isHidden = true
                
//                upcomingMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All orders delivered successfully.", value: "", table: nil))!
//                upcomingMsgViewHeight.constant = 40
//                upcomingMsgViewBtm.constant = 10
                upcomingMsgLbl.text = ""
                upcomingMsgViewHeight.constant = 0
                upcomingMsgViewBtm.constant = 0
            }else{
                if orderDetailsArray[0].OrderDetails![0].OrderStatus == "Upcoming"{
                    if orderDetailsArray[0].OrderDetails![0].InitiatedDate != ""{
                        let date = orderDetailsArray[0].OrderDetails![0].InitiatedDate.components(separatedBy: ".")[0]
                        upcomingMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order will be initiated on", value: "", table: nil))!) \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date))"
                    }else{
                        upcomingMsgLbl.text = ""
                    }
                    upcomingMsgViewHeight.constant = 40
                    upcomingMsgViewBtm.constant = 10
                }else{
                    upcomingMsgLbl.text = ""
                    upcomingMsgViewHeight.constant = 0
                    upcomingMsgViewBtm.constant = 0
                }
                
                self.orderItemsTableView.reloadData()
                addressLbl.text = orderDetailsArray[0].OrderDetails![0].CAddress
                
                if orderDetailsArray[0].OrderDetails![0].IsReschedule != nil{
                    if orderDetailsArray[0].OrderDetails![0].IsReschedule == false{
                        self.rescheduleView.isHidden = false
                    }else{
                        self.rescheduleView.isHidden = true
                    }
                }else{
                    self.rescheduleView.isHidden = true
                }
                
                if orderDetailsArray[0].OrderDetails![0].OrderStatus == "Cancel" || orderDetailsArray[0].OrderDetails![0].OrderStatus == "Reject"{
                    callView.isHidden = true
                }else{
                    callView.isHidden = false
                }
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    frequencyNameLbl.text = orderDetailsArray[0].OrderDetails![0].FrequencyNameEn
                }else{
                    frequencyNameLbl.text = orderDetailsArray[0].OrderDetails![0].FrequencyNameAr
                }
                
                OrderConfirmedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ORDER", value: "", table: nil))!
                let newOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 2})
                if newOrderArray.count > 0{
                    if newOrderArray[0].TrackingTime != ""{
                        if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                            let date = newOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            orderConfirmedTimeLbl.attributedText = time
                        }else{
                            let date = newOrderArray[0].TrackingTime.split(separator: ".")[0]
                            orderConfirmedTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        orderConfirmedTimeLbl.text = ""
                    }
                    orderConfirmedImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                    OrderConfirmedDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is placed", value: "", table: nil))!
                }else{
                    orderConfirmedImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                    orderConfirmedTimeLbl.text = ""
                    OrderConfirmedDescLbl.text = ""
                }
                
                var cancelOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 4})
                if cancelOrderArray.count == 0{
                    cancelOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 5})
                }
                
                acceptedOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ACCEPTED", value: "", table: nil))!
                let acceptOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 6})
                if acceptOrderArray.count > 0{
                    if acceptOrderArray[0].TrackingTime != ""{
                        if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                            let date = acceptOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            acceptedOrderTimeLbl.attributedText = time
                        }else{
                            let date = acceptOrderArray[0].TrackingTime.split(separator: ".")[0]
                            acceptedOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        acceptedOrderTimeLbl.text = ""
                    }
                    acceptedOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                    acceptedOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is accepted", value: "", table: nil))!
                }else{
                    acceptedOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                    acceptedOrderTimeLbl.text = ""
                    acceptedOrderDescLbl.text = ""
                }
                
                readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "READY", value: "", table: nil))!
                let readyOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 7})
                if readyOrderArray.count > 0{
                    if readyOrderArray[0].TrackingTime != ""{
                        if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                            let date = readyOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            readyOrderTimeLbl.attributedText = time
                        }else{
                            let date = readyOrderArray[0].TrackingTime.split(separator: ".")[0]
                            readyOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        readyOrderTimeLbl.text = ""
                    }
                    readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                    readyOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is ready, the driver has picked the order", value: "", table: nil))!
                }else{
                    readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                    readyOrderTimeLbl.text = ""
                    readyOrderDescLbl.text = ""
                }
                
                let onTheWayOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 9})
                if onTheWayOrderArray.count > 0{
                    if onTheWayOrderArray[0].TrackingTime != ""{
                        if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                            let date = onTheWayOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            onTheWayOrderTimeLbl.attributedText = time
                        }else{
                            let date = onTheWayOrderArray[0].TrackingTime.split(separator: ".")[0]
                            onTheWayOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        onTheWayOrderTimeLbl.text = ""
                    }
                    onTheWayOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                    onTheWayOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is on the way, will be delivered soon", value: "", table: nil))!
                }else{
                    onTheWayOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                    onTheWayOrderTimeLbl.text = ""
                    onTheWayOrderDescLbl.text = ""
                }
                
                deliveredOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DELIVERED", value: "", table: nil))!
                let deliveredOrderArray = orderDetailsArray[0].OrderDetails![0].Tracking!.filter({$0.OrderStatusId == 10})
                if deliveredOrderArray.count > 0{
                    if deliveredOrderArray[0].TrackingTime != ""{
                        if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                            let date = deliveredOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                            let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                            deliveredOrderTimeLbl.attributedText = time
                        }else{
                            let date = deliveredOrderArray[0].TrackingTime.split(separator: ".")[0]
                            deliveredOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                        }
                    }else{
                        deliveredOrderTimeLbl.text = ""
                    }
                    deliveredOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                    deliveredOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been delivered", value: "", table: nil))!
                }else{
                    deliveredOrderImgBGView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.9019607843, blue: 0.9058823529, alpha: 1)
                    deliveredOrderTimeLbl.text = ""
                    deliveredOrderDescLbl.text = ""
                }
                
                if cancelOrderArray.count > 0{
                    if acceptOrderArray.count == 0{
                        if cancelOrderArray[0].TrackingTime != ""{
                            if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                                let date = cancelOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                                let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                                acceptedOrderTimeLbl.attributedText = time
                            }else{
                                let date = cancelOrderArray[0].TrackingTime.split(separator: ".")[0]
                                acceptedOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                            }
                        }else{
                            acceptedOrderTimeLbl.text = ""
                        }
                        
                        acceptedOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                        acceptedOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                        acceptedOrderImage.image = UIImage(named: "track_order_cancel")
                        acceptedOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                        readyOrderView.isHidden = true
                        readyOrderImgBGView.isHidden = true
                        onTheWayOrderView.isHidden = true
                        onTheWayOrderImgBGView.isHidden = true
                        deliveredOrderView.isHidden = true
                        deliveredOrderImgBGView.isHidden = true
                        acceptedOrderLineLbl.isHidden = true
                        readyOrderLineLbl.isHidden = true
                        onTheWayOrderLineLbl.isHidden = true
                        acceptedOrderSepLineLbl.isHidden = true
                    }else if acceptOrderArray.count > 0 && readyOrderArray.count == 0{
                        if cancelOrderArray[0].TrackingTime != ""{
                            if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                                let date = cancelOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                                let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                                readyOrderTimeLbl.attributedText = time
                            }else{
                                let date = cancelOrderArray[0].TrackingTime.split(separator: ".")[0]
                                readyOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                            }
                        }else{
                            readyOrderTimeLbl.text = ""
                        }
                        readyOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                        readyOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                        readyOrderImage.image = UIImage(named: "track_order_cancel")
                        readyOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                        onTheWayOrderView.isHidden = true
                        onTheWayOrderImgBGView.isHidden = true
                        deliveredOrderView.isHidden = true
                        deliveredOrderImgBGView.isHidden = true
                        readyOrderLineLbl.isHidden = true
                        onTheWayOrderLineLbl.isHidden = true
                        readyOrderSepLineLbl.isHidden = true
                    }else if acceptOrderArray.count > 0 && readyOrderArray.count > 0 && onTheWayOrderArray.count == 0{
                        if cancelOrderArray[0].TrackingTime != ""{
                            if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                                let date = cancelOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                                let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                                onTheWayOrderTimeLbl.attributedText = time
                            }else{
                                let date = cancelOrderArray[0].TrackingTime.split(separator: ".")[0]
                                onTheWayOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                            }
                        }else{
                            onTheWayOrderTimeLbl.text = ""
                        }
                        onTheWayOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                        onTheWayOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                        onTheWayOrderImage.image = UIImage(named: "track_order_cancel")
                        onTheWayOrderImgBGView.backgroundColor = #colorLiteral(red: 0.7596324682, green: 0.9229233861, blue: 0.9565992951, alpha: 1)
                        deliveredOrderView.isHidden = true
                        deliveredOrderImgBGView.isHidden = true
                        onTheWayOrderLineLbl.isHidden = true
                        onTheWayOrderSepLineLbl.isHidden = true
                    }else if acceptOrderArray.count > 0 && readyOrderArray.count > 0 && onTheWayOrderArray.count > 0 && deliveredOrderArray.count == 0{
                        if cancelOrderArray[0].TrackingTime != ""{
                            if orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                                let date = cancelOrderArray[0].TrackingTime.components(separatedBy: ".")[0]
                                let time = boldAndRegularText(text1: DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA), text2: " \(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .ddMM))", text3: "")
                                deliveredOrderTimeLbl.attributedText = time
                            }else{
                                let date = cancelOrderArray[0].TrackingTime.split(separator: ".")[0]
                                deliveredOrderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                            }
                        }else{
                            deliveredOrderTimeLbl.text = ""
                        }
                        deliveredOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CANCEL", value: "", table: nil))!
                        deliveredOrderDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been cancel", value: "", table: nil))!
                        deliveredOrderImage.image = UIImage(named: "track_order_cancel")
                        deliveredOrderTimeLbl.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.9215686275, blue: 0.9568627451, alpha: 1)
                    }
                }
                //Price Details
                self.totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].OrderDetails![0].TotalPrice.withCommas())"
                self.discountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
                if orderDetailsArray[0].OrderDetails![0].DeliveryCharge > 0{
                    self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].OrderDetails![0].DeliveryCharge.withCommas())"
                }else  if orderDetailsArray[0].OrderDetails![0].DeliveryCharge == 0{
                    self.deliveryChargesLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Free", value: "", table: nil))!)"
                }else{
                    self.deliveryChargesLbl.text = ""
                }
                
                self.vatLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].OrderDetails![0].Vat.withCommas())"
                self.netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].OrderDetails![0].NetTotal.withCommas())"
                self.vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(orderDetailsArray[0].OrderDetails![0].VatPercentage)%) :"
                
                if orderDetailsArray[0].OrderDetails![0].Wallet != nil{
                    if orderDetailsArray[0].OrderDetails![0].Wallet == 0 || orderDetailsArray[0].OrderDetails![0].Wallet == 0.0{
                        walletDiscountHeight.constant = 0
                        walletDiscountBottom.constant = 0
                    }else{
                        walletDiscountHeight.constant = 21
                        walletDiscountBottom.constant = 10
                        walletDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].OrderDetails![0].Wallet!.withCommas())"
                    }
                }else{
                    walletDiscountHeight.constant = 0
                    walletDiscountBottom.constant = 0
                }
                if orderDetailsArray[0].OrderDetails![0].Discount! == 0 || orderDetailsArray[0].OrderDetails![0].Discount! == 0.0{
                    couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(orderDetailsArray[0].OrderDetails![0].Discount!.withCommas())"
                }else{
                    couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(orderDetailsArray[0].OrderDetails![0].Discount!.withCommas())"
                }
            }
//            if orderDetailsArray[0].OrderDetails![0].RetryPayment == 1{
//                retryPaymentViewHeight.constant = 55
//            }else{
                retryPaymentViewHeight.constant = 0
//            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Reschedule Button Action
    @IBAction func rescheduleBtn_Tapped(_ sender: Any) {
        if orderDetailsArray[0].OrderDetails!.count > 0{
            selectOrderId = orderDetailsArray[0].OrderDetails![0].OrderId
            selectFrequency = orderDetailsArray[0].OrderDetails![0].FrequencyId
            selectSubscriptionId = orderDetailsArray[0].OrderDetails![0].SubscriptionId
            selectDate = orderDetailsArray[0].OrderDetails![0].ExpectedDate
            getShippingTime = orderDetailsArray[0].OrderDetails![0].ShippingTime
            if orderDetailsArray[0].OrderDetails![0].StartDate != nil{
                orderStartDate = orderDetailsArray[0].OrderDetails![0].StartDate!
            }
            let date = orderDetailsArray[0].OrderDetails![0].CurrentTime.components(separatedBy: ".")[0]
            currentDate = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
            if shippingMethodsArray.count > 0{
                self.shippingMethodsArray[0].isSelect = true
                self.slotIdSelect()
                if self.shippingMethodsArray.count > 0{
                    let date = self.selectDate.components(separatedBy: ".")[0]
                    if self.getShippingTime != ""{
                        let weekDay = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
                        let fromTime = self.getShippingTime.components(separatedBy: " - ")[0]
                        let time = self.shippingMethodsArray[0].ShippingSlots![weekDay-1].Slot!.filter({$0.StartTime == "\(fromTime)"})
                        if time.count > 0{
                            self.getSlotId = time[0].Id
                        }
                    }
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TimeSlotVC") as! TimeSlotVC
                    obj.modalPresentationStyle = .overCurrentContext
                    obj.timeSlotVCDelegate = self
                    //obj.shipDetails = self.shippingMethodsArray
                    obj.rescheduleTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                    obj.rescheduleSelectSlotId = self.getSlotId
                    obj.shipTimeSlotDate = self.shipTimeSlotDate
                    obj.selectSlotId = self.selectSlotId
                    obj.reschedule = true
                    obj.currentDate = self.currentDate
                    if orderDetailsArray[0].OrderDetails![0].RescheduleMaxDays != nil{
                        obj.maxDays = orderDetailsArray[0].OrderDetails![0].RescheduleMaxDays!
                    }else{
                        obj.maxDays = 90
                    }
                    obj.modalPresentationStyle = .overFullScreen
                    self.present(obj, animated: true, completion: nil)
                }else{
                    print("No Data")
                }
            }else{
                shippingMethodDetailsGetService(orderId: orderDetailsArray[0].OrderDetails![0].OrderId, shippingMethod: orderDetailsArray[0].OrderDetails![0].ShippingMethod)
            }
        }
    }
    //MARK: Call Button Action
    @IBAction func callBtn_Tapped(_ sender: Any) {
        var mobile = ""
        if orderDetailsArray.count > 0{
            mobile = orderDetailsArray[0].OrderDetails![0].StoreMobileNo
        }
        print(mobile.removeWhitespace())
        if let url = URL(string: "tel://\(mobile.removeWhitespace())") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func getUserDetails(key:String) -> String {
        return UserDefaults.standard.value(forKey: key) == nil ? "" : "\(UserDefaults.standard.value(forKey: key)!)"
    }
    //MARK: Help Button Action
    @IBAction func helpBtn_Tapped(_ sender: Any) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
            // Create a user object
        let user = FreshchatUser.sharedInstance();
        user.firstName = "\((userDic["FullName"]! as! String))"
        user.lastName = "\((userDic["NickName"]! as! String))"
        user.email = "\((userDic["Email"]! as! String))"
        user.phoneCountryCode="966"
        user.phoneNumber = "\((userDic["Mobile"]! as! String))"
        Freshchat.sharedInstance().setUser(user)
        Freshchat.sharedInstance().showConversations(self)
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        if orderDetailsArray.count > 0{
            DispatchQueue.main.async() {
                let Name = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!
                let OrderId = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order No", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].Invoice)"
                var ExpectedTime = ""
                if self.orderDetailsArray[0].OrderDetails![0].ExpectedDate != ""{
                    let expectDate = self.orderDetailsArray[0].OrderDetails![0].ExpectedDate.components(separatedBy: ".")[0]
                    if self.orderDetailsArray[0].OrderDetails![0].SectionType == 2{
                        let shipTime = self.orderDetailsArray[0].OrderDetails![0].ShippingTime.components(separatedBy: " - ")
                        if shipTime.count > 0 && self.orderDetailsArray[0].OrderDetails![0].ShippingTime != ""{
                            ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectDate)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyy)) \(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                        }else{
                            ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectDate)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyyTimeA))"
                        }
                    }else{
                        ExpectedTime = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Time", value: "", table: nil))!) : \(DateConverts.convertStringToStringDates(inputDateStr: "\(expectDate)", inputDateformatType: .dateTtime, outputDateformatType: .ddMMMyyyyTimeA))"
                    }
                }
                var OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].orderType)"
                if AppDelegate.getDelegate().appLanguage == "English"{
                    OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].orderType)"
                }else{
                    OrderType = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].orderTypeAr)"
                }
                let ItemQuantity = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Items", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].countofItems)"
                let OrderAmount = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].NetTotal.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                let paymentMode = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Mode", value: "", table: nil))!) : \(self.orderDetailsArray[0].OrderDetails![0].PaymentType)"
                let Des = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Thank you for ordering at dr.CAFE Coffee. It has been a pleasure serving you. We hope to serve you again soon!", value: "", table: nil))!

                let shareAll = [Name,OrderId,ExpectedTime,OrderType,ItemQuantity,OrderAmount, paymentMode,Des as Any ] as [Any]
                print(shareAll)
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    //MARK: Order Details Drop Down Button Action
    @IBAction func orderDetailsDropDownBtn_Tapped(_ sender: Any) {
        if orderDetailsMainViewHeight.constant == 44{
            self.orderItemsTableView.reloadData()
            self.orderDetailsMainViewHeight.constant = self.orderItemsTableView.contentSize.height + 44
            if AppDelegate.getDelegate().appLanguage == "English"{
                orderDetailsDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                orderDetailsDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            orderDetailsMainViewHeight.constant = 44
            orderDetailsDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Amount Details Drop Down Button Action
    @IBAction func billAmountDropDownBtn_Tapped(_ sender: Any) {
        if priceDetailsViewHeight.constant == 45{
            priceDetailsViewHeight.constant = 233
            if AppDelegate.getDelegate().appLanguage == "English"{
                billAmountDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                billAmountDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            priceDetailsViewHeight.constant = 45
            billAmountDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Retry Payment Button Action
    @IBAction func retryPaymentBtn_Tapped(_ sender: Any) {
        let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
        let dic:[String:Any] = ["amount": orderDetailsArray[0].OrderDetails![0].NetTotal, "merchantTransactionId":orderDetailsArray[0].OrderDetails![0].Invoice, "customerEmail":"\((userDic["Email"]! as! String))", "customerPhone":"\((userDic["Mobile"]! as! String))", "customerName":"\((userDic["FullName"]! as! String))","OrderId":orderDetailsArray[0].OrderDetails![0].OrderId, "registrationId":orderDetailsArray[0].OrderDetails![0].registrationId!, "UserId":UserDef.getUserId(), "SubscriptionId":subscriptionID,  "isVisaMaster":true]
        SubscriptionModuleServices.RetryService(dic: dic, success: { (data) in
            if(data.Status == false){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
            }else{
                DispatchQueue.main.async {
                    self.getOrderDetailsService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Change Card Button Action
    @IBAction func changeCardBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ManageCardsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
        obj.isSubscription = true
        obj.orderId = orderDetailsArray[0].OrderDetails![0].OrderId
        obj.subscriptionId = subscriptionID
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Shipping Method
    func shippingMethodDetailsGetService(orderId:Int,shippingMethod:String){
        let dic:[String : Any] = ["OrderId": orderId, "RequestBy":2]
        CartModuleServices.ShippingMethodService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.shippingMethodsArray = data.Data!.filter({$0.DisplayNameEn == shippingMethod})
                self.shippingMethodsArray[0].isSelect = true
                self.slotIdSelect()
                if self.shippingMethodsArray.count > 0{
                    let date = self.selectDate.components(separatedBy: ".")[0]
                    if self.getShippingTime != ""{
                        let weekDay = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
                        let fromTime = self.getShippingTime.components(separatedBy: " - ")[0]
                        let time = self.shippingMethodsArray[0].ShippingSlots![weekDay-1].Slot!.filter({$0.StartTime == "\(fromTime)"})
                        if time.count > 0{
                            self.getSlotId = time[0].Id
                        }
                    }
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TimeSlotVC") as! TimeSlotVC
                    obj.modalPresentationStyle = .overCurrentContext
                    obj.timeSlotVCDelegate = self
                    obj.shipDetails = self.shippingMethodsArray
                    obj.rescheduleTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                    obj.rescheduleSelectSlotId = self.getSlotId
                    obj.shipTimeSlotDate = self.shipTimeSlotDate
                    obj.selectSlotId = self.selectSlotId
                    if self.orderDetailsArray[0].OrderDetails![0].RescheduleMaxDays != nil{
                        obj.maxDays = self.orderDetailsArray[0].OrderDetails![0].RescheduleMaxDays!
                    }else{
                        obj.maxDays = 90
                    }
                    obj.reschedule = true
                    obj.currentDate = self.currentDate
                    obj.modalPresentationStyle = .overFullScreen
                    self.present(obj, animated: true, completion: nil)
                }else{
                    print("No Data")
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func slotIdSelect(){
        if shippingMethodsArray.count == 1{
            var shipDate = ""
            var currentShipDate:Date = Date()
            if orderStartDate != ""{
                shipDate = orderStartDate.components(separatedBy: ".")[0]
                firstShipDate = orderStartDate.components(separatedBy: "T")[0]
                currentShipDate = DateConverts.convertStringToDate(date: orderStartDate.components(separatedBy: "T")[0], dateformatType: .dateR)
            }else{
                shipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: ".")[0]
                firstShipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0]
                currentShipDate = DateConverts.convertStringToDate(date: shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0], dateformatType: .dateR)
            }
            firstSelectShipId = shippingMethodsArray[0].Id
            var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
            var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
            if currentShipDate > currentTime{
                let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                if time.count > 0{
                    shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                    shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                    selectSlotId = time[0].Id
                    for i in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                        if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
                            shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                        }
                    }
                    print("Match")
                }else{
                    print("Not Match")
                    for i in 1...shippingMethodsArray[0].FutureDays{
                        nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                        let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                        if time.count > 0{
                            shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                            shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                            selectSlotId = time[0].Id
                            for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                }
                            }
//                            if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                            }
                            print("Match")
                            break
                        }else{
                            print("Not Match")
                        }
                    }
                }
            }else{
                let timeSlot = expectTimeSlot(presentDate: shipDate, index: 0)
                if timeSlot == "NotMatch"{
                    let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: 0)
                    if firstTimeSlot == "NotMatch"{
                        for i in 1...shippingMethodsArray[0].FutureDays{
                            nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                            let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                            if time.count > 0{
                                shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                selectSlotId = time[0].Id
                                for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                    if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                        shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                    }
                                }
//                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                                }
                                print("Match")
                                break
                            }else{
                                print("Not Match")
                            }
                        }
                    }else{
                        print("Match")
                    }
                }else{
                    print("Match")
                }
            }
        }
    }
    func expectTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        var status = ""
        for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
            let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
            let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
            if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: startTime, dateformatType: .dateNTimeA) as NSDate, endTime: DateConverts.convertStringToDate(date: endTime, dateformatType: .dateNTimeA) as NSDate) == true{
                if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                    shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                  shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                    selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                    status = "Match"
                    break
                }else{
                    status = "NotMatch"
                }
            }else{
                status = "NotMatch"
            }
        }
        print("Time Slot Status \(status)")
        return status
    }
    func selectFirstTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        let currentDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
        var status = ""
        let start = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![0].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        let end = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: start, dateformatType: .dateNTime) as NSDate, endTime: DateConverts.convertStringToDate(date: end, dateformatType: .dateNTime) as NSDate) == true || currentDate < start{
            for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
                let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
                let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                if status == "NotMatch"{
                    if currentDate < startTime && currentDate < endTime{
                        if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                            shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                            shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                            shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                            selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                            status =  "Match"
                            break
                        }else{
                            status =  "NotMatch"
                        }
                    }else{
                        status = "NotMatch"
                    }
                }else{
                    status = "NotMatch"
                }
            }
        }else{
            status = "NotMatch"
        }
        print("First Time Slot Status \(status)")
        return status
    }
    func isTimeStampCurrent(timeStamp:NSDate, startTime:NSDate, endTime:NSDate)->Bool{
        timeStamp.earlierDate(endTime as Date) == timeStamp as Date && timeStamp.laterDate(startTime as Date) == timeStamp as Date
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String, text3:String) -> NSMutableAttributedString{
        let boldAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 12.0)!
           ]
        let regularAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 12.0)!
           ]
        let boldText1 = NSAttributedString(string: text1, attributes: boldAttribute)
        let boldText2 = NSAttributedString(string: text2, attributes: boldAttribute)
        let regularText = NSAttributedString(string: text3, attributes: regularAttribute)
        let newString = NSMutableAttributedString()
        newString.append(boldText1)
        newString.append(boldText2)
        newString.append(regularText)
        return newString
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension SubscriptionTrackingVC : UITableViewDelegate, UITableViewDataSource, SubscriptionHistoryTVCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == orderItemsTableView{
            if orderDetailsArray.count > 0{
                if self.orderDetailsArray[0].OrderDetails!.count > 0{
                    if self.orderDetailsArray[0].OrderDetails![0].Items!.count > 0{
                        return self.orderDetailsArray[0].OrderDetails![0].Items!.count
                    }else{
                        return 0
                    }
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else if tableView == rejectedOrdersTableView{
            return self.rejectedOrderDetailsArray.count
        }else{
            if totalRows > self.orderHistoryDetailsArray.count {
                return self.orderHistoryDetailsArray.count + 1
            }else{
                return self.orderHistoryDetailsArray.count
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == orderItemsTableView{
            var additional = [""]
            if orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn != nil{
                if orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }
            var noteHeight = 0
            //Comments
            if let _ = self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments! != ""{
                    let height = heightForView(text: "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                    noteHeight = Int(10 + height)
                }else{
                    noteHeight = 0
                }
            }else{
                noteHeight = 0
            }
            
            if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals != nil{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals!.count - 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additional.append("\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].AdditionalEn) x \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].Qty)")
                        }else{
                            additional.append("\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].AdditionalAr) x \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].Qty)")
                        }
                    }
                }
                if additional.count > 1{
                    additional.remove(at: 0)
                    var height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                    height = 95 + height
                    return height + CGFloat(noteHeight)
                }else{
                    return CGFloat(90 + noteHeight)
                }
            }else{
                return CGFloat(90 + noteHeight)
            }
        }else if tableView == rejectedOrdersTableView{
            return UITableView.automaticDimension
        }else{
            if self.orderHistoryDetailsArray.count == indexPath.row {
               return 50
            }
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == orderItemsTableView{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderItemsTVCell", value: "", table: nil))!, value: "", table: nil))!) as! OrderItemsTVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].itemType == "Regular"{
                    cell.itemNameLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemNameEn)"
                }else{
                    cell.itemNameLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemNameEn) (\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].itemType))"
                }
                cell.itemQuantityLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].quantity) X \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemSubTotal.withCommas())"
            }else{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].itemType == "Regular"{
                    cell.itemNameLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemNameAr)"
                }else{
                    cell.itemNameLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemNameAr) (\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].itemTypeAr))"
                }
                cell.itemQuantityLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemSubTotal.withCommas()) X \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].quantity)"
            }
            cell.itemPriceLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].TotalPrice.withCommas())"
            cell.additionalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Additional", value: "", table: nil))!
            if indexPath.row%2 == 0{
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
            }
            var additional = [""]
            if orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn != nil{
                if orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }
            if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals != nil{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals!.count - 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additional.append("\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].AdditionalEn) x \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].Qty)")
                        }else{
                            additional.append("\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].AdditionalAr) x \(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Additionals![i].Qty)")
                        }
                    }
                }
            }
            
            if additional.count > 1{
                additional.remove(at: 0)
                cell.additionalsLbl.text = additional.joined(separator: ",")
                let height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
                cell.additionalsViewHeight.constant = 5 + height
            }else{
                cell.additionalsLbl.text = ""
                cell.additionalsViewHeight.constant = 0
            }
            
            cell.grinderViewHeight.constant = 0
            
            //Comments
            if let _ = self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments{
                if self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments! != ""{
                    cell.noteLbl.text = "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments!)"
                    let height = heightForView(text: "\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].ItemComments!)", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: cell.additionalsLbl.frame.width)
                    cell.noteViewHeight.constant = 10 + height
                }else{
                    cell.noteLbl.text = ""
                    cell.noteViewHeight.constant = 0
                }
            }else{
                cell.noteLbl.text = ""
                cell.noteViewHeight.constant = 0
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(self.orderDetailsArray[0].OrderDetails![0].Items![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.orderItemsTableView.layoutIfNeeded()
                self.orderItemsTableViewHeight.constant = self.orderItemsTableView.contentSize.height
                self.orderDetailsMainViewHeight.constant = self.orderItemsTableView.contentSize.height + 44
            }
            cell.selectionStyle = .none
            return cell
        }else if tableView == rejectedOrdersTableView{
            self.rejectedOrdersTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!)
            let cell = rejectedOrdersTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!, for: indexPath) as! SubscriptionHistoryTVCell
            
            cell.changeCardBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Card", value: "", table: nil))!, for: .normal)
            cell.retryPaymentBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Retry Payment", value: "", table: nil))!, for: .normal)
            
            let dic = self.rejectedOrderDetailsArray[indexPath.row]
            cell.invoiceNumLbl.text = "#\(dic.InvoiceNo)"
            if dic.SectionType == 2{
                var shipTime = dic.ShippingTime.components(separatedBy: " - ")
                if shipTime.count == 1{
                    shipTime = dic.ShippingTime.components(separatedBy: "-")
                }
                if dic.ExpectedTime != ""{
                    let expectDate = dic.ExpectedTime.components(separatedBy: "T")[0]
                    if shipTime.count > 0 && dic.ShippingTime != ""{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                    }else{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = ""
                    }
                }else{
                    if shipTime.count > 0 && dic.ShippingTime != ""{
                        cell.orderDateLbl.text = ""
                        cell.orderTimeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                    }else{
                        cell.orderDateLbl.text = ""
                        cell.orderTimeLbl.text = ""
                    }
                }
            }else{
                if dic.ExpectedTime != ""{
                    let expectDate = dic.ExpectedTime.components(separatedBy: ".")[0]
                    cell.orderDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    cell.orderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                }else{
                    cell.orderDateLbl.text = ""
                    cell.orderTimeLbl.text = ""
                }
            }
            cell.amountLbl.text = "\(dic.NetPrice.withCommas())"
            cell.amountCurrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
            //Order Status Functionality
            if dic.OrderStatus == "Close"{
                cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
            }else{
                cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: dic.OrderStatus, value: "", table: nil))!
            }
            cell.subscriptionImgWidth.constant = 0
            cell.subscriptionImgLeading.constant = 0
            
            //Payment Details
            cell.paymentTypeImg.isHidden = false
            cell.paymentTypeLbl.isHidden = false
            //Image
            if dic.PaymentBrandIcon != nil{
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.PaymentBrandIcon!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.paymentTypeImg.kf.setImage(with: url)
            }else{
                cell.paymentTypeImg.image = nil
            }
            if AppDelegate.getDelegate().appLanguage == "English"{
                var paymentName = ""
                if dic.PaymentMethod != nil{
                    paymentName = dic.PaymentMethod!
                }else{
                    paymentName = ""
                }
                if dic.PaymentDetails != nil{
                    if dic.PaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(paymentName) ****\(dic.PaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = paymentName
                    }
                }else{
                    cell.paymentTypeLbl.text = paymentName
                }
            }else{
                var paymentName = ""
                if dic.PaymentMethodAr != nil{
                    paymentName = dic.PaymentMethodAr!
                }else{
                    if dic.PaymentMethod != nil{
                        paymentName = dic.PaymentMethod!
                    }else{
                        paymentName = ""
                    }
                }
                if dic.PaymentDetails != nil{
                    if dic.PaymentDetails!.count > 0{
                        cell.paymentTypeLbl.text = "\(paymentName) ****\(dic.PaymentDetails![0].LastDigits)"
                    }else{
                        cell.paymentTypeLbl.text = paymentName
                    }
                }else{
                    cell.paymentTypeLbl.text = paymentName
                }
            }
            
            if dic.Retrypayment == 1{
                cell.retryPaymentBtn.isHidden = false
                cell.changeCardBtn.isHidden = false
            }else{
                cell.retryPaymentBtn.isHidden = true
                cell.changeCardBtn.isHidden = true
            }
            
            if dic.OrderType == "4"{
                if dic.DeliveryAddress != nil{
                    if dic.DeliveryAddress!.count > 0{
                        cell.addressLbl.text = dic.DeliveryAddress![0].Details
                    }else{
                        cell.addressLbl.text = ""
                    }
                }else{
                    cell.addressLbl.text = ""
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.addressLbl.text = dic.Store![0].StoreAddressEn
                }else{
                    cell.addressLbl.text = dic.Store![0].StoreAr
                }
            }
        
            //Favourite functionality
            cell.favouriteNameLbl.text = ""
            cell.favourireNameViewHeight.constant = 0
            
            //Rating functionality
            if dic.OrderStatus == "Close"{
                if dic.OrderRatings != nil{
                    if dic.OrderRatings!.count > 0{
                        cell.rattingView.rating = Double(dic.OrderRatings![0].Value)
                        cell.rateBtnView.isHidden = true
                        cell.rattingView.isHidden = false
                        if dic.OrderRatings![0].Value == 1{
                            cell.rattingView.fullImage = UIImage(named: "Rate 1")
                        }else if dic.OrderRatings![0].Value == 2{
                            cell.rattingView.fullImage = UIImage(named: "Rate 2")
                        }else if dic.OrderRatings![0].Value == 3{
                            cell.rattingView.fullImage = UIImage(named: "Rate 3")
                        }else if dic.OrderRatings![0].Value == 4{
                            cell.rattingView.fullImage = UIImage(named: "Rate 4")
                        }else if dic.OrderRatings![0].Value == 5{
                            cell.rattingView.fullImage = UIImage(named: "Rate 5")
                        }else{
                            cell.rattingView.fullImage = UIImage(named: "empStar")
                            cell.rateBtnView.isHidden = true
                            cell.rateMainView.isHidden = true
                        }
                    }else{
                        cell.rateBtnView.isHidden = false
                        cell.rateMainView.isHidden = true
                    }
                }else{
                    cell.rateBtnView.isHidden = false
                    cell.rateMainView.isHidden = true
                }
                cell.retryPaymentBtn.isHidden = true
                cell.changeCardBtn.isHidden = true
            }else{
                cell.rateMainView.isHidden = true
                cell.rateBtnView.isHidden = true
            }
            
            if dic.OrderStatus == "Cancel" || dic.OrderStatus == "Reject"{
                cell.orderStatusLbl.textColor = #colorLiteral(red: 0.7522415519, green: 0.1044703498, blue: 0.1641884744, alpha: 1)
            }else{
                cell.orderStatusLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.6196078431, blue: 0.262745098, alpha: 1)
            }
            
            cell.rateOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Order", value: "", table: nil))!
            
            cell.delegate = self
            cell.tableView = rejectedOrdersTableView
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.rejectedOrdersTableView.layoutIfNeeded()
                self.rejectedOrdersMainViewHeight.constant = self.rejectedOrdersTableView.contentSize.height + 40
            }
            
            cell.selectionStyle = .none
            return cell
        }else{
            if self.orderHistoryDetailsArray.count == indexPath.row && totalRows > self.orderHistoryDetailsArray.count {
                self.orderHistoryTableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
                cell.activityIndicatorView.startAnimating()
                cell.selectionStyle = .none
                return cell
            }else{
                self.orderHistoryTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!)
                let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionHistoryTVCell", value: "", table: nil))!, for: indexPath) as! SubscriptionHistoryTVCell
                
                let dic = self.orderHistoryDetailsArray[indexPath.row]
                cell.invoiceNumLbl.text = "#\(dic.InvoiceNo)"
                if dic.SectionType == 2{
                    let expectDate = dic.ExpectedTime.components(separatedBy: "T")[0]
                    var shipTime = dic.ShippingTime.components(separatedBy: " - ")
                    if shipTime.count == 1{
                        shipTime = dic.ShippingTime.components(separatedBy: "-")
                    }
                    if shipTime.count > 0 && dic.ShippingTime != ""{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = "\(shipTime[0]) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shipTime[1])"
                    }else{
                        cell.orderDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateR, outputDateformatType: .date))"
                        cell.orderTimeLbl.text = ""
                    }
                }else{
                    let expectDate = dic.ExpectedTime.components(separatedBy: ".")[0]
                    cell.orderDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    cell.orderTimeLbl.text = DateConverts.convertStringToStringDates(inputDateStr: expectDate, inputDateformatType: .dateTtime, outputDateformatType: .timeA)
                }
                cell.amountLbl.text = "\(dic.NetPrice.withCommas())"
                cell.amountCurrencyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!
                //Order Status Functionality
                if dic.OrderStatus == "Close"{
                    cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
                }else{
                    cell.orderStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: dic.OrderStatus, value: "", table: nil))!
                }
                cell.subscriptionImgWidth.constant = 0
                cell.subscriptionImgLeading.constant = 0
                
                //Payment Details
                cell.paymentTypeImg.isHidden = false
                cell.paymentTypeLbl.isHidden = false
                //Image
                if dic.PaymentBrandIcon != nil{
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.PaymentBrandIcon!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.paymentTypeImg.kf.setImage(with: url)
                }else{
                    cell.paymentTypeImg.image = nil
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    var paymentName = ""
                    if dic.PaymentMethod != nil{
                        paymentName = dic.PaymentMethod!
                    }else{
                        paymentName = ""
                    }
                    if dic.PaymentDetails != nil{
                        if dic.PaymentDetails!.count > 0{
                            cell.paymentTypeLbl.text = "\(paymentName) ****\(dic.PaymentDetails![0].LastDigits)"
                        }else{
                            cell.paymentTypeLbl.text = paymentName
                        }
                    }else{
                        cell.paymentTypeLbl.text = paymentName
                    }
                }else{
                    var paymentName = ""
                    if dic.PaymentMethodAr != nil{
                        paymentName = dic.PaymentMethodAr!
                    }else{
                        if dic.PaymentMethod != nil{
                            paymentName = dic.PaymentMethod!
                        }else{
                            paymentName = ""
                        }
                    }
                    if dic.PaymentDetails != nil{
                        if dic.PaymentDetails!.count > 0{
                            cell.paymentTypeLbl.text = "\(paymentName) ****\(dic.PaymentDetails![0].LastDigits)"
                        }else{
                            cell.paymentTypeLbl.text = paymentName
                        }
                    }else{
                        cell.paymentTypeLbl.text = paymentName
                    }
                }
                
                cell.retryPaymentBtn.isHidden = true
                cell.changeCardBtn.isHidden = true
                
                if dic.OrderType == "4"{
                    if dic.DeliveryAddress != nil{
                        if dic.DeliveryAddress!.count > 0{
                            cell.addressLbl.text = dic.DeliveryAddress![0].Details
                        }else{
                            cell.addressLbl.text = ""
                        }
                    }else{
                        cell.addressLbl.text = ""
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.addressLbl.text = dic.Store![0].StoreAddressEn
                    }else{
                        cell.addressLbl.text = dic.Store![0].StoreAr
                    }
                }
            
                //Favourite functionality
                cell.favouriteNameLbl.text = ""
                cell.favourireNameViewHeight.constant = 0
                
                //Rating functionality
                if dic.OrderStatus == "Close"{
                    if dic.OrderRatings != nil{
                        if dic.OrderRatings!.count > 0{
                            cell.rattingView.rating = Double(dic.OrderRatings![0].Value)
                            cell.rateBtnView.isHidden = true
                            cell.rattingView.isHidden = false
                            if dic.OrderRatings![0].Value == 1{
                                cell.rattingView.fullImage = UIImage(named: "Rate 1")
                            }else if dic.OrderRatings![0].Value == 2{
                                cell.rattingView.fullImage = UIImage(named: "Rate 2")
                            }else if dic.OrderRatings![0].Value == 3{
                                cell.rattingView.fullImage = UIImage(named: "Rate 3")
                            }else if dic.OrderRatings![0].Value == 4{
                                cell.rattingView.fullImage = UIImage(named: "Rate 4")
                            }else if dic.OrderRatings![0].Value == 5{
                                cell.rattingView.fullImage = UIImage(named: "Rate 5")
                            }else{
                                cell.rattingView.fullImage = UIImage(named: "empStar")
                                cell.rateBtnView.isHidden = true
                                cell.rateMainView.isHidden = true
                            }
                        }else{
                            cell.rateBtnView.isHidden = false
                            cell.rateMainView.isHidden = true
                        }
                    }else{
                        cell.rateBtnView.isHidden = false
                        cell.rateMainView.isHidden = true
                    }
                    cell.retryPaymentBtn.isHidden = true
                    cell.changeCardBtn.isHidden = true
                }else{
                    cell.rateMainView.isHidden = true
                    cell.rateBtnView.isHidden = true
                }
                
                if dic.OrderStatus == "Cancel" || dic.OrderStatus == "Reject"{
                    cell.orderStatusLbl.textColor = #colorLiteral(red: 0.7522415519, green: 0.1044703498, blue: 0.1641884744, alpha: 1)
                }else{
                    cell.orderStatusLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.6196078431, blue: 0.262745098, alpha: 1)
                }
                
                cell.delegate = self
                cell.tableView = orderHistoryTableView
                
                cell.rateOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Order", value: "", table: nil))!
                
                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.orderHistoryTableView.layoutIfNeeded()
                    self.orderHistoryTableViewHeight.constant = self.orderHistoryTableView.contentSize.height
                }
                
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == orderHistoryTableView{
            self.tableViewHeight += cell.frame.size.height
            orderHistoryTableViewHeight.constant = self.tableViewHeight
        }
    }
    func subscriptionButtonsTapped(cell: SubscriptionHistoryTVCell, invoice: Bool, rate: Bool, retryPayment: Bool, changeCard: Bool, tableView: UITableView) {
        if tableView == rejectedOrdersTableView{
            let indexPath = self.rejectedOrdersTableView.indexPath(for: cell)
            if invoice == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ReOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                obj.orderId = self.rejectedOrderDetailsArray[indexPath!.row].OrderId
                obj.isSubscription = self.rejectedOrderDetailsArray[indexPath!.row].IsSubscription
                obj.isArchieve = self.rejectedOrderDetailsArray[indexPath!.row].IsArchive
                self.navigationController?.pushViewController(obj, animated: true)
            }else if rate == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.OrderId = self.rejectedOrderDetailsArray[indexPath!.row].OrderId
                obj.orderType = Int(self.rejectedOrderDetailsArray[indexPath!.row].OrderType)!
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.StoreName = self.rejectedOrderDetailsArray[indexPath!.row].Store![0].StoreNameEn.capitalized
                }else{
                    obj.StoreName = self.rejectedOrderDetailsArray[indexPath!.row].Store![0].StoreAr.capitalized
                }
                obj.rateVCDelegate = self
                obj.modalPresentationStyle = .overFullScreen
                present(obj, animated: true, completion: nil)
            }else if retryPayment == true{
                let userDic = UserDefaults.standard.object(forKey: "userDic") as! [String:Any]
                let dic:[String:Any] = ["amount": self.rejectedOrderDetailsArray[indexPath!.row].NetPrice, "merchantTransactionId":self.rejectedOrderDetailsArray[indexPath!.row].InvoiceNo, "customerEmail":"\((userDic["Email"]! as! String))", "customerPhone":"\((userDic["Mobile"]! as! String))", "customerName":"\((userDic["FullName"]! as! String))","OrderId":self.rejectedOrderDetailsArray[indexPath!.row].OrderId, "registrationId":self.rejectedOrderDetailsArray[indexPath!.row].RegistrationId, "UserId":UserDef.getUserId(), "SubscriptionId":subscriptionID,  "isVisaMaster":true]
                SubscriptionModuleServices.RetryService(dic: dic, success: { (data) in
                    if(data.Status == false){
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                    }else{
                        DispatchQueue.main.async {
                            self.getOrderDetailsService()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                SaveAddressClass.getSubscriptionOrderHistoryArray.removeAll()
                                self.getOrderHistoryService()
                            }
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }else if changeCard == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ManageCardsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
                obj.isSubscription = true
                obj.orderId = self.rejectedOrderDetailsArray[indexPath!.row].OrderId
                obj.subscriptionId = subscriptionID
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            let indexPath = self.orderHistoryTableView.indexPath(for: cell)
            if invoice == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ReOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                obj.orderId = self.orderHistoryDetailsArray[indexPath!.row].OrderId
                obj.isSubscription = self.orderHistoryDetailsArray[indexPath!.row].IsSubscription
                obj.isArchieve = self.orderHistoryDetailsArray[indexPath!.row].IsArchive
                self.navigationController?.pushViewController(obj, animated: true)
            }else if rate == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RateVC") as! RateVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.OrderId = self.orderHistoryDetailsArray[indexPath!.row].OrderId
                obj.orderType = Int(self.orderHistoryDetailsArray[indexPath!.row].OrderType)!
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.StoreName = self.orderHistoryDetailsArray[indexPath!.row].Store![0].StoreNameEn.capitalized
                }else{
                    obj.StoreName = self.orderHistoryDetailsArray[indexPath!.row].Store![0].StoreAr.capitalized
                }
                obj.rateVCDelegate = self
                obj.modalPresentationStyle = .overFullScreen
                present(obj, animated: true, completion: nil)
            }
        }
    }
    //MARK: Favourite Button Tapped
    @objc func favouriteBtn_Tapped(sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TextPopUpVC") as! TextPopUpVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.textPopUpVCDelegate = self
        obj.id = sender.tag
        obj.modalPresentationStyle = .overFullScreen
        self.present(obj, animated: true, completion: nil)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height) {
            if totalRows > orderHistoryDetailsArray.count {
                pageNumber = pageNumber + 1
                self.getOrderHistoryService()
            }
        }
    }
    //Get Label Height Based on content
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()

        return label.frame.height
    }
}
//MARK: Rate VC Delegate
@available(iOS 13.0, *)
extension SubscriptionTrackingVC:RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            self.getOrderHistoryService()
        }
    }
}
@available(iOS 13.0, *)
extension SubscriptionTrackingVC:TimeSlotVCDelegate{
    func didTapAction(expectDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateDots))
        DispatchQueue.main.async {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubscriptionReschedulePopUpVC") as! SubscriptionReschedulePopUpVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.subscriptionId = self.selectSubscriptionId
            obj.orderId = self.selectOrderId
            obj.slotTime = "\(startTime) - \(endTime)"
            obj.slotId = SlotId
            obj.orderExpected = expectDate
            obj.subscriptionReschedulePopUpVCDelegate = self
            obj.modalPresentationStyle = .overFullScreen
            self.present(obj, animated: true, completion: nil)
        }
//        let dic:[String:Any] = ["ShippingTime": "\(startTime) - \(endTime)", "SlotId": SlotId, "StartDate": "\(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateR)) 00:00:00.000", "Frequency": selectFrequency, "OrderId": selectOrderId, "SubscriptionId": selectSubscriptionId, "Action": 5, "RequestBy": 2]
//        SubscriptionModuleServices.PauseOrderService(dic: dic, success: { (data) in
//            if(data.Status == false){
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                }else{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                }
//            }else{
//                //Alert.showToastDown(on: self, message: data.MessageEn)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    self.getOrderDetailsService()
//                }
//            }
//        }) { (error) in
//            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
//        }
    }
}
extension SubscriptionTrackingVC : SubscriptionReschedulePopUpVCDelegate{
    func didTapAction(status: String) {
        self.getOrderDetailsService()
    }
}
//MARK: TextPopUp Delegate
@available(iOS 13.0, *)
extension SubscriptionTrackingVC : TextPopUpVCDelegate{
    func didTapAction(ID: Int, name: String) {
        var dic:[String:Any] = [:]
        if self.orderHistoryDetailsArray[ID].Favourite == false{
            dic = ["OrderId": self.orderHistoryDetailsArray[ID].OrderId, "FavouriteName":name, "RequestBy":2]
        }else{
            dic = ["OrderId": self.orderHistoryDetailsArray[ID].OrderId, "RequestBy":2]
        }
        CartModuleServices.ToggleFevOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                var message = data.MessageEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    message = data.MessageEn
                }else{
                    message = data.MessageAr
                }
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                    //Favourite functionality
                    if self.orderHistoryDetailsArray[ID].Favourite == false{
                        self.orderHistoryDetailsArray[ID].Favourite = true
                    }else{
                        self.orderHistoryDetailsArray[ID].Favourite = false
                    }
                    self.orderHistoryTableView.reloadData()
                }))
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
