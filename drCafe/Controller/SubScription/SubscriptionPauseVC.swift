//
//  SubscriptionPauseVC.swift
//  drCafe
//
//  Created by Devbox on 22/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

protocol SubscriptionPauseVCDelegate {
    func didTapAction(status:String)
}

class SubscriptionPauseVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    //MARK: IBOutlet
    @IBOutlet weak var reasonLbl: UILabel!
    @IBOutlet weak var notesTV: UITextView!
    @IBOutlet weak var reasonDropDownBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var datesCollectionView: UICollectionView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var slotsLbl: UILabel!
    
    @IBOutlet weak var reasonNameLbl: UILabel!
    @IBOutlet weak var notesNameLbl: UILabel!
    
    var subscriptionPauseVCDelegate:SubscriptionPauseVCDelegate!
    var subscriptionID = 0
    var orderId = 0
    var reason = ""
    var shippingTime = ""
    var slotId = 0
    var startDate = ""
    var frequency = 0
    var shippingMethod = ""
    var selectSlotId = 0
    var firstShipDate = ""
    var firstSelectShipId = 0
    var shipTimeSlot = ""
    var shipTimeSlotDate = ""
    
    var dropDownArray = ["Out of town", "Stock available", "Not using frequently"]

    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Stock available", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Not using frequently", value: "", table: nil))!]
        reasonLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!
        reason = "Out of town"
        
        let time = shippingTime.components(separatedBy: " - ")
        slotsLbl.text = ""
        dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(startDate.components(separatedBy: " ")[0])", inputDateformatType: .dateR, outputDateformatType: .date)) \(time[0]) to \(time[1])"
        
        reasonNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reason", value: "", table: nil))!
        notesNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Notes", value: "", table: nil))!
        
        doneBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        
    }
    //MARK: Dismiss Button
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
        subscriptionPauseVCDelegate.didTapAction(status: "Cancel")
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Reason Drop Down Button
    @IBAction func reasonDropDownBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Done Button
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["ShippingTime": shippingTime, "SlotId": slotId, "StartDate": startDate, "Frequency": frequency, "OrderId": orderId, "SubscriptionId": subscriptionID, "Action": 5, "Pausereason": "\(reason)", "RequestBy": 2, "Note": notesTV.text!]
        SubscriptionModuleServices.PauseOrderService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.subscriptionPauseVCDelegate.didTapAction(status: "Done")
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
//MARK: Drop Down Delegate
@available(iOS 13.0, *)
extension SubscriptionPauseVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!{
            reason = "Out of town"
        }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Stock available", value: "", table: nil))!{
            reason = "Stock available"
        }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Not using frequently", value: "", table: nil))!{
            reason = "Not using frequently"
        }else{
            reason = name
        }
        reasonLbl.text = name
    }
}
