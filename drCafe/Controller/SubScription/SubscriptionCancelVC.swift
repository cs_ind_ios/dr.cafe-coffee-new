//
//  SubscriptionCancelVC.swift
//  drCafe
//
//  Created by Devbox on 22/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

protocol SubscriptionCancelVCDelegate {
    func didTapAction(status:String)
}

class SubscriptionCancelVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: IBOutLet
    @IBOutlet weak var reasonLbl: UILabel!
    @IBOutlet weak var notesTV: UITextView!
    @IBOutlet weak var reasonDropDownBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var reasonNameLbl: UILabel!
    @IBOutlet weak var notesNameLbl: UILabel!
    
    var subscriptionCancelVCDelegate:SubscriptionCancelVCDelegate!
    var subscriptionID = 0
    var reason = ""
    var VCTitle = ""
    
    var dropDownArray = ["Out of town", "Stock available", "Not using frequently"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dropDownArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Stock available", value: "", table: nil))!, (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Not using frequently", value: "", table: nil))!]
        reasonLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!
        reason = "Out of town"
        
        titleLbl.text = VCTitle

        reasonNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reason", value: "", table: nil))!
        notesNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Notes", value: "", table: nil))!
        
    }
    //MARK: Dismiss Button
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
        subscriptionCancelVCDelegate.didTapAction(status: "Cancel")
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Reason Drop Down
    @IBAction func reasonDropDownBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX - 12 , y: -13, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Done Button
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["SubscriptionId" : subscriptionID, "Cancelreason" : reason, "RequestBy": 2, "Note": notesTV.text!]
        SubscriptionModuleServices.SubscriptionCancelService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                Alert.showToastDown(on: self, message: data.MessageEn)
                self.subscriptionCancelVCDelegate.didTapAction(status: "Done")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
//MARK: Country Code Drop Down Delegate
@available(iOS 13.0, *)
extension SubscriptionCancelVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of town", value: "", table: nil))!{
            reason = "Out of town"
        }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Stock available", value: "", table: nil))!{
            reason = "Stock available"
        }else if name == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Not using frequently", value: "", table: nil))!{
            reason = "Not using frequently"
        }else{
            reason = name
        }
        reasonLbl.text = name
    }
}
