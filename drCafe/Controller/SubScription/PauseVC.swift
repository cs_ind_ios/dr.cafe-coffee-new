//
//  PauseVC.swift
//  drCafe
//
//  Created by Devbox on 14/12/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import FSCalendar

protocol PauseVCDelegate {
    func didTapAction(getDate:Date, startTime:String, endTime:String, whereType:Int, SlotId:Int)
}

class PauseVC: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var calenderView: FSCalendar!
    @IBOutlet weak var estimateDateNameLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeSlotBGView: UIView!
    @IBOutlet weak var timeSlotTableView: UITableView!
    
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    var obj_where:Int!
    var pauseVCDelegate:PauseVCDelegate!
    var minimumDate:Date!
    var maxDays:Int!
    var selectDate:Date!
    var firstSelectDate:Date!
    
    var shipDetails = [ShippingMethodDetailsModel]()
    var pauseFrequencyArray = [FrequencyOptionsModel]()
    var weekDayNum:Int!
    var firstTimeSlotSelectIndex = 0
    var selectSlotId:Int!
    
    var rescheduleSelectSlotId:Int!
    var rescheduleTimeSlotDate:String!
    var currentDate:String!
    var shipTimeSlotDate:String!
    var popover = Popover()
    
    fileprivate lazy var dateFormatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "EN")
        formatter.dateFormat = "EEEE"
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        calenderView.delegate = self
        calenderView.dataSource = self
        calenderView.appearance.headerDateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM yyyy", options: 0, locale: NSLocale(localeIdentifier: "en-US") as Locale)
        calenderView.calendarHeaderView.collectionViewLayout.collectionView?.semanticContentAttribute = .forceLeftToRight
        calenderView.locale = Locale(identifier: "en")

        dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(minimumDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .ddMMMyyyy)
        
        estimateDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pause Untill", value: "", table: nil))!
        popUpNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pause PopUp Msg", value: "", table: nil))!
        dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        doneBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, for: .normal)
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        firstSelectDate = DateConverts.convertDateToDateFormate(date: minimumDate!)
        selectDate = DateConverts.convertDateToDateFormate(date: minimumDate!)
        calenderView.today = nil
        
        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(minimumDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
        for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
            if selectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                firstTimeSlotSelectIndex = i
            }else{
                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
            }
        }
    }
    //MARK: Dismiss Button
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Close Button
    @IBAction func closeBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dateBtn_Tapped(_ sender: Any) {
    }
    //MARK: Info Button
    @IBAction func infoBtn_Tapped(_ sender: UIButton) {
        let startPoint = CGPoint(x: sender.frame.maxX - 10, y: BGView.frame.minY + 40)
        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 310, height: 100)
        popover.show(self.popUpLblMainView, point: startPoint)
    }
    //MARK: Done BUtton
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        self.weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(self.selectDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
        let selectTimeSlot = self.shipDetails[0].ShippingSlots![self.weekDayNum-1].Slot!.filter({$0.isSelect! == true})
        if selectTimeSlot.count > 0{
            Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Subscription has been Pause untill", value: "", table: nil))!) \(dateLbl.text!) \n \(selectTimeSlot[0].StartTime) to \(selectTimeSlot[0].EndTime)", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.pauseVCDelegate.didTapAction(getDate: self.selectDate, startTime: selectTimeSlot[0].StartTime, endTime: selectTimeSlot[0].EndTime, whereType: self.obj_where, SlotId: self.selectSlotId!)
                self.dismiss(animated: true, completion: nil)
            }))
        }
    }
}
//MARK: FSCalender Delegate Methods
@available(iOS 15.0, *)
extension PauseVC:FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance{
    func minimumDate(for calendar: FSCalendar) -> Date {
        return minimumDate!
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        let date = Calendar.current.date(byAdding: .day, value: maxDays!, to: minimumDate!)
        return date!
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        if obj_where == 1{
            return true
        }else{
            selectDate = DateConverts.convertDateToDateFormate(date: date)
            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
            for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
            }
            let time = shipDetails[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
            if time.count > 0{
                for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                    if firstSelectDate == selectDate{
                        for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                            if firstTimeSlotSelectIndex == i{
                                selectSlotId = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                            }else{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                            }
                        }
                    }else{
                        selectSlotId = time[0].Id
                        if time[0].Id == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                            shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                        }else{
                            shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                        }
                    }
                }
                return true
            }else{
                return false
            }
        }
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectDate = DateConverts.convertDateToDateFormate(date: date)
        dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTimeZ, outputDateformatType: .ddMMMyyyy)
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        if DateConverts.convertStringToStringDates(inputDateStr: "\(minimumDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR) == DateConverts.convertDateToString(date: date, dateformatType: .dateR){
            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(minimumDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
            for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                if selectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                    firstTimeSlotSelectIndex = i
                }else{
                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                }
            }
        }
        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
        let time = shipDetails[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
        if time.count > 0{
            return nil
        }else{
            return UIColor.lightGray
        }
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return UIColor.white
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        if DateConverts.convertStringToStringDates(inputDateStr: "\(minimumDate!)", inputDateformatType: .dateTimeZ, outputDateformatType: .dateR) == DateConverts.convertDateToString(date: date, dateformatType: .dateR){
            return #colorLiteral(red: 0.3647775054, green: 0.7203516364, blue: 0.7748775482, alpha: 1)
        }else{
            if pauseFrequencyArray.count > 0{
                let pauseDate =  pauseFrequencyArray.filter({DateConverts.convertStringToStringDates(inputDateStr: "\($0.OptionEN!.split(separator: ".")[0])", inputDateformatType: .dateTtime, outputDateformatType: .dateR) == DateConverts.convertDateToString(date: date, dateformatType: .dateR)})
                if pauseDate.count > 0{
                    return #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                }else{
                    return nil
                }
            }else{
                return nil
            }
        }
    }
}
//MARK: TableView Delegate Methods
extension PauseVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!) as! timeSlotTVCell
        
        cell.fromDateLbl.text = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].StartTime
        cell.toDateLbl.text = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].EndTime
        
        if firstSelectDate == selectDate{
            if firstTimeSlotSelectIndex != nil{
                if indexPath.row < firstTimeSlotSelectIndex{
                    cell.bookedNameLblWidth.constant = 50
                    cell.bookedNameLblTrailing.constant = 15
                }else{
                    if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                        cell.bookedNameLblWidth.constant = 50
                        cell.bookedNameLblTrailing.constant = 15
                    }else{
                        cell.bookedNameLblWidth.constant = 0
                        cell.bookedNameLblTrailing.constant = 0
                    }
                }
            }else{
                if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                    cell.bookedNameLblWidth.constant = 50
                    cell.bookedNameLblTrailing.constant = 15
                }else{
                    cell.bookedNameLblWidth.constant = 0
                    cell.bookedNameLblTrailing.constant = 0
                }
            }
        }else{
            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                cell.bookedNameLblWidth.constant = 50
                cell.bookedNameLblTrailing.constant = 15
            }else{
                cell.bookedNameLblWidth.constant = 0
                cell.bookedNameLblTrailing.constant = 0
            }
        }
        
        if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect! == true{
            cell.BGView.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.9294117647, blue: 0.9647058824, alpha: 1)
            cell.BGView.borderWidth = 0
        }else{
            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.BGView.borderWidth = 1
                cell.BGView.borderColor = #colorLiteral(red: 0.7098039216, green: 0.5411764706, blue: 0.5019607843, alpha: 1)
            }else{
                if firstTimeSlotSelectIndex != nil{
                    if indexPath.row < firstTimeSlotSelectIndex{
                        cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        cell.BGView.borderWidth = 1
                        cell.BGView.borderColor = #colorLiteral(red: 0.7098039216, green: 0.5411764706, blue: 0.5019607843, alpha: 1)
                    }else{
                        cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                        cell.BGView.borderWidth = 0
                    }
                }else{
                    cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.BGView.borderWidth = 0
                }
//                cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
//                cell.BGView.borderWidth = 0
            }
        }
        
        timeSlotTableView.layoutIfNeeded()
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if firstSelectDate == selectDate{
            if indexPath.row < firstTimeSlotSelectIndex{
                print("Not Selected")
            }else{
                if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Selected Time Slot is Full", value: "", table: nil))!)
                }else{
                    for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                    }
                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect = true
                    selectSlotId = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].Id
                    tableView.reloadData()
                }
            }
        }else{
            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Selected Time Slot is Full", value: "", table: nil))!)
            }else{
                for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                }
                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect = true
                selectSlotId = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].Id
                tableView.reloadData()
            }
        }
    }
}
