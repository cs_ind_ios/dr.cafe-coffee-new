//
//  CarInformationVC.swift
//  drCafe
//
//  Created by mac3 on 26/10/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

protocol CarInformationVCDelegate {
    func didAddOrUpdateCar(status:Bool)
}

class CarInformationVC: UIViewController, UIPopoverPresentationControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var titleNameLbl: UILabel!
    @IBOutlet weak var chooseVehicleNumberNameLbl: UILabel!
    @IBOutlet weak var chooseVehicleNumberTF: UITextField!
    @IBOutlet weak var chooseVehicleColorNameLbl: UILabel!
    @IBOutlet weak var chooseVehicleColorTF: UITextField!
    @IBOutlet weak var chooseVehicleBrandNameLbl: UILabel!
    @IBOutlet weak var chooseVehicleBrandTF: UITextField!
    @IBOutlet weak var addCarBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var chooseVehicleColorBtn: UIButton!
    @IBOutlet weak var chooseVehicleBrandBtn: UIButton!
    @IBOutlet weak var makeThisDefaultNameLbl: UILabel!
    @IBOutlet weak var makeThisDefaultSwitch: UISwitch!
    
    var carInformationVCDelegate:CarInformationVCDelegate!
    var whereObj = 0
    var selectColorId = 0
    var selectBrandId = 0
    var isDefault = 0
    var selectCarId = 0
    var carDetailsDic = [CarsModel]()
    var dropDownArray = [String]()
    var currentStoreId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        chooseVehicleNumberNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose the vehicle Number", value: "", table: nil))!
        chooseVehicleColorNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose the vehicle Color", value: "", table: nil))!
        chooseVehicleBrandNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Choose the vehicle Brand", value: "", table: nil))!
        makeThisDefaultNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make this as my default car", value: "", table: nil))!
        addCarBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add Car", value: "", table: nil))!, for: .normal)
        makeThisDefaultSwitch.addTarget(self, action: #selector(switchChanged(mySwitch:)), for: .valueChanged)
        if whereObj == 2 || whereObj == 4{
            if carDetailsDic.count > 0{
                selectColorId = carDetailsDic[0].ColorId
                selectBrandId = carDetailsDic[0].BrandId
                selectCarId = carDetailsDic[0].Id
                if carDetailsDic[0].IsDefault == true{
                    isDefault = 1
                }else{
                    isDefault = 0
                }
                chooseVehicleNumberTF.text = carDetailsDic[0].VehicleNumber
                if AppDelegate.getDelegate().appLanguage == "English"{
                    chooseVehicleColorTF.text = carDetailsDic[0].ColorEn
                    chooseVehicleBrandTF.text = carDetailsDic[0].BrandEn
                }else{
                    chooseVehicleColorTF.text = carDetailsDic[0].ColorAr
                    chooseVehicleBrandTF.text = carDetailsDic[0].BrandAr
                }
                addCarBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Update Car", value: "", table: nil))!, for: .normal)
            }else{
                chooseVehicleNumberTF.text = ""
                chooseVehicleColorTF.text = ""
                chooseVehicleBrandTF.text = ""
            }
        }
        makeThisDefaultSwitch.onTintColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        makeThisDefaultSwitch.thumbTintColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        addCarBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    @objc func switchChanged(mySwitch: UISwitch) {
        if makeThisDefaultSwitch.isOn == true{
            isDefault = 1
        }else{
            isDefault = 0
        }
    }
    //MARK: Choose Vehicle Color Button Action
    @IBAction func chooseVehicleColorBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        if SaveAddressClass.carInformationArray.count > 0{
            if let _ = SaveAddressClass.carInformationArray[0].CarColors{
                for dic in SaveAddressClass.carInformationArray[0].CarColors!{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        dropDownArray.append(dic.NameEn)
                    }else{
                        dropDownArray.append(dic.NameAr)
                    }
                }
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        var height = 0
        if dropDownArray.count > 0{
            height = dropDownArray.count * 50
        }else{
            height = 50
        }
        //let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 20 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 2
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Choose Vehicle Brand Button Action
    @IBAction func chooseVehicleBrandBtn_Tapped(_ sender: UIButton) {
        dropDownArray.removeAll()
        if SaveAddressClass.carInformationArray.count > 0{
            if let _ = SaveAddressClass.carInformationArray[0].CarBrands{
                for dic in SaveAddressClass.carInformationArray[0].CarBrands!{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        dropDownArray.append(dic.NameEn)
                    }else{
                        dropDownArray.append(dic.NameAr)
                    }
                }
            }
        }
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        var height = 0
        if dropDownArray.count > 0{
            height = dropDownArray.count * 50
        }else{
            height = 50
        }
        //let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            pVC?.sourceRect = CGRect(x: sender.frame.width - 20 , y: +15, width: 0, height: sender.frame.size.height)
        }else{
            pVC?.sourceRect = CGRect(x: sender.frame.minX + 20 , y: +15, width: 0, height: sender.frame.size.height)
        }
        obj.whereObj = 3
        obj.dropDownArray = dropDownArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 3 || whereObj == 4{
            self.navigationController?.popViewController(animated: true)
        }else{
            carInformationVCDelegate.didAddOrUpdateCar(status: false)
            self.dismiss(animated: true)
        }
    }
    //MARK:Add Car Button Action
    @IBAction func addCarBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        if whereObj == 1 || whereObj == 3{//Create
            let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "Id": 0, "BrandId": selectBrandId, "ColorId": selectColorId, "VehicleNumber": chooseVehicleNumberTF.text!, "IsDefault": isDefault]
            CartModuleServices.AddCustomerCarService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.getCarInformationDetails()
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{//Edit
            let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "Id": selectCarId, "BrandId": selectBrandId, "ColorId": selectColorId, "VehicleNumber": chooseVehicleNumberTF.text!, "IsDefault": 1]
            CartModuleServices.AddCustomerCarService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.getCarInformationDetails()
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    func getCarInformationDetails(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId": currentStoreId]
        CartModuleServices.CarInformationService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.carInformationVCDelegate.didAddOrUpdateCar(status: false)
                self.dismiss(animated: true)
            }else{
                SaveAddressClass.carInformationArray = [data.Data!]
                if self.whereObj == 3 || self.whereObj == 4{
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.carInformationVCDelegate.didAddOrUpdateCar(status: true)
                    self.dismiss(animated: true)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    //MARK: Validations
    func validInputParams() -> Bool {
        if chooseVehicleNumberTF.text == nil || (chooseVehicleNumberTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Enter Vehicle Number", value: "", table: nil))!)
            return false
        }
        if selectColorId == 0{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Color", value: "", table: nil))!)
            return false
        }
        if selectBrandId == 0{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Brand", value: "", table: nil))!)
            return false
        }
        return true
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
// MARK: Drop Down Delegate Methods
@available(iOS 13.0, *)
extension CarInformationVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        if whereType == 2{
            chooseVehicleColorTF.text = name
            var colorDetails = [CarColorsModel]()
            if AppDelegate.getDelegate().appLanguage == "English"{
                colorDetails = SaveAddressClass.carInformationArray[0].CarColors!.filter({$0.NameEn == name})
            }else{
                colorDetails = SaveAddressClass.carInformationArray[0].CarColors!.filter({$0.NameAr == name})
            }
            if colorDetails.count > 0{
                selectColorId = colorDetails[0].Id
            }
        }else{
            chooseVehicleBrandTF.text = name
            var brandDetails = [CarColorsModel]()
            if AppDelegate.getDelegate().appLanguage == "English"{
                brandDetails = SaveAddressClass.carInformationArray[0].CarBrands!.filter({$0.NameEn == name})
            }else{
                brandDetails = SaveAddressClass.carInformationArray[0].CarBrands!.filter({$0.NameAr == name})
            }
            if brandDetails.count > 0{
                selectBrandId = brandDetails[0].Id
            }
        }
    }
}
