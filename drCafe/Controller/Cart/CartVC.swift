//
//  CartVC.swift
//  drCafe
//
//  Created by Devbox on 10/06/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import Firebase
import ImageIO

@available(iOS 13.0, *)
class CartVC: UIViewController, UITabBarControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var serviceLoadView: UIView!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var changeAddressBtn: UIButton!
    @IBOutlet weak var changeAddressNameLbl: UILabel!
    @IBOutlet var changeAddressView: UIView!
    @IBOutlet weak var changeAddressViewWidth: NSLayoutConstraint!
    @IBOutlet weak var changeAddressViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var cartEmptyBtn: UIButton!
    @IBOutlet weak var cartEmptyLbl: UILabel!
    
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var yourOrderNameLbl: UILabel!
    @IBOutlet weak var tryItNameLbl: UILabel!
    @IBOutlet weak var likeItNameLbl: UILabel!
    
    @IBOutlet weak var itemsQuantityLbl: UILabel!
    @IBOutlet weak var orderItemsDropDownBtn: UIButton!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var itemsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var itemsListViewHeight: NSLayoutConstraint!
    @IBOutlet weak var itemsTableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tryItCollectionView: UICollectionView!
    @IBOutlet weak var tryItViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tryItViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var BGView: UIView!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var emptyCartMessageLbl: UILabel!
    @IBOutlet weak var emptyCartView: UIView!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var selectAddressBtn: UIButton!
    @IBOutlet var confirmOrderView: UIView!
    @IBOutlet weak var bottomLineLbl: UILabel!
    
    @IBOutlet weak var netTotalLbl: UILabel!
    @IBOutlet weak var netTotalNameLbl: UILabel!
    @IBOutlet weak var bottomFreeDeliveryMsgLblHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomView: CardView!
    @IBOutlet weak var bottomFreeDeliveryMsgLbl: UILabel!
    @IBOutlet weak var addMenuBtn: CustomButton!
    
    @IBOutlet weak var frequencyNameLbl: UILabel!
    @IBOutlet weak var setTheScheduleNameLbl: UILabel!
    @IBOutlet weak var deliveryFrequencyMainView: UIView!
    @IBOutlet weak var deliveryFrequencyMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryFrequencyMainViewBottom: NSLayoutConstraint!
    @IBOutlet weak var deliveryFrequencyPopUpView: UIView!
    @IBOutlet weak var deliveryFrequencyNameLbl: UILabel!
    @IBOutlet weak var frequencyOneNameLbl: UILabel!
    @IBOutlet weak var frequencyOnePercentageLbl: UILabel!
    @IBOutlet weak var frequeencyOneBGView: UIView!
    @IBOutlet weak var frequeencyTwoBGView: UIView!
    @IBOutlet weak var frequencyTwoNameLbl: UILabel!
    @IBOutlet weak var frequencyTwoPercentageLbl: UILabel!
    @IBOutlet weak var frequencyThreeNameLbl: UILabel!
    @IBOutlet weak var frequencyThreePercentageLbl: UILabel!
    @IBOutlet weak var frequeencyThreeBGView: UIView!
    
    @IBOutlet weak var subscriptionFrequencyNameLbl: UILabel!
    @IBOutlet weak var subscriptionFrequencyMainView: UIView!
    @IBOutlet weak var subscriptionFrequencyMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subscriptionFrequencyMainViewBottom: NSLayoutConstraint!
    @IBOutlet weak var subscriptionFrequencyCV: UICollectionView!
    
    @IBOutlet weak var multipleOrdersTitleLbl: UILabel!
    @IBOutlet weak var multipleOrdersMainView: UIView!
    @IBOutlet weak var multipleOrdersTableView: UITableView!
    
    @IBOutlet weak var redeemRewardsTitleLbl: UILabel!
    @IBOutlet weak var rewardsMsgLbl: UILabel!
    @IBOutlet weak var redeemRewardsCV: UICollectionView!
    @IBOutlet weak var redeemRewardsCVHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemRewardsMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemRewardsCoinImage: UIImageView!
    
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    static var instance: CartVC!
    var cellHeights = [IndexPath: CGFloat]()
    var cartDetailsArray = [LoadCartDataModel]()
    var crossItemDetails = [CrossItemsModel]()
    var allCartDetailsArray = [LoadCartDetailsModel]()
    var MultipleOrderDetailsArray = [MultipleOrdersDetailsModel]()
    var rewardsFilterArray = [RewardsArrayModel]()
    var quantity = 0
    var whereObj = 0
    var netAmount:Double = 0.0
    var deliveryCharge = 0.0
    var vatCharge = 0.0
    var back = false
    var isFirst = false
    
    var isMenu = false
    var orderType:Int!
    var storeId:Int!
    var sectionType:Int!
    var commentText:String = ""
    var selectAddressID = 0
    
    //var isSubscription = false
    var popover = Popover()
    var selectedFrequency = ""
    //var isAnimation = true
    var isSubscription = false
    var orderId = 0
    var ConfirmOrderId = 0
    var isRemove = true
    
    var selectFrequencyIndex:Int!
    var isMultipleOrders = false
    var currentCartStoreId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CartVC.instance = self
        
        //self.tabBarController?.delegate = self
        
        //isRemove = true

        
    }
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if self.tabBarController?.selectedIndex == 2 {
            self.isRemove = true
            self.everyTimeCallingMethod()
        }
    }
    func everyTimeCallingMethod(){
        if whereObj == 0 {
            tabBarController?.tabBar.isHidden = false
        }else{
            tabBarController?.tabBar.isHidden = true
        }
        
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
        tryItCollectionView.delegate = self
        tryItCollectionView.dataSource = self
        multipleOrdersTableView.delegate = self
        multipleOrdersTableView.dataSource = self
        subscriptionFrequencyCV.delegate = self
        subscriptionFrequencyCV.dataSource = self
        redeemRewardsCV.delegate = self
        redeemRewardsCV.dataSource = self
        
        bottomViewHeight.constant = 0
        bottomView.clipsToBounds = true
        netTotalLbl.isHidden = true
        netTotalNameLbl.isHidden = true
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            tryItCollectionView.semanticContentAttribute = .forceLeftToRight
            subscriptionFrequencyCV.semanticContentAttribute = .forceLeftToRight
            redeemRewardsCV.semanticContentAttribute = .forceLeftToRight
        }else{
            tryItCollectionView.semanticContentAttribute = .forceRightToLeft
            subscriptionFrequencyCV.semanticContentAttribute = .forceRightToLeft
            redeemRewardsCV.semanticContentAttribute = .forceRightToLeft
        }
        
        if back == false{
            self.emptyCartView.isHidden = true
            //self.backBtn.isHidden = true
            self.cartEmptyBtn.isHidden = true
            self.cartEmptyLbl.isHidden = true
            self.myScrollView.isHidden = true
            self.confirmOrderView.isHidden = true
            self.serviceLoadView.isHidden = false
            isFirst = true
        }else{
            orderId = ConfirmOrderId
            back = false
        }
        vatCharge = 0.0
        if isUserLogIn() == false {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
            obj.whereObj = 1
            self.navigationController?.pushViewController(obj, animated: false)
            return
        }else{
            if whereObj == 0 {
                if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                    ANLoader.showLoading("", disableUI: true)
                    //if back == false{
                        getOrderDetailsService()
                   // }else{
                    //    getDetailsService()
                    //}
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.getAppVersionService()
                    }
                }else{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                    obj.whereObj = 1
                    self.navigationController?.pushViewController(obj, animated: false)
                    return
                }
           }else{
               ANLoader.showLoading("", disableUI: true)
               getOrderDetailsService()
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                   self.getAppVersionService()
               }
           }
        }
        
        self.multipleOrdersMainView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.emptyCartView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            // User Interface is Dark
            BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            tryItNameLbl.textColor = .white
            likeItNameLbl.textColor = .white
            multipleOrdersTitleLbl.textColor = .white

            emptyCartMessageLbl.textColor = .white
            addMenuBtn.setTitleColor(.black, for: .normal)
            //addMenuBtn.borderColor1 = .white
            addMenuBtn.backgroundColor = .white
        } else {
            // User Interface is Light
            self.bottomView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            tryItNameLbl.textColor = .darkGray
            likeItNameLbl.textColor = .darkGray
            multipleOrdersTitleLbl.textColor = .black

            emptyCartMessageLbl.textColor = .black
            addMenuBtn.setTitleColor(.white, for: .normal)
            //addMenuBtn.borderColor1 = .black
            addMenuBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        }
        selectAddressBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        
        //setTheScheduleNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Set the Schedule", value: "", table: nil))!
        frequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Frequency", value: "", table: nil))!
        deliveryFrequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Frequency", value: "", table: nil))!
        likeItNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You may like also", value: "", table: nil))!
        redeemRewardsTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem Rewards", value: "", table: nil))!
        subscriptionFrequencyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Frequency", value: "", table: nil))!
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidEnterBackground),
                                                      name: UIApplication.didEnterBackgroundNotification,
                                                      object: nil)
               
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnteredForeground),
                                                      name: UIApplication.willEnterForegroundNotification,
                                                      object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        SaveAddressClass.promotionsArray.removeAll()
        self.everyTimeCallingMethod()
        popover.dismiss()
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        DispatchQueue.main.async {
            self.everyTimeCallingMethod()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        popover.dismiss()
    }
    func getOrderDetailsService(){
        let dic:[String : Any] = ["userId":UserDef.getUserId(), "requestBy": 2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("CartDetailsService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        CartModuleServices.CartDetailsWithOutLoaderService(dic: dic, success: { (data) in
            self.serviceLoadView.isHidden = true
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                self.emptyCartView.isHidden = false
                self.cartEmptyBtn.isHidden = true
                self.cartEmptyLbl.isHidden = true
                self.myScrollView.isHidden = true
                self.confirmOrderView.isHidden = true
                AppDelegate.getDelegate().cartQuantity = 0
                AppDelegate.getDelegate().subCartQuantity = 0
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
                if self.whereObj != 0{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        ANLoader.hide()
                    }
                    if self.isSubscription == true{
                        self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your subscription cart is empty. Add something from the menu", value: "", table: nil))!
                    }else{
                        self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu", value: "", table: nil))!
                    }
                }
            }else{
                self.MultipleOrderDetailsArray = data.Data!
                if data.Data != nil{
                    if self.whereObj == 0 {
                        if self.isMultipleOrders == true{
                            self.isMultipleOrders = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                                ANLoader.hide()
                            }
                            if data.Data!.count == 0{
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                                obj.whereObj = 1
                                self.navigationController?.pushViewController(obj, animated: false)
                                return
                            }else{
                                self.emptyCartView.isHidden = true
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                                self.bottomView.isHidden = true
                                self.multipleOrdersMainView.isHidden = false
                                self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Carts", value: "", table: nil))!
                                self.multipleOrdersTableView.reloadData()
                                self.multipleOrdersTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select the cart you want to check out first", value: "", table: nil))!
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[2]
                                    tabItem.badgeValue = "\(self.MultipleOrderDetailsArray[0].CartItemsQty)"
                                    tabItems[2].image = UIImage(named: "Cart Tab")
                                    tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                                }
                            }
                        }else{
                            if data.Data!.count > 1{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                                    ANLoader.hide()
                                }
                                self.emptyCartView.isHidden = true
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                                self.bottomView.isHidden = true
                                self.multipleOrdersMainView.isHidden = false
                                self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Carts", value: "", table: nil))!
                                self.multipleOrdersTableView.reloadData()
                                self.multipleOrdersTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select the cart you want to check out first", value: "", table: nil))!
                                DispatchQueue.main.async {
                                    ANLoader.hide()
                                }
                            }else if data.Data!.count == 0{
                                DispatchQueue.main.async {
                                    ANLoader.hide()
                                }
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                                obj.whereObj = 2
                                self.navigationController?.pushViewController(obj, animated: false)
                            }else{
                                self.multipleOrdersMainView.isHidden = true
                                self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                                self.orderId = data.Data![0].Id
                                self.currentCartStoreId = data.Data![0].StoreId
                                self.isSubscription = data.Data![0].IsSubscription
                                if self.isSubscription == false && self.sectionType == 1{
                                    self.addressUpdateService(addressId: self.selectAddressID, OrderId: data.Data![0].Id)
                                }else{
                                    self.getDetailsService()
                                }
                                //self.getDetailsService()
                            }
                        }
                    }else{
                        self.multipleOrdersMainView.isHidden = true
                        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                        if self.isSubscription == true{
                            let subArray = data.Data!.filter({$0.IsSubscription == true})
                            if subArray.count > 0{
                                self.orderId = subArray[0].Id
                                self.currentCartStoreId = subArray[0].StoreId
                                if self.isSubscription == false && self.sectionType == 1{
                                    self.addressUpdateService(addressId: self.selectAddressID, OrderId: subArray[0].Id)
                                }else{
                                    self.getDetailsService()
                                }
                                //self.getDetailsService()
                            }else{
                                //self.orderId = data.Data![0].Id
                                self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your subscription cart is empty. Add something from the menu", value: "", table: nil))!
                                self.emptyCartView.isHidden = false
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    ANLoader.hide()
                                }
                            }
                        }else{
                            let subArray = data.Data!.filter({$0.IsSubscription == false})
                            if subArray.count > 0{
                                self.orderId = subArray[0].Id
                                self.currentCartStoreId = subArray[0].StoreId
                                if self.isSubscription == false && self.sectionType == 1{
                                    self.addressUpdateService(addressId: self.selectAddressID, OrderId: subArray[0].Id)
                                }else{
                                    self.getDetailsService()
                                }
                                //self.getDetailsService()
                            }else{
                                //self.orderId = data.Data![0].Id
                                self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu", value: "", table: nil))!
                                self.emptyCartView.isHidden = false
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    ANLoader.hide()
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                    obj.whereObj = 2
                    self.navigationController?.pushViewController(obj, animated: false)
                }
            }
        }) { (error) in
            //self.backBtn.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getDetailsService(){
        self.navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
        var dic:[String : Any]!
        if isMenu == true{
            dic = ["Orderid":orderId, "storeId":storeId!, "OrderTypeId":orderType!, "sectiontype":sectionType!, "requestBy": 2, "IsRemove":isRemove, "IsSubscription": isSubscription]
        }else{
            dic = ["Orderid":orderId, "storeId":0, "OrderTypeId":0, "sectiontype":0, "requestBy": 2, "IsRemove":isRemove, "IsSubscription": isSubscription]
        }
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("LoadCartService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */

        CartModuleServices.LoadCartWithOutLoaderService(dic: dic, success: { (data) in
            self.serviceLoadView.isHidden = true
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                self.emptyCartView.isHidden = false
                self.cartEmptyBtn.isHidden = true
                self.cartEmptyLbl.isHidden = true
                self.myScrollView.isHidden = true
                self.confirmOrderView.isHidden = true
                AppDelegate.getDelegate().cartQuantity = 0
                AppDelegate.getDelegate().subCartQuantity = 0
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
//                if self.whereObj == 0 {
//                    self.backBtn.isHidden = true
//                }else{
//                    self.backBtn.isHidden = false
//                }
            }else{
                //self.backBtn.isHidden = false
                //ANLoader.showLoading("", disableUI: true)
                self.multipleOrdersMainView.isHidden = true
                self.bottomView.isHidden = false
                if data.Data != nil{
                    self.cartDetailsArray = [data.Data!]
                }
                if data.Data!.Carts != nil{
                    if data.Data!.Carts!.count > 0{
                        self.allCartDetailsArray = [data.Data!.Carts![0]]
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            if self.isFirst == true{
                                if self.allCartDetailsArray[0].Cart![0].SectionType! == 2 || self.isSubscription == true{
                                    if self.allCartDetailsArray[0].Address!.count > 0{
                                        if self.allCartDetailsArray[0].Address![0].Id == nil{
                                            ANLoader.showLoading("", disableUI: true)
                                            self.getAddressDetailsService()
                                        }
                                    }else{
                                        ANLoader.showLoading("", disableUI: true)
                                        self.getAddressDetailsService()
                                    }
                                }
                                self.isFirst = false
                            }
                        }
                        DispatchQueue.main.async {
                            if self.allCartDetailsArray.count > 0{
                                if self.allCartDetailsArray[0].CrossItems != nil{
                                    if self.allCartDetailsArray[0].CrossItems!.count > 0{
                                        self.crossItemDetails = self.allCartDetailsArray[0].CrossItems!.filter({$0.Id != nil})
                                        if self.crossItemDetails.count > 0{
                                            DispatchQueue.main.async {
                                                self.tryItCollectionView.reloadData()
                                            }
                                            
                                            self.tryItViewHeight.constant = 276
                                            self.tryItViewBottomConstraint.constant = 15
                                            //self.tryItCollectionView.reloadData()
                                        }else{
                                            self.tryItViewHeight.constant = 0
                                            self.tryItViewBottomConstraint.constant = 0
                                        }
                                    }else{
                                        self.tryItViewHeight.constant = 0
                                        self.tryItViewBottomConstraint .constant = 0
                                    }
                                }else{
                                    self.tryItViewHeight.constant = 0
                                    self.tryItViewBottomConstraint .constant = 0
                                }
                            }
                            let items = data.Data!.Carts!.filter({$0.Items!.count > 0})
                            if items.count > 0{
                                self.myScrollView.isHidden = false
                                self.confirmOrderView.isHidden = false
                                self.AllData()
                            }else{
                                self.emptyCartView.isHidden = false
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                AppDelegate.getDelegate().cartQuantity = 0
                                AppDelegate.getDelegate().subCartQuantity = 0
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[2]
                                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                                        if self.isSubscription == true{
                                            tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                                        }else{
                                            tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                                        }
                                        tabItems[2].image = UIImage(named: "Cart Tab")
                                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                                    }else{
                                        tabItem.badgeValue = nil
                                        tabItems[2].image = UIImage(named: "Tab Order")
                                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                                    }
                                }
                            }
                        }
                        if self.allCartDetailsArray[0].Cart![0].FrequencyId != nil{
                            if self.allCartDetailsArray[0].Cart![0].FrequencyId! == 0{
                                self.selectedFrequency = ""
                            }
                        }else{
                            self.selectedFrequency = ""
                        }
                    }else{
                        self.emptyCartView.isHidden = false
                        self.cartEmptyBtn.isHidden = true
                        self.cartEmptyLbl.isHidden = true
                        AppDelegate.getDelegate().cartQuantity = 0
                        AppDelegate.getDelegate().subCartQuantity = 0
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                                if self.isSubscription == true{
                                    tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                                }else{
                                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                                }
                                tabItems[2].image = UIImage(named: "Cart Tab")
                                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                            }else{
                                tabItem.badgeValue = nil
                                tabItems[2].image = UIImage(named: "Tab Order")
                                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                            }
                        }
                    }
                }else{
                    self.emptyCartView.isHidden = false
                    self.cartEmptyBtn.isHidden = true
                    self.cartEmptyLbl.isHidden = true
                    AppDelegate.getDelegate().cartQuantity = 0
                    AppDelegate.getDelegate().subCartQuantity = 0
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                            if self.isSubscription == true{
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                            }else{
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                            }
                            tabItems[2].image = UIImage(named: "Cart Tab")
                            tabItems[2].selectedImage = UIImage(named: "Cart Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                        }else{
                            tabItem.badgeValue = nil
                            tabItems[2].image = UIImage(named: "Tab Order")
                            tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
        }) { (error) in
            //self.backBtn.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Get Address Details Service
    func getAddressDetailsService(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId" : 0, "SectionType": 0]
        OrderModuleServices.AddressListService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                //ANLoader.hide()
                let addressDetails = data.Data!.filter({$0.Default == 1})
                if addressDetails.count > 0{
                    DispatchQueue.main.async { [self] in
                        self.addressUpdateService(addressId: addressDetails[0].Id, OrderId: self.allCartDetailsArray[0].Cart![0].Id!)
//                        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "OrderId" : self.allCartDetailsArray[0].Cart![0].Id!, "AddressId": addressDetails[0].Id]
//                        UserModuleServices.updateOrderAddressService(dic: dic, success: { (data) in
//                            if(data.Status == false){
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                                    ANLoader.hide()
//                                }
//                                if AppDelegate.getDelegate().appLanguage == "English"{
//                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                                }else{
//                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                                }
//                            }else{
//                                //ANLoader.hide()
//                                isRemove = false
//                                ANLoader.showLoading("", disableUI: true)
//                                self.getDetailsService()
//                            }
//                        }) { (error) in
//                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
//                        }
                    }
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func addressUpdateService(addressId:Int, OrderId:Int){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "OrderId" : OrderId, "AddressId": addressId]
        UserModuleServices.updateOrderAddressService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                //ANLoader.hide()
                self.isRemove = false
                //ANLoader.showLoading("", disableUI: true)
                self.getDetailsService()
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getAppVersionService(){
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        let dic = ["AppVersion":"\(appversion)","AppType":"IOS","RequestBy":2] as [String : Any]
        CartModuleServices.GetAppVersionService(dic: dic, success: { (data) in
            if(data.Status == true){
                DispatchQueue.main.async {
                    //Update App
                    if let _ = data.Data{
                        if let _ = data.Data!.Severity{
                            if data.Data!.Severity! == 1{
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "UpgradeAppVC") as! UpgradeAppVC
                                obj.modalPresentationStyle = .overCurrentContext
                                obj.modalPresentationStyle = .overFullScreen
                                self.present(obj, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
        }
    }
    func AllData(){
        if allCartDetailsArray[0].Cart![0].CartItemsQuantity != 0{
            emptyCartView.isHidden = true
            myScrollView.isHidden = false
            self.confirmOrderView.isHidden = false
            //self.backBtn.isHidden = false
            self.cartEmptyBtn.isHidden = false
            self.cartEmptyLbl.isHidden = false
        }else{
            emptyCartView.isHidden = false
            myScrollView.isHidden = true
            self.confirmOrderView.isHidden = true
            self.cartEmptyBtn.isHidden = true
            self.cartEmptyLbl.isHidden = true
        }
        
        checkoutBtn.isHidden = false
        selectAddressBtn.isHidden = false
        //bottomLineLbl.isHidden = false
        selectAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add More", value: "", table: nil))!, for: .normal)
        
        //Swathi
        if self.allCartDetailsArray[0].RewardsArray != nil && self.allCartDetailsArray[0].Items != nil{
            rewardsFilterArray.removeAll()
            if allCartDetailsArray[0].RewardsArray!.count > 0{
                for i in 0...allCartDetailsArray[0].RewardsArray!.count - 1{
                    for j in 0...allCartDetailsArray[0].Items!.count - 1{
                        if allCartDetailsArray[0].RewardsArray![i].RewardBand! == allCartDetailsArray[0].Items![j].RewardBand!{
                            if allCartDetailsArray[0].Items![j].CatalogChanged! == false{
                                let catalogA = allCartDetailsArray[0].Items![j].Additionals!
                                let catalogAdditionals = catalogA.filter({$0.CatalogChanged == 1})
                                if catalogAdditionals.count > 0{
                                    print("Additional catalog changed")
                                }else{
                                    if allCartDetailsArray[0].Items![j].IsOutOfStock! == 1{
                                        print("Out Of Stock")
                                    }else{
                                        let outOfStockA = allCartDetailsArray[0].Items![j].Additionals!
                                        let outOfStockAdditionals = outOfStockA.filter({$0.IsOutOfStock == 1})
                                        if outOfStockAdditionals.count > 0{
                                            print("Additional Out Of Stock")
                                        }else{
                                            if rewardsFilterArray.count > 0{
                                                let reward = rewardsFilterArray.filter({$0.RewardBand! == allCartDetailsArray[0].RewardsArray![i].RewardBand!})
                                                if reward.count == 0{
                                                    rewardsFilterArray.append(allCartDetailsArray[0].RewardsArray![i])
                                                }
                                            }else{
                                                rewardsFilterArray.append(allCartDetailsArray[0].RewardsArray![i])
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("catalog changed")
                            }
                        }
                    }
                }
            }
        }
        if rewardsFilterArray.count > 0{
            redeemRewardsCV.reloadData()
            var rewardsMsgLblHeight = 21
            if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                let reward = self.allCartDetailsArray[0].Items!.filter({$0.ConsumedPoints! == self.allCartDetailsArray[0].Cart![0].ConsumedPoints!})
                if reward.count > 0{
                    rewardsMsgLbl.text = "\(self.allCartDetailsArray[0].Cart![0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied to your", value: "", table: nil))!) \(reward[0].ItemNameEn!)"
                    rewardsMsgLblHeight = Int(Double(heightForView(text: "\(self.allCartDetailsArray[0].Cart![0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied to your", value: "", table: nil))!) \(reward[0].ItemNameEn!)", font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
                }else{
                    rewardsMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!
                    rewardsMsgLblHeight = Int(Double(heightForView(text: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!, font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
                }
            }else{
                rewardsMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!
                rewardsMsgLblHeight = Int(Double(heightForView(text: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!, font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
            }

            var redeemCVHeight = 30
            if rewardsFilterArray.count%2 == 0{
                redeemCVHeight = 30 * (rewardsFilterArray.count/2)
            }else{
                if rewardsFilterArray.count == 1{
                    redeemCVHeight = 30
                }else{
                    redeemCVHeight = Int(30 * (rewardsFilterArray.count/2) + 1)
                }
            }
            redeemRewardsCVHeight.constant = CGFloat(redeemCVHeight)
            redeemRewardsMainViewHeight.constant = CGFloat(90 + rewardsMsgLblHeight + redeemCVHeight)
            
            self.redeemRewardsCoinImage.animationDuration = 1
            self.redeemRewardsCoinImage.animationRepeatCount = 0
            var images = [UIImage]()
            for i in 1...14
            {
                images.append(UIImage(named: "Cup \(i)")!)
            }
            self.redeemRewardsCoinImage.animationImages = images
            self.redeemRewardsCoinImage.startAnimating()
        }else{
            print("Empty")
            redeemRewardsCVHeight.constant = 0
            redeemRewardsMainViewHeight.constant = 0
        }
        
//        if self.allCartDetailsArray[0].RewardsArray != nil{
//            if self.allCartDetailsArray[0].RewardsArray!.count > 0{
//                redeemRewardsCV.reloadData()
//                var rewardsMsgLblHeight = 21
//                if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
//                    let reward = self.allCartDetailsArray[0].Items!.filter({$0.ConsumedPoints! == self.allCartDetailsArray[0].Cart![0].ConsumedPoints!})
//                    if reward.count > 0{
//                        rewardsMsgLbl.text = "\(self.allCartDetailsArray[0].Cart![0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied to your", value: "", table: nil))!) \(reward[0].ItemNameEn!)"
//                        rewardsMsgLblHeight = Int(Double(heightForView(text: "\(self.allCartDetailsArray[0].Cart![0].ConsumedPoints!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied to your", value: "", table: nil))!) \(reward[0].ItemNameEn!)", font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
//                    }else{
//                        rewardsMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!
//                        rewardsMsgLblHeight = Int(Double(heightForView(text: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!, font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
//                    }
//                }else{
//                    rewardsMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!
//                    rewardsMsgLblHeight = Int(Double(heightForView(text: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats! you can redeem your points now", value: "", table: nil))!, font: UIFont.init(name: "Avenir Medium", size: 14.0)!, width: view.frame.width - 135)))
//                }
//
//                var redeemCVHeight = 30
//                if allCartDetailsArray[0].RewardsArray!.count%2 == 0{
//                    redeemCVHeight = 30 * (allCartDetailsArray[0].RewardsArray!.count/2)
//                }else{
//                    if allCartDetailsArray[0].RewardsArray!.count == 1{
//                        redeemCVHeight = 30
//                    }else{
//                        redeemCVHeight = Int(30 * (allCartDetailsArray[0].RewardsArray!.count/2) + 1)
//                    }
//                }
//                redeemRewardsCVHeight.constant = CGFloat(redeemCVHeight)
//                redeemRewardsMainViewHeight.constant = CGFloat(90 + rewardsMsgLblHeight + redeemCVHeight)
//
//                //let jeremyGif = UIImage.gifImageWithName("Cup.gif")
//                //redeemRewardsCoinImage = UIImageView(image: jeremyGif)
//
//                self.redeemRewardsCoinImage.animationDuration = 1
//                self.redeemRewardsCoinImage.animationRepeatCount = 0
//                var images = [UIImage]()
//                for i in 1...14
//                {
//                    images.append(UIImage(named: "Cup \(i)")!)
//                }
//                self.redeemRewardsCoinImage.animationImages = images
//                self.redeemRewardsCoinImage.startAnimating()
//            }else{
//                print("Empty")
//                redeemRewardsCVHeight.constant = 0
//                redeemRewardsMainViewHeight.constant = 0
//            }
//        }else{
//            print("Empty")
//            redeemRewardsCVHeight.constant = 0
//            redeemRewardsMainViewHeight.constant = 0
//        }
        
        netTotalNameLbl.isHidden = false
        netTotalLbl.isHidden = false
        netTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Total", value: "", table: nil))!
        if allCartDetailsArray[0].Cart![0].OrderType == "1"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DineIn", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DineIn at:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
        }else if allCartDetailsArray[0].Cart![0].OrderType == "2"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DriveThru", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DriveThru From:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
        }else if allCartDetailsArray[0].Cart![0].OrderType == "3"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pickup From:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = allCartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
        }else if allCartDetailsArray[0].Cart![0].OrderType == "4"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
            changeAddressBtn.isEnabled = true
            changeAddressViewWidth.constant = 80
            changeAddressViewTrailing.constant = 10
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivering to:", value: "", table: nil))!
            if allCartDetailsArray[0].Address!.count > 0{
                if let address = allCartDetailsArray[0].Address![0].Address {
                    addressLbl.text = address
                    changeAddressNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Address", value: "", table: nil))!
                }else{
                    addressLbl.text = ""
                    checkoutBtn.isHidden = true
                    selectAddressBtn.isHidden = false
                    //bottomLineLbl.isHidden = true
                    selectAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!, for: .normal)
                    
                    netTotalNameLbl.isHidden = false
                    netTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select delivery address", value: "", table: nil))!
                    netTotalLbl.isHidden = true
                    changeAddressNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!
                }
            }else{
                addressLbl.text = ""
                checkoutBtn.isHidden = true
                selectAddressBtn.isHidden = false
                //bottomLineLbl.isHidden = true
                selectAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!, for: .normal)
                
                netTotalNameLbl.isHidden = false
                netTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select delivery address", value: "", table: nil))!
                netTotalLbl.isHidden = true
                changeAddressNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!
            }
        }
        quantity = 0
        for i in 0...allCartDetailsArray[0].Items!.count-1{
            quantity = quantity + allCartDetailsArray[0].Items![i].Quantity!
        }
        //itemsQuantityLbl.text = "\(allCartDetailsArray[0].Items!.count)"
        itemsQuantityLbl.text = "\(allCartDetailsArray[0].Cart![0].CartItemsQuantity!)"
        if isSubscription == true{
            AppDelegate.getDelegate().subCartQuantity = allCartDetailsArray[0].Cart![0].CartItemsQuantity!
            AppDelegate.getDelegate().subCartAmount = allCartDetailsArray[0].Cart![0].NetTotal!
        }else{
            AppDelegate.getDelegate().cartQuantity = allCartDetailsArray[0].Cart![0].CartItemsQuantity!
            AppDelegate.getDelegate().cartAmount = allCartDetailsArray[0].Cart![0].NetTotal!
        }
        //AppDelegate.getDelegate().cartQuantity = quantity
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                if isSubscription == true{
                    tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                }else{
                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                }
                //tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        
        if allCartDetailsArray[0].Cart![0].SectionType! != 2{
            deliveryCharge = allCartDetailsArray[0].Cart![0].DeliveryCharge!
            vatCharge = 0.0
        }
        
        //Items Display
        itemsTableView.reloadData()
        itemsListViewHeight.constant = itemsTableView.contentSize.height + 50
        itemsTableViewHeight.constant = itemsTableView.contentSize.height
        if AppDelegate.getDelegate().appLanguage == "English"{
            orderItemsDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
        }else{
            orderItemsDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
        }
        if allCartDetailsArray[0].Cart![0].NetTotal != nil{
            netAmount = allCartDetailsArray[0].Cart![0].ItemTotal! + vatCharge + deliveryCharge + allCartDetailsArray[0].Cart![0].Vatcharges!
            netTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(allCartDetailsArray[0].Cart![0].NetTotal!.withCommas())"
        }else{
            netTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        }
        bottomViewHeight.constant = 110
        netTotalLbl.isHidden = false
        netTotalNameLbl.isHidden = false
        
        //Frequency
        if allCartDetailsArray[0].Cart![0].IsSubscription != nil{
            if allCartDetailsArray[0].Cart![0].IsSubscription! == true{
                self.subscriptionFrequencyCV.reloadData()
                DispatchQueue.main.async {
                    self.subscriptionFrequencyCV.reloadData()
                }
                var subFrequencyHeight = 142
                if allCartDetailsArray[0].Frequency!.count <= 3 && allCartDetailsArray[0].Frequency!.count > 0{
                    subFrequencyHeight = 142
                }else{
                    if allCartDetailsArray[0].Frequency!.count%3 == 0{
                        subFrequencyHeight = 52 + (90 * allCartDetailsArray[0].Frequency!.count/3)
                    }else{
                        if allCartDetailsArray[0].Frequency!.count+1%3 == 0{
                            subFrequencyHeight = 52 + (90 * allCartDetailsArray[0].Frequency!.count+1/3)
                        }else if allCartDetailsArray[0].Frequency!.count+2%3 == 0{
                            subFrequencyHeight = 52 + (90 * allCartDetailsArray[0].Frequency!.count+2/3)
                        }else{
                            subFrequencyHeight = 142
                        }
                    }
                }
                subscriptionFrequencyMainViewHeight.constant = CGFloat(subFrequencyHeight)
                subscriptionFrequencyMainViewBottom.constant = 15
            }else{
                subscriptionFrequencyMainViewHeight.constant = 0
                subscriptionFrequencyMainViewBottom.constant = 0
            }
        }else{
            subscriptionFrequencyMainViewHeight.constant = 0
            subscriptionFrequencyMainViewBottom.constant = 0
        }
        
        //Frequency
//        if allCartDetailsArray[0].Cart![0].IsSubscription != nil{
//            if allCartDetailsArray[0].Cart![0].IsSubscription! == true{
//                if allCartDetailsArray[0].Frequency != nil{
//                    if allCartDetailsArray[0].Frequency!.count > 0{
//                        if allCartDetailsArray[0].Frequency!.count == 3{
//                            if AppDelegate.getDelegate().appLanguage == "English"{
//                                frequencyOneNameLbl.text = allCartDetailsArray[0].Frequency![0].NameEn!
//                                frequencyTwoNameLbl.text = allCartDetailsArray[0].Frequency![1].NameEn!
//                                frequencyThreeNameLbl.text = allCartDetailsArray[0].Frequency![2].NameEn!
//                            }else{
//                                frequencyOneNameLbl.text = allCartDetailsArray[0].Frequency![0].NameAr!
//                                frequencyTwoNameLbl.text = allCartDetailsArray[0].Frequency![1].NameAr!
//                                frequencyThreeNameLbl.text = allCartDetailsArray[0].Frequency![2].NameAr!
//                            }
//                            frequencyOnePercentageLbl.text = "\(allCartDetailsArray[0].Frequency![0].Discount!.withCommas())%"
//                            frequencyTwoPercentageLbl.text = "\(allCartDetailsArray[0].Frequency![1].Discount!.withCommas())%"
//                            frequencyThreePercentageLbl.text = "\(allCartDetailsArray[0].Frequency![2].Discount!.withCommas())%"
//                        }
//                    }
//                }
//            }
//        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            if AppDelegate.getDelegate().isCartAnimationFirst == false{
//                AppDelegate.getDelegate().isCartAnimationFirst = true
//                self.swipeCell()
//            }
            if UserDefaults.standard.value(forKey: "CartSwipe") == nil{
                self.swipeCell()
            }
        }
    }
    func swipeCell(){
        if !UserDefaults.standard.bool(forKey: "CartSwipe") {
            UserDefaults.standard.set(false, forKey: "CartSwipe")
            UserDefaults.standard.synchronize()
        }
        //Automatic Swipe item Cell functionality
        let indexPath = NSIndexPath(row: 0, section: 0)
        let cell = itemsTableView.cellForRow(at: indexPath as IndexPath);

        let swipeView = UIView()
        swipeView.frame = CGRect(x: cell!.bounds.size.width, y: 0, width: 160, height: cell!.bounds.size.height)
        swipeView.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.8823529412, blue: 0.8862745098, alpha: 1)
        
        let swipeCommentView: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 80, height: cell!.bounds.size.height))
        swipeCommentView.backgroundColor = .clear
        swipeView.addSubview(swipeCommentView)
        
        let swipeCommentImg: UIImageView = UIImageView.init(frame: CGRect(x: swipeCommentView.frame.width/2 - 15, y: swipeCommentView.frame.height/2 - 12.5, width: 30, height: 25))
        swipeCommentImg.image = UIImage(named: "note")
        swipeCommentImg.backgroundColor = .clear
        swipeCommentView.addSubview(swipeCommentImg)
        
        let swipeDeleteView: UILabel = UILabel.init(frame: CGRect(x: swipeCommentView.frame.size.width, y: 0, width: 80, height: cell!.bounds.size.height))
        swipeDeleteView.backgroundColor = .clear
        swipeView.addSubview(swipeDeleteView)
        
        let swipeDeleteImg: UIImageView = UIImageView.init(frame: CGRect(x: swipeDeleteView.frame.width/2 - 10, y: swipeDeleteView.frame.height/2 - 12.5, width: 20, height: 25))
        swipeDeleteImg.image = UIImage(named: "delete")
        swipeDeleteImg.backgroundColor = .clear
        swipeDeleteView.addSubview(swipeDeleteImg)

        cell!.addSubview(swipeView)

        UIView.animate(withDuration: 0.80, animations: {
            cell!.frame = CGRect(x: cell!.frame.origin.x - 160, y: cell!.frame.origin.y, width: cell!.bounds.size.width + 160, height: cell!.bounds.size.height)
        }) { (finished) in
            UIView.animate(withDuration: 0.80, animations: {
                cell!.frame = CGRect(x: cell!.frame.origin.x + 160, y: cell!.frame.origin.y, width: cell!.bounds.size.width - 160, height: cell!.bounds.size.height)

            }, completion: { (finished) in
                for subview in swipeView.subviews {
                    subview.removeFromSuperview()
                }
                swipeView.removeFromSuperview()
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            let indexpath = IndexPath(row: 0, section: 0)
            self.itemsTableView.reloadRows(at: [indexpath], with: .none)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        print(allCartDetailsArray)
        print(MultipleOrderDetailsArray)
        if allCartDetailsArray.count > 0{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = MenuVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            obj.isCart = true
            if allCartDetailsArray[0].Cart![0].IsSubscription != nil{
                obj.isSubscription = allCartDetailsArray[0].Cart![0].IsSubscription!
            }else{
                obj.isSubscription = false
            }
            if allCartDetailsArray[0].Cart![0].OrderType! == "4"{
                obj.whereObj = 1
                obj.backObj = 11
                if allCartDetailsArray[0].Address!.count > 0{
                    if allCartDetailsArray[0].Address![0].Id != nil{
                        var deliveryInfo = ""
                        if allCartDetailsArray[0].Address![0].DeliveryInfo != nil{
                            deliveryInfo = allCartDetailsArray[0].Address![0].DeliveryInfo!
                        }
                        obj.addressDetailsArray = [AddressDetailsModel(Id: allCartDetailsArray[0].Address![0].Id!, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: allCartDetailsArray[0].Address![0].Name!, LandMark: allCartDetailsArray[0].Address![0].Landmark!, Address: allCartDetailsArray[0].Address![0].Address!, AddressType: allCartDetailsArray[0].Address![0].TypeId!, CountryCode: "", Default: 0, Latitude: allCartDetailsArray[0].Address![0].Latitude!, Longitude: allCartDetailsArray[0].Address![0].Longitude!, ContactPerson: allCartDetailsArray[0].Address![0].ContactPerson!, ContactNo: allCartDetailsArray[0].Address![0].ContactPersonPhone!, DeliveryInfo: deliveryInfo)]
                    }
                }
                obj.selectOrderType = 4
                obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
            }else{
                obj.whereObj = 2
                obj.backObj = 11
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.address = allCartDetailsArray[0].Stores![0].NameEn!
                }else{
                    obj.address = allCartDetailsArray[0].Stores![0].NameAr!
                }
                obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                obj.selectOrderType = Int(allCartDetailsArray[0].Cart![0].OrderType!)!
            }
            //if allCartDetailsArray[0].Cart![0].SectionType! == 2{
                obj.sectionType = allCartDetailsArray[0].Cart![0].SectionType!
            //}else{
              //  obj.sectionType = 1
            //}
            self.navigationController?.pushViewController(obj, animated: false)
        }else if MultipleOrderDetailsArray.count > 1{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = MenuVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            obj.isCart = true
            obj.isSubscription = MultipleOrderDetailsArray[0].IsSubscription
            if MultipleOrderDetailsArray[0].OrderTypeId == "4"{
                obj.whereObj = 1
                obj.backObj = 11
                if MultipleOrderDetailsArray[0].UserAddressDetails!.count > 0{
                    var addressId = 0
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Id != nil{
                        addressId = MultipleOrderDetailsArray[0].UserAddressDetails![0].Id!
                    }
                    var name = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Name != nil{
                        name = MultipleOrderDetailsArray[0].UserAddressDetails![0].Name!
                    }
                    var landMark = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].LandMark != nil{
                        landMark = MultipleOrderDetailsArray[0].UserAddressDetails![0].LandMark!
                    }
                    var address = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Address != nil{
                        address = MultipleOrderDetailsArray[0].UserAddressDetails![0].Address!
                    }
                    var typeId = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].TypeId != nil{
                        typeId = MultipleOrderDetailsArray[0].UserAddressDetails![0].TypeId!
                    }
                    var Latitude = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Latitude != nil{
                        Latitude = MultipleOrderDetailsArray[0].UserAddressDetails![0].Latitude!
                    }
                    var Longitude = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Longitude != nil{
                        Longitude = MultipleOrderDetailsArray[0].UserAddressDetails![0].Longitude!
                    }
                    var contactPerson = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].ContactPerson != nil{
                        contactPerson = MultipleOrderDetailsArray[0].UserAddressDetails![0].ContactPerson!
                    }
                    var contactPersonPhone = ""
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].ContactPersonPhone != nil{
                        contactPersonPhone = MultipleOrderDetailsArray[0].UserAddressDetails![0].ContactPersonPhone!
                    }
                    if MultipleOrderDetailsArray[0].UserAddressDetails![0].Id != nil{
                        obj.addressDetailsArray = [AddressDetailsModel(Id: addressId, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: name, LandMark: landMark, Address: address, AddressType: typeId, CountryCode: "", Default: 0, Latitude: Latitude, Longitude: Longitude, ContactPerson: contactPerson, ContactNo: contactPersonPhone, DeliveryInfo: "")]
                    }
                }
                obj.selectOrderType = 4
                obj.storeID = MultipleOrderDetailsArray[0].StoreId
            }else{
                obj.whereObj = 2
                obj.backObj = 11
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.address = MultipleOrderDetailsArray[0].StoreNameEn
                }else{
                    obj.address = MultipleOrderDetailsArray[0].StoreNameAr
                }
                obj.storeID = MultipleOrderDetailsArray[0].StoreId
                obj.selectOrderType = Int(MultipleOrderDetailsArray[0].OrderTypeId)!
            }
            //if MultipleOrderDetailsArray[0].SectionType == 2{
                obj.sectionType = MultipleOrderDetailsArray[0].SectionType
            //}else{
               // obj.sectionType = 1
            //}
            self.navigationController?.pushViewController(obj, animated: false)
        } else{
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func addMenuBtn_Tapped(_ sender: Any) {
//        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//        let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
//        obj.whereObj = 2
//        self.navigationController?.pushViewController(obj, animated: false)
        //self.tabBarController?.selectedIndex = 0
        AppDelegate.getDelegate().tabbarViewController()
    }
    //MARK: Empty Cart Button Action
    @IBAction func emptyCartBtn_Tapped(_ sender: Any) {
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart Clear Alert", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Clear", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            let dic = ["UserId": UserDef.getUserId(), "OrderId": self.allCartDetailsArray[0].Cart![0].Id!, "RequestBy":2] as [String : Any]
            CartModuleServices.EmptyCartService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showToastDown(on: self, message: data.MessageEn)
                    }else{
                        Alert.showToastDown(on: self, message: data.MessageAr)
                    }
                    DispatchQueue.main.async {
                        if self.whereObj == 0 {
                            self.tabBarController?.tabBar.isHidden = false
                        }else{
                            self.tabBarController?.tabBar.isHidden = true
                        }
                        if self.isSubscription == true{
                            AppDelegate.getDelegate().subCartQuantity = 0
                            AppDelegate.getDelegate().subCartAmount = 0
                        }else{
                            AppDelegate.getDelegate().cartQuantity = 0
                            AppDelegate.getDelegate().cartAmount = 0
                        }
                        AppDelegate.getDelegate().cartStoreId = 0
                        AppDelegate.getDelegate().cartStoreName = ""
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            tabItem.badgeValue = nil
                            tabItems[2].image = UIImage(named: "Tab Order")
                            tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                        }
                        if self.MultipleOrderDetailsArray.count == 2{
                            self.isMultipleOrders = true
                            ANLoader.showLoading("", disableUI: true)
                            self.getOrderDetailsService()
                        }else{
                            if self.whereObj == 0 {
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                                obj.whereObj = 1
                                self.navigationController?.pushViewController(obj, animated: false)
                                return
                            }else{
                                if self.isSubscription == true{
                                    self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your subscription cart is empty. Add something from the menu", value: "", table: nil))!
                                }else{
                                    self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu", value: "", table: nil))!
                                }
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.emptyCartView.isHidden = false
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                            }
                        }
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            return
        }))
    }
    @IBAction func checkoutBtn_Tapped(_ sender: Any) {
        let catalog = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! == true})
        if catalog.count > 0{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is currently unavailable", value: "", table: nil))!)
        }else{
            let catalogA = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! != true})
            let catalogAdditionals = catalogA.filter({$0.CatalogChanged! == true})
            if catalogAdditionals.count > 0{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is currently unavailable", value: "", table: nil))!)
            }else{
                let outOfStock = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! == 1})
                if outOfStock.count > 0{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is out of stock", value: "", table: nil))!)
                }else{
                    let outOfStockA = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! != 1})
                    var outOfStockAdditionals:[CartAdditionalsModel]!
                    outOfStockAdditionals = [CartAdditionalsModel(GroupId: 0, AdditionalId: 0, NameEn: "", NameAr: "", Quantity: 0, Image: "", Price: 0.0, CatalogUpdated: false, CatalogChanged: 0, ChangeMessageEn: "", ChangeMessageAr: "", IsOutOfStock: 0)]
                    if outOfStockA.count > 0{
                        for i in 0...outOfStockA.count-1{
                            let outOfStockAdd = outOfStockA[i].Additionals!.filter({$0.IsOutOfStock == 1})
                            if outOfStockAdd.count > 0{
                                outOfStockAdditionals.append(contentsOf: outOfStockAdd)
                            }
                        }
                    }
                    outOfStockAdditionals.remove(at: 0)
                    if outOfStockAdditionals != nil{
                        if outOfStockAdditionals.count > 0{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item additionals out of stock", value: "", table: nil))!)
                        }else{
                            if allCartDetailsArray[0].Cart![0].IsSubscription! == true {
                                if self.selectedFrequency == ""{
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select Subscription frequency", value: "", table: nil))!)
                                    return
                                }
                            }
                            
                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                            var obj = ConfirmationVC()
                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                            obj.whereObj = 1
                            obj.isMenu = isMenu
                            obj.orderType = orderType
                            obj.storeId = storeId
                            obj.orderId = orderId
                            obj.isSubscription = allCartDetailsArray[0].Cart![0].IsSubscription!
                            obj.sectionType = sectionType
                            obj.currentStoreId = currentCartStoreId
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }else{
                        if allCartDetailsArray[0].Cart![0].IsSubscription! == true {
                            if self.selectedFrequency == ""{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select Subscription frequency", value: "", table: nil))!)
                                return
                            }
                        }
                        
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = ConfirmationVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                        obj.whereObj = 1
                        obj.isMenu = isMenu
                        obj.orderType = orderType
                        obj.storeId = storeId
                        obj.orderId = orderId
                        obj.isSubscription = allCartDetailsArray[0].Cart![0].IsSubscription!
                        obj.sectionType = sectionType
                        obj.currentStoreId = currentCartStoreId
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
    }
    //MARK: ChangeAddress Button Action
    @IBAction func changeAddressBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ManageAddressVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
        obj.storeId = 190
        obj.whereObj = 123
        obj.sectionType = 4
        obj.orderId = allCartDetailsArray[0].Cart![0].Id!
        self.navigationController?.pushViewController(obj, animated: false)
    }
    //MARK: SelectAddress Button Action
    @IBAction func selectAddressBtn_Tapped(_ sender: Any) {
        if selectAddressBtn.currentTitle == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add More", value: "", table: nil))!{
            if allCartDetailsArray.count > 0{
                if allCartDetailsArray[0].Cart![0].IsSubscription != nil{
                    if allCartDetailsArray[0].Cart![0].IsSubscription! == true{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = MenuVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                        obj.isCart = true
                        obj.isSubscription = true
                        if allCartDetailsArray[0].Cart![0].OrderType! == "4"{
                            obj.whereObj = 1
                            obj.backObj = 11
                            if allCartDetailsArray[0].Address!.count > 0{
                                if allCartDetailsArray[0].Address![0].Id != nil{
                                    var deliveryInfo = ""
                                    if allCartDetailsArray[0].Address![0].DeliveryInfo != nil{
                                        deliveryInfo = allCartDetailsArray[0].Address![0].DeliveryInfo!
                                    }
                                    obj.addressDetailsArray = [AddressDetailsModel(Id: allCartDetailsArray[0].Address![0].Id!, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: allCartDetailsArray[0].Address![0].Name!, LandMark: allCartDetailsArray[0].Address![0].Landmark!, Address: allCartDetailsArray[0].Address![0].Address!, AddressType: allCartDetailsArray[0].Address![0].TypeId!, CountryCode: "", Default: 0, Latitude: allCartDetailsArray[0].Address![0].Latitude!, Longitude: allCartDetailsArray[0].Address![0].Longitude!, ContactPerson: allCartDetailsArray[0].Address![0].ContactPerson!, ContactNo: allCartDetailsArray[0].Address![0].ContactPersonPhone!, DeliveryInfo: deliveryInfo)]
                                }
                            }
                            obj.selectOrderType = 4
                            obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                        }else{
                            obj.whereObj = 2
                            obj.backObj = 11
                            obj.address = allCartDetailsArray[0].Stores![0].NameEn!
                            obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                            obj.selectOrderType = Int(allCartDetailsArray[0].Cart![0].OrderType!)!
                        }
                        //if allCartDetailsArray[0].Cart![0].SectionType! == 2{
                            obj.sectionType = allCartDetailsArray[0].Cart![0].SectionType!
                        //}else{
                         //   obj.sectionType = 1
                        //}
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = MenuVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                        obj.isCart = true
                        obj.isSubscription = false
                        if allCartDetailsArray[0].Cart![0].OrderType! == "4"{
                            obj.whereObj = 1
                            obj.backObj = 11
                            if allCartDetailsArray[0].Address!.count > 0{
                                if allCartDetailsArray[0].Address![0].Id != nil{
                                    var deliveryInfo = ""
                                    if allCartDetailsArray[0].Address![0].DeliveryInfo != nil{
                                        deliveryInfo = allCartDetailsArray[0].Address![0].DeliveryInfo!
                                    }
                                    obj.addressDetailsArray = [AddressDetailsModel(Id: allCartDetailsArray[0].Address![0].Id!, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: allCartDetailsArray[0].Address![0].Name!, LandMark: allCartDetailsArray[0].Address![0].Landmark!, Address: allCartDetailsArray[0].Address![0].Address!, AddressType: allCartDetailsArray[0].Address![0].TypeId!, CountryCode: "", Default: 0, Latitude: allCartDetailsArray[0].Address![0].Latitude!, Longitude: allCartDetailsArray[0].Address![0].Longitude!, ContactPerson: allCartDetailsArray[0].Address![0].ContactPerson!, ContactNo: allCartDetailsArray[0].Address![0].ContactPersonPhone!, DeliveryInfo: deliveryInfo)]
                                }
                            }
                            obj.selectOrderType = 4
                            obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                        }else{
                            obj.whereObj = 2
                            obj.backObj = 11
                            obj.address = allCartDetailsArray[0].Stores![0].NameEn!
                            obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                            obj.selectOrderType = Int(allCartDetailsArray[0].Cart![0].OrderType!)!
                        }
                        //if allCartDetailsArray[0].Cart![0].SectionType! == 2{
                            obj.sectionType = allCartDetailsArray[0].Cart![0].SectionType!
                        //}else{
                           // obj.sectionType = 1
                        //}
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }else{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = MenuVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                    obj.isCart = true
                    obj.isSubscription = false
                    if allCartDetailsArray[0].Cart![0].OrderType! == "4"{
                        obj.whereObj = 1
                        obj.backObj = 11
                        if allCartDetailsArray[0].Address!.count > 0{
                            if allCartDetailsArray[0].Address![0].Id != nil{
                                var deliveryInfo = ""
                                if allCartDetailsArray[0].Address![0].DeliveryInfo != nil{
                                    deliveryInfo = allCartDetailsArray[0].Address![0].DeliveryInfo!
                                }
                                obj.addressDetailsArray = [AddressDetailsModel(Id: allCartDetailsArray[0].Address![0].Id!, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: allCartDetailsArray[0].Address![0].Name!, LandMark: allCartDetailsArray[0].Address![0].Landmark!, Address: allCartDetailsArray[0].Address![0].Address!, AddressType: allCartDetailsArray[0].Address![0].TypeId!, CountryCode: "", Default: 0, Latitude: allCartDetailsArray[0].Address![0].Latitude!, Longitude: allCartDetailsArray[0].Address![0].Longitude!, ContactPerson: allCartDetailsArray[0].Address![0].ContactPerson!, ContactNo: allCartDetailsArray[0].Address![0].ContactPersonPhone!, DeliveryInfo: deliveryInfo)]
                            }
                        }
                        obj.selectOrderType = 4
                        obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                    }else{
                        obj.whereObj = 2
                        obj.backObj = 11
                        obj.address = allCartDetailsArray[0].Stores![0].NameEn!
                        obj.storeID = allCartDetailsArray[0].Cart![0].StoreId!
                        obj.selectOrderType = Int(allCartDetailsArray[0].Cart![0].OrderType!)!
                    }
                    //if allCartDetailsArray[0].Cart![0].SectionType! == 2{
                        obj.sectionType = allCartDetailsArray[0].Cart![0].SectionType!
                    //}else{
                      //  obj.sectionType = 1
                    //}
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            obj.storeId = 190
            obj.whereObj = 123
            obj.sectionType = 4
            obj.orderId = allCartDetailsArray[0].Cart![0].Id!
            self.navigationController?.pushViewController(obj, animated: false)
        }
    }
    //MARK: Order Items Drop Down Button Action
    @IBAction func orderItemsDropDownBtn_Tapped(_ sender: Any) {
        if itemsListViewHeight.constant == 50{
            itemsTableView.reloadData()
            itemsListViewHeight.constant = itemsTableView.contentSize.height + 60
            itemsTableViewHeight.constant = itemsTableView.contentSize.height
            itemsTableViewBottomConstraint.constant = 10
            if AppDelegate.getDelegate().appLanguage == "English"{
                orderItemsDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                orderItemsDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            itemsListViewHeight.constant = 50
            itemsTableViewBottomConstraint.constant = 0
            orderItemsDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //Delivery Frequency
    @IBAction func selectFrequencyBtn_Tapped(_ sender: UIButton) {
        if allCartDetailsArray[0].Frequency != nil{
            //let startPoint = CGPoint(x: sender.frame.maxX - 40, y: 310 - myScrollView.contentOffset.y)
            let startPoint = CGPoint(x: deliveryFrequencyMainView.frame.width - 50, y: 330 - myScrollView.contentOffset.y)
            self.deliveryFrequencyPopUpView.frame =  CGRect(x: 0, y: 0, width: 310, height: 153)
            popover.show(self.deliveryFrequencyPopUpView, point: startPoint)
        }
    }
    @IBAction func frequencyOneBtn_Tapped(_ sender: Any) {
        frequeencyOneBGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
        frequeencyTwoBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        frequeencyThreeBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        if allCartDetailsArray[0].Frequency!.count > 0{
            DispatchQueue.main.async {
                self.deliveryFrequencyUpdate(Id: self.allCartDetailsArray[0].Frequency![0].Id!)
                self.selectedFrequency = self.allCartDetailsArray[0].Frequency![0].NameEn!
            }
        }
    }
    @IBAction func frequencyTwoBtn_Tapped(_ sender: Any) {
        frequeencyTwoBGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
        frequeencyOneBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        frequeencyThreeBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        if allCartDetailsArray[0].Frequency!.count > 1{
            DispatchQueue.main.async {
                self.deliveryFrequencyUpdate(Id: self.allCartDetailsArray[0].Frequency![1].Id!)
                self.selectedFrequency = self.allCartDetailsArray[0].Frequency![1].NameEn!
            }
        }
    }
    @IBAction func frequencyThreeBtn_Tapped(_ sender: Any) {
        frequeencyThreeBGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
        frequeencyOneBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        frequeencyTwoBGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        if allCartDetailsArray[0].Frequency!.count > 2{
            DispatchQueue.main.async {
                self.deliveryFrequencyUpdate(Id: self.allCartDetailsArray[0].Frequency![2].Id!)
                self.selectedFrequency = self.allCartDetailsArray[0].Frequency![2].NameEn!
            }
        }
    }
    func deliveryFrequencyUpdate(Id:Int){
        popover.dismiss()
        let dic = ["userId": UserDef.getUserId(),
                   "OrderId": allCartDetailsArray[0].Cart![0].Id!,
                   "FrequencyId": Id, "RequestBy":2] as [String : Any]
        CartModuleServices.DeliveryFrequencyUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.isRemove = false
                DispatchQueue.main.async {
                    ANLoader.showLoading("", disableUI: true)
                    self.getDetailsService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension CartVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == multipleOrdersTableView{
            return MultipleOrderDetailsArray.count
        }else{
            if allCartDetailsArray.count > 0{
                return allCartDetailsArray[0].Items!.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == multipleOrdersTableView{
            return UITableView.automaticDimension
        }else{
            var additional = [""]
            var messageHeight = 0.0
            var isOutOfStock = false
            var isRewards = false
            var isCatalogUpdate = false
            if allCartDetailsArray[0].Items![indexPath.row].CatalogChanged! == true{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].ChangeMessageEn!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                }else{
                    messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].ChangeMessageAr!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                }
                isOutOfStock = true
            }else{
                let catalogA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                let catalogAdditionals = catalogA.filter({$0.CatalogChanged == 1})
                if catalogAdditionals.count > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        messageHeight = Double(heightForView(text: catalogAdditionals[0].ChangeMessageEn, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                    }else{
                        messageHeight = Double(heightForView(text: catalogAdditionals[0].ChangeMessageAr, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                    }
                    isOutOfStock = true
                }else{
                    if allCartDetailsArray[0].Items![indexPath.row].IsOutOfStock! == 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageEn!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                        }else{
                            messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageAr!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                        }
                        isOutOfStock = true
                    }else{
                        let outOfStockA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                        let outOfStockAdditionals = outOfStockA.filter({$0.IsOutOfStock == 1})
                        if outOfStockAdditionals.count > 0{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageEn!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }else{
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageAr!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }
                            isOutOfStock = true
                        }else{
                            if allCartDetailsArray[0].Cart![0].ConsumedPoints != nil{
                                if allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                                    if allCartDetailsArray[0].Cart![0].ConsumedPoints! == allCartDetailsArray[0].Items![indexPath.row].ConsumedPoints!{
                                        isRewards = true
                                    }else{
                                        if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                            isCatalogUpdate = true
                                        }else{
                                            let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                            let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                            if catalogUpdated.count > 0{
                                                isCatalogUpdate = true
                                            }
                                        }
                                    }
                                }else{
                                    if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                        isCatalogUpdate = true
                                    }else{
                                        let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                        let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                        if catalogUpdated.count > 0{
                                            isCatalogUpdate = true
                                        }
                                    }
                                }
                            }else{
                                if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                    isCatalogUpdate = true
                                }else{
                                    let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                    let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                    if catalogUpdated.count > 0{
                                        isCatalogUpdate = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
//            if messageHeight == 0{
//                if allCartDetailsArray[0].Cart![0].ConsumedPoints != nil{
//                    if allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
//                        if allCartDetailsArray[0].Cart![0].ConsumedPoints! == allCartDetailsArray[0].Items![indexPath.row].ConsumedPoints!{
//                            messageHeight = 36
//                        }else{
//                            messageHeight = 0
//                        }
//                    }else{
//                        messageHeight = 0
//                    }
//                }
//            }
            if isOutOfStock == false{
                if isRewards == true{
                    messageHeight = 21
                }else{
                    if isCatalogUpdate == true{
                        if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].CatlogMessageEn!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }else{
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].CatlogMessageAr!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }
                        }else{
                            let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                            let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                            if catalogUpdated.count > 0{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    messageHeight = Double(heightForView(text:  catalogUpdated[0].ChangeMessageEn, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                                }else{
                                    messageHeight = Double(heightForView(text:  catalogUpdated[0].ChangeMessageAr, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                                }
                                //messageHeight = Double(heightForView(text: "Item price has been changed oldPrice to newprice", font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }else{
                                messageHeight = 0
                            }
                        }
                    }else{
                        messageHeight = 0
                    }
                }
            }
            
            if allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn != nil{
                if allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(allCartDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }

            if messageHeight < 20 && messageHeight > 0{
                messageHeight = 31
            }else if messageHeight > 20{
                messageHeight = messageHeight + 10
            }
            
            if allCartDetailsArray[0].Items![indexPath.row].Additionals != nil{
                if allCartDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...allCartDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additional.append("\(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn) x \(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity)")
                        }else{
                            additional.append("\(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].NameAr) x \(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity)")
                        }
                    }
                }
                
                if additional.count > 1{
                    additional.remove(at: 0)
                    var height = heightForView(text: additional.joined(separator: ","), font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40)
                    if AppDelegate.getDelegate().appLanguage != "English"{
                        if Int(height/19) == Int(2){
                            if additional.count >= 6{
                                height = height + 10
                            }
                        }
                    }
                    height = 90 + height + 10
                    if messageHeight > 0{
                        return height + CGFloat(messageHeight)
                    }else{
                        return height
                    }
                }else{
                    if messageHeight > 0{
                        return CGFloat(messageHeight) + 90
                    }else{
                        return 90
                    }
                }
            }else{
                if messageHeight > 0{
                    return CGFloat(messageHeight) + 90
                }else{
                    return 90
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == multipleOrdersTableView{
            multipleOrdersTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MultipleOrdersTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MultipleOrdersTVCell", value: "", table: nil))!)
            let cell = multipleOrdersTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MultipleOrdersTVCell", value: "", table: nil))!) as! MultipleOrdersTVCell
            
            cell.bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
            cell.bottomView.clipsToBounds = true
            
            cell.checkoutBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Check Out", value: "", table: nil))!, for: .normal)
            
            if MultipleOrderDetailsArray[indexPath.row].OrderTypeEn == "PickUp"{
                cell.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up From", value: "", table: nil))!
                cell.orderTypeImg.image = UIImage(named: "Home Pick up")
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.addressTitleNameLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreNameEn
                    cell.addressLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreAddressEn
                }else{
                    cell.addressTitleNameLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreNameAr
                    cell.addressLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreAddressAr
                }
            }else if MultipleOrderDetailsArray[indexPath.row].OrderTypeEn == "Delivery"{
                if MultipleOrderDetailsArray[indexPath.row].IsSubscription == true{
                    cell.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Delivery to", value: "", table: nil))!
                    cell.orderTypeImg.image = UIImage(named: "Subscription")
                }else{
                    cell.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery to", value: "", table: nil))!
                    cell.orderTypeImg.image = UIImage(named: "delivery")
                }
                if MultipleOrderDetailsArray[indexPath.row].UserAddressDetails != nil{
                    if MultipleOrderDetailsArray[indexPath.row].UserAddressDetails!.count > 0{
                        if MultipleOrderDetailsArray[indexPath.row].UserAddressDetails![0].TypeId != nil{
                            cell.addressTitleNameLbl.text = MultipleOrderDetailsArray[indexPath.row].UserAddressDetails![0].TypeId!
                        }else{
                            cell.addressTitleNameLbl.text = ""
                        }
                        if MultipleOrderDetailsArray[indexPath.row].UserAddressDetails![0].Address != nil{
                            cell.addressLbl.text = MultipleOrderDetailsArray[indexPath.row].UserAddressDetails![0].Address!
                        }else{
                            cell.addressLbl.text = ""
                        }
                    }else{
                        cell.addressTitleNameLbl.text = ""
                        cell.addressLbl.text = ""
                    }
                }else{
                    cell.addressTitleNameLbl.text = ""
                    cell.addressLbl.text = ""
                }
            }else{
                cell.orderTypeImg.image = UIImage(named: "Dine In")
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.orderTypeNameLbl.text = MultipleOrderDetailsArray[indexPath.row].OrderTypeEn
                    cell.addressTitleNameLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreNameEn
                    cell.addressLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreAddressEn
                }else{
                    cell.orderTypeNameLbl.text = MultipleOrderDetailsArray[indexPath.row].OrderTypeAr
                    cell.addressTitleNameLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreNameAr
                    cell.addressLbl.text = MultipleOrderDetailsArray[indexPath.row].StoreAddressAr
                }
            }
            if MultipleOrderDetailsArray[indexPath.row].CartItemsQty == 1{
                cell.numOfItemsLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No of Item", value: "", table: nil))!) \(MultipleOrderDetailsArray[indexPath.row].CartItemsQty)"
            }else{
                cell.numOfItemsLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No of Items", value: "", table: nil))!) \(MultipleOrderDetailsArray[indexPath.row].CartItemsQty)"
            }
            
            checkoutBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Check Out", value: "", table: nil))!, for: .normal)
            cell.checkoutBtn.tag = indexPath.row
            cell.checkoutBtn.addTarget(self, action: #selector(multipleOrderCheckoutBtn_Tapped(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOutTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOutTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOutTVCell", value: "", table: nil))!) as! CheckOutTVCell
            if allCartDetailsArray[0].Items![indexPath.row].SizeNameEn! == "Regular"{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.itemNameLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].ItemNameEn!)"
                }else{
                    cell.itemNameLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].ItemNameAr!)"
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.itemNameLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].ItemNameEn!) (\(allCartDetailsArray[0].Items![indexPath.row].SizeNameEn!))"
                }else{
                    cell.itemNameLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].ItemNameAr!) (\(allCartDetailsArray[0].Items![indexPath.row].SizeNameAr!))"
                }
            }
            
            cell.itemPriceLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].TotalPrice!.withCommas())"
            cell.quantityLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].Quantity!)"
            cell.plusBtn.isEnabled = true
            
            cell.pointsItemsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Points Item", value: "", table: nil))!
            
            var messageHeight = 0.0
            var isOutOfStock = false
            var isRewards = false
            var isCatalogUpdate = false
            //let catalog = cartDetailsArray[0].Items!.filter({$0.CatalogChanged! == 1})
            if allCartDetailsArray[0].Items![indexPath.row].CatalogChanged! == true{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].ChangeMessageEn!
                    messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].ChangeMessageEn!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                }else{
                    cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].ChangeMessageAr!
                    messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].ChangeMessageAr!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                }
                cell.messageViewHeight.constant = 31
                cell.messageLbl.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                cell.plusBtn.isEnabled = false
                isOutOfStock = true
            }else{
                let catalogA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                let catalogAdditionals = catalogA.filter({$0.CatalogChanged == 1})
                if catalogAdditionals.count > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.messageLbl.text = catalogAdditionals[0].ChangeMessageEn
                        messageHeight = Double(heightForView(text: catalogAdditionals[0].ChangeMessageEn, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                    }else{
                        cell.messageLbl.text = catalogAdditionals[0].ChangeMessageAr
                        messageHeight = Double(heightForView(text: catalogAdditionals[0].ChangeMessageAr, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                    }
                    cell.messageLbl.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                    cell.messageViewHeight.constant = 31
                    cell.plusBtn.isEnabled = false
                    isOutOfStock = true
                }else{
                    if allCartDetailsArray[0].Items![indexPath.row].IsOutOfStock! == 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageEn!
                            messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageEn!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                        }else{
                            cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageAr!
                            messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageAr!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                        }
                        cell.messageViewHeight.constant = 31
                        cell.messageLbl.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                        cell.plusBtn.isEnabled = false
                        isOutOfStock = true
                    }else{
                        let outOfStockA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                        let outOfStockAdditionals = outOfStockA.filter({$0.IsOutOfStock == 1})
                        if outOfStockAdditionals.count > 0{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                cell.messageLbl.text = outOfStockAdditionals[0].ChangeMessageEn
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageEn!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                            }else{
                                cell.messageLbl.text = outOfStockAdditionals[0].ChangeMessageAr
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].OutOfStockMessageAr!, font: UIFont.init(name: "Avenir Book", size: 15.0)!, width: view.frame.width - 40))
                            }
                            cell.messageViewHeight.constant = 31
                            cell.messageLbl.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                            cell.plusBtn.isEnabled = false
                            isOutOfStock = true
                        }else{
                            if allCartDetailsArray[0].Cart![0].ConsumedPoints != nil{
                                if allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                                    if allCartDetailsArray[0].Cart![0].ConsumedPoints! == allCartDetailsArray[0].Items![indexPath.row].ConsumedPoints!{
                                        isRewards = true
                                    }else{
                                        if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                            isCatalogUpdate = true
                                        }else{
                                            let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                            let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                            if catalogUpdated.count > 0{
                                                isCatalogUpdate = true
                                            }
                                        }
                                    }
                                }else{
                                    if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                        isCatalogUpdate = true
                                    }else{
                                        let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                        let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                        if catalogUpdated.count > 0{
                                            isCatalogUpdate = true
                                        }
                                    }
                                }
                            }else{
                                if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                                    isCatalogUpdate = true
                                }else{
                                    let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                                    let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                                    if catalogUpdated.count > 0{
                                        isCatalogUpdate = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if isOutOfStock == true{
                cell.redeemPointsView.isHidden = true
            }else{
                if allCartDetailsArray[0].Items![indexPath.row].RewardEligibility != nil{
                    if allCartDetailsArray[0].Items![indexPath.row].RewardEligibility! == true{
                        if allCartDetailsArray[0].RewardsArray != nil{
                            if rewardsFilterArray.count > 0{
                                let reward = rewardsFilterArray.filter({$0.RewardBand! == allCartDetailsArray[0].Items![indexPath.row].RewardBand!})
                                if reward.count > 0{
                                    cell.redeemPointsView.isHidden = false
                                    cell.pointsLbl.text = "\(allCartDetailsArray[0].Items![indexPath.row].RewardBand!)"
                                }else{
                                    cell.redeemPointsView.isHidden = true
                                }
                            }else{
                                cell.redeemPointsView.isHidden = true
                            }
                        }else{
                            cell.redeemPointsView.isHidden = true
                        }
                    }else{
                        cell.redeemPointsView.isHidden = true
                    }
                }else{
                    cell.redeemPointsView.isHidden = true
                }
            }
            
            //Reward functionality
            if isOutOfStock == false{
                if isRewards == true{
                    cell.redeemPointsLbl.text = "- \(allCartDetailsArray[0].Items![indexPath.row].RewardBand!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!)"
                    cell.messageLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "It’s on us!", value: "", table: nil))!) \(allCartDetailsArray[0].Items![indexPath.row].RewardBand!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "points Reward applied.", value: "", table: nil))!)"
                    cell.messageLbl.textColor = #colorLiteral(red: 0.1058823529, green: 0.4901960784, blue: 0.6117647059, alpha: 1)
                    cell.messageViewHeight.constant = 31
                    cell.redeemPointsLblWidth.constant = 82
                    cell.redeemPointsLblTrailing.constant = 10
                    messageHeight = 21
                }else{
                    if isCatalogUpdate == true{
                        cell.redeemPointsLblWidth.constant = 0
                        cell.redeemPointsLblTrailing.constant = 0
                        if allCartDetailsArray[0].Items![indexPath.row].CatalogUpdated! == true{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].CatlogMessageEn!
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].CatlogMessageEn!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }else{
                                cell.messageLbl.text = allCartDetailsArray[0].Items![indexPath.row].CatlogMessageAr!
                                messageHeight = Double(heightForView(text: allCartDetailsArray[0].Items![indexPath.row].CatlogMessageAr!, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                            }
                            cell.messageLbl.textColor = #colorLiteral(red: 0.9490196078, green: 0.4196078431, blue: 0.2274509804, alpha: 1)
                            cell.messageViewHeight.constant = 31
                            cell.plusBtn.isEnabled = true
                        }else{
                            let catalogUpdateA = allCartDetailsArray[0].Items![indexPath.row].Additionals!
                            let catalogUpdated = catalogUpdateA.filter({$0.CatalogUpdated == true})
                            if catalogUpdated.count > 0{
                                cell.messageLbl.textColor = #colorLiteral(red: 0.9490196078, green: 0.4196078431, blue: 0.2274509804, alpha: 1)
                                cell.messageViewHeight.constant = 31
                                cell.plusBtn.isEnabled = true
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    cell.messageLbl.text = catalogUpdated[0].ChangeMessageEn
                                    messageHeight = Double(heightForView(text: catalogUpdated[0].ChangeMessageEn, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                                }else{
                                    cell.messageLbl.text = catalogUpdated[0].ChangeMessageAr
                                    messageHeight = Double(heightForView(text: catalogUpdated[0].ChangeMessageAr, font: UIFont.init(name: "Avenir Book", size: 14.0)!, width: view.frame.width - 40))
                                }
                                cell.messageLbl.textColor = #colorLiteral(red: 0.9490196078, green: 0.4196078431, blue: 0.2274509804, alpha: 1)
                                cell.messageViewHeight.constant = 31
                                cell.plusBtn.isEnabled = true
                            }else{
                                cell.messageViewHeight.constant = 0
                                cell.plusBtn.isEnabled = true
                                messageHeight = 0
                            }
                        }
                    }else{
                        cell.messageViewHeight.constant = 0
                        cell.plusBtn.isEnabled = true
                        messageHeight = 0
                    }
                }
            }else{
                cell.redeemPointsLblWidth.constant = 0
                cell.redeemPointsLblTrailing.constant = 0
            }
            
            cell.redeemPointsViewHeight.constant = 0
            
            //CheckOut Button Enable
            let catalog = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! == true})
            if catalog.count > 0{
                //checkoutBtn.isEnabled = false
                //checkoutBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
            }else{
                let catalogA = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! != true})
                var catalogAdditionals:[CartAdditionalsModel]!
                catalogAdditionals = [CartAdditionalsModel(GroupId: 0, AdditionalId: 0, NameEn: "", NameAr: "", Quantity: 0, Image: "", Price: 0.0, CatalogUpdated: false, CatalogChanged: 0, ChangeMessageEn: "", ChangeMessageAr: "", IsOutOfStock: 0)]
                if catalogA.count > 0{
                    for i in 0...catalogA.count-1{
                        let catalogAdd = catalogA[i].Additionals!.filter({$0.CatalogChanged == 1})
                        if catalogAdd.count > 0{
                            catalogAdditionals.append(contentsOf: catalogAdd)
                        }
                    }
                }
                catalogAdditionals.remove(at: 0)
                if catalogAdditionals != nil{
                    if catalogAdditionals.count > 0{
                        //checkoutBtn.isEnabled = false
                        //checkoutBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                        checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
                    }else{
                        let outOfStock = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! == 1})
                        if outOfStock.count > 0{
                            //checkoutBtn.isEnabled = false
                            //checkoutBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                            checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
                        }else{
                            let outOfStockA = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! != 1})
                            var outOfStockAdditionals:[CartAdditionalsModel]!
                            outOfStockAdditionals = [CartAdditionalsModel(GroupId: 0, AdditionalId: 0, NameEn: "", NameAr: "", Quantity: 0, Image: "", Price: 0.0, CatalogUpdated: false, CatalogChanged: 0, ChangeMessageEn: "", ChangeMessageAr: "", IsOutOfStock: 0)]
                            if outOfStockA.count > 0{
                                for i in 0...outOfStockA.count-1{
                                    let outOfStockAdd = outOfStockA[i].Additionals!.filter({$0.IsOutOfStock == 1})
                                    if outOfStockAdd.count > 0{
                                        outOfStockAdditionals.append(contentsOf: outOfStockAdd)
                                    }
                                }
                            }
                            outOfStockAdditionals.remove(at: 0)
                            if outOfStockAdditionals != nil{
                                if outOfStockAdditionals.count > 0{
                                    //checkoutBtn.isEnabled = false
                                    checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
                                }else{
                                    //checkoutBtn.isEnabled = true
                                    checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                                }
                            }else{
                                checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                            }
                        }
                    }
                }else{
                    let outOfStock = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! == 1})
                    if outOfStock.count > 0{
                        //checkoutBtn.isEnabled = false
                        //checkoutBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                        checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
                    }else{
                        let outOfStockA = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! != 1})
                        var outOfStockAdditionals:[CartAdditionalsModel]!
                        outOfStockAdditionals = [CartAdditionalsModel(GroupId: 0, AdditionalId: 0, NameEn: "", NameAr: "", Quantity: 0, Image: "", Price: 0.0, CatalogUpdated: false, CatalogChanged: 0, ChangeMessageEn: "", ChangeMessageAr: "", IsOutOfStock: 0)]
                        if outOfStockA.count > 0{
                            for i in 0...outOfStockA.count-1{
                                let outOfStockAdd = outOfStockA[i].Additionals!.filter({$0.IsOutOfStock == 1})
                                if outOfStockAdd.count > 0{
                                    outOfStockAdditionals.append(contentsOf: outOfStockAdd)
                                }
                            }
                        }
                        outOfStockAdditionals.remove(at: 0)
                        if outOfStockAdditionals != nil{
                            if outOfStockAdditionals.count > 0{
                                //checkoutBtn.isEnabled = false
                                //checkoutBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                                checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)", Alpha: 200)
                            }else{
                                //checkoutBtn.isEnabled = true
                                checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                            }
                        }else{
                            checkoutBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                        }
                    }
                }
            }
                        
            var additional = [""]
            if allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn != nil{
                if allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn != ""{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        additional.append("\(allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn!) x 1")
                    }else{
                        additional.append("\(allCartDetailsArray[0].Items![indexPath.row].GrinderNameAr!) x 1")
                    }
                }
            }
            if allCartDetailsArray[0].Items![indexPath.row].Additionals != nil{
                if allCartDetailsArray[0].Items![indexPath.row].Additionals!.count > 0{
                    for i in 0...allCartDetailsArray[0].Items![indexPath.row].Additionals!.count - 1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additional.append("\(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].NameEn) x \(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity)")
                        }else{
                            additional.append("\(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].NameAr) x \(allCartDetailsArray[0].Items![indexPath.row].Additionals![i].Quantity)")
                        }
                    }
                }
            }
            
            if additional.count > 1{
                additional.remove(at: 0)
                cell.additionalsLbl.text = additional.joined(separator: ", ")
                let height = heightForView(text: additional.joined(separator: ", "), font: UIFont.init(name: "Avenir Book", size: 16.0)!, width: cell.additionalsLbl.frame.width)
                cell.additionalsViewHeight.constant = height + 10
            }else{
                cell.additionalsLbl.text = ""
                cell.additionalsViewHeight.constant = 0
            }
            if messageHeight > 0{
                cell.messageViewHeight.constant = CGFloat(messageHeight + 10)
            }else{
                cell.messageViewHeight.constant = 0
            }
            
//            if allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn != nil{
//                if allCartDetailsArray[0].Items![indexPath.row].GrinderId! == 0{
//                    cell.grinderViewHeight.constant = 0
//                }else{
//                    cell.grinderNameLbl.text = "Selected Bean : \(allCartDetailsArray[0].Items![indexPath.row].GrinderNameEn!)"
//                    cell.grinderViewHeight.constant = 31
//                }
//            }else{
//                cell.grinderViewHeight.constant = 0
//            }
            cell.grinderViewHeight.constant = 0
            
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(allCartDetailsArray[0].Items![indexPath.row].ItemImage!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(itemPlusBtn_Tapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(itemMinusBtn_Tapped(_:)), for: .touchUpInside)
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(itemRemoveBtn_Tapped(_:)), for: .touchUpInside)
            
            if indexPath.row%2 == 0{
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                tableView.layoutIfNeeded()
                self.itemsTableViewHeight.constant = tableView.contentSize.height
                self.itemsListViewHeight.constant = self.itemsTableView.contentSize.height + 60
                self.itemsTableViewBottomConstraint.constant = 10
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == multipleOrdersTableView{
            orderId = MultipleOrderDetailsArray[indexPath.row].Id
            currentCartStoreId = MultipleOrderDetailsArray[indexPath.row].StoreId
            isSubscription = MultipleOrderDetailsArray[indexPath.row].IsSubscription
            ANLoader.showLoading("", disableUI: true)
            if isSubscription == false && sectionType == 1{
                self.addressUpdateService(addressId: selectAddressID, OrderId: MultipleOrderDetailsArray[indexPath.row].Id)
            }else{
                self.getDetailsService()
            }
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == itemsTableView{
            let editAction = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = CommentVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "CommentVC") as! CommentVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .crossDissolve
                obj.commentVCDelegate = self
                if self.allCartDetailsArray[0].Items![indexPath.row].Comments != nil{
                    if self.allCartDetailsArray[0].Items![indexPath.row].Comments! != ""{
                        obj.commentsStr = self.allCartDetailsArray[0].Items![indexPath.row].Comments!
                    }else{
                        obj.commentsStr = ""
                    }
                }else{
                    obj.commentsStr = ""
                }
                //obj.commentsStr = self.commentText
                obj.indexPath = indexPath.row
                self.present(obj, animated: false, completion: nil)
                completion(true)
            }
            let deleteAction = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
                let dic = ["CartItemId": self.allCartDetailsArray[0].Items![indexPath.row].CartItemId!, "OrderId": self.allCartDetailsArray[0].Cart![0].Id!] as [String : Any]
                CartModuleServices.RemoveCartItemService(dic: dic as [String : Any], success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                        }
                    }else{
                        self.cartDetailsArray = [data.Data!]
                        if data.Data!.Carts!.count > 0{
                            self.allCartDetailsArray = [data.Data!.Carts![0]]
                            DispatchQueue.main.async {
                                self.AllData()
                                self.itemsTableView.reloadData()
                            }
                        }else{
                            DispatchQueue.main.async {
                                if self.isSubscription == true{
                                    AppDelegate.getDelegate().subCartQuantity = 0
                                    AppDelegate.getDelegate().subCartAmount = 0
                                }else{
                                    AppDelegate.getDelegate().cartQuantity = 0
                                    AppDelegate.getDelegate().cartAmount = 0
                                }
                                AppDelegate.getDelegate().cartStoreId = 0
                                AppDelegate.getDelegate().cartStoreName = ""
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[2]
                                    tabItem.badgeValue = nil
                                    tabItems[2].image = UIImage(named: "Tab Order")
                                    tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                                }
                                if self.MultipleOrderDetailsArray.count == 2{
                                    self.isMultipleOrders = true
                                    ANLoader.showLoading("", disableUI: true)
                                    self.getOrderDetailsService()
                                }else{
                                    if self.whereObj == 0 {
                                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                        let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                                        obj.whereObj = 1
                                        self.navigationController?.pushViewController(obj, animated: false)
                                        return
                                    }else{
                                        if self.isSubscription == true{
                                            self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your subscription cart is empty. Add something from the menu", value: "", table: nil))!
                                        }else{
                                            self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu", value: "", table: nil))!
                                        }
                                        self.cartEmptyBtn.isHidden = true
                                        self.cartEmptyLbl.isHidden = true
                                        self.emptyCartView.isHidden = false
                                        self.myScrollView.isHidden = true
                                        self.confirmOrderView.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                }
                completion(true)
            }

            editAction.image = UIImage(named: "note")
            editAction.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.8823529412, blue: 0.8862745098, alpha: 1)
            deleteAction.image = UIImage(named: "delete")
            deleteAction.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.8823529412, blue: 0.8862745098, alpha: 1)
            return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        }else{
            let swipeAction = UISwipeActionsConfiguration(actions: [])
            return swipeAction
        }
    }
    @objc func multipleOrderCheckoutBtn_Tapped(_ sender: UIButton){
        orderId = MultipleOrderDetailsArray[sender.tag].Id
        ANLoader.showLoading("", disableUI: true)
        getDetailsService()
    }
    @objc func itemRemoveBtn_Tapped(_ sender: UIButton){
        let dic = ["CartItemId": self.allCartDetailsArray[0].Items![sender.tag].CartItemId!, "OrderId": self.allCartDetailsArray[0].Cart![0].Id!] as [String : Any]
        CartModuleServices.RemoveCartItemService(dic: dic as [String : Any], success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                self.cartDetailsArray = [data.Data!]
                self.allCartDetailsArray = [data.Data!.Carts![0]]
                DispatchQueue.main.async {
                    self.AllData()
                    self.itemsTableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
        }
    }
    @objc func itemPlusBtn_Tapped(_ sender: UIButton){
        if allCartDetailsArray[0].Items![sender.tag].MaxOrderQty! > allCartDetailsArray[0].Items![sender.tag].Quantity!{
            let dic = ["CartItemId": allCartDetailsArray[0].Items![sender.tag].CartItemId!, "Action": "+", "RequestBy":2] as [String : Any]
            CartModuleServices.UpdateCartItemService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.cartDetailsArray = [data.Data!]
                    self.allCartDetailsArray = [data.Data!.Carts![0]]
                    DispatchQueue.main.async {
                        self.AllData()
                        self.itemsTableView.reloadData()
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have exceeded max quantity for this item", value: "", table: nil))!)
        }
    }
    @objc func itemMinusBtn_Tapped(_ sender: UIButton){
        if quantity > 1{
            if allCartDetailsArray[0].Items![sender.tag].Quantity! > 1{
                let dic = ["CartItemId": allCartDetailsArray[0].Items![sender.tag].CartItemId!, "Action": "-", "RequestBy":2] as [String : Any]
                CartModuleServices.UpdateCartItemService(dic: dic, success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                        }
                    }else{
                        self.cartDetailsArray = [data.Data!]
                        self.allCartDetailsArray = [data.Data!.Carts![0]]
                        DispatchQueue.main.async {
                            self.AllData()
                            self.itemsTableView.reloadData()
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }else{
                let dic = ["CartItemId": allCartDetailsArray[0].Items![sender.tag].CartItemId!, "OrderId": allCartDetailsArray[0].Cart![0].Id!, "RequestBy":2] as [String : Any]
                CartModuleServices.RemoveCartItemService(dic: dic as [String : Any], success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                        }
                    }else{
                        self.cartDetailsArray = [data.Data!]
                        self.allCartDetailsArray = [data.Data!.Carts![0]]
                        DispatchQueue.main.async {
                            self.AllData()
                            self.itemsTableView.reloadData()
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }
        }else{
            let dic = ["UserId": UserDef.getUserId(), "OrderId": allCartDetailsArray[0].Cart![0].Id!, "RequestBy":2] as [String : Any]
            CartModuleServices.EmptyCartService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showToastDown(on: self, message: data.MessageEn)
                    }else{
                        Alert.showToastDown(on: self, message: data.MessageAr)
                    }
                    DispatchQueue.main.async {
                        if self.whereObj == 0 {
                            self.tabBarController?.tabBar.isHidden = false
                        }else{
                            self.tabBarController?.tabBar.isHidden = true
                        }
                        if self.isSubscription == true{
                            AppDelegate.getDelegate().subCartQuantity = 0
                            AppDelegate.getDelegate().subCartAmount = 0
                        }else{
                            AppDelegate.getDelegate().cartQuantity = 0
                            AppDelegate.getDelegate().cartAmount = 0
                        }
                        AppDelegate.getDelegate().cartStoreId = 0
                        AppDelegate.getDelegate().cartStoreName = ""
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            tabItem.badgeValue = nil
                            tabItems[2].image = UIImage(named: "Tab Order")
                            tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                        }
                        if self.MultipleOrderDetailsArray.count == 2{
                            self.isMultipleOrders = true
                            ANLoader.showLoading("", disableUI: true)
                            self.getOrderDetailsService()
                        }else{
                            if self.whereObj == 0 {
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
                                obj.whereObj = 1
                                self.navigationController?.pushViewController(obj, animated: false)
                                return
                            }else{
                                if self.isSubscription == true{
                                    self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your subscription cart is empty. Add something from the menu", value: "", table: nil))!
                                }else{
                                    self.emptyCartMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu", value: "", table: nil))!
                                }
                                self.cartEmptyBtn.isHidden = true
                                self.cartEmptyLbl.isHidden = true
                                self.emptyCartView.isHidden = false
                                self.myScrollView.isHidden = true
                                self.confirmOrderView.isHidden = true
                            }
                        }
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension CartVC:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == subscriptionFrequencyCV{
            if allCartDetailsArray.count > 0{
                if allCartDetailsArray[0].Frequency != nil{
                    return self.allCartDetailsArray[0].Frequency!.count
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else if collectionView == redeemRewardsCV{
            if allCartDetailsArray.count > 0{
                if allCartDetailsArray[0].RewardsArray != nil{
                    return self.rewardsFilterArray.count
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else{
            if allCartDetailsArray.count > 0{
                return crossItemDetails.count
            }else{
                return 0
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == subscriptionFrequencyCV{
            subscriptionFrequencyCV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionFrequencyCartCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionFrequencyCartCVCell", value: "", table: nil))!)
            let cell = subscriptionFrequencyCV.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SubscriptionFrequencyCartCVCell", value: "", table: nil))!, for: indexPath) as! SubscriptionFrequencyCartCVCell
            
            let dic = self.allCartDetailsArray[0].Frequency![indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.frquencyNameLbl.text = dic.NameEn!
            }else{
                cell.frquencyNameLbl.text = dic.NameAr!
            }
            cell.percentageLbl.text = "\(Int(dic.Discount!))% \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "off", value: "", table: nil))!)"
            
            if self.allCartDetailsArray[0].Frequency![indexPath.row].Id == self.allCartDetailsArray[0].Cart![0].FrequencyId!{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                self.selectedFrequency = self.allCartDetailsArray[0].Frequency![indexPath.row].NameEn!
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
            }
            
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(frequencyInfoBtn_Tapped(_:)), for: .touchUpInside)
            
            return cell
        }else if collectionView == redeemRewardsCV{
            redeemRewardsCV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RedeemRewardCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RedeemRewardCVCell", value: "", table: nil))!)
            let cell = redeemRewardsCV.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RedeemRewardCVCell", value: "", table: nil))!, for: indexPath) as! RedeemRewardCVCell
            
            cell.pointsLbl.text = "\(rewardsFilterArray[indexPath.row].RewardBand!)"
            
            if allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                if allCartDetailsArray[0].Cart![0].ConsumedPoints! == rewardsFilterArray[indexPath.row].RewardBand!{
                    cell.nameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Applied", value: "", table: nil))!
                    cell.BGView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.7725490196, blue: 0.4274509804, alpha: 1)
                }else{
                    cell.nameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply", value: "", table: nil))!
                    cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }else{
                cell.nameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply", value: "", table: nil))!
                cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            return cell
        }else{
            tryItCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!)
            let cell = tryItCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!, for: indexPath) as! ConfirmationVCCVCell
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.itemNameLbl.text = crossItemDetails[indexPath.row].NameEn!
            }else{
                cell.itemNameLbl.text = crossItemDetails[indexPath.row].NameAr!
            }
            if crossItemDetails[indexPath.row].Prices != nil{
                if crossItemDetails[indexPath.row].Prices!.count > 0{
                    cell.itemPriceLbl.text = "\(crossItemDetails[indexPath.row].Prices![0].Price!.withCommas())"
                }else{
                    cell.itemPriceLbl.text = "0.00"
                }
            }else{
                cell.itemPriceLbl.text = "0.00"
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(crossItemDetails[indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            cell.sugestiveItemAddBtn.isHidden = true
            cell.addBtnView.isHidden = false
            
            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(recomendedAdBtn_Tapped(_:)), for: .touchUpInside)
            
            cell.addBtnView.backgroundColor = .white
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == subscriptionFrequencyCV{
            let screenSize: CGRect = self.subscriptionFrequencyCV.bounds
            let screenWidth = screenSize.width
            return CGSize(width: screenWidth/3 - 20, height: screenSize.height)
        }else if collectionView == redeemRewardsCV{
            let screenSize: CGRect = self.redeemRewardsCV.bounds
            let screenWidth = screenSize.width
            //if view.frame.width > 400{
                return CGSize(width: screenWidth/3 - 5, height: 30)
            //}else{
                //return CGSize(width: screenWidth/2 - 5, height: 30)
            //}
        }else{
            let screenSize: CGRect = self.tryItCollectionView.bounds
            let screenWidth = screenSize.width
            return CGSize(width: screenWidth/2 + 50, height: 230)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == subscriptionFrequencyCV{
            selectFrequencyIndex = indexPath.row
            subscriptionFrequencyCV.reloadData()
            self.deliveryFrequencyUpdate(Id: self.allCartDetailsArray[0].Frequency![indexPath.row].Id!)
            self.selectedFrequency = self.allCartDetailsArray[0].Frequency![indexPath.row].NameEn!
        }else if collectionView == redeemRewardsCV{
//            if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
//                if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! == self.rewardsFilterArray[indexPath.row].RewardBand!{
//                    self.applyRewardPoints(isRemove: true, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
//                }else{
//                    self.applyRewardPoints(isRemove: false, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
//                }
//            }else{
//                self.applyRewardPoints(isRemove: false, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
//            }
            
            //Swathi
            let catalog = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! == true})
            if catalog.count > 0{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is currently unavailable", value: "", table: nil))!)
            }else{
                let catalogA = allCartDetailsArray[0].Items!.filter({$0.CatalogChanged! != true})
                let catalogAdditionals = catalogA.filter({$0.CatalogChanged! == true})
                if catalogAdditionals.count > 0{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is currently unavailable", value: "", table: nil))!)
                }else{
                    let outOfStock = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! == 1})
                    if outOfStock.count > 0{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item is out of stock", value: "", table: nil))!)
                    }else{
                        let outOfStockA = allCartDetailsArray[0].Items!.filter({$0.IsOutOfStock! != 1})
                        let outOfStockAdditionals = outOfStockA.filter({$0.IsOutOfStock! == 1})
                        if outOfStockAdditionals.count > 0{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item additionals out of stock", value: "", table: nil))!)
                        }else{
                            if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                                if self.allCartDetailsArray[0].Cart![0].ConsumedPoints! == self.rewardsFilterArray[indexPath.row].RewardBand!{
                                    self.applyRewardPoints(isRemove: true, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
                                }else{
                                    self.applyRewardPoints(isRemove: false, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
                                }
                            }else{
                                self.applyRewardPoints(isRemove: false, rewardBand: self.rewardsFilterArray[indexPath.row].RewardBand!)
                            }
                        }
                    }
                }
            }
        }else{
            print("")
        }
    }
    @objc func frequencyInfoBtn_Tapped(_ sender : UIButton){
        popUpNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order delivered every", value: "", table: nil))!) \(self.allCartDetailsArray[0].Frequency![sender.tag].Days!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "day", value: "", table: nil))!)"
        let cellWidth = self.subscriptionFrequencyCV.bounds.width/3 - 20
        var xPosition = sender.frame.maxX
        if AppDelegate.getDelegate().appLanguage == "English"{
            if sender.tag == 0{
                xPosition = sender.frame.maxX
            }else if sender.tag == 1{
                xPosition = subscriptionFrequencyCV.frame.midX + (cellWidth/2) - 15
            }else if sender.tag == 2{
                xPosition = subscriptionFrequencyCV.frame.maxX - 17
            }
        }else{
            if sender.tag == 0{
                xPosition = subscriptionFrequencyCV.frame.maxX - cellWidth + 15
            }else if sender.tag == 1{
                xPosition = subscriptionFrequencyCV.frame.midX - (cellWidth/2) + 15
            }else if sender.tag == 2{
                xPosition = sender.frame.maxX
            }
        }
        var yPosition = 300 - myScrollView.contentOffset.y
        if view.frame.width > 380{
            yPosition = 315 - myScrollView.contentOffset.y
        }else{
            yPosition = 300 - myScrollView.contentOffset.y
        }
        let startPoint = CGPoint(x: xPosition, y: yPosition)
        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 285, height: 40)
        popover.show(self.popUpLblMainView, point: startPoint)
    }
    func applyRewardPoints(isRemove:Bool, rewardBand:Int){
        ANLoader.showLoading("", disableUI: true)
        let dic = ["OrderId": orderId, "IsRemove": isRemove, "RewardBand": rewardBand, "RequestBy":2] as [String : Any]
        CartModuleServices.ApplyRewardPointsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    self.isRemove = false
                    self.getDetailsService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    @objc func recomendedAdBtn_Tapped(_ sender : UIButton){
        if crossItemDetails[sender.tag].HasVariables != nil{
            if crossItemDetails[sender.tag].HasVariables! == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = AdditionalsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
                obj.itemID = crossItemDetails[sender.tag].Id!
                obj.storeId = allCartDetailsArray[0].Stores![0].Id!
                obj.selectOrderType = Int(allCartDetailsArray[0].Cart![0].OrderType!)
                //Address
                obj.selectAddressId = 0
                if let _ = allCartDetailsArray[0].Address{
                    if allCartDetailsArray[0].Address!.count > 0{
                        if allCartDetailsArray[0].Address![0].Id != nil{
                            obj.selectAddressId = Int(allCartDetailsArray[0].Address![0].Id!)
                        }
                    }
                }
                
                /*if allCartDetailsArray[0].Address![0].Id != nil{
                    obj.selectAddressId = Int(allCartDetailsArray[0].Address![0].Id!)
                }else{
                    obj.selectAddressId = 0
                }*/
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.titleLbl = crossItemDetails[sender.tag].NameEn!
                    obj.address = allCartDetailsArray[0].Stores![0].NameEn!
                }else{
                    obj.titleLbl = crossItemDetails[sender.tag].NameAr!
                    obj.address = allCartDetailsArray[0].Stores![0].NameAr!
                }
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                var addressId = 0
                if  allCartDetailsArray[0].Address!.count > 0 {
                    if allCartDetailsArray[0].Address![0].Id != nil{
                        addressId = Int(allCartDetailsArray[0].Address![0].Id!)
                    }
                }
                
                let dic:[String:Any] = ["userId": UserDef.getUserId(),"StoreId": allCartDetailsArray[0].Stores![0].Id!,"OrderType": Int(allCartDetailsArray[0].Cart![0].OrderType!)!,"MainCategoryId": crossItemDetails[sender.tag].MainCategoryId!,"CategoryId": crossItemDetails[sender.tag].CategoryId!,"SubCategoryId": crossItemDetails[sender.tag].SubCategoryId!,"MenuItemId": crossItemDetails[sender.tag].Id!,"GrinderItemId":0,"SizeId": crossItemDetails[sender.tag].Prices![0].SizeId!,"Quantity": 1,"Comment": "","AddressId" : addressId,"OrderStatusId" : 1,"Additionals": [], "RequestBy":2, "SectionType": allCartDetailsArray[0].Cart![0].SectionType!]
                CartModuleServices.AddToCartService(dic: dic, success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                        }
                    }else{
                        if self.isSubscription == true{
                            AppDelegate.getDelegate().subCartQuantity = data.Data!.CartItemsQuantity!
                            AppDelegate.getDelegate().subCartAmount = data.Data!.TotalPrice!
                        }else{
                            AppDelegate.getDelegate().cartQuantity = data.Data!.CartItemsQuantity!
                            AppDelegate.getDelegate().cartAmount = data.Data!.TotalPrice!
                        }
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                                if self.isSubscription == true{
                                    tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                                }else{
                                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                                }
                                tabItems[2].image = UIImage(named: "Cart Tab")
                                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                            }else{
                                tabItem.badgeValue = nil
                                tabItems[2].image = UIImage(named: "Tab Order")
                                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                            }
                        }
                        self.isRemove = true
                        DispatchQueue.main.async {
                            ANLoader.showLoading("", disableUI: true)
                            self.getDetailsService()
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }
        }else{
            var addressId = 0
            if allCartDetailsArray[0].Address![0].Id != nil{
                addressId = Int(allCartDetailsArray[0].Address![0].Id!)
            }else{
                addressId = 0
            }
            let dic:[String:Any] = ["userId": UserDef.getUserId(),"StoreId": allCartDetailsArray[0].Stores![0].Id!,"OrderType": Int(allCartDetailsArray[0].Cart![0].OrderType!)!,"MainCategoryId": crossItemDetails[sender.tag].MainCategoryId!,"CategoryId": crossItemDetails[sender.tag].CategoryId!,"SubCategoryId": crossItemDetails[sender.tag].SubCategoryId!,"MenuItemId": crossItemDetails[sender.tag].Id!,"GrinderItemId":0,"SizeId": crossItemDetails[sender.tag].Prices![0].SizeId!,"Quantity": 1,"Comment": "","AddressId" : addressId,"OrderStatusId" : 1,"Additionals": [], "RequestBy":2, "SectionType": allCartDetailsArray[0].Cart![0].SectionType!]
            CartModuleServices.AddToCartService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    if self.isSubscription == true{
                        AppDelegate.getDelegate().subCartQuantity = data.Data!.CartItemsQuantity!
                        AppDelegate.getDelegate().subCartAmount = data.Data!.TotalPrice!
                    }else{
                        AppDelegate.getDelegate().cartQuantity = data.Data!.CartItemsQuantity!
                        AppDelegate.getDelegate().cartAmount = data.Data!.TotalPrice!
                    }
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                            if self.isSubscription == true{
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().subCartQuantity)"
                            }else{
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                            }
                            tabItems[2].image = UIImage(named: "Cart Tab")
                            tabItems[2].selectedImage = UIImage(named: "Cart Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                        }else{
                            tabItem.badgeValue = nil
                            tabItems[2].image = UIImage(named: "Tab Order")
                            tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                            tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                        }
                    }
                    self.isRemove = true
                    DispatchQueue.main.async {
                        ANLoader.showLoading("", disableUI: true)
                        self.getDetailsService()
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
}
@available(iOS 13.0, *)
extension CartVC:CommentVCDelegate{
    func didTapAction(note: String, IndexPath: Int) {
        commentText = note
        let dic = ["CartItemId": allCartDetailsArray[0].Items![IndexPath].CartItemId!, "Action": "c", "Comment":"\(note)", "RequestBy":2] as [String : Any]
        CartModuleServices.UpdateCartItemService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.cartDetailsArray = [data.Data!]
                self.allCartDetailsArray = [data.Data!.Carts![0]]
                DispatchQueue.main.async {
                    self.AllData()
                    self.itemsTableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
