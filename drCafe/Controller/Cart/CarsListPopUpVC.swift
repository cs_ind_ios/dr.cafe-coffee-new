//
//  CarsListPopUpVC.swift
//  drCafe
//
//  Created by mac3 on 26/10/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

protocol CarsListPopUpVCDelegate {
    func didSelectCar(Id:Int)
}

class CarsListPopUpVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCarBtn: UIButton!
    
    var carsListPopUpVCDelegate:CarsListPopUpVCDelegate!
    var carDicArray = [CarsModel]()
    var currentStoreId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        if SaveAddressClass.carInformationArray.count > 0{
            if let _ = SaveAddressClass.carInformationArray[0].Cars{
                carDicArray = SaveAddressClass.carInformationArray[0].Cars!
            }
        }
        addCarBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
    }
    //MARK: Dismiss Button Action
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
//        let carDetails = carDicArray.filter({$0.IsDefault == true})
//        if carDetails.count > 0{
//            carsListPopUpVCDelegate.didSelectCar(Id: carDetails[0].Id)
//        }else{
//            carsListPopUpVCDelegate.didSelectCar(Id: 0)
//        }
        carsListPopUpVCDelegate.didSelectCar(Id: 0)
        self.dismiss(animated: true)
    }
    //MARK: Add Car Button Action
    @IBAction func addCarBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CarInformationVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarInformationVC") as! CarInformationVC
        obj.whereObj = 1
        obj.currentStoreId = currentStoreId
        obj.carInformationVCDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        present(obj, animated: false, completion: nil)
    }
}
extension CarsListPopUpVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carDicArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CarsListTVCell", value: "", table: nil))!, for: indexPath) as! CarsListTVCell
        
        let dic = carDicArray[indexPath.row]
        cell.carNumberLbl.text = dic.VehicleNumber
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.carColorLbl.text = dic.ColorEn
            cell.brandLbl.text = dic.BrandEn
        }else{
            cell.carColorLbl.text = dic.ColorAr
            cell.brandLbl.text = dic.BrandAr
        }
        
        if dic.IsDefault == true{
            cell.defaultCheckBtn.setImage(UIImage(named: "car_check_box"), for: .normal)
        }else{
            cell.defaultCheckBtn.setImage(UIImage(named: "car_check_box_unselected"), for: .normal)
        }

        cell.carImg.setImageColor(color: UIColor(hexString: "\(dic.ColorCode)"))
        
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editBtn_Tapped(_:)), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtn_Tapped(_:)), for: .touchUpInside)
        
        cell.carNumberNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Number", value: "", table: nil))!
        cell.brandNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Brand", value: "", table: nil))!
        cell.carColorNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Color", value: "", table: nil))!
        cell.defaultNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Default", value: "", table: nil))!
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        carsListPopUpVCDelegate.didSelectCar(Id: carDicArray[indexPath.row].Id)
        self.dismiss(animated: true)
    }
    @objc func editBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CarInformationVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarInformationVC") as! CarInformationVC
        obj.whereObj = 2
        obj.carDetailsDic = [carDicArray[sender.tag]]
        obj.currentStoreId = currentStoreId
        obj.carInformationVCDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        present(obj, animated: false, completion: nil)
    }
    @objc func deleteBtn_Tapped(_ sender: UIButton){
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are you sure to delete car information?", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            return
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            let dic:[String:Any] = ["Id":self.carDicArray[sender.tag].Id]
            CartModuleServices.DeleteCustomerCarService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    SaveAddressClass.carInformationArray[0].Cars!.remove(at: sender.tag)
                    self.carDicArray.remove(at: sender.tag)
                    self.tableView.reloadData()
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }))
    }
}
extension CarsListPopUpVC: CarInformationVCDelegate{
    func didAddOrUpdateCar(status: Bool) {
        if status == true{
            if SaveAddressClass.carInformationArray.count > 0{
                if let _ = SaveAddressClass.carInformationArray[0].Cars{
                    carDicArray = SaveAddressClass.carInformationArray[0].Cars!
                }
            }
            self.tableView.reloadData()
        }
    }
}
