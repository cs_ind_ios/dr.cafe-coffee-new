//
//  TimeSlotVC.swift
//  drCafe
//
//  Created by Devbox on 17/06/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

protocol TimeSlotVCDelegate {
    func didTapAction(expectDate:Date, startTime:String, endTime:String, whereType:Int, SlotId:Int)
}

@available(iOS 13.0, *)
class TimeSlotVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var estimateDateNameLbl: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var daysCollectionView: UICollectionView!
    @IBOutlet weak var timeSlotTableView: UITableView!
    @IBOutlet weak var timeSlotTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var timeSlotTableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeSlotTableViewBtmConstraint: NSLayoutConstraint!
    @IBOutlet weak var BGViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    var timeSlotVCDelegate:TimeSlotVCDelegate!
    var shipDetails = [ShippingMethodDetailsModel]()
    var whereObj = 0
    var weekDayNum:Int!
    var daySelectIndex = 0
    var firstTimeSlotSelectIndex = 0
    var selectSlotId:Int!
    var shipTimeSlotDate:String!
    var pauseFrequencyArray = [FrequencyOptionsModel]()
    var reschedule = false
    var rescheduleSelectSlotId:Int!
    var rescheduleTimeSlotDate:String!
    var currentDate:String!
    var maxDays:Int!
    var popover = Popover()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        daysCollectionView.delegate = self
        daysCollectionView.dataSource = self
        timeSlotTableView.delegate = self
        timeSlotTableView.dataSource = self
        
        //let timeExpect = shipDetails[0].ShippingTime!.components(separatedBy: "T")[0]
        dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: shipTimeSlotDate, inputDateformatType: .date, outputDateformatType: .ddMMMyyyy)
        dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        
        if reschedule == true{
            popUpNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reschedule PopUp Msg", value: "", table: nil))!
            estimateDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reschedule Upcoming Order", value: "", table: nil))!
            infoBtn.isHidden = false
        }else{
            estimateDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Date & Time Slot", value: "", table: nil))!
            popUpNameLbl.text = ""
            infoBtn.isHidden = true
        }
        if whereObj != 1{
            var numOfDays = shipDetails[0].FutureDays
            if reschedule == false{
                numOfDays = shipDetails[0].FutureDays
            }else{
                numOfDays = maxDays
            }
            SaveAddressClass.expectDatesArray.removeAll()
            SaveAddressClass.expectDatesArray.append(ExpectDates(id: 0, date: shipTimeSlotDate, isSelect: true))
            for i in 1...numOfDays{
                let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: DateConverts.convertStringToDate(date: SaveAddressClass.expectDatesArray[i-1].date, dateformatType: .date))
                //print(nextDay!)
                SaveAddressClass.expectDatesArray.append(ExpectDates(id: i, date: DateConverts.convertStringToStringDates(inputDateStr: "\(nextDay!)", inputDateformatType: .dateTimeZ, outputDateformatType: .date), isSelect: false))
            }
            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipTimeSlotDate, inputDateformatType: .date, outputDateformatType: .EEEE))")
            if reschedule == false{
                for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                    if selectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                        firstTimeSlotSelectIndex = i
                    }else{
                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                    }
                }
            }else{
                //if currentDate != nil{
//                    if currentDate > rescheduleTimeSlotDate{
//                        for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
//                            shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
//                        }
//                    }else{
                        if shipTimeSlotDate == rescheduleTimeSlotDate{
                            for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                                if rescheduleSelectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                                    firstTimeSlotSelectIndex = i
                                }else{
                                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                                }
                            }
                        }else{
                            for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                                if selectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                                    firstTimeSlotSelectIndex = i
                                }
                            }
                        }
//                    }
//                }else{
//                    for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
//                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
//                    }
//                }
            }
            
            //estimateDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Date & Time Slot", value: "", table: nil))!
            timeSlotTableViewTopConstraint.constant = 15
            timeSlotTableViewBtmConstraint.constant = 15
        }else{
            SaveAddressClass.expectDatesArray.removeAll()
            SaveAddressClass.expectDatesArray.append(ExpectDates(id: 0, date: shipTimeSlotDate, isSelect: true))
            for i in 1...90{
                let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: DateConverts.convertStringToDate(date: SaveAddressClass.expectDatesArray[i-1].date, dateformatType: .date))
                //print(nextDay!)
                SaveAddressClass.expectDatesArray.append(ExpectDates(id: i, date: DateConverts.convertStringToStringDates(inputDateStr: "\(nextDay!)", inputDateformatType: .dateTimeZ, outputDateformatType: .date), isSelect: false))
            }
            timeSlotTableViewHeight.constant = 0
            timeSlotTableViewTopConstraint.constant = 0
            timeSlotTableViewBtmConstraint.constant = 0
            BGViewHeight.constant = 260
            estimateDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Date", value: "", table: nil))!
        }
    }
    override func viewWillAppear(_ animated: Bool) {
//        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
//            doneBtn.setTitleColor(.white, for: .normal)
//            estimateDateNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
//        }else{
//            doneBtn.setTitleColor(.black, for: .normal)
//            estimateDateNameLbl.textColor = .darkGray
//            BGView.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
//        }
    }
    @IBAction func dateBtn_Tapped(_ sender: UIButton) {
    }
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func infoBtn_Tapped(_ sender: UIButton) {
        let startPoint = CGPoint(x: sender.frame.maxX - 10, y: BGView.frame.minY + 40)
        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 310, height: 130)
        popover.show(self.popUpLblMainView, point: startPoint)
    }
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        if reschedule == true{
            if whereObj != 1{
                let selectTimeSlot = shipDetails[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.isSelect! == true})
                if selectTimeSlot.count > 0{
                    Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Subscription has been Rescheduled", value: "", table: nil))!) \(dateLbl.text!) \n \(selectTimeSlot[0].StartTime) - \(selectTimeSlot[0].EndTime)", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.dismiss(animated: true, completion: nil)
                    }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.timeSlotVCDelegate.didTapAction(expectDate: DateConverts.convertStringToDate(date: self.dateLbl.text!, dateformatType: .date), startTime: selectTimeSlot[0].StartTime,endTime: selectTimeSlot[0].EndTime, whereType: self.whereObj, SlotId: self.selectSlotId!)
                        self.dismiss(animated: true, completion: nil)
                    }))
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Time Slot", value: "", table: nil))!)
                }
            }else{
                Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Subscription has been Rescheduled", value: "", table: nil))!) \(dateLbl.text!)", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.dismiss(animated: true, completion: nil)
                }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.timeSlotVCDelegate.didTapAction(expectDate: DateConverts.convertStringToDate(date: self.dateLbl.text!, dateformatType: .date), startTime: "",endTime: "", whereType: self.whereObj, SlotId: 0)
                    self.dismiss(animated: true, completion: nil)
                }))
            }
        }else{
            if whereObj != 1{
                let selectTimeSlot = shipDetails[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.isSelect! == true})
                if selectTimeSlot.count > 0{
                    timeSlotVCDelegate.didTapAction(expectDate: DateConverts.convertStringToDate(date: dateLbl.text!, dateformatType: .date), startTime: selectTimeSlot[0].StartTime,endTime: selectTimeSlot[0].EndTime, whereType: whereObj, SlotId: selectSlotId!)
                    dismiss(animated: true, completion: nil)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Time Slot", value: "", table: nil))!)
                }
            }else{
                timeSlotVCDelegate.didTapAction(expectDate: DateConverts.convertStringToDate(date: dateLbl.text!, dateformatType: .date), startTime: "",endTime: "", whereType: whereObj, SlotId: 0)
                dismiss(animated: true, completion: nil)
            }
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
@available(iOS 13.0, *)
extension TimeSlotVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if whereObj != 1{
            return shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if whereObj != 1{
            return 60
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if whereObj != 1{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!) as! timeSlotTVCell
            
            cell.fromDateLbl.text = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].StartTime
            cell.toDateLbl.text = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].EndTime
            
            if daySelectIndex == 0{
                if firstTimeSlotSelectIndex != nil{
                    if indexPath.row < firstTimeSlotSelectIndex{
                        cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booked", value: "", table: nil))!
                        cell.bookedNameLblWidth.constant = 50
                        cell.bookedNameLblTrailing.constant = 15
                    }else{
                        if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true{
                                cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                            }else{
                                cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booked", value: "", table: nil))!
                            }
                            cell.bookedNameLblWidth.constant = 50
                            cell.bookedNameLblTrailing.constant = 15
                        }else{
                            cell.bookedNameLblWidth.constant = 0
                            cell.bookedNameLblTrailing.constant = 0
                        }
                    }
                }else{
                    if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                        if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true{
                            cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                        }else{
                            cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booked", value: "", table: nil))!
                        }
                        cell.bookedNameLblWidth.constant = 50
                        cell.bookedNameLblTrailing.constant = 15
                    }else{
                        cell.bookedNameLblWidth.constant = 0
                        cell.bookedNameLblTrailing.constant = 0
                    }
                }
            }else{
                if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                    if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true{
                        cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
                    }else{
                        cell.bookedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Booked", value: "", table: nil))!
                    }
                    cell.bookedNameLblWidth.constant = 50
                    cell.bookedNameLblTrailing.constant = 15
                }else{
                    cell.bookedNameLblWidth.constant = 0
                    cell.bookedNameLblTrailing.constant = 0
                }
            }
            
            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect! == true{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.9294117647, blue: 0.9647058824, alpha: 1)
                cell.BGView.borderWidth = 0
            }else{
                if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                    cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.BGView.borderWidth = 1
                    cell.BGView.borderColor = #colorLiteral(red: 0.7098039216, green: 0.5411764706, blue: 0.5019607843, alpha: 1)
                }else{
                    if firstTimeSlotSelectIndex != nil{
                        if indexPath.row < firstTimeSlotSelectIndex{
                            cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.BGView.borderWidth = 1
                            cell.BGView.borderColor = #colorLiteral(red: 0.7098039216, green: 0.5411764706, blue: 0.5019607843, alpha: 1)
                        }else{
                            cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                            cell.BGView.borderWidth = 0
                        }
                    }else{
                        cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                        cell.BGView.borderWidth = 0
                    }
//                    cell.BGView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
//                    cell.BGView.borderWidth = 0
                }
            }
            
            if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count > 1{
                BGView.layoutIfNeeded()
                timeSlotTableView.layoutIfNeeded()
                timeSlotTableViewHeight.constant = timeSlotTableView.contentSize.height + 20
                BGViewHeight.constant = 310 + timeSlotTableView.contentSize.height
            }else{
                BGView.layoutIfNeeded()
                timeSlotTableView.layoutIfNeeded()
                timeSlotTableViewHeight.constant = 60
                BGViewHeight.constant = 350
            }
            
            cell.selectionStyle = .none
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "timeSlotTVCell", value: "", table: nil))!) as! timeSlotTVCell
            cell.fromDateLbl.text = ""
            cell.toDateLbl.text = ""
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if whereObj != 1{
            if daySelectIndex == 0{
                if indexPath.row < firstTimeSlotSelectIndex{
                    print("Not Selected")
                }else{
                    if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Selected Time Slot is Full", value: "", table: nil))!)
                    }else{
                        for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                            shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                        }
                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect = true
                        selectSlotId = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].Id
                        tableView.reloadData()
                    }
                }
            }else{
                if shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsClose == true || shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].IsSlotFull == true{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Selected Time Slot is Full", value: "", table: nil))!)
                }else{
                    for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                    }
                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].isSelect = true
                    selectSlotId = shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![indexPath.row].Id
                    tableView.reloadData()
                }
            }
        }else{
            print("")
        }
    }
}
@available(iOS 13.0, *)
extension TimeSlotVC:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SaveAddressClass.expectDatesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "dayWithDateTVCell", bundle: nil), forCellWithReuseIdentifier: "dayWithDateTVCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayWithDateTVCell", for: indexPath) as! dayWithDateTVCell

        cell.dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .dd))"
        cell.dayNameLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .EEE))"
        
        if pauseFrequencyArray.count > 0{
            if SaveAddressClass.expectDatesArray[indexPath.row].isSelect == true{
                cell.dateLbl.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.8039215686, blue: 0.968627451, alpha: 1)
                cell.dateLbl.layer.cornerRadius = 10
                cell.dateLbl.clipsToBounds = true
            }else{
                let pauseDate = pauseFrequencyArray.filter({DateConverts.convertStringToStringDates(inputDateStr: "\($0.OptionEN!.split(separator: ".")[0])", inputDateformatType: .dateTtime, outputDateformatType: .dateR) == DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .dateR)})
                if pauseDate.count > 0{
                    cell.dateLbl.backgroundColor = #colorLiteral(red: 0.8612491488, green: 0.9493328929, blue: 0.9784371257, alpha: 1)
                    cell.dateLbl.layer.cornerRadius = 10
                    cell.dateLbl.clipsToBounds = true
                    cell.dateLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }else{
                    cell.dateLbl.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    cell.dateLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
            }
        }else{
            if SaveAddressClass.expectDatesArray[indexPath.row].isSelect == true{
                cell.dateLbl.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.8039215686, blue: 0.968627451, alpha: 1)
                cell.dateLbl.layer.cornerRadius = 10
                cell.dateLbl.clipsToBounds = true
            }else{
                cell.dateLbl.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.dateLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if whereObj != 1{
            for i in 0...SaveAddressClass.expectDatesArray.count - 1{
                SaveAddressClass.expectDatesArray[i].isSelect = false
            }
            daySelectIndex = indexPath.row
            SaveAddressClass.expectDatesArray[indexPath.row].isSelect = true
            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .EEEE))")
            for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
            }
            let time = shipDetails[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
            if time.count > 0{
                for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                    if daySelectIndex == 0{
                        for i in 0...shipDetails[0].ShippingSlots![weekDayNum!-1].Slot!.count - 1{
                            if reschedule == false{
                                if firstTimeSlotSelectIndex == i{
                                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                                }else{
                                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                                }
                            }else{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                            }
                        }
                    }else{
                        if reschedule == false{
                            if time[0].Id == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                            }else{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                            }
                        }else{
                            if rescheduleTimeSlotDate != nil{
                                if SaveAddressClass.expectDatesArray[indexPath.row].date == rescheduleTimeSlotDate{
                                    if rescheduleSelectSlotId == shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].Id{
                                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = true
                                    }else{
                                        shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                                    }
                                }else{
                                    shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                                }
                            }else{
                                shipDetails[0].ShippingSlots![weekDayNum!-1].Slot![i].isSelect = false
                            }
                        }
                    }
                }
            }
            timeSlotTableView.reloadData()
            daysCollectionView.reloadData()
            dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .ddMMMyyyy))"
        }else{
            for i in 0...SaveAddressClass.expectDatesArray.count - 1{
                SaveAddressClass.expectDatesArray[i].isSelect = false
            }
            daySelectIndex = indexPath.row
            SaveAddressClass.expectDatesArray[indexPath.row].isSelect = true
            daysCollectionView.reloadData()
            dateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.expectDatesArray[indexPath.row].date, inputDateformatType: .date, outputDateformatType: .ddMMMyyyy))"
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.daysCollectionView.bounds
        //let screenWidth = screenSize.width
        return CGSize(width: 150, height: 177)
    }
}
