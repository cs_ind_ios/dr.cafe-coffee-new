//
//  SubscriptionFrequencyCartCVCell.swift
//  drCafe
//
//  Created by Devbox on 30/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SubscriptionFrequencyCartCVCell: UICollectionViewCell {
    
    @IBOutlet var BGView: UIView!
    @IBOutlet weak var frquencyNameLbl: UILabel!
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var infoBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
