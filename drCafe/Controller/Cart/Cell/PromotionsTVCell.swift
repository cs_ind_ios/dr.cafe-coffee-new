//
//  PromotionsTVCell.swift
//  drCafe
//
//  Created by Devbox on 13/07/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class PromotionsTVCell: UITableViewCell {

    @IBOutlet weak var promotionSelectImg: UIImageView!
    @IBOutlet weak var promotionNameLbl: UILabel!
    @IBOutlet weak var validTillNameLbl: UILabel!
    @IBOutlet weak var validDateLbl: UILabel!
    @IBOutlet weak var promotionImg: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var availableDrinksNameLbl: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var quantityBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantityView: CustomView!
    @IBOutlet weak var availableDrinksViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var promotionQuantityLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
