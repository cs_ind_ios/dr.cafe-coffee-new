//
//  timeSlotTVCell.swift
//  drCafe
//
//  Created by Devbox on 17/06/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class timeSlotTVCell: UITableViewCell {

    @IBOutlet weak var BGView: CustomView!
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toNameLbl: UILabel!
    @IBOutlet weak var bookedNameLbl: UILabel!
    @IBOutlet weak var bookedNameLblWidth: NSLayoutConstraint!
    @IBOutlet weak var bookedNameLblTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
