//
//  MultipleOrdersTVCell.swift
//  drCafe
//
//  Created by Devbox on 08/10/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class MultipleOrdersTVCell: UITableViewCell {

    @IBOutlet weak var orderTypeImg: UIImageView!
    @IBOutlet weak var addressTitleNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var numOfItemsLbl: UILabel!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
