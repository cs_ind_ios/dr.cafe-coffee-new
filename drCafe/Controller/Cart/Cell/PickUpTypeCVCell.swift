//
//  PickUpTypeCVCell.swift
//  drCafe
//
//  Created by mac3 on 09/11/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class PickUpTypeCVCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var typeImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
