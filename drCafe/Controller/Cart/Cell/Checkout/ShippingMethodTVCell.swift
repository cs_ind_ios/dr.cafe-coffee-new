//
//  ShippingMethodTVCell.swift
//  drCafe
//
//  Created by Devbox on 30/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class ShippingMethodTVCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var methodCostLbl: UILabel!
    @IBOutlet weak var methodNameLbl: UILabel!
    @IBOutlet weak var methodSelectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
