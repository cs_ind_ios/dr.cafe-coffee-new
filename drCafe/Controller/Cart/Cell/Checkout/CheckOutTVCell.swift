//
//  CheckOutTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 19/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class CheckOutTVCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemWtLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    
    @IBOutlet weak var additionalNameLbl: UILabel!
    @IBOutlet weak var additionalsLbl: UILabel!
    @IBOutlet weak var additionalsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var messageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemPointsLblWidth: NSLayoutConstraint!
    @IBOutlet weak var redeemPointsLblTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var quantityStackView: UIStackView!
    
    @IBOutlet weak var grinderNameLbl: UILabel!
    @IBOutlet weak var grinderViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var pointsLblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pointsItemsNameLbl: UILabel!
    @IBOutlet weak var redeemPointsMsgLbl: UILabel!
    @IBOutlet weak var redeemPointsLbl: UILabel!
    @IBOutlet weak var redeemPointsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemPointsView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
