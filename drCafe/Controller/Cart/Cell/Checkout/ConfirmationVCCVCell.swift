//
//  ConfirmationVCCVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 21/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class ConfirmationVCCVCell: UICollectionViewCell {

    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var currencyNameLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var addBtnView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var sugestiveItemAddBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
