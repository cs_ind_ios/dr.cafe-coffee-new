//
//  PaymentMethodTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 20/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class PaymentMethodTVCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var BGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
