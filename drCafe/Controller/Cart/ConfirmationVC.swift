//
//  ConfirmationVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 20/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SafariServices
import Firebase
@available(iOS 13.0, *)
class ConfirmationVC: UIViewController, CLLocationManagerDelegate, SFSafariViewControllerDelegate, UIPopoverPresentationControllerDelegate{
    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var changeAddressNameLbl: UILabel!
    @IBOutlet var changeAddressView: UIView!
    @IBOutlet weak var changeAddressViewWidth: NSLayoutConstraint!
    @IBOutlet weak var changeAddressViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var changeAddressBtn: UIButton!
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var orderTypeLblHeight: NSLayoutConstraint!
    @IBOutlet weak var orderTypeLblTop: NSLayoutConstraint!
    @IBOutlet weak var changeAddressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var carInfoNameLbl: UILabel!
    @IBOutlet weak var carVehicleNumNameLbl: UILabel!
    @IBOutlet weak var carVehicleNumLbl: UILabel!
    @IBOutlet weak var carBrandNameLbl: UILabel!
    @IBOutlet weak var carBrandLbl: UILabel!
    @IBOutlet weak var carColorNameLbl: UILabel!
    @IBOutlet weak var carColorLbl: UILabel!
    @IBOutlet weak var carDetailsView: UIView!
    @IBOutlet weak var carDetailsViewHeight: NSLayoutConstraint!//123
    @IBOutlet weak var carDetailsViewTop: NSLayoutConstraint!//15
    
    @IBOutlet weak var estimateDeliveryTimeNameLbl: UILabel!
    @IBOutlet weak var changeDateNameLbl: UILabel!
    @IBOutlet weak var estimateDeliveryDateLbl: UILabel!
    @IBOutlet weak var estimateDeliveryTimeLbl: UILabel!
    @IBOutlet weak var changeTimeBtn: UIButton!
    @IBOutlet weak var changeDateView: CustomView!
    
    @IBOutlet var BGView: UIView!

    @IBOutlet weak var billAmountNameLbl: UILabel!
    @IBOutlet weak var promocodeLbl: UILabel!
    @IBOutlet weak var applyPromocodeLbl: UILabel!
    @IBOutlet weak var promocodeView: CustomView!
    @IBOutlet weak var promocodeNameLbl: UILabel!
    @IBOutlet weak var promocodeDropDownBtn: UIButton!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargeslbl: UILabel!
    @IBOutlet weak var vatNameLbl: UILabel!
    @IBOutlet weak var vatAmountLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var billAmountDropDownBtn: UIButton!
    @IBOutlet weak var priceDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var DCWalletNameLbl: UILabel!
    @IBOutlet weak var netAmountViewHeight: NSLayoutConstraint!
    @IBOutlet weak var walletAvailableBalLbl: UILabel!
    @IBOutlet weak var walletCheckedBtn: UIButton!
    @IBOutlet weak var walledLbl: UILabel!
    @IBOutlet weak var remainingNetAmountLbl: UILabel!
    @IBOutlet weak var paymentMethodDropBtn: UIButton!
    @IBOutlet weak var paymentMethodNameLbl: UILabel!
    @IBOutlet weak var paymentDetailsView: UIView!
    @IBOutlet weak var paymentDetailsTableView: UITableView!
    @IBOutlet weak var paymentDetailsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet var changeTimeView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var changeTimeCancelBtn: CustomButton!
    @IBOutlet weak var changeTimeDoneBtn: CustomButton!
    @IBOutlet weak var walletCalcViewHeight: NSLayoutConstraint!
    @IBOutlet weak var walletCalcViewBottom: NSLayoutConstraint!
    //@IBOutlet var confirmOrderView: UIView!
    @IBOutlet weak var confirmOrderBtn: UIButton!
    //@IBOutlet weak var editOrderBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    //@IBOutlet weak var middleLineLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryChargesNameLblBottom: NSLayoutConstraint!
    @IBOutlet weak var dcPayNameLbl: UILabel!
    @IBOutlet weak var dcPayLbl: UILabel!
    @IBOutlet weak var dcPayNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var dcPayNameLblBottom: NSLayoutConstraint!
    
    @IBOutlet weak var promocodeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var promoCodeViewBtm: NSLayoutConstraint!
    @IBOutlet weak var dcPayViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dcPayViewBtm: NSLayoutConstraint!
    
    @IBOutlet weak var bottomNetTotalNameLbl: UILabel!
    @IBOutlet weak var bottomNetTotalLbl: UILabel!
    @IBOutlet weak var deliveryChargesNameLbl: UILabel!
    @IBOutlet weak var bottomFreeDeliveryMsgLblHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bottomView: CardView!
    @IBOutlet weak var bottomFreeDeliveryMsgLbl: UILabel!
    @IBOutlet weak var shippingMethodNameLbl: UILabel!
    @IBOutlet weak var shippingMethodDropBtn: UIButton!
    @IBOutlet weak var shippingMethodMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shippingMethodMainViewBottom: NSLayoutConstraint!
    @IBOutlet weak var shippingMethodTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shippingMethodTableView: UITableView!
    @IBOutlet weak var shippingMethodLineLbl: UILabel!
    
    @IBOutlet weak var deliveryFrequencyMainView: UIView!
    @IBOutlet weak var deliveryFrequencyMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryFrequencyMainViewBottom: NSLayoutConstraint!
    @IBOutlet weak var frequencyLbl: UILabel!
    @IBOutlet weak var frequencyChangeNameLbl: UILabel!
    
    @IBOutlet weak var inStoreView: UIView!
    @IBOutlet weak var inStoreNameLbl: UILabel!
    @IBOutlet weak var inStoreBtn: UIButton!
    @IBOutlet weak var driveInView: UIView!
    @IBOutlet weak var driveInNameLbl: UILabel!
    @IBOutlet weak var driveInBtn: UIButton!
    @IBOutlet weak var driveThruView: UIView!
    @IBOutlet weak var driveThruLbl: UILabel!
    @IBOutlet weak var driveThruBtn: UIButton!
    @IBOutlet weak var inStoreViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pickupTypeCollectionView: UICollectionView!
    @IBOutlet weak var pickupTypeCollectionViewHeight: NSLayoutConstraint!
    
    var cellHeights = [IndexPath: CGFloat]()
    var paymentNameArray:[String]!
    //let paymentImgsArray = ["Online Payment", "Apple Pay", "STC Pay", "Cash On Pickup"]
    let paymentImgsArray = ["Apple Pay","Cash On Pickup"]
    var paymentSelectIndex:Int!
    var cartDetailsArray = [LoadCartDetailsModel]()
    var paymentDetailsArray = [PayementMethodsModel]()
    var shippingMethodsArray = [ShippingMethodDetailsModel]()
    var quantity = 0
    var whereObj = 0
    var popover = Popover()
    var currentDate : Date = Date()
    var isChangeTime:Bool = false
    var changeTime:Date!
    var travelDuration:Int = 0
    var expectTime:Date!
    var kitchenStartTime:Date!
    var orderExpectTime:Date!
    var paymentSelectId = 0
    var walletselect = 0
    var walletAmount = 0.0
    var deliveryCharge = 0.0
    var vatCharge = 0.0
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    //Payment
    static var instance: ConfirmationVC!
    var isApplePay:Bool = false
    var isSTCPay:Bool = false
    var isMadaPay:Bool = false
    var isVisa:Bool = false
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.drcafecoffee.payments"
    let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    // MARK: - The payment brands for Ready-to-use UI "APPLEPAY"
    static let checkoutPaymentBrands = ["VISA", "MASTER", "AMEX"]
    static let STCPAYPaymentBrands = ["STC_PAY"]
    static let MADAPAYPaymentBrands = ["MADA"]
    
    var netAmount:Double = 0.0
    var InvoiceNoStr:String = ""
    var stcPayNumber:String = ""
    
    var isFirst = true
    var isShippingMethodCall = true
    var firstShipDate = ""
    var firstSelectShipId = 0
    var shipTimeSlot = ""
    var shipTimeSlotDate = ""
    var selectSlotId = 0
    
    var isMenu = false
    var orderType:Int!
    var storeId:Int!
    var sectionType:Int!
    var SwiftTimer = Timer()
    var manualTimer = Timer()
    //var loadTimer = Timer()
    var isSubscription = false
    var orderId = 0
    var frequencyId = 0
    var trackOrderId = 0
    
    var isOnlinePaymeny:Bool = false
    var isSelectAddress:Bool = false
    var isBackApplePay:Bool = false
    var isAppleDevice:Bool = false
    
    var carDetailsArray:SubOrderDetailsDataModel!
    var pickupSelectId:Int!
    var currentStoreId = 0
    var selectCarDicArray = [CarsModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        ConfirmationVC.instance = self
        paymentDetailsTableView.delegate = self
        paymentDetailsTableView.dataSource = self
        shippingMethodTableView.delegate = self
        shippingMethodTableView.dataSource = self
        backBtn.isHidden = false
        pickupTypeCollectionView.delegate = self
        pickupTypeCollectionView.dataSource = self
        
        tabBarController?.tabBar.isHidden = true
        isFirst = true
        firstShipDate = ""
        firstSelectShipId = 0
        vatCharge = 0.0
        
        // Apple Play Support
        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
        request.currencyCode = "SAR"
        let amount = NSDecimalNumber(value: 10)
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
            if let _ = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                // Apple Pay Support
                isAppleDevice = true
            }else{
                // Apple Pay not support
                isAppleDevice = false
            }
        }else{
            // Apple Pay not support
            isAppleDevice = false
        }
        
        if isAppleDevice == true {
            paymentNameArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apple Pay", value: "", table: nil))!,(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cash", value: "", table: nil))!]
        }else{
            paymentNameArray = [(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cash", value: "", table: nil))!]
        }
        self.myScrollView.isHidden = true
        self.bottomView.isHidden = true
        if isUserLogIn() == false {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: false)
            return
        }

        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        
        bottomFreeDeliveryMsgLbl.textColor = .black
        bottomFreeDeliveryMsgLbl.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.9450980392, blue: 0.9725490196, alpha: 1)

        BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        confirmOrderBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        changeTimeCancelBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        changeTimeDoneBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        
        bottomFreeDeliveryMsgLbl.text = ""
        bottomNetTotalLbl.text = ""
        bottomFreeDeliveryMsgLblHeight.constant = 0
        bottomNetTotalNameLbl.text = ""
        self.promocodeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply Coupon", value: "", table: nil))!
        self.frequencyChangeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change", value: "", table: nil))!
        self.carInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Information", value: "", table: nil))!
        self.carVehicleNumNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Number", value: "", table: nil))!
        self.carBrandNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Brand", value: "", table: nil))!
        self.carColorNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Car Color", value: "", table: nil))!
        
        //getDetailsService()
        
        if isSubscription == true{
            promocodeViewHeight.constant = 0
            promoCodeViewBtm.constant = 0
            dcPayViewHeight.constant = 0
            dcPayViewBtm.constant = 0
        }else{
            promocodeViewHeight.constant = 62
            promoCodeViewBtm.constant = 15
            dcPayViewHeight.constant = 80
            dcPayViewBtm.constant = 15
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            pickupTypeCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            pickupTypeCollectionView.semanticContentAttribute = .forceRightToLeft
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidEnterBackground),
                                                      name: UIApplication.didEnterBackgroundNotification,
                                                      object: nil)
               
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnteredForeground),
                                                      name: UIApplication.willEnterForegroundNotification,
                                                      object: nil)
        if isOnlinePaymeny == false {
            getDetailsService()
        }
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
        if isOnlinePaymeny == false {
            SwiftTimer.invalidate()
            manualTimer.invalidate()
            //loadTimer.invalidate()
        }
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        if isOnlinePaymeny == false {
            DispatchQueue.main.async {
                self.getDetailsService()
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if self.isSelectAddress == true || self.isOnlinePaymeny == true{
            self.isSelectAddress = false
            return
        }
        SwiftTimer.invalidate()
        manualTimer.invalidate()
        //loadTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    //MARK: Start Timer Get Current Time
    func startTimer() {
        SwiftTimer = Timer.scheduledTimer(timeInterval: 300.0, target: self, selector: #selector(ConfirmationVC.currentTimeGetService), userInfo: nil, repeats: true);
    }
    //MARK: Start Timer Manually Set Current Time
    func startManualTimer() {
        manualTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(ConfirmationVC.manualTimeCalculate), userInfo: nil, repeats: true);
    }
//    //MARK: Start Timer Get Load Cart
//    func startLoadCartTimer() {
//        loadTimer = Timer.scheduledTimer(timeInterval: 300.0, target: self, selector: #selector(ConfirmationVC.getDetailsService), userInfo: nil, repeats: true);
//    }
    @objc func manualTimeCalculate(){
        let curDate = Calendar.current.date(byAdding: .minute, value: 1, to: currentDate)
        currentDate = curDate!
        if self.travelDuration == 0{
            self.getEstimationTime()
        }else{
            self.ExpetectedTimeCalculatMethod()
        }
    }
    func selectOrderTypeVCMethod(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj:SelectOrderTypeVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectOrderTypeVC") as! SelectOrderTypeVC
        self.navigationController?.pushViewController(obj, animated: false)
    }
    @objc func getDetailsService(){
        var dic:[String : Any]!
        if isMenu == true{
            dic = ["Orderid":orderId, "storeId":storeId!, "OrderTypeId":orderType!, "sectiontype":sectionType!, "requestBy": 2, "IsRemove":false, "IsSubscription": isSubscription]
        }else{
            //dic = ["UserId":"\(UserDef.getUserId())", "RequestBy":2]
            dic = ["Orderid":orderId, "storeId":0, "OrderTypeId":0, "sectiontype":0, "requestBy": 2, "IsRemove":false, "IsSubscription": isSubscription]
        }
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("LoadCartService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        CartModuleServices.LoadCartService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.myScrollView.isHidden = true
                self.bottomView.isHidden = true
                AppDelegate.getDelegate().cartQuantity = 0
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 {
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
                SaveAddressClass.promotionsArray.removeAll()
                //self.selectOrderTypeVCMethod()
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                }else{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                }
            }else{
                if self.isSubscription == true{
                    self.cartDetailsArray = data.Data!.Carts!.filter({$0.Subscription == true})
                }else{
                    self.cartDetailsArray = data.Data!.Carts!.filter({$0.Subscription == false})
                }
                
                // Payment Array
                self.paymentDetailsArray = data.Data!.Payment!
                if self.isAppleDevice == false {
                    if let row =   self.paymentDetailsArray.firstIndex(where: {$0.Id == 5}) {
                        self.paymentDetailsArray.remove(at: row)
                    }
                }
                
                DispatchQueue.main.async {
                    if self.cartDetailsArray.count > 0{
                        self.frequencyId = self.cartDetailsArray[0].Cart![0].FrequencyId!
                        if self.cartDetailsArray[0].Stores![0].CurrentDateTime != nil{
                            if self.cartDetailsArray[0].Stores![0].CurrentDateTime!.contains("."){
                                var time = self.cartDetailsArray[0].Stores![0].CurrentDateTime!
                                time = time.components(separatedBy: ".")[0]
                                self.currentDate = DateConverts.convertStringToDate(date: time, dateformatType: .dateTtime)
                            }else{
                                self.currentDate = DateConverts.convertStringToDate(date: self.cartDetailsArray[0].Stores![0].CurrentDateTime!, dateformatType: .dateTtime)
                            }
                        }
                        if self.cartDetailsArray[0].Items!.count > 0{
                            self.myScrollView.isHidden = false
                            self.bottomView.isHidden = false
                            if self.cartDetailsArray[0].Cart![0].SectionType! == 2{
                                //self.shippingMethodDetailsGetService()
                                if self.isShippingMethodCall == false{
                                    self.shippingMethodData()
                                    self.AllData()
                                }else{
                                    self.isShippingMethodCall = false
                                    self.shippingMethodDetailsGetService()
                                }
                            }else{
                                self.AllData()
                            }
                        }else{
                            //self.selectOrderTypeVCMethod()
                            AppDelegate.getDelegate().cartQuantity = 0
                            if let tabItems = self.tabBarController?.tabBar.items {
                                let tabItem = tabItems[2]
                                if AppDelegate.getDelegate().cartQuantity > 0 {
                                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                                    tabItems[2].image = UIImage(named: "Cart Tab")
                                    tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                                }else{
                                    tabItem.badgeValue = nil
                                    tabItems[2].image = UIImage(named: "Tab Order")
                                    tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                                }
                            }
                        }
                    }
                }
                if self.paymentDetailsArray.count > 0{
                    for i in 0...self.paymentDetailsArray.count-1{
                        self.paymentDetailsArray[i].isSelect = false
                        if self.paymentSelectIndex != nil{
                            if self.paymentSelectId == self.paymentDetailsArray[i].Id!{
                                self.paymentDetailsArray[i].isSelect! = true
                            }
                        }
                    }
                }
                if self.paymentDetailsArray.count == 1{
                    self.paymentDetailsArray[0].isSelect = true
                    self.paymentSelectId = self.paymentDetailsArray[0].Id!
                }
//                if self.isFirst == true{
//                    self.PromotionDetailsGetService()
//                    self.isFirst = false
//                }else{
//                    //Promocode
//                    if self.cartDetailsArray[0].Cart![0].PromoCode != nil && self.cartDetailsArray[0].Cart![0].PromoCode! != ""{
//                        self.promocodeLbl.isHidden = true
//                        self.promocodeNameLbl.isHidden = true
//                        self.applyPromocodeLbl.isHidden = false
//                        self.applyPromocodeLbl.text = self.cartDetailsArray[0].Cart![0].PromoCode!
//                        //self.promocodeNameLbl.text = self.cartDetailsArray[0].Cart![0].PromoCode!
//                        self.promocodeDropDownBtn.setImage(UIImage(named: "promo_cancel"), for: .normal)
//                    }else{
//                        self.promocodeLbl.isHidden = false
//                        self.promocodeNameLbl.isHidden = false
//                        self.applyPromocodeLbl.isHidden = true
//                    }
//                }
                if SaveAddressClass.promotionsArray.count > 0{
                    //Promocode
                    if self.cartDetailsArray[0].Cart![0].PromoCode != nil && self.cartDetailsArray[0].Cart![0].PromoCode! != ""{
                        self.promocodeLbl.isHidden = true
                        self.promocodeNameLbl.isHidden = true
                        self.applyPromocodeLbl.isHidden = false
                        self.applyPromocodeLbl.text = self.cartDetailsArray[0].Cart![0].PromoCode!
                        self.promocodeDropDownBtn.setImage(UIImage(named: "promo_cancel"), for: .normal)
                    }else{
                        self.promocodeLbl.isHidden = false
                        self.promocodeNameLbl.isHidden = false
                        self.applyPromocodeLbl.isHidden = true
                        if SaveAddressClass.promotionsArray.count > 0{
                            self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have", value: "", table: nil))!) \(SaveAddressClass.promotionsArray.count) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupons", value: "", table: nil))!)"
                        }else{
                            self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!)"
                        }
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            self.promocodeDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                        }else{
                            self.promocodeDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                        }
                    }
                }else{
                    self.promocodeLbl.isHidden = false
                    self.promocodeNameLbl.isHidden = false
                    self.applyPromocodeLbl.isHidden = true
                    self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "You may have coupons", value: "", table: nil))!)"
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        self.promocodeDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                    }else{
                        self.promocodeDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func shippingMethodDetailsGetService(){
        let dic:[String : Any] = ["OrderId":self.cartDetailsArray[0].Cart![0].Id!, "RequestBy":2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("ShippingMethodService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        CartModuleServices.ShippingMethodService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.shippingMethodsArray = data.Data!
                DispatchQueue.main.async {
                    if self.shippingMethodsArray.count > 0{
                        for i in 0...self.shippingMethodsArray.count - 1{
                            if self.shippingMethodsArray[i].Id == self.firstSelectShipId{
                                self.shippingMethodsArray[i].isSelect = true
                            }else{
                                self.shippingMethodsArray[i].isSelect = false
                            }
                        }
                    }
                    var shipDate = ""
                    if self.shippingMethodsArray.count == 1{
                        shipDate = self.shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0]
                    }else{
                        let shipDetails = self.shippingMethodsArray.filter({$0.Id == self.firstSelectShipId})
                        if shipDetails.count > 0{
                            shipDate = shipDetails[0].ShippingTime.components(separatedBy: "T")[0]
                        }
                    }
                    self.shippingMethodData()
                    self.AllData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func shippingMethodData(){
        //Shipping Method
        if cartDetailsArray[0].Cart![0].SectionType! == 2 && cartDetailsArray[0].Address![0].Id != nil{
            if shippingMethodsArray.count > 0{
                shippingMethodMainViewHeight.constant = CGFloat(50 + (50 * shippingMethodsArray.count))
                shippingMethodMainViewBottom.constant = 15
                shippingMethodTableView.reloadData()
                if shippingMethodsArray.count == 1{
                    shippingMethodsArray[0].isSelect = true
                    var vatPercentage = (Double(cartDetailsArray[0].Cart![0].VatPercentage!)/100.0 + 1.0)
                    vatPercentage = Double(shippingMethodsArray[0].ShippingCost) / vatPercentage
                    vatCharge = Double(vatPercentage - shippingMethodsArray[0].ShippingCost) * -1
                    deliveryCharge = shippingMethodsArray[0].ShippingCost - vatCharge
                    bottomFreeDeliveryMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add items worth", value: "", table: nil))!) \(shippingMethodsArray[0].FreeDeliveryAmount) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "more for free delivery", value: "", table: nil))!)"
                    //netAmount = cartDetailsArray[0].Cart![0].NetTotal! + shippingMethodsArray[0].ShippingCost
                    netAmount = cartDetailsArray[0].Cart![0].ItemTotal! + deliveryCharge
                    if netAmount >= shippingMethodsArray[0].FreeDeliveryAmount{
                        bottomFreeDeliveryMsgLblHeight.constant = 0
                        bottomViewHeight.constant = 80
                    }else{
                        bottomFreeDeliveryMsgLblHeight.constant = 21
                        bottomViewHeight.constant = 101
                        bottomView.clipsToBounds = true
                    }
                }else{
                    for i in 0...self.shippingMethodsArray.count - 1{
                        if self.shippingMethodsArray[i].Id == self.firstSelectShipId{
                            self.shippingMethodsArray[i].isSelect = true
                            var vatPercentage = (Double(cartDetailsArray[0].Cart![0].VatPercentage!)/100.0 + 1.0)
                            vatPercentage = Double(shippingMethodsArray[i].ShippingCost) / vatPercentage
                            vatCharge = Double(vatPercentage - shippingMethodsArray[i].ShippingCost) * -1
                            deliveryCharge = shippingMethodsArray[i].ShippingCost - vatCharge
                            bottomFreeDeliveryMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add items worth", value: "", table: nil))!) \(shippingMethodsArray[i].FreeDeliveryAmount) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "more for free delivery", value: "", table: nil))!)"
                            netAmount = cartDetailsArray[0].Cart![0].ItemTotal! + deliveryCharge
                            if netAmount >= shippingMethodsArray[0].FreeDeliveryAmount{
                                bottomFreeDeliveryMsgLblHeight.constant = 0
                                bottomViewHeight.constant = 80
                            }else{
                                bottomFreeDeliveryMsgLblHeight.constant = 21
                                bottomViewHeight.constant = 101
                                bottomView.clipsToBounds = true
                            }
                        }else{
                            self.shippingMethodsArray[i].isSelect = false
                        }
                    }
//                    bottomFreeDeliveryMsgLblHeight.constant = 0
//                    bottomViewHeight.constant = 80
                }
            }else{
                shippingMethodMainViewBottom.constant = 0
                shippingMethodMainViewHeight.constant = 0
                bottomFreeDeliveryMsgLblHeight.constant = 0
                bottomViewHeight.constant = 80
            }
        }else{
            shippingMethodMainViewBottom.constant = 0
            shippingMethodMainViewHeight.constant = 0
            bottomFreeDeliveryMsgLblHeight.constant = 0
            bottomViewHeight.constant = 80
        }
    }
    func PromotionDetailsGetService(){
        let dic:[String : Any] = ["userId":UserDef.getUserId(), "RequestBy":2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("PomotionDetailsService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        CartModuleServices.PomotionDetailsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                //DispatchQueue.main.sync {
                    if data.Data != nil{
                        if data.Data!.count > 0{
                            SaveAddressClass.promotionsArray = data.Data!
                            for i in 0...SaveAddressClass.promotionsArray.count - 1{
                                SaveAddressClass.promotionsArray[i].isSelect = false
                                SaveAddressClass.promotionsArray[i].quantity = 3
                                if self.cartDetailsArray.count > 0{
                                    if self.cartDetailsArray[0].Cart![0].PromoCode != nil{
                                        if self.cartDetailsArray[0].Cart![0].PromoCode! == SaveAddressClass.promotionsArray[i].PromoCode{
                                            SaveAddressClass.promotionsArray[i].isSelect = true
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        SaveAddressClass.promotionsArray.removeAll()
                    }
                    //Promocode
                    if self.cartDetailsArray[0].Cart![0].PromoCode != nil && self.cartDetailsArray[0].Cart![0].PromoCode! != ""{
                        self.promocodeLbl.isHidden = true
                        self.promocodeNameLbl.isHidden = true
                        self.applyPromocodeLbl.isHidden = false
                        self.applyPromocodeLbl.text = self.cartDetailsArray[0].Cart![0].PromoCode!
                       // self.promocodeNameLbl.text = self.cartDetailsArray[0].Cart![0].PromoCode!
                        self.promocodeDropDownBtn.setImage(UIImage(named: "promo_cancel"), for: .normal)
                    }else{
                        self.promocodeLbl.isHidden = false
                        self.promocodeNameLbl.isHidden = false
                        self.applyPromocodeLbl.isHidden = true
                        if data.Data != nil{
                            if data.Data!.count > 0{
                                self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have", value: "", table: nil))!) \(SaveAddressClass.promotionsArray.count) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupons", value: "", table: nil))!)"
                            }else{
                                self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!)"
                            }
                        }else{
                            self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!)"
                        }
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            self.promocodeDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                        }else{
                            self.promocodeDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                        }
                    }
                //}
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getCarInformationDetails(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId": currentStoreId]
        CartModuleServices.CarInformationService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.pickupTypeCollectionViewHeight.constant = 0//50
            }else{
                self.carDetailsArray = data.Data!
                SaveAddressClass.carInformationArray = [data.Data!]
                //if self.cartDetailsArray[0].Cart![0].OrderType == "3"{
                    if let _ = data.Data!.SubOrderTypes{
                        if data.Data!.SubOrderTypes!.count > 0{
                            self.pickupSelectId = self.carDetailsArray.SubOrderTypes![0].Id
                            self.pickupTypeCollectionView.reloadData()
                            self.orderTypeLblHeight.constant = 0
                            self.orderTypeLblTop.constant = 0
                            self.changeAddressViewHeight.constant = 51
                            self.pickupTypeCollectionViewHeight.constant = 50
                            self.pickupTypeCollectionView.reloadData()
                        }else{
                            self.orderTypeLblHeight.constant = 21
                            self.orderTypeLblTop.constant = 8
                            self.changeAddressViewHeight.constant = 80
                            self.pickupTypeCollectionViewHeight.constant = 0
                        }
                    }else{
                        self.orderTypeLblHeight.constant = 21
                        self.orderTypeLblTop.constant = 8
                        self.changeAddressViewHeight.constant = 80
                        self.pickupTypeCollectionViewHeight.constant = 0
                    }
//                }else{
//                    self.pickupTypeCollectionViewHeight.constant = 0
//                }
            }
        }) { (error) in
            print(error)
        }
    }
    func AllData(){
        if cartDetailsArray[0].Cart![0].IsSubscription != nil{
            if cartDetailsArray[0].Cart![0].IsSubscription! == true{
                deliveryFrequencyMainViewHeight.constant = 75
                deliveryFrequencyMainViewBottom.constant = 15
            }else{
                deliveryFrequencyMainViewHeight.constant = 0
                deliveryFrequencyMainViewBottom.constant = 0
            }
        }else{
            deliveryFrequencyMainViewHeight.constant = 0
            deliveryFrequencyMainViewBottom.constant = 0
        }
        if cartDetailsArray[0].Cart![0].CartItemsQuantity != 0{
            myScrollView.isHidden = false
            self.bottomView.isHidden = false
        }else{
            myScrollView.isHidden = true
            self.bottomView.isHidden = true
        }
        bottomView.isHidden = false
        //middleLineLbl.isHidden = false
        confirmOrderBtn.isHidden = false
        bottomNetTotalLbl.isHidden = false
        bottomNetTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Net Total", value: "", table: nil))!
        changeDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Date", value: "", table: nil))!
        self.orderTypeLblHeight.constant = 21
        self.orderTypeLblTop.constant = 8
        if cartDetailsArray[0].Cart![0].OrderType == "1"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DineIn", value: "", table: nil))!
            //estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Date & time", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DineIn From:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
        }else if cartDetailsArray[0].Cart![0].OrderType == "2"{
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DriveThru", value: "", table: nil))!
            //estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Date & time", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DriveThru From:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
        }else if cartDetailsArray[0].Cart![0].OrderType == "3"{
            //estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Pickup Date & time", value: "", table: nil))!
            self.orderTypeLblHeight.constant = 0
            self.orderTypeLblTop.constant = 0
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pickup From:", value: "", table: nil))!
            if AppDelegate.getDelegate().appLanguage == "English"{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressEn!
            }else{
                addressLbl.text = cartDetailsArray[0].Stores![0].AddressAr!
            }
            changeAddressBtn.isEnabled = false
            changeAddressViewWidth.constant = 0
            changeAddressViewTrailing.constant = 0
            //inStoreViewHeight.constant = 50
            self.getCarInformationDetails()
        }else if cartDetailsArray[0].Cart![0].OrderType == "4"{
            //estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Delivery Date & time", value: "", table: nil))!
            orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
            changeAddressBtn.isEnabled = true
            changeAddressViewWidth.constant = 80
            changeAddressViewTrailing.constant = 10
            orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivering to:", value: "", table: nil))!
            if let address = cartDetailsArray[0].Address![0].Address {
                addressLbl.text = address
            }else{
                addressLbl.text = ""
                confirmOrderBtn.isHidden = true
                bottomNetTotalLbl.isHidden = true
                bottomNetTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select delivery address", value: "", table: nil))!
            }
        }
        quantity = 0
        for i in 0...cartDetailsArray[0].Items!.count-1{
            quantity = quantity + cartDetailsArray[0].Items![i].Quantity!
        }
        AppDelegate.getDelegate().cartQuantity = quantity
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 {
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        
        if cartDetailsArray[0].Cart![0].IsSubscription! == true{
            self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
        }else{
            if cartDetailsArray[0].Cart![0].ConsumedPoints! != 0{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Discount", value: "", table: nil))!) :"
            }else if cartDetailsArray[0].Cart![0].PromoCode! != ""{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!) :"
            }else{
                self.couponDiscountNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!) :"
            }
        }
        
        AppDelegate.getDelegate().cartAmount = cartDetailsArray[0].Cart![0].NetTotal!
        
        if cartDetailsArray[0].Cart![0].AvailableWalletAmt != nil{
            walletAvailableBalLbl.text = "( \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available credit", value: "", table: nil))!) \(cartDetailsArray[0].Cart![0].AvailableWalletAmt!.withCommas()) )"
        }else{
            walletAvailableBalLbl.text = "( \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available credit", value: "", table: nil))!) 0.00 )"
        }
        
        if cartDetailsArray[0].Cart![0].SectionType! == 2{
            deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Shipping Charges", value: "", table: nil))!) :"
            //estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Date / Slot", value: "", table: nil))!
        }else{
            deliveryChargesNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charges", value: "", table: nil))!) :"
            deliveryCharge = cartDetailsArray[0].Cart![0].DeliveryCharge!
            vatCharge = 0.0
        }
        if cartDetailsArray[0].Cart![0].IsSubscription! == true{
            estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription Start Date / Slot", value: "", table: nil))!
        }else{
            if cartDetailsArray[0].Cart![0].SectionType! == 2{
                estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Date / Slot", value: "", table: nil))!
            }else{
                if cartDetailsArray[0].Cart![0].OrderType == "1" || cartDetailsArray[0].Cart![0].OrderType == "2"{
                    estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Date & time", value: "", table: nil))!
                }else if cartDetailsArray[0].Cart![0].OrderType == "3"{
                    estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Pickup Date & time", value: "", table: nil))!
                }else if cartDetailsArray[0].Cart![0].OrderType == "4"{
                    estimateDeliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Estimated Delivery Date & time", value: "", table: nil))!
                }
            }
        }
        
        //Expect Time Functionality
        if self.isChangeTime == false {
            if cartDetailsArray[0].Cart![0].OrderType == "4"{
                if cartDetailsArray[0].Cart![0].SectionType! == 2{
                    if shippingMethodsArray.count == 1{
                        let shipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: ".")[0]
                        firstShipDate = shippingMethodsArray[0].ShippingTime.components(separatedBy: "T")[0]
                        firstSelectShipId = shippingMethodsArray[0].Id
                        var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
                        var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
                        let timeSlot = expectTimeSlot(presentDate: shipDate, index: 0)
                        if timeSlot == "NotMatch"{
                            let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: 0)
                            if firstTimeSlot == "NotMatch"{
                                for i in 1...shippingMethodsArray[0].FutureDays{
                                    nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                                    weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                                    let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                                    if time.count > 0{
                                        shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                        shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                        selectSlotId = time[0].Id
                                        if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
                                            shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                                        }
                                        print("Match")
                                        break
                                    }else{
                                        print("Not Match")
                                    }
                                }
                            }else{
                                print("Match")
                            }
                        }else{
                            print("Match")
                        }
                        self.estimateDeliveryTimeLbl.text = shipTimeSlot
                        self.estimateDeliveryDateLbl.text = shipTimeSlotDate
                    }else{
                        if firstSelectShipId != 0{
                            for i in 0...self.shippingMethodsArray.count - 1{
                                if self.shippingMethodsArray[i].Id == self.firstSelectShipId{
                                    let shipDate = shippingMethodsArray[i].ShippingTime.components(separatedBy: ".")[0]
                                    firstShipDate = shippingMethodsArray[i].ShippingTime.components(separatedBy: "T")[0]
                                    firstSelectShipId = shippingMethodsArray[i].Id
                                    var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
                                    var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
                                    let timeSlot = expectTimeSlot(presentDate: shipDate, index: 0)
                                    if timeSlot == "NotMatch"{
                                        let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: 0)
                                        if firstTimeSlot == "NotMatch"{
                                            for i in 1...shippingMethodsArray[i].FutureDays{
                                                nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                                                weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                                                let time = shippingMethodsArray[i].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                                                if time.count > 0{
                                                    shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                                    shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                                    selectSlotId = time[0].Id
                                                    if time[0].Id == shippingMethodsArray[i].ShippingSlots![weekDayNum-1].Slot![i].Id{
                                                        shippingMethodsArray[i].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                                                    }
                                                    print("Match")
                                                    break
                                                }else{
                                                    print("Not Match")
                                                }
                                            }
                                        }else{
                                            print("Match")
                                        }
                                    }else{
                                        print("Match")
                                    }
                                    self.estimateDeliveryTimeLbl.text = shipTimeSlot
                                    self.estimateDeliveryDateLbl.text = shipTimeSlotDate
                                }
                            }
                        }else{
                            self.estimateDeliveryTimeLbl.text = ""
                            self.estimateDeliveryDateLbl.text = ""
                            //deliveryChargeslbl.text = "0.00 SAR"
                            deliveryCharge = 0.00
                        }
                    }
//                    if let _ = cartDetailsArray[0].Address!.Address {
//                        changeAddressBtn.isHidden = false
//                    }else{
//                        changeAddressBtn.isHidden = true
//                    }
                }else{
                    if let _ = cartDetailsArray[0].Address![0].Address {
                        DispatchQueue.main.async {
                            if self.travelDuration == 0{
                                self.getEstimationTime()
                            }else{
                                self.ExpetectedTimeCalculatMethod()
                            }
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    if self.travelDuration == 0{
                        self.getEstimationTime()
                    }else{
                        self.ExpetectedTimeCalculatMethod()
                    }
                }
            }
        }
        
        //Payment Methods Display
        if paymentDetailsArray.count > 0{
            paymentDetailsViewHeight.constant = CGFloat(60 * paymentDetailsArray.count + 50)
            paymentDetailsTableViewHeight.constant = CGFloat(60 * paymentDetailsArray.count)
        }else{
            paymentDetailsViewHeight.constant = CGFloat(60 * paymentNameArray.count + 50)
            paymentDetailsTableViewHeight.constant = CGFloat(60 * paymentNameArray.count)
        }
        
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            paymentMethodDropBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
        }else{
            paymentMethodDropBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
        }
        paymentDetailsTableView.reloadData()
        
        //Prices Display
        if cartDetailsArray[0].Cart![0].OrderType == "4"{
            deliveryChargesNameLblHeight.constant = 21
            deliveryChargesNameLblBottom.constant = 10
            priceDetailsViewHeight.constant = 240
        }else{
            deliveryChargesNameLblHeight.constant = 0
            deliveryChargesNameLblBottom.constant = 0
            priceDetailsViewHeight.constant = 210
        }
        netAmountViewHeight.constant = 43
        if AppDelegate.getDelegate().appLanguage == "English"{
            billAmountDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
        }else{
            billAmountDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
        }
        
        // Vat
        vatNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(cartDetailsArray[0].Cart![0].VatPercentage!)%) :"
        priceCalculation()
        
        //Wallet Details
        if cartDetailsArray[0].Cart![0].AvailableWalletAmt != nil{
            if cartDetailsArray[0].Cart![0].AvailableWalletAmt! > 0{
                walletCheckedBtn.setImage(UIImage(named: "Box Selected"), for: .normal)
                walletCheckedBtn_Tapped(walletCheckedBtn)
            }else{
                walletCheckedBtn.setImage(UIImage(named: "Box Un Select"), for: .normal)
            }
        }
        
        //self.loadTimer.invalidate()
        //self.startLoadCartTimer()
        if cartDetailsArray[0].Cart![0].OrderType != "4"{
            SwiftTimer.invalidate()
            startTimer()
            manualTimer.invalidate()
            startManualTimer()
        }
        
        //Frequency
        if cartDetailsArray[0].Cart![0].IsSubscription != nil{
            if cartDetailsArray[0].Cart![0].IsSubscription! == true{
                if cartDetailsArray[0].Cart![0].FrequencyId != nil{
                    if cartDetailsArray[0].Cart![0].FrequencyId! == 0{
                        frequencyLbl.text = ""
                    }else{
                        if cartDetailsArray[0].Frequency != nil{
                            if cartDetailsArray[0].Frequency!.count > 0{
                                let frequency = cartDetailsArray[0].Frequency!.filter({$0.Id == cartDetailsArray[0].Cart![0].FrequencyId!})
                                if frequency.count > 0{
                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        frequencyLbl.text = frequency[0].NameEn
                                    }else{
                                        frequencyLbl.text = frequency[0].NameAr
                                    }
                                }else{
                                    frequencyLbl.text = ""
                                }
                            }
                        }
                    }
                }else{
                    frequencyLbl.text = ""
                }
            }
        }
        
        if cartDetailsArray[0].Cart![0].AvailableWalletAmt! > 0{
            walletCheckedBtn.isEnabled = true
        }else{
            walletCheckedBtn.isEnabled = false
        }
        if cartDetailsArray[0].Cart![0].ConsumedPoints != nil{
            if cartDetailsArray[0].Cart![0].ConsumedPoints! > 0{
                if cartDetailsArray[0].Cart![0].NetTotal! == 0{
                    let cashDetails = self.paymentDetailsArray.filter({$0.Id! == 2})
                    if cashDetails.count > 0{
                        for i in 0...paymentDetailsArray.count-1{
                            paymentDetailsArray[i].isSelect! = false
                            if paymentDetailsArray[i].Id! == 2{
                                paymentDetailsArray[i].isSelect! = true
                            }
                        }
                        paymentDetailsTableView.reloadData()
                    }else{
                        for i in 0...paymentDetailsArray.count-1{
                            paymentDetailsArray[i].isSelect! = false
                        }
                        paymentDetailsTableView.reloadData()
                    }
                    paymentDetailsTableView.isUserInteractionEnabled = false
                    paymentSelectId = 2
                    walletCheckedBtn.isEnabled = false
                }else{
                    paymentDetailsTableView.isUserInteractionEnabled = true
                    if cartDetailsArray[0].Cart![0].AvailableWalletAmt! > 0{
                        walletCheckedBtn.isEnabled = true
                    }else{
                        walletCheckedBtn.isEnabled = false
                    }
                }
            }
        }
    }
    func priceCalculation(){
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("PriceCalculation", forKey: "Method")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        /* End Crashlytics Logs */
        vatAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \((vatCharge + cartDetailsArray[0].Cart![0].Vatcharges!).withCommas())"
        deliveryChargeslbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(deliveryCharge.withCommas())"
        //Price Details
        if cartDetailsArray[0].Cart![0].ItemTotal != nil{
            totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(cartDetailsArray[0].Cart![0].ItemTotal!.withCommas())"
        }else{
            totalAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        }
        if cartDetailsArray[0].Cart![0].Discount != nil{
            if cartDetailsArray[0].Cart![0].Discount! == 0 || cartDetailsArray[0].Cart![0].Discount! == 0.0{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(cartDetailsArray[0].Cart![0].Discount!.withCommas())"
            }else{
                couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(cartDetailsArray[0].Cart![0].Discount!.withCommas())"
            }
        }else{
            couponDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        }
        discountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
        
        
        if isSubscription == true{
            let shipMethod = shippingMethodsArray.filter({$0.isSelect == true})
            if shipMethod.count == 0{
                netAmount = cartDetailsArray[0].Cart![0].NetTotal!
            }else{//Swathi
                netAmount = (netAmount + vatCharge + cartDetailsArray[0].Cart![0].Vatcharges!) - cartDetailsArray[0].Cart![0].Discount!
            }
            print(cartDetailsArray[0].Cart![0].NetTotal!)
            print(netAmount)
            netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
            bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
        }else{
            if cartDetailsArray[0].Cart![0].NetTotal != nil{
                if cartDetailsArray[0].Cart![0].SectionType! == 2{
                    let shipMethod = shippingMethodsArray.filter({$0.isSelect == true})
                    if shipMethod.count == 0{
                        netAmount = cartDetailsArray[0].Cart![0].NetTotal!
                    }else{//Swathi
                        netAmount = (netAmount + vatCharge + cartDetailsArray[0].Cart![0].Vatcharges!) - cartDetailsArray[0].Cart![0].Discount!
                    }
                    netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
                    bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
                }else{
                    netAmount = cartDetailsArray[0].Cart![0].NetTotal!
                    netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
                    bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
                }
                //netAmount = cartDetailsArray[0].Cart!.NetTotal!
            }else{
                netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
                bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) 0.00"
            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        CartVC.instance.back = true
        if isSubscription == true{
            CartVC.instance.isRemove = true
        }
        CartVC.instance.ConfirmOrderId = orderId
        navigationController?.popViewController(animated: true)
    }    //MARK: ChangeAddress Button Action
    @IBAction func changeAddressBtn_Tapped(_ sender: Any) {
        self.isSelectAddress = true
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ManageAddressVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
        obj.storeId = 190
        obj.whereObj = 123
        obj.sectionType = 4
        obj.orderId = cartDetailsArray[0].Cart![0].Id!
        obj.manageAddressVCDelegate = self
        obj.isConfirmation = true
        self.navigationController?.pushViewController(obj, animated: false)
    }
    @IBAction func shippingMethodDropBtn_Tapped(_ sender: Any) {
        if shippingMethodMainViewHeight.constant == 45{
            shippingMethodMainViewHeight.constant = CGFloat(50 + (50 * shippingMethodsArray.count))
            shippingMethodTableViewHeight.constant = CGFloat(50 * shippingMethodsArray.count)
            shippingMethodLineLbl.isHidden = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                shippingMethodDropBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                shippingMethodDropBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            shippingMethodMainViewHeight.constant = 45
            shippingMethodTableViewHeight.constant = 0
            shippingMethodLineLbl.isHidden = true
            shippingMethodDropBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    @IBAction func frequencyChangeBtn_Tapped(_ sender: Any) {
        CartVC.instance.back = true
        CartVC.instance.ConfirmOrderId = orderId
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Wallet Button Action
    @IBAction func walletCheckedBtn_Tapped(_ sender: UIButton) {
        if walletCheckedBtn.currentImage == UIImage(named: "Box Selected"){
            walletCheckedBtn.setImage(UIImage(named: "Box Un Select"), for: .normal)
            walledLbl.text = "- 0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            if cartDetailsArray[0].Cart![0].NetTotal != nil{
                remainingNetAmountLbl.text = "\(cartDetailsArray[0].Cart![0].NetTotal!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            }else{
                remainingNetAmountLbl.text = "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            }
            if priceDetailsViewHeight.constant == 45{
                priceDetailsViewHeight.constant = 45
                netAmountViewHeight.constant = 0
            }else{
                if cartDetailsArray[0].Cart![0].OrderType == "4"{
                    priceDetailsViewHeight.constant = 240
                }else{
                    priceDetailsViewHeight.constant = 210
                }
                netAmountViewHeight.constant = 43
            }
            bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
            netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(netAmount.withCommas())"
            walletAmount = 0.0
            walletselect = 0
            walletCalcViewHeight.constant = 0
            walletCalcViewBottom.constant = 0
            dcPayNameLblHeight.constant = 0
            dcPayNameLblBottom.constant = 0
            dcPayLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - 0.00"
            paymentDetailsTableView.isUserInteractionEnabled = true
        }else{
            if cartDetailsArray[0].Cart![0].SectionType! == 2{
                let shipDetails = shippingMethodsArray.filter({$0.isSelect! == true})
                if shipDetails.count == 0{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                    return
                }
            }
            walletCheckedBtn.setImage(UIImage(named: "Box Selected"), for: .normal)
            var remainingAmt = 0.0
            if cartDetailsArray[0].Cart![0].AvailableWalletAmt != nil{
                if netAmount > cartDetailsArray[0].Cart![0].AvailableWalletAmt!{
                    walledLbl.text = "- \(cartDetailsArray[0].Cart![0].AvailableWalletAmt!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    dcPayLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(cartDetailsArray[0].Cart![0].AvailableWalletAmt!.withCommas())"
                    remainingAmt = netAmount - cartDetailsArray[0].Cart![0].AvailableWalletAmt!
                    walletAmount = cartDetailsArray[0].Cart![0].AvailableWalletAmt!
                }else{
                    walledLbl.text = "- \(netAmount.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    dcPayLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - \(netAmount.withCommas())"
                    remainingAmt = 0.0
                    walletAmount = netAmount
                }
                //walletAmount = cartDetailsArray[0].Cart!.AvailableWalletAmt!
            }else{
                walledLbl.text = "- 0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                dcPayLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) - 0.00"
                if cartDetailsArray[0].Cart![0].NetTotal != nil{
                    remainingAmt = netAmount
                }else{
                    remainingAmt = 0.0
                }
                walletAmount = 0
            }
            if priceDetailsViewHeight.constant == 45{
                priceDetailsViewHeight.constant = 45
                netAmountViewHeight.constant = 0
            }else{
                if cartDetailsArray[0].Cart![0].OrderType == "4"{
                    priceDetailsViewHeight.constant = 271
                }else{
                    priceDetailsViewHeight.constant = 241
                }
                netAmountViewHeight.constant = 43
            }
            walletselect = 1
            remainingNetAmountLbl.text = "\(remainingAmt.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            bottomNetTotalLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(remainingAmt.withCommas())"
            netAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(remainingAmt.withCommas())"
            //walletCalcViewHeight.constant = 80
            //walletCalcViewBottom.constant = 10
            walletCalcViewHeight.constant = 0
            walletCalcViewBottom.constant = 0
            dcPayNameLblHeight.constant = 21
            dcPayNameLblBottom.constant = 10
            
            if remainingAmt == 0.0{
                paymentDetailsTableView.isUserInteractionEnabled = false
                if paymentDetailsArray.count > 0{
                    for i in 0...paymentDetailsArray.count-1{
                        paymentDetailsArray[i].isSelect! = false
                    }
                    paymentDetailsTableView.reloadData()
                }
            }else{
                paymentDetailsTableView.isUserInteractionEnabled = true
            }
        }
    }
    //MARK: Map Button Action
    @IBAction func mapBtn_Tapped(_ sender: Any) {
    }
    //MARK: Change Time Button Action
    @IBAction func changeTimeBtn_Tapped(_ sender: UIButton) {
        if cartDetailsArray[0].Cart![0].OrderType == "4"{
            guard let _ = cartDetailsArray[0].Address![0].Address else {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select address", value: "", table: nil))!)
                return
            }
//            if let _ = cartDetailsArray[0].Address!.Address {
//            }else{
//                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:"Please select address")
//                return
//            }
        }
        if cartDetailsArray[0].Cart![0].SectionType! == 2{
            let shipDetails = shippingMethodsArray.filter({$0.isSelect! == true})
            if shipDetails.count > 0{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj = mainStoryBoard.instantiateViewController(withIdentifier: "TimeSlotVC") as! TimeSlotVC
                obj.modalPresentationStyle = .overCurrentContext
                obj.timeSlotVCDelegate = self
                obj.shipDetails = shipDetails
                obj.shipTimeSlotDate = shipTimeSlotDate
                obj.selectSlotId = selectSlotId
                obj.modalPresentationStyle = .overFullScreen
                self.present(obj, animated: true, completion: nil)
//                let timeExpect = shipDetails[0].ShippingTime!.components(separatedBy: ".")[0]
//                let obj : CalenderVC! = (self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC)
//                obj.modalPresentationStyle = .overCurrentContext
//                obj.modalTransitionStyle = .crossDissolve
//                obj.modalPresentationStyle = .popover
//                obj.preferredContentSize = CGSize(width: 300, height: 300)
//                let pVC = obj.popoverPresentationController
//                pVC?.permittedArrowDirections = .up
//                pVC?.sourceView = sender
//                pVC?.delegate = self
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    pVC?.sourceRect = CGRect(x: sender.frame.midX , y: -5, width: 0, height: sender.frame.size.height)
//                }else{
//                    pVC?.sourceRect = CGRect(x: sender.frame.midX - 25 , y: -5, width: 0, height: sender.frame.size.height)
//                }
//                obj.obj_where = 2
//                obj.maxDays = 30
//                obj.minimumDate = DateConverts.convertStringToDate(date: timeExpect, dateformatType: .dateTtime)
//                obj.calenderVCDelegate = self
//                self.present(obj, animated: false, completion: nil)
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                return
            }
        }else{
            self.datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
            self.datePicker.setDate(currentDate, animated: true)
            self.datePicker.setValue(UIColor.black, forKey: "textColor")
            var dateComponents = DateComponents()
            dateComponents.day = 1
            let maximumDays = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            self.datePicker.maximumDate = maximumDays
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            }
            let calendar = Calendar.current
            var date:Date!
            //date = calendar.date(byAdding: .minute, value: 0, to: expectTime!)
            var prepTime = 0.0
            for i in 0...cartDetailsArray[0].Items!.count - 1{
                prepTime = prepTime + (cartDetailsArray[0].Items![i].ServingTime! * Double(cartDetailsArray[0].Items![i].Quantity!))
            }
            let preperationTime = Int(round(Double(prepTime)))
            date = calendar.date(byAdding: .minute, value: Int(preperationTime), to: currentDate)
            self.datePicker.datePickerMode = .dateAndTime
            self.datePicker.minimumDate = date!
            //self.datePicker.locale = Locale(identifier: "en")
            let view = self.view.viewWithTag(104)
            let frame = self.view.convert(changeTimeBtn.frame, from:view)
            //let startPoint = CGPoint(x: frame.midX, y: frame.maxY)
            let startPoint = CGPoint(x: changeDateView.frame.origin.x + 40, y: 310 - myScrollView.contentOffset.y)
            self.changeTimeView.frame =  CGRect(x: 0, y: 0, width: 280, height: 280)
            popover.show(self.changeTimeView, point: startPoint)
        }
    }
    @IBAction func changeTimeCancelBtn_Tapped(_ sender: Any) {
        popover.dismiss()
    }
    @IBAction func changeTimeDoneBtn_Tapped(_ sender: Any) {
        isChangeTime = true
        self.datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: "EN")
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        timeFormatter.locale = Locale(identifier: "EN")
        self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:self.datePicker.date)
        self.estimateDeliveryDateLbl.text = dateFormatter.string(from:self.datePicker.date)
        changeTime = DateConverts.convertDateToDateFormate(date: self.datePicker.date, dateformatType: .dateTimeZ)
        popover.dismiss()
    }
    func ExpetectedTimeCalculatMethod() {
        //let preperationTime = cartDetailsArray[0].Items![0].ServingTime!
        var prepTime = 0.0
        for i in 0...cartDetailsArray[0].Items!.count - 1{
            prepTime = prepTime + (cartDetailsArray[0].Items![i].ServingTime! * Double(cartDetailsArray[0].Items![i].Quantity!))
        }
        let preperationTime = Int(round(Double(prepTime)))
        if cartDetailsArray[0].Cart![0].OrderType == "1" || cartDetailsArray[0].Cart![0].OrderType == "2" || cartDetailsArray[0].Cart![0].OrderType == "3"{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "hh:mm a"
            timeFormatter.locale = Locale(identifier: "EN")
            let calendar = Calendar.current
            expectTime = calendar.date(byAdding: .minute, value: 0, to: currentDate)
            if Int(preperationTime) >= travelDuration{
                expectTime = calendar.date(byAdding: .minute, value: Int(preperationTime), to: expectTime!)
            }else{
                expectTime = calendar.date(byAdding: .minute, value: travelDuration, to: expectTime!)
            }
            kitchenStartTime = calendar.date(byAdding: .minute, value: Int(-preperationTime), to: expectTime!)
            orderExpectTime = expectTime
            //self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
            //self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
            if isChangeTime == false{//Change Swathi 27/07/2022
                self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
                self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
            }else{
                if expectTime > changeTime{
                    self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
                    self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
                }
            }
            popover.dismiss()
        }else if cartDetailsArray[0].Cart![0].OrderType == "4"{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "hh:mm a"
            timeFormatter.locale = Locale(identifier: "EN")
            
            let calendar = Calendar.current
            let kitchenTime = Int(preperationTime) + travelDuration
            expectTime = calendar.date(byAdding: .minute, value: Int(preperationTime), to: currentDate)
            expectTime = calendar.date(byAdding: .minute, value: travelDuration, to: expectTime!)
            kitchenStartTime = calendar.date(byAdding: .minute, value: Int(-kitchenTime), to: expectTime!)
            orderExpectTime = expectTime
            //self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
            //self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
            if isChangeTime == false{//Change Swathi 27/07/2022
                self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
                self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
            }else{
                if expectTime > changeTime{
                    self.estimateDeliveryTimeLbl.text = timeFormatter.string(from:expectTime!)
                    self.estimateDeliveryDateLbl.text = dateFormatterGet.string(from:expectTime!)
                }
            }
            popover.dismiss()
        }
    }
    func getEstimationTime(){
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("getEstimationTime", forKey: "Method")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        /* End Crashlytics Logs */
        let dic = cartDetailsArray[0].Stores![0]
        var origin = ""
        var destination = ""
        if cartDetailsArray[0].Cart![0].OrderType == "1" || cartDetailsArray[0].Cart![0].OrderType == "2" || cartDetailsArray[0].Cart![0].OrderType == "3"{
            if !self.hasLocationPermission() {
                if AppDelegate.getDelegate().currentlocation != nil{
                    origin = "\(AppDelegate.getDelegate().currentlocation.coordinate.latitude),\(AppDelegate.getDelegate().currentlocation.coordinate.longitude)"
                }else{
                    print("Not There")
                }
            }else{
                origin = "\(latitude),\(longitude)"
            }
            //origin = "\(latitude),\(longitude)"
            destination = "\(dic.Latitude!),\(dic.Longitude!)"
        }else if cartDetailsArray[0].Cart![0].OrderType == "4"{
            origin = "\(dic.Latitude!),\(dic.Longitude!)"
            destination = "\(cartDetailsArray[0].Address![0].Latitude!),\(cartDetailsArray[0].Address![0].Longitude!)"
        }
        CartModuleServices.LocationGetService(url: "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM", success: { (Data) in
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["rows"] as! [NSDictionary]
            
            for route in data{
                let elements = route["elements"] as! [NSDictionary]
                for durationIn in elements{
                    if durationIn["duration_in_traffic"] != nil {
                        let duration = durationIn["duration_in_traffic"] as! NSDictionary
                        var travelTime:Double = duration["value"] as! Double
                        travelTime =  travelTime/60
                        self.travelDuration = Int(round(Double(travelTime)))
                    }else{
                        self.travelDuration = 15
                    }
                }
            }
            
            //Swathi
//            if !self.hasLocationPermission() {
//                if self.cartDetailsArray[0].Cart![0].OrderType == "1" || self.cartDetailsArray[0].Cart![0].OrderType == "3"{
//                    self.travelDuration = 15
//                }
//            }
            
            if !self.hasLocationPermission() {
                if AppDelegate.getDelegate().currentlocation == nil{
                    self.travelDuration = 15
                }
            }
            self.ExpetectedTimeCalculatMethod()
        }) { (error) in
            //print(error)
            self.travelDuration = 15
            self.ExpetectedTimeCalculatMethod()
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
    }
    /*//MARK: Edit Order Button Action
    @IBAction func editOrderBtn_Tapped(_ sender: Any) {
        if editOrderBtn.titleLabel!.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
                obj.storeId = 190
                obj.whereObj = 123
                obj.sectionType = 4
                obj.orderId = cartDetailsArray[0].Cart![0].Id!
            self.navigationController?.pushViewController(obj, animated: false)
            return
        }
       // if whereObj == 1 || whereObj == 2 || whereObj == 6{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = MenuVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            if cartDetailsArray[0].Cart![0].OrderType! == "4"{
                obj.whereObj = 1
                obj.addressDetailsArray = [AddressDetailsModel(Id: cartDetailsArray[0].Address![0].Id!, UserId: Int(UserDef.getUserId())!, HouseNo: "", HouseName: cartDetailsArray[0].Address![0].Name!, LandMark: cartDetailsArray[0].Address![0].Landmark!, Address: cartDetailsArray[0].Address![0].Address!, AddressType: cartDetailsArray[0].Address![0].TypeId!, CountryCode: "", Default: 0, Latitude: cartDetailsArray[0].Address![0].Latitude!, Longitude: cartDetailsArray[0].Address![0].Longitude!, ContactPerson: cartDetailsArray[0].Address![0].ContactPerson!, ContactNo: cartDetailsArray[0].Address![0].ContactPersonPhone!, DeliveryInfo: cartDetailsArray[0].Address![0].DeliveryInfo!)]
                obj.selectOrderType = 4
            }else{
                obj.address = cartDetailsArray[0].Stores![0].NameEn!
                obj.storeID = cartDetailsArray[0].Stores![0].Id!
                obj.selectOrderType = Int(cartDetailsArray[0].Cart![0].OrderType!)!
            }
            self.navigationController?.pushViewController(obj, animated: true)
//        }else if whereObj == 3{
//            self.navigationController?.popViewController(animated: false)
//        }else if whereObj == 4{
//            ItemsVC.instance.backWhereObj = 1
//            self.navigationController?.popViewController(animated: false)
//        }else if whereObj == 5{
//            BeverageVC.instance.backWhereObj = 1
//            self.navigationController?.popViewController(animated: false)
//        }else{
//            self.navigationController?.popViewController(animated: false)
//        }
    }*/
    //MARK: Confirm Button Action
    @IBAction func ConfirmBtn_Tapped(_ sender: Any) {
        if self.estimateDeliveryDateLbl.text! == "" || self.estimateDeliveryTimeLbl.text! == "" || self.estimateDeliveryDateLbl.text! == "-" || self.estimateDeliveryTimeLbl.text! == "-"{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Expect Time", value: "", table: nil))!)
            return
        }
        if self.pickupTypeCollectionViewHeight.constant != 0 {
            if cartDetailsArray[0].Cart![0].OrderType == "3"{
            if self.pickupSelectId == nil{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select Pickup Type", value: "", table: nil))!)
                return
            }
            if self.pickupSelectId == 15{
                if selectCarDicArray.count == 0{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select Car Details", value: "", table: nil))!)
                    return
                }
            }
           }
        }
        
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("ConfirmBtn_Tapped", forKey: "Method")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        /* End Crashlytics Logs */
        if isUserLogIn() == true{
            if promocodeDropDownBtn.currentImage == UIImage(named: "promo_cancel"){
                if cartDetailsArray[0].Cart![0].NetTotal != 0{
                    ConfirmOrderValidation()
                }else{
                    ANLoader.showLoading("", disableUI: true)
                    paymentSelectId = 2
                    self.confirmOrder()
                }
            }else{
                ConfirmOrderValidation()
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    func ConfirmOrderValidation(){
        if paymentSelectId != 0{
            if cartDetailsArray[0].Stores![0].StoreStatus! == false{//Store is Closed}
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
            }else{//Store is Open
                if walletselect == 0 || walletselect == 1 && remainingNetAmountLbl.text! != "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                    if cartDetailsArray[0].Stores![0].is24x7! == true{
                        if cartDetailsArray[0].Stores![0].OrderTypeBusy! == true{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(cartDetailsArray[0].Stores![0].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)")
                            return
                        }else{
                            if cartDetailsArray[0].Cart![0].SectionType! == 2 && self.isChangeTime == false{
                                let shipDetails = shippingMethodsArray.filter({$0.isSelect! == true})
                                if shipDetails.count > 0{
                                    //self.getDetailsService()
                                    ANLoader.showLoading("", disableUI: true)
                                    self.confirmOrder()
                                }else{
                                    DispatchQueue.main.async {
                                        ANLoader.hide()
                                    }
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                                    return
                                }
                            }else{
                                ANLoader.showLoading("", disableUI: true)
                                self.confirmOrder()
                            }
                        }
                    }else{
                        if cartDetailsArray[0].Stores![0].isShiftOpen! == true{
                            if cartDetailsArray[0].Stores![0].OrderTypeBusy! == true{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(cartDetailsArray[0].Stores![0].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)")
                                return
                            }else{
                                if cartDetailsArray[0].Cart![0].SectionType! == 2 && self.isChangeTime == false{
                                    let shipDetails = shippingMethodsArray.filter({$0.isSelect! == true})
                                    if shipDetails.count > 0{
                                        //self.getDetailsService()
                                        ANLoader.showLoading("", disableUI: true)
                                        self.confirmOrder()
                                    }else{
                                        DispatchQueue.main.async {
                                            ANLoader.hide()
                                        }
                                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                                        return
                                    }
                                }else{
                                    ANLoader.showLoading("", disableUI: true)
                                    self.confirmOrder()
                                }
                            }
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
                        }
                    }
                }else{
                    paymentSelectId = 1
                    ANLoader.showLoading("", disableUI: true)
                    self.confirmOrder()
                }
            }
        }else{
            if cartDetailsArray[0].Cart![0].SectionType! == 2 && self.isChangeTime == false{
                let shipDetails = shippingMethodsArray.filter({$0.isSelect! == true})
                if shipDetails.count > 0{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    if walletselect == 0 || walletselect == 1 && remainingNetAmountLbl.text! != "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Payment Method", value: "", table: nil))!)
                        return
                    }else{
                        paymentSelectId = 1
                        //self.getDetailsService()
                        ANLoader.showLoading("", disableUI: true)
                        self.confirmOrder()
                    }
                }else{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                    return
                }
            }else{
                if walletselect == 0 || walletselect == 1 && remainingNetAmountLbl.text! != "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Payment Method", value: "", table: nil))!)
                    return
                }else{
                    paymentSelectId = 1
                    ANLoader.showLoading("", disableUI: true)
                    self.confirmOrder()
                }
            }
        }
    }
    func confirmOrder(){
        var shippingMethodId = 0
        var costId = 0
       // var shippingcost = 0.0
        var estimateDateTime = ""
        var kitchenTime = ""
        if cartDetailsArray[0].Cart![0].SectionType! == 2{
            if shippingMethodsArray.count > 0{
                //let shipDetails = cartDetailsArray[0].ShippingMethods!.filter({$0.isSelect! == true && $0.CostId != nil})
                let shipDetails = self.shippingMethodsArray.filter({$0.Id == self.firstSelectShipId})
                if shipDetails.count > 0{
                    shippingMethodId = shipDetails[0].Id
                    if shipDetails[0].CostId != nil{
                        costId = shipDetails[0].CostId!
                    }else{
                        costId = 0
                    }
                   // shippingcost = shipDetails[0].ShippingCost
                    //if isChangeTime == true{
                        //estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: shipDetails[0].StartTime!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                        let time = estimateDeliveryTimeLbl.text!.components(separatedBy: " to ")
                        estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: time[0], inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                        orderExpectTime = DateConverts.convertStringToDate(date: estimateDateTime, dateformatType: .dateTimeSSS)
                    let kitchen = Calendar.current.date(byAdding: .hour, value: -shipDetails[0].MaxDeliveryHours, to: DateConverts.convertStringToDate(date: estimateDateTime, dateformatType: .dateTimeSSS))
                        kitchenTime = DateConverts.convertDateToString(date: kitchen!, dateformatType: .dateTimeSSS)
                    kitchenStartTime = Calendar.current.date(byAdding: .hour, value: -shipDetails[0].MaxDeliveryHours, to: DateConverts.convertStringToDate(date: estimateDateTime, dateformatType: .dateTimeSSS))
//                    }else{
//                        estimateDateTime = DateConverts.convertStringToStringDates(inputDateStr: shipDetails[0].ShippingTime!, inputDateformatType: .dateTtimeSSS, outputDateformatType: .dateTimeSSS)
//                        let kitchen = Calendar.current.date(byAdding: .hour, value: -shipDetails[0].MaxDeliveryHours!, to: DateConverts.convertStringToDate(date: shipDetails[0].ShippingTime!, dateformatType: .dateTtimeSSS))
//                        kitchenTime = DateConverts.convertDateToString(date: kitchen!, dateformatType: .dateTimeSSS)
//                    }
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                }
            }else{
                shippingMethodId = 0
                costId = 0
                //shippingcost = 0
            }
        }else{
            estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryTimeLbl.text!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
            kitchenTime = DateConverts.convertDateToString(date: kitchenStartTime!, dateformatType: .dateTimeSSS)
            shippingMethodId = 0
            costId = 0
            //shippingcost = 0
        }
        var expectTime = ""
        if cartDetailsArray[0].Cart![0].SectionType! == 2{
            let time = estimateDeliveryTimeLbl.text!.components(separatedBy: " to ")
            expectTime = "\(time[0]) - \(time[1])"
            //print(expectTime)
        }
        var toatalNetAmount = netAmount
        if walletselect == 1{
            if paymentSelectId == 1 && remainingNetAmountLbl.text! == "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                toatalNetAmount = 0.0
            }else{
                if remainingNetAmountLbl.text! != "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                    toatalNetAmount = netAmount - cartDetailsArray[0].Cart![0].AvailableWalletAmt!
                }else if remainingNetAmountLbl.text! == "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"{
                    toatalNetAmount = 0.0
                }else{
                    toatalNetAmount = netAmount
                }
            }
        }else{
            toatalNetAmount = netAmount
        }
        var orderStatusId = 1
        if paymentSelectId == 1 && remainingNetAmountLbl.text! == "0.00 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)" || paymentSelectId == 2{
            orderStatusId = 2
        }else{
            orderStatusId = 1
        }
        var slotId = ""
        if selectSlotId == 0{
            slotId = ""
        }else{
            slotId = "\(selectSlotId)"
        }
        let vat = cartDetailsArray[0].Cart![0].Vatcharges! + vatCharge
        
        var language = "En"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }else{
            language = "Ar"
        }
        var isSubScription = false
        if cartDetailsArray[0].Cart![0].IsSubscription != nil{
            if cartDetailsArray[0].Cart![0].IsSubscription! == true{
                isSubScription = true
            }
        }
        // Shift Business Date
        let ShiftBusinessDate = "\(cartDetailsArray[0].Stores![0].ShiftBusinessDate) 00:00:00.000"
        let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//        let deviceInfo:[String:Any] = [
//            "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//            "applang":language,
//            "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//            "SDKversion":"",
//            "Modelname":"\(UIDevice.current.name)",
//            "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//            "AppVersion":"IOS(\(appVersion))",
//            "TimeZone":"\(TimeZone.current.abbreviation()!)",
//            "TimeZoneRegion":"\(TimeZone.current.identifier)"
//        ]
        
        var deviceInfoStr = "\(getDeviceInfo())"
        deviceInfoStr = deviceInfoStr.replace(string: "[", replacement: "{")
        deviceInfoStr = deviceInfoStr.replace(string: "]", replacement: "}")
        var dic = ["userId": Int(UserDef.getUserId())!,
        "CartId": cartDetailsArray[0].Cart![0].Id!,
        "EstimatedDatetime":"\(estimateDateTime)",
        "KitchenStartTime":"\(kitchenTime)",
        "TravelTime":travelDuration,
        "IncludeWallet":walletselect,
        "WalletAmount":walletAmount,
        "PaymentType":paymentSelectId,
        "OrderStatusId":orderStatusId,
        "NetAmount":toatalNetAmount,
        "VatAmount":vat,
        "ShippingMethodId":shippingMethodId,
        "CostId":costId,
        "ShippingTime":expectTime,
        "Shippingcost":deliveryCharge,
        "SlotId":slotId,
        "RequestBy":2,
        "DeviceInfo":deviceInfoStr,
        "AppVersion":"IOS(\(appVersion))",
        "IsSubscription":isSubScription,
        "FrequencyId": cartDetailsArray[0].Cart![0].FrequencyId!,
        "ShiftBusinessDate":ShiftBusinessDate
        ] as [String : Any]
        
        if cartDetailsArray[0].Cart![0].OrderType == "3"{
            //Car Details
            dic["SubOrderTypeId"] = pickupSelectId
            var carInfo:[String:Any] = [:]
            if selectCarDicArray.count > 0{
                carInfo = ["CarId":selectCarDicArray[0].Id, "CarColorEn":"\(selectCarDicArray[0].ColorEn)", "CarColorAr":"\(selectCarDicArray[0].ColorAr)", "CarBrandEn":"\(selectCarDicArray[0].BrandEn)", "CarBrandAr":"\(selectCarDicArray[0].BrandAr)", "CarVehicleNo":"\(selectCarDicArray[0].VehicleNumber)"]
                dic["CarId"] = selectCarDicArray[0].Id
                var carInfoStr = "\(carInfo)"
                carInfoStr = carInfoStr.replace(string: "[", replacement: "{")
                carInfoStr = carInfoStr.replace(string: "]", replacement: "}")
                dic["CarJson"] = "\(carInfoStr)"
            }else{
                dic["CarId"] = ""
                dic["CarJson"] = ""
            }
        }
        //"OrderStatusId":self.paymentSelectId ==  2 ? 2:1,
        //print(dic)
        
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("ConfirmOrderService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        //Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        CartModuleServices.ConfirmOrderService(dic:dic,isLoader:false, isUIDisable:true, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if data.MessageEn == "Order has been placed already!"{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }else{
                        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }
            }else{
                if self.paymentSelectId == 2 || self.paymentSelectId == 1{
                    self.TrackOrderScreen()
                }else{
                    self.InvoiceNoStr = data.Data!.InvoiceNo!
                    self.getCheckoutId(amount: data.Data!.TotalAmount!)
                }
                self.trackOrderId = data.Data!.OrderId!
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func TrackOrderScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        AppDelegate.getDelegate().cartAmount = 0
        AppDelegate.getDelegate().cartStoreId = 0
        AppDelegate.getDelegate().cartQuantity = 0
        AppDelegate.getDelegate().cartStoreName = ""
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 {
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        DispatchQueue.main.async {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = TrackOrderVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
            obj.orderFrom = 1
            obj.orderId = self.trackOrderId
            self.navigationController?.pushViewController(obj, animated: false)
        }
    }
    //MARK: Promocode Button Action
    @IBAction func promocodeDropDownBtn_Tapped(_ sender: Any) {
        if promocodeDropDownBtn.currentImage == UIImage(named: "promo_cancel"){
            var estimateDateTime = ""
            var shippingMethodId = 0
            if cartDetailsArray[0].Cart![0].SectionType! == 2{
                if shippingMethodsArray.count > 0{
                    let shipDetails = self.shippingMethodsArray.filter({$0.Id == self.firstSelectShipId})
                    if shipDetails.count > 0{
                        let time = estimateDeliveryTimeLbl.text!.components(separatedBy: " to ")
                        estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: time[0], inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                        shippingMethodId = shipDetails[0].Id
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                        return
                    }
                }
            }else{
                if self.estimateDeliveryDateLbl.text! == "" || self.estimateDeliveryTimeLbl.text! == "" || self.estimateDeliveryDateLbl.text! == "-" || self.estimateDeliveryTimeLbl.text! == "-"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Expect Time", value: "", table: nil))!)
                    return
                }else{
                    estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryTimeLbl.text!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                }
            }
            var dic:[String : Any]!
            if selectSlotId != 0{
                dic = ["CartId":cartDetailsArray[0].Cart![0].Id!, "PromoCode":"\(applyPromocodeLbl.text!)", "action": 2, "EstimatedDateTime":"\(estimateDateTime)", "SlotId": "\(selectSlotId)", "shippingMethodId":"\(shippingMethodId)"] as [String : Any]
            }else{
                dic = ["CartId":cartDetailsArray[0].Cart![0].Id!, "PromoCode":"\(applyPromocodeLbl.text!)", "action": 2, "EstimatedDateTime":"\(estimateDateTime)", "SlotId": "", "shippingMethodId":""] as [String : Any]
            }
            
            CartModuleServices.PromotionAppliedService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                    }
                }else{
                    self.promocodeLbl.isHidden = false
                    self.promocodeNameLbl.isHidden = false
                    self.applyPromocodeLbl.isHidden = true
                    if SaveAddressClass.promotionsArray.count > 0{
                        self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have", value: "", table: nil))!) \(SaveAddressClass.promotionsArray.count) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupons", value: "", table: nil))!)"
                    }else{
                        self.promocodeNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!)"
                    }
                    self.promocodeDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
                    self.getDetailsService()
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = PromotionsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "PromotionsVC") as! PromotionsVC
            obj.promotionsVCDelegate = self
            obj.cartId = cartDetailsArray[0].Cart![0].Id!
            var estimateDateTime = ""
            if cartDetailsArray[0].Cart![0].SectionType! == 2{
                if shippingMethodsArray.count > 0{
                    let shipDetails = self.shippingMethodsArray.filter({$0.Id == self.firstSelectShipId})
                    if shipDetails.count > 0{
                        let time = estimateDeliveryTimeLbl.text!.components(separatedBy: " to ")
                        estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: time[0], inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                        obj.slotId = selectSlotId
                        obj.shippingMethodId = shipDetails[0].Id
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Shipping Method", value: "", table: nil))!)
                        return
                    }
                }
            }else{
                if self.estimateDeliveryDateLbl.text! == "" || self.estimateDeliveryTimeLbl.text! == "" || self.estimateDeliveryDateLbl.text! == "-" || self.estimateDeliveryTimeLbl.text! == "-"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Expect Time", value: "", table: nil))!)
                    return
                }else{
                    estimateDateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryTimeLbl.text!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                }
                obj.slotId = 0
                obj.shippingMethodId = 0
            }
            obj.estimateDate = estimateDateTime
            if SaveAddressClass.promotionsArray.count > 0{
                for i in 0...SaveAddressClass.promotionsArray.count - 1{
                    SaveAddressClass.promotionsArray[i].isSelect = false
                }
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Amount Details Drop Down Button Action
    @IBAction func billAmountDropDownBtn_Tapped(_ sender: Any) {
        if priceDetailsViewHeight.constant == 45{
            if walletCheckedBtn.currentImage == UIImage(named: "Box Selected"){
                if cartDetailsArray[0].Cart![0].OrderType == "4"{
                    priceDetailsViewHeight.constant = 271
                }else{
                    priceDetailsViewHeight.constant = 241
                }
            }else{
                if cartDetailsArray[0].Cart![0].OrderType == "4"{
                    priceDetailsViewHeight.constant = 240
                }else{
                    priceDetailsViewHeight.constant = 210
                }
            }
            netAmountViewHeight.constant = 43
            if AppDelegate.getDelegate().appLanguage == "English"{
                billAmountDropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                billAmountDropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
        }else{
            priceDetailsViewHeight.constant = 45
            netAmountViewHeight.constant = 0
            billAmountDropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
        }
    }
    //MARK: Payment Method Drop Down Button Action
    @IBAction func paymentMethodDropBtn_Tapped(_ sender: Any) {
        if paymentDetailsViewHeight.constant == 49{
            if paymentDetailsArray.count > 0{
                paymentDetailsViewHeight.constant = CGFloat(60 * paymentDetailsArray.count + 50)
                paymentDetailsTableViewHeight.constant = CGFloat(60 * paymentDetailsArray.count)
            }else{
                paymentDetailsViewHeight.constant = CGFloat(60 * paymentNameArray.count + 50)
                paymentDetailsTableViewHeight.constant = CGFloat(60 * paymentNameArray.count)
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                paymentMethodDropBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
            }else{
                paymentMethodDropBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
            }
            paymentDetailsTableView.reloadData()
        }else{
            paymentMethodDropBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
            paymentDetailsViewHeight.constant = 49
        }
    }
    func getCheckoutId(amount:Double) {
        //ANLoader.showLoading("Loading..", disableUI: true)
        let userDic = getUserDetails()
        //let userDetails = UserDef.getUserProfileDic(key: "userDetails")
       // let merchantTransactionId = "\(userDic.UserId)iOS\(self.TimeStamp())"
       // print(userDic)
        self.isOnlinePaymeny = true
        var customerPhone = userDic.Mobile
        if self.isSTCPay == true{
            customerPhone = stcPayNumber
        }
        let dic:[String:Any] = [
            "amount":String(format: "%.2f", amount),
            "shopperResultUrl": "\(self.urlScheme)://result" ,
            "isCardRegistration": "false",
            "merchantTransactionId": self.InvoiceNoStr,
            "customerEmail":userDic.Email,
            "userId":userDic.UserId,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "isMada":isMadaPay,
            "isVisaMaster":isVisa,
            "customerPhone":customerPhone,
            "customerName":userDic.FullName,
            "IsSubscription":self.isSubscription
            //"notificationUrl":"http://csadms.com/ChefAppAPITest/api/Notify/NewInfo"
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.getDetailsService()
                }))
            }else{
                DispatchQueue.main.async {
                   // print(data.Data![0].id)
                    self.checkoutID = data.Data![0].id
                    if self.isApplePay == true {
                        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
                        request.currencyCode = "SAR"
                        let amount = NSDecimalNumber(value: amount)
                        //print("ApplePay:\(amount)")
                        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
                        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
                            if let vc = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                            }else{
                                self.isOnlinePaymeny = false
                                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Unable to present Apple Pay authorization.", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                                    NSLog("Apple Pay not supported.");
                                    self.getDetailsService()
                                    
                                }))
                            }
                        }else{
                            
                        }
                    }else{
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                        self.checkoutProvider?.delegate = self
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: nil)
                    }
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    @objc func currentTimeGetService(){
        self.manualTimer.invalidate()
        let dic = ["RequestBy":2] as [String : Any]
        CartModuleServices.GetCurrentTimeService(dic: dic, success: { (data) in
            if(data.Status == false){
                let curDate = Calendar.current.date(byAdding: .minute, value: 1, to: self.currentDate)
                self.currentDate = curDate!
                if self.travelDuration == 0{
                    self.getEstimationTime()
                }else{
                    self.ExpetectedTimeCalculatMethod()
                }
                self.startManualTimer()
            }else{
               // print(data)
                DispatchQueue.main.async {
                    //if self.isChangeTime == false{
                        if data.Data.contains("."){
                            var time = data.Data
                            time = time.components(separatedBy: ".")[0]
                            self.currentDate = DateConverts.convertStringToDate(date: time, dateformatType: .dateTtime)
                        }else{
                            self.currentDate = DateConverts.convertStringToDate(date: data.Data, dateformatType: .dateTtime)
                        }
                    if self.travelDuration == 0{
                        self.getEstimationTime()
                    }else{
                        self.ExpetectedTimeCalculatMethod()
                    }
                    self.startManualTimer()
//                    }else{
//                        self.SwiftTimer.invalidate()
//                    }
                }
            }
        }) { (error) in
            let curDate = Calendar.current.date(byAdding: .minute, value: 1, to: self.currentDate)
            self.currentDate = curDate!
            if self.travelDuration == 0{
                self.getEstimationTime()
            }else{
                self.ExpetectedTimeCalculatMethod()
            }
            self.startManualTimer()
        }
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    //MARK: In Store Button Action
    @IBAction func inStoreBtn_Tapped(_ sender: Any) {
        inStoreView.backgroundColor = #colorLiteral(red: 0.7254901961, green: 0.3254901961, blue: 0.2588235294, alpha: 1)
        driveInView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        driveThruView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    }
    //MARK: Drive In Button Action
    @IBAction func driveInBtn_Tapped(_ sender: Any) {
        inStoreView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        driveInView.backgroundColor = #colorLiteral(red: 0.7254901961, green: 0.3254901961, blue: 0.2588235294, alpha: 1)
        driveThruView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarsListPopUpVC") as! CarsListPopUpVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
    }
    //MARK: Drive Thru Button Action
    @IBAction func driveThruBtn_Tapped(_ sender: Any) {
        inStoreView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        driveInView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        driveThruView.backgroundColor = #colorLiteral(red: 0.7254901961, green: 0.3254901961, blue: 0.2588235294, alpha: 1)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension ConfirmationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == shippingMethodTableView {
            return shippingMethodsArray.count
        }else{
            if cartDetailsArray.count > 0{
                if paymentDetailsArray.count > 0{
                    let paymentArray = paymentDetailsArray.filter({$0.IsActive! == true})
                    return paymentArray.count
                }else{
                    return paymentNameArray.count
                }
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == shippingMethodTableView {
            return 50
        }else{
            return 60
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == shippingMethodTableView {
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ShippingMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ShippingMethodTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ShippingMethodTVCell", value: "", table: nil))!, for: indexPath) as! ShippingMethodTVCell
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.methodNameLbl.text = shippingMethodsArray[indexPath.row].DisplayNameEn
            }else{
                cell.methodNameLbl.text = shippingMethodsArray[indexPath.row].DisplayNameAr
            }
            if shippingMethodsArray[indexPath.row].ShippingCost == 0.00 || shippingMethodsArray[indexPath.row].ShippingCost == 0.0 || shippingMethodsArray[indexPath.row].ShippingCost == 0{
                cell.methodCostLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Free", value: "", table: nil))!
            }else{
                cell.methodCostLbl.text = "\(shippingMethodsArray[indexPath.row].ShippingCost.withCommas())"
            }
            
            if shippingMethodsArray[indexPath.row].isSelect! == true{
                cell.methodSelectBtn.setImage(UIImage(named: "Oval Copy"), for: .normal)
            }else{
                cell.methodSelectBtn.setImage(UIImage(named: "Oval Copy 2"), for: .normal)
            }
            cell.methodSelectBtn.tag = indexPath.row
            cell.methodSelectBtn.addTarget(self, action: #selector(shippingMethodSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            shippingMethodTableViewHeight.constant = tableView.contentSize.height
            return cell
        }else{
            tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!)
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PaymentMethodTVCell", value: "", table: nil))!, for: indexPath) as! PaymentMethodTVCell
            
            if paymentDetailsArray.count > 0{
                if paymentDetailsArray[indexPath.row].IsActive! == true{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameEn!
                    }else{
                        cell.nameLbl.text = paymentDetailsArray[indexPath.row].NameAr!
                    }
                    
                    if paymentDetailsArray[indexPath.row].isSelect! == true{
                        cell.selectImg.image = UIImage(named: "Box Selected")
                    }else{
                        cell.selectImg.image = UIImage(named: "Box Un Select")
                    }
                    //Image
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(paymentDetailsArray[indexPath.row].Icon!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.img.kf.setImage(with: url)
                }
            }else{
                cell.img.image = UIImage(named: paymentImgsArray[indexPath.row])
                cell.nameLbl.text = paymentNameArray[indexPath.row]
                if paymentSelectIndex == indexPath.row{
                    cell.selectImg.image = UIImage(named: "Box Selected")
                }else{
                    cell.selectImg.image = UIImage(named: "Box Un Select")
                }
            }
            
            paymentDetailsTableView.layoutIfNeeded()
            paymentDetailsTableViewHeight.constant = tableView.contentSize.height
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == paymentDetailsTableView{
            paymentSelectIndex = indexPath.row
           // print(paymentDetailsArray as Any)
            if paymentDetailsArray.count > 0{
                for i in 0...paymentDetailsArray.count-1{
                    paymentDetailsArray[i].isSelect! = false
                }
                paymentDetailsArray[indexPath.row].isSelect! = true
                paymentSelectId = paymentDetailsArray[indexPath.row].Id!
                if paymentDetailsArray[indexPath.row].Id! == 2{
                    paymentSelectId = 2
                    isApplePay = false
                    isSTCPay = false
                    isMadaPay = false
                    isVisa = false
                }else if paymentDetailsArray[indexPath.row].Id! == 3{
                    paymentSelectId = 3
                    isApplePay = false
                    isSTCPay = false
                    isMadaPay = false
                    isVisa = true
                }else if paymentDetailsArray[indexPath.row].Id! == 4{
                    paymentSelectId = 4
                    isApplePay = false
                    isSTCPay = false
                    isMadaPay = true
                    isVisa = false
                }else if paymentDetailsArray[indexPath.row].Id! == 5{
                    paymentSelectId = 5
                    isApplePay = true
                    isSTCPay = false
                    isMadaPay = false
                    isVisa = false
                }else if paymentDetailsArray[indexPath.row].Id! == 6{
                    paymentSelectId = 6
                    isApplePay = false
                    isSTCPay = true
                    isMadaPay = false
                    isVisa = false
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "STCPayVC") as! STCPayVC
                    obj.modalPresentationStyle = .overCurrentContext
                    obj.modalTransitionStyle = .crossDissolve
                    obj.stcPayVCDelegate =  self
                    self.present(obj, animated: false, completion: nil)
                }
            }else{
                if indexPath.row == 0{
                    paymentSelectId = 5
                    isApplePay = true
                    isSTCPay = false
                    isVisa = false
                    isMadaPay = false

                }else if indexPath.row == 1{
                    paymentSelectId = 2
                    isApplePay = false
                    isSTCPay = false
                    isVisa = false
                    isMadaPay = false
                }
            }
//            if paymentDetailsArray != nil{
//
//            }else{
//                if indexPath.row == 0{
//                    paymentSelectId = 5
//                    isApplePay = true
//                    isSTCPay = false
//                    isMadaPay = false
//                    isVisa = false
//
//                }else if indexPath.row == 1{
//                    paymentSelectId = 2
//                    isApplePay = false
//                    isSTCPay = false
//                    isMadaPay = false
//                    isVisa = false
//
//
//                }
//                if indexPath.row == 0{
//                    paymentSelectId = 3
//                    isApplePay = false
//                    isSTCPay = false
//                }else if indexPath.row == 1{
//                    paymentSelectId = 5
//                    isApplePay = true
//                    isSTCPay = false
//                }else if indexPath.row == 2{
//                    paymentSelectId = 6
//                    isApplePay = false
//                    isSTCPay = true
//                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "STCPayVC") as! STCPayVC
//                    obj.modalPresentationStyle = .overCurrentContext
//                    obj.modalTransitionStyle = .crossDissolve
//                    obj.stcPayVCDelegate =  self
//                    self.present(obj, animated: false, completion: nil)
//                }else if indexPath.row == 3{
//                    paymentSelectId = 2
//                    isApplePay = false
//                    isSTCPay = false
//                }
            //}
            paymentDetailsTableView.reloadData()
        }else{
            self.shippingMethodSelection(indexPath.row)
        }
    }
    @objc func shippingMethodSelectBtn_Tapped(_ sender : UIButton){
        self.shippingMethodSelection(sender.tag)
    }
    func shippingMethodSelection(_ index:Int){
        if shippingMethodsArray.count > 1{
            for i in 0...shippingMethodsArray.count-1{
                shippingMethodsArray[i].isSelect = false
            }
            let shipDate = shippingMethodsArray[index].ShippingTime.components(separatedBy: ".")[0]
            firstShipDate = shippingMethodsArray[index].ShippingTime.components(separatedBy: "T")[0]
            firstSelectShipId = shippingMethodsArray[index].Id
            var weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: shipDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
            var nextShipDate = DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime)
            let currentShipDate = DateConverts.convertStringToDate(date: shippingMethodsArray[index].ShippingTime.components(separatedBy: "T")[0], dateformatType: .dateR)
            let todayDate = DateConverts.convertDateToDateFormate(date: currentDate)
            if currentShipDate > todayDate{
                let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                if time.count > 0{
                    shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                    shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                    selectSlotId = time[0].Id
                    for i in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                        if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].Id{
                            shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                        }
                    }
                    print("Match")
                }else{
                    print("Not Match")
                    for i in 1...shippingMethodsArray[0].FutureDays{
                        nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                        weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                        let time = shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                        if time.count > 0{
                            shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                            shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                            selectSlotId = time[0].Id
                            for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                    shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                }
                            }
                            print("Match")
                            break
                        }else{
                            print("Not Match")
                        }
                    }
                }
            }else{
                let timeSlot = expectTimeSlot(presentDate: shipDate, index: index)
                if timeSlot == "NotMatch"{
                    let firstTimeSlot = selectFirstTimeSlot(presentDate: DateConverts.convertDateToString(date: nextShipDate, dateformatType: .dateTtime), index: index)
                    if firstTimeSlot == "NotMatch"{
                        for i in 1...shippingMethodsArray[index].FutureDays{
                            nextShipDate = Calendar.current.date(byAdding: .day, value: +i, to: DateConverts.convertStringToDate(date: shipDate, dateformatType: .dateTtime))!
                            weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .EEEE))")
                            let time = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.filter({$0.IsClose == false && $0.IsSlotFull == false})
                            if time.count > 0{
                                shipTimeSlot = "\(time[0].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(time[0].EndTime)"
                                shipTimeSlotDate = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(nextShipDate)", inputDateformatType: .dateTimeZ, outputDateformatType: .date))"
                                selectSlotId = time[0].Id
                                for j in 1...shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot!.count-1{
                                    if time[0].Id == shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].Id{
                                        shippingMethodsArray[0].ShippingSlots![weekDayNum-1].Slot![j].isSelect = true
                                    }
                                }
//                                if time[0].Id == shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id{
//                                    shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
//                                }
                                print("Match")
                                break
                            }else{
                                print("Not Match")
                            }
                        }
                    }else{
                        print("Match")
                    }
                }else{
                    print("Match")
                }
            }
            self.estimateDeliveryTimeLbl.text = shipTimeSlot
            self.estimateDeliveryDateLbl.text = shipTimeSlotDate
            shippingMethodsArray[index].isSelect = !shippingMethodsArray[index].isSelect!
            shippingMethodTableView.reloadData()
            //deliveryCharge = cartDetailsArray[0].ShippingMethods![index].ShippingCost!
            //vatCharge = Double(cartDetailsArray[0].ShippingMethods![index].ShippingCost!) * (Double(cartDetailsArray[0].Cart!.VatPercentage!)/100.0)
            //netAmount = cartDetailsArray[0].Cart!.ItemTotal! + vatCharge + deliveryCharge + cartDetailsArray[0].Cart!.Vatcharges!
            var vatPercentage = (Double(cartDetailsArray[0].Cart![0].VatPercentage!)/100.0 + 1.0)
            vatPercentage = Double(shippingMethodsArray[index].ShippingCost) / vatPercentage
            vatCharge = Double(vatPercentage - shippingMethodsArray[index].ShippingCost) * -1
            deliveryCharge = shippingMethodsArray[index].ShippingCost - vatCharge
            //netAmount = cartDetailsArray[0].Cart![0].NetTotal! + shippingMethodsArray[index].ShippingCost
            netAmount = cartDetailsArray[0].Cart![0].ItemTotal! + deliveryCharge
            
            //if Int(netAmount) >= cartDetailsArray[0].ShippingMethods![0].FreeDeliveryAmount!{
            if shippingMethodsArray[index].FreeDeliveryAmount == 0{
                bottomFreeDeliveryMsgLblHeight.constant = 0
                bottomViewHeight.constant = 80
            }else{
                bottomFreeDeliveryMsgLblHeight.constant = 21
                bottomViewHeight.constant = 101
                bottomView.clipsToBounds = true
            }
            bottomFreeDeliveryMsgLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add items worth", value: "", table: nil))!) \(Double(shippingMethodsArray[index].FreeDeliveryAmount).withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "more for free delivery", value: "", table: nil))!)"
            priceCalculation()
            if walletCheckedBtn.currentImage == UIImage(named: "Box Selected"){
                self.walletCheckedBtn_Tapped(walletCheckedBtn)
            }else{
                walletCheckedBtn.setImage(UIImage(named: "Box Un Select"), for: .normal)
            }
        }
    }
    func expectTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        var status = ""
        for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
            let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
            let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
            if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: startTime, dateformatType: .dateNTimeA) as NSDate, endTime: DateConverts.convertStringToDate(date: endTime, dateformatType: .dateNTimeA) as NSDate) == true{
                if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                    shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                  shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                    shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                    selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                    status = "Match"
                    break
                }else{
                    status = "NotMatch"
                }
            }else{
                status = "NotMatch"
            }
        }
        print("Time Slot Status \(status)")
        return status
    }
    func selectFirstTimeSlot(presentDate:String, index:Int) -> String{
        let weekDayNum = WeekDaysModel.weekDayNum(Name: "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .EEEE))")
        let currentDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .dateNTimeA)
        var status = ""
        let start = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![0].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        let end = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(DateConverts.convertStringToStringDates(inputDateStr: shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1].StartTime, inputDateformatType: .timeA, outputDateformatType: .time))"
        if isTimeStampCurrent(timeStamp: DateConverts.convertStringToDate(date: presentDate, dateformatType: .dateTtime) as NSDate, startTime: DateConverts.convertStringToDate(date: start, dateformatType: .dateNTime) as NSDate, endTime: DateConverts.convertStringToDate(date: end, dateformatType: .dateNTime) as NSDate) == true || currentDate < start{
            for i in 0...shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot!.count-1{
                let startTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime)"
                let endTime = "\(DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                if status == "NotMatch"{
                    if currentDate < startTime && currentDate < endTime{
                        if shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsClose == false && shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].IsSlotFull == false{
                            shipTimeSlot = "\(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "to", value: "", table: nil))!) \(shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].EndTime)"
                            shipTimeSlotDate = DateConverts.convertStringToStringDates(inputDateStr: presentDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                            shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].isSelect = true
                            selectSlotId = shippingMethodsArray[index].ShippingSlots![weekDayNum-1].Slot![i].Id
                            status =  "Match"
                            break
                        }else{
                            status =  "NotMatch"
                        }
                    }else{
                        status = "NotMatch"
                    }
                }else{
                    status = "NotMatch"
                }
            }
        }else{
            status = "NotMatch"
        }
        print("First Time Slot Status \(status)")
        return status
    }
    func isTimeStampCurrent(timeStamp:NSDate, startTime:NSDate, endTime:NSDate)->Bool{
        timeStamp.earlierDate(endTime as Date) == timeStamp as Date && timeStamp.laterDate(startTime as Date) == timeStamp as Date
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension ConfirmationVC:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = carDetailsArray{
            if let _ = carDetailsArray.SubOrderTypes{
                return carDetailsArray.SubOrderTypes!.count
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        pickupTypeCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PickUpTypeCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PickUpTypeCVCell", value: "", table: nil))!)
        let cell = pickupTypeCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PickUpTypeCVCell", value: "", table: nil))!, for: indexPath) as! PickUpTypeCVCell
        
        let dic = carDetailsArray.SubOrderTypes![indexPath.row]
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.nameLbl.text = dic.SubTypeEn
        }else{
            cell.nameLbl.text = dic.SubTypeAr
        }
        //Image
        let ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.typeImg.kf.setImage(with: url)
        
        if let _ = pickupSelectId{
            if pickupSelectId == dic.Id{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.7254901961, green: 0.3254901961, blue: 0.2588235294, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            }
        }else{
            cell.BGView.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.pickupTypeCollectionView.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth/3 + 1, height: screenSize.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        pickupSelectId = carDetailsArray.SubOrderTypes![indexPath.row].Id
        self.pickupTypeCollectionView.reloadData()
        if carDetailsArray.SubOrderTypes![indexPath.row].Id == 15{
            if selectCarDicArray.count > 0{
                self.carDetailsView.isHidden = false
                self.carDetailsViewHeight.constant = 123
                self.carDetailsViewTop.constant = 15
            }
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "CarsListPopUpVC") as! CarsListPopUpVC
            obj.currentStoreId = currentStoreId
            obj.carsListPopUpVCDelegate = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: false, completion: nil)
        }else{
            self.carDetailsView.isHidden = true
            self.carDetailsViewHeight.constant = 0
            self.carDetailsViewTop.constant = 0
        }
    }
}
@available(iOS 13.0, *)
extension ConfirmationVC:PKPaymentAuthorizationViewControllerDelegate{
   func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
       controller.dismiss(animated: true, completion: nil)
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
           ANLoader.hide()
       }
       self.isOnlinePaymeny = false
        Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            controller.dismiss(animated: true, completion: nil)
            self.getDetailsService()
        }))
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    if let params = try? OPPApplePayPaymentParams(checkoutID: self.checkoutID, tokenData: payment.token.paymentData) as OPPApplePayPaymentParams? {
        self.transaction = OPPTransaction(paymentParams: params)
        self.provider.submitTransaction(OPPTransaction(paymentParams: params), completionHandler: { (transaction, error) in
        if (error != nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            controller.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.getDetailsService()
                }))
            }
        }else{
            controller.dismiss(animated: true, completion: nil)
            self.provider.requestCheckoutInfo(withCheckoutID: self.checkoutID) { (checkoutInfo, error) in
            DispatchQueue.main.async {
            guard let resourcePath = checkoutInfo?.resourcePath else {
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)..", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.getDetailsService()
                }))
                return
            }
            ANLoader.showLoading("", disableUI: true)
            self.transaction = nil
                var estimateTime = ""
                if self.cartDetailsArray[0].Cart![0].SectionType! == 2{
                    estimateTime = "\(self.orderExpectTime!)"
                }else{
                    estimateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryTimeLbl.text!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
                }
                let dic:[String:Any] = ["resourcePath": resourcePath,"userId":"\(UserDef.getUserId())", "isApplePay":self.isApplePay, "isSTCPay":self.isSTCPay, "isMada":self.isMadaPay, "InvoiceNo":self.InvoiceNoStr, "isVisaMaster":self.isVisa, "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))", "ExpectedDate":"\(estimateTime)", "KitchenStartTime":"\(DateConverts.convertDateToString(date: self.kitchenStartTime!, dateformatType: .dateTimeSSS))", "OrderStatus":"New"]
            PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    self.TrackOrderScreen()
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                    self.isOnlinePaymeny = false
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }
            }
            }){(error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                self.isOnlinePaymeny = false
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                    self.getDetailsService()
                }))
            }
            }
        }
        }
        })
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)...", action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
        }
    }
}
@available(iOS 13.0, *)
extension ConfirmationVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    // MARK: - Payment helpers
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
            return
        }
        self.transaction = transaction
        if transaction.type == .synchronous {
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        } else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = OPPCheckoutSettings.init()
        if self.isSTCPay == true{
            checkoutSettings.paymentBrands = ConfirmationVC.STCPAYPaymentBrands
        }else if self.isMadaPay == true {
            checkoutSettings.paymentBrands = ConfirmationVC.MADAPAYPaymentBrands
        }else{
            checkoutSettings.paymentBrands = ConfirmationVC.checkoutPaymentBrands
        }
        checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
        checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
        checkoutSettings.theme.confirmationButtonColor = self.mainColor
        checkoutSettings.theme.accentColor = self.mainColor
        checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
        checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            self.isOnlinePaymeny = false
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resource path is invalid", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
            return
        }
        ANLoader.showLoading("", disableUI: true)
        self.transaction = nil
        // self.processingView.startAnimating()
        var estimateTime = ""
        if cartDetailsArray[0].Cart![0].SectionType! == 2{
            estimateTime = "\(self.orderExpectTime!)"
        }else{
            estimateTime = "\(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryDateLbl.text!, inputDateformatType: .date, outputDateformatType: .dateR)) \(DateConverts.convertStringToStringDates(inputDateStr: self.estimateDeliveryTimeLbl.text!, inputDateformatType: .timeA, outputDateformatType: .timeSSS))"
        }
        var dic:[String:Any] = ["resourcePath": resourcePath,
                                "userId":"\(UserDef.getUserId())",
                                "isApplePay":self.isApplePay,
                                "isSTCPay":self.isSTCPay,
                                "isMada":self.isMadaPay,
                                "isVisaMaster" : self.isVisa,
                                "InvoiceNo":self.InvoiceNoStr,
                                "OrderDate":"\(DateConverts.convertDateToString(date: Date(), dateformatType: .dateTtimeSSS))", //2021-03-18T17:37:22.179
                                "ExpectedDate":"\(estimateTime)",
                                "KitchenStartTime":"\(DateConverts.convertDateToString(date: self.kitchenStartTime!, dateformatType: .dateTimeSSS))",
                                "OrderStatus":"New"]
        if self.isSubscription == true {
              dic["IsSubscription"] = true
              dic["FrequencyId"] = self.frequencyId
              dic["OrderId"] = self.orderId
        }
        //print(dic)
        PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                self.isOnlinePaymeny = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }else{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                   // DispatchQueue.main.async {
                    self.TrackOrderScreen()
                    //}
                }else{
                    self.isOnlinePaymeny = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    //Utils.showResult(presenter: self, success: false, message: "Your payment was not successful")
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!)
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your payment was not successful", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.getDetailsService()
                    }))
                }
            }
        }){ (error) in
            self.isOnlinePaymeny = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            //Alert.showToastAlert(on:self, message:error) //Change to Alert Swathi
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                self.getDetailsService()
            }))
        }
    }
    // MARK: - Async payment callback
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
}
@available(iOS 13.0, *)
extension ConfirmationVC: STCPayVCDelegate{
    func didTapAction(mobileNumber: String) {
        stcPayNumber = mobileNumber
     }
}
//// MARK: Calender Delegate Methods
//@available(iOS 13.0, *)
//extension ConfirmationVC:CalenderVCDelegate{
//    func didTapAction(getDate:Date,whereType:Int){
//        print(DateConverts.convertDateToString(date: getDate, dateformatType: .dateDots))
//        estimateDeliveryDateLbl.text = "\(DateConverts.convertDateToString(date: getDate, dateformatType: .date))"
//        let shipDetails = self.cartDetailsArray[0].ShippingMethods!.filter({$0.CostId! == self.firstSelectShipId})
//        estimateDeliveryTimeLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: shipDetails[0].StartTime!, inputDateformatType: .timeA, outputDateformatType: .timeA))"
//    }
//}
// MARK: Time Slot Delegate Methods
@available(iOS 13.0, *)
extension ConfirmationVC:TimeSlotVCDelegate{
    func didTapAction(expectDate: Date, startTime: String, endTime: String, whereType: Int, SlotId: Int) {
        print(DateConverts.convertDateToString(date: expectDate, dateformatType: .dateDots))
        estimateDeliveryDateLbl.text = "\(DateConverts.convertDateToString(date: expectDate, dateformatType: .date))"
        estimateDeliveryTimeLbl.text = "\(startTime) to \(endTime)"
        selectSlotId = SlotId
    }
}
extension ConfirmationVC : ManageAddressVCDelegate{
    func didSelectAddress() {
        self.isShippingMethodCall = true
        self.firstSelectShipId = 0
        bottomFreeDeliveryMsgLblHeight.constant = 0
        bottomViewHeight.constant = 80
        self.getDetailsService()
    }
}
extension ConfirmationVC : PromotionsVCDelegate{
    func didSelectPromotion() {
        self.isShippingMethodCall = true
        self.firstSelectShipId = 0
        bottomFreeDeliveryMsgLblHeight.constant = 0
        bottomViewHeight.constant = 80
        self.getDetailsService()
    }
}
extension ConfirmationVC: CarsListPopUpVCDelegate{
    func didSelectCar(Id: Int) {
        if SaveAddressClass.carInformationArray.count > 0{
            if let _ = SaveAddressClass.carInformationArray[0].Cars{
                if Id != 0{
                    selectCarDicArray = SaveAddressClass.carInformationArray[0].Cars!.filter({$0.Id == Id})
                    self.carDetailsDisplay()
                }else{
                    if selectCarDicArray.count > 0{
                        self.carDetailsDisplay()
                    }else{
                        selectCarDicArray = SaveAddressClass.carInformationArray[0].Cars!.filter({$0.Id == Id})
                        self.carDetailsDisplay()
                    }
                }
            }
        }
    }
    func carDetailsDisplay(){
        if selectCarDicArray.count > 0{
            self.carDetailsView.isHidden = false
            self.carDetailsViewHeight.constant = 123
            self.carDetailsViewTop.constant = 15
            self.carVehicleNumLbl.text = selectCarDicArray[0].VehicleNumber
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.carBrandLbl.text = selectCarDicArray[0].BrandEn
                self.carColorLbl.text = selectCarDicArray[0].ColorEn
            }else{
                self.carBrandLbl.text = selectCarDicArray[0].BrandAr
                self.carColorLbl.text = selectCarDicArray[0].ColorAr
            }
        }else{
            self.carDetailsView.isHidden = true
            self.carDetailsViewHeight.constant = 0
            self.carDetailsViewTop.constant = 0
        }
    }
}
