//
//  STCPayVC.swift
//  Chef
//
//  Created by Creative Solutions on 2/1/21.
//  Copyright © 2021 Creative Solutions. All rights reserved.
//

import UIKit
protocol STCPayVCDelegate {
    func didTapAction(mobileNumber:String)
}
@available(iOS 13.0, *)
class STCPayVC: UIViewController {
    var stcPayVCDelegate:STCPayVCDelegate!

    @IBOutlet var mobileNumberTf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mobileNumberTf.delegate = self
        //let userDetails = UserDef.getUserProfileDic(key: "userDetails")
        let userDic = getUserDetails()

        self.mobileNumberTf.text = "\(userDic.Mobile)"
        //self.mobileNumberTf.becomeFirstResponder()
       // self.mobileNumberTf.clearButtonMode = .always
    }
    @IBAction func continueBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if (self.validInputParams()==false){
            return
        }
        self.dismiss(animated: true, completion: nil)
        self.stcPayVCDelegate.didTapAction(mobileNumber: self.mobileNumberTf.text!)
    }
    func makePrefix() {
        let attributedString = NSMutableAttributedString(string: "+966 ")
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0,5))
        mobileNumberTf.attributedText = attributedString
    }
    func validInputParams() -> Bool {
        if mobileNumberTf.text == nil || (mobileNumberTf.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else {
            if !Validations.isSTCMobileNumberValid(mobile:mobileNumberTf.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
            
        }
        return true
    }
}
@available(iOS 13.0, *)
extension STCPayVC:UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
            if textField == mobileNumberTf {
                let char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    if range.location == 0 {
                        return false
                    }
                }
                
                if(mobileNumberTf.text?.count == 3){
                    let num = "123456789"
                    let checNum = CharacterSet.init(charactersIn: num)
                    if (string.rangeOfCharacter(from: checNum) != nil){
                        return true
                    }else{
                        return false
                    }
                }
                let currentText = textField.text ?? ""
                guard let stringRange = Range(range, in: currentText) else { return false }
                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                    return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                        updatedText.safelyLimitedTo(length: 12)
                
            }
            return true
        }
    
    @objc func textFieldDidChange(textField: UITextField){
            
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}

