//
//  CommentVC.swift
//  drCafe
//
//  Created by Devbox on 25/08/21.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import UIKit

protocol CommentVCDelegate {
    func didTapAction(note:String, IndexPath:Int)
}

@available(iOS 13.0, *)
class CommentVC: UIViewController {

    @IBOutlet weak var commentTV: UITextView!
    
    var commentVCDelegate:CommentVCDelegate!
    var commentPlaceholderText:String!
    var indexPath:Int = 0
    var commentsStr:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if commentsStr.count > 0 {
            commentTV.text! = commentsStr
        }else{
             commentTV.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter Comment", value: "", table: nil))!
        
        }
        commentPlaceholderText = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter Comment", value: "", table: nil))!
        commentTV.layer.borderWidth = 1
        commentTV.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        if commentTV.text! == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter Comment", value: "", table: nil))! {
            commentTV.textColor = UIColor.lightGray
        }else{
             commentTV.textColor = UIColor.black
        }
        commentTV.layer.cornerRadius = 5
        commentTV.delegate = self
        commentTV.returnKeyType = .done
    }
    
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func doneBtn_Tapped(_ sender: Any) {
//        let trimmedString = commentTV.text.trimmingCharacters(in: .whitespaces)
//        if trimmedString == commentPlaceholderText! || trimmedString == "" || trimmedString == " "{
//            Alert.showAlert(on: self, title: Title, message: "Please enter Comments")
//            return
//        }
        if commentTV.text! == "" || commentTV.text == nil{
            commentVCDelegate.didTapAction(note: "", IndexPath: indexPath)
            dismiss(animated: true, completion: nil)
        }else{
            let newString = commentTV.text!.replacingOccurrences(of: "'", with: "")
            commentVCDelegate.didTapAction(note: newString, IndexPath: indexPath)
            dismiss(animated: true, completion: nil)
        }
    }
}
//MARK: UITextView Delegate Methods
@available(iOS 13.0, *)
extension CommentVC:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = commentPlaceholderText!
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            if textView.text != (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter Comment", value: "", table: nil))! && textView.text != nil{
                doneBtn_Tapped(0)
            }
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textView.textInputMode?.primaryLanguage == "emoji") || text.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
