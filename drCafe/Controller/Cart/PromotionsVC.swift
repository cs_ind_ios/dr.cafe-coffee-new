//
//  PromotionsVC.swift
//  drCafe
//
//  Created by Devbox on 13/07/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import Firebase

protocol PromotionsVCDelegate {
    func didSelectPromotion()
}

@available(iOS 13.0, *)
class PromotionsVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var addNewVoucherNameLbl: UILabel!
    @IBOutlet weak var addVoucherTF: UITextField!
    @IBOutlet weak var addVoucherApplyBtn: UIButton!
    
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var promotionsTV: UITableView!
    
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    var promotionsVCDelegate:PromotionsVCDelegate!
    
    var cartId = 0
    var estimateDate = ""
    var slotId = 0
    var shippingMethodId = 0
    var popover = Popover()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        promotionsTV.delegate = self
        promotionsTV.dataSource = self
        addVoucherTF.delegate = self
        
        promotionsTV.estimatedRowHeight = 390
        
        if SaveAddressClass.promotionsArray.count > 0{
            promotionsTV.reloadData()
        }else{
            self.PromotionDetailsGetService()
        }
        
        BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        addVoucherApplyBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        addNewVoucherNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add Coupons", value: "", table: nil))!
        
    }
    func PromotionDetailsGetService(){
        let dic:[String : Any] = ["userId":UserDef.getUserId(), "RequestBy":2]
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("PomotionDetailsService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        CartModuleServices.PomotionDetailsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                //DispatchQueue.main.sync {
                    if data.Data != nil{
                        if data.Data!.count > 0{
                            SaveAddressClass.promotionsArray = data.Data!
                            for i in 0...SaveAddressClass.promotionsArray.count - 1{
                                SaveAddressClass.promotionsArray[i].isSelect = false
                                SaveAddressClass.promotionsArray[i].quantity = 3
                            }
                        }
                    }else{
                        SaveAddressClass.promotionsArray.removeAll()
                    }
                    self.promotionsTV.reloadData()
                //}
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func infoBtn_Tapped(_ sender: UIButton) {
        popUpNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enter the Coupon Code or select coupon from below list", value: "", table: nil))!
        let startPoint = CGPoint(x: infoBtn.center.x + 12, y: 130)
        self.popUpLblMainView.frame =  CGRect(x: 0, y: 0, width: 220, height: 60)
        popover.show(self.popUpLblMainView, point: startPoint)
    }
    @IBAction func addVoucherApplyBtn_Tapped(_ sender: Any) {
        if addVoucherTF.text != nil && addVoucherTF.text! != ""{
            var dic:[String : Any]!
            if slotId != 0{
                dic = ["CartId":cartId, "PromoCode":"\(addVoucherTF.text!)", "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "\(slotId)", "shippingMethodId":"\(shippingMethodId)"] as [String : Any]
            }else{
                dic = ["CartId":cartId, "PromoCode":"\(addVoucherTF.text!)", "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "", "shippingMethodId":""] as [String : Any]
            }
            CartModuleServices.PromotionAppliedService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                    }
                }else{
                    Alert.showToastDown(on: self, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Promotion Applied", value: "", table: nil))!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.promotionsVCDelegate.didSelectPromotion()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            Alert.showToast(on: self, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Enter Promocode", value: "", table: nil))!)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
@available(iOS 13.0, *)
extension PromotionsVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SaveAddressClass.promotionsArray.count > 0{
            return SaveAddressClass.promotionsArray.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        promotionsTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!)
        let cell = promotionsTV.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!, for: indexPath) as! PromotionsTVCell
        
        let dic = SaveAddressClass.promotionsArray[indexPath.row]
        
        cell.promotionNameLbl.text = dic.PromoCode
        cell.validTillNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Valid Till", value: "", table: nil))!
        if dic.EndDate != ""{
            let validDate = dic.EndDate.components(separatedBy: ".")[0]
            cell.validDateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: validDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
        }else{
            cell.validDateLbl.text = ""
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.descLbl.text = dic.DescriptionEn
        }else{
            cell.descLbl.text = dic.DescriptionAr
        }
        cell.availableDrinksNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quantity", value: "", table: nil))!
        cell.quantityBtn.setTitle("", for: .normal)
        cell.quantityBtn.isUserInteractionEnabled = false
        cell.quantityLbl.text = "\(dic.quantity!)"
        cell.promotionQuantityLbl.text = "\(dic.Quantity)"
        
        if dic.Quantity > 0{
            cell.availableDrinksViewHeight.constant = 59
        }else{
            cell.availableDrinksViewHeight.constant = 0
        }
        
        if dic.isSelect! == true{
            cell.promotionSelectImg.image = UIImage(named: "Oval Copy")
            cell.quantityView.isHidden = false
            cell.quantityBtn.isHidden = true
        }else{
            cell.promotionSelectImg.image = UIImage(named: "Oval Copy 2")
            cell.quantityView.isHidden = true
            cell.quantityBtn.isHidden = false
        }
        
        //Image
        var ImgStr:NSString!
        if AppDelegate.getDelegate().appLanguage == "English"{
            ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
        }else{
            ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
        }
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.promotionImg.kf.setImage(with: url)
        
        cell.plusBtn.tag = indexPath.row
        cell.plusBtn.addTarget(self, action: #selector(plusBtn_Tapped(_:)), for: .touchUpInside)
        cell.minusBtn.tag = indexPath.row
        cell.minusBtn.addTarget(self, action: #selector(minusBtn_Tapped(_:)), for: .touchUpInside)
        cell.applyBtn.tag = indexPath.row
        cell.applyBtn.addTarget(self, action: #selector(promotionApplyBtn_Tapped(_:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if SaveAddressClass.promotionsArray[indexPath.row].IsEnable == 1{
            var dic:[String : Any]!
            if slotId != 0{
                dic = ["CartId":cartId, "PromoCode":SaveAddressClass.promotionsArray[indexPath.row].PromoCode, "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "\(slotId)", "shippingMethodId":"\(shippingMethodId)"] as [String : Any]
            }else{
                dic = ["CartId":cartId, "PromoCode":SaveAddressClass.promotionsArray[indexPath.row].PromoCode, "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "", "shippingMethodId":""] as [String : Any]
            }
            CartModuleServices.PromotionAppliedService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                    }
                }else{
                    for i in 0...SaveAddressClass.promotionsArray.count - 1{
                        SaveAddressClass.promotionsArray[i].isSelect = false
                    }
                    SaveAddressClass.promotionsArray[indexPath.row].isSelect = true
                    self.promotionsTV.reloadData()
    //                if AppDelegate.getDelegate().appLanguage == "English"{
    //                    Alert.showToastDown(on: self, message: data.MessageEn)
    //                }else{
    //                    Alert.showToastDown(on: self, message: data.MessageAr)
    //                }
                    Alert.showToastDown(on: self, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Promotion Applied", value: "", table: nil))!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.promotionsVCDelegate.didSelectPromotion()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            let dic = SaveAddressClass.promotionsArray[indexPath.row]
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "Promotion available from \(dic.StartTime) to \(dic.EndTime)")
            //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Promotion available from", value: "", table: nil))!) \(dic.StartTime) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "To", value: "", table: nil))!) \(dic.EndTime)")
        }
    }
    @objc func plusBtn_Tapped(_ sender: UIButton){
        if SaveAddressClass.promotionsArray[sender.tag].quantity! < 3{
            SaveAddressClass.promotionsArray[sender.tag].quantity! = SaveAddressClass.promotionsArray[sender.tag].quantity! + 1
        }
        self.promotionsTV.reloadData()
    }
    @objc func minusBtn_Tapped(_ sender: UIButton){
        if SaveAddressClass.promotionsArray[sender.tag].quantity! > 1{
            SaveAddressClass.promotionsArray[sender.tag].quantity! = SaveAddressClass.promotionsArray[sender.tag].quantity! - 1
        }
        self.promotionsTV.reloadData()
    }
    @objc func promotionApplyBtn_Tapped(_ sender: UIButton){
        var dic:[String : Any]!
        if slotId != 0{
            dic = ["CartId":cartId, "PromoCode":SaveAddressClass.promotionsArray[sender.tag].PromoCode, "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "\(slotId)", "shippingMethodId":"\(shippingMethodId)"] as [String : Any]
        }else{
            dic = ["CartId":cartId, "PromoCode":SaveAddressClass.promotionsArray[sender.tag].PromoCode, "action": 1, "EstimatedDateTime":"\(estimateDate)", "SlotId": "", "shippingMethodId":""] as [String : Any]
        }
        CartModuleServices.PromotionAppliedService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr)
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
//MARK: TextField Delegate Method
@available(iOS 13.0, *)
extension PromotionsVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil) || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
