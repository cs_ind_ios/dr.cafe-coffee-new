//
//  StoresFilterVC.swift
//  drCafe
//
//  Created by Devbox on 28/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

//MARK: Stores Filter PopUp Delegate Protocol
protocol StoresFilterVCDelegate {
    func didTapAction(dic:[StoresListModel], where:Int)
}

@available(iOS 13.0, *)
class StoresFilterVC: UIViewController {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var orderTypeCV: UICollectionView!
    @IBOutlet weak var orderTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var storeTypeNameLbl: UILabel!
    @IBOutlet weak var storeTypeCV: UICollectionView!
    @IBOutlet weak var storeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var facilityNameLbl: UILabel!
    @IBOutlet weak var facilityCV: UICollectionView!
    @IBOutlet weak var facilityViewHeight: NSLayoutConstraint!
    @IBOutlet weak var segmentNameLbl: UILabel!
    @IBOutlet weak var segmentCV: UICollectionView!
    @IBOutlet weak var segmentViewHeight: NSLayoutConstraint!
    
    var storesFilterVCDelegate:StoresFilterVCDelegate!
    var filterDetailsArray = [StoresListModel]()
    
    var orderType = 0
    var whereObj = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        orderTypeCV.delegate = self
        orderTypeCV.dataSource = self
        storeTypeCV.delegate = self
        storeTypeCV.dataSource = self
        facilityCV.delegate = self
        facilityCV.dataSource = self
        segmentCV.delegate = self
        segmentCV.dataSource = self
        
        orderTypeCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        storeTypeCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        facilityCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        segmentCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            orderTypeCV.semanticContentAttribute = .forceLeftToRight
            storeTypeCV.semanticContentAttribute = .forceLeftToRight
            facilityCV.semanticContentAttribute = .forceLeftToRight
            segmentCV.semanticContentAttribute = .forceLeftToRight
        }else{
            orderTypeCV.semanticContentAttribute = .forceRightToLeft
            storeTypeCV.semanticContentAttribute = .forceRightToLeft
            facilityCV.semanticContentAttribute = .forceRightToLeft
            segmentCV.semanticContentAttribute = .forceRightToLeft
        }
        
        self.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!
        self.storeTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Type", value: "", table: nil))!
        self.facilityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Facility", value: "", table: nil))!
        self.segmentNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Segment", value: "", table: nil))!
        
    }
    override func viewWillAppear(_ animated: Bool) {
        doneBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        if SaveAddressClass.selectFilterDetailsArray.count > 0{
            if filterDetailsArray.count > 0{
                for i in 0...filterDetailsArray[0].OrderTypes!.count - 1{
                    self.filterDetailsArray[0].OrderTypes![i].isSelect = false
                }
                for i in 0...filterDetailsArray[0].Facilities!.count - 1{
                    self.filterDetailsArray[0].Facilities![i].isSelect = false
                }
                for i in 0...filterDetailsArray[0].Segments!.count - 1{
                    self.filterDetailsArray[0].Segments![i].isSelect = false
                }
                for i in 0...filterDetailsArray[0].StoreTypes!.count - 1{
                    self.filterDetailsArray[0].StoreTypes![i].isSelect = false
                }
                let selectOrderTypes = SaveAddressClass.selectFilterDetailsArray[0].OrderTypes!.filter({$0.isSelect! == true})
                if selectOrderTypes.count > 0{
                    for j in 0...selectOrderTypes.count-1{
                        for i in 0...filterDetailsArray[0].OrderTypes!.count - 1{
                            if filterDetailsArray[0].OrderTypes![i].Id == selectOrderTypes[j].Id{
                                self.filterDetailsArray[0].OrderTypes![i].isSelect = true
                                break
                            }
                        }
                    }
                }
                let selectFacilities = SaveAddressClass.selectFilterDetailsArray[0].Facilities!.filter({$0.isSelect! == true})
                if selectFacilities.count > 0{
                    for j in 0...selectFacilities.count-1{
                        for i in 0...filterDetailsArray[0].Facilities!.count - 1{
                            if filterDetailsArray[0].Facilities![i].Id == selectFacilities[j].Id{
                                self.filterDetailsArray[0].Facilities![i].isSelect = true
                                break
                            }
                        }
                    }
                }
                let selectSegments = SaveAddressClass.selectFilterDetailsArray[0].Segments!.filter({$0.isSelect! == true})
                if selectSegments.count > 0{
                    for j in 0...selectSegments.count-1{
                        for i in 0...filterDetailsArray[0].Segments!.count - 1{
                            if filterDetailsArray[0].Segments![i].Id == selectSegments[j].Id{
                                self.filterDetailsArray[0].Segments![i].isSelect = true
                                break
                            }
                        }
                    }
                }
                let selectStoreTypes = SaveAddressClass.selectFilterDetailsArray[0].StoreTypes!.filter({$0.isSelect! == true})
                if selectStoreTypes.count > 0{
                    for j in 0...selectStoreTypes.count-1{
                        for i in 0...filterDetailsArray[0].StoreTypes!.count - 1{
                            if filterDetailsArray[0].StoreTypes![i].Id == selectStoreTypes[j].Id{
                                self.filterDetailsArray[0].StoreTypes![i].isSelect = true
                                break
                            }
                        }
                    }
                }
            }
        }else{
            if filterDetailsArray.count > 0{
                for i in 0...filterDetailsArray[0].OrderTypes!.count - 1{
                    if filterDetailsArray[0].OrderTypes![i].Id == orderType{
                        self.filterDetailsArray[0].OrderTypes![i].isSelect = true
                    }else{
                        self.filterDetailsArray[0].OrderTypes![i].isSelect = false
                    }
                }
                if filterDetailsArray[0].Facilities!.count > 0{
                    for i in 0...filterDetailsArray[0].Facilities!.count - 1{
                        self.filterDetailsArray[0].Facilities![i].isSelect = false
                    }
                }
                if filterDetailsArray[0].Segments!.count > 0{
                    for i in 0...filterDetailsArray[0].Segments!.count - 1{
                        self.filterDetailsArray[0].Segments![i].isSelect = false
                    }
                }
                if filterDetailsArray[0].StoreTypes!.count > 0{
                    for i in 0...filterDetailsArray[0].StoreTypes!.count - 1{
                        self.filterDetailsArray[0].StoreTypes![i].isSelect = false
                    }
                }
            }
        }
    }
    @IBAction func cancelBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        storesFilterVCDelegate.didTapAction(dic: filterDetailsArray, where:whereObj)
        self.dismiss(animated: true, completion: nil)
    }
}
@available(iOS 13.0, *)
extension StoresFilterVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filterDetailsArray.count > 0{
            if collectionView == orderTypeCV{
                return filterDetailsArray[0].OrderTypes!.count
            }else if collectionView == storeTypeCV{
                return filterDetailsArray[0].StoreTypes!.count
            }else if collectionView == facilityCV{
                return filterDetailsArray[0].Facilities!.count
            }else{
                return filterDetailsArray[0].Segments!.count
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == orderTypeCV{
            let cell = orderTypeCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = filterDetailsArray[0].OrderTypes![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = filterDetailsArray[0].OrderTypes![indexPath.row].NameAr
            }
            
            if filterDetailsArray[0].OrderTypes![indexPath.row].isSelect == true{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            }else{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(filterDetailsArray[0].OrderTypes![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            orderTypeViewHeight.constant = orderTypeCV.contentSize.height + 52
            return cell
        }else if collectionView == storeTypeCV{
            let cell = storeTypeCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = filterDetailsArray[0].StoreTypes![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = filterDetailsArray[0].StoreTypes![indexPath.row].NameAr
            }
            
            if filterDetailsArray[0].StoreTypes![indexPath.row].isSelect == true{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            }else{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(filterDetailsArray[0].StoreTypes![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            storeTypeViewHeight.constant = storeTypeCV.contentSize.height + 52
            return cell
        }else if collectionView == facilityCV{
            let cell = facilityCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = filterDetailsArray[0].Facilities![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = filterDetailsArray[0].Facilities![indexPath.row].NameAr
            }
            if filterDetailsArray[0].Facilities![indexPath.row].isSelect == true{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            }else{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
            }
            
            //Item Image
            let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(filterDetailsArray[0].Facilities![indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            facilityViewHeight.constant = facilityCV.contentSize.height + 52
            return cell
        }else{
            let cell = segmentCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = filterDetailsArray[0].Segments![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = filterDetailsArray[0].Segments![indexPath.row].NameAr
            }
            if filterDetailsArray[0].Segments![indexPath.row].isSelect == true{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            }else{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
            }
            
            if filterDetailsArray[0].Segments![indexPath.row].Image != nil{
                //Item Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(filterDetailsArray[0].Segments![indexPath.row].Image!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.filterImg.kf.setImage(with: url)
            }else{
                cell.filterImg.image = UIImage(named: "")
            }
            
            //cell.filterImg.image = UIImage(named: filterDetailsArray[0].Segments![indexPath.row].NameEn)
            
            segmentViewHeight.constant = segmentCV.contentSize.height + 52
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width/4 - 20, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == orderTypeCV{
            for i in 0...filterDetailsArray[0].OrderTypes!.count - 1{
                self.filterDetailsArray[0].OrderTypes![i].isSelect = false
            }
            self.filterDetailsArray[0].OrderTypes![indexPath.row].isSelect = true
            orderTypeCV.reloadData()
        }else if collectionView == storeTypeCV{
            for i in 0...filterDetailsArray[0].StoreTypes!.count - 1{
                self.filterDetailsArray[0].StoreTypes![i].isSelect = false
            }
            self.filterDetailsArray[0].StoreTypes![indexPath.row].isSelect = true
            storeTypeCV.reloadData()
            //self.filterDetailsArray[0].StoreTypes![indexPath.row].isSelect = !self.filterDetailsArray[0].StoreTypes![indexPath.row].isSelect!
        }else if collectionView == facilityCV{
            self.filterDetailsArray[0].Facilities![indexPath.row].isSelect = !self.filterDetailsArray[0].Facilities![indexPath.row].isSelect!
            facilityCV.reloadData()
        }else{
            self.filterDetailsArray[0].Segments![indexPath.row].isSelect = !self.filterDetailsArray[0].Segments![indexPath.row].isSelect!
            segmentCV.reloadData()
        }
    }
}
