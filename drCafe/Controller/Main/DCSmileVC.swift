//
//  DCSmileVC.swift
//  drCafe
//
//  Created by Devbox on 03/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class DCSmileVC: UIViewController, UITabBarControllerDelegate {

    @IBOutlet weak var DCSmileNameLbl: UILabel!
    @IBOutlet weak var loyaltyProgramNameLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var DCSmileNameImage: UIImageView!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var rewardCardBGImage: UIImageView!
    @IBOutlet weak var couponBtn: UIButton!
    
    @IBOutlet weak var convertPointsNameLbl: UILabel!
    @IBOutlet weak var dcPayNameLbl: UILabel!
    @IBOutlet weak var redeemSmileBGImg: UIImageView!
    @IBOutlet weak var redeemSmileNameLbl: UILabel!
    
    @IBOutlet weak var couponImgBGView: UIView!
    @IBOutlet weak var couponImg: UIImageView!
    @IBOutlet weak var couponNameLbl: UILabel!
    @IBOutlet weak var dealsImgBGView: UIView!
    @IBOutlet weak var dealsImg: UIImageView!
    @IBOutlet weak var dealsNameLbl: UILabel!
    @IBOutlet weak var historyImgBGView: UIView!
    @IBOutlet weak var historyNameLbl: UILabel!
    
    @IBOutlet weak var noDealsAvailableLbl: UILabel!
    @IBOutlet weak var detailsTV: UITableView!
    @IBOutlet weak var detailsTVHeight: NSLayoutConstraint!
    @IBOutlet weak var detailsTVTop: NSLayoutConstraint!

    @IBOutlet weak var couponDetailsTV: UITableView!
    @IBOutlet weak var couponDetailsTVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var earnMoreNameLbl: UILabel!
    @IBOutlet weak var mySmilesNameLbl: UILabel!
    @IBOutlet weak var validThruNameLbl: UILabel!
    @IBOutlet weak var smileLevelNameLbl: UILabel!
    @IBOutlet weak var mySmilesLbl: UILabel!
    @IBOutlet weak var validThruLbl: UILabel!
    @IBOutlet weak var smileLevelLbl: UILabel!
    
    @IBOutlet weak var bottomPopUpView: UIView!
    @IBOutlet weak var popUpDismissBtn: UIButton!
    @IBOutlet var popUpBtnsBGView: [CardView]!
    @IBOutlet weak var aboutSmileNameLbl: UILabel!
    @IBOutlet weak var FAQNameLbl: UILabel!
    @IBOutlet weak var TAndCNameLbl: UILabel!
    
    @IBOutlet weak var noRewardsBGView: UIView!
    @IBOutlet weak var dcRewardsNameLbl: UILabel!
    @IBOutlet weak var muchOfRewardsNameLbl: UILabel!
    @IBOutlet weak var joinNowNameLbl: UILabel!
    @IBOutlet weak var dcLoyaltyProgramNameLbl: UILabel!
    
    @IBOutlet weak var expiryMsgLbl: UILabel!
    @IBOutlet weak var expiryMsgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var expiryMsgViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var celebrationAnimationImg: UIImageView!
    
    var detailsArray = [DCSmileDetailsModel]()
    var histryDetailsArray = [DCSmileHistoryDetailsModel]()
    var dealsArray = [DCSmileDealsDetailsModel]()
    var isCoupon = true
    var isDeals = false
    var pageNumber:Int = 1
    var totalRows:Int = 0
    var tableViewHeight: CGFloat = 0
    var whereObj = 0
    
    var SwiftTimer = Timer()
    var couponCategoryArray:[[String:Any]] = [["Name":"Available","isExpand":true]]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsTV.delegate = self
        detailsTV.dataSource = self
        
        couponDetailsTV.delegate = self
        couponDetailsTV.dataSource = self
        
        myScrollView.delegate = self
        self.tabBarController?.delegate = self
        
        couponDetailsTV.rowHeight = UITableView.automaticDimension
        couponDetailsTV.estimatedRowHeight = 370
        
        //getDetailsService()
        //getHistoryDetailsService()

        historyNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Record", value: "", table: nil))!
        dealsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Deals", value: "", table: nil))!
        couponNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Coupons", value: "", table: nil))!
        aboutSmileNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Smile", value: "", table: nil))!
        TAndCNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "T&C", value: "", table: nil))!
        redeemSmileNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem Smiles", value: "", table: nil))!
        convertPointsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Convert your smiles to", value: "", table: nil))!
        dcPayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE PAY", value: "", table: nil))!
        mySmilesNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Points", value: "", table: nil))!
        validThruNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Valid Till", value: "", table: nil))!
        smileLevelNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Level", value: "", table: nil))!
        loyaltyProgramNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Much of Rewards !", value: "", table: nil))!
        dcRewardsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE SMILE", value: "", table: nil))!
        muchOfRewardsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Much of Rewards !", value: "", table: nil))!
        joinNowNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Join Now", value: "", table: nil))!
        noDealsAvailableLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Deals available for now", value: "", table: nil))!
        let dcLoyalty = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discover the", value: "", table: nil))!, text2: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Loyality Program", value: "", table: nil))!)
        dcLoyaltyProgramNameLbl.attributedText = dcLoyalty
        
        if isUserLogIn() == false{
            self.celebrationAnimationImg.isHidden = true
        }
    }
    @objc func startAnimation(){
        var images = [UIImage]()
        for i in 1...50
        {
            images.append(UIImage(named: "Celebration \(i)")!)
        }
        let animation = UIImage.animatedImage(with: images, duration: 2.0)
        self.celebrationAnimationImg.image = animation
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //self.startTimer()
            //self.celebrationAnimationImg.animationHide()
            //self.celebrationAnimationImg.isHidden = true
            UIViewPropertyAnimator(duration: 0.5, curve: .easeOut, animations: {
                self.celebrationAnimationImg.alpha = 0.0
            }).startAnimation()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        bottomPopUpView.isHidden = true
        
        /*Start Hide - Ravi Nadendla
        //detailsTV.isHidden = true
        //couponDetailsTV.isHidden = false
        //noDealsAvailableLbl.isHidden = true
//        couponImgBGView.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.5411764706, blue: 0.7529411765, alpha: 1)
//        dealsImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
//        historyImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
         End Hide - Ravi Nadendla */
        
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black" {
            noDealsAvailableLbl.textColor = .white
        }else{
            noDealsAvailableLbl.textColor = #colorLiteral(red: 0.3764705882, green: 0.2901960784, blue: 0.09411764706, alpha: 1)
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            DCSmileNameImage.isHidden = false
            DCSmileNameLbl.isHighlighted = true
        }else{
            DCSmileNameImage.isHidden = true
            DCSmileNameLbl.isHighlighted = false
        }
        if isUserLogIn() == true{
            //getDetailsService() // Hide - Ravi Nadendla
            // Add - Ravi Nadendla
            self.detailsArray.removeAll()
            self.dealsArray.removeAll()
            self.histryDetailsArray.removeAll()
            self.pageNumber = 1
            self.totalRows = 0
            self.couponBtn_Tapped(0)
            myScrollView.isHidden = false
            noRewardsBGView.isHidden = true
        }else{
            myScrollView.isHidden = true
            noRewardsBGView.isHidden = false
        }
        
        if whereObj == 0{
            tabBarController?.tabBar.isHidden = false
            backBtn.isHidden = true
        }else{
            tabBarController?.tabBar.isHidden = true
            backBtn.isHidden = false
        }
    }
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if self.tabBarController?.selectedIndex == 3 {
            if isUserLogIn() == true{
                startAnimation()
            }else{
                self.celebrationAnimationImg.isHidden = true
            }
        }
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
        SwiftTimer.invalidate()
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
    }
    //MARK: Start Timer Get Current Time
    func startTimer() {
        SwiftTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(startAnimation), userInfo: nil, repeats: false)
        self.celebrationAnimationImg.isHidden = true
        SwiftTimer.invalidate()
    }
    @IBAction func popUpDismissBtn_Tapped(_ sender: Any) {
        bottomPopUpView.isHidden = true
    }
    @IBAction func dismissBtn_Tapped(_ sender: Any) {
        bottomPopUpView.isHidden = true
    }
    @IBAction func aboutSmileBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            if detailsArray.count > 0{
                if detailsArray[0].Tiers != nil{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    let obj:AboutDCSmileVC = mainStoryBoard.instantiateViewController(withIdentifier: "AboutDCSmileVC") as! AboutDCSmileVC
                    obj.detailsArray = detailsArray[0].Tiers!
                    if detailsArray[0].UserInfo!.count > 0{
                        obj.badgeID = detailsArray[0].UserInfo![0].BadgeId
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            obj.desc = detailsArray[0].UserInfo![0].UpgradeMsgEn
                            obj.smileLevel = "\(detailsArray[0].UserInfo![0].BadgeNameEn)"
                            obj.earnMsg = "\(detailsArray[0].UserInfo![0].GreetingsEn)"
                        }else{
                            obj.desc = detailsArray[0].UserInfo![0].UpgradeMsgAr
                            obj.smileLevel = "\(detailsArray[0].UserInfo![0].BadgeNameAr)"
                            obj.earnMsg = "\(detailsArray[0].UserInfo![0].GreetingsAr)"
                        }
                        //couponBtn_Tapped(couponBtn!)
                        obj.mySmiles = detailsArray[0].UserInfo![0].LoyaltyPoints
                        if detailsArray[0].UserInfo![0].BadgeExpiry != ""{
                            let date = detailsArray[0].UserInfo![0].BadgeExpiry.split(separator: ".")[0]
                            obj.validThru = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .MMYYYY))"
                        }else{
                            obj.validThru = ""
                        }
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }else{
            print("")
        }
        bottomPopUpView.isHidden = true

    }
    @IBAction func FAQBtn_Tapped(_ sender: Any) {
    }
    @IBAction func TAndCBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms and Conditions", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            Obj.url = "https://drcafestore.com/Shopping/AppRewards"
        }else{
            Obj.url = "https://drcafestore.com/ar/Shopping/AppRewards"
        }
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
        bottomPopUpView.isHidden = true
    }
    @IBAction func joinNowBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = LoginVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func popUpBtn_Tapped(_ sender: Any) {
        if bottomPopUpView.isHidden == true{
            bottomPopUpView.isHidden = false
        }else{
            bottomPopUpView.isHidden = true
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func rewardSmileBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RedeemSmileVC") as! RedeemSmileVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.redeemSmileVCDelegate = self
        obj.badgeNum = detailsArray[0].UserInfo![0].BadgeId
        obj.redeemPoints = detailsArray[0].UserInfo![0].LoyaltyPoints
        obj.redeemSmiles = detailsArray[0].UserInfo![0].LoyaltySmiles
        obj.minWalletAmount = Double(detailsArray[0].UserInfo![0].MinWalletLimit)
        obj.modalPresentationStyle = .overFullScreen
        self.present(obj, animated: true, completion: nil)
    }
    @IBAction func couponBtn_Tapped(_ sender: Any) {
        isCoupon = true
        isDeals = false
        detailsTVTop.constant = 0
        couponDetailsTVHeight.constant = 5
        detailsTVHeight.constant = 0
        couponDetailsTV.isHidden = false
        noDealsAvailableLbl.isHidden = true
        detailsTV.isHidden = true
        couponImgBGView.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.5411764706, blue: 0.7529411765, alpha: 1)
        dealsImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        historyImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        if self.detailsArray.count > 0 { // Add - Ravi Nadendla
            DispatchQueue.main.async {
                if self.detailsArray[0].Promotions!.count > 0 || self.detailsArray[0].UsedPromotions!.count > 0 || self.detailsArray[0].ExpiredPromotions!.count > 0{
                        self.couponDetailsTV.reloadData()
                }else{
                    self.couponDetailsTV.isHidden = true
                    self.couponDetailsTVHeight.constant = 0
                    self.noDealsAvailableLbl.isHidden = false
                    self.noDealsAvailableLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!
                }
            }
        }else{
            self.getDetailsService()
        }
    }
    @IBAction func dealsBtn_Tapped(_ sender: Any) {
        isCoupon = false
        isDeals = true
        couponImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        historyImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        dealsImgBGView.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.5411764706, blue: 0.7529411765, alpha: 1)
        
        detailsTVTop.constant = 0
        couponDetailsTVHeight.constant = 5
        detailsTVHeight.constant = 0
        couponDetailsTV.isHidden = false
        noDealsAvailableLbl.isHidden = true
        detailsTV.isHidden = true
        DispatchQueue.main.async {
            if self.dealsArray.count > 0{
                self.couponDetailsTV.reloadData()
            }else{
                self.getDealsService()
            }
        }
    }
    @IBAction func historyBtn_Tapped(_ sender: Any) {
        isCoupon = false
        isDeals = false
        detailsTVTop.constant = 15
        couponDetailsTVHeight.constant = 0
        detailsTVHeight.constant = 1
        detailsTV.isHidden = false
        noDealsAvailableLbl.isHidden = true
        couponDetailsTV.isHidden = true
        couponImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        dealsImgBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        historyImgBGView.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.5411764706, blue: 0.7529411765, alpha: 1)
        DispatchQueue.main.async {
            if self.histryDetailsArray.count > 0{
                self.detailsTV.reloadData()
            }else{
                self.getHistoryDetailsService()
            }
        }
    }
    func getDetailsService(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2]
        OrderModuleServices.DCSmileService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.couponDetailsTV.isHidden = true
                self.noDealsAvailableLbl.isHidden = false
                self.noDealsAvailableLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Coupons Available", value: "", table: nil))!
                self.detailsTV.isHidden = true
            }else{
                self.detailsArray = [data.Data!]
                DispatchQueue.main.async {
                    self.couponCategoryArray.removeAll()
                    self.couponCategoryArray = [["Name":"Available","isExpand":false]]
                    if self.detailsArray.count > 0{
                        if self.detailsArray[0].Promotions!.count > 0{
                            self.couponCategoryArray.insert(["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available", value: "", table: nil))!,"isExpand":true], at: self.couponCategoryArray.count)
                        }
                        if self.detailsArray[0].UsedPromotions!.count > 0{
                            self.couponCategoryArray.insert(["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Used", value: "", table: nil))!,"isExpand":false], at: self.couponCategoryArray.count)
                        }
                        if self.detailsArray[0].ExpiredPromotions!.count > 0{
                            self.couponCategoryArray.insert(["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expired", value: "", table: nil))!,"isExpand":false], at: self.couponCategoryArray.count)
                        }
                    }
                    self.couponCategoryArray.remove(at: 0)
                    if self.couponCategoryArray.count > 0{
                        self.couponCategoryArray[0]["isExpand"] = true
                    }
                    self.myScrollView.layoutIfNeeded()
                    self.couponDetailsTV.reloadData()
                    self.couponDetailsTVHeight.constant = self.couponDetailsTV.contentSize.height
                    //self.myScrollView.layoutIfNeeded()
                    self.allData()
                 }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getHistoryDetailsService(){
        if pageNumber == 1 {
           ANLoader.showLoading("", disableUI: false)
        }
        let dic:[String:Any] = ["UserId":UserDef.getUserId(), "PageSize":10, "PageNumber":pageNumber, "Action":1, "RequestBy":2]
        OrderModuleServices.DCSmileHistoryService(dic: dic, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    self.noDealsAvailableLbl.isHidden = false
                    self.noDealsAvailableLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
                    self.detailsTV.isHidden = true
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if self.pageNumber == 1 {
                    SaveAddressClass.getDCSmileHistoryArray.removeAll()
                }
                if data.Data!.count > 0{
                    SaveAddressClass.getDCSmileHistoryArray.append(contentsOf: data.Data!)
                    self.totalRows = data.Data![0].TotalRows
                }
                self.histryDetailsArray.removeAll()
                self.histryDetailsArray = SaveAddressClass.getDCSmileHistoryArray
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    self.detailsTV.reloadData()
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getDealsService(){
        let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2]
        OrderModuleServices.DCSmileDealsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.couponDetailsTV.isHidden = true
                self.noDealsAvailableLbl.isHidden = false
                self.detailsTV.isHidden = true
            }else{
                self.dealsArray = data.Data!
                DispatchQueue.main.async {
                    if data.Data!.count > 0{
                        self.myScrollView.layoutIfNeeded()
                        self.couponDetailsTV.reloadData()
                        self.couponDetailsTVHeight.constant = self.couponDetailsTV.contentSize.height
                    }else{
                        self.couponDetailsTV.isHidden = true
                        self.couponDetailsTVHeight.constant = 0
                        self.noDealsAvailableLbl.isHidden = false
                        self.noDealsAvailableLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
                    }
                 }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func allData(){
        if isUserLogIn() == true{
            self.celebrationAnimationImg.isHidden = false
            startAnimation()
        }else{
            self.celebrationAnimationImg.isHidden = true
        }
        if detailsArray.count > 0{
            let dic = detailsArray[0]
            if dic.UserInfo!.count > 0{
                couponBtn_Tapped(couponBtn!)
                let myPoints = round(dic.UserInfo![0].LoyaltyPoints)
                //mySmilesLbl.text = "\(Int(myPoints).intWithCommas())"
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                nf.locale = Locale(identifier: "EN")
                mySmilesLbl.text = nf.string(from: NSNumber(value: Int(myPoints)))
                if dic.UserInfo![0].BadgeExpiry != ""{
                    let date = dic.UserInfo![0].BadgeExpiry.components(separatedBy: ".")[0]
                    validThruLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .MMYYYY))"
                }else{
                    validThruLbl.text = ""
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    smileLevelLbl.text = "\(dic.UserInfo![0].BadgeNameEn)"
                    earnMoreNameLbl.text = "\(dic.UserInfo![0].UpgradeMsgEn)"
                }else{
                    smileLevelLbl.text = "\(dic.UserInfo![0].BadgeNameAr)"
                    earnMoreNameLbl.text = "\(dic.UserInfo![0].UpgradeMsgAr)"
                }
                if dic.UserInfo![0].PointsExpiryEn != nil{
                    if dic.UserInfo![0].PointsExpiryEn != ""{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            expiryMsgLbl.text = "\(dic.UserInfo![0].PointsExpiryEn!)"
                        }else{
                            expiryMsgLbl.text = "\(dic.UserInfo![0].PointsExpiryAr!)"
                        }
                        expiryMsgViewTop.constant = 15
                        expiryMsgViewHeight.constant = 150
                    }else{
                        expiryMsgViewTop.constant = 0
                        expiryMsgViewHeight.constant = 0
                    }
                }else{
                    expiryMsgViewTop.constant = 0
                    expiryMsgViewHeight.constant = 0
                }
                self.validThruNameLbl.isHidden = false
                self.validThruLbl.isHidden = false
                if dic.UserInfo![0].BadgeId == 1{
                    rewardCardBGImage.image = UIImage(named: "dcRewards Coffee Lover")
                    redeemSmileBGImg.image = UIImage(named: "smile_rect_standard")
                    self.convertPointsNameLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
                    self.dcPayNameLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
                    self.mySmilesLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.validThruNameLbl.isHidden = true
                    self.validThruLbl.isHidden = true
                }else if dic.UserInfo![0].BadgeId == 2{
                    rewardCardBGImage.image = UIImage(named: "dcRewards Coffee Expert")
                    redeemSmileBGImg.image = UIImage(named: "smile_rect_platinum")
                    self.convertPointsNameLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
                    self.dcPayNameLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
                    self.mySmilesLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }else if dic.UserInfo![0].BadgeId == 3{
                    rewardCardBGImage.image = UIImage(named: "dcRewards Coffee Master")
                    redeemSmileBGImg.image = UIImage(named: "smile_rect_gold")
                    self.convertPointsNameLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
                    self.dcPayNameLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
                    self.mySmilesLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }else{
                    rewardCardBGImage.image = UIImage(named: "dcRewards Coffee Guru")
                    redeemSmileBGImg.image = UIImage(named: "smile_rect_elite")
                    self.convertPointsNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.dcPayNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.mySmilesLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
        }
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String) -> NSMutableAttributedString{
        let boldAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 16.0)!
           ]
        let regularAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 16.0)!
           ]
        let regularText = NSAttributedString(string: text1, attributes: regularAttribute)
        let boldText1 = NSAttributedString(string: text2, attributes: boldAttribute)
        let newString = NSMutableAttributedString()
        newString.append(regularText)
        newString.append(boldText1)
        return newString
    }
}
@available(iOS 13.0, *)
extension DCSmileVC: UITableViewDelegate, UITableViewDataSource, DCSmileCouponsTVCellDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if detailsArray.count > 0{
            if isCoupon == true{
                return couponCategoryArray.count
            }else{
                return 1
            }
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if detailsArray.count > 0{
            if isCoupon == true{
                if couponCategoryArray.count > 0{
                    if couponCategoryArray[section]["isExpand"] as! Bool == false{
                        return 1
                    }else{
                        if couponCategoryArray[section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available", value: "", table: nil))!{
                            if detailsArray[0].Promotions!.count > 0{
                                return detailsArray[0].Promotions!.count + 1
                            }else{
                                return 1
                            }
                        }else if couponCategoryArray[section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expired", value: "", table: nil))!{
                            if detailsArray[0].ExpiredPromotions!.count > 0{
                                return detailsArray[0].ExpiredPromotions!.count + 1
                            }else{
                                return 1
                            }
                        }else{
                            if detailsArray[0].UsedPromotions!.count > 0{
                                return detailsArray[0].UsedPromotions!.count + 1
                            }else{
                                return 1
                            }
                        }
                    }
                }else{
                    return 0
                }
            }else{
                if isDeals == true{
                    return dealsArray.count
                }else{
//                    if self.histryDetailsArray.count > 0{
//                        if totalRows > self.histryDetailsArray.count {
//                            return self.histryDetailsArray.count + 1
//                        }else{
//                            return self.histryDetailsArray.count
//                        }
//                    }else{
//                        return 0
//                    }
                    if totalRows > self.histryDetailsArray.count {
                        return self.histryDetailsArray.count + 1
                    }else{
                        return self.histryDetailsArray.count
                    }
                }
            }
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isCoupon == true{
            if indexPath.row == 0{
                return 60
            }else{
                return UITableView.automaticDimension
            }
        }else{
            if isDeals == true{
                return UITableView.automaticDimension
            }else{
                if self.histryDetailsArray.count == indexPath.row {
                   return 50
                }else{
                    return 69
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(cell.frame.size.height, self.tableViewHeight)
        self.tableViewHeight += cell.frame.size.height
        if isCoupon == true{
            couponDetailsTVHeight.constant = self.tableViewHeight
        }else{
            if isDeals == true{
                couponDetailsTVHeight.constant = self.tableViewHeight
            }else{
                detailsTVHeight.constant = self.tableViewHeight
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isCoupon == true{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Header Details
                couponDetailsTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CouponHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CouponHeaderTVCell", value: "", table: nil))!)
                let headerCell = couponDetailsTV.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CouponHeaderTVCell", value: "", table: nil))!, for: indexPath) as! CouponHeaderTVCell
                
                headerCell.nameLbl.text = "\(couponCategoryArray[indexPath.section]["Name"]!)"
                headerCell.nameLbl.textColor = .black
                headerCell.nameLbl.font = UIFont(name: "Avenir Medium", size: 16)
                
                if couponCategoryArray[indexPath.section]["isExpand"] as! Bool == false{
                    headerCell.dropDownBtn.setImage(UIImage(named: "Down Arrow"), for: .normal)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.dropDownBtn.setImage(UIImage(named: "Right Arrow-1"), for: .normal)
                    }else{
                        headerCell.dropDownBtn.setImage(UIImage(named: "Left Arrow"), for: .normal)
                    }
                }
                
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(dropDownBtn_Tapped(_:)), for: .touchUpInside)
                
                cell = headerCell
            }else{
                //MARK: SubCell Details
                couponDetailsTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileCouponsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileCouponsTVCell", value: "", table: nil))!)
                let subCell = couponDetailsTV.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileCouponsTVCell", value: "", table: nil))!, for: indexPath) as! DCSmileCouponsTVCell
                
                //if detailsArray[0].Promotions!.count > indexPath.row{
                var dic:DCSmilePromotionsDetailsModel!
                if detailsArray.count > 0{
                    if couponCategoryArray[indexPath.section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available", value: "", table: nil))!{
                        dic = detailsArray[0].Promotions![indexPath.row - 1]
                        subCell.validForNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Valid Till", value: "", table: nil))!
                        if dic.EndDate != ""{
                            let date = dic.EndDate.components(separatedBy: ".")[0]
                            subCell.validLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                            let voucherEndDate = DateConverts.convertStringToDate(date: date, dateformatType: .dateTtime)
                            let todayDate = DateConverts.convertDateToDateFormate(date: Date(), dateformatType: .dateTtime)
                            if voucherEndDate >= todayDate{
                                subCell.validLbl.textColor = .darkGray
                            }else{
                                subCell.validLbl.textColor = #colorLiteral(red: 0.6901960784, green: 0, blue: 0.1254901961, alpha: 1)
                            }
                        }else{
                            subCell.validLbl.text = ""
                        }
                    }else if couponCategoryArray[indexPath.section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expired", value: "", table: nil))!{
                        dic = detailsArray[0].ExpiredPromotions![indexPath.row - 1]
                        subCell.validForNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expired on", value: "", table: nil))!
                        if dic.EndDate != ""{
                            let date = dic.EndDate.components(separatedBy: ".")[0]
                            subCell.validLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                            let voucherEndDate = DateConverts.convertStringToDate(date: date, dateformatType: .dateTtime)
                            let todayDate = DateConverts.convertDateToDateFormate(date: Date(), dateformatType: .dateTtime)
                            if voucherEndDate >= todayDate{
                                subCell.validLbl.textColor = .darkGray
                            }else{
                                subCell.validLbl.textColor = #colorLiteral(red: 0.6901960784, green: 0, blue: 0.1254901961, alpha: 1)
                            }
                        }else{
                            subCell.validLbl.text = ""
                        }
                    }else{
                        dic = detailsArray[0].UsedPromotions![indexPath.row - 1]
                        subCell.validForNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Used on", value: "", table: nil))!
                        subCell.validLbl.text = ""
                        if let _ = dic.ConsumedDate {
                            if dic.ConsumedDate! != ""{
                                let date = dic.ConsumedDate!.components(separatedBy: ".")[0]
                                subCell.validLbl.text = DateConverts.convertStringToStringDates(inputDateStr: "\(date)", inputDateformatType: .dateTtime, outputDateformatType: .date)
                                let voucherEndDate = DateConverts.convertStringToDate(date: date, dateformatType: .dateTtime)
                                let todayDate = DateConverts.convertDateToDateFormate(date: Date(), dateformatType: .dateTtime)
                                if voucherEndDate >= todayDate{
                                    subCell.validLbl.textColor = .darkGray
                                }else{
                                    subCell.validLbl.textColor = #colorLiteral(red: 0.6901960784, green: 0, blue: 0.1254901961, alpha: 1)
                                }
                            }
                        }
                    }
                    subCell.nameLbl.text = dic.PromoCode
                                    
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        subCell.descLbl.text = dic.DescriptionEn
                    }else{
                        subCell.descLbl.text = dic.DescriptionAr
                    }
                    subCell.availableDrinksLbl.text = "\(dic.Quantity)"
                    if dic.Quantity > 0{
                        subCell.availableDrinksViewHeight.constant = 59
                    }else{
                        subCell.availableDrinksViewHeight.constant = 0
                    }
                        
                    //Image
                    var ImgStr:NSString!
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageEn)" as NSString
                    }else{
                        ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(dic.ImageAr)" as NSString
                    }
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    subCell.img.kf.setImage(with: url)
                }
                
                //}
                
                if indexPath.row%2 == 0{
                    subCell.BGView.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                }else{
                    subCell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                subCell.availableNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quantity", value: "", table: nil))!
                subCell.moreInfoBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "More Info", value: "", table: nil))!, for: .normal)
                
                subCell.delegate = self
                subCell.tableView = couponDetailsTV
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.couponDetailsTV.layoutIfNeeded()
                self.couponDetailsTVHeight.constant = tableView.contentSize.height + 50
            }
            
            cell.selectionStyle = .none
            return cell
        }else{
            if isDeals == true{
                couponDetailsTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCDealsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCDealsTVCell", value: "", table: nil))!)
                let cell = couponDetailsTV.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCDealsTVCell", value: "", table: nil))!, for: indexPath) as! DCDealsTVCell
                
                cell.pointsLbl.text = "\(dealsArray[indexPath.row].Value) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Points", value: "", table: nil))!)"
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.titleLbl.text = dealsArray[indexPath.row].NameEn
                    cell.descLbl.text = dealsArray[indexPath.row].DescriptionEn
                }else{
                    cell.titleLbl.text = dealsArray[indexPath.row].NameAr
                    cell.descLbl.text = dealsArray[indexPath.row].DescriptionAr
                }
                
                //Deal Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dealsArray[indexPath.row].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.BGImage.kf.setImage(with: url)

                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.couponDetailsTV.layoutIfNeeded()
                    self.couponDetailsTVHeight.constant = tableView.contentSize.height + 10
                }
                
                cell.selectionStyle = .none
                return cell
            }else{
                if self.histryDetailsArray.count == indexPath.row && totalRows > self.histryDetailsArray.count {
                    detailsTV.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell") as! LoaderCell
                    cell.activityIndicatorView.startAnimating()
                    cell.selectionStyle = .none
                    return cell
                }else{
                    detailsTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileHistoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileHistoryTVCell", value: "", table: nil))!)
                    let cell = detailsTV.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DCSmileHistoryTVCell", value: "", table: nil))!) as! DCSmileHistoryTVCell
                    
                    if indexPath.row%2 == 0{
                        cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    }else{
                        cell.BGView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
                    }
                    
                    if histryDetailsArray.count > 0{
                        cell.idLbl.text = "#\(histryDetailsArray[indexPath.row].InvoiceNo)"
                        if histryDetailsArray[indexPath.row].Operation == "Debit"{
                            //cell.idLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeemed to dr.CAFE PAY", value: "", table: nil))!
                            cell.amountLbl.text = "-\(histryDetailsArray[indexPath.row].Points.withCommas())"
                            cell.amountLbl.textColor = #colorLiteral(red: 0.6901960784, green: 0, blue: 0.1254901961, alpha: 1)
                        }else{
                            //cell.idLbl.text = "#\(histryDetailsArray[indexPath.row].InvoiceNo)"
                            cell.amountLbl.text = "+\(histryDetailsArray[indexPath.row].Points.withCommas())"
                            cell.amountLbl.textColor = #colorLiteral(red: 0, green: 0.4705882353, blue: 0.568627451, alpha: 1)
                        }
                        if histryDetailsArray[indexPath.row].CreatedOn != ""{
                            let createdDate = histryDetailsArray[indexPath.row].CreatedOn.components(separatedBy: ".")[0]
                            cell.dateLbl.text = DateConverts.convertStringToStringDates(inputDateStr: createdDate, inputDateformatType: .dateTtime, outputDateformatType: .date)
                        }else{
                            cell.dateLbl.text = ""
                        }
                        
                        DispatchQueue.main.async {
                            self.detailsTV.layoutIfNeeded()
                            self.myScrollView.layoutIfNeeded()
                            self.detailsTVHeight.constant = tableView.contentSize.height + 50
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.detailsTV.layoutIfNeeded()
                            self.myScrollView.layoutIfNeeded()
                            self.detailsTVHeight.constant = 0
                        }
                    }
                    return cell
                }
            }
        }
    }
    func MoreInfoButtonTapped(cell: DCSmileCouponsTVCell, MoreInfoBtn: Bool, tableView: UITableView) {
        let indexPath = self.couponDetailsTV.indexPath(for: cell)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj:RewardVC = mainStoryBoard.instantiateViewController(withIdentifier: "RewardVC") as! RewardVC
        if couponCategoryArray[indexPath!.section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Available", value: "", table: nil))!{
            obj.detailsArray = [detailsArray[0].Promotions![indexPath!.row-1]]
            obj.expireCoupon = false
        }else if couponCategoryArray[indexPath!.section]["Name"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expired", value: "", table: nil))!{
            obj.detailsArray = [detailsArray[0].ExpiredPromotions![indexPath!.row-1]]
            obj.expireCoupon = true
        }else{
            obj.detailsArray = [detailsArray[0].UsedPromotions![indexPath!.row-1]]
            obj.expireCoupon = true
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCoupon == true{
            if indexPath.row == 0 {
                //MARK: Coupon Expand
                if self.couponCategoryArray[indexPath.section]["isExpand"] as! Bool == true{
                    self.couponCategoryArray[indexPath.section]["isExpand"] = false
                }else{
                    self.couponCategoryArray[indexPath.section]["isExpand"] = true
                }
                couponDetailsTV.reloadSections([indexPath.section], with: .automatic)
            }else{
                print("Selected")
            }
        }else{
            if isDeals == true{
                print("Selected")
            }else{
                print("Selected")
            }
            
        }
    }
    @objc func dropDownBtn_Tapped(_ sender: UIButton){
        if self.couponCategoryArray[sender.tag]["isExpand"] as! Bool == true{
            self.couponCategoryArray[sender.tag]["isExpand"] = false
        }else{
            self.couponCategoryArray[sender.tag]["isExpand"] = true
        }
        couponDetailsTV.reloadSections([sender.tag], with: .automatic)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        DispatchQueue.main.async {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height + 650
            let guide = self.view.safeAreaLayoutGuide
            let height = guide.layoutFrame.size.height
            print(scrollView.contentOffset.y + scrollView.frame.size.height)
            print(scrollView.contentSize.height)
            print(height)
            print(guide.layoutFrame)
            print(self.view.frame.size.height)
            print(self.view.frame.size.width)
            if (bottomEdge >= scrollView.contentSize.height) {
                if self.isCoupon == false{
                    if self.totalRows > self.histryDetailsArray.count {
                        self.pageNumber = self.pageNumber + 1
                      self.getHistoryDetailsService()
                    }
                }else{
                    print("None")
                }
            }
        }
    }
}
@available(iOS 13.0, *)
extension DCSmileVC : RedeemSmileVCDelegate{
    func didTapReedem() {
        self.getDetailsService()
        getHistoryDetailsService()
    }
}
extension UIView{
func animationShow(){

        UIView.animate(withDuration: 2, delay: 5, options: [.transitionFlipFromBottom],
                       animations: {
                        
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animationHide(){
        UIView.animate(withDuration: 1, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()

        },  completion: {(_ completed: Bool) -> Void in
        self.isHidden = true
            })
    }
}
