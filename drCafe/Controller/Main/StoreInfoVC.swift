//
//  StoreInfoVC.swift
//  drCafe
//
//  Created by Devbox on 22/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

@available(iOS 13.0, *)
class StoreInfoVC: UIViewController, CLLocationManagerDelegate,  MKMapViewDelegate, GMSMapViewDelegate {

    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var V12Lbl: UILabel!
    @IBOutlet weak var V12LineLbl: UILabel!
    @IBOutlet weak var storeAddressLbl: UILabel!
    
    @IBOutlet weak var storeImgsCollectionView: UICollectionView!
    @IBOutlet weak var storeImgsCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var storeRateView: FloatRatingView!
    @IBOutlet weak var ratingLbl: UILabel!
    
    @IBOutlet weak var storeStatusMessageLbl: UILabel!
    @IBOutlet weak var storeStatusMessageLblHeight: NSLayoutConstraint!
    @IBOutlet weak var openingHoursTableView: UITableView!
    @IBOutlet weak var openingHoursTVHeight: NSLayoutConstraint!
    @IBOutlet weak var openingTwentyFourHoursImg: UIImageView!
    @IBOutlet weak var openingHoursTVBGView: UIView!
    
    @IBOutlet weak var mobileNumLbl: UILabel!
    
    @IBOutlet weak var orderNowBtn: CustomButton!
    @IBOutlet weak var orderNowBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet var orderTypeView: UIView!
    @IBOutlet weak var orderTypeDineInBtn: UIButton!
    @IBOutlet weak var orderTypePickUpBtn: UIButton!
    @IBOutlet weak var orderTypeDeliveryBtn: UIButton!
    
    @IBOutlet weak var callNameLbl: UILabel!
    @IBOutlet weak var getNameLbl: UILabel!
    @IBOutlet weak var directionsNameLbl: UILabel!
    @IBOutlet weak var shareMyLocationNameLbl: UILabel!
    @IBOutlet weak var inviteFriendsNameLbl: UILabel!
    @IBOutlet weak var addAsFavouriteNameLbl: UILabel!
    @IBOutlet weak var rateTheStoreNameLbl: UILabel!
    @IBOutlet weak var openingsHoursNameLbl: UILabel!
    
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var orderTypeCV: UICollectionView!
    @IBOutlet weak var orderTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var storeTypeNameLbl: UILabel!
    @IBOutlet weak var storeTypeCV: UICollectionView!
    @IBOutlet weak var storeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var facilityNameLbl: UILabel!
    @IBOutlet weak var facilityCV: UICollectionView!
    @IBOutlet weak var facilityViewHeight: NSLayoutConstraint!
    @IBOutlet weak var segmentNameLbl: UILabel!
    @IBOutlet weak var segmentCV: UICollectionView!
    @IBOutlet weak var segmentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    
    var popover = Popover()
    
    var storeInfoArray = [StoreInfoDetailsModel]()
    var storeId:Int!
    var storeDistance:Double = 0

    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var storeLatitude = Double()
    var storeLongitute = Double()
    var rotation:Double = 0
    
    var whereObj = 0
    
    var orderType = 0
    var isShiftOpen:Bool!
    var isBusy:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        orderTypeCV.delegate = self
        orderTypeCV.dataSource = self
        storeTypeCV.delegate = self
        storeTypeCV.dataSource = self
        facilityCV.delegate = self
        facilityCV.dataSource = self
        segmentCV.delegate = self
        segmentCV.dataSource = self
        
        self.openingTwentyFourHoursImg.isHidden = true
        
        orderTypeCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        storeTypeCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        facilityCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        segmentCV.register(UINib(nibName: "StoresFilterCVCell", bundle: nil), forCellWithReuseIdentifier: "StoresFilterCVCell")
        
        openingHoursTableView.delegate = self
        openingHoursTableView.dataSource = self

        storeImgsCollectionView.delegate = self
        storeImgsCollectionView.dataSource = self
        
        self.getStoreDetailsService()
        
        //location Delegates
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            storeImgsCollectionView.semanticContentAttribute = .forceLeftToRight
            orderTypeCV.semanticContentAttribute = .forceLeftToRight
            storeTypeCV.semanticContentAttribute = .forceLeftToRight
            facilityCV.semanticContentAttribute = .forceLeftToRight
            segmentCV.semanticContentAttribute = .forceLeftToRight
        }else{
            storeImgsCollectionView.semanticContentAttribute = .forceRightToLeft
            orderTypeCV.semanticContentAttribute = .forceRightToLeft
            storeTypeCV.semanticContentAttribute = .forceRightToLeft
            facilityCV.semanticContentAttribute = .forceRightToLeft
            segmentCV.semanticContentAttribute = .forceRightToLeft
        }

        self.orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderType", value: "", table: nil))!
        self.storeTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Type", value: "", table: nil))!
        self.facilityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Facility", value: "", table: nil))!
        self.segmentNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Segment", value: "", table: nil))!
        self.getNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get", value: "", table: nil))!
        self.callNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Call", value: "", table: nil))!
        self.directionsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Directions", value: "", table: nil))!
        self.shareMyLocationNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Share My Location", value: "", table: nil))!
        self.inviteFriendsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invite Friends", value: "", table: nil))!
        self.rateTheStoreNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate the store", value: "", table: nil))!
        self.openingsHoursNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Opening Hours", value: "", table: nil))!
        self.orderTypeDineInBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Dine In", value: "", table: nil))!, for: .normal)
        self.orderTypePickUpBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!, for: .normal)
        self.orderTypeDeliveryBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!, for: .normal)
        self.orderNowBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Now", value: "", table: nil))!, for: .normal)

        mapView.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        if whereObj == 1{
//            orderNowBtnHeight.constant = 35
//        }else{
           // orderNowBtnHeight.constant = 0
        //}
        
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            openingsHoursNameLbl.textColor = .white
            storeNameLbl.textColor = .white
            V12Lbl.textColor = .white
            V12LineLbl.textColor = .white
            storeAddressLbl.textColor = .white
            orderNowBtn.backgroundColor = #colorLiteral(red: 0.9612344848, green: 0.9594177604, blue: 0.959405601, alpha: 1)
            orderNowBtn.setTitleColor(#colorLiteral(red: 0.3535414934, green: 0.3579595685, blue: 0.3621532321, alpha: 1), for: .normal)
            self.storeStatusMessageLbl.textColor = .white
        } else {
            // User Interface is Light
            openingsHoursNameLbl.textColor = .darkGray
            storeNameLbl.textColor = .black
            V12Lbl.textColor = .black
            V12LineLbl.textColor = .black
            storeAddressLbl.textColor = .black
            orderNowBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            orderNowBtn.setTitleColor(.white, for: .normal)
            orderTypePickUpBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            orderTypeDineInBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        }
    }
    //MARK: Order Now Button Action
    @IBAction func orderNowBtn_Tapped(_ sender: UIButton) {
        //let startPoint = CGPoint(x: sender.frame.midX, y: 238 - myScrollView.contentOffset.y)
        var startPoint = CGPoint(x: sender.frame.midX, y: sender.frame.midY + 95)
        if view.frame.width > 380{
            startPoint = CGPoint(x: sender.frame.midX, y: sender.frame.midY + 120)
        }else{
            startPoint = CGPoint(x: sender.frame.midX, y: sender.frame.midY + 95)
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
            if orderTypeArr.count == 2{
                self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 150, height: 80)
                popover.show(self.orderTypeView, point: startPoint)
            }else if orderTypeArr.count == 1{
                //self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 80, height: 80)
                if orderTypeArr[0].NameEn == "PickUp"{
//                    self.orderTypeDineInBtn.isHidden = true
//                    self.orderTypePickUpBtn.isHidden = false
                    self.orderTypePickUpBtn_Tapped(orderTypePickUpBtn!)
                }else if orderTypeArr[0].NameEn == "DineIn"{
//                    self.orderTypeDineInBtn.isHidden = false
//                    self.orderTypePickUpBtn.isHidden = true
                    self.orderTypeDineInBtn_Tapped(orderTypeDineInBtn!)
                }
            }else{
                self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 150, height: 80)
                popover.show(self.orderTypeView, point: startPoint)
            }
        }else{
            let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
            if orderTypeArr.count == 2{
                self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 190, height: 110)
                popover.show(self.orderTypeView, point: startPoint)
            }else if orderTypeArr.count == 1{
                //self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 100, height: 110)
                if orderTypeArr[0].NameEn == "PickUp"{
//                    self.orderTypeDineInBtn.isHidden = true
//                    self.orderTypePickUpBtn.isHidden = false
                    self.orderTypePickUpBtn_Tapped(orderTypePickUpBtn!)
                }else if orderTypeArr[0].NameEn == "DineIn"{
//                    self.orderTypeDineInBtn.isHidden = false
//                    self.orderTypePickUpBtn.isHidden = true
                    self.orderTypeDineInBtn_Tapped(orderTypeDineInBtn!)
                }
            }else{
                self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 190, height: 110)
                popover.show(self.orderTypeView, point: startPoint)
            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Get Store Details Service
    func getStoreDetailsService(){
        let dic = ["UserId":"\(UserDef.getUserId())", "StoreId" : storeId!, "RequestBy":2] as [String : Any]
        OrderModuleServices.GetStoreInfoService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeInfoArray = [data.Data!]
                DispatchQueue.main.async {
                    self.storeLatitude = Double(self.storeInfoArray[0].Latitude)!
                    self.storeLongitute = Double(self.storeInfoArray[0].Longitude)!
                    if self.storeInfoArray[0].StoreImages!.count > 0{
                        self.storeImgsCollectionViewHeight.constant = 200
                        self.storeImgsCollectionView.reloadData()
                    }else{
                        self.storeImgsCollectionViewHeight.constant = 0
                    }
                    if self.storeInfoArray[0].StoreStatus == true{
                        if self.storeInfoArray[0].Shifts!.count > 0{
                            if self.storeInfoArray[0].is24x7 == true{
                                self.openingTwentyFourHoursImg.isHidden = false
                                self.openingHoursTVBGView.isHidden = true
                                self.orderNowBtnHeight.constant = 35
                            }else{
                                self.openingHoursTVBGView.isHidden = false
                                self.openingHoursTableView.reloadData()
                                if self.storeInfoArray[0].Kilometers >=  self.storeDistance {
                                    self.orderNowBtnHeight.constant = 35
                                }else{
                                    self.orderNowBtnHeight.constant = 0
                                }
                            }
                        }else{
                            self.openingHoursTVHeight.constant = 0
                            self.orderNowBtnHeight.constant = 0
                            self.openingHoursTVBGView.isHidden = true
                        }
                    }else{
                        self.openingHoursTVHeight.constant = 0
                        self.storeStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Closed", value: "", table: nil))!
                        self.storeStatusMessageLblHeight.constant = 22
                        self.orderNowBtnHeight.constant = 0
                        self.openingHoursTVBGView.isHidden = true
                    }
                    self.initGoogleMaps()
                    self.AllData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Data
    func AllData(){
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.storeNameLbl.text = storeInfoArray[0].NameEn
            self.storeAddressLbl.text = storeInfoArray[0].AddressEn
        }else{
            self.storeNameLbl.text = storeInfoArray[0].NameAr
            self.storeAddressLbl.text = storeInfoArray[0].AddressAr
        }
        self.mobileNumLbl.text = storeInfoArray[0].MobileNo
        
        self.V12Lbl.isHidden = true
        self.V12LineLbl.isHidden = true
        
        storeRateView.rating = Double(storeInfoArray[0].StoreRatings)
        ratingLbl.text = "\(storeInfoArray[0].StoreRatings)/5"
        
        self.storeTypeCV.reloadData()
        self.orderTypeCV.reloadData()
        self.facilityCV.reloadData()
        
        //Favourite
        if storeInfoArray[0].Favourite == true{
            favBtn.setImage(UIImage(named: "Favourite Store icon"), for: .normal)
        }else{
            favBtn.setImage(UIImage(named: "img_fav_unselected"), for: .normal)
        }
        
        if storeInfoArray[0].Segments!.count > 0{
            self.segmentCV.reloadData()
        }else{
            self.segmentViewHeight.constant = 0
        }
        if storeInfoArray[0].Facilities!.count > 0{
            self.facilityCV.reloadData()
        }else{
            self.facilityViewHeight.constant = 0
        }
        if storeInfoArray[0].OrderTypes!.count > 0{
            self.orderTypeCV.reloadData()
        }else{
            self.orderTypeViewHeight.constant = 0
        }
        if storeInfoArray[0].StoreTypes!.count > 0{
            self.storeTypeCV.reloadData()
        }else{
            self.storeTypeViewHeight.constant = 0
        }
        
        if storeInfoArray[0].Favourite == true{
            self.addAsFavouriteNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Remove as a Favorite", value: "", table: nil))!
        }else{
            self.addAsFavouriteNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add as a Favorite", value: "", table: nil))!
        }
    }
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:storeLatitude, longitude: storeLongitute, zoom: 16.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = false
        self.mapView.settings.myLocationButton = false
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        print ("Rotation:=\(heading.magneticHeading)")
        rotation = heading.magneticHeading
    }
    //MARK: Location ButtonAction
    @IBAction func locationBtn_Tapped(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open((URL(string:
                "comgooglemaps://?saddr=\(latitude),\(longitude)&daddr=\(storeInfoArray[0].Latitude),\(storeInfoArray[0].Longitude)&directionsmode=driving")!), options: [:], completionHandler: nil)
        } else {
            print("Can't use comgooglemaps://");
            UIApplication.shared.open((URL(string:
                "https://www.google.co.in/maps/dir/?saddr=\(latitude),\(longitude)&daddr=\(storeInfoArray[0].Latitude),\(storeInfoArray[0].Longitude)")!), options: [:], completionHandler: nil)
        }
    }
    //Order Types Button Actions
    @IBAction func orderTypeDineInBtn_Tapped(_ sender: Any) {
        popover.dismiss()
        if storeInfoArray[0].StoreStatus == true{
            if isShiftOpen == true && isBusy == false || storeInfoArray[0].is24x7 == true && isBusy == false{
                self.selectOrderType(selectOrderType: 1)
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
        }
    }
    @IBAction func orderTypePickUpBtn_Tapped(_ sender: Any) {
        popover.dismiss()
        if storeInfoArray[0].StoreStatus == true{
            if isShiftOpen == true && isBusy == false || storeInfoArray[0].is24x7 == true && isBusy == false{
                self.selectOrderType(selectOrderType: 3)
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
        }
    }
    @IBAction func orderTypeDeliveryBtn_Tapped(_ sender: Any) {
        popover.dismiss()
        if isUserLogIn() == true{
            let orderStatusDetails = storeInfoArray[0].BusySchedule!.filter({$0.OrderTypeId == 4})
            if storeInfoArray[0].StoreStatus == true{
                if orderStatusDetails.count > 0{
                    if isShiftOpen == true && isBusy == false || storeInfoArray[0].is24x7 == true && isBusy == false{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = OrderTypeVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderTypeVC") as! OrderTypeVC
                        obj.whereObj = 2
                        obj.isLogin = false
                        obj.storeId = storeId!
                        obj.storeLatitude = self.storeInfoArray[0].Latitude
                        obj.storeLongitude = self.storeInfoArray[0].Longitude
                        self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
                    }
                }else{
                    print("Order Status Not getting")
                }
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    func selectOrderType(selectOrderType: Int){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.address = self.storeInfoArray[0].NameEn
        }else{
            obj.address = self.storeInfoArray[0].NameAr
        }
        obj.emptyCart = false
        obj.storeID = self.storeInfoArray[0].Id
        obj.selectOrderType = selectOrderType
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Call Button Action
    @IBAction func callBtn_Tapped(_ sender: Any) {
        if storeInfoArray.count > 0{
            let mobile = storeInfoArray[0].MobileNo
            print(mobile.removeWhitespace())
            if let url = URL(string: "tel://\(mobile.removeWhitespace())") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    //MARK: Get Directions Button Action
    @IBAction func getDirectionsBtn_Tapped(_ sender: Any) {
        if storeInfoArray.count > 0{
            if storeInfoArray[0].Latitude != "" && storeInfoArray[0].Longitude != ""{
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    UIApplication.shared.open((URL(string:
                        "comgooglemaps://?saddr\(latitude),\(longitude)=&daddr=\(storeInfoArray[0].Latitude),\(storeInfoArray[0].Longitude)&directionsmode=driving")!), options: [:], completionHandler: nil)
                    
                } else {
                    print("Can't use comgooglemaps://");
                    UIApplication.shared.open((URL(string:
                        "https://www.google.co.in/maps/dir/?saddr\(latitude),\(longitude)=&daddr=\(String(describing: storeInfoArray[0].Latitude)),\(String(describing: storeInfoArray[0].Longitude))")!), options: [:], completionHandler: nil)
                    
                }
            }
        }
    }
    //MARK: Add As Favourite Button Action
    @IBAction func addAsFavouriteBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let dic = ["UserId":"\(UserDef.getUserId())", "StoreId" : storeId!, "RequestBy":2] as [String : Any]
            OrderModuleServices.GetStoreMakeFavouriteService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    DispatchQueue.main.async {
                       if AppDelegate.getDelegate().appLanguage == "English"{
                           Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                       }else{
                           Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                       }
                        if self.favBtn.currentImage == UIImage(named: "img_fav_unselected"){
                            self.favBtn.setImage(UIImage(named: "Favourite Store icon"), for: .normal)
                        }else{
                            self.favBtn.setImage(UIImage(named: "img_fav_unselected"), for: .normal)
                        }
                    }
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Invite Friend Button Action
    @IBAction func inviteFriendsBtn_Tapped(_ sender: Any) {
        if storeInfoArray.count > 0{
            DispatchQueue.main.async() {
                var URLString = "https://www.google.com/maps/search/?api=1&query="
                if self.storeInfoArray[0].Latitude != "" && self.storeInfoArray[0].Longitude != ""{
                    URLString = "https://www.google.com/maps/search/?api=1&query=\(self.storeInfoArray[0].Latitude),\(self.storeInfoArray[0].Longitude)"
                }
                let url = NSURL(string: URLString)
                var storeName = self.storeInfoArray[0].NameEn
                if AppDelegate.getDelegate().appLanguage == "English"{
                    storeName = self.storeInfoArray[0].NameEn
                }else{
                    storeName = self.storeInfoArray[0].NameAr
                }
                let Desc = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "I would love to have a Cup of coffee with you at dr.CAFE Coffee in", value: "", table: nil))!) \(storeName) \(url!)"
                let shareAll = [Desc] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    //MARK: Share My Location Button Action
    @IBAction func shareMyLocationBtn_Tapped(_ sender: Any) {
        if storeInfoArray.count > 0{
            var URLString = "https://www.google.com/maps/search/?api=1&query="
            if storeInfoArray[0].Latitude != "" && storeInfoArray[0].Longitude != ""{
                URLString = "https://www.google.com/maps/search/?api=1&query=\(self.storeInfoArray[0].Latitude),\(self.storeInfoArray[0].Longitude)"
            }
            let url = NSURL(string: URLString)
            var Desc = "\(self.storeInfoArray[0].NameEn) , \(self.storeInfoArray[0].AddressEn) \(url!)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                Desc = "\(self.storeInfoArray[0].NameEn) , \(self.storeInfoArray[0].AddressEn) \(url!)"
            }else{
                Desc = "\(self.storeInfoArray[0].NameAr) , \(self.storeInfoArray[0].AddressAr) \(url!)"
            }
            let shareAll = [Desc] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension StoreInfoVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if storeInfoArray.count > 0{
            return storeInfoArray[0].Shifts!.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        openingHoursTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreInfoTimingsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreInfoTimingsTVCell", value: "", table: nil))!)
        let cell = openingHoursTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreInfoTimingsTVCell", value: "", table: nil))!, for: indexPath) as! StoreInfoTimingsTVCell

        if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 1{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Saturday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 2{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sunday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 3{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Monday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 4{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Tuesday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 5{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Wednesday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 6{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Thursday", value: "", table: nil))!
        }else if storeInfoArray[0].Shifts![indexPath.row].WeekDay == 7{
            cell.dayNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Friday", value: "", table: nil))!
        }
        
        if storeInfoArray[0].Shifts![indexPath.row].IsClose == true{
            cell.fromTimeLbl.text = ""
            cell.toTimeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Closed", value: "", table: nil))!
            cell.fromTimeLbl.isHidden = true
            cell.toNameLbl.isHidden = true
            cell.backGroundView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            if storeInfoArray[0].is24x7 == true{
                cell.fromTimeLbl.text = ""
                cell.toTimeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "24/7 Open", value: "", table: nil))!
                cell.toTimeLbl.font = UIFont(name: "Avenir Book", size: 14.0)
                cell.fromTimeLbl.isHidden = true
                cell.toNameLbl.isHidden = true
            }else{
                cell.fromTimeLbl.text = storeInfoArray[0].Shifts![indexPath.row].StartTime
                cell.toTimeLbl.text = storeInfoArray[0].Shifts![indexPath.row].EndTime
                cell.fromTimeLbl.isHidden = false
                cell.toNameLbl.isHidden = false
            }
            cell.backGroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        openingHoursTableView.layoutIfNeeded()
        openingHoursTVHeight.constant = openingHoursTableView.contentSize.height
        cell.selectionStyle = .none
        
        return cell
    }
}
//MARK: Collectionview delegate methods
@available(iOS 13.0, *)
extension StoreInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if storeInfoArray.count > 0{
            if collectionView == storeImgsCollectionView{
                return storeInfoArray[0].StoreImages!.count
            }else if collectionView == orderTypeCV{
                let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
                return orderTypeArr.count
            }else if collectionView == storeTypeCV{
                return storeInfoArray[0].StoreTypes!.count
            }else if collectionView == facilityCV{
                return storeInfoArray[0].Facilities!.count
            }else{
                return storeInfoArray[0].Segments!.count
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == storeImgsCollectionView{
            storeImgsCollectionView.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
            let cell = storeImgsCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
            cell.percentNumLbl.isHidden = true
            cell.nameLbl.isHidden = true
            cell.percentNumBGView.isHidden = true
            
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(storeInfoArray[0].StoreImages![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.cellBGImg.kf.setImage(with: url)
            
            cell.cellBGView.layer.cornerRadius = 10
            return cell
        }else if collectionView == orderTypeCV{
            let cell = orderTypeCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            let orderTypeArr = storeInfoArray[0].OrderTypes!.filter({$0.Id != 0})
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = orderTypeArr[indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = orderTypeArr[indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            
            if orderTypeArr[indexPath.row].Id == orderType{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.6549019608, green: 0.368627451, blue: 0.3294117647, alpha: 1)
            }else{
                cell.filterImgBGView.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.2862745098, blue: 0.2901960784, alpha: 1)
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(orderTypeArr[indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            orderTypeViewHeight.constant = orderTypeCV.contentSize.height + 42
            return cell
        }else if collectionView == storeTypeCV{
            let cell = storeTypeCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].StoreTypes![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].StoreTypes![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(storeInfoArray[0].StoreTypes![indexPath.row].Image!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            storeTypeViewHeight.constant = storeTypeCV.contentSize.height + 42
            return cell
        }else if collectionView == facilityCV{
            let cell = facilityCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].Facilities![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].Facilities![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            
            //Item Image
            let ImgStr:NSString = "\(displayImages.profileImages.ProfilePath())\(storeInfoArray[0].Facilities![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            
            facilityViewHeight.constant = facilityCV.contentSize.height + 42
            return cell
        }else{
            let cell = segmentCV.dequeueReusableCell(withReuseIdentifier: "StoresFilterCVCell", for: indexPath) as! StoresFilterCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.filterNameLbl.text = storeInfoArray[0].Segments![indexPath.row].NameEn
            }else{
                cell.filterNameLbl.text = storeInfoArray[0].Segments![indexPath.row].NameAr
            }
            cell.filterNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(String(describing: storeInfoArray[0].Segments![indexPath.row].Image!))" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.filterImg.kf.setImage(with: url)
            //cell.filterImg.image = UIImage(named: storeInfoArray[0].Segments![indexPath.row].NameEn)
            
            segmentViewHeight.constant = segmentCV.contentSize.height + 42
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == storeImgsCollectionView{
            let width = storeImgsCollectionView.bounds.width
            let height = storeImgsCollectionView.bounds.height
            //return CGSize(width: width - 60, height: height)
            if storeInfoArray[0].StoreImages!.count == 1{
                return CGSize(width: width, height: height)
            }else{
                return CGSize(width: width - 60, height: height)
            }
        }else{
            let width = collectionView.frame.width
            return CGSize(width: width/4 - 20, height: 110)
        }
    }
}
