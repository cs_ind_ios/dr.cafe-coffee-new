//
//  OrderTypeVC.swift
//  drCafe
//
//  Created by Devbox on 11/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class OrderTypeVC: UIViewController {
    
    @IBOutlet weak var beveragesBGView: UIView!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var beansBGView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet var nameLbls: [UILabel]!
    @IBOutlet weak var orderTypeTableView: UITableView!
    
    var whereObj = 0
    var storeId = 0
    var storeLatitude:String!
    var storeLongitude:String!
    var isLogin:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderTypeTableView.delegate = self
        orderTypeTableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
        }
        nameLbls[0].textColor = .darkGray
        nameLbls[1].textColor = .darkGray
        nameLbls[2].textColor = .black
        nameLbls[3].textColor = .black
        nameLbls[4].textColor = .black
        nameLbls[5].textColor = .black
        beansBGView.backgroundColor = .white
        beveragesBGView.backgroundColor = .white
        
        
        if whereObj == 1 || whereObj == 2{
            tabBarController?.tabBar.isHidden = true
            self.backBtn.isHidden = false
        }else{
            tabBarController?.tabBar.isHidden = false
            self.backBtn.isHidden = true
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isLogin == true{
            LoginVC.instance.whereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Beverage and Food Button Action
    @IBAction func beverageAndFoodBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ManageAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            if whereObj == 2{
                obj.storeLatitude = storeLatitude!
                obj.storeLongitude = storeLongitude!
            }
            obj.whereObj = whereObj
            obj.storeId = storeId
            obj.sectionType = 1
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.whereObj = 17
            if whereObj == 2{
                obj.storeLatitude = storeLatitude!
                obj.storeLongitude = storeLongitude!
            }
            obj.orderTypeObj = whereObj
            obj.storeId = storeId
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Beans and MerchaDise Button Action
    @IBAction func beansAndMerchaDiseBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        obj.whereObj = whereObj
        obj.sectionType = 2
        obj.storeID = 190
        obj.selectOrderType = 4
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK: TableView Delegate Methods
extension OrderTypeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        orderTypeTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderTypeTVCell", value: "", table: nil))!)
        let cell = orderTypeTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OrderTypeTVCell", value: "", table: nil))!, for: indexPath) as! OrderTypeTVCell
        
        if indexPath.row == 0{
            cell.categoryImg.image = UIImage(named: "B&F")
            cell.categoryNameLbl.attributedText = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Beverage", value: "", table: nil))!, text2: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "and", value: "", table: nil))!, text3: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Food", value: "", table: nil))!)
        }else if indexPath.row == 1{
            cell.categoryImg.image = UIImage(named: "B&M")
            cell.categoryNameLbl.attributedText = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Beans", value: "", table: nil))!, text2: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "and", value: "", table: nil))!, text3: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Merchandise", value: "", table: nil))!)
        }else if indexPath.row == 2{
            cell.categoryImg.image = UIImage(named: "Order Subscription")
            cell.categoryNameLbl.attributedText = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscription", value: "", table: nil))!, text2: "", text3: "")
        }else if indexPath.row == 3{
            cell.categoryImg.image = UIImage(named: "Order Bulk")
            cell.categoryNameLbl.attributedText = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Bulk Order", value: "", table: nil))!, text2: "", text3: "")
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if isUserLogIn() == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ManageAddressVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
                if whereObj == 2{
                    obj.storeLatitude = storeLatitude!
                    obj.storeLongitude = storeLongitude!
                }
                obj.whereObj = whereObj
                obj.storeId = storeId
                obj.sectionType = 1
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                obj.whereObj = 17
                if whereObj == 2{
                    obj.storeLatitude = storeLatitude!
                    obj.storeLongitude = storeLongitude!
                }
                obj.orderTypeObj = whereObj
                obj.storeId = storeId
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else if indexPath.row == 1{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = MenuVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            obj.whereObj = whereObj
            obj.sectionType = 2
            obj.storeID = 190
            obj.selectOrderType = 4
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 2{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = SubScriptionVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubScriptionVC") as! SubScriptionVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if indexPath.row == 3{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BulkOrderDetailsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BulkOrderDetailsVC") as! BulkOrderDetailsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String, text3:String) -> NSMutableAttributedString{
        let boldAttribute1 = [
            NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 16.0)!,
              NSAttributedString.Key.foregroundColor: UIColor.black
           ]
        let regularAttribute = [
            NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 14.0)!,
              NSAttributedString.Key.foregroundColor: UIColor.darkGray
           ]
        let boldAttribute2 = [
            NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 16.0)!,
              NSAttributedString.Key.foregroundColor: UIColor.black
           ]
        let boldText1 = NSAttributedString(string: text1, attributes: boldAttribute1)
        let regularText = NSAttributedString(string: text2, attributes: regularAttribute)
        let boldText2 = NSAttributedString(string: text3, attributes: boldAttribute2)
        let newString = NSMutableAttributedString()
        newString.append(boldText1)
        newString.append(regularText)
        newString.append(boldText2)
        return newString
    }
}
