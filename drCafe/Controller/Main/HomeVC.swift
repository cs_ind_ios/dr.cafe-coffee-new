//
//  HomeVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 16/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import TinderSwipeView
import GoogleMaps
import GooglePlaces
import Firebase
import MaterialShowcase
import SwiftUI

@available(iOS 13.0, *)
class HomeVC: UIViewController, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    var checkoutProvider: OPPCheckoutProvider?

    //MARK: Outlets
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var bannerPageController: UIPageControl!
    @IBOutlet weak var topShadowBGView: UIView!
    @IBOutlet weak var topShadowView: UIView!
    
    @IBOutlet weak var orderStatusMessageLbl: UILabel!
    @IBOutlet weak var trackOrderNameLbl: UILabel!
    @IBOutlet weak var orderTrackDetailsView: UIView!
    @IBOutlet weak var trackOrCartImg: UIImageView!
    @IBOutlet weak var orderTrackDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderTrackDetailsViewBottom: NSLayoutConstraint!
    @IBOutlet weak var bellImg: UIImageView!
    
    @IBOutlet weak var orderTypeBGView: UIView!
    @IBOutlet weak var deliveryNameLbl: UILabel!
    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var pickupNameLbl: UILabel!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var dineInBtn: UIButton!
    @IBOutlet weak var dineInNameLbl: UILabel!
    @IBOutlet weak var historyNameLbl: UILabel!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var orderTypeCollectionView: UICollectionView!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var appTitleImg: UIImageView!
    @IBOutlet weak var languageImg: UIImageView!
    @IBOutlet weak var bottomPopUpBtn: UIButton!
    @IBOutlet weak var bottomPopUpView: UIView!
    @IBOutlet weak var bottomPopUpBGView: UIView!
    @IBOutlet weak var freeCoffeeNameLbl: UILabel!
    @IBOutlet weak var shareAppNameLbl: UILabel!
    @IBOutlet weak var contactUsNameLbl: UILabel!
    @IBOutlet weak var feedBackNameLbl: UILabel!
    @IBOutlet weak var changeLanguageBtn: UIButton!
    
    @IBOutlet weak var btmFreeCoffeeNameLbl: UILabel!
    @IBOutlet weak var btmFreeCoffeeNameLblWidth: NSLayoutConstraint!
    @IBOutlet weak var btmFreeCoffeeNameLblLeading: NSLayoutConstraint!
    @IBOutlet weak var bottomPopUpViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var popUpBtmFreeCoffeeNameLbl: UILabel!
    @IBOutlet weak var popUpBtmFreeCoffeeNameLblWidth: NSLayoutConstraint!
    @IBOutlet weak var popUpBtmFreeCoffeeNameLblLeading: NSLayoutConstraint!
    @IBOutlet weak var popUpBtmBGViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var popUpImgsBGView: [UIView]!
    @IBOutlet var imagesBGView: [UIView]!
    
    @IBOutlet weak var popUpDismissBtn: UIButton!
    @IBOutlet weak var popUpMainView: UIView!
    @IBOutlet weak var popUpSubView: UIView!
    
    @IBOutlet weak var loyaltyPointsBGImage: UIImageView!
    @IBOutlet weak var loyaltyPointsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var loyaltyPointsView: UIView!
    @IBOutlet weak var loyaltyPointsViewBottom: NSLayoutConstraint!
    @IBOutlet weak var congratesNameLbl: UILabel!
    @IBOutlet weak var youHaveReachNameLbl: UILabel!
    @IBOutlet weak var loyaltyPointsLbl: UILabel!
    @IBOutlet weak var pointsNameLbl: UILabel!
    @IBOutlet weak var rewardAnimationImage: UIImageView!
    
    @IBOutlet weak var dcRewardsNameLbl: UILabel!
    @IBOutlet weak var muchOfRewardsNameLbl: UILabel!
    @IBOutlet weak var joinNowNameLbl: UILabel!
    @IBOutlet weak var dcLoyaltyProgramNameLbl: UILabel!
    @IBOutlet weak var bottomViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    
    var bannerPopUpID = 0
    var bannerPopUpSelectIndex = 0
    
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    
    //MaterialShowcase
    var backgroundAplha:Double = 0.95
    var backgroundColor:UIColor = .systemGray5
    var showcaseTextColor:UIColor = .black
    var showcase = MaterialShowcase()
    
    //OrderPopUp
    private var swipeView: TinderSwipeView<UserOrderModel>!{
        didSet{
            self.swipeView.delegate = self
        }
    }
    
    var SwiftTimer = Timer()
    var isThemeChange = false
    static var instance: HomeVC!
    var isFirstBannerSwipe = false
    
    var userOrderModels:[UserOrderModel]!
    
    var bannerDetailsArray = [BannerDetailsModel]()
    var popUpDetailsArray = [BannerDetailsModel]()
    var dashBoardArray = [DashBoardDetailsModel]()
    var categoryArray = [["Name":"Beta","topImg":"","BGImg":"Beta"], ["Name":"Subscription","topImg":"Subscription","BGImg":"Subscription 2"], ["Name":"Gift","topImg":"Gift","BGImg":"Gift Icon"], ["Name":"Bulk Order","topImg":"Home Bulk order-1","BGImg":"Bulk order"], ["Name":"Catering","topImg":"Home Catering","BGImg":"Catering 2"]]
    var orderTypesArray = [["Name":"Delivery","Img":"delivery"], ["Name":"Pickup","Img":"Home Pick up"], ["Name":"Dine In","Img":"dinein"], ["Name":"History","Img":"history"]]

    var categoryDetailsArray = [[:]]
    
    var lastContentOffset: CGFloat = 0
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
          let numbers = [0]
          let _ = numbers[1]
      }
    override func viewDidLoad() {
        super.viewDidLoad()
//        let button = UIButton(type: .roundedRect)
//            button.frame = CGRect(x: 20, y: 150, width: 100, height: 30)
//            button.setTitle("Test Crash", for: [])
//            button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
//        view.addSubview(button)
        
        
        orderTypeCollectionView.delegate = self
        orderTypeCollectionView.dataSource = self
        myScrollView.delegate = self
        HomeVC.instance = self
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 350
        tableView.rowHeight = UITableView.automaticDimension
        //getDetailsService()
        if AppDelegate.getDelegate().appLanguage == "English"{
            bannerCollectionView.semanticContentAttribute = .forceLeftToRight
            categoryCollectionView.semanticContentAttribute = .forceLeftToRight
            orderTypeCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            bannerCollectionView.semanticContentAttribute = .forceRightToLeft
            categoryCollectionView.semanticContentAttribute = .forceRightToLeft
            orderTypeCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(popUpTap(_:)))
        tapGR.delegate = self
        tapGR.numberOfTapsRequired = 2
        popUpSubView.addGestureRecognizer(tapGR)
        
        bannerPageController.addTarget(self, action: #selector(bannerPageSelection(_:)), for: .valueChanged)
        //self.getJwtService()
    }
    func getJwtService() {
        let dic:[String:Any] = ["Username": username,
                  "Key_Pair": key_Pair, "Pair_Value":pair_Value]
        AuthorizationModuleService.getJWTService(dic: dic, success: { (data) in
           UserDef.saveToUserDefault(value: data, key: "JWT")
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    override func viewDidAppear(_ animated: Bool){
        if (isMaterialShowcase() == false) && AppDelegate.getDelegate().appLanguage == "English"{
          self.showcase = MaterialShowcase()
          self.showcase.setTargetView(button: changeLanguageBtn, tapThrough: false)
          self.showcase.skipButtonPosition = .belowInstruction
          self.showcase.skipButtonTitle = "Next"
          self.showcase.primaryText = "Language"
          self.showcase.secondaryText = "We speak your language. Select for the best experience!"
          self.showcase.primaryTextColor = showcaseTextColor
          self.showcase.secondaryTextColor = showcaseTextColor
          self.showcase.shouldSetTintColor = true // It should be set to false when button uses image.
          self.showcase.backgroundPromptColor = backgroundColor
          self.showcase.backgroundAlpha = backgroundAplha
          self.showcase.isTapRecognizerForTargetView = false
          self.showcase.backgroundRadius = 280
          self.showcase.delegate = self
          self.showcase.skipButton = {
            self.showcase.completeShowcase()
          }
          self.showcase.show(hasSkipButton: true) {
            //self.showcase.skipButton.col
          }
          let button = UIButton(frame: CGRect(x: self.showcase.frame.width - 100, y: 180, width: 80, height: 40))
          button.titleLabel!.textAlignment = .right
          button.layer.borderWidth = 1
          button.layer.borderColor = showcaseTextColor.cgColor
          button.layer.cornerRadius = 4
          button.setTitle("Skip All", for: .normal)
          button.setTitleColor(showcaseTextColor, for: .normal)
          button.addTarget(self, action: #selector(skipAllBtn_Tapped), for: .touchUpInside)
          self.showcase.addSubview(button)
        }
    }
    @IBAction func skipAllBtn_Tapped(_ sender: Any) {
        UserDef.methodForSaveStringObjectValue("1", andKey: "MaterialShowcase")
        self.showcase.isHidden = true
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        bottomPopUpBGView.isHidden = true
        self.popUpMainView.isHidden = true
        
        //if dashBoardArray.count == 0{
            //getDetailsService()
        //}else{
            //startTimer()
        //}
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        /* End Crashlytics Logs */
        
        // Siri Shortcts
        if AppDelegate.getDelegate().siriShort == UserActivityType.TrackOrder {
            AppDelegate.getDelegate().siriShort = ""
            self.trackOrder()
            return
        }else if AppDelegate.getDelegate().siriShort == UserActivityType.OrderHistory {
            AppDelegate.getDelegate().siriShort = ""
            self.historyBtn_Tapped(0)
            //self.tabBarController?.selectedIndex = 3
            return
        }
    
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        isFirstBannerSwipe = true
        
        //self.view.backgroundColor = UIColor(hexString: "#F3F3F3")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            if AppDelegate.getDelegate().appLanguage == "English"{
                appTitleImg.image = UIImage(named: "BitmapWhite")
            }else{
                appTitleImg.image = UIImage(named: "ARABIC LOGO White")
            }
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                appTitleImg.image = UIImage(named: "app_name3x")
            }else{
                appTitleImg.image = UIImage(named: "ARABIC LOGO")
            }
        }
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.topShadowView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        self.topShadowBGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            languageImg.image = UIImage(named: "language")
            changeLanguageBtn.setImage(UIImage(named: "language"), for: .normal)
            self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
        }else{
            languageImg.image = UIImage(named: "FLAG")
            changeLanguageBtn.setImage(UIImage(named: "FLAG"), for: .normal)
            self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
        }
        orderTypeBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.tabBarController?.tabBar.isHidden = false
        
        self.bottomPopUpViewWidthConstraint.constant = 60
        self.btmFreeCoffeeNameLblWidth.constant = 0
        self.btmFreeCoffeeNameLblLeading.constant = 0
        self.popUpBtmBGViewWidthConstraint.constant = 60
        self.popUpBtmFreeCoffeeNameLblWidth.constant = 0
        self.popUpBtmFreeCoffeeNameLblLeading.constant = 0
        
        for i in 0...imagesBGView.count - 1{
            imagesBGView[i].backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
        }
        for i in 0...popUpImgsBGView.count - 1{
            popUpImgsBGView[i].backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
        }
        
        categoryDetailsArray = [["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Drive In", value: "", table: nil))!, "SubTitle":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Deliver to your car", value: "", table: nil))!, "Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "No need to wait in the Q. Our barista will deviver to your car", value: "", table: nil))!,"TypeName": (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!, "TypeImage":"Join", "TypeBGImage":"DC Drive In"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk order delivery", value: "", table: nil))!, "SubTitle":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Fresh Roasted Daily", value: "", table: nil))!, "Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Feel free to contact: +966 581570462", value: "", table: nil))!,"TypeName": (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk Order Now", value: "", table: nil))!, "TypeImage":"bulk", "TypeBGImage":"Home Bulk Order"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Coffee at Home", value: "", table: nil))!, "SubTitle":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "A subscription for coffee enthusiastic", value: "", table: nil))!, "Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Get up to 15% off on your subscription orders", value: "", table: nil))!,"TypeName": (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subscribe", value: "", table: nil))!, "TypeImage":"coffee at home", "TypeBGImage":"Home Subscribe"], ["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Catering", value: "", table: nil))!, "SubTitle":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "The Perfect choice for any occasion", value: "", table: nil))!, "Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Feel free to contact: +966 593007070", value: "", table: nil))!,"TypeName": (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Book Now", value: "", table: nil))!, "TypeImage":"Dashboard Cat", "TypeBGImage":"Catering-1"]]
        if isUserLogIn() == false{
            categoryDetailsArray.insert(["Title":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE SMILE", value: "", table: nil))!, "SubTitle":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Much of Rewards !", value: "", table: nil))!, "Desc":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discover the dr.CAFE Loyality Program", value: "", table: nil))!,"TypeName": (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Join Now", value: "", table: nil))!, "TypeImage":"Join Now Icon", "TypeBGImage":"Dashboard dc Program"], at: 0)
        }
        orderTypesArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!,"Img":"delivery"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!,"Img":"Home Pick up"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Dine In", value: "", table: nil))!,"Img":"dinein"], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Home History", value: "", table: nil))!,"Img":"history"]]
        DispatchQueue.main.async {
            self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
            self.categoryCollectionView.reloadData()
            self.tableView.reloadData()
            self.orderTypeCollectionView.reloadData()
        }
    
        DispatchQueue.main.async {
            if isUserLogIn() == true{
                //self.loyaltyPointsViewHeight.constant = 100
                //self.loyaltyPointsViewBottom.constant = 10
               
                self.bottomViewHeight.constant = 0
                self.bottomViewTopConstraint.constant = 0
                self.loyaltyPointsBGImage.layer.cornerRadius = 10
                self.loyaltyPointsBGImage.clipsToBounds = true
                self.loyaltyPointsBGImage.layer.masksToBounds = true
            }else{
                //self.loyaltyPointsViewHeight.constant = 0
                //self.loyaltyPointsViewBottom.constant = 0
                
                //self.bottomViewHeight.constant = 350
                //self.bottomViewTopConstraint.constant = 7
            }
        }
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidEnterBackground),
                                                      name: UIApplication.didEnterBackgroundNotification,
                                                      object: nil)
               
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnteredForeground),
                                                      name: UIApplication.willEnterForegroundNotification,
                                                      object: nil)
        self.tabBarColor()
        self.createUserActivity()
    }
    // Siri Shortcts
    func createUserActivity(){
        let activity = NSUserActivity(activityType:UserActivityType.TrackOrder)
        activity.title = "Track Order"
        //activity.userInfo = ["orderId":"120"]
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        self.userActivity = activity
        self.userActivity?.becomeCurrent()
    }
    func trackOrder() {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = TrackOrderVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
            obj.orderId = 0
            obj.orderFrom = 2
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
        SwiftTimer.invalidate()
        self.locationManager.stopUpdatingLocation()
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        DispatchQueue.main.async {
            AppDelegate.getDelegate().isDashboardFirst = true
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
        self.locationManager.stopUpdatingLocation()
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
        self.locationManager.stopUpdatingLocation()
    }
    
   
    //MARK: Start Timer Banner Scrolling
    func startTimer() {
        SwiftTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(HomeVC.scrollToNextCell), userInfo: nil, repeats: true);
    }
    //MARK: Banner Scroll To next cell
    @objc func scrollToNextCell(){
        //get cell size
        let cellSize = CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = bannerCollectionView.contentOffset;
        if bannerCollectionView.contentSize.width <= bannerCollectionView.contentOffset.x + cellSize.width{
            bannerCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y  , width: cellSize.width, height: cellSize.height), animated: false)
        }else{
            bannerCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        if bannerPageController.currentPage == bannerPageController.numberOfPages - 1 {
            bannerPageController.currentPage = 0
        } else {
            bannerPageController.currentPage += 1
        }
    }
    @objc func bannerPageSelection(_ sender: UIPageControl){
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            let page: Int? = sender.currentPage
//            print("Current Page \(sender.currentPage)")
//            let nextItem: IndexPath = IndexPath(item: page!, section: 0)
//            print("Next Page \(nextItem)")
//            if nextItem.row < self.bannerDetailsArray.count {
//                self.bannerCollectionView.scrollToItem(at: nextItem, at: .right, animated: true)
//            }
//        }
    }
    func tabBarColor() {
        let appearance = UITabBarAppearance()
        //appearance.backgroundColor = .white
        //appearance.shadowImage = UIImage()
        // appearance.shadowColor = .white
        let color = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        appearance.stackedLayoutAppearance.normal.iconColor = color.withAlphaComponent(0.7)
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color.withAlphaComponent(0.7)]
        //appearance.stackedLayoutAppearance.normal.badgeBackgroundColor = .yellow
        appearance.stackedLayoutAppearance.selected.iconColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")]
        // set padding between tabbar item title and image
        //appearance.stackedLayoutAppearance.selected.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 4)
        //appearance.stackedLayoutAppearance.normal.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 4)
        self.tabBarController?.tabBar.standardAppearance = appearance
    }
    //MARK: DashBoard Details Get Service
    func getDetailsService(action:Int){
        var originLat = String(format: "%.6f", latitude)
        var originLong = String(format: "%.6f", longitude)
        if latitude > 0 && longitude > 0{
            originLat = String(format: "%.6f", latitude)
            originLong = String(format: "%.6f", longitude)
        }else{
            originLat = ""
            originLong = ""
        }
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        let dic = ["UserId": UserDef.getUserId(),"AppVersion" : "\(appversion)","AppType": "IOS","DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2, "IsSubscription":false, "Latitude":originLat,"Longitude":originLong, "action" : action] as [String : Any]
        OrderModuleServices.DashBoardService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    if action == 2{
                        self.popUpDetailsArray = data.Data!.Result!
                        if AppDelegate.getDelegate().isDashboardFirst == true{
                            AppDelegate.getDelegate().isDashboardFirst = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 20.0) {
                                self.BannerPopUp()
                            }
                        }
                    }else if action == 1{
                        self.bannerDetailsArray = data.Data!.Result!
                        //self.bannerCollectionView.reloadData()
                    }else{
                        self.dashBoardArray = [data.Data!]
                        self.categoryCollectionView.reloadData()
                        if data.Data!.CartDetails!.CartItemsQuantity != nil{
                            AppDelegate.getDelegate().cartQuantity = data.Data!.CartDetails!.CartItemsQuantity!
                        }else{
                            AppDelegate.getDelegate().cartQuantity = 0
                        }
                        if data.Data!.CartDetails!.SubItemQty != nil{
                            AppDelegate.getDelegate().subCartQuantity = data.Data!.CartDetails!.SubItemQty!
                        }else{
                            AppDelegate.getDelegate().subCartQuantity = 0
                        }
                        self.allData()
                        if data.Data!.CartDetails!.ImageBasePathUrl != nil{
                            if data.Data!.CartDetails!.ImageBasePathUrl! != ""{
                                AppDelegate.getDelegate().imagePathURL = data.Data!.CartDetails!.ImageBasePathUrl!
                            }else{
                                AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                            }
                        }else{
                            AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                        }
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                                tabItems[2].image = UIImage(named: "Cart Tab")
                                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                            }else{
                                tabItem.badgeValue = nil
                                tabItems[2].image = UIImage(named: "Tab Order")
                                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                            }
                        }
                        self.bannerCollectionView.reloadData()
                        if data.Data!.CartDetails!.TotalPrice != nil{
                            AppDelegate.getDelegate().cartAmount = data.Data!.CartDetails!.TotalPrice!
                        }else{
                            AppDelegate.getDelegate().cartAmount = 0.0
                        }
                        if data.Data!.CartDetails!.SubTotalPrice != nil{
                            AppDelegate.getDelegate().subCartAmount = data.Data!.CartDetails!.SubTotalPrice!
                        }else{
                            AppDelegate.getDelegate().subCartAmount = 0.0
                        }
                        if data.Data!.themes != nil{
                            SaveAddressClass.themeColorArray = data.Data!.themes!
                        }
                        if data.Data!.CartDetails != nil{
                            SaveAddressClass.cartDetailsArray = [data.Data!.CartDetails!]
                        }
                        if data.Data!.CartDetails!.StoreId != nil{
                            AppDelegate.getDelegate().cartStoreId = data.Data!.CartDetails!.StoreId!
                        }else{
                            AppDelegate.getDelegate().cartStoreId = 0
                        }
                        
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            if data.Data!.CartDetails!.StoreNameEn != nil{
                                AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameEn!
                            }else{
                                AppDelegate.getDelegate().cartStoreName = ""
                            }
                        }else{
                            if data.Data!.CartDetails!.StoreNameAr != nil{
                                AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameAr!
                            }else{
                                AppDelegate.getDelegate().cartStoreName = ""
                            }
                        }
                        var themeColor = data.Data!.themes!.filter({$0.DefaultSelection == true})
                        if themeColor.count > 0{
                            if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                                AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                                AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                                AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                            }else{
                                AppDelegate.getDelegate().selectColor = "535353"
                                AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                AppDelegate.getDelegate().selectColorname = ""
                            }
                        }else{
                            themeColor = data.Data!.themes!.filter({$0.IsSelected == true})
                            if themeColor.count > 0{
                                if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                                    AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                                    AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                                    AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                                }else{
                                    AppDelegate.getDelegate().selectColor = "535353"
                                    AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                    AppDelegate.getDelegate().selectColorname = ""
                                }
                            }else{
                                if data.Data!.themes!.count > 0{
                                    if data.Data!.themes![0].ForegroundColor != nil && data.Data!.themes![0].BackgroundColor != nil{
                                        AppDelegate.getDelegate().selectColor = data.Data!.themes![0].ForegroundColor!
                                        AppDelegate.getDelegate().selectBGColor = data.Data!.themes![0].BackgroundColor!
                                        AppDelegate.getDelegate().selectColorname = data.Data!.themes![0].ThemeName
                                    }else{
                                        AppDelegate.getDelegate().selectColor = "535353"
                                        AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                        AppDelegate.getDelegate().selectColorname = ""
                                    }
                                }else{
                                    AppDelegate.getDelegate().selectColor = "535353"
                                    AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                    AppDelegate.getDelegate().selectColorname = ""
                                }
                            }
                        }
                        self.tabBarColor()
                        
                        if let _ = data.Data!.CartDetails!.BetaInfoTitle1{
                            AppDelegate.getDelegate().BetaInfoTitle1 = data.Data!.CartDetails!.BetaInfoTitle1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitle2{
                            AppDelegate.getDelegate().BetaInfoTitle2 = data.Data!.CartDetails!.BetaInfoTitle2!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoMobile1{
                            AppDelegate.getDelegate().BetaInfoMobile1 = data.Data!.CartDetails!.BetaInfoMobile1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoMobile2{
                            AppDelegate.getDelegate().BetaInfoMobile2 = data.Data!.CartDetails!.BetaInfoMobile2!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitleAr1{
                            AppDelegate.getDelegate().BetaInfoTitleAr1 = data.Data!.CartDetails!.BetaInfoTitleAr1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitleAr2{
                            AppDelegate.getDelegate().BetaInfoTitleAr2 = data.Data!.CartDetails!.BetaInfoTitleAr2!
                        }
                    }
                    if self.isFirstBannerSwipe == true{
                        self.isFirstBannerSwipe = false
                        self.startTimer()
                    }
                    //Update App
                    if data.Data!.updateInfo != nil{
                        if data.Data!.updateInfo!.count > 0{
                            if data.Data!.updateInfo![0].Severity != nil{
                                if data.Data!.updateInfo![0].Severity! == 1{
                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "UpgradeAppVC") as! UpgradeAppVC
                                    obj.modalPresentationStyle = .overCurrentContext
                                    obj.modalPresentationStyle = .overFullScreen
                                    self.present(obj, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
                if action == 2{
                    self.getDetailsService(action: 1)
                }else if action == 1{
                    self.getDetailsService(action: 0)
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: DashBoard Details Get Service WithOut Loader
    func getDetailsServiceWithoutLoader(action:Int){
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        var originLat = String(format: "%.6f", latitude)
        var originLong = String(format: "%.6f", longitude)
        if latitude > 0 && longitude > 0{
            originLat = String(format: "%.6f", latitude)
            originLong = String(format: "%.6f", longitude)
        }else{
            originLat = ""
            originLong = ""
        }
        let dic = ["UserId": UserDef.getUserId(),"AppVersion" : "\(appversion)","AppType": "IOS","DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2, "IsSubscription":false, "Latitude":originLat,"Longitude":originLong, "action" : action] as [String : Any]
        OrderModuleServices.DashBoardServiceWithoutLoader(dic: dic, success: { (data) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    if action == 2{
                        self.popUpDetailsArray = data.Data!.Result!
                        if AppDelegate.getDelegate().isDashboardFirst == true{
                            AppDelegate.getDelegate().isDashboardFirst = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 20.0) {
                                self.BannerPopUp()
                            }
                        }
                    }else if action == 1{
                        self.bannerDetailsArray = data.Data!.Result!
                        //self.bannerCollectionView.reloadData()
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            ANLoader.hide()
                        }
                        self.dashBoardArray = [data.Data!]
                        self.categoryCollectionView.reloadData()
                        if data.Data!.CartDetails!.CartItemsQuantity != nil{
                            AppDelegate.getDelegate().cartQuantity = data.Data!.CartDetails!.CartItemsQuantity!
                        }else{
                            AppDelegate.getDelegate().cartQuantity = 0
                        }
                        if data.Data!.CartDetails!.SubItemQty != nil{
                            AppDelegate.getDelegate().subCartQuantity = data.Data!.CartDetails!.SubItemQty!
                        }else{
                            AppDelegate.getDelegate().subCartQuantity = 0
                        }
                        self.allData()
                        if data.Data!.CartDetails!.ImageBasePathUrl != nil{
                            if data.Data!.CartDetails!.ImageBasePathUrl! != ""{
                                AppDelegate.getDelegate().imagePathURL = data.Data!.CartDetails!.ImageBasePathUrl!
                            }else{
                                AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                            }
                        }else{
                            AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                        }
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                                tabItems[2].image = UIImage(named: "Cart Tab")
                                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                            }else{
                                tabItem.badgeValue = nil
                                tabItems[2].image = UIImage(named: "Tab Order")
                                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                            }
                        }
                        self.bannerCollectionView.reloadData()
                        if data.Data!.CartDetails!.TotalPrice != nil{
                            AppDelegate.getDelegate().cartAmount = data.Data!.CartDetails!.TotalPrice!
                        }else{
                            AppDelegate.getDelegate().cartAmount = 0.0
                        }
                        if data.Data!.CartDetails!.SubTotalPrice != nil{
                            AppDelegate.getDelegate().subCartAmount = data.Data!.CartDetails!.SubTotalPrice!
                        }else{
                            AppDelegate.getDelegate().subCartAmount = 0.0
                        }
                        if data.Data!.themes != nil{
                            SaveAddressClass.themeColorArray = data.Data!.themes!
                        }
                        if data.Data!.CartDetails != nil{
                            SaveAddressClass.cartDetailsArray = [data.Data!.CartDetails!]
                        }
                        if data.Data!.CartDetails!.StoreId != nil{
                            AppDelegate.getDelegate().cartStoreId = data.Data!.CartDetails!.StoreId!
                        }else{
                            AppDelegate.getDelegate().cartStoreId = 0
                        }
                        
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            if data.Data!.CartDetails!.StoreNameEn != nil{
                                AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameEn!
                            }else{
                                AppDelegate.getDelegate().cartStoreName = ""
                            }
                        }else{
                            if data.Data!.CartDetails!.StoreNameAr != nil{
                                AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameAr!
                            }else{
                                AppDelegate.getDelegate().cartStoreName = ""
                            }
                        }
                        var themeColor = data.Data!.themes!.filter({$0.DefaultSelection == true})
                        if themeColor.count > 0{
                            if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                                AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                                AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                                AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                            }else{
                                AppDelegate.getDelegate().selectColor = "535353"
                                AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                AppDelegate.getDelegate().selectColorname = ""
                            }
                        }else{
                            themeColor = data.Data!.themes!.filter({$0.IsSelected == true})
                            if themeColor.count > 0{
                                if themeColor[0].ForegroundColor != nil && themeColor[0].BackgroundColor != nil{
                                    AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                                    AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                                    AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                                }else{
                                    AppDelegate.getDelegate().selectColor = "535353"
                                    AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                    AppDelegate.getDelegate().selectColorname = ""
                                }
                            }else{
                                if data.Data!.themes!.count > 0{
                                    if data.Data!.themes![0].ForegroundColor != nil && data.Data!.themes![0].BackgroundColor != nil{
                                        AppDelegate.getDelegate().selectColor = data.Data!.themes![0].ForegroundColor!
                                        AppDelegate.getDelegate().selectBGColor = data.Data!.themes![0].BackgroundColor!
                                        AppDelegate.getDelegate().selectColorname = data.Data!.themes![0].ThemeName
                                    }else{
                                        AppDelegate.getDelegate().selectColor = "535353"
                                        AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                        AppDelegate.getDelegate().selectColorname = ""
                                    }
                                }else{
                                    AppDelegate.getDelegate().selectColor = "535353"
                                    AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                                    AppDelegate.getDelegate().selectColorname = ""
                                }
                            }
                        }
                        self.tabBarColor()
                        
                        if let _ = data.Data!.CartDetails!.BetaInfoTitle1{
                            AppDelegate.getDelegate().BetaInfoTitle1 = data.Data!.CartDetails!.BetaInfoTitle1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitle2{
                            AppDelegate.getDelegate().BetaInfoTitle2 = data.Data!.CartDetails!.BetaInfoTitle2!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoMobile1{
                            AppDelegate.getDelegate().BetaInfoMobile1 = data.Data!.CartDetails!.BetaInfoMobile1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoMobile2{
                            AppDelegate.getDelegate().BetaInfoMobile2 = data.Data!.CartDetails!.BetaInfoMobile2!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitleAr1{
                            AppDelegate.getDelegate().BetaInfoTitleAr1 = data.Data!.CartDetails!.BetaInfoTitleAr1!
                        }
                        if let _ = data.Data!.CartDetails!.BetaInfoTitleAr2{
                            AppDelegate.getDelegate().BetaInfoTitleAr2 = data.Data!.CartDetails!.BetaInfoTitleAr2!
                        }
                    }
                    if self.isFirstBannerSwipe == true{
                        self.isFirstBannerSwipe = false
                        self.startTimer()
                    }
                    //MARK: Update App
                    if data.Data!.updateInfo != nil{
                        if data.Data!.updateInfo!.count > 0{
                            if data.Data!.updateInfo![0].Severity != nil{
                                if data.Data!.updateInfo![0].Severity! == 1{
                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                    let obj = mainStoryBoard.instantiateViewController(withIdentifier: "UpgradeAppVC") as! UpgradeAppVC
                                    obj.modalPresentationStyle = .overCurrentContext
                                    obj.modalPresentationStyle = .overFullScreen
                                    self.present(obj, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
                if action == 2{
                    self.getDetailsServiceWithoutLoader(action: 1)
                }else if action == 1{
                    self.getDetailsServiceWithoutLoader(action: 0)
                }
            }
        }) { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK:AllData
    func allData(){
        DispatchQueue.main.async {
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.appTitleImg.image = UIImage(named: "BitmapWhite")
                }else{
                    self.appTitleImg.image = UIImage(named: "ARABIC LOGO White")
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.appTitleImg.image = UIImage(named: "app_name3x")
                }else{
                    self.appTitleImg.image = UIImage(named: "ARABIC LOGO")
                }
            }
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            self.topShadowView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            self.topShadowBGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            for i in 0...self.imagesBGView.count - 1{
                self.imagesBGView[i].backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
            }
            for i in 0...self.popUpImgsBGView.count - 1{
                self.popUpImgsBGView[i].backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
            }
            
            if self.dashBoardArray[0].CartDetails!.IsFavourite != nil{
                if self.dashBoardArray[0].CartDetails!.IsFavourite! > 0{
                    if self.orderTypesArray.count == 4{
                        self.orderTypesArray.insert(["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Favourite", value: "", table: nil))!,"Img":"heart_fill"], at: 4)
                    }
                }
            }
            self.orderTypeCollectionView.reloadData()
            self.orderTypeCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
        }
        
        if dashBoardArray[0].Orders!.count > 0{
            trackOrCartImg.image = UIImage(named: "delivery")
            trackOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Track Order", value: "", table: nil))!
            orderTrackDetailsView.isHidden = false
            orderTrackDetailsViewHeight.constant = 76
            orderTrackDetailsViewBottom.constant = 17
            if dashBoardArray[0].Orders![0].OrderType == "4"{
                if dashBoardArray[0].Orders![0].OrderStatusId == 2{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Confirmed", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 6{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Preparing Your Order", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 7{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order is Ready", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 9{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rider is picking up your order", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 10{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Delivered", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 4{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Cancel", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 5{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Rejected", value: "", table: nil))!
                }else{
                    orderStatusMessageLbl.text = ""
                }
            }else{
                if dashBoardArray[0].Orders![0].OrderStatusId == 2{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Confirmed", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 6{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Preparing Your Order", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 7{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order is Ready", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 10{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Served", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 4{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Cancel", value: "", table: nil))!
                }else if dashBoardArray[0].Orders![0].OrderStatusId == 5{
                    orderStatusMessageLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Rejected", value: "", table: nil))!
                }else{
                    orderStatusMessageLbl.text = ""
                }
            }
        }else{
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                trackOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                orderStatusMessageLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have", value: "", table: nil))!) \(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "items in cart", value: "", table: nil))!)"
                trackOrCartImg.image = UIImage(named: "cart white")
                orderTrackDetailsView.isHidden = false
                orderTrackDetailsViewHeight.constant = 76
                orderTrackDetailsViewBottom.constant = 17
            }else{
                orderTrackDetailsView.isHidden = true
                orderTrackDetailsViewHeight.constant = 0
                orderTrackDetailsViewBottom.constant = 0
            }
        }

        //Bell Animation
        
        self.bellImg.animationDuration = 1
        self.bellImg.animationRepeatCount = 4 //count in zero then it will animate infinity times
        var images = [UIImage]()
        for i in 1...30
        {
            images.append(UIImage(named: "\(i)")!)
        }
        self.bellImg.animationImages = images
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.bellImg.startAnimating()
        }
        
        //Layalty Points
        DispatchQueue.main.async {
//            if isUserLogIn() == true{
//                self.loyaltyPointsViewHeight.constant = 100
//                self.loyaltyPointsViewBottom.constant = 10
//            }else{
//                self.loyaltyPointsViewHeight.constant = 0
//                self.loyaltyPointsViewBottom.constant = 0
//            }
            //if self.dashBoardArray[0].CartDetails!.LoyaltyPoints == 0 || self.dashBoardArray[0].CartDetails!.LoyaltyPoints == 0.0{
                //self.loyaltyPointsViewHeight.constant = 0
                //self.loyaltyPointsViewBottom.constant = 0
            //}else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.congratesNameLbl.text = self.dashBoardArray[0].CartDetails!.GreetingsEn
            }else{
                self.congratesNameLbl.text = self.dashBoardArray[0].CartDetails!.GreetingsAr
            }
            self.youHaveReachNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have Earn", value: "", table: nil))!
            self.pointsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "points", value: "", table: nil))!
            if isUserLogIn() == true{
                //if self.view.frame.width > 420{
                    //self.loyaltyPointsViewHeight.constant = 110
                //}else{
                    //self.loyaltyPointsViewHeight.constant = 100
                //}
                self.loyaltyPointsViewHeight.constant = 90
                self.loyaltyPointsViewBottom.constant = 10
                
                self.rewardAnimationImage.animationDuration = 1
                self.rewardAnimationImage.animationRepeatCount = 0
                var images = [UIImage]()
                for i in 1...14
                {
                    images.append(UIImage(named: "Cup \(i)")!)
                }
                self.rewardAnimationImage.animationImages = images
                self.rewardAnimationImage.startAnimating()
            }else{
                self.loyaltyPointsViewHeight.constant = 0
                self.loyaltyPointsViewBottom.constant = 0
            }
            var myPoints = round(0)
            if self.dashBoardArray[0].CartDetails!.LoyaltyPoints != nil{
                myPoints = round(self.dashBoardArray[0].CartDetails!.LoyaltyPoints!)
            }else{
                myPoints = 0
            }
            
            //self.loyaltyPointsLbl.text = "\(Int(myPoints).intWithCommas())"
            let nf = NumberFormatter()
            nf.numberStyle = .decimal
            nf.locale = Locale(identifier: "EN")
            self.loyaltyPointsLbl.text = nf.string(from: NSNumber(value: Int(myPoints)))
                if self.dashBoardArray[0].CartDetails!.BadgeId == 1{
                    self.youHaveReachNameLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
                    self.pointsNameLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
                    self.loyaltyPointsLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
                }else if self.dashBoardArray[0].CartDetails!.BadgeId == 2{
                    self.youHaveReachNameLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
                    self.pointsNameLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
                    self.loyaltyPointsLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
                }else if self.dashBoardArray[0].CartDetails!.BadgeId == 3{
                    self.youHaveReachNameLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
                    self.pointsNameLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
                    self.loyaltyPointsLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
                }else{
                    self.youHaveReachNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.pointsNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.loyaltyPointsLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            
            if self.dashBoardArray[0].CartDetails!.BadgeId == 1{
                self.loyaltyPointsBGImage.image = UIImage(named: "smile_rect_standard")
            }else if self.dashBoardArray[0].CartDetails!.BadgeId == 2{
                self.loyaltyPointsBGImage.image = UIImage(named: "smile_rect_platinum")
            }else if self.dashBoardArray[0].CartDetails!.BadgeId == 3{
                self.loyaltyPointsBGImage.image = UIImage(named: "smile_rect_gold")
            }else{
                self.loyaltyPointsBGImage.image = UIImage(named: "smile_rect_elite")
            }
        }
    }
    //MARK:Track Order
    @IBAction func trackOrderBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            if trackOrderNameLbl.text! == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!{
                self.tabBarController?.selectedIndex = 2
//                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//                var obj = CartVC()
//                obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//                obj.whereObj = 1
//                obj.isRemove = true
//                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                if dashBoardArray.count > 0 {
                    if let _ = dashBoardArray[0].Orders {
                        if dashBoardArray[0].Orders!.count > 0{
                            if dashBoardArray[0].Orders![0].OrderStatusId != 10{
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                var obj = TrackOrderVC()
                                obj = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                                obj.orderId = dashBoardArray[0].Orders![0].OrderId
                                obj.orderFrom = 2
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                        }
                    }
                }
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK:Order Popup
    func BannerPopUp(){
        //newOrdersArray = acceptOrdersArray
        if popUpDetailsArray.count > 0{
            popUpMainView.isHidden = false
            var model : [UserOrderModel] = []
            //var storeModel : [StoreDetailsModel] = []
            for n in 0...popUpDetailsArray.count - 1 {
                var storeList = [StoreDetailsModel]()
                if popUpDetailsArray[n].StoreList != nil{
                    storeList = popUpDetailsArray[n].StoreList!
                }
                model.append(UserOrderModel(Id: popUpDetailsArray[n].Id, BannerName: popUpDetailsArray[n].BannerName, BannerImageEn: popUpDetailsArray[n].BannerImageEn, BannerImageAr: popUpDetailsArray[n].BannerImageAr, IsClick: popUpDetailsArray[n].IsClick, MainCategoryId: popUpDetailsArray[n].MainCategoryId, MainCategoryEn: popUpDetailsArray[n].MainCategoryEn, MainCategoryAr: popUpDetailsArray[n].MainCategoryAr, CategoryId: popUpDetailsArray[n].CategoryId, CategoryEn: popUpDetailsArray[n].CategoryEn, CategoryAr: popUpDetailsArray[n].CategoryAr, SubCategoryId: popUpDetailsArray[n].SubCategoryId, SubCategoryEn: popUpDetailsArray[n].SubCategoryEn, SubCategoryAr: popUpDetailsArray[n].SubCategoryAr, ItemId: popUpDetailsArray[n].ItemId, DineId: popUpDetailsArray[n].DineId, TakeAway: popUpDetailsArray[n].TakeAway, DriveThru: popUpDetailsArray[n].DriveThru, Delivery: popUpDetailsArray[n].Delivery, SequenceId: popUpDetailsArray[n].SequenceId, StoreList: storeList))
                //model.append(UserOrderModel(Id: popUpDetailsArray[n].Id, Name: popUpDetailsArray[n].Name, TypeOfPopUp: popUpDetailsArray[n].TypeOfPopUp, ImageEn: popUpDetailsArray[n].ImageEn, ImageAr: popUpDetailsArray[n].ImageAr, MainCategoryId: popUpDetailsArray[n].MainCategoryId, CategoryId: popUpDetailsArray[n].CategoryId, SubCategoryId: popUpDetailsArray[n].SubCategoryId, ItemId: popUpDetailsArray[n].ItemId, OfferId: popUpDetailsArray[n].OfferId, PackageId: popUpDetailsArray[n].PackageId, SubscriptionId: popUpDetailsArray[n].SubscriptionId, WebUrl: popUpDetailsArray[n].WebUrl))
            }
            userOrderModels = model
        }else{
            popUpMainView.isHidden = true
            return
        }
    
        // Dynamically create view for each tinder card
        let contentView: (Int, CGRect, UserOrderModel) -> (UIView) = { (index: Int ,frame: CGRect , userModel: UserOrderModel) -> (UIView) in
            let customView = HomePopUpView(frame: frame)
            customView.userOrderModel = userModel
            
            //Image
            var ImgStr:NSString!
            if AppDelegate.getDelegate().appLanguage == "English"{
                ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(userModel.BannerImageEn)" as NSString
            }else{
                ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(userModel.BannerImageAr)" as NSString
            }
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            customView.bannerImg.kf.setImage(with: url)
            
            self.bannerPopUpID = self.userOrderModels[0].Id
            self.bannerPopUpSelectIndex = 0
            
            return customView
        }
        swipeView = TinderSwipeView<UserOrderModel>(frame: popUpSubView.bounds, contentView: contentView)
        popUpSubView.addSubview(swipeView)
        swipeView.showTinderCards(with: userOrderModels ,isDummyShow: false)
        swipeView = TinderSwipeView<UserOrderModel>(frame: popUpSubView.bounds, contentView: contentView)
        popUpSubView.addSubview(swipeView)
        swipeView.showTinderCards(with: userOrderModels ,isDummyShow: false)
    }
    @objc func popUpTap(_ gesture: UITapGestureRecognizer){
        DispatchQueue.main.async {
            self.popUpSelected()
        }
    }
    func popUpSelected(){
        var popUpDetails = popUpDetailsArray.filter({$0.Id == bannerPopUpID})
        if popUpDetails.count > 0{
            popUpDetails = popUpDetailsArray.filter({$0.Id == bannerPopUpID})
        }else{
            popUpDetails = [popUpDetailsArray[0]]
        }
        if popUpDetailsArray.count > 0{
            for i in 0...popUpDetailsArray.count-1{
                if popUpDetailsArray[i].Id == bannerPopUpID{
                    bannerPopUpSelectIndex = i
                }
            }
        }else{
            bannerPopUpSelectIndex = 0
        }
        //Multiple OrderTypes True functionality
        if popUpDetails[0].IsClick == true{
            if !hasLocationPermission() {
                if AppDelegate.getDelegate().currentlocation != nil{
                    var orderTypeId:Int!
                    if popUpDetails[0].DineId == true{
                        orderTypeId = 1
                    }else if popUpDetails[0].DriveThru == true{
                        orderTypeId = 2
                    }else if popUpDetails[0].TakeAway == true{
                        orderTypeId = 3
                    }else if popUpDetails[0].Delivery == true{
                        orderTypeId = 4
                    }
                    if popUpDetails[0].StoreList != nil{
                        if popUpDetails[0].StoreList!.count > 0{
                            if popUpDetails[0].StoreList!.count == 1{
                                if popUpDetails[0].StoreList![0].StoreStatus == false{//Store is Closed
                                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
                                }else{//Store is Open
                                    for i in 0...popUpDetails[0].StoreList![0].BusySchedule!.count - 1{
                                        if popUpDetails[0].StoreList![0].BusySchedule![i].OrderTypeId == orderTypeId{
                                            if popUpDetails[0].StoreList![0].BusySchedule![i].IsBusy == true{
                                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(popUpDetails[0].StoreList![0].BusySchedule![i].OpensInMinute) mins")
                                                return
                                            }else{
                                                AppDelegate.getDelegate().cartStoreId = popUpDetails[0].StoreList![0].Id
                                                if AppDelegate.getDelegate().appLanguage == "English"{
                                                    AppDelegate.getDelegate().cartStoreName = popUpDetails[0].StoreList![0].NameEn
                                                }else{
                                                    AppDelegate.getDelegate().cartStoreName = popUpDetails[0].StoreList![0].NameAr
                                                }
                                                AppDelegate.getDelegate().selectAddress = ""
                                                //self.bannerSelect(sender: self.bannerPopUpSelectIndex)
                                                if popUpDetails[0].ItemId == 0{// single store
                                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                                    var obj = ItemsVC()
                                                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                                                    obj.sectionType = 0
                                                    obj.isV12 = false
                                                    obj.selectOrderType = orderTypeId!
                                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                                        obj.address = popUpDetails[0].StoreList![0].NameEn
                                                    }else{
                                                        obj.address = popUpDetails[0].StoreList![0].NameAr
                                                    }
                                                    obj.selectItemName = popUpDetails[0].BannerName
                                                    obj.categoryId = popUpDetails[0].CategoryId
                                                    obj.storeId = popUpDetails[0].StoreList![0].Id
                                                    obj.selectAddressID = 0
                                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                                        obj.selectItemName = popUpDetails[0].MainCategoryEn
                                                        obj.categoryName = popUpDetails[0].CategoryEn
                                                    }else{
                                                        obj.selectItemName = popUpDetails[0].MainCategoryAr
                                                        obj.categoryName = popUpDetails[0].CategoryAr
                                                    }
                                                    obj.isBanner = true
                                                    self.navigationController?.pushViewController(obj, animated: true)
                                                }else{//single store and Single item
                                                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                                    var obj = AdditionalsVC()
                                                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
                                                    obj.itemID = popUpDetails[0].ItemId
                                                    obj.selectOrderType = orderTypeId!
                                                    obj.sectionType = 0
                                                    obj.storeId = popUpDetails[0].StoreList![0].Id
                                                    obj.selectAddressId = 0
                                                    obj.isV12 = false
                                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                                        obj.address = popUpDetails[0].StoreList![0].NameEn
                                                        obj.titleLbl = popUpDetails[0].ItemEn
                                                    }else{
                                                        obj.address = popUpDetails[0].StoreList![0].NameAr
                                                        obj.titleLbl = popUpDetails[0].ItemAr
                                                    }
                                                    self.navigationController?.pushViewController(obj, animated: true)
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{//Multiple stores
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                var obj = StoresVC()
                                obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                                obj.whereObj = 2
                                obj.isBanner = true
                                obj.bannerId = popUpDetails[0].Id
                                obj.orderType = orderTypeId!
                                obj.storeDetailsArray = popUpDetails[0].StoreList!
                                SaveAddressClass.selectBannerStores = [popUpDetails[0]]
                                obj.itemId = popUpDetails[0].ItemId
                                obj.categoryId = popUpDetails[0].CategoryId
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    obj.itemTitle = popUpDetails[0].ItemEn
                                    obj.categoryName = popUpDetails[0].CategoryEn
                                }else{
                                    obj.itemTitle = popUpDetails[0].ItemAr
                                    obj.categoryName = popUpDetails[0].CategoryAr
                                }
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                        }else{
                            print("Not Clickable")
                        }
                    }else{
                        print("Not Clickable")
                    }
                }else{
                    let vc = LocationAutocompleteVC(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LocationAutocompleteVC", value: "", table: nil))!, bundle: nil)
                    vc.locationAutocompleteVCDelegate = self
                    vc.whereObj = 10
                    vc.isBanner = false
                    vc.isPopup = true
                    vc.bannerId = popUpDetails[0].Id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                var orderTypeId:Int!
                if popUpDetails[0].DineId == true{
                    orderTypeId = 1
                }else if popUpDetails[0].DriveThru == true{
                    orderTypeId = 2
                }else if popUpDetails[0].TakeAway == true{
                    orderTypeId = 3
                }else if popUpDetails[0].Delivery == true{
                    orderTypeId = 4
                }
                if popUpDetails[0].StoreList != nil{
                    if popUpDetails[0].StoreList!.count == 1{
                        if popUpDetails[0].StoreList![0].StoreStatus == false{//Store is Closed
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
                        }else{//Store is Open
                            for i in 0...popUpDetails[0].StoreList![0].BusySchedule!.count - 1{
                                if popUpDetails[0].StoreList![0].BusySchedule![i].OrderTypeId == orderTypeId{
                                    if popUpDetails[0].StoreList![0].BusySchedule![i].IsBusy == true{
                                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(popUpDetails[0].StoreList![0].BusySchedule![i].OpensInMinute) mins")
                                        return
                                    }else{
                                        AppDelegate.getDelegate().cartStoreId = popUpDetails[0].StoreList![0].Id
                                        if AppDelegate.getDelegate().appLanguage == "English"{
                                            AppDelegate.getDelegate().cartStoreName = popUpDetails[0].StoreList![0].NameEn
                                        }else{
                                            AppDelegate.getDelegate().cartStoreName = popUpDetails[0].StoreList![0].NameAr
                                        }
                                        AppDelegate.getDelegate().selectAddress = ""
                                        //self.bannerSelect(sender: self.bannerPopUpSelectIndex)
                                        if popUpDetails[0].ItemId == 0{// single store
                                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                            var obj = ItemsVC()
                                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                                            obj.sectionType = 0
                                            obj.isV12 = false
                                            obj.selectOrderType = orderTypeId!
                                            if AppDelegate.getDelegate().appLanguage == "English"{
                                                obj.address = popUpDetails[0].StoreList![0].NameEn
                                            }else{
                                                obj.address = popUpDetails[0].StoreList![0].NameAr
                                            }
                                            obj.selectItemName = popUpDetails[0].BannerName
                                            obj.categoryId = popUpDetails[0].CategoryId
                                            obj.storeId = popUpDetails[0].StoreList![0].Id
                                            obj.selectAddressID = 0
                                            //obj.whereObj = 2
                                            if AppDelegate.getDelegate().appLanguage == "English"{
                                                obj.selectItemName = popUpDetails[0].MainCategoryEn
                                                obj.categoryName = popUpDetails[0].CategoryEn
                                            }else{
                                                obj.selectItemName = popUpDetails[0].MainCategoryAr
                                                obj.categoryName = popUpDetails[0].CategoryAr
                                            }
                                            obj.isBanner = true
                                            self.navigationController?.pushViewController(obj, animated: true)
                                        }else{//single store and Single item
                                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                            var obj = AdditionalsVC()
                                            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
                                            obj.itemID = popUpDetails[0].ItemId
                                            obj.selectOrderType = orderTypeId!
                                            obj.sectionType = 0
                                            obj.storeId = popUpDetails[0].StoreList![0].Id
                                            obj.selectAddressId = 0
                                            obj.isV12 = false
                                            if AppDelegate.getDelegate().appLanguage == "English"{
                                                obj.address = popUpDetails[0].StoreList![0].NameEn
                                                obj.titleLbl = popUpDetails[0].ItemEn
                                            }else{
                                                obj.address = popUpDetails[0].StoreList![0].NameAr
                                                obj.titleLbl = popUpDetails[0].ItemAr
                                            }
                                            self.navigationController?.pushViewController(obj, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }else{//Multiple stores
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        var obj = StoresVC()
                        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                        obj.whereObj = 2
                        obj.isBanner = true
                        obj.bannerId = popUpDetails[0].Id
                        obj.orderType = orderTypeId!
                        obj.storeDetailsArray = popUpDetails[0].StoreList!
                        SaveAddressClass.selectBannerStores = [popUpDetails[0]]
                        obj.itemId = popUpDetails[0].ItemId
                        obj.categoryId = popUpDetails[0].CategoryId
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            obj.itemTitle = popUpDetails[0].ItemEn
                            obj.categoryName = popUpDetails[0].CategoryEn
                        }else{
                            obj.itemTitle = popUpDetails[0].ItemAr
                            obj.categoryName = popUpDetails[0].CategoryAr
                        }
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }else{
                    print("Not Clickable")
                }
            }
        }
    }
    //MARK: PopUp Dismiss Button Action
    @IBAction func popUpDismissBtn_Tapped(_ sender: Any) {
        let dic:[String:Any] = ["UserId": UserDef.getUserId() ,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "PopupId":"\(bannerPopUpID)"]
        OrderModuleServices.PopUpUserVisibilityUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.popUpMainView.isHidden = true
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Delivery Button Action
    @IBAction func DeliveryBtn_Tapped(_ sender: Any) {
        //if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = OrderTypeVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderTypeVC") as! OrderTypeVC
            obj.whereObj = 1
            obj.isLogin = false
            self.navigationController?.pushViewController(obj, animated: true)
//        }else{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            var obj = LoginVC()
//            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            obj.whereObj = 10
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
    }
    //MARK: Pickup Button Action
    @IBAction func pickupBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = StoresVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
        obj.whereObj = 1
        obj.isBanner = false
        obj.orderType = 3
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: DineIn Button Action
    @IBAction func dineInBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = StoresVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
        obj.whereObj = 1
        obj.isBanner = false
        obj.orderType = 1
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: History Button Action
    @IBAction func historyBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = NewOrderHistoryVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewOrderHistoryVC") as! NewOrderHistoryVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.whereObj = 11
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: language change Button Action
    @IBAction func languageChangeBtn_Tapped(_ sender: Any) {
        var language = "En"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "En"
        }else{
            language = "Ar"
        }
//        let appVersion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
//        let deviceInfo:[String:Any] = [
//            "devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
//            "applang":language,
//            "OSversion":"IOS(\(UIDevice.current.systemVersion))",
//            "SDKversion":"",
//            "Modelname":"\(UIDevice.current.name)",
//            "deivcelanguage":AppDelegate.getDelegate().appLanguage!,
//            "AppVersion":"IOS(\(appVersion))",
//            "TimeZone":"\(TimeZone.current.abbreviation()!)",
//            "TimeZoneRegion":"\(TimeZone.current.identifier)"]
        if isUserLogIn() == true{
            let dic = ["UserId": UserDef.getUserId(),
             "Language": language,
             "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))",
             "DeviceInfo": "\(getDeviceInfo())", "RequestBy":2] as [String : Any]
            UserModuleServices.changeLanguageService(dic: dic, success: { (data) in
                /*if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    //ANLoader.hide()
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showToast(on: self, message: data.MessageEn)
                    }else{
                        Alert.showToast(on: self, message: data.MessageAr)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.languageChange()
                    }
                }*/
            }) { (error) in
                //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            //self.languageChange()
        }
        
        self.languageChange()
    }
    func languageChange(){
        if(AppDelegate.getDelegate().myLanguage == "English"){
            var path = String()
            path = Bundle.main.path(forResource: "ar", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "Arabic"
            UserDefaults.standard.set("Arabic", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "Arabic"
            AppDelegate.getDelegate().tabbarViewController()
        }else{
            var path = String()
            path = Bundle.main.path(forResource: "en", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "English"
            UserDefaults.standard.set("English", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "English"
            AppDelegate.getDelegate().tabbarViewController()
        }
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.whereObj = 1
            obj.isRemove = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Location Button Action
    @IBAction func locationBtn_Tapped(_ sender: Any) {
    }
    //MARK: Share FeedBack Button Show and Hide ButtonAction
    @IBAction func bottomPopUpBtn_Tapped(_ sender: Any) {
        if bottomPopUpBGView.isHidden == true{
            bottomPopUpBGView.isHidden = false
        }else{
            bottomPopUpBGView.isHidden = true
        }
    }
    @IBAction func bottomPopUpDismissBtn_Tapped(_ sender: Any) {
        bottomPopUpBGView.isHidden = true
    }
    //MARK: Free Coffee Button Action
    @IBAction func freeCoffeeBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = FreeCoffeeVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "FreeCoffeeVC") as! FreeCoffeeVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.whereObj = 18
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async {
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let shareUrl = URL.init(string: urlStr as String)
            let Name = ""
            let Des = "dr.CAFE® COFFEE launches the newest and latest mobile ordering & pickup application in the coffee industry allowing you to order your favorite beverage and/or food and ready to deliver or pick up or enjoy at the store.\n\n"
            let shareAll = [Name,Des,"Download the app at ",shareUrl as Any] as [Any]

            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    //MARK: ContactUs Button Action
    @IBAction func contactUsBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = ContactUsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: FeedBack Button Action
    @IBAction func feedBackBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = SelectFeedbackTypeVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectFeedbackTypeVC") as! SelectFeedbackTypeVC
        if dashBoardArray[0].CartDetails != nil{
            if dashBoardArray[0].CartDetails!.ClosedOrder != nil{
                obj.detailsArray = [dashBoardArray[0].CartDetails!]
            }
        }
        obj.longitude = latitude
        obj.longitude = longitude
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Loyalty Points Button Action
    @IBAction func loyaltyPointsBtn_Tapped(_ sender: Any) {
//        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//        let obj:DCSmileVC = mainStoryBoard.instantiateViewController(withIdentifier: "DCSmileVC") as! DCSmileVC
//        obj.whereObj = 1
//        self.navigationController?.pushViewController(obj, animated: true)
        self.tabBarController?.selectedIndex = 3
    }
    //MARK: Loyalty Program Button Action
    @IBAction func loyaltyPrograaeBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = LoginVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
        self.locationManager.stopUpdatingLocation()
        if !hasLocationPermission() {
            if AppDelegate.getDelegate().currentlocation != nil{
                self.latitude = AppDelegate.getDelegate().currentlocation.coordinate.latitude
                self.longitude = AppDelegate.getDelegate().currentlocation.coordinate.longitude
            }else{
                self.latitude = 24.7233943939208
                self.longitude = 46.6365165710449
            }
            if AppDelegate.getDelegate().isDashboardFirst == true{
                ANLoader.showLoading("", disableUI: true)
                getDetailsServiceWithoutLoader(action: 2)
                //getDetailsService(action: 2)
            }else{
                getDetailsServiceWithoutLoader(action: 1)
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        AppDelegate.getDelegate().currentLatitude = (location?.coordinate.latitude)!
        AppDelegate.getDelegate().currentLongitude = (location?.coordinate.longitude)!
//        if latitude < 24{
//            latitude = 24.782765
//            longitude = 46.767433
//        }
        if AppDelegate.getDelegate().isDashboardFirst == true{
            ANLoader.showLoading("", disableUI: true)
            getDetailsServiceWithoutLoader(action: 2)
            //getDetailsService(action: 2)
        }else{
            //getDetailsService(action: 1)
            getDetailsServiceWithoutLoader(action: 1)
        }
        self.locationManager.stopUpdatingLocation()
    }
    //Label Text Bold and Normal
    func boldAndRegularText(text1:String, text2:String) -> NSMutableAttributedString{
        let boldAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 16.0)!
           ]
        let regularAttribute = [
              NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 16.0)!
           ]
        let regularText = NSAttributedString(string: text1, attributes: regularAttribute)
        let boldText1 = NSAttributedString(string: text2, attributes: boldAttribute)
        let newString = NSMutableAttributedString()
        newString.append(regularText)
        newString.append(boldText1)
        return newString
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if lastContentOffset > scrollView.contentOffset.y && lastContentOffset < scrollView.contentSize.height - scrollView.frame.height {//Scroll to Top
            //if (scrollView.contentOffset.y <= 40){
                self.bottomPopUpViewWidthConstraint.constant = 60
                self.btmFreeCoffeeNameLblWidth.constant = 0
                self.btmFreeCoffeeNameLblLeading.constant = 0
                self.popUpBtmBGViewWidthConstraint.constant = 60
                self.popUpBtmFreeCoffeeNameLblWidth.constant = 0
                self.popUpBtmFreeCoffeeNameLblLeading.constant = 0
                if (scrollView.contentOffset.y <= 25){
                    self.topShadowView.isHidden = true
                }
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            //}
        } else if lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0 {//Scroll to Bottom
            self.bottomPopUpViewWidthConstraint.constant = 120
            self.btmFreeCoffeeNameLblWidth.constant = 50
            self.btmFreeCoffeeNameLblLeading.constant = 10
            self.popUpBtmBGViewWidthConstraint.constant = 120
            self.popUpBtmFreeCoffeeNameLblWidth.constant = 50
            self.popUpBtmFreeCoffeeNameLblLeading.constant = 10
            self.topShadowView.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        // update the new position acquired
        lastContentOffset = scrollView.contentOffset.y
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension HomeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryDetailsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeVCCategoryTypeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeVCCategoryTypeTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeVCCategoryTypeTVCell", value: "", table: nil))!, for: indexPath) as! HomeVCCategoryTypeTVCell
        
        cell.titleLbl.text = categoryDetailsArray[indexPath.row]["Title"] as? String
        cell.subTitleLbl.text = categoryDetailsArray[indexPath.row]["SubTitle"] as? String
        cell.typeNameLbl.text = categoryDetailsArray[indexPath.row]["TypeName"] as? String
        cell.typeImage.image = UIImage(named: categoryDetailsArray[indexPath.row]["TypeImage"] as! String)
        cell.typeBGImage.image = UIImage(named: categoryDetailsArray[indexPath.row]["TypeBGImage"] as! String)
        cell.descLbl.text = categoryDetailsArray[indexPath.row]["Desc"] as? String
        if isUserLogIn() == false{
            if indexPath.row == 0{
                let dcLoyalty = boldAndRegularText(text1: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discover the", value: "", table: nil))!, text2: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Loyality Program", value: "", table: nil))!)
                cell.descLbl.attributedText = dcLoyalty
            }
        }
        
        tableView.layoutIfNeeded()
        tableViewHeightConstraint.constant = tableView.contentSize.height
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categoryDetailsArray[indexPath.row]["Title"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE SMILE", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if categoryDetailsArray[indexPath.row]["Title"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bulk order delivery", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = BulkOrderDetailsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "BulkOrderDetailsVC") as! BulkOrderDetailsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if categoryDetailsArray[indexPath.row]["Title"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Coffee at Home", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = SubScriptionVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubScriptionVC") as! SubScriptionVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if categoryDetailsArray[indexPath.row]["Title"] as! String == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "dr.CAFE Catering", value: "", table: nil))!{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CateringVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CateringVC") as! CateringVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
//MARK: CollectionView Delegate Methods
@available(iOS 13.0, *)
extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView{
            return categoryArray.count
        }else if collectionView == orderTypeCollectionView{
            return orderTypesArray.count
        }else{
            bannerPageController.numberOfPages = bannerDetailsArray.count
            return bannerDetailsArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Category Details display
        if collectionView == categoryCollectionView{
            categoryCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeCategoryCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeCategoryCVCell", value: "", table: nil))!)
            let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HomeCategoryCVCell", value: "", table: nil))!, for: indexPath) as! HomeCategoryCVCell
            cell.categoryNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: categoryArray[indexPath.row]["Name"]!, value: "", table: nil))!
            cell.categoryMainImg.image = UIImage(named: categoryArray[indexPath.row]["BGImg"]!)
            cell.categoryImg.image = UIImage(named: categoryArray[indexPath.row]["topImg"]!)
            
            cell.categoryImgBGView.backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
            let width = categoryCollectionView.bounds.width
            if width > 380{
                cell.categoryNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
            }else{
                cell.categoryNameLbl.font = UIFont(name: "Avenir Book", size: 12.0)
            }
            
            if indexPath.row == 0{
                cell.categoryImg.isHidden = true
                cell.categoryNameLbl.isHidden = true
                cell.categoryImgBGView.isHidden = true
                cell.betaImg.isHidden = true
                cell.betaView.isHidden = false
                cell.categoryMainImg.isHidden = true
            }else{
                cell.categoryImg.isHidden = false
                cell.categoryNameLbl.isHidden = false
                cell.categoryImgBGView.isHidden = false
                cell.betaImg.isHidden = true
                cell.betaView.isHidden = true
                cell.categoryMainImg.isHidden = false
            }
            
            //cell.BGView.backgroundColor = .white
            return cell
        }else if collectionView == orderTypeCollectionView{
            orderTypeCollectionView.register(UINib(nibName: "OrderTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "OrderTypeCVCell")
            let cell = orderTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "OrderTypeCVCell", for: indexPath) as! OrderTypeCVCell
            
            cell.orderTypeNameLbl.text = orderTypesArray[indexPath.row]["Name"]!
            cell.orderTypeImg.image = UIImage(named: orderTypesArray[indexPath.row]["Img"]!)
            cell.orderTypeImgBGView.backgroundColor = UIColor(hexString: "#\(String(describing: AppDelegate.getDelegate().selectColor))")
            
            return cell
        }else{//Banners Display
            bannerCollectionView.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
            let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
            cell.percentNumLbl.isHidden = true
            cell.nameLbl.isHidden = true
            cell.percentNumBGView.isHidden = true
            //cell.cellBGImg.image = UIImage(named: "\(bannerDetailsArray[indexPath.row].BannerImageEn)")
            //cell.cellBGView.layer.cornerRadius = 5
            
            //Image
            //let ImgStr:NSString = "\(displayImages.images.path())\(bannerDetailsArray[indexPath.row].BannerImageEn)" as NSString
            var bannerImage = bannerDetailsArray[indexPath.row].BannerImageEn
            if AppDelegate.getDelegate().appLanguage == "English"{
                bannerImage = bannerDetailsArray[indexPath.row].BannerImageEn
            }else{
                bannerImage = bannerDetailsArray[indexPath.row].BannerImageAr
            }
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(bannerImage)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.cellBGImg.kf.setImage(with: url)
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoryCollectionView{
            let width = categoryCollectionView.bounds.width
            let height = categoryCollectionView.bounds.height
            if width > 380{
                return CGSize(width: width/2-50, height: height)
            }else{
                return CGSize(width: 130, height: height)
            }
        }else if collectionView == orderTypeCollectionView{
            let width = collectionView.frame.width
            return CGSize(width: width/4 - 10, height: orderTypeCollectionView.bounds.height)
        }else{
            let width = bannerCollectionView.bounds.width
            let height = bannerCollectionView.bounds.height
            return CGSize(width: width, height: height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Category Selection
        if collectionView == categoryCollectionView{
             if indexPath.row == 0{
                if isUserLogIn() == true{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = NewBetaDetailsVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewBetaDetailsVC") as! NewBetaDetailsVC
                    self.navigationController?.pushViewController(obj, animated: true)
                    }else{
                          let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                          var obj = LoginVC()
                          obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                          self.navigationController?.pushViewController(obj, animated: true)
                    }
            }else if indexPath.row == 1{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = SubScriptionVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "SubScriptionVC") as! SubScriptionVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 2{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ItemsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                obj.sectionType = 2
                obj.isV12 = false
                obj.selectOrderType = 4
                obj.address = ""
                obj.selectItemName = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gift & More", value: "", table: nil))!
                obj.categoryName = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gift & More", value: "", table: nil))!
                obj.categoryId = 13
                obj.storeId = 190
                obj.selectAddressID = 0
                obj.isBanner = false
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else if indexPath.row == 3{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = BulkOrderDetailsVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "BulkOrderDetailsVC") as! BulkOrderDetailsVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 4{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = CateringVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "CateringVC") as! CateringVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                print("Selected")
            }
        }else if collectionView == orderTypeCollectionView{
            if indexPath.row == 0{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = OrderTypeVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderTypeVC") as! OrderTypeVC
                obj.whereObj = 1
                obj.isLogin = false
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 1{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = StoresVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                obj.whereObj = 1
                obj.isBanner = false
                obj.orderType = 3
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 2{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = StoresVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                obj.whereObj = 1
                obj.isBanner = false
                obj.orderType = 1
                self.navigationController?.pushViewController(obj, animated: true)
            }else if indexPath.row == 3{
                if isUserLogIn() == true{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = NewOrderHistoryVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewOrderHistoryVC") as! NewOrderHistoryVC
                    self.navigationController?.pushViewController(obj, animated: true)
                }else{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = LoginVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    obj.whereObj = 11
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }else if indexPath.row == 4{
                if isUserLogIn() == true{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = NewOrderHistoryVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "NewOrderHistoryVC") as! NewOrderHistoryVC
                    obj.favourite = true
                    self.navigationController?.pushViewController(obj, animated: true)
                }else{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = LoginVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    obj.whereObj = 11
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }else{//Banner Selection functionality
            SwiftTimer.invalidate()
            //Multiple OrderTypes True functionality
            if bannerDetailsArray[indexPath.row].IsClick == true{
                if !hasLocationPermission() {
                    if AppDelegate.getDelegate().currentlocation != nil{
                        self.bannerSelection(sender:indexPath.row)
                    }else{
                        let vc = LocationAutocompleteVC(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LocationAutocompleteVC", value: "", table: nil))!, bundle: nil)
                        vc.locationAutocompleteVCDelegate = self
                        vc.whereObj = 10
                        vc.isBanner = true
                        vc.isPopup = false
                        vc.bannerId = bannerDetailsArray[indexPath.row].Id
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    self.bannerSelection(sender:indexPath.row)
                }
            }
        }
    }
    func bannerSelection(sender:Int){
        var orderTypeId:Int!
        if bannerDetailsArray[sender].DineId == true{
            orderTypeId = 1
        }else if bannerDetailsArray[sender].DriveThru == true{
            orderTypeId = 2
        }else if bannerDetailsArray[sender].TakeAway == true{
            orderTypeId = 3
        }else if bannerDetailsArray[sender].Delivery == true{
            orderTypeId = 4
        }
        if bannerDetailsArray[sender].StoreList != nil{
            if bannerDetailsArray[sender].StoreList!.count == 1{
                if bannerDetailsArray[sender].StoreList![0].StoreStatus == false{//Store is Closed
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
                }else{//Store is Open
                    for i in 0...bannerDetailsArray[sender].StoreList![0].BusySchedule!.count - 1{
                        if bannerDetailsArray[sender].StoreList![0].BusySchedule![i].OrderTypeId == orderTypeId{
                            if bannerDetailsArray[sender].StoreList![0].BusySchedule![i].IsBusy == true{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Customer flow is to high on store at the moment store will be available to take your order by", value: "", table: nil))!) \(bannerDetailsArray[sender].StoreList![0].BusySchedule![i].OpensInMinute) mins")
                                return
                            }else{
                                AppDelegate.getDelegate().cartStoreId = self.bannerDetailsArray[sender].StoreList![0].Id
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    AppDelegate.getDelegate().cartStoreName = self.bannerDetailsArray[sender].StoreList![0].NameEn
                                }else{
                                    AppDelegate.getDelegate().cartStoreName = self.bannerDetailsArray[sender].StoreList![0].NameAr
                                }
                                AppDelegate.getDelegate().selectAddress = ""
                                self.bannerSelect(sender: sender)
                            }
                        }
                    }
                }
            }else{//Multiple stores
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = StoresVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
                obj.whereObj = 2
                obj.isBanner = true
                obj.orderType = orderTypeId!
                obj.bannerId = bannerDetailsArray[sender].Id
                obj.storeDetailsArray = bannerDetailsArray[sender].StoreList!
                SaveAddressClass.selectBannerStores = [bannerDetailsArray[sender]]
                obj.itemId = bannerDetailsArray[sender].ItemId
                obj.categoryId = bannerDetailsArray[sender].CategoryId
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.itemTitle = bannerDetailsArray[sender].ItemEn
                    obj.categoryName = bannerDetailsArray[sender].CategoryEn
                }else{
                    obj.itemTitle = bannerDetailsArray[sender].ItemAr
                    obj.categoryName = bannerDetailsArray[sender].CategoryAr
                }
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            print("Not Clickable")
        }
    }
    func bannerSelect(sender:Int){
        var orderTypeId:Int!
        if bannerDetailsArray[sender].DineId == true{
            orderTypeId = 1
        }else if bannerDetailsArray[sender].DriveThru == true{
            orderTypeId = 2
        }else if bannerDetailsArray[sender].TakeAway == true{
            orderTypeId = 3
        }else if bannerDetailsArray[sender].Delivery == true{
            orderTypeId = 4
        }
        if bannerDetailsArray[sender].ItemId == 0{// single store
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = ItemsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
            obj.sectionType = 0
            obj.isV12 = false
            obj.selectOrderType = orderTypeId!
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.address = bannerDetailsArray[sender].StoreList![0].NameEn
            }else{
                obj.address = bannerDetailsArray[sender].StoreList![0].NameAr
            }
            obj.selectItemName = bannerDetailsArray[sender].BannerName
            obj.categoryId = bannerDetailsArray[sender].CategoryId
            obj.storeId = bannerDetailsArray[sender].StoreList![0].Id
            obj.selectAddressID = 0
            //obj.whereObj = 2
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.selectItemName = bannerDetailsArray[sender].MainCategoryEn
                obj.categoryName = bannerDetailsArray[sender].CategoryEn
            }else{
                obj.selectItemName = bannerDetailsArray[sender].MainCategoryAr
                obj.categoryName = bannerDetailsArray[sender].CategoryAr
            }
            obj.isBanner = true
            self.navigationController?.pushViewController(obj, animated: true)
        }else{//single store and Single item
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = AdditionalsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
            obj.itemID = bannerDetailsArray[sender].ItemId
            obj.selectOrderType = orderTypeId!
            obj.sectionType = 0
            obj.storeId = bannerDetailsArray[sender].StoreList![0].Id
            obj.selectAddressId = 0
            obj.isV12 = false
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.address = bannerDetailsArray[sender].StoreList![0].NameEn
                obj.titleLbl = bannerDetailsArray[sender].ItemEn
            }else{
                obj.address = bannerDetailsArray[sender].StoreList![0].NameAr
                obj.titleLbl = bannerDetailsArray[sender].ItemAr
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //Banner auto scrool
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == bannerCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.bannerCollectionView.indexPathsForVisibleItems
            indexes.sort()
            guard let _ = indexes.first else { /* Handle nil case */ return }
            var index = indexes.first!
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row
                }else{
                    index.row = index.row + 1
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
            }
            
            if bannerDetailsArray.count > index.row {
                DispatchQueue.main.async {
                    self.bannerCollectionView.scrollToItem(at: index, at: .right, animated: true )
                }
            }
            DispatchQueue.main.async {
                self.bannerPageController.currentPage = index.row
            }
        }
    }
}
//MARK: OrderPopUp View Swiping Delegate Methods
@available(iOS 13.0, *)
extension HomeVC : TinderSwipeViewDelegate{
    func dummyAnimationDone() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear, animations: {
        }, completion: nil)
        print("Watch out shake action")
    }
    func didSelectCard(model: Any) {
        print("Selected card")
    }
    func fallbackCard(model: Any) {
        let userModel = model as! UserOrderModel
        print("Cancelling \(userModel.BannerName)")
    }
    func cardGoesLeft(model: Any) {
        let userModel = model as! UserOrderModel
        //var index = 0
//        for i in 0...popUpDetailsArray.count-1{
//            if popUpDetailsArray[i].Id == bannerPopUpID{
//                if i > popUpDetailsArray.count-1{
//                    index = i
//                }else{
//                    index = i+1
//                }
//            }
//        }
//        bannerPopUpID = userOrderModels[index].Id
        bannerPopUpID = userModel.Id
        self.popUpUserVisibilityUpdateService(popUpId: userModel.Id)
        print("Watchout Left \(userModel.BannerName)")
    }
    func cardGoesRight(model : Any) {
        let userModel = model as! UserOrderModel
        self.popUpUserVisibilityUpdateService(popUpId:userModel.Id)
        bannerPopUpID = userModel.Id
        print("Watchout Right \(userModel.BannerName)")
        popUpSelected()
    }
    func undoCardsDone(model: Any) {
        let userModel = model as! UserOrderModel
        print("Reverting done \(userModel.BannerName)")
    }
    func endOfCardsReached() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
        }, completion: nil)
        //popUpMainView.isHidden = true
        print("End of all cards")
    }
    func currentCardStatus(card object: Any, distance: CGFloat) {
        if distance == 0 {
        }else{
            let value = Float(min(abs(distance/100), 1.0) * 5)
            let sorted = distance > 0  ? 2.5 + (value * 5) / 10  : 2.5 - (value * 5) / 10
        }
        print(distance)
    }
    //PopUp Service Integration
    func popUpUserVisibilityUpdateService(popUpId:Int){
        let dic:[String:Any] = ["UserId": UserDef.getUserId() ,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "PopupId":"\(popUpId)"]
        OrderModuleServices.PopUpUserVisibilityUpdateService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                print(data)
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
//MARK: OrderPopUp View Extension
extension UIView {
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.map { $0.superview(of: type)! }
    }
    func subview<T>(of type: T.Type) -> T? {
        return subviews.compactMap { $0 as? T ?? $0.subview(of: type) }.first
    }
}
//MARK: OrderType Popup Delegate
//@available(iOS 13.0, *)
//extension HomeVC:DropDownPopUpVCDelegate{
//    func didTapAction(ID: String, name: String, selectBannerDetailsArray: [BannerDetailsModel]!) {
//        var orderTypeId:Int!
//        if name == "DineIn"{
//            orderTypeId = 1
//        }else if name == "DriveThru"{
//            orderTypeId = 2
//        }else if name == "PickUp"{
//            orderTypeId = 3
//        }else{
//            orderTypeId = 4
//        }
//        if selectBannerDetailsArray[0].StoreList!.count == 1{
//            if selectBannerDetailsArray[0].StoreList![0].StoreStatus == false{
//                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store is Close", value: "", table: nil))!)
//            }else{
//                if selectBannerDetailsArray[0].ItemId == 0{
//                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//                    var obj = ItemsVC()
//                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
//
//                    obj.sectionType = 0
//                    obj.isV12 = false
//                    obj.selectOrderType = orderTypeId!
//                    if AppDelegate.getDelegate().appLanguage == "English"{
//                        obj.address = selectBannerDetailsArray[0].StoreList![0].NameEn
//                    }else{
//                        obj.address = selectBannerDetailsArray[0].StoreList![0].NameAr
//                    }
//                    obj.selectItemName = selectBannerDetailsArray[0].BannerName
//                    obj.categoryId = selectBannerDetailsArray[0].CategoryId
//                    obj.storeId = selectBannerDetailsArray[0].StoreList![0].Id
//                    obj.selectAddressID = 0
//                    //obj.whereObj = 2
//                    if AppDelegate.getDelegate().appLanguage == "English"{
//                        obj.selectItemName = selectBannerDetailsArray[0].MainCategoryEn
//                        obj.categoryName = selectBannerDetailsArray[0].CategoryEn
//                    }else{
//                        obj.selectItemName = selectBannerDetailsArray[0].MainCategoryAr
//                        obj.categoryName = selectBannerDetailsArray[0].CategoryAr
//                    }
//                    obj.isBanner = true
//
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }else{
//                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//                    var obj = ItemDetailsVC()
//                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
//                    obj.itemID = selectBannerDetailsArray[0].ItemId
//                    obj.selectOrderType = orderTypeId!
//                    obj.sectionType = 0
//                    obj.storeId = selectBannerDetailsArray[0].StoreList![0].Id
//                    obj.selectAddressId = 0
//                    obj.isV12 = false
//                    if AppDelegate.getDelegate().appLanguage == "English"{
//                        obj.address = selectBannerDetailsArray[0].StoreList![0].NameEn
//                        obj.titleLbl = selectBannerDetailsArray[0].ItemEn
//                    }else{
//                        obj.address = selectBannerDetailsArray[0].StoreList![0].NameAr
//                        obj.titleLbl = selectBannerDetailsArray[0].ItemAr
//                    }
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }
//            }
//        }else{
//            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
//            var obj = StoresVC()
//            obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
//            obj.whereObj = 2
//            obj.isBanner = true
//            obj.orderType = orderTypeId!
//            obj.bannerId = selectBannerDetailsArray[0].Id
//            obj.itemId = selectBannerDetailsArray[0].ItemId
//            obj.categoryId = selectBannerDetailsArray[0].CategoryId
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                obj.itemTitle = selectBannerDetailsArray[0].ItemEn
//                obj.categoryName = selectBannerDetailsArray[0].CategoryEn
//            }else{
//                obj.itemTitle = selectBannerDetailsArray[0].ItemAr
//                obj.categoryName = selectBannerDetailsArray[0].CategoryAr
//            }
//            obj.storeDetailsArray = selectBannerDetailsArray[0].StoreList!
//            SaveAddressClass.selectBannerStores = [selectBannerDetailsArray[0]]
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
//    }
//}
extension HomeVC: MaterialShowcaseDelegate {
    func showCaseDidDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
      UserDef.methodForSaveStringObjectValue("1", andKey: "MaterialShowcase")
      if showcase.primaryText == "Language" {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
          self.showcase = MaterialShowcase()
          self.showcase.setTargetView(collectionView: self.categoryCollectionView, section: 0, item: 0)
          self.showcase.backgroundViewType = .circle
          //self.showcase.targetTintColor = UIColor.clear
          //self.showcase.targetHolderColor = UIColor.clear
          self.showcase.skipButtonPosition = .belowInstruction
          self.showcase.skipButtonTitle = "Next"
          self.showcase.primaryText = "Subscription"
          self.showcase.secondaryText = "Never run out of your favorite\ncoffee with a subscription."
          self.showcase.primaryTextColor = self.showcaseTextColor
          self.showcase.secondaryTextColor = self.showcaseTextColor
          self.showcase.shouldSetTintColor = false // It should be set to false when button uses image.
          self.showcase.backgroundPromptColor = self.backgroundColor
          self.showcase.backgroundAlpha = self.backgroundAplha
          self.showcase.isTapRecognizerForTargetView = false
          self.showcase.backgroundRadius = 280
          self.showcase.delegate = self
          self.showcase.skipButton = {
            self.showcase.completeShowcase()
           }
          self.showcase.show(hasSkipButton: true) {
           }
          let button = UIButton(frame: CGRect(x: self.showcase.frame.midX+20, y: self.showcase.frame.midY + 30, width: 80, height: 40))
          button.titleLabel!.textAlignment = .right
          button.layer.borderWidth = 1
          button.layer.borderColor = UIColor.white.cgColor
          button.layer.cornerRadius = 4
          button.setTitle("Skip All", for: .normal)
          button.addTarget(self, action: #selector(self.skipAllBtn_Tapped), for: .touchUpInside)
          button.layer.borderColor = self.showcaseTextColor.cgColor
          button.setTitleColor(self.showcaseTextColor, for: .normal)
          self.showcase.addSubview(button)
        }
       }else
      if showcase.primaryText == "Subscription" {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
          self.showcase = MaterialShowcase()
          self.showcase.setTargetView(tabBar: self.tabBarController!.tabBar, itemIndex: 1, tapThrough: false)
          self.showcase.backgroundViewType = .circle
          self.showcase.targetTintColor = UIColor.clear
          self.showcase.targetHolderColor = UIColor.clear
          self.showcase.skipButtonPosition = .belowInstruction
          self.showcase.skipButtonTitle = "Next"
          self.showcase.primaryText = "Stores"
          self.showcase.secondaryText = "Discover your nearest dr.Cafe Coffee store."
          self.showcase.primaryTextColor = self.showcaseTextColor
          self.showcase.secondaryTextColor = self.showcaseTextColor
          self.showcase.shouldSetTintColor = true // It should be set to false when button uses image.
          self.showcase.backgroundPromptColor = self.backgroundColor
          self.showcase.backgroundAlpha = self.backgroundAplha
          self.showcase.isTapRecognizerForTargetView = false
          self.showcase.backgroundRadius = 300
          self.showcase.delegate = self
          self.showcase.skipButton = {
            self.showcase.completeShowcase()
          }
          self.showcase.show(hasSkipButton: true) {
          }
          let button = UIButton(frame: CGRect(x: self.showcase.frame.width - 120, y: self.showcase.frame.height - 80, width: 80, height: 40))
          button.titleLabel!.textAlignment = .right
          button.layer.borderWidth = 1
          button.layer.borderColor = UIColor.white.cgColor
          button.layer.cornerRadius = 4
          button.setTitle("Skip All", for: .normal)
          button.addTarget(self, action: #selector(self.skipAllBtn_Tapped), for: .touchUpInside)
          button.layer.borderColor = self.showcaseTextColor.cgColor
          button.setTitleColor(self.showcaseTextColor, for: .normal)
          self.showcase.addSubview(button)
        }
       }
      else if showcase.primaryText == "Stores" {
         DispatchQueue.main.asyncAfter(deadline: .now()) {
           self.showcase = MaterialShowcase()
           self.showcase.setTargetView(tabBar: self.tabBarController!.tabBar, itemIndex: 2, tapThrough: false)
           self.showcase.backgroundViewType = .circle
           self.showcase.targetTintColor = UIColor.clear
           self.showcase.targetHolderColor = UIColor.clear
           self.showcase.skipButtonPosition = .belowInstruction
           self.showcase.skipButtonTitle = "Next"
           self.showcase.primaryText = "Cart"
           self.showcase.secondaryText = "Visit & manage your shopping cart...easy as one, two, click!"
           self.showcase.primaryTextColor = self.showcaseTextColor
           self.showcase.secondaryTextColor = self.showcaseTextColor
           self.showcase.shouldSetTintColor = true // It should be set to false when button uses image.
           self.showcase.backgroundPromptColor = self.backgroundColor
           self.showcase.backgroundAlpha = self.backgroundAplha
           self.showcase.isTapRecognizerForTargetView = false
           self.showcase.backgroundRadius = 260
           self.showcase.delegate = self
           self.showcase.skipButton = {
             self.showcase.completeShowcase()
           }
           self.showcase.show(hasSkipButton: true) {
           }
           let button = UIButton(frame: CGRect(x: self.showcase.frame.width - 120, y: self.showcase.frame.height - 80, width: 80, height: 40))
           button.titleLabel!.textAlignment = .right
           button.layer.borderWidth = 1
           button.layer.borderColor = UIColor.white.cgColor
           button.layer.cornerRadius = 4
           button.setTitle("Skip All", for: .normal)
           button.addTarget(self, action: #selector(self.skipAllBtn_Tapped), for: .touchUpInside)
           button.layer.borderColor = self.showcaseTextColor.cgColor
           button.setTitleColor(self.showcaseTextColor, for: .normal)
           self.showcase.addSubview(button)
        }
       }else if showcase.primaryText == "Cart" {
         DispatchQueue.main.asyncAfter(deadline: .now()) {
           self.showcase = MaterialShowcase()
           self.showcase.setTargetView(tabBar: self.tabBarController!.tabBar, itemIndex: 3, tapThrough: false)
           self.showcase.backgroundViewType = .circle
           self.showcase.targetTintColor = UIColor.clear
           self.showcase.targetHolderColor = UIColor.clear
           self.showcase.skipButtonPosition = .belowInstruction
           self.showcase.skipButtonTitle = "Got It"
           self.showcase.primaryText = "\n\n\n\n\n\n\n\n\nRewards"
           self.showcase.secondaryText = "We value our customers. Join & start earning rewards today!"
           self.showcase.primaryTextColor = self.showcaseTextColor
           self.showcase.secondaryTextColor = self.showcaseTextColor
           self.showcase.shouldSetTintColor = true // It should be set to false when button uses image.
           self.showcase.backgroundPromptColor = self.backgroundColor
           self.showcase.backgroundAlpha = self.backgroundAplha
           self.showcase.isTapRecognizerForTargetView = false
           self.showcase.backgroundRadius = 280
           self.showcase.delegate = self
           self.showcase.skipButton = {
             self.showcase.completeShowcase()
           }
           self.showcase.show(hasSkipButton: true) {
           }
        }
       }else{
         self.showcase.isHidden = true
       }
    }
 }
extension HomeVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String, Status: String) {
//        if latitude == 0.0{
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                if Status == "Allow"{
//                    //Redirect to Settings app
//                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
//                }else if Status == "Back"{
//                    self.navigationController?.popViewController(animated: true)
//                }else{
//                    self.locationCheck()
//                }
//            }
//        }else{
//            print(MainAddress)
//            self.latitude = latitude
//            self.longitude = longitude
//            DispatchQueue.main.async() {
//                self.initGoogleMaps()
//            }
//        }
    }
}
