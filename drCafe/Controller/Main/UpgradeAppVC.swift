//
//  UpgradeAppVC.swift
//  drCafe
//
//  Created by Devbox on 12/10/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class UpgradeAppVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    @IBAction func updateNowBtn_Tapped(_ sender: Any) {
        let url = URL(string: "itms-apps://itunes.apple.com/app/id1083344883")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        self.dismiss(animated: false, completion: nil)
    }
}
