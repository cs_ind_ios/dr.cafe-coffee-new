//
//  UserActivityType.swift
//  drCafe
//
//  Created by Creative Solutions on 3/3/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import Foundation
struct UserActivityType {
    static let TrackOrder = "com.drCafe.TrackOrder"
    static let OrderHistory = "com.drCafe.OrderHistory"


}
