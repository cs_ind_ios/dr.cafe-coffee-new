//
//  StoreDetailsTVCell.swift
//  drCafe
//
//  Created by Devbox on 05/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class StoreDetailsTVCell: UITableViewCell {

    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var storeStatusBtn: UIButton!
    @IBOutlet weak var storeSelectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
