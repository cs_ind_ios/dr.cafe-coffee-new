//
//  StoresListCVCell.swift
//  drCafe
//
//  Created by Mac2 on 16/03/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class StoresListCVCell: UICollectionViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var timeOneLbl: UILabel!
    @IBOutlet weak var timeTwoLbl: UILabel!
    @IBOutlet weak var statusBtnName: UIButton!
    @IBOutlet weak var timeImg: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var viewDetailsBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
