//
//  StoreListTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 13/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class StoreListTVCell: UITableViewCell {

    @IBOutlet weak var statusBtnName: UIButton!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var v12Line: UILabel!
    @IBOutlet weak var v12NameLbl: UILabel!
    @IBOutlet weak var v12ViewWidth: NSLayoutConstraint!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var favouriteBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var timeOneLbl: UILabel!
    @IBOutlet weak var timeTwoLbl: UILabel!
    @IBOutlet weak var timeImg: UIImageView!
    
    @IBOutlet weak var facilitesStackView: UIStackView!
    @IBOutlet var faciliteisImg: [UIImageView]!
    
    @IBOutlet weak var facilitiesImgWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
