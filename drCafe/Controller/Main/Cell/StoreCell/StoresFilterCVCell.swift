//
//  StoresFilterCVCell.swift
//  drCafe
//
//  Created by Devbox on 28/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class StoresFilterCVCell: UICollectionViewCell {

    @IBOutlet weak var filterImg: UIImageView!
    @IBOutlet weak var filterNameLbl: UILabel!
    @IBOutlet weak var filterImgBGView: CustomView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
