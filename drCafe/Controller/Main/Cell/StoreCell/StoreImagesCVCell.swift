//
//  StoreImagesCVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 13/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class StoreImagesCVCell: UICollectionViewCell {

    @IBOutlet weak var storeImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
