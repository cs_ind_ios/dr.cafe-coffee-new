//
//  StoreInfoTimingsTVCell.swift
//  drCafe
//
//  Created by Devbox on 22/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class StoreInfoTimingsTVCell: UITableViewCell {

    @IBOutlet weak var dayNameLbl: UILabel!
    @IBOutlet weak var fromTimeLbl: UILabel!
    @IBOutlet weak var toNameLbl: UILabel!
    @IBOutlet weak var toTimeLbl: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
