//
//  CarsListTVCell.swift
//  drCafe
//
//  Created by mac3 on 26/10/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class CarsListTVCell: UITableViewCell {

    @IBOutlet weak var carNumberNameLbl: UILabel!
    @IBOutlet weak var carNumberLbl: UILabel!
    @IBOutlet weak var carColorNameLbl: UILabel!
    @IBOutlet weak var carColorLbl: UILabel!
    @IBOutlet weak var brandNameLbl: UILabel!
    @IBOutlet weak var brandLbl: UILabel!
    @IBOutlet weak var defaultNameLbl: UILabel!
    @IBOutlet weak var defaultCheckBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var carImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
