//
//  OrderTypeTVCell.swift
//  drCafe
//
//  Created by Mac2 on 02/03/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class OrderTypeTVCell: UITableViewCell {
    
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
