//
//  ManageCardsTVCell.swift
//  drCafe
//
//  Created by Devbox on 11/01/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class ManageCardsTVCell: UITableViewCell {

    @IBOutlet weak var nameOnCardNameLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cardNumberNameLbl: UILabel!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var expiryDateNameLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var cardDeleteBtn: UIButton!
    @IBOutlet weak var cardTypeImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
