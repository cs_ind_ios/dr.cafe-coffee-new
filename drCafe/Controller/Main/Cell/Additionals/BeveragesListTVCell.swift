//
//  BeveragesListTVCell.swift
//  drCafe
//
//  Created by Devbox on 11/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

protocol BeveragesListTVCellDelegate: AnyObject {
    func additionalButtonsTapped(cell: BeveragesListTVCell, plus:Bool, minus:Bool, tableView: UITableView)
}

class BeveragesListTVCell: UITableViewCell {

    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var itemCalLbl: UILabel!
    
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantityViewWidth: NSLayoutConstraint!
    
    weak var delegate: BeveragesListTVCellDelegate?
    var tableView:UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func plusBtn_Tapped(_ sender: Any) {
        delegate?.additionalButtonsTapped(cell: self, plus: true, minus: false, tableView: tableView)
    }
    @IBAction func minusBtn_Tapped(_ sender: Any) {
        delegate?.additionalButtonsTapped(cell: self, plus: false, minus: true, tableView: tableView)
    }
}
