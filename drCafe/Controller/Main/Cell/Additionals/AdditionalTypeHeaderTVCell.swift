//
//  AdditionalTypeHeaderTVCell.swift
//  drCafe
//
//  Created by Mac2 on 11/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class AdditionalTypeHeaderTVCell: UITableViewCell {

    @IBOutlet weak var titleNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
