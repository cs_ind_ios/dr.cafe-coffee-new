//
//  BeveragesVCSizeTVCell.swift
//  drCafe
//
//  Created by Devbox on 15/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class BeveragesVCSizeTVCell: UITableViewCell {

    @IBOutlet weak var sizeNameLbl: UILabel!
    @IBOutlet weak var sizePriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
