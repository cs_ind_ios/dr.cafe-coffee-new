//
//  AdditionalTypeTVCell.swift
//  drCafe
//
//  Created by Mac2 on 11/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

protocol AdditionalTypeTVCellDelegate: AnyObject {
    func collectionView(collectionviewcell: AdditionalLatteCVCell?, index: Int, didTappedInTableViewCell: AdditionalTypeTVCell,btnName: String, isRecommend: Bool, section:Int)
}

class AdditionalTypeTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var additionalTypeTVCellDelegate:AdditionalTypeTVCellDelegate!
    var itemsArray:[AdditionalsModel]?
    var selectSizeId = 0
    var isRecommend:Bool!
    var section:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            collectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            collectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        // Register the xib for collection view cell
        let cellNib = UINib(nibName: "AdditionalLatteCVCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "AdditionalLatteCVCell")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}
extension AdditionalTypeTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func updateCellWith(row: [AdditionalsModel]) {
        self.itemsArray = row
        self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsArray?.count ?? 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdditionalLatteCVCell", for: indexPath) as? AdditionalLatteCVCell{
            let dic = itemsArray![indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nameLbl.text = dic.NameEn
            }else{
                cell.nameLbl.text = dic.NameAr
            }
            
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic.Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)

            let priceArray = itemsArray!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
            if priceArray.count > 0{
                let pricearray =  dic.AdditionalPrices!.filter({$0.Price > 0})
                if pricearray.count > 0{
                    let priceDetails = dic.AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        cell.priceLbl.text = "\(priceDetails[0].Price.withCommas())"
                    }else{
                        cell.priceLbl.text = "0.00"
                    }
                }else{
                    cell.priceLbl.text = "0.00"
                }
                cell.priceLblHeight.constant = 22
                cell.priceLblBottomConstraint.constant = 5
            }else{
                cell.priceLbl.text = ""
                cell.priceLblHeight.constant = 0
                cell.priceLblBottomConstraint.constant = 0
            }
            
            //cell.selectBtn.tag = indexPath.row
            //cell.selectBtn.addTarget(self, action: #selector(latteSelectBtn_Tapped(_:)), for: .touchUpInside)
            
            if dic.IsOutOfStock! == 1{
                cell.soldOutImg.isHidden = false
                cell.soldOutView.isHidden = false
                cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }else{
                cell.soldOutImg.isHidden = true
                cell.soldOutView.isHidden = true
                if dic.ItemSelect! == true{
                    cell.selectBtn.setImage(UIImage(named: "Select"), for: .normal)
                    cell.BGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                    cell.BGView.shadowColor1 = .lightGray
                    cell.BGView.shadowOpacity1 = 0.6
                }else{
                    cell.selectBtn.setImage(UIImage(named: "Un Select"), for: .normal)
                    cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                    cell.BGView.shadowOpacity1 = 0
                    cell.BGView.shadowOpacity = 0
                }
            }
            
            //Quantity Functionality
            cell.plusBtn.tag = indexPath.row
            cell.plusBtn.addTarget(self, action: #selector(lattePlusBtnTapped(_:)), for: .touchUpInside)
            cell.minusBtn.tag = indexPath.row
            cell.minusBtn.addTarget(self, action: #selector(latteMinusBtnTapped(_:)), for: .touchUpInside)
            if dic.MaxQty > 1{
                cell.quantityViewHeight.constant = 30
                cell.quantityViewTopConstraint.constant = 0
                cell.quantityViewBtmConstraint.constant = 15
                cell.quantityLbl.text = "\(dic.additionalQty!)"
            }else{
                cell.quantityViewHeight.constant = 0
                cell.quantityViewTopConstraint.constant = 0
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? AdditionalLatteCVCell
        self.additionalTypeTVCellDelegate.collectionView(collectionviewcell: cell, index: indexPath.item, didTappedInTableViewCell: self, btnName: "Select", isRecommend: isRecommend, section: section)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let priceArray =  itemsArray!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
        let quantityArray =  itemsArray!.filter({$0.MaxQty > 1})
        var quantityHeight = 0
        if quantityArray.count > 0{
            quantityHeight = 40
        }else{
            quantityHeight = 0
        }
        var priceHeight = 0
        if priceArray.count > 0{
            priceHeight = 32
        }else{
            priceHeight = 0
        }
        return CGSize(width: Int(width)/4, height: 121 + priceHeight + quantityHeight)
//        let width = collectionView.frame.width
//        let priceArray =  itemsArray!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
//        if priceArray.count > 0{
//            if itemsArray![indexPath.row].MaxQty > 1{
//                return CGSize(width: width/4, height: 193)
//            }else{
//                return CGSize(width: width/4, height: 153)
//            }
//        }else{
//            if itemsArray![indexPath.row].MaxQty > 1{
//                return CGSize(width: width/4, height: 161)
//            }else{
//                return CGSize(width: width/4, height: 121)
//            }
//        }
    }
    //MARK: Latte cell Button Actions
    @objc func lattePlusBtnTapped(_ sender: UIButton){
        let indexpath = IndexPath(item: sender.tag, section: 0)
        let cell = collectionView.cellForItem(at: indexpath) as? AdditionalLatteCVCell
        self.additionalTypeTVCellDelegate.collectionView(collectionviewcell: cell, index: sender.tag, didTappedInTableViewCell: self, btnName: "Plus", isRecommend: isRecommend, section: section)
    }
    @objc func latteMinusBtnTapped(_ sender: UIButton){
        let indexpath = IndexPath(item: sender.tag, section: 0)
        let cell = collectionView.cellForItem(at: indexpath) as? AdditionalLatteCVCell
        self.additionalTypeTVCellDelegate.collectionView(collectionviewcell: cell, index: sender.tag, didTappedInTableViewCell: self, btnName: "Minus", isRecommend: isRecommend, section: section)
    }
}
