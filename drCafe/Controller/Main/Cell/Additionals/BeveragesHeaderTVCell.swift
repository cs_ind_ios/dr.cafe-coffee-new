//
//  BeveragesHeaderTVCell.swift
//  drCafe
//
//  Created by Devbox on 11/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class BeveragesHeaderTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dropDownBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
