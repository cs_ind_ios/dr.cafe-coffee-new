//
//  RecommendedBeansCVCell.swift
//  drCafe
//
//  Created by Devbox on 18/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class RecommendedBeansCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBGView: CardViewAllSides!
    @IBOutlet weak var BGView: CardView!
    @IBOutlet weak var recommendedImg: UIImageView!
    @IBOutlet weak var beanTypeImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceLblHeight: NSLayoutConstraint!
    @IBOutlet weak var priceBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var recommendedImgHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
