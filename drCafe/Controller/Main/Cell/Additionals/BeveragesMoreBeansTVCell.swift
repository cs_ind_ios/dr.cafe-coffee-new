//
//  BeveragesMoreBeansTVCell.swift
//  drCafe
//
//  Created by Devbox on 23/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class BeveragesMoreBeansTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var beanImg: UIImageView!
    @IBOutlet weak var beanInfoBtn: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
