//
//  AdditionalLatteCVCell.swift
//  drCafe
//
//  Created by Devbox on 05/11/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class AdditionalLatteCVCell: UICollectionViewCell {

    @IBOutlet weak var imgBGView: CardViewAllSides!
    @IBOutlet weak var BGView: CardView!
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLblHeight: NSLayoutConstraint!
    @IBOutlet weak var priceLblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var quantityViewHeight: NSLayoutConstraint!
    @IBOutlet weak var quantityViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var quantityViewBtmConstraint: NSLayoutConstraint!
    @IBOutlet weak var soldOutImg: UIImageView!
    @IBOutlet weak var soldOutView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
