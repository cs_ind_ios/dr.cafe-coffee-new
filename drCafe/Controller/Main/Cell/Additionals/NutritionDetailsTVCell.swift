//
//  NutritionDetailsTVCell.swift
//  drCafe
//
//  Created by Devbox on 30/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class NutritionDetailsTVCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var nutritionNameLbl: UILabel!
    @IBOutlet weak var nutritionCalLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
