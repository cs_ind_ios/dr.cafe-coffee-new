//
//  GrindingMethodTVCell.swift
//  drCafe
//
//  Created by Devbox on 04/06/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class GrindingMethodTVCell: UITableViewCell {

    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var grindImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
