//
//  ManageAddressTVCell.swift
//  drCafe
//
//  Created by Devbox on 08/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class ManageAddressTVCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var addressBGView: CardViewAllSides!
    
    @IBOutlet weak var addressTypeLbl: UILabel!
    @IBOutlet var btnBGView: [CardView]!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var defaultLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
