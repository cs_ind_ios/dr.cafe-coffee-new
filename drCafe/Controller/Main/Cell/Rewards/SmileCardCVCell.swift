//
//  SmileCardCVCell.swift
//  drCafe
//
//  Created by Devbox on 24/09/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

class SmileCardCVCell: UICollectionViewCell {
    
    @IBOutlet weak var rewardCardBGImage: UIImageView!
    @IBOutlet weak var earnSmilesNameLbl: UILabel!
    @IBOutlet weak var mySmilesLbl: UILabel!
    @IBOutlet weak var validThruLbl: UILabel!
    @IBOutlet weak var smileLevelLbl: UILabel!
    @IBOutlet weak var mySmilesNameLbl: UILabel!
    @IBOutlet weak var validThruNameLbl: UILabel!
    @IBOutlet weak var smileLevelNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
