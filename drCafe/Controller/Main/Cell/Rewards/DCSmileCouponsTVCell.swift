//
//  DCSmileCouponsTVCell.swift
//  drCafe
//
//  Created by Devbox on 03/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

protocol DCSmileCouponsTVCellDelegate: AnyObject {
    func MoreInfoButtonTapped(cell: DCSmileCouponsTVCell, MoreInfoBtn:Bool, tableView: UITableView)
}

class DCSmileCouponsTVCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var moreInfoBtn: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var validForNameLbl: UILabel!
    @IBOutlet weak var validLbl: UILabel!
    @IBOutlet weak var availableNameLbl: UILabel!
    @IBOutlet weak var availableDrinksLbl: UILabel!
    @IBOutlet weak var availableDrinksViewHeight: NSLayoutConstraint!
    
    weak var delegate: DCSmileCouponsTVCellDelegate?
    var tableView:UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func moreInfoBtn_Tapped(_ sender: Any) {
        delegate?.MoreInfoButtonTapped(cell: self, MoreInfoBtn: true, tableView: tableView)
    }
}
