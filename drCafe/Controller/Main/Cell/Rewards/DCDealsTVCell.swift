//
//  DCDealsTVCell.swift
//  drCafe
//
//  Created by Mac2 on 04/05/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class DCDealsTVCell: UITableViewCell {

    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var BGImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
