//
//  SettingsVCTVCell.swift
//  drCafe
//
//  Created by Devbox on 16/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class SettingsVCTVCell: UITableViewCell {

    @IBOutlet var BGView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
