//
//  DropVCTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 29/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class DropVCTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var calariesLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
