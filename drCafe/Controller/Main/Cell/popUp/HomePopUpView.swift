//
//  HomePopUpView.swift
//  TinderSwipeView_Example
//
//  Created by Devbox on 08/10/20.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class HomePopUpView: UIView {
        
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var bannerImg: UIImageView!
       
    var userOrderModel : UserOrderModel! {
        didSet{
            print(self.attributeStringForModel(userModel: userOrderModel))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(HomePopUpView.className, owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    private func attributeStringForModel(userModel:UserOrderModel) -> NSAttributedString{
        let attributedText = NSMutableAttributedString(string: userModel.BannerName, attributes: [.foregroundColor: UIColor.black,.font:UIFont.boldSystemFont(ofSize: 16)])
        return attributedText
    }
}

extension UIView{
    func fixInView(_ container: UIView!) -> Void{

        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}

extension NSObject {

    class var className: String {
        return String(describing: self)
    }
}
