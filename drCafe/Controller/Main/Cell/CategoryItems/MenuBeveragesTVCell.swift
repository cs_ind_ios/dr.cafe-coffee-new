//
//  MenuBeveragesTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 17/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class MenuBeveragesTVCell: UITableViewCell {

    @IBOutlet weak var beveragesBGView: UIView!
    @IBOutlet weak var titleNameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var beverageImg: UIImageView!
    @IBOutlet weak var nextArrowBtn: UIButton!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var subViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subViewTitleLbl: UILabel!
    @IBOutlet weak var subViewSubTitleLbl: UILabel!
    @IBOutlet weak var subDescLbl: UILabel!
    @IBOutlet weak var subViewImg: UIImageView!
    
    @IBOutlet weak var categoryColorLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
