//
//  ImagesCell.swift
//  drCafe
//
//  Created by Creative Solutions on 3/11/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class ImagesCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playerView: PlayerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
