//
//  ItemsTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 21/05/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class ItemsTVCell: UITableViewCell {

    @IBOutlet weak var addNameLbl: UILabel!
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDiscLbl: UILabel!
    @IBOutlet weak var itemDescLblHeight: NSLayoutConstraint!
    @IBOutlet weak var itemCalLbl: UILabel!
    @IBOutlet weak var itemCalLblHeight: NSLayoutConstraint!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var addBtnView: UIView!
    @IBOutlet weak var itemQuantityLbl: UILabel!
    @IBOutlet weak var quantityNumView: UIView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var itemPriceLblHeight: NSLayoutConstraint!
    @IBOutlet weak var currencyLblHeight: NSLayoutConstraint!
    @IBOutlet weak var quantityNumViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addBtnViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
