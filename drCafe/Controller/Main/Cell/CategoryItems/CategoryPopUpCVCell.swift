//
//  CategoryPopUpCVCell.swift
//  drCafe
//
//  Created by Devbox on 09/10/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class CategoryPopUpCVCell: UICollectionViewCell {

    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var leftLineLbl: UILabel!
    @IBOutlet weak var rightLineLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
