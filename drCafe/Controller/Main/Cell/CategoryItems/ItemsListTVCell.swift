//
//  ItemsListTVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 19/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
//1. call delegate method
protocol ItemsListTVCellDelegate:AnyObject {
    func SelectPlayBtn_tapped(cell: UITableViewCell)
}
protocol TagsCVCellDelegate: AnyObject {
    func collectionView(collectionviewcell: TagsCVCell?, index: Int, didTappedInTableViewCell: ItemsListTVCell)
}
class ItemsListTVCell: UITableViewCell {
    
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var starBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    
    @IBOutlet weak var outOfStockLbl: UILabel!
    @IBOutlet weak var outOfStockLblHeight: NSLayoutConstraint!
    @IBOutlet weak var outOfStockLblWidth: NSLayoutConstraint!
    @IBOutlet weak var outOfStockLblTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var soldOutLbl: UILabel!
    
    weak var delegate: ItemsListTVCellDelegate?
    var rowWithTags: [TagsModel]?
    weak var cellDelegate: TagsCVCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            tagsCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            tagsCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 33, height: 33)
        flowLayout.minimumLineSpacing = 2.0
        flowLayout.minimumInteritemSpacing = 5.0
        self.tagsCollectionView.collectionViewLayout = flowLayout
        self.tagsCollectionView.showsHorizontalScrollIndicator = false
        
        self.tagsCollectionView.dataSource = self
        self.tagsCollectionView.delegate = self
                
        // Register the xib for collection view cell
        let cellNib = UINib(nibName: "TagsCVCell", bundle: nil)
        self.tagsCollectionView.register(cellNib, forCellWithReuseIdentifier: "TagsCVCell")
        
    }
    @IBAction func SelectPlayBtn_tapped(sender: AnyObject) {
        //4. call delegate method
        //check delegate is not nil with `?`
        delegate?.SelectPlayBtn_tapped(cell: self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
extension ItemsListTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // The data we passed from the TableView send them to the CollectionView Model
    func updateCellWith(row: [TagsModel]) {
        self.rowWithTags = row
        self.tagsCollectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.rowWithTags?.count ?? 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    // Set the data for each cell (color and color name)
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagsCVCell", for: indexPath) as? TagsCVCell {
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(rowWithTags![indexPath.item].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.img.kf.setImage(with: url)
            return cell
        }
        return UICollectionViewCell()
    }
    
    // Add spaces at the beginning and the end of the collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let cell = collectionView.cellForItem(at: indexPath) as? TagsCVCell
            self.cellDelegate?.collectionView(collectionviewcell: cell, index: indexPath.item, didTappedInTableViewCell: self)
        }
}
