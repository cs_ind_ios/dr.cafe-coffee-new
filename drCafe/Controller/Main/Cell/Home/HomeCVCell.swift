//
//  HomeCVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 16/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class HomeCVCell: UICollectionViewCell {

    @IBOutlet weak var cellBGImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var percentNumLbl: UILabel!
    @IBOutlet weak var percentNumBGView: UIView!
    @IBOutlet weak var cellBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
