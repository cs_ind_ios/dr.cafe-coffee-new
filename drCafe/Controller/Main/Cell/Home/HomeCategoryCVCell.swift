//
//  HomeCategoryCVCell.swift
//  Dr.Cafe
//
//  Created by Devbox on 01/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

class HomeCategoryCVCell: UICollectionViewCell {

    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryMainImg: UIImageView!
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var categoryImgBGView: UIView!
    @IBOutlet weak var betaNameLbl: UILabel!
    @IBOutlet weak var betaImg: UIImageView!
    @IBOutlet weak var betaView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
