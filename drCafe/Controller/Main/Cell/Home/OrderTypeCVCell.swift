//
//  OrderTypeCVCell.swift
//  drCafe
//
//  Created by Mac2 on 05/04/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class OrderTypeCVCell: UICollectionViewCell {

    @IBOutlet weak var orderTypeImg: UIImageView!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var orderTypeImgBGView: CustomView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
