//
//  HomeVCCategoryTypeTVCell.swift
//  drCafe
//
//  Created by Mac2 on 10/02/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit

class HomeVCCategoryTypeTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var typeNameLbl: UILabel!
    @IBOutlet weak var typeBGImage: UIImageView!
    @IBOutlet weak var typeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
