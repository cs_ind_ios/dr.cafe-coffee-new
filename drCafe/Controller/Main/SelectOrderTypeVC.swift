//
//  SelectOrderTypeVC.swift
//  drCafe
//
//  Created by Devbox on 03/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SelectOrderTypeVC: UIViewController,UITabBarControllerDelegate {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var dineInLbl: UILabel!
    @IBOutlet weak var pickupLbl: UILabel!
    @IBOutlet weak var deliveryLbl: UILabel!
    @IBOutlet weak var dineInBGView: UIView!
    @IBOutlet weak var pickupBGView: UIView!
    @IBOutlet weak var deliveryBGView: UIView!
    
    var whereObj = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if whereObj == 1{
            backBtn.isHidden = true
        }else{
            backBtn.isHidden = false
        }
        self.tabBarController?.delegate = self
        
        pickupLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
        dineInLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Dine In", value: "", table: nil))!
        deliveryLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!

    }
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if self.tabBarController?.selectedIndex == 2 {
            if AppDelegate.getDelegate().cartQuantity > 0{
                self.navigationController?.popToRootViewController(animated: false)
            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if whereObj == 1{
            self.navigationController?.popToRootViewController(animated: true)
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 0
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if whereObj == 1{
            tabBarController?.tabBar.isHidden = false
        }else{
            tabBarController?.tabBar.isHidden = true
        }
        
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
        }
        dineInLbl.textColor = .black
        pickupLbl.textColor = .black
        deliveryLbl.textColor = .black
        dineInBGView.backgroundColor = .white
        pickupBGView.backgroundColor = .white
        deliveryBGView.backgroundColor = .white
    }
    //MARK: DineIn Button Action
    @IBAction func dineInBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = StoresVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
        obj.whereObj = 1
        obj.isBanner = false
        obj.orderType = 1
        obj.isHome = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Pickup Button Action
    @IBAction func pickupBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = StoresVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
        obj.whereObj = 1
        obj.isBanner = false
        obj.orderType = 3
        obj.isHome = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Delivery Button Action
    @IBAction func deliveryBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = OrderTypeVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderTypeVC") as! OrderTypeVC
            obj.whereObj = 1
            obj.isLogin = false
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
