//
//  AboutDCSmileVC.swift
//  drCafe
//
//  Created by Devbox on 31/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class AboutDCSmileVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var navigationBarDescLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var categoryMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var categoryOneMainView: UIView!
    @IBOutlet weak var categoryOneBtn: UIButton!
    @IBOutlet weak var categoryNameOneLbl: UILabel!
    @IBOutlet weak var categoryOneImgBGView: CustomView!
    @IBOutlet weak var categoryTwoMainView: UIView!
    @IBOutlet weak var categoryNameTwoLbl: UILabel!
    @IBOutlet weak var categoryTwoImgBGView: CustomView!
    @IBOutlet weak var categoryThreeMainView: UIView!
    @IBOutlet weak var categoryNameThreeLbl: UILabel!
    @IBOutlet weak var categoryThreeImgBGView: CustomView!
    @IBOutlet weak var categoryFourMainView: UIView!
    @IBOutlet weak var categoryNameFourLbl: UILabel!
    @IBOutlet weak var categoryFourImgBGView: CustomView!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var selectCategoryNameBGView: UIView!
    
    @IBOutlet weak var congratesNameLbl: UILabel!
    @IBOutlet weak var congratesDescLbl: UILabel!
    @IBOutlet weak var congratesPopUpView: UIView!
    @IBOutlet weak var congratesPopUpViewHeight: NSLayoutConstraint!
    @IBOutlet weak var congratesPopUpViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var titleInfoNameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    var detailsArray = [DCSmileTiersDetailsModel]()
    var badgeID = 0
    var mySmiles = 0.0
    var validThru = ""
    var smileLevel = ""
    var earnMsg = ""
    var desc = ""
    var popover = Popover()
    
    //var cardsArray = ["standard","Platinum","Gold","Elite"]
    var cardsArray = ["dcRewards Coffee Lover","dcRewards Coffee Expert","dcRewards Coffee Master","dcRewards Coffee Guru"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About dr.CAFE REWARDS", value: "", table: nil))!
        navigationBarDescLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Welcome to dr Cafe loyalty program", value: "", table: nil))!
    }
    override func viewWillAppear(_ animated: Bool) {
        if detailsArray.count == 4{
            if AppDelegate.getDelegate().appLanguage == "English"{
                categoryNameOneLbl.text = detailsArray[0].NameEn
                categoryNameTwoLbl.text = detailsArray[1].NameEn
                categoryNameThreeLbl.text = detailsArray[2].NameEn
                categoryNameFourLbl.text = detailsArray[3].NameEn
                titleInfoNameLbl.text = "\(detailsArray[0].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            }else{
                categoryNameOneLbl.text = detailsArray[0].NameAr
                categoryNameTwoLbl.text = detailsArray[1].NameAr
                categoryNameThreeLbl.text = detailsArray[2].NameAr
                categoryNameFourLbl.text = detailsArray[3].NameAr
                titleInfoNameLbl.text = "\(detailsArray[0].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            }
        }
        //titleInfoNameLbl.textColor = #colorLiteral(red: 0.1019607843, green: 0.3882352941, blue: 0.4588235294, alpha: 1)
        selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.3882352941, blue: 0.4588235294, alpha: 1)
        congratesPopUpView.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
        DispatchQueue.main.async {
            self.categoryOneBtn_Tapped(self.categoryOneBtn)
        }
        
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            htmlView(htmlString: detailsArray[0].DescriptionEn)
        }else{
            htmlView(htmlString: detailsArray[0].DescriptionAr)
        }
        
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func categoryOneBtn_Tapped(_ sender: UIButton) {
        //titleInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Lover Rewards Info", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleInfoNameLbl.text = "\(detailsArray[0].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[0].DescriptionEn)
        }else{
            titleInfoNameLbl.text = "\(detailsArray[0].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[0].DescriptionAr)
        }
        selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.3882352941, blue: 0.4588235294, alpha: 1)
        congratesPopUpView.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.9176470588, blue: 0.9647058824, alpha: 1)
        DispatchQueue.main.async {
            self.popUpDisplay(self.categoryNameFourLbl, Id: 0)
        }
    }
    @IBAction func categoryTwoBtn_Tapped(_ sender: UIButton) {
        //titleInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Expert Rewards Info", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleInfoNameLbl.text = "\(detailsArray[1].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[1].DescriptionEn)
        }else{
            titleInfoNameLbl.text = "\(detailsArray[1].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[1].DescriptionAr)
        }
        selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
        congratesPopUpView.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
        DispatchQueue.main.async {
            self.popUpDisplay(self.categoryNameFourLbl, Id: 1)
        }
    }
    @IBAction func categoryThreeBtn_Tapped(_ sender: UIButton) {
        //titleInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Master Rewards Info", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleInfoNameLbl.text = "\(detailsArray[2].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[2].DescriptionEn)
        }else{
            titleInfoNameLbl.text = "\(detailsArray[2].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[2].DescriptionAr)
        }
        selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.5960784314, blue: 0.3607843137, alpha: 1)
        congratesPopUpView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.8156862745, blue: 0.6039215686, alpha: 1)
        DispatchQueue.main.async {
            self.popUpDisplay(self.categoryNameFourLbl, Id: 2)
        }
    }
    @IBAction func categoryFourBtn_Tapped(_ sender: UIButton) {
        //titleInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coffee Guru Rewards Info", value: "", table: nil))!
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleInfoNameLbl.text = "\(detailsArray[3].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[3].DescriptionEn)
        }else{
            titleInfoNameLbl.text = "\(detailsArray[3].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
            htmlView(htmlString: detailsArray[3].DescriptionAr)
        }
        selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.3960784314, blue: 0.3529411765, alpha: 1)
        congratesPopUpView.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.6156862745, blue: 0.5647058824, alpha: 1)
        DispatchQueue.main.async {
            self.popUpDisplay(self.categoryNameFourLbl, Id: 3)
        }
    }
    func htmlView(htmlString:String){
        // works even without <html><body> </body></html> tags, BTW
        let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        // suppose we have an UILabel, but any element with NSAttributedString will do
        self.descLbl.attributedText = attrStr
    }
    func popUpDisplay(_ sender: UILabel,Id:Int){
        self.popView.layer.sublayers?.removeAll()
        congratesNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Congrats", value: "", table: nil))!
        congratesDescLbl.text = desc
            
        if self.detailsArray[Id].Id == self.badgeID {
            self.categoryMainViewHeight.constant = 215
            self.congratesPopUpViewHeight.constant = 100
            self.congratesPopUpViewBottom.constant = 15
        }else{
            self.congratesPopUpViewHeight.constant = 0
            self.congratesPopUpViewBottom.constant = 0
            self.categoryMainViewHeight.constant = 100
            self.popView.backgroundColor = .clear
        }
        var lblWidth = categoryNameOneLbl.frame.midX
        var selectColor = #colorLiteral(red: 0.7058823529, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
        if Id == 0{
            congratesPopUpView.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.9176470588, blue: 0.9647058824, alpha: 1)
            congratesNameLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
            congratesDescLbl.textColor = #colorLiteral(red: 0.1764705882, green: 0.431372549, blue: 0.5098039216, alpha: 1)
            selectColor = #colorLiteral(red: 0.768627451, green: 0.9176470588, blue: 0.9647058824, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                lblWidth = categoryNameOneLbl.frame.midX + 10
            }else{
                let width = view.frame.width/4
                lblWidth = (3 * width) + categoryNameOneLbl.frame.midX + 10
            }
        }else if Id == 1{
            congratesPopUpView.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
            congratesNameLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
            congratesDescLbl.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
            selectColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                let width = view.frame.width/4
                lblWidth = width + categoryNameTwoLbl.frame.midX + 10
            }else{
                let width = view.frame.width/4
                lblWidth = (2 * width) + categoryNameTwoLbl.frame.midX + 10
            }
        }else if Id == 2{
            congratesPopUpView.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.8156862745, blue: 0.6039215686, alpha: 1)
            congratesNameLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
            congratesDescLbl.textColor = #colorLiteral(red: 0.5137254902, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
            selectColor = #colorLiteral(red: 0.9058823529, green: 0.8156862745, blue: 0.6039215686, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                let width = view.frame.width/4
                lblWidth = (2 * width) + categoryNameThreeLbl.frame.midX + 10
            }else{
                let width = view.frame.width/4
                lblWidth = width + categoryNameThreeLbl.frame.midX + 10
            }
        }else if Id == 3{
            congratesPopUpView.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.6156862745, blue: 0.5647058824, alpha: 1)
            congratesNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            congratesDescLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            selectColor = #colorLiteral(red: 0.8823529412, green: 0.6156862745, blue: 0.5647058824, alpha: 1)
            if AppDelegate.getDelegate().appLanguage == "English"{
                let width = view.frame.width/4
                lblWidth = (3 * width) + categoryNameFourLbl.frame.midX + 10
            }else{
                lblWidth = categoryNameFourLbl.frame.midX + 10
            }
        }
        if detailsArray[Id].Id == badgeID {
            let height = popView.frame.height
            popView.drawLineFromPoint(start: CGPoint(x: 0, y: height), curveStart: CGPoint(x: lblWidth - 13, y: height), curveHeight: CGPoint(x: lblWidth, y: height - 10), curveEnd: CGPoint(x: lblWidth + 13, y: height), toPoint: CGPoint(x: UIScreen.main.bounds.width - 20, y: height), ofColor: selectColor, inView: popView)
        }
        DispatchQueue.main.async {
            if self.detailsArray[Id].Id == self.badgeID {
                self.categoryMainViewHeight.constant = 215
                self.congratesPopUpViewHeight.constant = 100
                self.congratesPopUpViewBottom.constant = 15
                self.congratesPopUpView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
                self.popView.roundCorners(corners: [.topLeft,.topRight], radius: 15)
            }else{
                self.congratesPopUpViewHeight.constant = 0
                self.congratesPopUpViewBottom.constant = 0
                self.categoryMainViewHeight.constant = 100
                self.popView.backgroundColor = .clear
            }
        }
    }
}
extension UIView{
    func drawLineFromPoint(start : CGPoint, curveStart : CGPoint, curveHeight : CGPoint, curveEnd : CGPoint, toPoint end:CGPoint, ofColor lineColor: UIColor, inView view:UIView) {
        let bezierPath = UIBezierPath()
        bezierPath.move(to: start)
        bezierPath.addLine(to: curveStart)
        bezierPath.addLine(to: curveHeight)
        bezierPath.addLine(to: curveEnd)
        bezierPath.addLine(to: end)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.cgPath
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.fillColor = nil
        shapeLayer.lineWidth = 15
        self.layer.addSublayer(shapeLayer)
    }
    func removeLineFromPoint(ofColor lineColor: UIColor, inView view:UIView) {
        let bezierPath = UIBezierPath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.cgPath
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.fillColor = lineColor.cgColor
        shapeLayer.lineWidth = 1
        self.layer.addSublayer(shapeLayer)
        self.setNeedsDisplay()
    }
}
//MARK: View Round Corners
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
extension AboutDCSmileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        categoryCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SmileCardCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SmileCardCVCell", value: "", table: nil))!)
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SmileCardCVCell", value: "", table: nil))!, for: indexPath) as! SmileCardCVCell
        cell.rewardCardBGImage.image = UIImage(named: cardsArray[indexPath.row])
        cell.mySmilesNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Points", value: "", table: nil))!
        cell.validThruNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Valid Till", value: "", table: nil))!
        cell.smileLevelNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Reward Level", value: "", table: nil))!
        if badgeID == 1{
            if indexPath.row == 0{
                cell.mySmilesLbl.text = "\(mySmiles)"
                cell.validThruLbl.text = validThru
                cell.smileLevelLbl.text = smileLevel
                cell.earnSmilesNameLbl.text = earnMsg
            }else{
                cell.mySmilesLbl.text = ""
                cell.validThruLbl.text = ""
                cell.smileLevelLbl.text = ""
                cell.earnSmilesNameLbl.text = ""
            }
        }else if badgeID == 2{
            if indexPath.row == 1{
                cell.mySmilesLbl.text = "\(mySmiles)"
                cell.validThruLbl.text = validThru
                cell.smileLevelLbl.text = smileLevel
                cell.earnSmilesNameLbl.text = earnMsg
            }else{
                cell.mySmilesLbl.text = ""
                cell.validThruLbl.text = ""
                cell.smileLevelLbl.text = ""
                cell.earnSmilesNameLbl.text = ""
            }
        }else if badgeID == 3{
            if indexPath.row == 2{
                cell.mySmilesLbl.text = "\(mySmiles)"
                cell.validThruLbl.text = validThru
                cell.smileLevelLbl.text = smileLevel
                cell.earnSmilesNameLbl.text = earnMsg
            }else{
                cell.mySmilesLbl.text = ""
                cell.validThruLbl.text = ""
                cell.smileLevelLbl.text = ""
                cell.earnSmilesNameLbl.text = ""
            }
        }else if badgeID == 4{
            if indexPath.row == 3{
                cell.mySmilesLbl.text = "\(mySmiles)"
                cell.validThruLbl.text = validThru
                cell.smileLevelLbl.text = smileLevel
                cell.earnSmilesNameLbl.text = earnMsg
            }else{
                cell.mySmilesLbl.text = ""
                cell.validThruLbl.text = ""
                cell.smileLevelLbl.text = ""
                cell.earnSmilesNameLbl.text = ""
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = categoryCollectionView.frame.width
        let height = categoryCollectionView.frame.height
        return CGSize(width: width - 30, height: height)
    }
    //Banner auto scrool
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == categoryCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.categoryCollectionView.indexPathsForVisibleItems
            indexes.sort()
            var index = indexes.first!
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row
                }else{
                    index.row = index.row + 1
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
            }
            if cardsArray.count > index.row {
                DispatchQueue.main.async {
                    self.categoryCollectionView.scrollToItem(at: index, at: .right, animated: true )
                }
            }
            DispatchQueue.main.async {
                if index.row == 0{
                    self.selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.3882352941, blue: 0.4588235294, alpha: 1)
                }else if index.row == 1{
                    self.selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.3647058824, blue: 0.3647058824, alpha: 1)
                }else if index.row == 2{
                    self.selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.5960784314, blue: 0.3607843137, alpha: 1)
                }else if index.row == 3{
                    self.selectCategoryNameBGView.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.3960784314, blue: 0.3529411765, alpha: 1)
                }
                var htmlString = ""
                if AppDelegate.getDelegate().appLanguage == "English"{
                    if index.row == 0{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[0].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[0].DescriptionEn
                    }else if index.row == 1{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[1].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[1].DescriptionEn
                    }else if index.row == 2{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[2].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[2].DescriptionEn
                    }else if index.row == 3{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[3].NameEn) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[3].DescriptionEn
                    }
                }else{
                    if index.row == 0{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[0].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[0].DescriptionAr
                    }else if index.row == 1{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[1].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[1].DescriptionAr
                    }else if index.row == 2{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[2].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[2].DescriptionAr
                    }else if index.row == 3{
                        self.titleInfoNameLbl.text = "\(self.detailsArray[3].NameAr) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rewards Info", value: "", table: nil))!)"
                        htmlString = self.detailsArray[3].DescriptionAr
                    }
                }
                // works even without <html><body> </body></html> tags, BTW
                let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
                let attrStr = try? NSAttributedString( // do catch
                    data: data,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                // suppose we have an UILabel, but any element with NSAttributedString will do
                self.descLbl.attributedText = attrStr
            }
        }
    }
}

