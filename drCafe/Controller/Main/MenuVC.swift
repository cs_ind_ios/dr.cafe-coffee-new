//
//  MenuVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 17/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import Firebase
import SwiftUI

@available(iOS 13.0, *)
class MenuVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var navigationBGView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cartCountLbl: UILabel!
    
    @IBOutlet weak var bottomCartBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomCartAmountLbl: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchViewTop: NSLayoutConstraint!
    
    static var instance: MenuVC!
    
    var whereObj = 0
    var backObj = 0
    var sectionType = 0
    var latitude:Double!
    var longitude:Double!
    var isSubscription = false
   
    var categoryArray = [StoresCategoryDetailsModel]()
    var addressDetailsArray = [AddressDetailsModel]()

    var menuSelectIndex = 0
    var beverageSelectIndex:Int!
    
    var address:String!
    var storeID:Int!
    var selectOrderType:Int!
    var itemId:Int!
    var itemName:String!
    
    var emptyCart = false
    var isCart = false
    var isBanner = false
    var categoryId = 0
    var categoryName = ""
    var isItemsBack = false
    var isGift = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        searchTF.delegate = self
        MenuVC.instance = self
        if whereObj == 1 || whereObj == 2 {
            if selectOrderType == 4 {
                if addressDetailsArray.count == 0{
                    latitude = 24.7233943939208
                    longitude = 46.6365165710449
                }else{
                    latitude = Double(addressDetailsArray[0].Latitude)
                    longitude = Double(addressDetailsArray[0].Longitude)
                    if latitude! <= 17.5{
                        latitude = 24.7233943939208
                        longitude = 46.6365165710449
                    }
                }
            }
        }
        if isSubscription == true{
            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make A New Subscription", value: "", table: nil))!
        }else{
            if sectionType == 2{
                if whereObj == 4{
                    navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make A New Subscription", value: "", table: nil))!
                }else{
                    navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Beans and Merchandise", value: "", table: nil))!
                }
            }else{
                if whereObj == 1 || whereObj == 2 {
                    if selectOrderType == 4 {
                        if addressDetailsArray.count == 0{
                            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Menu", value: "", table: nil))!
                        }else{
                            navigationBarTitleLbl.text = addressDetailsArray[0].Address
                        }
                    }else{
                        navigationBarTitleLbl.text = address!
                    }
                }else if whereObj == 4{
                    navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make A New Subscription", value: "", table: nil))!
                }else{
                    navigationBarTitleLbl.text = address!
                }
            }
        }
        /*if whereObj == 1 || whereObj == 2{
            if latitude! <= 17.5{
                latitude = 24.7233943939208
                longitude = 46.6365165710449
            }
        }*/
        self.getCategoriesService()
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        
        //Theme color
        backgroundView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        viewCartBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        viewCartBtn.setTitleColor(.white, for: .normal)
        
        if isSubscription == false{
            if AppDelegate.getDelegate().cartQuantity > 0{
                if emptyCart == true{
                    bottomCartBtnHeight.constant = 0
                    bottomCartAmountLbl.isHidden = true
                    cartCountLbl.text = "0"
                }else{
                    cartCountLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
                    bottomCartBtnHeight.constant = 55
                    bottomCartAmountLbl.text = "\(AppDelegate.getDelegate().cartAmount.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    bottomCartAmountLbl.isHidden = false
                }
            }else{
                bottomCartBtnHeight.constant = 0
                bottomCartAmountLbl.isHidden = true
            }
        }else{
            if AppDelegate.getDelegate().subCartQuantity > 0{
                if emptyCart == true{
                    bottomCartBtnHeight.constant = 0
                    bottomCartAmountLbl.isHidden = true
                    cartCountLbl.text = "0"
                }else{
                    cartCountLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
                    bottomCartBtnHeight.constant = 55
                    bottomCartAmountLbl.text = "\(AppDelegate.getDelegate().subCartAmount.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    bottomCartAmountLbl.isHidden = false
                }
            }else{
                bottomCartBtnHeight.constant = 0
                bottomCartAmountLbl.isHidden = true
            }
        }
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
    }
    //MARK: Bottom Cart Button Action
    @IBAction func bottomCartBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CartVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        obj.isMenu = true
        obj.sectionType = sectionType
        obj.orderType = selectOrderType
        obj.storeId = storeID!
        obj.isSubscription = isSubscription
        obj.whereObj = 3
        obj.isRemove = true
        if addressDetailsArray.count > 0{
            obj.selectAddressID = addressDetailsArray[0].Id
        }else{
            obj.selectAddressID = 0
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Search Button Action
    @IBAction func searchBtn_Tapped(_ sender: Any) {
        view.endEditing(true)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj:ItemsSearchVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsSearchVC") as! ItemsSearchVC
        if whereObj == 1{
            if selectOrderType == 4 {
                if sectionType == 1 && addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                    obj.storeId = storeID!
                }else{
                    obj.address = ""
                    obj.storeId = storeID!
                    obj.selectAddressID = 0
                }
            }else{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = addressDetailsArray[0].Id
            }
        }else if whereObj == 2{
            if addressDetailsArray.count > 0{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = addressDetailsArray[0].Id
                obj.addressDetailsArray = addressDetailsArray
            }else{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }
        }else{
            if isSubscription == false{
                obj.address = address!
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }else{
                if addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                }else{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = 0
                }
            }
        }
        obj.isV12 = false
        obj.sectionType = sectionType
        obj.selectOrderType = selectOrderType!
        obj.isSubscription = isSubscription
        obj.isBanner = isBanner
        obj.navigationBartitle = navigationBarTitleLbl.text!
        self.navigationController?.pushViewController(obj, animated: false)
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if backObj == 11{
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else if isItemsBack == true && isGift == false{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = StoresVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
            obj.whereObj = 2
            obj.isBanner = isBanner
            obj.orderType = selectOrderType
            obj.storeDetailsArray = SaveAddressClass.selectBannerStores[0].StoreList!
            obj.categoryId = categoryId
            obj.categoryName = categoryName
            obj.address = address!
            if itemId != nil{
                obj.itemId = itemId!
            }
            if itemName != nil{
                obj.itemTitle = itemName!
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }else if isItemsBack == true && isGift == true{
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.whereObj = 3
            obj.isMenu = true
            obj.isRemove = true
            obj.sectionType = sectionType
            obj.orderType = selectOrderType
            obj.isSubscription = isSubscription
            obj.storeId = storeID!
            if addressDetailsArray.count > 0{
                obj.selectAddressID = addressDetailsArray[0].Id
            }else{
                obj.selectAddressID = 0
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Get Store Categories Service
    func getCategoriesService(){
        if isSubscription == false{
            if isCart == true{
                self.getStoreCategoriesService(orderSectionType: sectionType)
            }else{
                if whereObj == 1{
                    self.getDeliveryStoreCategoriesService()
                }else if whereObj == 2{
                    if sectionType == 1{
                        self.getStoreCategoriesService(orderSectionType: sectionType)
                    }else{
                        self.getDeliveryStoreCategoriesService()
                    }
                }else{
                    self.getStoreCategoriesService(orderSectionType: 0)
                }
            }
        }else{
            //self.getSubscriptionCategoriesService()
            self.getDeliveryStoreCategoriesService()
        }
    }
    func getDeliveryStoreCategoriesService(){
        SaveAddressClass.discoverCategoryArray.removeAll()
        SaveAddressClass.newCategoryArray.removeAll()
        var dic:[String:Any]!
        if isSubscription == false{
            if sectionType == 2{
                dic = ["UserId": UserDef.getUserId(), "Latitude": "", "Longitude": "", "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "OrderType": 4, "SectionType": sectionType, "OnlineStores": 1, "RequestBy":2, "EmptyCart":emptyCart, "IsSubscription": isSubscription]
            }else{
                dic = ["UserId": UserDef.getUserId(), "Latitude": latitude!, "Longitude": longitude!, "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "OrderType": 4, "SectionType": sectionType, "OnlineStores": 1, "RequestBy":2, "EmptyCart":emptyCart, "IsSubscription": isSubscription]
            }
        }else{
            dic = ["UserId": UserDef.getUserId(), "Latitude": "", "Longitude": "", "DeviceToken": "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "OrderType": 4, "SectionType": sectionType, "OnlineStores": 1, "RequestBy":2, "EmptyCart":emptyCart, "IsSubscription": isSubscription]
        }
        /* Start Crashlytics Logs */
        Crashlytics.crashlytics().setUserID(UserDef.getUserId())
        Crashlytics.crashlytics().setCustomValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
        Crashlytics.crashlytics().setCustomValue("LoadCartService", forKey: "API")
        Crashlytics.crashlytics().setCustomValue("\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", forKey: "DeviceToken")
        Crashlytics.crashlytics().setCustomKeysAndValues(dic)
        /* End Crashlytics Logs */
        
        OrderModuleServices.GetStoreCategoyDeliveryService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageEn, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }else{
                    Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.MessageAr, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }))
                }
                self.tableView.isHidden = true
            }else{
                self.tableView.isHidden = false
                if data.Data!.DiscoverCategory!.count > 0 {
                    SaveAddressClass.discoverCategoryArray = data.Data!.DiscoverCategory!
                    self.storeID = data.Data!.DiscoverCategory![0].StoreId
                }
                if data.Data!.NewCategory!.count > 0 {
                    SaveAddressClass.newCategoryArray = data.Data!.NewCategory!
                }
                
                //self.categoryArray = [data.Data!]
                //SaveAddressClass.categoryArray = [data.Data!]
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func getStoreCategoriesService(orderSectionType:Int){
        SaveAddressClass.discoverCategoryArray.removeAll()
        SaveAddressClass.newCategoryArray.removeAll()
        let dic:[String:Any]!
        if isUserLogIn() == true{
            dic = ["UserId": UserDef.getUserId(), "StoreId": storeID!, "EmptyCart":emptyCart, "RequestBy":2,"SectionType": orderSectionType]
        }else{
            dic = ["StoreId": storeID!, "EmptyCart":emptyCart, "RequestBy":2]
        }
        
        OrderModuleServices.GetStoreCategoriesService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.tableView.isHidden = true
            }else{
                self.tableView.isHidden = false
                if data.Data!.DiscoverCategory!.count > 0 {
                    SaveAddressClass.discoverCategoryArray = data.Data!.DiscoverCategory!
                }
                if data.Data!.NewCategory!.count > 0 {
                    SaveAddressClass.newCategoryArray = data.Data!.NewCategory!
                }
                //self.categoryArray = [data.Data!]
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
//    func getSubscriptionCategoriesService(){
//        let dic:[String:Any] = ["RequestBy":2]
//        OrderModuleServices.GetSubScriptionCategoryService(dic: dic, success: { (data) in
//            if(data.Status == false){
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
//                }else{
//                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
//                }
//                self.tableView.isHidden = true
//            }else{
//                self.tableView.isHidden = false
//                if data.Data!.count > 0{
//                    SaveAddressClass.SubscriptionCategory = data.Data!
//                }
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    self.tableView.reloadData()
//                }
//            }
//        }) { (error) in
//            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
//        }
//    }
}
//MARK: Tableview Delegate Methods
@available(iOS 13.0, *)
extension MenuVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        //if isSubscription == false{
            if SaveAddressClass.discoverCategoryArray.count > 0{
                return SaveAddressClass.newCategoryArray.count + 1
            }else{
                return SaveAddressClass.newCategoryArray.count
            }
        //}else{
            //return SaveAddressClass.SubscriptionCategory.count
       // }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if isSubscription == false{
            if SaveAddressClass.discoverCategoryArray.count > 0{
                if section == 0 {
                    return 1
                }else{
                    return SaveAddressClass.newCategoryArray[section-1].Category!.count
                }
            }
            return SaveAddressClass.newCategoryArray[section].Category!.count
        //}else{
            //return SaveAddressClass.SubscriptionCategory[section].Category!.count
       // }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        //if isSubscription == false{
            if SaveAddressClass.discoverCategoryArray.count > 0{
                if section == 0 {
                    return 0
                }
            }
            return 35
        //}else{
           // return 40
        //}
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuCategoryTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuCategoryTVCell", value: "", table: nil))!)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuCategoryTVCell", value: "", table: nil))!) as? MenuCategoryTVCell else {
          return nil
        }
        //cell.contentView.layer.zPosition = 1
        if SaveAddressClass.discoverCategoryArray.count > 0{
            if section == 0 {
                cell.titleLbl.text = "" //Avenir Book 18.0
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.titleLbl.text = SaveAddressClass.newCategoryArray[section-1].NameEn.capitalized
                }else{
                    cell.titleLbl.text = SaveAddressClass.newCategoryArray[section-1].NameAr.capitalized
                }
            }
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.titleLbl.text = SaveAddressClass.newCategoryArray[section].NameEn.capitalized
            }else{
                cell.titleLbl.text = SaveAddressClass.newCategoryArray[section].NameAr.capitalized
            }
        }
        return cell
        ////if isSubscription == false{
//            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
//            let label = UILabel()
//            label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-12, height: headerView.frame.height-10)
//            if SaveAddressClass.discoverCategoryArray.count > 0{
//                if section == 0 {
//                    label.text = "" //Avenir Book 18.0
//                }else{
//                    if AppDelegate.getDelegate().appLanguage == "English"{
//                        label.text = SaveAddressClass.newCategoryArray[section-1].NameEn.capitalized
//                    }else{
//                        label.text = SaveAddressClass.newCategoryArray[section-1].NameAr.capitalized
//                    }
//                }
//            }else{
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    label.text = SaveAddressClass.newCategoryArray[section].NameEn.capitalized
//                }else{
//                    label.text = SaveAddressClass.newCategoryArray[section].NameAr.capitalized
//                }
//            }
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                label.textAlignment = .left
//            }else{
//                label.textAlignment = .right
//            }
//            label.font = UIFont.init(name: "Avenir-Book", size: 18.0)
//            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
//                label.textColor = .white
//            }else{
//                label.textColor = .black
//            }
//            headerView.addSubview(label)
//            headerView.backgroundColor = backgroundView.backgroundColor
//            return headerView
//        }else{
//            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
//            let label = UILabel()
//            label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-12, height: headerView.frame.height-10)
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                label.text = SaveAddressClass.SubscriptionCategory[section].NameEn.capitalized
//            }else{
//                label.text = SaveAddressClass.SubscriptionCategory[section].NameAr.capitalized
//            }
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                label.textAlignment = .left
//            }else{
//                label.textAlignment = .right
//            }
//            label.font = UIFont.init(name: "Avenir-Book", size: 18.0)
//            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
//                label.textColor = .white
//            }else{
//                label.textColor = .black
//            }
//            headerView.addSubview(label)
//            headerView.backgroundColor = backgroundView.backgroundColor
//            return headerView
//        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuBeveragesTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuBeveragesTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "MenuBeveragesTVCell", value: "", table: nil))!, for: indexPath) as! MenuBeveragesTVCell
        //if isSubscription == false{
            var dic: CategoryDetailsModel?
            if SaveAddressClass.discoverCategoryArray.count > 0{
                if indexPath.section == 0{
                    cell.categoryColorLbl.isHidden = true
                    dic = SaveAddressClass.discoverCategoryArray[indexPath.row]
                }else{
                    if indexPath.row == 0{
                        cell.categoryColorLbl.isHidden = false
                    }else{
                        cell.categoryColorLbl.isHidden = true
                    }
                    dic = SaveAddressClass.newCategoryArray[indexPath.section-1].Category![indexPath.row]
                }
            }else{
                if indexPath.row == 0{
                    cell.categoryColorLbl.isHidden = false
                }else{
                    cell.categoryColorLbl.isHidden = true
                }
                dic = SaveAddressClass.newCategoryArray[indexPath.section].Category![indexPath.row]
            }
        
//        if indexPath.row == 0{
//            cell.categoryColorLbl.isHidden = false
//        }else{
//            cell.categoryColorLbl.isHidden = true
//        }
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.titleNameLbl.text = dic!.NameEn
                cell.descLbl.text = dic!.DescEn
            }else{
                cell.titleNameLbl.text = dic!.NameAr
                cell.descLbl.text = dic!.DescAr
            }

            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(dic!.Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.beverageImg.kf.setImage(with: url)
        
        if dic!.BackgroundColor != nil{
            if dic!.BackgroundColor! != ""{
                cell.beveragesBGView.backgroundColor = UIColor(hexString: "\(dic!.BackgroundColor!)")
                cell.titleNameLbl.textColor = UIColor(hexString: "\(dic!.ForeGroundColor!)")
                cell.descLbl.textColor = UIColor(hexString: "\(dic!.ForeGroundColor!)")
            }else{
                if dic!.NameEn == " V12" || dic!.NameEn == "V12"{
                    cell.beveragesBGView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.1921568627, blue: 0.1960784314, alpha: 1)
                    cell.titleNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    cell.descLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }else{
                    cell.beveragesBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.titleNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.descLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }
            }
        }else{
            if dic!.NameEn == " V12" || dic!.NameEn == "V12"{
                cell.beveragesBGView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.1921568627, blue: 0.1960784314, alpha: 1)
                cell.titleNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.descLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }else{
                cell.beveragesBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.titleNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.descLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            }
        }
        cell.beveragesBGView.layer.cornerRadius = 10
        cell.beveragesBGView.clipsToBounds = true
        
            //tableView.bringSubviewToFront(cell)
        cell.contentView.layer.zPosition = -1
        //cell.beveragesBGView.layer.zPosition = -1
        
//        }else{
//            var dic: SubScriptionCategoryDetailsModel?
//            dic = SaveAddressClass.SubscriptionCategory[indexPath.section].Category![indexPath.row]
//
//            if AppDelegate.getDelegate().appLanguage == "English"{
//                cell.titleNameLbl.text = dic!.NameEn
//                cell.descLbl.text = ""
//            }else{
//                cell.titleNameLbl.text = dic!.NameAr
//                cell.descLbl.text = ""
//            }
//
////            //Image
////            let ImgStr:NSString = "\(displayImages.images.path())\(dic!.Image)" as NSString
////            let charSet = CharacterSet.urlFragmentAllowed
////            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
////            let url = URL.init(string: urlStr as String)
////            cell.beverageImg.kf.setImage(with: url)
//            cell.beverageImg.backgroundColor = .clear
//
//            if dic!.NameEn == " V12" || dic!.NameEn == "V12"{
//                cell.beveragesBGView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.1921568627, blue: 0.1960784314, alpha: 1)
//                cell.titleNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                cell.descLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            }else{
//                cell.beveragesBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                cell.titleNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                cell.descLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
//            }
//        }
        
        /*if categoryArray.count > 0{
            if categoryArray[0].DiscoverCategory!.count > 0{
                if indexPath.row == 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.titleNameLbl.text = categoryArray[0].DiscoverCategory![indexPath.row].NameEn
                        cell.descLbl.text = categoryArray[0].DiscoverCategory![indexPath.row].DescEn
                    }else{
                        cell.titleNameLbl.text = categoryArray[0].DiscoverCategory![indexPath.row].NameAr
                        cell.descLbl.text = categoryArray[0].DiscoverCategory![indexPath.row].DescAr
                    }
                    
                    //Image
                    let ImgStr:NSString = "\(displayImages.images.path())\(categoryArray[0].DiscoverCategory![indexPath.row].Image)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.beverageImg.kf.setImage(with: url)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.titleNameLbl.text = categoryArray[0].Category![indexPath.row-1].NameEn
                        cell.descLbl.text = categoryArray[0].Category![indexPath.row-1].DescEn
                    }else{
                        cell.titleNameLbl.text = categoryArray[0].Category![indexPath.row-1].NameAr
                        cell.descLbl.text = categoryArray[0].Category![indexPath.row-1].DescAr
                    }
                    
                    //Image
                    let ImgStr:NSString = "\(displayImages.images.path())\(categoryArray[0].Category![indexPath.row - 1].Image)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    cell.beverageImg.kf.setImage(with: url)
                    
                    if categoryArray[0].Category![indexPath.row-1].NameEn == " V12" || categoryArray[0].Category![indexPath.row-1].NameEn == "V12"{
                        cell.beveragesBGView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.1921568627, blue: 0.1960784314, alpha: 1)
                        cell.titleNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                        cell.descLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    }else{
                        cell.beveragesBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        cell.titleNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                        cell.descLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.titleNameLbl.text = categoryArray[0].Category![indexPath.row].NameEn
                    cell.descLbl.text = categoryArray[0].Category![indexPath.row].DescEn
                }else{
                    cell.titleNameLbl.text = categoryArray[0].Category![indexPath.row].NameAr
                    cell.descLbl.text = categoryArray[0].Category![indexPath.row].DescAr
                }
                
                //Image
                let ImgStr:NSString = "\(displayImages.images.path())\(categoryArray[0].Category![indexPath.row].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.beverageImg.kf.setImage(with: url)
                
                if categoryArray[0].Category![indexPath.row].NameEn == " V12" || categoryArray[0].Category![indexPath.row].NameEn == "V12"{
                    cell.beveragesBGView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.1921568627, blue: 0.1960784314, alpha: 1)
                    cell.titleNameLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    cell.descLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }else{
                    cell.beveragesBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.titleNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.descLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }
            }
        }*/
        
        //Description Text next Arrow Button Action
        
        //cell.nextArrowBtn.tag = indexPath.row
        //cell.nextArrowBtn.addTarget(self, action: #selector(beverageArrowBtn_Tapped(_:)), for: .touchUpInside)
//        if beverageSelectIndex == indexPath.row{
//            let height = heightForView(text: "Bold ristretto shots of expresso get the perfect amount of steamed whole milk.", font: UIFont(name: "Avenir Book", size: 14.0)!, width: cell.subDescLbl.frame.width)
//            cell.subViewHeight.constant = 55 + height
//        }else{
//            cell.subViewHeight.constant = 0
//        }
        
        tableView.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    @objc func beverageArrowBtn_Tapped(_ sender:UIButton){
        beverageSelectIndex = sender.tag
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj:ItemsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
        obj.sectionType = sectionType
        var dic: CategoryDetailsModel?
        if SaveAddressClass.discoverCategoryArray.count > 0{
            if indexPath.section == 0{
                dic = SaveAddressClass.discoverCategoryArray[indexPath.row]
                obj.isV12 = false
                obj.whereObj = 2
                if whereObj == 2{
                    if sectionType == 1{
                        obj.sectionType = 1
                    }else{
                        obj.sectionType = 0
                    }
                }else{
                    obj.sectionType = 0
                }
            }else{
                dic = SaveAddressClass.newCategoryArray[indexPath.section-1].Category![indexPath.row]
            }
        }else{
            dic = SaveAddressClass.newCategoryArray[indexPath.section].Category![indexPath.row]
        }
        var backgroundColor = "#FFFFFF"
        if dic!.BackgroundColor != nil{
            backgroundColor = dic!.BackgroundColor!
        }
        var foregroundColor = "#888888"
        if dic!.ForeGroundColor != nil{
            foregroundColor = dic!.ForeGroundColor!
        }
        obj.categoryArray.insert(CategoryDetailsModel(StoreId: dic!.StoreId, Id: dic!.Id, NameEn: dic!.NameEn, NameAr: dic!.NameAr, DescEn: dic!.DescEn, DescAr: dic!.DescAr, Image: dic!.Image, BackgroundColor: backgroundColor, ForeGroundColor: foregroundColor), at: 0)
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.selectItemName = dic!.NameEn
            obj.categoryName = dic!.NameEn
        }else{
            obj.selectItemName = dic!.NameAr
            obj.categoryName = dic!.NameAr
        }
        obj.categoryId = dic!.Id
        if whereObj == 1{
            if selectOrderType == 4 {
                if sectionType == 1 && addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                    if isCart == true{
                        obj.storeId = storeID!
                    }else{
                        obj.storeId = dic!.StoreId!
                    }
                }else{
                    obj.address = ""
                    obj.storeId = storeID
                    obj.selectAddressID = 0
                }
            }else{
                obj.address = ""
                obj.storeId = dic!.StoreId!
                obj.selectAddressID = addressDetailsArray[0].Id
            }
        }else if whereObj == 2{
            if addressDetailsArray.count > 0{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = addressDetailsArray[0].Id
                obj.addressDetailsArray = addressDetailsArray
            }else{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }
        }else{
            if isSubscription == false{
                obj.address = address!
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }else{
                if addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                }else{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = 0
                }
            }
        }
        if dic!.NameEn == " V12" || dic!.NameEn == "V12"{
            obj.isV12 = true
        }else{
            obj.isV12 = false
        }
        obj.sectionType = sectionType
        obj.selectOrderType = selectOrderType!
        obj.isSubscription = isSubscription
        obj.isBanner = isBanner
        if isBanner == true{
            obj.isItems = true
        }else{
            obj.isItems = false
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Find Label Height
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
extension MenuVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view.endEditing(true)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj:ItemsSearchVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsSearchVC") as! ItemsSearchVC
        if whereObj == 1{
            if selectOrderType == 4 {
                if sectionType == 1 && addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                    obj.storeId = storeID!
                }else{
                    obj.address = ""
                    obj.storeId = storeID!
                    obj.selectAddressID = 0
                }
            }else{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = addressDetailsArray[0].Id
            }
        }else if whereObj == 2{
            if addressDetailsArray.count > 0{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = addressDetailsArray[0].Id
                obj.addressDetailsArray = addressDetailsArray
            }else{
                obj.address = ""
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }
        }else{
            if isSubscription == false{
                obj.address = address!
                obj.storeId = storeID!
                obj.selectAddressID = 0
            }else{
                if addressDetailsArray.count > 0{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = addressDetailsArray[0].Id
                    obj.addressDetailsArray = addressDetailsArray
                }else{
                    obj.address = ""
                    obj.storeId = 190
                    obj.selectAddressID = 0
                }
            }
        }
        obj.isV12 = false
        obj.sectionType = sectionType
        obj.selectOrderType = selectOrderType!
        obj.isSubscription = isSubscription
        obj.isBanner = isBanner
        obj.navigationBartitle = navigationBarTitleLbl.text!
        self.navigationController?.pushViewController(obj, animated: false)
    }
}
