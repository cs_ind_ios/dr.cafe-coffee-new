//
//  ItemsSearchVC.swift
//  drCafe
//
//  Created by Mac2 on 14/09/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import AlgoliaSearchClient

struct SearchModel : Decodable {
    let id : String
    let name : String
    let description : String
   let image : String
}


class ItemsSearchVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var cartCountLbl: UILabel!
    
    var searchArray = [Hit<JSON>]()
    var searchDetailsArray = [SearchModel]()
    
    var storeId:Int!
    var selectOrderType:Int!
    var selectAddressID:Int!
    var address:String!
    var itemID:Int!
    var sectionType = 0
    var isBanner = false
    var isSubscription = false
    var isV12 = false
    var addressDetailsArray = [AddressDetailsModel]()
    var navigationBartitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add the client to the dependencies of your targets
       
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!)
        tableView.delegate = self
        tableView.dataSource = self
        
//         // Create a new index and add a record
//        let results = index.search(query: "test_record")
//        print(results.hits[0])
        searchTF.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        
        //Theme color
        backgroundView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        navigationBarTitleLbl.text = navigationBartitle
        
        if isSubscription == false{
            if AppDelegate.getDelegate().cartQuantity > 0{
                cartCountLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
            }else{
                cartCountLbl.text = "0"
            }
        }else{
            if AppDelegate.getDelegate().subCartQuantity > 0{
                cartCountLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
            }else{
                cartCountLbl.text = "0"
            }
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.whereObj = 3
            obj.isMenu = true
            obj.isRemove = true
            obj.sectionType = sectionType
            obj.orderType = selectOrderType
            obj.isSubscription = isSubscription
            obj.storeId = storeId!
            if addressDetailsArray.count > 0{
                obj.selectAddressID = addressDetailsArray[0].Id
            }else{
                obj.selectAddressID = 0
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    //MARK: Search Button Action
    @IBAction func searchBtn_Tapped(_ sender: Any) {
        view.endEditing(true)
        let client = SearchClient(appID: "BW80FQ7S1L", apiKey: "4b821d633c38af61cf29a3c8729ad276")
        let index = client.index(withName: "product")
        let query = Query("\(searchTF.text!)")
        index.search(query: query) { [self] result in
            
            if case .success(let response) = result {
                if response.hits.count > 0{
                    print("Response: \(response.hits[0])")
                    print("Response2: \(response.hits[0].object)")
                    print("Image: \(response.hits[0].object["image"]!)")
                    self.searchArray = response.hits
                  
                    self.searchDetailsArray.removeAll()
                    for dic in response.hits{
                        self.searchDetailsArray.append(SearchModel(id: "\(dic.object["ItemId"]!)", name: "\(dic.object["name"]!)", description: "\(dic.object["description"]!)", image: "\(dic.object["image"]!)"))
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }else{
                    self.searchDetailsArray.removeAll()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }else{
                print("Failed")
            }
        }
    }
}
extension ItemsSearchVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!, for: indexPath) as! ItemsListTVCell
        
        let dic = self.searchDetailsArray[indexPath.row]
        cell.itemNameLbl.text = dic.name.replacingOccurrences(of: "\"", with: "")
        cell.itemDescLbl.text = dic.description.replacingOccurrences(of: "\"", with: "")
        
        cell.playBtn.isHidden = true
        cell.playerView.isHidden = true
        cell.tagsCollectionView.isHidden = true
        //Image
        let ImgStr:NSString = "\(dic.image.replacingOccurrences(of: "\"", with: ""))" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        cell.itemImg.kf.setImage(with: url)
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = AdditionalsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
        var itemId = self.searchDetailsArray[indexPath.row].id.replacingOccurrences(of: "\"", with: "")
        itemId = itemId.components(separatedBy: ".")[0]
        obj.itemID = Int(itemId)
        obj.sectionType = sectionType
        if let _ = storeId{
            obj.storeId = storeId!
        }
        if let _ = selectOrderType{
            obj.selectOrderType = selectOrderType!
        }
        if let _ = selectAddressID{
            obj.selectAddressId = selectAddressID!
        }
        if addressDetailsArray.count > 0{
            obj.addressDetailsArray = addressDetailsArray
        }
        obj.isV12 = isV12
        obj.titleLbl = self.searchDetailsArray[indexPath.row].name.replacingOccurrences(of: "\"", with: "")
        if let _ = address{
            obj.address = address!
        }
        obj.isSubscription = isSubscription
        obj.isBanner = isBanner
        //obj.categoryId = categoryId
        //obj.categoryName = categoryName
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
