//
//  ItemsVC.swift
//  Dr.Cafe
//
//  Created by Devbox on 19/03/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

@available(iOS 13.0, *)
class ItemsVC: UIViewController{
    //MARK: Outlets
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navigationBarBGView: UIView!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var cartCountLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var categoryMenuViewHeight: NSLayoutConstraint!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var btmMenuView: UIView!
    
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var bottomCartAmountLbl: UILabel!
    @IBOutlet weak var addMoreBtn: UIButton!
    @IBOutlet weak var netTotalNameLbl: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    
    var storeItemsArray = [StoreItemsDetailsModel]()
    static var instance: ItemsVC!
    var sectionType = 0
    var menuSelectIndex = 0
    var selectItemName:String!
    var categoryId:Int!
    var storeId:Int!
    var selectOrderType:Int!
    var selectAddressID:Int!
    var address:String!
    var categoryName:String!
    var isV12 = false
    var whereObj = 1
    var backWhereObj = 0
    var categoryArray = [CategoryDetailsModel]()
    var addressDetailsArray = [AddressDetailsModel]()
    var isBanner = false
    var isSubscription = false
    var isManualLocation = false
    var itemID:Int!
    var itemName:String!
    var isItems = false
    var isStores = false
    //var categoryArray = [StoresCategoryDetailsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        ItemsVC.instance = self
        tabBarController?.tabBar.isHidden = true
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!)
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsHeaderTVCell", value: "", table: nil))!)
        
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
            self.collectionView.collectionViewLayout = flowLayout
            }
        
//        //let width = collectionView.bounds.width
//        let flowLayout = UICollectionViewFlowLayout()
//        flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
//        flowLayout.scrollDirection = .horizontal
////        flowLayout.itemSize = CGSize(width: width/3, height: 40)
////        flowLayout.minimumLineSpacing = 0
////        flowLayout.minimumInteritemSpacing = 0
//        //flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 5, right: 5)
//        self.collectionView.collectionViewLayout = flowLayout
//        DispatchQueue.main.async {
//            self.collectionView.reloadData()
//        }
//
        if AppDelegate.getDelegate().appLanguage == "English"{
            collectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            collectionView.semanticContentAttribute = .forceRightToLeft
        }

        if whereObj == 1{
            getStoreItemsService()
        }else{
            getDiscoverStoreItemsService()
        }
        
        if isBanner == true{
            btmMenuView.isHidden = true
        }else{
            btmMenuView.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        //Theme color
        BGView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        viewCartBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        addMoreBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        addMoreBtn.setTitleColor(.white, for: .normal)
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            viewCartBtn.backgroundColor = .darkGray
            viewCartBtn.setTitleColor(.white, for: .normal)
        } else {
            viewCartBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            viewCartBtn.setTitleColor(.white, for: .normal)
        }
        tabBarController?.tabBar.isHidden = true
        
        if backWhereObj == 0{
            navigationBarTitleLbl.text = categoryName!
            if categoryName == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gift & More", value: "", table: nil))!{
                btmMenuView.isHidden = true
            }else{
                if isBanner == true{
                    btmMenuView.isHidden = true
                }else{
                    btmMenuView.isHidden = false
                }
            }
            self.allDetails()
            if isSubscription == false{
                cartCountLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
            }else{
                cartCountLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
            }
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[2]
                if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                    tabItems[2].image = UIImage(named: "Cart Tab")
                    tabItems[2].selectedImage = UIImage(named: "Cart Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                }else{
                    tabItem.badgeValue = nil
                    tabItems[2].image = UIImage(named: "Tab Order")
                    tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                }
            }
            collectionView.setNeedsLayout()
            collectionView.layoutIfNeeded()
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
        if isSubscription == false{
            if AppDelegate.getDelegate().cartQuantity > 0{
                cartCountLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
                bottomViewHeight.constant = 110
                bottomCartAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(AppDelegate.getDelegate().cartAmount.withCommas())"
                bottomView.isHidden = false
            }else{
                bottomViewHeight.constant = 10
                bottomView.isHidden = true
            }
        }else{
            if AppDelegate.getDelegate().subCartQuantity > 0{
                cartCountLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
                bottomViewHeight.constant = 110
                bottomCartAmountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(AppDelegate.getDelegate().subCartAmount.withCommas())"
                bottomView.isHidden = false
            }else{
                bottomViewHeight.constant = 10
                bottomView.isHidden = true
            }
        }
    }
    //MARK: All Details Assign to Model Class
    func allDetails(){
//        if AppDelegate.getDelegate().selectBGColor == "#000000"  || AppDelegate.getDelegate().selectColorname == "Black"{
//            if isV12 == true{
//                navigationBarBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                navigationBarTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                cartCountLbl.textColor = #colorLiteral(red: 0.3568627451, green: 0.1803921569, blue: 0.1725490196, alpha: 1)
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    backBtn.setImage(UIImage(named: "Back"), for: .normal)
//                }else{
//                    backBtn.setImage(UIImage(named: "backAr"), for: .normal)
//                }
//                cartBtn.setImage(UIImage(named: "Cart"), for: .normal)
//            }else{
//                navigationBarBGView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                navigationBarTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                cartCountLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                if AppDelegate.getDelegate().appLanguage == "English"{
//                    backBtn.setImage(UIImage(named: "back white"), for: .normal)
//                }else{
//                    backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
//                }
//                cartBtn.setImage(UIImage(named: "cart white"), for: .normal)
//            }
//        }else{
            if isV12 == true{
                navigationBarBGView.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                navigationBarTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                view.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                cartCountLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    backBtn.setImage(UIImage(named: "back white"), for: .normal)
                }else{
                    backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                }
                cartBtn.setImage(UIImage(named: "cart white"), for: .normal)
            }else{
                navigationBarBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                navigationBarTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cartCountLbl.textColor = #colorLiteral(red: 0.3568627451, green: 0.1803921569, blue: 0.1725490196, alpha: 1)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    backBtn.setImage(UIImage(named: "Back"), for: .normal)
                }else{
                    backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                }
                cartBtn.setImage(UIImage(named: "Cart"), for: .normal)
            }
        //}
        
        navigationBarTitleLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
    }
    //MARK: Get Store Items Service
    func getStoreItemsService(){
        let dic:[String:Any] = ["StoreId": storeId!, "CategoryId": categoryId!, "RequestBy":2, "IsSubscription": isSubscription]
        OrderModuleServices.GetStoreItemsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeItemsArray = data.Data!
                if self.storeItemsArray.count > 0{
                    for i in 0...self.storeItemsArray.count - 1{
                        self.storeItemsArray[i].sectionIsExpanded = true
                    }
                    self.tableView.reloadData()
                    if self.storeItemsArray.count > 1{
                        self.categoryMenuViewHeight.constant = 60
                        //self.collectionView.reloadData()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                            //self.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true )
                            //self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                        }
                    }else{
                        self.categoryMenuViewHeight.constant = 0
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Get Discover Store Items Service
    func getDiscoverStoreItemsService(){
        var dic:[String:Any] = [:]
        if isSubscription == true{
            dic = ["StoreId": storeId!, "RequestBy":2, "SectionType": sectionType, "IsSubscription": isSubscription]
        }else{
            dic = ["StoreId": storeId!, "RequestBy":2, "IsSubscription": isSubscription]
        }
        OrderModuleServices.GetDiscoverStoreItemsService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeItemsArray = data.Data!.filter({$0.NameEn != nil})
                if self.storeItemsArray.count > 0{
                    for i in 0...self.storeItemsArray.count - 1{
                        self.storeItemsArray[i].sectionIsExpanded = true
                    }
                    self.tableView.reloadData()
                    if self.storeItemsArray.count > 1{
                        self.categoryMenuViewHeight.constant = 60
                        //self.collectionView.reloadData()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                            //self.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true )
                            //self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                        }
                    }else{
                        self.categoryMenuViewHeight.constant = 0
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isBanner == false{
            if isManualLocation == true{
                self.tabBarController?.selectedIndex = 0
                self.navigationController?.popToRootViewController(animated: false)
            }else{
                navigationController?.popViewController(animated: true)
            }
        }else{
            if isItems == true || isStores == true{
                navigationController?.popViewController(animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = MenuVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                obj.address = address!
                obj.categoryId = SaveAddressClass.selectBannerStores[0].CategoryId
                if itemID != nil{
                    obj.itemId = itemID!
                }
                if itemName != nil{
                    obj.itemName = itemName!
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.categoryName = SaveAddressClass.selectBannerStores[0].CategoryEn
                }else{
                    obj.categoryName = SaveAddressClass.selectBannerStores[0].CategoryAr
                }
                obj.storeID = storeId!
                obj.emptyCart = false
                obj.selectOrderType = selectOrderType!
                obj.isBanner = isBanner
                obj.isItemsBack = true
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
   func backBtn_Tap() {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.whereObj = 4
            obj.isMenu = true
            obj.sectionType = sectionType
            obj.orderType = selectOrderType
            obj.storeId = storeId!
            obj.isRemove = true
            obj.isSubscription = isSubscription
            obj.selectAddressID = selectAddressID
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Add More Button Action
    @IBAction func addMoreBtn_Tapped(_ sender: Any) {
        if isItems == true{
            navigationController?.popViewController(animated: true)
        }else{
            if categoryName == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gift & More", value: "", table: nil))! || isBanner == true{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = MenuVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                if categoryName == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Gift & More", value: "", table: nil))!{
                    obj.address = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Beans and Merchandise", value: "", table: nil))!
                    obj.categoryId = categoryId!
                    obj.categoryName = categoryName!
                    obj.isGift = true
                }else{
                    obj.address = address!
                    obj.categoryId = SaveAddressClass.selectBannerStores[0].CategoryId
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        obj.categoryName = SaveAddressClass.selectBannerStores[0].CategoryEn
                    }else{
                        obj.categoryName = SaveAddressClass.selectBannerStores[0].CategoryAr
                    }
                    if itemID != nil{
                        obj.itemId = itemID!
                    }
                    if itemName != nil{
                        obj.itemName = itemName!
                    }
                }
                obj.storeID = storeId!
                obj.emptyCart = false
                obj.selectOrderType = selectOrderType!
                obj.isBanner = isBanner
                obj.isItemsBack = true
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //MARK: Bottom Cart Button Action
    @IBAction func bottomCartBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CartVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        obj.isMenu = true
        obj.sectionType = sectionType
        obj.orderType = selectOrderType!
        obj.storeId = storeId!
        obj.isSubscription = isSubscription
        obj.whereObj = 4
        obj.isRemove = true
        obj.selectAddressID = selectAddressID
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Menu Button Action
    @IBAction func menuBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "CategoryPopUpVC") as! CategoryPopUpVC
        obj.categoryPopUpVCDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.categoryArray = categoryArray
        present(obj, animated: false, completion: nil)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension ItemsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return storeItemsArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if storeItemsArray[section].sectionIsExpanded == false{
            return 1
        }else{
            if storeItemsArray[section].Items == nil{
                return 1
            }else{
                return storeItemsArray[section].Items!.count + 1
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 55
        }else{
            return 150
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0{
             //MARK: Items Header Details
            let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsHeaderTVCell", value: "", table: nil))!, for: indexPath) as! ItemsHeaderTVCell
            if AppDelegate.getDelegate().appLanguage == "English"{
                if storeItemsArray[indexPath.section].NameEn != nil{
                    headerCell.nameLbl.text = storeItemsArray[indexPath.section].NameEn!
                }else{
                    headerCell.nameLbl.text = ""
                }
            }else{
                if storeItemsArray[indexPath.section].NameAr != nil{
                    headerCell.nameLbl.text = storeItemsArray[indexPath.section].NameAr!
                }else{
                    headerCell.nameLbl.text = ""
                }
            }
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                headerCell.nameLbl.textColor = .white
                headerCell.dotLbl.backgroundColor = .white
            }else{
                headerCell.nameLbl.textColor = .black
                headerCell.dotLbl.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            }
            headerCell.dropDownBtn.isHidden = true
            if storeItemsArray[indexPath.section].sectionIsExpanded! == false{
                //headerCell.dropDownBtn.setImage(UIImage(named: "Path 3 Copy 6"), for: .normal)
                headerCell.lineLbl.isHidden = false
            }else{
                //headerCell.dropDownBtn.setImage(UIImage(named: "Right Arrow"), for: .normal)
                headerCell.lineLbl.isHidden = true
            }
            cell = headerCell
        }else{
             //MARK: Items Details
            let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsListTVCell", value: "", table: nil))!, for: indexPath) as! ItemsListTVCell
            subCell.delegate = self
            if AppDelegate.getDelegate().appLanguage == "English"{
                subCell.itemNameLbl.text = storeItemsArray[indexPath.section].Items![indexPath.row-1].NameEn
                subCell.itemDescLbl.text = storeItemsArray[indexPath.section].Items![indexPath.row-1].ItemsDescEn
            }else{
                subCell.itemNameLbl.text = storeItemsArray[indexPath.section].Items![indexPath.row-1].NameAr
                subCell.itemDescLbl.text = storeItemsArray[indexPath.section].Items![indexPath.row-1].ItemsDescAr
            }
            //subCell.contentView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9490196078, alpha: 1)
            let name = storeItemsArray[indexPath.section].Items![indexPath.row-1].Image
            subCell.itemImg.image = UIImage(named: "")
            let fullNameArr = name.split(separator:".")
            if fullNameArr.count > 1 {
                if fullNameArr[1] == "mp4" {
                    subCell.playBtn.tag = indexPath.row-1
                    //subCell.playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
                    subCell.playBtn.setImage(UIImage(named: "play"), for: .normal)
                    subCell.playBtn.isHidden = false
                    subCell.playerView.isHidden = false
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    let avPlayer = AVPlayer(url: url! as URL);
                    subCell.playerView!.playerLayer.player = avPlayer;
                    subCell.playerView!.playerLayer.player!.isMuted = true
                    subCell.contentView.bringSubviewToFront(subCell.playBtn)
                }else{
                    subCell.playBtn.isHidden = true
                    subCell.playerView.isHidden = true
                        //Image
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(storeItemsArray[indexPath.section].Items![indexPath.row-1].Image)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    subCell.itemImg.kf.setImage(with: url)
                }
                if storeItemsArray[indexPath.section].Items![indexPath.row-1].Tags != nil{
                    let rowArray = storeItemsArray[indexPath.section].Items![indexPath.row-1].Tags
                    subCell.updateCellWith(row: rowArray!)
                }
                if storeItemsArray[indexPath.section].Items![indexPath.row-1].Tags != nil{
                    if storeItemsArray[indexPath.section].Items![indexPath.row-1].Tags!.count > 0{
                        subCell.tagsCollectionView.isHidden = false
                        subCell.cellDelegate = self
    //                    subCell.starBtn.isHidden = false
    //
    //                    //Image
    //                    let ImgStr:NSString = "\(displayImages.images.path())\(storeItemsArray[indexPath.section].Items![indexPath.row-1].Tags![0].Image)" as NSString
    //                    let charSet = CharacterSet.urlFragmentAllowed
    //                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
    //                    let url = URL.init(string: urlStr as String)
    //                    subCell.starBtn.kf.setImage(with: url, for: .normal)
                    }else{
                        subCell.tagsCollectionView.isHidden = true
                    }
                }else{
                    subCell.tagsCollectionView.isHidden = true
                }
                    
                //Out Of Stock functionality
                subCell.outOfStockLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out Of Stock", value: "", table: nil))!
                subCell.soldOutLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out Of Stock", value: "", table: nil))!
                subCell.outOfStockLblWidth.constant = 0
                subCell.outOfStockLblTrailing.constant = 0
                
                if storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices != nil{
                    if storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices!.count > 1{
                        if storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices![0].IsOutOfStock != nil{
                            let outofStock = storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices!.filter({$0.IsOutOfStock! == 1})
                            if outofStock.count == storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices!.count{
                                subCell.soldOutLbl.isHidden = false
                                subCell.itemImg.alpha = 0.5
                            }else{
                                subCell.soldOutLbl.isHidden = true
                                subCell.itemImg.alpha = 1
                            }
                        }else{
                            subCell.soldOutLbl.isHidden = true
                            subCell.itemImg.alpha = 1
                        }
                    }else if storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices!.count == 1{
                        if storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices![0].IsOutOfStock != nil{
                            let outofStock = storeItemsArray[indexPath.section].Items![indexPath.row-1].Prices!.filter({$0.IsOutOfStock! == 1})
                            if outofStock.count > 0{
                                subCell.soldOutLbl.isHidden = false
                                subCell.itemImg.alpha = 0.5
                            }else{
                                subCell.soldOutLbl.isHidden = true
                                subCell.itemImg.alpha = 1
                            }
                        }else{
                            subCell.soldOutLbl.isHidden = true
                            subCell.itemImg.alpha = 1
                        }
                    }else{
                        subCell.soldOutLbl.isHidden = true
                        subCell.itemImg.alpha = 1
                    }
                }else{
                    subCell.soldOutLbl.isHidden = true
                    subCell.itemImg.alpha = 1
                }
                
                cell = subCell
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //Section Expand code
            //storeItemsArray[indexPath.section].sectionIsExpanded = !storeItemsArray[indexPath.section].sectionIsExpanded!
            //tableView.reloadSections([indexPath.section], with: .automatic)
            if storeItemsArray[indexPath.section].sectionIsExpanded == true{
                menuSelectIndex = indexPath.section
                collectionView.reloadData()
            }else{
                menuSelectIndex = 0
                collectionView.reloadData()
            }
        }else{
            let name = storeItemsArray[indexPath.section].Items![indexPath.row-1].Image
            let fullNameArr = name.split(separator:".")
            if fullNameArr.count > 1 {
                if fullNameArr[1] == "mp4" {
                    let cell = self.tableView.cellForRow(at: indexPath as IndexPath) as! ItemsListTVCell?
                //if let image = cell!.playBtn.currentImage {
//                    if image.isEqual(UIImage(named: "play")) {
//                    }else{
                    cell!.playBtn.isHidden = false
                    cell!.playBtn.setImage(UIImage(named: "play"), for: .normal)
                    cell!.playerView!.player!.pause()
                    cell!.contentView.bringSubviewToFront(cell!.playBtn)
                    //}
                //}
                }
            }
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = AdditionalsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
            obj.itemID = storeItemsArray[indexPath.section].Items![indexPath.row-1].Id
            obj.sectionType = sectionType
            if let _ = storeId{
                obj.storeId = storeId!
            }
            if let _ = selectOrderType{
                obj.selectOrderType = selectOrderType!
            }
            if let _ = selectAddressID{
                obj.selectAddressId = selectAddressID!
            }
            if addressDetailsArray.count > 0{
                obj.addressDetailsArray = addressDetailsArray
            }
            obj.isV12 = isV12
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.titleLbl = storeItemsArray[indexPath.section].Items![indexPath.row-1].NameEn
            }else{
                obj.titleLbl = storeItemsArray[indexPath.section].Items![indexPath.row-1].NameAr
            }
            if let _ = address{
                obj.address = address!
            }
            obj.isSubscription = isSubscription
            obj.isBanner = isBanner
            obj.categoryId = categoryId
            obj.categoryName = categoryName
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.storeItemsArray.count > 1 {
            if scrollView == self.tableView {
                var indexes = self.tableView.indexPathsForVisibleRows
                indexes!.sort()
                var index = indexes!.first!
                index[0] = index[0] > 0 ? index[0]:0
                menuSelectIndex = index.section
                let visibleIndexPath: IndexPath = [0, menuSelectIndex]
                DispatchQueue.main.async {
                    self.collectionView.scrollToItem(at:visibleIndexPath, at: .centeredHorizontally, animated: true )
                    self.collectionView.reloadData()
                }
            }
        }
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if self.storeItemsArray.count > 1 {
            if scrollView == self.tableView {
                var indexes = self.tableView.indexPathsForVisibleRows
                indexes!.sort()
                var index = indexes!.first!
                index[0] = index[0] > 0 ? index[0]:0
                menuSelectIndex = index.section
                let visibleIndexPath: IndexPath = [0, menuSelectIndex]
                DispatchQueue.main.async {
                    self.collectionView.scrollToItem(at:visibleIndexPath, at: .centeredHorizontally, animated: true )
                    self.collectionView.reloadData()
                }
            }
        }
    }
}
//MARK: CollectionView Delegate methods
@available(iOS 13.0, *)
extension ItemsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeItemsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "MenuCVCell", bundle: nil), forCellWithReuseIdentifier: "MenuCVCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCVCell", for: indexPath) as! MenuCVCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.menuNameLbl.text = storeItemsArray[indexPath.row].NameEn!
        }else{
            cell.menuNameLbl.text = storeItemsArray[indexPath.row].NameAr!
        }
        
        if menuSelectIndex == indexPath.row{
            cell.BGView.layer.cornerRadius = 15
            cell.BGView.clipsToBounds = true
            cell.BGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
            cell.menuNameLbl.textColor = .black
        }else{
            cell.BGView.layer.cornerRadius = 15
            cell.BGView.clipsToBounds = true
            cell.BGView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            cell.menuNameLbl.textColor = .black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width/3, height: 35)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if storeItemsArray[indexPath.row].sectionIsExpanded == false{
            storeItemsArray[indexPath.row].sectionIsExpanded = true
            tableView.reloadData()
        }
        
        menuSelectIndex = indexPath.row
        menuSelectIndex = menuSelectIndex > 0 ? menuSelectIndex : 0
        
        collectionView.selectItem(at: NSIndexPath(item: menuSelectIndex, section: 0) as IndexPath, animated: true, scrollPosition: .centeredHorizontally)
        if menuSelectIndex > 0{
            tableView.scrollToRow(at: IndexPath(row: 0, section: menuSelectIndex), at: .top, animated: true)
        }else{
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
        collectionView.reloadData()
    }
    
}
//MARK: SubMenu Delegate Method
@available(iOS 13.0, *)
extension ItemsVC : CategoryPopUpVCDelegate{
    func didTapAction(dic: CategoryDetailsModel) {
        categoryId = dic.Id
        if AppDelegate.getDelegate().appLanguage == "English"{
            navigationBarTitleLbl.text = dic.NameEn
        }else{
            navigationBarTitleLbl.text = dic.NameAr
        }
        if dic.NameEn == " V12" || dic.NameEn == "V12"{
            self.isV12 = true
        }else{
            self.isV12 = false
        }
        self.allDetails()
        menuSelectIndex = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if dic.NameEn == "Discover"{
                self.getDiscoverStoreItemsService()
            }else{
                self.getStoreItemsService()
            }
        }
    }
}
@available(iOS 13.0, *)
extension ItemsVC : ItemsListTVCellDelegate {
    func SelectPlayBtn_tapped(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)
        let name = storeItemsArray[indexPath!.section].Items![indexPath!.row-1].Image
         let fullNameArr = name.split(separator:".")
         if fullNameArr.count > 1 {
         if fullNameArr[1] == "mp4" {
            let cell = self.tableView.cellForRow(at: indexPath! as IndexPath) as! ItemsListTVCell?
            //let cell = tableView.cellForRow(at: indexPath!) as! ItemsListTVCell?
            if let image = cell!.playBtn.currentImage {
                if image.isEqual(UIImage(named: "play")) {
                    cell!.playBtn.setImage(UIImage(named: ""), for: .normal)
                    cell?.playerView!.player!.play()
                    cell!.contentView.bringSubviewToFront(cell!.playBtn)
                }else{
                    cell!.playBtn.isHidden = false
                    cell!.playBtn.setImage(UIImage(named: "play"), for: .normal)
                    cell!.playerView!.player!.pause()
                    cell!.contentView.bringSubviewToFront(cell!.playBtn)
                }
            }else{
                cell!.playBtn.isHidden = false
                cell!.playBtn.setImage(UIImage(named: "play"), for: .normal)
                cell!.playerView!.player!.pause()
                cell!.contentView.bringSubviewToFront(cell!.playBtn)
            }
         }
      }
    }
}
extension ItemsVC: TagsCVCellDelegate {
    func collectionView(collectionviewcell: TagsCVCell?, index: Int, didTappedInTableViewCell: ItemsListTVCell) {
        if let tagRow = didTappedInTableViewCell.rowWithTags {
            print("You tapped the cell \(index) in the row of colors \(tagRow)")
            // You can also do changes to the cell you tapped using the 'collectionviewcell'
        }
    }
}
