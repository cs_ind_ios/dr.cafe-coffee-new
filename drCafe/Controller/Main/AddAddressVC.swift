//
//  AddAddressVC.swift
//  drCafe
//
//  Created by Devbox on 08/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@available(iOS 13.0, *)
class AddAddressVC: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var locationNameTF: UITextField!
    @IBOutlet weak var placeTF: UITextField!
    @IBOutlet weak var landMarkTF: UITextField!
    @IBOutlet weak var makeThisDefaultSwitch: UISwitch!
    @IBOutlet weak var makeThisDefaultLbl: UILabel!
    @IBOutlet weak var contactPersonTF: UITextField!
    @IBOutlet weak var ContactNumTF: UITextField!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var countryCodeNameLbl: UILabel!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var typeOfAddressNameLbl: UILabel!
    @IBOutlet weak var homeNameLbl: UILabel!
    @IBOutlet weak var officeNameLbl: UILabel!
    @IBOutlet weak var otherNameLbl: UILabel!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var officeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var homeSelectImg: UIImageView!
    @IBOutlet weak var officeSelectImg: UIImageView!
    @IBOutlet weak var otherSelectImg: UIImageView!
    @IBOutlet weak var addressBGView: UIView!
    @IBOutlet weak var allDetailsBGView: UIView!
    @IBOutlet weak var deliveryInstructionsTf: UITextField!
    @IBOutlet weak var mapViewImg: UIImageView!

    var isNewUser = Bool()
    var Latitude:Double = 0.0
    var Longitude:Double = 0.0
    var address:String!
    var isManual = false

    var addressDetailsArray = [SaveAddressDetailsModel]()
    var locationManager = CLLocationManager()
    var addTypeStr = String()
    var addressId : UInt?
    var Id : Int!
    var defaultId = 0
    
    var selectCountryCode:Int!
    var selectCCMobileNumDigits:Int!
    var selectCCMinLength = 9
    var selectCCMaxLength = 9
    
    var storeLatitude:String!
    var storeLongitude:String!
    var whereObj = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        placeTF.delegate = self
        landMarkTF.delegate = self
        locationNameTF.delegate = self
        ContactNumTF.delegate = self
        isManual = false
        deliveryInstructionsTf.delegate = self
        makeThisDefaultSwitch.addTarget(self, action: #selector(switchChanged(mySwitch:)), for: .valueChanged)
        if isNewUser == true{
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.startUpdatingLocation()
//            locationManager.startMonitoringSignificantLocationChanges()
            //locationCheck()
            addressId = 0
            homeBtn_Tapped(homeBtn!)
        }else{
            addTypeStr = "Home"
            getAddressDetailsService()
        }
        
        if SaveAddressClass.CountryCodesListArray.count == 0{
            self.getCountryCodesListService()
        }else{
            if isNewUser == true{
                self.countryCodeLbl.text = "+966"
                self.selectCountryCode = 966
                self.selectCCMobileNumDigits = 9
            }
            if SaveAddressClass.CountryCodesListArray.count == 1{
                countryBtn.isHidden = true
            }else{
                countryBtn.isHidden = false
            }
        }
    }
    func locationCheck(){
        if !hasLocationPermission() {
            if Latitude == 0 && Longitude == 0{
                openSettingApp(title:NSLocalizedString("Location Services Disabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service for this app.", comment: ""))
            }
        }else{
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        }else {
            hasPermission = false
        }
        return hasPermission
    }
    func openSettingApp(title: String,message: String) {
        let alertController = UIAlertController (title: message, message:nil , preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
          guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
          }
          if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
          }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default) { (_) -> Void in
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = AddressSelectMapVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AddressSelectMapVC") as! AddressSelectMapVC
            obj.addressSelectMapVCDelegate = self
            if self.whereObj == 2{
                obj.Latitude = Double(self.storeLatitude!)!
                obj.Longitude = Double(self.storeLongitude!)!
                obj.whereObj = 2
            }
            obj.isNewUser = false
            self.navigationController?.pushViewController(obj, animated: true)
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        if isManual == false{
            if isNewUser == true{
                locationCheck()
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        //Theme Color
        self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        saveBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        self.allDetailsBGView.backgroundColor = .white
        makeThisDefaultSwitch.onTintColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        makeThisDefaultSwitch.thumbTintColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            // User Interface is Dark
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
        } else {
            // User Interface is Light
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
        }
        
        if isNewUser == true{
            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add Address", value: "", table: nil))!
        }else{
            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Edit Address", value: "", table: nil))!
        }
        typeOfAddressNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Type of address", value: "", table: nil))!
        homeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Home", value: "", table: nil))!
        officeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Office", value: "", table: nil))!
        otherNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Other", value: "", table: nil))!
        locationNameTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My home", value: "", table: nil))!
        placeTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Building Name / Flat No.", value: "", table: nil))!
        landMarkTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LandMark", value: "", table: nil))!
        contactPersonTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Person", value: "", table: nil))!
        ContactNumTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact No", value: "", table: nil))!
        deliveryInstructionsTf.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Instructions", value: "", table: nil))!

        countryCodeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Country Code", value: "", table: nil))!
        makeThisDefaultLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make this as my default address", value: "", table: nil))!
        saveBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Save", value: "", table: nil))!, for: .normal)
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            locationNameTF.textAlignment = .left
            locationNameTF.semanticContentAttribute = .forceLeftToRight
            placeTF.textAlignment = .left
            placeTF.semanticContentAttribute = .forceLeftToRight
            landMarkTF.textAlignment = .left
            landMarkTF.semanticContentAttribute = .forceLeftToRight
            contactPersonTF.textAlignment = .left
            contactPersonTF.semanticContentAttribute = .forceLeftToRight
            ContactNumTF.textAlignment = .left
            ContactNumTF.semanticContentAttribute = .forceLeftToRight
            deliveryInstructionsTf.textAlignment = .left
            deliveryInstructionsTf.semanticContentAttribute = .forceLeftToRight
        }else{
            locationNameTF.textAlignment = .right
            locationNameTF.semanticContentAttribute = .forceRightToLeft
            placeTF.textAlignment = .right
            placeTF.semanticContentAttribute = .forceRightToLeft
            landMarkTF.textAlignment = .right
            landMarkTF.semanticContentAttribute = .forceRightToLeft
            contactPersonTF.textAlignment = .right
            contactPersonTF.semanticContentAttribute = .forceRightToLeft
            ContactNumTF.textAlignment = .right
            ContactNumTF.semanticContentAttribute = .forceRightToLeft
            deliveryInstructionsTf.textAlignment = .right
            deliveryInstructionsTf.semanticContentAttribute = .forceRightToLeft
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        locationCheck()
    }
    func getCountryCodesListService(){
        let dic:[String:Any] = [:]
        UserModuleServices.getCountryCodesService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                SaveAddressClass.CountryCodesListArray = data.Data!
                if data.Data!.count == 1{
                    self.countryBtn.isHidden = true
                }else{
                    self.countryBtn.isHidden = false
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    if self.isNewUser == true{
                        let saudiCC = data.Data!.filter({$0.MobileCode == 966})
                        if saudiCC.count > 0{
                            self.countryCodeLbl.text = "+966"
                            self.selectCountryCode = 966
                            self.selectCCMobileNumDigits = saudiCC[0].NumberOfDigits
                            //self.selectCCMinLength = saudiCC[0].MinLength
                            //self.selectCCMaxLength = saudiCC[0].MaxLength
                        }else{
                            self.countryCodeLbl.text = "+966"
                            self.selectCountryCode = 966
                            self.selectCCMobileNumDigits = 9
                            self.selectCCMinLength = 9
                            self.selectCCMaxLength = 9
                        }
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    @IBAction func countryBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = SaveAddressClass.CountryCodesListArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 200, height: height)
        let pVC = obj.popoverPresentationController
        if view.frame.height > 700{
            pVC?.permittedArrowDirections = .up
        }else{
            pVC?.permittedArrowDirections = .down
        }
        pVC?.sourceView = sender
        pVC?.delegate = self
        if AppDelegate.getDelegate().appLanguage == "English"{
            if view.frame.height > 700{
                pVC?.sourceRect = CGRect(x: sender.center.x+25 , y: -15, width: 0, height: sender.frame.size.height)
            }else{
                pVC?.sourceRect = CGRect(x: sender.center.x+26 , y: sender.center.y-5, width: 0, height: sender.frame.size.height)
            }
        }else{
            if view.frame.height > 700{
                pVC?.sourceRect = CGRect(x: sender.center.x-25 , y: -15, width: 0, height: sender.frame.size.height)
            }else{
                pVC?.sourceRect = CGRect(x: sender.center.x-26 , y: sender.center.y-5, width: 0, height: sender.frame.size.height)
            }
        }
        obj.whereObj = 1
        obj.codeDropDownArray = SaveAddressClass.CountryCodesListArray
        obj.dropVCDelegate = self
        obj.isSingle = true
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    func getAddressDetailsService(){
        let dic:[String:Any] = ["Id":Id!, "UserId": UserDef.getUserId(), "RequestBy":2]
        OrderModuleServices.AddressEditService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    if data.MessageAr != nil{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }
                }
            }else{
                self.addressDetailsArray = [data.Data!]
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.AllData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    func AllData(){
        self.countryCodeLbl.text = "+\(addressDetailsArray[0].CountryCode)"
        self.selectCountryCode = Int(addressDetailsArray[0].CountryCode.removeWhitespace())
        if addressDetailsArray[0].ContactNo == "" || addressDetailsArray[0].ContactNo == " "{
            let code = SaveAddressClass.CountryCodesListArray.filter({$0.MobileCode == Int(addressDetailsArray[0].CountryCode.removeWhitespace())})
            if code.count > 0{
                self.selectCCMobileNumDigits = code[0].NumberOfDigits
            }
        }else{
            self.selectCCMobileNumDigits = addressDetailsArray[0].ContactNo.count
        }
        ContactNumTF.text = addressDetailsArray[0].ContactNo
        defaultId = addressDetailsArray[0].Default
        Latitude = Double(addressDetailsArray[0].Latitude)!
        Longitude = Double(addressDetailsArray[0].Longitude)!
        addressLbl.text = addressDetailsArray[0].Address
        locationNameTF.text = addressDetailsArray[0].HouseName
        placeTF.text = addressDetailsArray[0].HouseNo
        landMarkTF.text = "\(addressDetailsArray[0].LandMark)"
        contactPersonTF.text = addressDetailsArray[0].ContactPerson
        deliveryInstructionsTf.text = addressDetailsArray[0].DeliveryInfo

        if addressDetailsArray[0].Default == 1{
            makeThisDefaultSwitch.isOn = true
        }else{
            makeThisDefaultSwitch.isOn = false
        }
        if addressDetailsArray[0].AddressType == "Home"{
            homeSelectImg.image = UIImage(named: "Oval Copy")
            officeSelectImg.image = UIImage(named: "Oval Copy 2")
            otherSelectImg.image = UIImage(named: "Oval Copy 2")
        }else if addressDetailsArray[0].AddressType == "Office"{
            officeSelectImg.image = UIImage(named: "Oval Copy")
            homeSelectImg.image = UIImage(named: "Oval Copy 2")
            otherSelectImg.image = UIImage(named: "Oval Copy 2")
        }else if addressDetailsArray[0].AddressType == "Others"{
            otherSelectImg.image = UIImage(named: "Oval Copy")
            officeSelectImg.image = UIImage(named: "Oval Copy 2")
            homeSelectImg.image = UIImage(named: "Oval Copy 2")
        }else{
            homeSelectImg.image = UIImage(named: "Oval Copy")
            officeSelectImg.image = UIImage(named: "Oval Copy 2")
            otherSelectImg.image = UIImage(named: "Oval Copy 2")
        }
    }
    //MARK: Location Delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        self.locationManager.stopUpdatingLocation()
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!), completionHandler: { [self] response, error in

            if let addressObj = response?.firstResult() {

                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }

                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }

                self.Latitude = addressObj.coordinate.latitude
                self.Longitude = addressObj.coordinate.longitude

                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(String(describing: self.Latitude))",
                    "Longitude":"\(String(describing: self.Longitude))"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"CurrentLocationDetails")
                UserDefaults.standard.synchronize()
                self.addressLbl.text = "\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))"
                self.addressLbl.layoutIfNeeded()
                self.addressBGView.layoutIfNeeded()
                self.allDetailsBGView.layoutIfNeeded()
                //self.mapViewImg.downloaded(from: "https://maps.google.com/maps/api/staticmap?center=\(Latitude!),\(Longitude!)&zoom=18&size=40x40&scale=2&maptype=roadmap&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM")
            }
        })
    }
    //MARK: Address Button Action
    @IBAction func addressBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = AddressSelectMapVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "AddressSelectMapVC") as! AddressSelectMapVC
        obj.addressSelectMapVCDelegate = self
        if whereObj == 2{
            obj.Latitude = Double(storeLatitude!)!
            obj.Longitude = Double(storeLongitude!)!
            obj.whereObj = 2
        }
        obj.isNewUser = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Address Type Selection Button Action
    @IBAction func homeBtn_Tapped(_ sender: Any) {
        homeSelectImg.image = UIImage(named: "Oval Copy")
        officeSelectImg.image = UIImage(named: "Oval Copy 2")
        otherSelectImg.image = UIImage(named: "Oval Copy 2")
        self.addTypeStr = "Home"
    }
    @IBAction func officeBtn_Tapped(_ sender: Any) {
        homeSelectImg.image = UIImage(named: "Oval Copy 2")
        officeSelectImg.image = UIImage(named: "Oval Copy")
        otherSelectImg.image = UIImage(named: "Oval Copy 2")
        self.addTypeStr = "Office"
    }
    @IBAction func otherBtn_Tapped(_ sender: Any) {
        homeSelectImg.image = UIImage(named: "Oval Copy 2")
        officeSelectImg.image = UIImage(named: "Oval Copy 2")
        otherSelectImg.image = UIImage(named: "Oval Copy")
        self.addTypeStr = "Others"
    }
    @objc func switchChanged(mySwitch: UISwitch) {
        if makeThisDefaultSwitch.isOn == true{
            defaultId = 1
        }else{
            defaultId = 0
        }
    }
    //MARK: Save Button Action
    @IBAction func saveBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if validInputParams() == false {
            return
        }
        if whereObj == 2{
            //My location
            let myLocation = CLLocation(latitude: Latitude, longitude: Longitude)
            //My buddy's location
            let myBuddysLocation = CLLocation(latitude: Double(storeLatitude!)!, longitude: Double(storeLongitude!)!)
            //Measuring my distance to my buddy's (in km)
            let distance = myLocation.distance(from: myBuddysLocation) / 1000
            if distance < 10.0{
                self.addAddress()
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location must be within 10 KM radius", value: "", table: nil))!)
            }
        }else{
            self.addAddress()
        }
    }
    func addAddress(){
        if isNewUser == true{
            let dic:[String:Any] = ["UserId" :UserDef.getUserId(), "HouseNo" :"\(placeTF.text!)",
            "HouseName" :"\(locationNameTF.text!)",
            "Address" :"\(addressLbl.text!)",
                "AddressType" :"\(addTypeStr)",
            "CountryCode" :"\(selectCountryCode!)",
                "Default" :defaultId,
            "LandMark" :"\(landMarkTF.text!)",
                "Latitude" :"\(Latitude)",
                "Longitude" :"\(Longitude)",
                "ContactPerson" :"\(contactPersonTF.text!)",
                "ContactNo" :"\(ContactNumTF.text!)",
                "DeliveryInfo" :"\(deliveryInstructionsTf.text!)",
                "RequestBy" :2]
            OrderModuleServices.AddressSaveService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        if data.MessageAr != nil{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr!)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                        }
                    }
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }else{
            let dic:[String:Any] = ["Id":Id!, "UserId": UserDef.getUserId(), "HouseNo" : "\(placeTF.text!)",
            "HouseName" :"\(locationNameTF.text!)",
            "Address" : "\(addressLbl.text!)",
                "AddressType" : "\(addTypeStr)",
            "CountryCode" : "\(selectCountryCode!)",
                "Default": defaultId,
            "LandMark" :" \(landMarkTF.text!)",
                "Latitude": "\(Latitude)",
                "Longitude" : "\(Longitude)",
                "ContactPerson" : "\(contactPersonTF.text!)",
                "ContactNo" : "\(ContactNumTF.text!)", "RequestBy":2]
            OrderModuleServices.AddressUpdateService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    // text fields validation
    func validInputParams() -> Bool {
        if self.placeTF.text == nil || (self.placeTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BuildingName", value: "", table: nil))!)
            return false
        }
//        if ContactNumTF.text != nil && (ContactNumTF.text != ""){
//            if self.contactPersonTF.text == nil || (self.contactPersonTF.text == ""){
//                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter contact person name", value: "", table: nil))!)
//                return false
//            }
//            if !Validations.isMobileNumberValid(mobile: ContactNumTF.text!, numOfDifits: selectCCMobileNumDigits){
//                Alert.showAlert(on: self, title:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))! , message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter valid Phone Number", value: "", table: nil))!)
//                return false
//            }
//        }
//        if contactPersonTF.text != nil && (contactPersonTF.text != ""){
//            if self.ContactNumTF.text == nil || (self.ContactNumTF.text == ""){
//                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter contact person mobile number", value: "", table: nil))!)
//                return false
//            }
//        }
        
        if ContactNumTF.text == nil || (ContactNumTF.text == ""){
//            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please enter Phone Number", value: "", table: nil))!)
//            return false
        }else {
            if !Validations.isMobileNumberValidWithNumOfDigits(mobile: ContactNumTF.text!, minDigits: selectCCMinLength, maxDigits: selectCCMaxLength){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range1 = string.rangeOfCharacter(from: whitespaceSet)
        if ((textField.text?.count)! == 0 && range1 != nil)
              || ((textField.text?.count)! > 0 && textField.text?.last == " " && range1 != nil) {
            return false
        }
        if textField == self.locationNameTF || textField == self.landMarkTF || textField == self.placeTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 50
        }
        if textField == self.ContactNumTF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                updatedText.safelyLimitedTo(length: selectCCMobileNumDigits)
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
@available(iOS 13.0, *)
//MARK: Address Selection Delegate
extension AddAddressVC: AddressSelectMapVCDelegate{
    func selectLocation(address: String, latitude: Double, longitude: Double) {
        addressLbl.text = address
        Latitude = latitude
        Longitude = longitude
        isManual = true
    }
}
//MARK: Country Code Selection Delegate
@available(iOS 13.0, *)
extension AddAddressVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        countryCodeLbl.text = "+\(ID)"
        selectCountryCode = Int(ID)
        let selectDic = SaveAddressClass.CountryCodesListArray.filter({$0.CountryNameEn == name})
        self.selectCCMobileNumDigits = selectDic[0].NumberOfDigits
        ContactNumTF.text = ""
    }
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
