//
//  NutritionDetailsVC.swift
//  drCafe
//
//  Created by Devbox on 30/04/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class NutritionDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dropDownArray = [["Name":"","Calories":""]]
    var allergensArray:[String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        if #available(iOS 15.0, *) {
            //UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
            tableView.sectionHeaderTopPadding = 0
        }
    }
    @IBAction func poupCancelBtn_Tapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
@available(iOS 13.0, *)
extension NutritionDetailsVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = allergensArray{
            if allergensArray.count > 0{
                return 2
            }else{
                return 1
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = allergensArray{
            if allergensArray.count > 0{
                if section == 0{
                    return dropDownArray.count
                }else{
                    return allergensArray.count
                }
            }else{
                return dropDownArray.count
            }
        }else{
            return dropDownArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!) as? NutritionDetailsTVCell else {
          return nil
        }
        if section == 0{
            cell.nutritionNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Nutrition", value: "", table: nil))!
            cell.nutritionNameLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
            cell.nutritionNameLbl.textColor = .black
            cell.nutritionCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Units", value: "", table: nil))!
            cell.nutritionCalLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
            cell.nutritionCalLbl.textColor = .black
            cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else{
            cell.nutritionNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Allergens", value: "", table: nil))!
            cell.nutritionNameLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
            cell.nutritionNameLbl.textColor = .black
            cell.nutritionCalLbl.text = ""
            cell.BGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NutritionDetailsTVCell", value: "", table: nil))!, for: indexPath) as! NutritionDetailsTVCell
        if let _ = allergensArray{
            if allergensArray.count > 0{
                if indexPath.section == 0{
                    cell.nutritionNameLbl.text = dropDownArray[indexPath.row]["Name"]
                    cell.nutritionCalLbl.text = dropDownArray[indexPath.row]["Calories"]
                }else{
                    cell.nutritionNameLbl.text = allergensArray[indexPath.row]
                    cell.nutritionCalLbl.text = ""
                }
            }else{
                cell.nutritionNameLbl.text = dropDownArray[indexPath.row]["Name"]
                cell.nutritionCalLbl.text = dropDownArray[indexPath.row]["Calories"]
            }
        }else{
            cell.nutritionNameLbl.text = dropDownArray[indexPath.row]["Name"]
            cell.nutritionCalLbl.text = dropDownArray[indexPath.row]["Calories"]
        }
        cell.nutritionNameLbl.font = UIFont(name: "Avenir Book", size: 14.0)
        cell.nutritionCalLbl.font = UIFont(name: "Avenir Book", size: 14.0)
        cell.BGView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        return cell
    }
}
