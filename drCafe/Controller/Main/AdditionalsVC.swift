//
//  AdditionalsVC.swift
//  drCafe
//
//  Created by Mac2 on 11/08/22.
//  Copyright © 2022 Devbox. All rights reserved.
//

import UIKit
import UIKit
import AVFoundation
import AVKit

@available(iOS 13.0, *)
class AdditionalsVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var topDetailsView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var cartNumLbl: UILabel!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var navigationBarBGView: UIView!
    @IBOutlet weak var sizeImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var allDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sizesTVMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nutritionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noteViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var itemImg: UIImageView!
    
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    @IBOutlet weak var tagsCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tagsCollectionViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var pointsNameLbl: UILabel!
    @IBOutlet weak var rewardPointsLbl: UILabel!
    @IBOutlet weak var pointsBtn: UIButton!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var calView: UIView!
    @IBOutlet weak var calMainView: UIView!
    @IBOutlet weak var calMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calMainViewBtmLineLbl: UILabel!
    
    @IBOutlet weak var weightNameLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var weightViewHeight: NSLayoutConstraint!
    @IBOutlet weak var outOfStockLbl: UILabel!
    
    @IBOutlet weak var quantityAmountLbl: UILabel!
    @IBOutlet weak var successfullyAddedMsgLbl: UILabel!
    @IBOutlet weak var currecyNameLbl: UILabel!
    @IBOutlet weak var successfullyAddedMsgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sizeTableView: UITableView!
    @IBOutlet weak var sizeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeNameLbl: UILabel!
    @IBOutlet weak var sizeDescLbl: UILabel!
    @IBOutlet weak var calLbl: UILabel!
    @IBOutlet weak var sizeWtLbl: UILabel!
    @IBOutlet weak var sizeWtLblHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeWtLblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noteView: UIView!
    
    @IBOutlet weak var selectBeanMainView: UIView!
    @IBOutlet weak var selectBeanMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectBeanCV: UICollectionView!
    
    @IBOutlet weak var topCoffeeTypeTableView: UITableView!
    @IBOutlet weak var topCoffeeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topCoffeeTypeTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var additionalsTableView: UITableView!
    @IBOutlet weak var additionalsTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var noteTF: UITextField!
    @IBOutlet weak var noteTV: UITextView!
    @IBOutlet weak var noteLineLbl: UILabel!
    
    @IBOutlet weak var moreCoffeeTypeTableView: UITableView!
    @IBOutlet weak var moreCoffeeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var moreCoffeeTypeTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var coffeeTypeTableView: UITableView!
    @IBOutlet weak var coffeeTypeLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeBelowLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeBottomConstraint: NSLayoutConstraint!
    
    //Not Recommend
    @IBOutlet weak var customizedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customizedNameLbl: UILabel!
    
    @IBOutlet weak var additionalsNotRecommendTableView: UITableView!
    @IBOutlet weak var additionalsNotRecommendTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var moreCoffeeTypeNotRecommendTableView: UITableView!
    @IBOutlet weak var moreCoffeeTypeNotRecommendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var moreCoffeeTypeNotRecommendTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var coffeeTypeNotRecommendTableView: UITableView!
    @IBOutlet weak var coffeeTypeNotRecommendTVHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeNotRecommendLineLbl: UILabel!
    @IBOutlet weak var coffeeTypeNotRecommendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coffeeTypeNotRecommendBelowLineLbl: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    @IBOutlet weak var recommendedNameLbl: UILabel!
    @IBOutlet weak var recommendedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var RecommendedTableView: UITableView!
    @IBOutlet weak var RecommendedTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recommendedCV: UICollectionView!
    
    @IBOutlet weak var addBtnView: UIView!
    @IBOutlet weak var addMoreBtnView: UIView!
    @IBOutlet weak var noThanksView: UIView!
    @IBOutlet weak var noThanksBtn: UIButton!
    @IBOutlet weak var addMoreBtn: UIButton!
    @IBOutlet weak var viewCartBtn: UIButton!
    
    @IBOutlet weak var nutritionInfoBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var selectNutritionCalLbl: UILabel!
    @IBOutlet weak var allDetailsView: UIView!
    
    @IBOutlet weak var noRecordFoundView: UIView!
    @IBOutlet weak var noRecordFoundLbl: UILabel!
    
    @IBOutlet var popUpLblMainView: UIView!
    @IBOutlet weak var popUpNameLbl: UILabel!
    
    
    var quantity:Int = 1
    var totalAmount:Double = 1

    var popover = Popover()
    static var instance: AdditionalsVC!
    var backWhereObj = 0
    
    var whereObj = 1
    var sectionType = 0
    var isManualLocation = false
    
    var addressDetailsArray = [AddressDetailsModel]()
    var dropDownArray = [DropDownModel]()
    var nutritionDetailsArray:[[String:Any]]!
    var selectNutrition:String!
    var dropWhereObj:Int!
    var selectIndex:Int!
    var itemID:Int!
    var storeId:Int!
    var selectOrderType:Int!
    var selectAddressId:Int!
    var isV12 = false
    var address:String!
    
    var selectSizeId:Int!
    var selectSizePrice:Double!
    var selectSizeIndex = 1
    var grinderItemId = 0
    var otherBeanSelect = false
    var otherBeanSelectId = 0
    
    var isSubscription = false
    var isCustomized = false
    
    var isBanner = false
    var categoryId:Int!
    var categoryName:String!
    
    var allergensArray:[String]!
    var additionalsArray = [AdditionalsDetailsModel]()
    var latteDetailsArray = [AdditionalGroupsModel]()
    var latteNotRecommendDetailsArray = [AdditionalGroupsModel]()
    var grinderDetailsArray = [AdditionalGroupsModel]()
    var additionalItemsArray = [AdditionalGroupsModel]()
    var minSelectAdditionalItemsArray = [AdditionalGroupsModel]()
    var topMinSelectAdditionalItemsArray = [AdditionalGroupsModel]()
    var titleLbl:String!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    var itemImagesArray = [ItemImagesModel]()
    @IBOutlet weak var bannerPageController: UIPageControl!
    var SwiftTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        AdditionalsVC.instance = self
        let width = UIScreen.main.bounds.width - 30
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: width, height: width * 0.7)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .horizontal
        bannerCollectionView.collectionViewLayout = layout
        bannerCollectionView.dataSource = self
        bannerCollectionView.delegate = self
        tabBarController?.tabBar.isHidden = true
        quantityLbl.text = "\(quantity)"
        
        RecommendedTableView.delegate = self
        RecommendedTableView.dataSource = self
        recommendedCV.delegate = self
        recommendedCV.dataSource = self
        coffeeTypeTableView.delegate = self
        coffeeTypeTableView.dataSource = self
        moreCoffeeTypeTableView.delegate = self
        moreCoffeeTypeTableView.dataSource = self
        sizeTableView.delegate = self
        sizeTableView.dataSource = self
        noteTV.delegate = self
        topCoffeeTypeTableView.delegate = self
        topCoffeeTypeTableView.dataSource = self
        
        coffeeTypeNotRecommendTableView.delegate = self
        coffeeTypeNotRecommendTableView.dataSource = self
        moreCoffeeTypeNotRecommendTableView.delegate = self
        moreCoffeeTypeNotRecommendTableView.dataSource = self
        additionalsNotRecommendTableView.delegate = self
        additionalsNotRecommendTableView.dataSource = self
        
        selectBeanCV.delegate = self
        selectBeanCV.dataSource = self
        additionalsTableView.delegate = self
        additionalsTableView.dataSource = self
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        
        RecommendedTableView.estimatedRowHeight = 130
        RecommendedTableView.rowHeight = UITableView.automaticDimension
        
        getStoreAdditionalsService()
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            bannerCollectionView.semanticContentAttribute = .forceLeftToRight
            selectBeanCV.semanticContentAttribute = .forceLeftToRight
            tagsCollectionView.semanticContentAttribute = .forceLeftToRight
            recommendedCV.semanticContentAttribute = .forceLeftToRight
        }else{
            bannerCollectionView.semanticContentAttribute = .forceRightToLeft
            selectBeanCV.semanticContentAttribute = .forceRightToLeft
            tagsCollectionView.semanticContentAttribute = .forceRightToLeft
            recommendedCV.semanticContentAttribute = .forceRightToLeft
        }
        
        bannerPageController.addTarget(self, action: #selector(pageControllerSelection(_:)), for: .valueChanged)
        self.noteTV.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!) :"
        self.noteTV.textColor = .lightGray
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        
        if backWhereObj == 1{
            ItemsVC.instance.backWhereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            if isV12 == true{
                navigationBarBGView.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                navigationBarTitleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                view.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 1)
                cartNumLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    backBtn.setImage(UIImage(named: "back white"), for: .normal)
                }else{
                    backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                }
                cartBtn.setImage(UIImage(named: "cart white"), for: .normal)
            }else{
                navigationBarBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                navigationBarTitleLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cartNumLbl.textColor = #colorLiteral(red: 0.3568627451, green: 0.1803921569, blue: 0.1725490196, alpha: 1)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    backBtn.setImage(UIImage(named: "Back"), for: .normal)
                }else{
                    backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                }
                cartBtn.setImage(UIImage(named: "Cart"), for: .normal)
            }
            addBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            addMoreBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            viewCartBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            
            navigationBarTitleLbl.font = UIFont(name: "Avenir Medium", size: 16.0)
            outOfStockLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
            pointsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Points", value: "", table: nil))!
            customizedNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "customized", value: "", table: nil))!
            if whereObj == 2{
                self.navigationController?.popViewController(animated: true)
            }
            navigationBarTitleLbl.text! = titleLbl!
            if isSubscription == false{
                cartNumLbl.text = "\(AppDelegate.getDelegate().cartQuantity)"
            }else{
                cartNumLbl.text = "\(AppDelegate.getDelegate().subCartQuantity)"
            }
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[2]
                if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                    tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                    tabItems[2].image = UIImage(named: "Cart Tab")
                    tabItems[2].selectedImage = UIImage(named: "Cart Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                }else{
                    tabItem.badgeValue = nil
                    tabItems[2].image = UIImage(named: "Tab Order")
                    tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                    tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                }
            }
            
            //Theme Color
            myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            noRecordFoundView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                noRecordFoundLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                recommendedNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                noRecordFoundLbl.textColor = .darkGray
                recommendedNameLbl.textColor = .darkGray
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
    }
    //MARK: Start Timer Banner Scrolling
    func startTimer() {
        SwiftTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(HomeVC.scrollToNextCell), userInfo: nil, repeats: true);
    }
    //MARK: Banner Scroll To next cell
    @objc func scrollToNextCell(){
        //get cell size
        let cellSize = CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = bannerCollectionView.contentOffset;
        if bannerCollectionView.contentSize.width <= bannerCollectionView.contentOffset.x + cellSize.width{
            bannerCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y  , width: cellSize.width, height: cellSize.height), animated: false)
        }else{
            bannerCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        if bannerPageController.currentPage == bannerPageController.numberOfPages - 1 {
            bannerPageController.currentPage = 0
        } else {
            bannerPageController.currentPage += 1
        }
    }
    @objc func pageControllerSelection(_ sender: UIPageControl){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let page: Int? = sender.currentPage
            let nextItem: IndexPath = IndexPath(item: page!, section: 0)
            if nextItem.row < self.itemImagesArray.count {
                self.bannerCollectionView.scrollToItem(at: nextItem, at: .right, animated: true)
            }
        }
    }
    func getStoreAdditionalsService(){
        let dic:[String:Any] = ["StoreId": storeId!, "ItemId": itemID!, "OrderTypeId": selectOrderType!, "RequestBy":2, "IsSubscription": isSubscription]
        OrderModuleServices.GetAdditionalItemsService(dic: dic, success: { (data) in
            if(data.Status == false){
                self.mainView.isHidden = true
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.additionalsArray = data.Data!
                if self.additionalsArray.count > 0 {
                    if self.additionalsArray[0].ItemImages != nil{
                        //self.itemImagesArray = self.additionalsArray[0].ItemImages!
                        self.itemImagesArray = self.additionalsArray[0].ItemImages!.filter({$0.IsFeatured == true})
                        if self.itemImagesArray.count > 0{
                            self.itemImagesArray.append(contentsOf: self.additionalsArray[0].ItemImages!.filter({$0.IsFeatured == false}))
                        }else{
                            self.itemImagesArray = self.additionalsArray[0].ItemImages!.filter({$0.IsFeatured == false})
                        }
                    }
                    self.bannerPageController.numberOfPages = self.itemImagesArray.count
                    self.bannerCollectionView.reloadData()
                    let nextItem: IndexPath = IndexPath(item: 0, section: 0)
                    self.bannerCollectionView.scrollToItem(at: nextItem, at: .right, animated: true)
                    //self.startTimer()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    // additionals section expand functionality
                    if self.additionalsArray.count > 0{
                        self.noRecordFoundView.isHidden = true
                        if self.additionalsArray[0].AdditionalGroups != nil{
                            print("Additionals Not Empty")
                            if self.additionalsArray[0].AdditionalGroups!.count > 0{
                                for i in 0...self.additionalsArray[0].AdditionalGroups!.count-1{
                                    if self.additionalsArray[0].AdditionalGroups![i].MinSelection! >= 1{
                                        self.additionalsArray[0].AdditionalGroups![i].sectionIsExpand = true
                                    }else{
                                        self.additionalsArray[0].AdditionalGroups![i].sectionIsExpand = false
                                    }
                                    
                                    if self.additionalsArray[0].AdditionalGroups![i].Additionals != nil{
                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![0].ItemSelect = false
                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![0].additionalQty = 0
                                        if self.additionalsArray[0].AdditionalGroups![i].Additionals!.count > 1{
                                            for j in 0...self.additionalsArray[0].AdditionalGroups![i].Additionals!.count-1{
                                                self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                            }
                                        }
                                        if self.additionalsArray[0].AdditionalGroups![i].Additionals!.count > 1{
                                            for j in 0...self.additionalsArray[0].AdditionalGroups![i].Additionals!.count-1{
                                                if self.additionalsArray[0].AdditionalGroups![i].Additionals![j].AdditionalPrices![0].IsRecommended != nil{
                                                    if self.additionalsArray[0].AdditionalGroups![i].Additionals![j].AdditionalPrices![0].IsRecommended! == true{
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = true
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 1
                                                    }else{
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                        self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                                    }
                                                }else{
                                                    self.additionalsArray[0].AdditionalGroups![i].Additionals![j].ItemSelect = false
                                                    self.additionalsArray[0].AdditionalGroups![i].Additionals![j].additionalQty = 0
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("Additionals Empty")
                            }
                        }else{
                            print("Additionals Empty")
                        }
                        if self.additionalsArray[0].Grinders != nil{
                            if self.additionalsArray[0].Grinders!.count > 0{
                                for i in 0...self.additionalsArray[0].Grinders!.count-1{
                                    self.additionalsArray[0].Grinders![i].selectGrinder = false
                                }
                                self.additionalsArray[0].Grinders![0].selectGrinder = true
                                self.grinderItemId = self.additionalsArray[0].Grinders![0].Id
                            }
                        }
                        if let _ = self.additionalsArray[0].Allergens{
                            if self.additionalsArray[0].Allergens! != ""{
                                self.allergensArray = self.additionalsArray[0].Allergens!.components(separatedBy: ",")
                            }
                        }
                        self.AllDetails()
                    }else{
                        self.noRecordFoundView.isHidden = false
                        print("No Item Details")
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: All Details
    func AllDetails(){
        recommendedViewHeight.constant = 0
        
        //Tag Details
        if additionalsArray[0].Tags != nil{
            if additionalsArray[0].Tags!.count > 0{
                tagsCollectionViewHeight.constant = 40
                tagsCollectionViewTop.constant = 10
                tagsCollectionView.reloadData()
            }else{
                tagsCollectionViewHeight.constant = 0
                tagsCollectionViewTop.constant = 0
            }
        }else{
            tagsCollectionViewHeight.constant = 0
            tagsCollectionViewTop.constant = 0
        }
        if AppDelegate.getDelegate().appLanguage == "English"{
            itemNameLbl.text = additionalsArray[0].NameEn
            itemDescLbl.text = additionalsArray[0].DescEn
            //sizeDescLbl.text = additionalsArray[0].Prices![0].DescEn
            //sizeNameLbl.text = additionalsArray[0].Prices![0].SizeEn
        }else{
            itemNameLbl.text = additionalsArray[0].NameAr
            itemDescLbl.text = additionalsArray[0].DescAr
            //sizeDescLbl.text = additionalsArray[0].Prices![0].DescAr
            //sizeNameLbl.text = additionalsArray[0].Prices![0].SizeAr
        }
        
        var defaultSizeArray = [PriceDetailsModel]()
        if let _ = additionalsArray[0].Prices{
            for i in 0...additionalsArray[0].Prices!.count-1{
                if let _ = additionalsArray[0].Prices![i].DefaultSize{
                    if additionalsArray[0].Prices![i].DefaultSize! == true && additionalsArray[0].Prices![i].IsOutOfStock! == 0{
                        selectSizeIndex = i+1
                        defaultSizeArray = [additionalsArray[0].Prices![i]]
                    }
                }
            }
        }
        if defaultSizeArray.count > 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                sizeDescLbl.text = defaultSizeArray[0].DescEn
                sizeNameLbl.text = defaultSizeArray[0].SizeEn
            }else{
                sizeDescLbl.text = defaultSizeArray[0].DescAr
                sizeNameLbl.text = defaultSizeArray[0].SizeAr
            }
            
            selectSizePrice = defaultSizeArray[0].Price!
            selectSizeId = defaultSizeArray[0].SizeId!
            addBtn.isEnabled = true
            plusBtn.isEnabled = true
            minusBtn.isEnabled = true
            addBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            
            //Item Size Image
            let ImgStr1:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(defaultSizeArray[0].Image!)" as NSString
            let charSet1 = CharacterSet.urlFragmentAllowed
            let urlStr1 : NSString = ImgStr1.addingPercentEncoding(withAllowedCharacters: charSet1)! as NSString
            let url1 = URL.init(string: urlStr1 as String)
            sizeImage.kf.setImage(with: url1)
            
            selectNutritionCalLbl.text = "\(defaultSizeArray[0].TotalFat!) KL"
            var UOM = defaultSizeArray[0].UOM!
            if UOM == "Whole"{
                UOM = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Whole", value: "", table: nil))!
            }
            nutritionDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calcium", value: "", table: nil))!,"Calories":defaultSizeArray[0].Calcium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Iron", value: "", table: nil))!,"Calories":defaultSizeArray[0].Iron!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminC", value: "", table: nil))!,"Calories":defaultSizeArray[0].VitaminC!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminA", value: "", table: nil))!,"Calories":defaultSizeArray[0].VitaminA!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Protein", value: "", table: nil))!,"Calories":defaultSizeArray[0].Protein!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sugars", value: "", table: nil))!,"Calories":defaultSizeArray[0].Sugars!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "DietaryFibers", value: "", table: nil))!,"Calories":defaultSizeArray[0].DietaryFibers!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Carbohydrates", value: "", table: nil))!,"Calories":defaultSizeArray[0].Carbohydrates!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sodium", value: "", table: nil))!,"Calories":defaultSizeArray[0].Sodium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cholestrol", value: "", table: nil))!,"Calories":defaultSizeArray[0].Cholestrol!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TransFat", value: "", table: nil))!,"Calories":defaultSizeArray[0].TransFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "SaturatedFat", value: "", table: nil))!,"Calories":defaultSizeArray[0].SaturatedFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "UOM", value: "", table: nil))!,"Calories":UOM],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "CaloriesFromFat", value: "", table: nil))!,"Calories":defaultSizeArray[0].CaloriesFromFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calories", value: "", table: nil))!,"Calories":defaultSizeArray[0].Calories!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TotalFat", value: "", table: nil))!,"Calories":defaultSizeArray[0].TotalFat!]]
        }else{
            if let _ = additionalsArray[0].Prices{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    sizeDescLbl.text = additionalsArray[0].Prices![0].DescEn
                    sizeNameLbl.text = additionalsArray[0].Prices![0].SizeEn
                }else{
                    sizeDescLbl.text = additionalsArray[0].Prices![0].DescAr
                    sizeNameLbl.text = additionalsArray[0].Prices![0].SizeAr
                }
                
                let priceDetails = additionalsArray[0].Prices!.filter({$0.IsOutOfStock! == 0})
                if priceDetails.count > 0{
                    selectSizePrice = priceDetails[0].Price!
                    selectSizeId = priceDetails[0].SizeId!
                    addBtn.isEnabled = true
                    plusBtn.isEnabled = true
                    minusBtn.isEnabled = true
                    addBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
                }else{
                    selectSizePrice = additionalsArray[0].Prices![0].Price!
                    selectSizeId = additionalsArray[0].Prices![0].SizeId!
                    addBtn.isEnabled = false
                    plusBtn.isEnabled = false
                    minusBtn.isEnabled = false
                    addBtn.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 0.6957804699)
                }
                
                //Item Size Image
                let ImgStr1:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].Prices![0].Image!)" as NSString
                let charSet1 = CharacterSet.urlFragmentAllowed
                let urlStr1 : NSString = ImgStr1.addingPercentEncoding(withAllowedCharacters: charSet1)! as NSString
                let url1 = URL.init(string: urlStr1 as String)
                sizeImage.kf.setImage(with: url1)
                
                selectNutritionCalLbl.text = "\(additionalsArray[0].Prices![0].TotalFat!) KL"
                var UOM = additionalsArray[0].Prices![0].UOM!
                if UOM == "Whole"{
                    UOM = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Whole", value: "", table: nil))!
                }
                nutritionDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calcium", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Calcium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Iron", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Iron!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminC", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].VitaminC!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminA", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].VitaminA!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Protein", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Protein!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sugars", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Sugars!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "DietaryFibers", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].DietaryFibers!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Carbohydrates", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Carbohydrates!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sodium", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Sodium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cholestrol", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Cholestrol!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TransFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].TransFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "SaturatedFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].SaturatedFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "UOM", value: "", table: nil))!,"Calories":UOM],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "CaloriesFromFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].CaloriesFromFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calories", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].Calories!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TotalFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![0].TotalFat!]]
            }
        }
    
        myScrollView.layoutIfNeeded()
        
        if let _ = additionalsArray[0].Prices{
            //Weight Functionality
            if additionalsArray[0].Prices!.count == 1{
                if additionalsArray[0].Prices![0].Weight! > 0.0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        weightLbl.text = "\(additionalsArray[0].Prices![0].Weight!.withCommas()) \(additionalsArray[0].Prices![0].WeightUOM!)"
                    }else{
                        weightLbl.text = "\(additionalsArray[0].Prices![0].WeightUOM!) \(additionalsArray[0].Prices![0].Weight!.withCommas())"
                    }
                    if additionalsArray[0].Prices![0].IsOutOfStock! == 1{
                        outOfStockLbl.isHidden = false
                    }else{
                        outOfStockLbl.isHidden = true
                    }
                    weightViewHeight.constant = 40
                    calMainViewBtmLineLbl.isHidden = true
                    weightNameLbl.isHidden = false
                    weightLbl.isHidden = false
                }else{
                    if additionalsArray[0].Prices![0].IsOutOfStock! == 1{
                        weightViewHeight.constant = 40
                        calMainViewBtmLineLbl.isHidden = true
                        outOfStockLbl.isHidden = false
                    }else{
                        weightViewHeight.constant = 0
                        calMainViewBtmLineLbl.isHidden = false
                        outOfStockLbl.isHidden = true
                    }
                    weightNameLbl.isHidden = true
                    weightLbl.isHidden = true
                }
            }else{
                weightViewHeight.constant = 0
                calMainViewBtmLineLbl.isHidden = false
                outOfStockLbl.isHidden = true
                weightNameLbl.isHidden = true
                weightLbl.isHidden = true
            }
            var sizeWeight = 0.0
            var sizeWeightUOM = ""
            if defaultSizeArray.count > 0{
                sizeWeight = defaultSizeArray[0].Weight!
                sizeWeightUOM = defaultSizeArray[0].WeightUOM!
            }else{
                sizeWeight = additionalsArray[0].Prices![0].Weight!
                sizeWeightUOM = additionalsArray[0].Prices![0].WeightUOM!
            }
            if sizeWeight > 0.0{
                sizeWtLbl.text = "WT : \(sizeWeight.withCommas()) \(sizeWeightUOM)"
                sizeWtLblHeight.constant = 21
                sizeWtLblTopConstraint.constant = 5
            }else{
                sizeWtLbl.text = ""
                sizeWtLblHeight.constant = 0
                sizeWtLblTopConstraint.constant = 0
            }

        }
                
        var sizeCalories = ""
        if defaultSizeArray.count > 0{
            sizeCalories = defaultSizeArray[0].Calories!
        }else{
            if let _ = additionalsArray[0].Prices{
                sizeCalories = additionalsArray[0].Prices![0].Calories!
            }
        }
        if additionalsArray[0].RewardEligibility != nil{
            if sizeCalories != "" || additionalsArray[0].RewardEligibility! == true{
                self.calMainViewHeight.constant = 50
                if additionalsArray[0].RewardEligibility! == true{
                    pointsView.isHidden = false
                    if additionalsArray[0].RewardBand != nil{
                        rewardPointsLbl.text = "\(String(describing: additionalsArray[0].RewardBand!))"
                    }else{
                        rewardPointsLbl.text = ""
                    }
                }else{
                    pointsView.isHidden = true
                }
                if sizeCalories != ""{
                    calLbl.text = "\(sizeCalories) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                    calView.isHidden = false
                }else{
                    calView.isHidden = true
                }
            }else{
                self.calMainViewHeight.constant = 0
            }
        }else{
            pointsView.isHidden = true
            if sizeCalories != ""{
                calLbl.text = "\(sizeCalories) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                calView.isHidden = false
                self.calMainViewHeight.constant = 50
            }else{
                calView.isHidden = true
                self.calMainViewHeight.constant = 0
            }
        }
        
        //Item Image
        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].Image)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        itemImg.kf.setImage(with: url)
        
        additionalItemsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 1 && $0.MinSelection == 0})
        //minSelectAdditionalItemsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 1 && $0.MinSelection != 0})
        latteDetailsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 2 && $0.MinSelection != 0})
        latteNotRecommendDetailsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 2 && $0.MinSelection == 0})
        grinderDetailsArray = additionalsArray[0].AdditionalGroups!.filter({$0.GroupType! == 3})
        var isFirst = true
        minSelectAdditionalItemsArray.removeAll()
        topMinSelectAdditionalItemsArray.removeAll()
        if additionalsArray[0].AdditionalGroups != nil{
            if additionalsArray[0].AdditionalGroups!.count > 0{
                for i in 0...additionalsArray[0].AdditionalGroups!.count - 1{
                    if additionalsArray[0].AdditionalGroups![i].GroupType! == 3{
                        isFirst = false
                    }else if additionalsArray[0].AdditionalGroups![i].GroupType! == 2{
                        isFirst = false
                    }else{
                        if isFirst == true{
                            if additionalsArray[0].AdditionalGroups![i].GroupType! == 1 && additionalsArray[0].AdditionalGroups![i].MinSelection! != 0{
                                topMinSelectAdditionalItemsArray.append(additionalsArray[0].AdditionalGroups![i])
                            }
                        }else{
                            if additionalsArray[0].AdditionalGroups![i].GroupType! == 1 && additionalsArray[0].AdditionalGroups![i].MinSelection! != 0{
                                minSelectAdditionalItemsArray.append(additionalsArray[0].AdditionalGroups![i])
                            }
                        }
                    }
                }
            }
        }
        
        if additionalItemsArray.count == 0 && latteDetailsArray.count == 0 && grinderDetailsArray.count == 0 && minSelectAdditionalItemsArray.count == 0{
            coffeeTypeBelowLineLbl.isHidden = true
            coffeeTypeBottomConstraint.constant = 0
        }else{
            coffeeTypeBelowLineLbl.isHidden = false
            coffeeTypeBottomConstraint.constant = 15
        }
        
        //Grinders functionality
        if additionalsArray[0].Grinders != nil{
            if additionalsArray[0].Grinders!.count > 0{
                if additionalsArray[0].ManualBrew != nil{
                    if additionalsArray[0].ManualBrew! == true{
                        self.selectBeanMainViewHeight.constant = 280
                    }else{
                        self.selectBeanMainViewHeight.constant = 255
                    }
                }else{
                    self.selectBeanMainViewHeight.constant = 255
                }
                DispatchQueue.main.async {
                    self.selectBeanCV.reloadData()
                }
                coffeeTypeLineLbl.isHidden = false
                myScrollView.layoutIfNeeded()
            }else{
                coffeeTypeLineLbl.isHidden = true
                self.selectBeanMainViewHeight.constant = 0
                myScrollView.layoutIfNeeded()
            }
        }else{
            coffeeTypeLineLbl.isHidden = true
            self.selectBeanMainViewHeight.constant = 0
            myScrollView.layoutIfNeeded()
        }
        
        //Additionals Functionality
        if additionalsArray[0].AdditionalGroups != nil{
            if latteDetailsArray.count > 0 {
                DispatchQueue.main.async {
                    self.additionalsTableViewHeight.constant = 1
                    self.additionalsTableView.reloadData()
                }
            }else{
                additionalsTableViewHeight.constant = 0
            }
            if latteNotRecommendDetailsArray.count > 0 {
                self.isCustomized = true
                DispatchQueue.main.async {
                    self.additionalsNotRecommendTableViewHeight.constant = 1
                    self.additionalsNotRecommendTableView.reloadData()
                }
            }else{
                additionalsNotRecommendTableViewHeight.constant = 0
            }
            
            if grinderDetailsArray.count > 0{
                if grinderDetailsArray[0].MinSelection == 0{
                    isCustomized = true
                    moreCoffeeTypeNotRecommendTableViewHeight.constant = CGFloat(grinderDetailsArray.count * 50)
                    moreCoffeeTypeNotRecommendViewHeight.constant = CGFloat(grinderDetailsArray.count * 50) + 21
                    moreCoffeeTypeNotRecommendTableView.reloadData()
                }else{
                    moreCoffeeTypeTableViewHeight.constant = CGFloat(grinderDetailsArray.count * 50)
                    moreCoffeeTypeViewHeight.constant = CGFloat(grinderDetailsArray.count * 50) + 21
                    moreCoffeeTypeTableView.reloadData()
                }
            }else{
                moreCoffeeTypeTableViewHeight.constant = 0
                moreCoffeeTypeViewHeight.constant = 0
                moreCoffeeTypeNotRecommendTableViewHeight.constant = 0
                moreCoffeeTypeNotRecommendViewHeight.constant = 0
            }
            
            if additionalsArray.count > 0{
                if minSelectAdditionalItemsArray.count > 0{
                    coffeeTypeTableViewHeight.constant = CGFloat(minSelectAdditionalItemsArray.count * 50)
                    coffeeTypeViewHeight.constant = CGFloat(minSelectAdditionalItemsArray.count * 50) + 21
                    coffeeTypeTableView.reloadData()
                }else{
                    coffeeTypeTableViewHeight.constant = 0
                    coffeeTypeViewHeight.constant = 0
                }
                if topMinSelectAdditionalItemsArray.count > 0{
                    topCoffeeTypeTableViewHeight.constant = CGFloat(topMinSelectAdditionalItemsArray.count * 50)
                    topCoffeeTypeViewHeight.constant = CGFloat(topMinSelectAdditionalItemsArray.count * 50) + 21
                    topCoffeeTypeTableView.reloadData()
                }else{
                    topCoffeeTypeTableViewHeight.constant = 0
                    topCoffeeTypeViewHeight.constant = 0
                }
                if additionalItemsArray.count > 0{
                    isCustomized = true
                    coffeeTypeNotRecommendTVHeight.constant = CGFloat(additionalItemsArray.count * 50)
                    coffeeTypeNotRecommendViewHeight.constant = CGFloat(additionalItemsArray.count * 50) + 21
                    coffeeTypeNotRecommendTableView.reloadData()
                }else{
                    coffeeTypeNotRecommendTVHeight.constant = 0
                    coffeeTypeNotRecommendViewHeight.constant = 0
                }
            }else{
                coffeeTypeTableViewHeight.constant = 0
                coffeeTypeViewHeight.constant = 0
                customizedViewHeight.constant = 0
                coffeeTypeNotRecommendTVHeight.constant = 0
                topCoffeeTypeTableViewHeight.constant = 0
                topCoffeeTypeViewHeight.constant = 0
            }
            myScrollView.layoutIfNeeded()
        }else{
            self.additionalsTableViewHeight.constant = 0
            self.additionalsNotRecommendTableViewHeight.constant = 0
            moreCoffeeTypeTableViewHeight.constant = 0
            moreCoffeeTypeTableViewHeight.constant = 0
            coffeeTypeTableViewHeight.constant = 0
            coffeeTypeViewHeight.constant = 0
            topCoffeeTypeTableViewHeight.constant = 0
            topCoffeeTypeViewHeight.constant = 0
            coffeeTypeNotRecommendTVHeight.constant = 0
            myScrollView.layoutIfNeeded()
        }
        
        noteLineLbl.isHidden = true
        
        DispatchQueue.main.async {
            if let _ = self.additionalsArray[0].Prices{
                if self.additionalsArray[0].Prices!.count > 1{
                    self.sizesTVMainViewHeight.constant = CGFloat((30 * self.additionalsArray[0].Prices!.count) + 50)
                    self.sizeTableView.reloadData()
                }else{
                    self.sizesTVMainViewHeight.constant = 0
                }
            }else{
                self.sizesTVMainViewHeight.constant = 0
            }
            
            if self.isCustomized == true{
                self.customizedViewHeight.constant = 50
            }else{
                self.customizedViewHeight.constant = 0
            }
        }
        self.priceCalculation()
        self.mainView.isHidden = false
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isManualLocation == true{
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Calories Info Button Action
    @IBAction func caloriesInfoBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "NutritionDetailsVC") as! NutritionDetailsVC
        obj.dropDownArray = nutritionDetailsArray as! [[String : String]]
        if let _ = allergensArray{
            if allergensArray.count > 0{
                obj.allergensArray = allergensArray
            }
        }
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        present(obj, animated: false, completion: nil)
    }
    //MARK: Nutrition Info Button Action
    @IBAction func nutritionInfoBtn_Tapped(_ sender: UIButton) {
        let obj : DropVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropVC") as! DropVC)
        let height = dropDownArray.count * 50
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.modalPresentationStyle = .popover
        obj.preferredContentSize = CGSize(width: 300, height: height)
        let pVC = obj.popoverPresentationController
        pVC?.permittedArrowDirections = .up
        pVC?.sourceView = sender
        pVC?.delegate = self
        pVC?.sourceRect = CGRect(x: sender.center.x , y: 0, width: 0, height: sender.frame.size.height)
        obj.whereObj = 2
        obj.dropVCDelegate = self
        obj.isSingle = false
        self.present(obj, animated: false, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    //MARK: Cart Button Action
    @IBAction func cartBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CartVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            obj.isSubscription = isSubscription
            obj.whereObj = 5
            obj.isRemove = true
            obj.isMenu = true
            obj.selectAddressID = selectAddressId
            obj.sectionType = sectionType
            obj.orderType = selectOrderType!
            obj.storeId = storeId!
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Plus Button Action
    @IBAction func plusBtn_Tapped(_ sender: Any) {
        if additionalsArray.count == 0 {
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have exceeded max quantity for this item", value: "", table: nil))!)
            return
        }
        if additionalsArray[0].MaxOrderQty > quantity{
            quantity = quantity + 1
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You have exceeded max quantity for this item", value: "", table: nil))!)
        }
        priceCalculation()
    }
    //MARK: Minus Button Action
    @IBAction func minusBtn_Tapped(_ sender: Any) {
        if quantity > 1{
            quantity = quantity - 1
            priceCalculation()
        }else{
            quantity = 1
        }
    }
    //MARK: Add Button Action
    @IBAction func addBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            if self.addressDetailsArray.count > 0{
                AppDelegate.getDelegate().selectAddress = self.addressDetailsArray[0].Address
                AppDelegate.getDelegate().cartStoreName = ""
            }else{
                AppDelegate.getDelegate().selectAddress = ""
                if let _ = self.address{
                    AppDelegate.getDelegate().cartStoreName = self.address!
                }else{
                    AppDelegate.getDelegate().cartStoreName = ""
                }
            }
            self.addToCart()
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Add To Cart function
    func addToCart(){
        AppDelegate.getDelegate().cartStoreId = storeId
        var additionals:[[String:Any]] = [[:]]
        var additionalName:[String] = [""]
        if additionalsArray[0].AdditionalGroups != nil{
            if additionalsArray[0].AdditionalGroups!.count > 0{
                var additionalArray:[String:Any]!
                if self.grinderDetailsArray.count > 0{
                    for i in 0...grinderDetailsArray.count-1{
                        if grinderDetailsArray[i].MinSelection! > 0{
                            let minSelect = grinderDetailsArray[i].MinSelection!
                            let minSelectArray = grinderDetailsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect <= minSelectArray.count{
                                for j in 0...grinderDetailsArray[i].Additionals!.count - 1{
                                    if grinderDetailsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": grinderDetailsArray[i].Id!,"MenuAdditionalId": grinderDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": grinderDetailsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(grinderDetailsArray[i].NameEn!)
                                }else{
                                    additionalName.append(grinderDetailsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...grinderDetailsArray[i].Additionals!.count - 1{
                                if grinderDetailsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": grinderDetailsArray[i].Id!,"MenuAdditionalId": grinderDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": grinderDetailsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if self.latteDetailsArray.count > 0{
                    for i in 0...latteDetailsArray.count-1{
                        if latteDetailsArray[i].MinSelection! > 0{
                            let minSelect = latteDetailsArray[i].MinSelection!
                            let minSelectArray = latteDetailsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect == minSelectArray.count{
                                for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                                    if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": latteDetailsArray[i].Id!,"MenuAdditionalId": latteDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteDetailsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(latteDetailsArray[i].NameEn!)
                                }else{
                                    additionalName.append(latteDetailsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                                if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": latteDetailsArray[i].Id!,"MenuAdditionalId": latteDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteDetailsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if self.latteNotRecommendDetailsArray.count > 0{
                    for i in 0...latteNotRecommendDetailsArray.count-1{
                        if latteNotRecommendDetailsArray[i].MinSelection! > 0{
                            let minSelect = latteNotRecommendDetailsArray[i].MinSelection!
                            let minSelectArray = latteNotRecommendDetailsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect == minSelectArray.count{
                                for j in 0...latteNotRecommendDetailsArray[i].Additionals!.count - 1{
                                    if latteNotRecommendDetailsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": latteNotRecommendDetailsArray[i].Id!,"MenuAdditionalId": latteNotRecommendDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteNotRecommendDetailsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(latteNotRecommendDetailsArray[i].NameEn!)
                                }else{
                                    additionalName.append(latteNotRecommendDetailsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...latteNotRecommendDetailsArray[i].Additionals!.count - 1{
                                if latteNotRecommendDetailsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": latteNotRecommendDetailsArray[i].Id!,"MenuAdditionalId": latteNotRecommendDetailsArray[i].Additionals![j].AdditionalItemId,"Quantity": latteNotRecommendDetailsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if self.additionalItemsArray.count > 0{
                    for i in 0...additionalItemsArray.count - 1{
                        if additionalItemsArray[i].MinSelection! > 0{
                            let minSelect = additionalItemsArray[i].MinSelection!
                            let minSelectArray = additionalItemsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect == minSelectArray.count{
                                for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                                    if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": additionalItemsArray[i].Id!,"MenuAdditionalId": additionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": additionalItemsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(additionalItemsArray[i].NameEn!)
                                }else{
                                    additionalName.append(additionalItemsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                                if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": additionalItemsArray[i].Id!,"MenuAdditionalId": additionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": additionalItemsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if self.minSelectAdditionalItemsArray.count > 0{
                    for i in 0...minSelectAdditionalItemsArray.count - 1{
                        if minSelectAdditionalItemsArray[i].MinSelection! > 0{
                            let minSelect = minSelectAdditionalItemsArray[i].MinSelection!
                            let minSelectArray = minSelectAdditionalItemsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect == minSelectArray.count{
                                for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                                    if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": minSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": minSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(minSelectAdditionalItemsArray[i].NameEn!)
                                }else{
                                    additionalName.append(minSelectAdditionalItemsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                                if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": minSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": minSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if self.topMinSelectAdditionalItemsArray.count > 0{
                    for i in 0...topMinSelectAdditionalItemsArray.count - 1{
                        if topMinSelectAdditionalItemsArray[i].MinSelection! > 0{
                            let minSelect = topMinSelectAdditionalItemsArray[i].MinSelection!
                            let minSelectArray = topMinSelectAdditionalItemsArray[i].Additionals!.filter({$0.ItemSelect! == true})
                            if minSelect == minSelectArray.count{
                                for j in 0...topMinSelectAdditionalItemsArray[i].Additionals!.count - 1{
                                    if topMinSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                        additionalArray = ["AdditionalGroupId": topMinSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": topMinSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                        additionals.append(additionalArray!)
                                    }
                                }
                            }else{
                                if AppDelegate.getDelegate().appLanguage == "English"{
                                    additionalName.append(topMinSelectAdditionalItemsArray[i].NameEn!)
                                }else{
                                    additionalName.append(topMinSelectAdditionalItemsArray[i].NameAr!)
                                }
                            }
                        }else{
                            for j in 0...topMinSelectAdditionalItemsArray[i].Additionals!.count - 1{
                                if topMinSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                                    additionalArray = ["AdditionalGroupId": topMinSelectAdditionalItemsArray[i].Id!,"MenuAdditionalId": topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalItemId,"Quantity": topMinSelectAdditionalItemsArray[i].Additionals![j].additionalQty!]
                                    additionals.append(additionalArray!)
                                }
                            }
                        }
                    }
                }
                if additionals.count > 1{
                    additionals.remove(at: 0)
                }
                if additionalName.count > 1{
                    additionalName.remove(at: 0)
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please Select Additionals in", value: "", table: nil))!) \(additionalName.joined(separator: ","))")
                    return
                }
            }else{
                additionals = [[:]]
            }
        }else{
            additionals = [[:]]
        }
        var note = ""
        if noteTV.text! != "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!) :"{
            note = noteTV.text!
        }
        
        let dic:[String:Any] = ["userId": UserDef.getUserId(),"StoreId": storeId!,"OrderType": selectOrderType!,"MainCategoryId": additionalsArray[0].MainCategoryId,"CategoryId": additionalsArray[0].CategoryId,"SubCategoryId": additionalsArray[0].SubCategoryId,"MenuItemId": additionalsArray[0].Id,"GrinderItemId":"\(grinderItemId)","SizeId": selectSizeId!,"Quantity": quantity,"Comment": "\(note)","AddressId" : selectAddressId!,"OrderStatusId" : 1,"Additionals": additionals, "RequestBy":2, "SectionType":sectionType, "IsSubscription" : isSubscription]
        CartModuleServices.AddToCartService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if let _ = data.Data!.CartItemsQuantity{
                    self.cartNumLbl.text = "\(data.Data!.CartItemsQuantity!)"
                    if self.isSubscription == true{
                        AppDelegate.getDelegate().subCartQuantity = data.Data!.CartItemsQuantity!
                    }else{
                        AppDelegate.getDelegate().cartQuantity = data.Data!.CartItemsQuantity!
                    }
                }
                if let _ = data.Data!.TotalPrice{
                    if self.isSubscription == true{
                        AppDelegate.getDelegate().subCartAmount = data.Data!.TotalPrice!
                    }else{
                        AppDelegate.getDelegate().cartAmount = data.Data!.TotalPrice!
                    }
                }
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[2]
                    if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0{
                        tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                        tabItems[2].image = UIImage(named: "Cart Tab")
                        tabItems[2].selectedImage = UIImage(named: "Cart Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
                    }else{
                        tabItem.badgeValue = nil
                        tabItems[2].image = UIImage(named: "Tab Order")
                        tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                        tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
                    }
                }
                AppDelegate.getDelegate().selectSectionType = self.sectionType
                DispatchQueue.main.async {
                    if self.additionalsArray[0].SuggestiveItems != nil{
                        if self.additionalsArray[0].SuggestiveItems!.count > 0{
                            self.successfullyAddedMsgViewHeight.constant = 35
                            self.allDetailsViewHeight.constant = 0
                            self.sizesTVMainViewHeight.constant = 0
                            self.nutritionViewHeight.constant = 0
                            self.noteViewHeight.constant = 0
                            self.selectBeanMainViewHeight.constant = 0
                            self.additionalsTableViewHeight.constant = 0
                            self.additionalsNotRecommendTableViewHeight.constant = 0
                            self.topCoffeeTypeViewHeight.constant = 0
                            self.topCoffeeTypeTableViewHeight.constant = 0
                            self.coffeeTypeViewHeight.constant = 0
                            self.coffeeTypeNotRecommendTVHeight.constant = 0
                            self.coffeeTypeNotRecommendViewHeight.constant = 0
                            self.weightViewHeight.constant = 0
                            self.calMainViewHeight.constant = 0
                            self.coffeeTypeLineLbl.isHidden = true
                            self.coffeeTypeNotRecommendLineLbl.isHidden = true
                            self.moreCoffeeTypeViewHeight.constant = 0
                            self.moreCoffeeTypeTableViewHeight.constant = 0
                            self.moreCoffeeTypeNotRecommendViewHeight.constant = 0
                            self.moreCoffeeTypeNotRecommendTableViewHeight.constant = 0
                            self.customizedViewHeight.constant = 0
                            self.isCustomized = false
                            self.grinderItemId = 0
                            self.recommendedViewHeight.constant = 305
                            DispatchQueue.main.async {
                                self.recommendedCV.reloadData()
                            }
                            self.quantityAmountLbl.text = "\(self.selectSizePrice!.withCommas()) X \(self.quantity) = \(self.totalAmount.withCommas())"
                            if let _ = self.additionalsArray[0].SuggestiveItems{
                                self.noThanksView.isHidden = true
                                self.addMoreBtnView.isHidden = false
                                self.addBtnView.isHidden = true
                            }else{
                                self.noThanksView.isHidden = true
                                self.addMoreBtnView.isHidden = false
                                self.addBtnView.isHidden = true
                            }
                        }else{
                            if self.isBanner == false{
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                let obj:ItemsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                                obj.categoryId = self.categoryId
                                obj.categoryName = self.categoryName
                                obj.address = self.address
                                obj.storeId = self.storeId
                                obj.selectAddressID = 0
                                obj.isV12 = false
                                obj.selectOrderType = self.selectOrderType
                                obj.isBanner = self.isBanner
                                if let _ = self.itemID{
                                    obj.itemID = self.itemID!
                                }
                                if let _ = self.titleLbl{
                                    obj.itemName = self.titleLbl!
                                }
                                obj.isStores = false
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                        }
                    }else{
                        if self.isBanner == false{
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                            let obj:ItemsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                            obj.categoryId = self.categoryId
                            obj.categoryName = self.categoryName
                            obj.address = self.address
                            obj.storeId = self.storeId
                            obj.selectAddressID = 0
                            obj.isV12 = false
                            obj.selectOrderType = self.selectOrderType
                            obj.isBanner = self.isBanner
                            if let _ = self.itemID{
                                obj.itemID = self.itemID!
                            }
                            if let _ = self.titleLbl{
                                obj.itemName = self.titleLbl!
                            }
                            obj.isStores = false
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: No thanks Button Action
    @IBAction func noThanksBtn_Tapped(_ sender: Any) {
        noThanksView.isHidden = true
        addMoreBtnView.isHidden = false
        addBtnView.isHidden = true
    }
    //MARK: Share Button Action
    @IBAction func shareBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async() {
            let ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(self.additionalsArray[0].Image)"
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let imageUrl = URL.init(string: urlStr as String)
            if let data = try? Data(contentsOf: imageUrl!){
                let ItemName = self.additionalsArray[0].NameEn
                let ItemDes = "\n\(self.additionalsArray[0].DescEn)"
                let image = UIImage(data: data)
                let message = "\n\nDownload the app at"
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let shareUrl = URL.init(string: urlStr as String)
            
                let shareAll = [image as Any, ItemName, ItemDes, message, shareUrl as Any] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }else{
                let ItemName = self.additionalsArray[0].NameEn
                let ItemDes = "\n\(self.additionalsArray[0].DescEn)"
                let message = "\n\nDownload the app at"
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = AppURL.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let shareUrl = URL.init(string: urlStr as String)
                let shareAll = [ItemName, ItemDes, message, shareUrl as Any] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    //MARK: Add More Button Action
    @IBAction func addMoreBtn_Tapped(_ sender: Any) {
        if self.isBanner == false{
            ItemsVC.instance.backWhereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = MenuVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            obj.address = address!
            obj.categoryId = self.categoryId
            if itemID != nil{
                obj.itemId = itemID!
            }
            if titleLbl != nil{
                obj.itemName = titleLbl!
            }
            obj.categoryName = self.categoryName
            obj.storeID = storeId!
            obj.emptyCart = false
            obj.selectOrderType = selectOrderType!
            obj.isBanner = isBanner
            obj.isItemsBack = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    @IBAction func itemImageBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.imageName = additionalsArray[0].Image
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    //MARK: View Cart Button Action
    @IBAction func viewCartBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CartVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        obj.isSubscription = isSubscription
        obj.whereObj = 5
        obj.isRemove = true
        obj.isMenu = true
        obj.selectAddressID = selectAddressId
        obj.sectionType = sectionType
        obj.orderType = selectOrderType!
        obj.storeId = storeId!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Price calculation
    func priceCalculation(){
        quantityLbl.text = "\(quantity)"
        var itemPrice = selectSizePrice
        if selectSizePrice == nil{
            itemPrice = 0.0
        }
        var additionalPrice = 0
        //additional Select Price Calculation
        if additionalItemsArray.count > 0{
            for i in 0...additionalItemsArray.count - 1{
                for j in 0...additionalItemsArray[i].Additionals!.count - 1{
                    if additionalItemsArray[i].Additionals![j].ItemSelect! == true{
                        //additionalPrice = Int(additionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(additionalItemsArray[i].Additionals![j].additionalQty!))
                        if additionalItemsArray[i].Additionals![j].HasVariant == false{
                            if additionalItemsArray[i].Additionals![j].MaxQty > 1{
                                var addPrice = additionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price
                                addPrice = addPrice * Double(additionalItemsArray[i].Additionals![j].additionalQty!)
                                additionalPrice = Int(addPrice)
                            }else{
                                additionalPrice = Int(additionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price)
                            }
                        }else{
                            let priceDetails = additionalItemsArray[i].Additionals![j].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                            if priceDetails.count > 0{
                                additionalPrice = Int(priceDetails[0].Price * Double(additionalItemsArray[i].Additionals![j].additionalQty!))
                            }else{
                                additionalPrice = 0
                            }
                        }
                        itemPrice = itemPrice! + Double(additionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        var minAdditionalPrice = 0
        //additional Select Price Calculation
        if minSelectAdditionalItemsArray.count > 0{
            for i in 0...minSelectAdditionalItemsArray.count - 1{
                for j in 0...minSelectAdditionalItemsArray[i].Additionals!.count - 1{
                    if minSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                        //minAdditionalPrice = Int(minSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!))
                        if minSelectAdditionalItemsArray[i].Additionals![j].HasVariant == false{
                            if minSelectAdditionalItemsArray[i].Additionals![j].MaxQty > 1{
                                var addPrice = minSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price
                                addPrice = addPrice * Double(minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!)
                                minAdditionalPrice = Int(addPrice)
                            }else{
                                minAdditionalPrice = Int(minSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price)
                            }
                        }else{
                            let priceDetails = minSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                            if priceDetails.count > 0{
                                minAdditionalPrice = Int(priceDetails[0].Price * Double(minSelectAdditionalItemsArray[i].Additionals![j].additionalQty!))
                            }else{
                                minAdditionalPrice = 0
                            }
                        }
                        itemPrice = itemPrice! + Double(minAdditionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        var topMinAdditionalPrice = 0
        //additional Select Price Calculation
        if topMinSelectAdditionalItemsArray.count > 0{
            for i in 0...topMinSelectAdditionalItemsArray.count - 1{
                for j in 0...topMinSelectAdditionalItemsArray[i].Additionals!.count - 1{
                    if topMinSelectAdditionalItemsArray[i].Additionals![j].ItemSelect! == true{
                        //topMinAdditionalPrice = Int(topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(topMinSelectAdditionalItemsArray[i].Additionals![j].additionalQty!))
                        if topMinSelectAdditionalItemsArray[i].Additionals![j].HasVariant == false{
                            if topMinSelectAdditionalItemsArray[i].Additionals![j].MaxQty > 1{
                                var addPrice = topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price
                                addPrice = addPrice * Double(topMinSelectAdditionalItemsArray[i].Additionals![j].additionalQty!)
                                topMinAdditionalPrice = Int(addPrice)
                            }else{
                                topMinAdditionalPrice = Int(topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices![0].Price)
                            }
                        }else{
                            let priceDetails = topMinSelectAdditionalItemsArray[i].Additionals![j].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                            if priceDetails.count > 0{
                                topMinAdditionalPrice = Int(priceDetails[0].Price * Double(topMinSelectAdditionalItemsArray[i].Additionals![j].additionalQty!))
                            }else{
                                topMinAdditionalPrice = 0
                            }
                        }
                        itemPrice = itemPrice! + Double(topMinAdditionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        //Grinder Select Price Calculation
        if additionalsArray[0].ManualBrew != nil{
            if additionalsArray[0].ManualBrew! == true{
                let grinderDetails =  additionalsArray[0].Grinders!.filter({$0.selectGrinder! == true})
                if grinderDetails.count > 0{
                    if grinderDetails[0].Price > 0{
                        let price = grinderDetails[0].Price + itemPrice!
                        itemPrice = price
                    }
                }
            }
        }
        
        //Additional Price
        if latteDetailsArray.count > 0{
            for i in 0...latteDetailsArray.count - 1{
                for j in 0...latteDetailsArray[i].Additionals!.count - 1{
                    if latteDetailsArray[i].Additionals![j].ItemSelect! == true{
                        //additionalPrice = Int(latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price * Double(latteDetailsArray[i].Additionals![j].additionalQty!))
                        if latteDetailsArray[i].Additionals![j].HasVariant == false{
                            if latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price == 0 || latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price == 0.0{
                                additionalPrice = 0
                            }else{
                                if latteDetailsArray[i].Additionals![j].MaxQty > 1{
                                    var addPrice = latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price
                                    addPrice = addPrice * Double(latteDetailsArray[i].Additionals![j].additionalQty!)
                                    additionalPrice = Int(addPrice)
                                }else{
                                    additionalPrice = Int(latteDetailsArray[i].Additionals![j].AdditionalPrices![0].Price)
                                }
                            }
                        }else{
                            let priceDetails = latteDetailsArray[i].Additionals![j].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                            if priceDetails.count > 0{
                                additionalPrice = Int(priceDetails[0].Price * Double(latteDetailsArray[i].Additionals![j].additionalQty!))
                            }else{
                                additionalPrice = 0
                            }
                        }
                        itemPrice = itemPrice! + Double(additionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        //Additional Price
        if latteNotRecommendDetailsArray.count > 0{
            for i in 0...latteNotRecommendDetailsArray.count - 1{
                for j in 0...latteNotRecommendDetailsArray[i].Additionals!.count - 1{
                    if latteNotRecommendDetailsArray[i].Additionals![j].ItemSelect! == true{
                        if latteNotRecommendDetailsArray[i].Additionals![j].HasVariant == false{
                            if latteNotRecommendDetailsArray[i].Additionals![j].AdditionalPrices![0].Price == 0 || latteNotRecommendDetailsArray[i].Additionals![j].AdditionalPrices![0].Price == 0.0{
                                additionalPrice = 0
                            }else{
                                if latteNotRecommendDetailsArray[i].Additionals![j].MaxQty > 1{
                                    var addPrice = latteNotRecommendDetailsArray[i].Additionals![j].AdditionalPrices![0].Price
                                    addPrice = addPrice * Double(latteNotRecommendDetailsArray[i].Additionals![j].additionalQty!)
                                    additionalPrice = Int(addPrice)
                                }else{
                                    additionalPrice = Int(latteNotRecommendDetailsArray[i].Additionals![j].AdditionalPrices![0].Price)
                                }
                            }
                        }else{
                            let priceDetails = latteNotRecommendDetailsArray[i].Additionals![j].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                            if priceDetails.count > 0{
                                additionalPrice = Int(priceDetails[0].Price * Double(latteNotRecommendDetailsArray[i].Additionals![j].additionalQty!))
                            }else{
                                additionalPrice = 0
                            }
                        }
                        itemPrice = itemPrice! + Double(additionalPrice)
                    }else{
                        itemPrice = itemPrice! + 0.0
                    }
                }
            }
        }else{
            print("Additionals Empty")
        }
        
        totalAmount = Double(itemPrice! * Double(quantity))
        totalAmountLbl.text = "\(totalAmount.withCommas())"
        if itemPrice == 0{
            addBtn.isEnabled = false
            plusBtn.isEnabled = false
            minusBtn.isEnabled = false
            addBtn.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.1843137255, blue: 0.1843137255, alpha: 0.6957804699)
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension AdditionalsVC:UITableViewDelegate, UITableViewDataSource, BeveragesListTVCellDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == coffeeTypeNotRecommendTableView{
            if additionalsArray.count > 0{
                return additionalItemsArray.count
            }else{
                return 0
            }
        }else if tableView == topCoffeeTypeTableView{
            if additionalsArray.count > 0{
                return topMinSelectAdditionalItemsArray.count
            }else{
                return 0
            }
        }else if tableView == coffeeTypeTableView{
            if additionalsArray.count > 0{
                return minSelectAdditionalItemsArray.count
            }else{
                return 0
            }
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if grinderDetailsArray.count > 0{
                return grinderDetailsArray.count
            }else{
                return 0
            }
        }else if tableView == additionalsTableView{
            if additionalsArray.count > 0{
                return latteDetailsArray.count
            }else{
                return 0
            }
        }else if tableView == additionalsNotRecommendTableView{
            if additionalsArray.count > 0{
                return latteNotRecommendDetailsArray.count
            }else{
                return 0
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == coffeeTypeNotRecommendTableView{
            if additionalsArray.count > 0{
                if additionalItemsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return additionalItemsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == topCoffeeTypeTableView{
            if additionalsArray.count > 0{
                if topMinSelectAdditionalItemsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return topMinSelectAdditionalItemsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == coffeeTypeTableView{
            if additionalsArray.count > 0{
                if minSelectAdditionalItemsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return minSelectAdditionalItemsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if grinderDetailsArray.count > 0{
                if grinderDetailsArray[section].sectionIsExpand == false{
                    return 1
                }else{
                    return grinderDetailsArray[section].Additionals!.count + 1
                }
            }else{
                return 0
            }
        }else if tableView == sizeTableView{
            if additionalsArray.count > 0{
                if let _ = additionalsArray[0].Prices{
                    return additionalsArray[0].Prices!.count + 1
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else if tableView == additionalsTableView || tableView == additionalsNotRecommendTableView{
            if additionalsArray.count > 0{
                return 2
            }else{
                return 0
            }
        }else{
            //if additionalsArray.count > 0{
               // return additionalsArray[0].SuggestiveItems!.count
            //}else{
                return 0
            //}
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == coffeeTypeNotRecommendTableView {
            return 50
        }else if tableView == coffeeTypeTableView || tableView == topCoffeeTypeTableView{
            return 50
        }else if tableView == moreCoffeeTypeTableView || tableView == moreCoffeeTypeNotRecommendTableView{
            if indexPath.row == 0{
                return 50
            }else{
                return 80
            }
        }else if tableView == sizeTableView{
            if additionalsArray[0].ManualBrew! == true && additionalsArray[0].Grinders!.count > 0{
                return 40
            }else{
                return 30
            }
        }else if tableView == additionalsTableView{
            if indexPath.row == 0{
                return 55
            }else{
                let priceArray =  latteDetailsArray[indexPath.section].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                let quantityArray =  latteDetailsArray[indexPath.section].Additionals!.filter({$0.MaxQty > 1})
                var quantityHeight = 0
                if quantityArray.count > 0{
                    quantityHeight = 40
                }else{
                    quantityHeight = 0
                }
                var priceHeight = 0
                if priceArray.count > 0{
                    priceHeight = 27
                }else{
                    priceHeight = 0
                }
                return CGFloat(131 + priceHeight + quantityHeight)
            }
        }else if tableView == additionalsNotRecommendTableView{
            if indexPath.row == 0{
                return 55
            }else{
                let priceArray =  latteNotRecommendDetailsArray[indexPath.section].Additionals!.filter({$0.AdditionalPrices!.filter({$0.Price > 0}).count > 0})
                let quantityArray =  latteNotRecommendDetailsArray[indexPath.section].Additionals!.filter({$0.MaxQty > 1})
                var quantityHeight = 0
                if quantityArray.count > 0{
                    quantityHeight = 40
                }else{
                    quantityHeight = 0
                }
                var priceHeight = 0
                if priceArray.count > 0{
                    priceHeight = 27
                }else{
                    priceHeight = 0
                }
                return CGFloat(131 + priceHeight + quantityHeight)
            }
        }else{
            return 140
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == coffeeTypeNotRecommendTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Coffee Type Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                if additionalItemsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(additionalItemsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(additionalItemsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(additionalItemsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(additionalItemsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = additionalItemsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(dropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Coffee Type Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesListTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].HasVariant == false{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0 || additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0.0{
                        subCell.itemCalLbl.isHidden = true
                    }else{
                        if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                            var addPrice = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price
                            addPrice = addPrice * Double(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                            subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                        }else{
                            subCell.itemCalLbl.text = "\(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price.withCommas())"
                        }
                        subCell.itemCalLbl.isHidden = false
                        subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }else{
                    let priceDetails = additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        if priceDetails[0].Price == 0 || priceDetails[0].Price == 0.0{
                            subCell.itemCalLbl.isHidden = true
                        }else{
                            if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                                var addPrice = priceDetails[0].Price
                                addPrice = addPrice * Double(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                                subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                            }else{
                                subCell.itemCalLbl.text = "\(priceDetails[0].Price.withCommas())"
                            }
                            subCell.itemCalLbl.isHidden = false
                            subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                        }
                    }else{
                        subCell.itemCalLbl.isHidden = true
                    }
                }
                
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 1{
                    subCell.itemCalLbl.isHidden = false
                    subCell.itemCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                    subCell.itemCalLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                }

                if additionalItemsArray[indexPath.section].MaxSelection! > 1{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                subCell.delegate = self
                subCell.tableView = coffeeTypeNotRecommendTableView
                
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                    subCell.quantityViewWidth.constant = 85
                    subCell.quantityLbl.text = "\(additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)"
                }else{
                    subCell.quantityViewWidth.constant = 0
                }
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.coffeeTypeNotRecommendTableView.layoutIfNeeded()
                self.coffeeTypeNotRecommendTVHeight.constant = self.coffeeTypeNotRecommendTableView.contentSize.height
                self.coffeeTypeNotRecommendViewHeight.constant = self.coffeeTypeNotRecommendTableView.contentSize.height + 21
            }
            cell.selectionStyle = .none
            return cell
        }else if tableView == topCoffeeTypeTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Coffee Type Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                if topMinSelectAdditionalItemsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(topMinSelectAdditionalItemsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(topMinSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(topMinSelectAdditionalItemsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(topMinSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = topMinSelectAdditionalItemsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = topMinSelectAdditionalItemsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(topMinDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Coffee Type Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesListTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].HasVariant == false{
                    if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0 || topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0.0{
                        subCell.itemCalLbl.isHidden = true
                    }else{
                        if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                            var addPrice = topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price
                            addPrice = addPrice * Double(topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                            subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                        }else{
                            subCell.itemCalLbl.text = "\(topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price.withCommas())"
                        }
                        subCell.itemCalLbl.isHidden = false
                        subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }else{
                    let priceDetails = topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        if priceDetails[0].Price == 0 || priceDetails[0].Price == 0.0{
                            subCell.itemCalLbl.isHidden = true
                        }else{
                            if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                                var addPrice = priceDetails[0].Price
                                addPrice = addPrice * Double(topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                                subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                            }else{
                                subCell.itemCalLbl.text = "\(priceDetails[0].Price.withCommas())"
                            }
                            subCell.itemCalLbl.isHidden = false
                            subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                        }
                    }else{
                        subCell.itemCalLbl.isHidden = true
                    }
                }
                
                if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 1{
                    subCell.itemCalLbl.isHidden = false
                    subCell.itemCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                    subCell.itemCalLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                }

                if topMinSelectAdditionalItemsArray[indexPath.section].MaxSelection! > 1{
                    if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                subCell.tableView = topCoffeeTypeTableView
                subCell.delegate = self

                if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                    subCell.quantityViewWidth.constant = 85
                    subCell.quantityLbl.text = "\(topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)"
                }else{
                    subCell.quantityViewWidth.constant = 0
                }
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.topCoffeeTypeTableView.layoutIfNeeded()
                self.topCoffeeTypeTableViewHeight.constant = self.topCoffeeTypeTableView.contentSize.height
                self.topCoffeeTypeViewHeight.constant = self.topCoffeeTypeTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
        }else if tableView == coffeeTypeTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Coffee Type Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                if minSelectAdditionalItemsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(minSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(minSelectAdditionalItemsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(minDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Coffee Type Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesListTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesListTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].HasVariant == false{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0 || minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price == 0.0{
                        subCell.itemCalLbl.isHidden = true
                    }else{
                        if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                            var addPrice = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price
                            addPrice = addPrice * Double(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                            subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                        }else{
                            subCell.itemCalLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices![0].Price.withCommas())"
                        }
                        subCell.itemCalLbl.isHidden = false
                        subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }
                }else{
                    let priceDetails = minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].AdditionalPrices!.filter({$0.SizeId == selectSizeId})
                    if priceDetails.count > 0{
                        if priceDetails[0].Price == 0 || priceDetails[0].Price == 0.0{
                            subCell.itemCalLbl.isHidden = true
                        }else{
                            if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                                var addPrice = priceDetails[0].Price
                                addPrice = addPrice * Double(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)
                                subCell.itemCalLbl.text = "\(addPrice.withCommas())"
                            }else{
                                subCell.itemCalLbl.text = "\(priceDetails[0].Price.withCommas())"
                            }
                            subCell.itemCalLbl.isHidden = false
                            subCell.itemCalLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                        }
                    }else{
                        subCell.itemCalLbl.isHidden = true
                    }
                }
                
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 1{
                    subCell.itemCalLbl.isHidden = false
                    subCell.itemCalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                    subCell.itemCalLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                }

                if minSelectAdditionalItemsArray[indexPath.section].MaxSelection! > 1{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                subCell.tableView = coffeeTypeTableView
                subCell.delegate = self

                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].MaxQty > 1{
                    subCell.quantityViewWidth.constant = 85
                    subCell.quantityLbl.text = "\(minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty!)"
                }else{
                    subCell.quantityViewWidth.constant = 0
                }
                
                cell = subCell
            }
            
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.coffeeTypeTableView.layoutIfNeeded()
                self.coffeeTypeTableViewHeight.constant = self.coffeeTypeTableView.contentSize.height
                self.coffeeTypeViewHeight.constant = self.coffeeTypeTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
        } else if tableView == sizeTableView{
            if additionalsArray[0].ManualBrew! == true && additionalsArray[0].Grinders!.count > 0{
                //MARK: Size TableView Details
                sizeTableView.register(UINib(nibName: "SettingsVCTVCell", bundle: nil), forCellReuseIdentifier: "SettingsVCTVCell")
                let cell = sizeTableView.dequeueReusableCell(withIdentifier: "SettingsVCTVCell", for: indexPath) as! SettingsVCTVCell
                
                if indexPath.row == 0{
                    cell.nameLbl.text = "  \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Size", value: "", table: nil))!)"
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.nameLbl.font = UIFont(name: "Avenir Medium", size: 16)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.nameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeEn!)"
                    }else{
                        cell.nameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeAr!)"
                    }
                    cell.nameLbl.font = UIFont(name: "Avenir Book", size: 14)
                }
                
                if indexPath.row == selectSizeIndex{
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                }else{
                    cell.nameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                cell.nameLbl.layer.cornerRadius = cell.nameLbl.frame.height/2
                cell.nameLbl.clipsToBounds = true
                
                cell.selectionStyle = .none
                return cell
            }else{
                //MARK: Size TableView Details
                sizeTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!)
                let cell = sizeTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesVCSizeTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesVCSizeTVCell
                
                if indexPath.row == 0{
                    cell.sizeNameLbl.text = "   \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Size", value: "", table: nil))!)"
                    cell.sizePriceLbl.text = "   \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!)"
                    cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizeNameLbl.font = UIFont(name: "Avenir Medium", size: 16)
                    cell.sizePriceLbl.font = UIFont(name: "Avenir Medium", size: 16)
                    cell.sizePriceLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.sizeNameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeEn!)"
                    }else{
                        cell.sizeNameLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].SizeAr!)"
                    }
                    if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 0{
                        cell.sizePriceLbl.text = "   \(additionalsArray[0].Prices![indexPath.row - 1].Price!.withCommas())"
                        cell.sizePriceLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                    }else{
                        cell.sizePriceLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Out of Stock", value: "", table: nil))!
                        cell.sizePriceLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                    }
                    cell.sizeNameLbl.font = UIFont(name: "Avenir Book", size: 14)
                    cell.sizePriceLbl.font = UIFont(name: "Avenir Book", size: 14)
                }
                
                if indexPath.row == selectSizeIndex{
                    if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 1{
                        if selectSizeIndex <= additionalsArray[0].Prices!.count{
                            selectSizeIndex = selectSizeIndex + 1
                            sizeTableView.reloadData()
                        }else{
                            selectSizeIndex = 0
                            cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        }
                    }else{
                        cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.862745098, blue: 0.8980392157, alpha: 1)
                        cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                    }
                }else{
                    cell.sizeNameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.sizePriceLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                sizeTableViewHeight.constant = sizeTableView.contentSize.height
                
                cell.selectionStyle = .none
                return cell
            }
        }else if tableView == moreCoffeeTypeTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Grinders Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                
                if grinderDetailsArray[indexPath.section].MinSelection! > 0{
                    
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(grinderDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Grinders Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, for: indexPath) as! GrindingMethodTVCell
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if grinderDetailsArray[indexPath.section].MaxSelection! > 1{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                subCell.grindImg.kf.setImage(with: url)
                
                subCell.grindImg.layer.cornerRadius = subCell.grindImg.frame.width/2
                subCell.grindImg.clipsToBounds = true
                
                cell = subCell
            }
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.moreCoffeeTypeTableView.layoutIfNeeded()
                self.moreCoffeeTypeTableViewHeight.constant = self.moreCoffeeTypeTableView.contentSize.height
                self.moreCoffeeTypeViewHeight.constant = self.moreCoffeeTypeTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
            
        }else if tableView == moreCoffeeTypeNotRecommendTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                //MARK: Grinders Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "BeveragesHeaderTVCell", value: "", table: nil))!, for: indexPath) as! BeveragesHeaderTVCell
                
                if grinderDetailsArray[indexPath.section].MinSelection! > 0{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameEn!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }else{
                        headerCell.nameLbl.text = "\(grinderDetailsArray[indexPath.section].NameAr!) (\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "min", value: "", table: nil))!)-\(grinderDetailsArray[indexPath.section].MinSelection!))"
                    }
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameEn!
                    }else{
                        headerCell.nameLbl.text = grinderDetailsArray[indexPath.section].NameAr!
                    }
                }
                headerCell.dropDownBtn.tag = indexPath.section
                headerCell.dropDownBtn.addTarget(self, action: #selector(grinderNotRecommendDropDownBtn_Tapped(_:)), for: .touchUpInside)
                cell = headerCell
            }else{
                //MARK: Grinders Sub Cell Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!)
                let subCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "GrindingMethodTVCell", value: "", table: nil))!, for: indexPath) as! GrindingMethodTVCell
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameEn
                }else{
                    subCell.nameLbl.text = grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].NameAr
                }
                
                if grinderDetailsArray[indexPath.section].MaxSelection! > 1{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "check_box_unselected")
                    }else{
                        subCell.selectImg.image = UIImage(named: "check_box_selected")
                    }
                }else{
                    if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect! == false{
                        subCell.selectImg.image = UIImage(named: "Oval Copy 2")
                    }else{
                        subCell.selectImg.image = UIImage(named: "Oval Copy")
                    }
                }
                
                //Image
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].Image)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                subCell.grindImg.kf.setImage(with: url)
                
                subCell.grindImg.layer.cornerRadius = subCell.grindImg.frame.width/2
                subCell.grindImg.clipsToBounds = true
                
                cell = subCell
            }
            DispatchQueue.main.async {
                self.myScrollView.layoutIfNeeded()
                self.moreCoffeeTypeNotRecommendTableView.layoutIfNeeded()
                self.moreCoffeeTypeNotRecommendTableViewHeight.constant = self.moreCoffeeTypeNotRecommendTableView.contentSize.height
                self.moreCoffeeTypeNotRecommendViewHeight.constant = self.moreCoffeeTypeNotRecommendTableView.contentSize.height + 21
            }
            
            cell.selectionStyle = .none
            return cell
            
        }else if tableView == additionalsTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                 //MARK: Items Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!, for: indexPath) as! AdditionalTypeHeaderTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    headerCell.titleNameLbl.text = latteDetailsArray[indexPath.section].NameEn!
                }else{
                    headerCell.titleNameLbl.text = latteDetailsArray[indexPath.section].NameAr!
                }
                
                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.additionalsTableView.layoutIfNeeded()
                    self.additionalsTableViewHeight.constant = self.additionalsTableView.contentSize.height
                }
                
                cell = headerCell
            }else{
                tableView.register(UINib(nibName: "AdditionalTypeTVCell", bundle: nil), forCellReuseIdentifier: "AdditionalTypeTVCell")
                let subCell = tableView.dequeueReusableCell(withIdentifier: "AdditionalTypeTVCell", for: indexPath) as! AdditionalTypeTVCell
                
                let rowArray = latteDetailsArray[indexPath.section].Additionals!
                subCell.selectSizeId = selectSizeId
                subCell.isRecommend = true
                subCell.section = indexPath.section
                subCell.updateCellWith(row: rowArray)
                subCell.additionalTypeTVCellDelegate = self
                
                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.additionalsTableView.layoutIfNeeded()
                    self.additionalsTableViewHeight.constant = self.additionalsTableView.contentSize.height
                }
                
                return subCell
            }

            cell.selectionStyle = .none
            return cell
        }else if tableView == additionalsNotRecommendTableView{
            var cell = UITableViewCell()
            if indexPath.row == 0{
                 //MARK: Items Header Details
                tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!)
                let headerCell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AdditionalTypeHeaderTVCell", value: "", table: nil))!, for: indexPath) as! AdditionalTypeHeaderTVCell
                if AppDelegate.getDelegate().appLanguage == "English"{
                    headerCell.titleNameLbl.text = latteNotRecommendDetailsArray[indexPath.section].NameEn!
                }else{
                    headerCell.titleNameLbl.text = latteNotRecommendDetailsArray[indexPath.section].NameAr!
                }
                
                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.additionalsNotRecommendTableView.layoutIfNeeded()
                    self.additionalsNotRecommendTableViewHeight.constant = self.additionalsNotRecommendTableView.contentSize.height
                }
                
                cell = headerCell
            }else{
                tableView.register(UINib(nibName: "AdditionalTypeTVCell", bundle: nil), forCellReuseIdentifier: "AdditionalTypeTVCell")
                let subCell = tableView.dequeueReusableCell(withIdentifier: "AdditionalTypeTVCell", for: indexPath) as! AdditionalTypeTVCell
                
                let rowArray = latteNotRecommendDetailsArray[indexPath.section].Additionals!
                subCell.selectSizeId = selectSizeId
                subCell.isRecommend = false
                subCell.section = indexPath.section
                subCell.updateCellWith(row: rowArray)
                subCell.additionalTypeTVCellDelegate = self
                
                DispatchQueue.main.async {
                    self.myScrollView.layoutIfNeeded()
                    self.additionalsNotRecommendTableView.layoutIfNeeded()
                    self.additionalsNotRecommendTableViewHeight.constant = self.additionalsNotRecommendTableView.contentSize.height
                }
                
                return subCell
            }
            
            cell.selectionStyle = .none
            return cell
        }else{
            //MARK: Recommended TableView Details
            RecommendedTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!)
            let cell = RecommendedTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ItemsTVCell", value: "", table: nil))!, for: indexPath) as! ItemsTVCell

            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
                cell.itemDiscLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].DescEn
            }else{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
                cell.itemDiscLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].DescAr
            }

            if additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories! == "0" || additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories! == "0.0"{
                cell.itemCalLbl.text = ""
                cell.itemCalLbl.isHidden = true
            }else{
                cell.itemCalLbl.text = "\(additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                cell.itemCalLbl.isHidden = false
            }
            
            cell.itemPriceLbl.text = "\(additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Price!.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"

            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(addBtnTapped(_:)), for: .touchUpInside)

            cell.itemQuantityLbl.isHidden = true
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].SuggestiveItems![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)

            myScrollView.layoutIfNeeded()
            RecommendedTableView.layoutIfNeeded()
            recommendedViewHeight.constant = 305
            //RecommendedTableViewHeight.constant = RecommendedTableView.contentSize.height + 10
            RecommendedTableViewHeight.constant = 0

            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == coffeeTypeNotRecommendTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.additionalItemsArray[indexPath.section].sectionIsExpand = !additionalItemsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                        self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                    }else{
                        self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = additionalItemsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = additionalItemsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(additionalItemsArray[indexPath.section].MaxSelection!)")
                            }
                        }else{
                            if self.additionalItemsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.additionalItemsArray[indexPath.section].Additionals!.count-1{
                                    self.additionalItemsArray[indexPath.section].Additionals![i].ItemSelect = false
                                    self.additionalItemsArray[indexPath.section].Additionals![i].additionalQty! = 0
                                }
                                self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    self.additionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0

                }
            }
            self.priceCalculation()
        }else if tableView == topCoffeeTypeTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.topMinSelectAdditionalItemsArray[indexPath.section].sectionIsExpand = !topMinSelectAdditionalItemsArray[indexPath.section].sectionIsExpand!
                
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                        self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                    }else{
                        self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = topMinSelectAdditionalItemsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = topMinSelectAdditionalItemsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(topMinSelectAdditionalItemsArray[indexPath.section].MaxSelection!)")
                            }
                        }else{
                            if self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals!.count-1{
                                    self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![i].ItemSelect = false
                                    self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![i].additionalQty = 0
                                }
                                self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    self.topMinSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                }
            }
            self.priceCalculation()
        }else if tableView == coffeeTypeTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.minSelectAdditionalItemsArray[indexPath.section].sectionIsExpand = !minSelectAdditionalItemsArray[indexPath.section].sectionIsExpand!
                
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                        self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                    }else{
                        self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = minSelectAdditionalItemsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = minSelectAdditionalItemsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath.section].MaxSelection!)")
                            }
                        }else{
                            if self.minSelectAdditionalItemsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.minSelectAdditionalItemsArray[indexPath.section].Additionals!.count-1{
                                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    self.minSelectAdditionalItemsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                }
            }
            self.priceCalculation()
        }else if tableView == moreCoffeeTypeTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.grinderDetailsArray[indexPath.section].sectionIsExpand = !grinderDetailsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = grinderDetailsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = grinderDetailsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                                tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                                self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!)")
                            }
                        }else{
                            if self.grinderDetailsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.grinderDetailsArray[indexPath.section].Additionals!.count-1{
                                    self.grinderDetailsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                }
            }
            self.priceCalculation()
        }else if tableView == moreCoffeeTypeNotRecommendTableView{
            if indexPath.row == 0 {
                //MARK: Coffee Type Expand
                self.grinderDetailsArray[indexPath.section].sectionIsExpand = !grinderDetailsArray[indexPath.section].sectionIsExpand!
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else{
                //MARK: Coffee Type Selection
                if grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].IsOutOfStock! == 0{
                    if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == true{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                    }else{
                        self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = !grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect!
                        let maxSelect = grinderDetailsArray[indexPath.section].MaxSelection!
                        let maxSelectArray = grinderDetailsArray[indexPath.section].Additionals!.filter({$0.ItemSelect! == true})
                        if maxSelect > 1{
                            if maxSelect >= maxSelectArray.count{
                              tableView.reloadSections([indexPath.section], with: .automatic)
                            }else{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum Additional selection is", value: "", table: nil))!) \(grinderDetailsArray[indexPath.section].MaxSelection!)")
                            }
                        }else{
                            if self.grinderDetailsArray[indexPath.section].Additionals!.count > 1{
                                for i in 0...self.grinderDetailsArray[indexPath.section].Additionals!.count-1{
                                    self.grinderDetailsArray[indexPath.section].Additionals![i].ItemSelect = false
                                }
                                self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = true
                            }else{
                                print("")
                            }
                        }
                        if self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect == false{
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 0
                        }else{
                            
                            self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].additionalQty! = 1
                        }
                    }
                    tableView.reloadSections([indexPath.section], with: .automatic)
                }else{
                    self.grinderDetailsArray[indexPath.section].Additionals![indexPath.row-1].ItemSelect = false
                }
            }
            self.priceCalculation()
        }else if tableView == sizeTableView{//MARK: Size Selection
            if indexPath.row != 0{
                if additionalsArray[0].Prices![indexPath.row - 1].IsOutOfStock! == 1{
                    print("Out Of Stock")
                }else{
                    selectSizeIndex = indexPath.row
                    selectSizeId = additionalsArray[0].Prices![indexPath.row-1].SizeId
                    selectSizePrice = additionalsArray[0].Prices![indexPath.row-1].Price
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        sizeDescLbl.text = additionalsArray[0].Prices![indexPath.row-1].DescEn
                        sizeNameLbl.text = additionalsArray[0].Prices![indexPath.row-1].SizeEn
                    }else{
                        sizeDescLbl.text = additionalsArray[0].Prices![indexPath.row-1].DescAr
                        sizeNameLbl.text = additionalsArray[0].Prices![indexPath.row-1].SizeAr
                    }
                    if additionalsArray[0].RewardEligibility != nil{
                        if additionalsArray[0].Prices![indexPath.row-1].Calories! != "" || additionalsArray[0].RewardEligibility! == true{
                            self.calMainViewHeight.constant = 50
                            if additionalsArray[0].Prices![indexPath.row-1].Calories! != ""{
                                calLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                                calView.isHidden = false
                            }else{
                                calView.isHidden = true
                            }
                        }else{
                            self.calMainViewHeight.constant = 0
                        }
                    }else{
                        pointsView.isHidden = true
                        if additionalsArray[0].Prices![indexPath.row-1].Calories! != ""{
                            calLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].Calories!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cal", value: "", table: nil))!)"
                            calView.isHidden = false
                            self.calMainViewHeight.constant = 50
                        }else{
                            calView.isHidden = true
                            self.calMainViewHeight.constant = 0
                        }
                    }
                    
                    if additionalsArray[0].Prices![indexPath.row-1].Weight! > 0.0{
                        sizeWtLbl.text = "WT : \(additionalsArray[0].Prices![indexPath.row-1].Weight!.withCommas()) \(additionalsArray[0].Prices![indexPath.row-1].WeightUOM!)"
                        sizeWtLblHeight.constant = 21
                        sizeWtLblTopConstraint.constant = 5
                    }else{
                        sizeWtLbl.text = ""
                        sizeWtLblHeight.constant = 0
                        sizeWtLblTopConstraint.constant = 0
                    }
                    var UOM = additionalsArray[0].Prices![indexPath.row-1].UOM!
                    if UOM == "Whole"{
                        UOM = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Whole", value: "", table: nil))!
                    }
                    nutritionDetailsArray = [["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calcium", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Calcium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Iron", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Iron!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminC", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].VitaminC!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "VitaminA", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].VitaminA!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Protein", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Protein!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sugars", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Sugars!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "DietaryFibers", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].DietaryFibers!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Carbohydrates", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Carbohydrates!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sodium", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Sodium!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cholestrol", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Cholestrol!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TransFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].TransFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "SaturatedFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].SaturatedFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "UOM", value: "", table: nil))!,"Calories":UOM],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "CaloriesFromFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].CaloriesFromFat!],["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Calories", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].Calories!], ["Name":(AppDelegate.getDelegate().filePath?.localizedString(forKey: "TotalFat", value: "", table: nil))!,"Calories":additionalsArray[0].Prices![indexPath.row-1].TotalFat!]]
                    
                    //Item Size Image
                    let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].Prices![indexPath.row-1].Image!)" as NSString
                    let charSet = CharacterSet.urlFragmentAllowed
                    let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                    let url = URL.init(string: urlStr as String)
                    sizeImage.kf.setImage(with: url)
                    
                    selectNutritionCalLbl.text = "\(additionalsArray[0].Prices![indexPath.row-1].TotalFat!) KL"
                    self.priceCalculation()
                    self.sizeTableView.reloadData()
                    self.selectBeanCV.reloadData()
                    self.coffeeTypeTableView.reloadData()
                    self.coffeeTypeNotRecommendTableView.reloadData()
                    self.topCoffeeTypeTableView.reloadData()
                    if latteDetailsArray.count > 0{
                        self.additionalsTableView.reloadData()
                    }
                    if latteNotRecommendDetailsArray.count > 0{
                        self.additionalsNotRecommendTableView.reloadData()
                    }
                }
            }else{
                print(indexPath.row)
            }
        }else if tableView == RecommendedTableView{ //MARK:Recomended TableView
            itemID = additionalsArray[0].SuggestiveItems![indexPath.row].Id
            storeId = storeId!
            if AppDelegate.getDelegate().appLanguage == "English"{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
            }else{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
            }
            self.successfullyAddedMsgViewHeight.constant = 0
            self.noteViewHeight.constant = 111
            self.quantity = 1
            self.quantityLbl.text = "\(quantity)"
            self.noThanksView.isHidden = true
            self.addMoreBtnView.isHidden = true
            self.addBtnView.isHidden = false
            self.coffeeTypeLineLbl.isHidden = false
            self.getStoreAdditionalsService()
        } else if tableView == additionalsTableView || tableView == additionalsNotRecommendTableView{
            print("Selected")
        }else{
            print(indexPath.row)
        }
    }
    @objc func grinderDropDownBtn_Tapped(_ sender: UIButton){
        self.grinderDetailsArray[sender.tag].sectionIsExpand = !grinderDetailsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        moreCoffeeTypeTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func grinderNotRecommendDropDownBtn_Tapped(_ sender: UIButton){
        self.grinderDetailsArray[sender.tag].sectionIsExpand = !grinderDetailsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        moreCoffeeTypeNotRecommendTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func dropDownBtn_Tapped(_ sender: UIButton){
        self.additionalItemsArray[sender.tag].sectionIsExpand = !additionalItemsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        coffeeTypeNotRecommendTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func minDropDownBtn_Tapped(_ sender: UIButton){
        self.minSelectAdditionalItemsArray[sender.tag].sectionIsExpand = !minSelectAdditionalItemsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        coffeeTypeTableView.reloadSections([sender.tag], with: .automatic)
    }
    @objc func topMinDropDownBtn_Tapped(_ sender: UIButton){
        self.topMinSelectAdditionalItemsArray[sender.tag].sectionIsExpand = !topMinSelectAdditionalItemsArray[sender.tag].sectionIsExpand!
        self.priceCalculation()
        topCoffeeTypeTableView.reloadSections([sender.tag], with: .automatic)
    }
    //MARK: Coffee Type cell Button Action
    func additionalButtonsTapped(cell: BeveragesListTVCell, plus: Bool, minus: Bool, tableView: UITableView) {
        if tableView == coffeeTypeNotRecommendTableView{
            let indexPath = self.coffeeTypeNotRecommendTableView.indexPath(for: cell)
            if plus == true{
                if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty > additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty!{
                        additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! + 1
                        priceCalculation()
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty)")
                    }
                }else{
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeNotRecommendTableView.reloadData()
            }else if minus == true{
                if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! == 1{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                    }else{
                        additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! - 1
                        priceCalculation()
                    }
                }else{
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    additionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeNotRecommendTableView.reloadData()
            }else{
                print(indexPath!.section)
                print(indexPath!.row)
            }
        }else if tableView == topCoffeeTypeTableView{
            let indexPath = self.topCoffeeTypeTableView.indexPath(for: cell)
            if plus == true{
                if topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty > topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty!{
                        topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! + 1
                        priceCalculation()
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty)")
                    }
                }else{
                    topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                topCoffeeTypeTableView.reloadData()
            }else if minus == true{
                if topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! == 1{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                    }else{
                        topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! - 1
                        priceCalculation()
                    }
                }else{
                    topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    topMinSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                topCoffeeTypeTableView.reloadData()
            }else{
                print(indexPath!.section)
                print(indexPath!.row)
            }
        }else{
            let indexPath = self.coffeeTypeTableView.indexPath(for: cell)
            if plus == true{
                if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty > minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty!{
                        minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! + 1
                        priceCalculation()
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].MaxQty)")
                    }
                }else{
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeTableView.reloadData()
            }else if minus == true{
                if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! == true{
                    if minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! == 1{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                    }else{
                        minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! - 1
                        priceCalculation()
                    }
                }else{
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].ItemSelect! = true
                    minSelectAdditionalItemsArray[indexPath!.section].Additionals![indexPath!.row-1].additionalQty! = 1
                }
                coffeeTypeTableView.reloadData()
            }else{
                print(indexPath!.section)
                print(indexPath!.row)
            }
        }
    }
    //MARK: Recommended cell Button Actions
    @objc func addBtnTapped(_ sender: UIButton){
        itemID = additionalsArray[0].SuggestiveItems![sender.tag].Id
        storeId = storeId!
        if AppDelegate.getDelegate().appLanguage == "English"{
            navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![sender.tag].NameEn
        }else{
            navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![sender.tag].NameAr
        }
        self.successfullyAddedMsgViewHeight.constant = 0
        self.noteViewHeight.constant = 111
        self.quantity = 1
        self.quantityLbl.text = "\(quantity)"
        self.noThanksView.isHidden = true
        self.addMoreBtnView.isHidden = true
        self.addBtnView.isHidden = false
        self.coffeeTypeLineLbl.isHidden = false
        self.getStoreAdditionalsService()
    }
}
@available(iOS 13.0, *)
extension AdditionalsVC:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == selectBeanCV{
            if additionalsArray.count > 0{
                return additionalsArray[0].Grinders!.count
            }else{
                return 0
            }
        }else if collectionView == bannerCollectionView{
            return itemImagesArray.count
        }else if collectionView == tagsCollectionView{
            if additionalsArray.count > 0{
                if additionalsArray[0].Tags != nil{
                    return additionalsArray[0].Tags!.count
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else if collectionView == recommendedCV{
            if additionalsArray.count > 0{
                return additionalsArray[0].SuggestiveItems!.count
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == selectBeanCV{
            selectBeanCV.register(UINib(nibName: "RecommendedBeansCVCell", bundle: nil), forCellWithReuseIdentifier: "RecommendedBeansCVCell")
            let cell = selectBeanCV.dequeueReusableCell(withReuseIdentifier: "RecommendedBeansCVCell", for: indexPath) as! RecommendedBeansCVCell
            
            //Top Recommended Image Functionality
            if additionalsArray[0].Grinders![indexPath.row].Recommended == true{
                cell.recommendedImg.isHidden = false
            }else{
                cell.recommendedImg.isHidden = true
            }
            let recommend = additionalsArray[0].Grinders!.filter({$0.Recommended == true})
            if recommend.count > 0{
                cell.recommendedImgHeight.constant = 15
            }else{
                cell.recommendedImgHeight.constant = 0
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameEn
            }else{
                cell.nameLbl.text = additionalsArray[0].Grinders![indexPath.row].NameAr
            }
            
            if additionalsArray[0].ManualBrew != nil{
                if additionalsArray[0].ManualBrew! == true{
                    cell.priceLblHeight.constant = 21
                    cell.priceBottomConstraint.constant = 5
                    cell.priceLbl.isHidden = false
                    if additionalsArray[0].Grinders![indexPath.row].Price > 0{
                        let price = additionalsArray[0].Grinders![indexPath.row].Price + selectSizePrice
                        cell.priceLbl.text = "\(price.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    }else{
                        cell.priceLbl.text = "\(selectSizePrice.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
                    }
                }else{
                    cell.priceLbl.isHidden = true
                    cell.priceLblHeight.constant = 0
                    cell.priceBottomConstraint.constant = 0
                }
            }else{
                cell.priceLbl.isHidden = true
                cell.priceLblHeight.constant = 0
                cell.priceBottomConstraint.constant = 0
            }
                        
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].Grinders![indexPath.row].Icon)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.beanTypeImg.kf.setImage(with: url)
            
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(beanInfoBtn_Tapped(_:)), for: .touchUpInside)
            
            if additionalsArray[0].Grinders![indexPath.row].selectGrinder == true{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.937254902, blue: 0.9725490196, alpha: 1)
                cell.BGView.shadowColor1 = .lightGray
                cell.BGView.shadowOpacity1 = 0.6
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.BGView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
                cell.BGView.shadowOpacity1 = 0
                cell.BGView.shadowOpacity = 0
            }
            
            return cell
            
        }else if collectionView == bannerCollectionView{
            bannerCollectionView.register(UINib(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
            let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
            let name = itemImagesArray[indexPath.row].Image
            cell.img.image = UIImage(named: "")
            let fullNameArr = name.split(separator:".")
            if fullNameArr.count > 1 {
            if fullNameArr[1] == "mp4" {
                cell.playBtn.tag = indexPath.row
                cell.playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
                cell.playBtn.isHidden = false
                cell.playerView.isHidden = false
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                let avPlayer = AVPlayer(url: url! as URL);
                cell.playerView!.playerLayer.player = avPlayer;
                cell.contentView.bringSubviewToFront(cell.playBtn)
            }else{
                cell.playBtn.isHidden = true
                cell.playerView.isHidden = true
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            }else{
                cell.playBtn.isHidden = true
                cell.playerView.isHidden = true
                let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(name)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let url = URL.init(string: urlStr as String)
                cell.img.kf.setImage(with: url)
            }
            
            return cell
        }else if collectionView == tagsCollectionView{
            tagsCollectionView.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
            let cell = tagsCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
            cell.cellBGImg.contentMode = .scaleAspectFit
            cell.cellBGImg.clipsToBounds = true
            //Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(additionalsArray[0].Tags![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.cellBGImg.kf.setImage(with: url)
            
            return cell
        }else{
            recommendedCV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!)
            let cell = recommendedCV.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmationVCCVCell", value: "", table: nil))!, for: indexPath) as! ConfirmationVCCVCell
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
            }else{
                cell.itemNameLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
            }
            if  additionalsArray[0].SuggestiveItems![indexPath.row].Prices != nil{
                if additionalsArray[0].SuggestiveItems![indexPath.row].Prices!.count > 0{
                    cell.itemPriceLbl.text = "\( additionalsArray[0].SuggestiveItems![indexPath.row].Prices![0].Price!.withCommas())"
                }else{
                    cell.itemPriceLbl.text = "0.00"
                }
            }else{
                cell.itemPriceLbl.text = "0.00"
            }
            
            //Item Image
            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\( additionalsArray[0].SuggestiveItems![indexPath.row].Image)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            cell.itemImg.kf.setImage(with: url)
            
            cell.sugestiveItemAddBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add", value: "", table: nil))!, for: .normal)
            cell.sugestiveItemAddBtn.isHidden = false
            cell.addBtnView.isHidden = true
            cell.sugestiveItemAddBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            
            cell.sugestiveItemAddBtn.tag = indexPath.row
            cell.sugestiveItemAddBtn.addTarget(self, action: #selector(addBtnTapped(_:)), for: .touchUpInside)
            
            cell.addBtnView.backgroundColor = .white
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bannerCollectionView{
          let name = itemImagesArray[indexPath.row].Image
           let fullNameArr = name.split(separator:".")
           if fullNameArr.count > 1 {
           if fullNameArr[1] == "mp4" {
               let indexPath = NSIndexPath(row: indexPath.row, section: 0)
               let cell = bannerCollectionView.cellForItem(at: indexPath as IndexPath) as! ImagesCell?
               if cell!.playBtn.isHidden == true {
                   cell!.playBtn.isHidden = false
                   cell!.playerView!.player!.pause()
                   cell!.contentView.bringSubviewToFront(cell!.playBtn)
               }
           }
           }
               let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
               let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpVC") as! ImagePopUpVC
               //obj.modalPresentationStyle = .overCurrentContext
               if AppDelegate.getDelegate().appLanguage == "English"{
                   obj.itemName = additionalsArray[0].NameEn
               }else{
                   obj.itemName = additionalsArray[0].NameAr
               }
               obj.itemImagesArray = itemImagesArray
               //obj.modalPresentationStyle = .overFullScreen
               present(obj, animated: false, completion: nil)
           //}
        }else if collectionView == tagsCollectionView{
            print("")
        }else if collectionView == selectBeanCV{
            if additionalsArray[0].Grinders!.count > 1{
                for i in 0...additionalsArray[0].Grinders!.count-1{
                    self.additionalsArray[0].Grinders![i].selectGrinder = false
                }
            }
            grinderItemId = additionalsArray[0].Grinders![indexPath.row].Id
            self.additionalsArray[0].Grinders![indexPath.row].selectGrinder = !additionalsArray[0].Grinders![indexPath.row].selectGrinder!
            selectBeanCV.reloadData()
            if additionalsArray[0].ManualBrew != nil{
                if additionalsArray[0].ManualBrew! == true{
                    if additionalsArray[0].Grinders![indexPath.row].Price > 0{
                        priceCalculation()
                    }
                }
            }
        }else if collectionView == recommendedCV{
            itemID = additionalsArray[0].SuggestiveItems![indexPath.row].Id
            storeId = storeId!
            if AppDelegate.getDelegate().appLanguage == "English"{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameEn
            }else{
                navigationBarTitleLbl.text = additionalsArray[0].SuggestiveItems![indexPath.row].NameAr
            }
            self.successfullyAddedMsgViewHeight.constant = 0
            self.noteViewHeight.constant = 111
            self.quantity = 1
            self.quantityLbl.text = "\(quantity)"
            self.noThanksView.isHidden = true
            self.addMoreBtnView.isHidden = true
            self.addBtnView.isHidden = false
            self.coffeeTypeLineLbl.isHidden = false
            self.getStoreAdditionalsService()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == selectBeanCV{
            let CVWidth = selectBeanCV.frame.width
            var width = 150.0
            //if CVWidth > 330{
                //width = Double((CVWidth/4)-5)
            //}else{
                width = Double((CVWidth/3)-10)
            //}
            //if view.frame.width <= 375{
                if additionalsArray[0].ManualBrew! == true{
                    return CGSize(width: width, height: 223)
                }else{
                    return CGSize(width: width, height: 198)
                }
//            }else{
//                if additionalsArray[0].ManualBrew! == true{
//                    return CGSize(width: width, height: 220)
//                }else{
//                    return CGSize(width: width, height: 195)
//                }
//            }
        }else if collectionView == bannerCollectionView{
            print("Width:\(bannerCollectionView.frame.width)")
              return CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
        }else if collectionView == tagsCollectionView{
            return CGSize(width: 40, height: tagsCollectionView.frame.height)
        }else{
            let screenSize: CGRect = self.recommendedCV.bounds
            let screenWidth = screenSize.width
            return CGSize(width: screenWidth/2 + 50, height: 230)
        }
    }
    @objc func beanSelectBtn_Tapped(_ sender:UIButton){
        if additionalsArray[0].Grinders!.count > 1{
            for i in 0...additionalsArray[0].Grinders!.count-1{
                self.additionalsArray[0].Grinders![i].selectGrinder = false
            }
        }
        grinderItemId = additionalsArray[0].Grinders![sender.tag].Id
        self.additionalsArray[0].Grinders![sender.tag].selectGrinder = !additionalsArray[0].Grinders![sender.tag].selectGrinder!
        selectBeanCV.reloadData()
        if additionalsArray[0].ManualBrew != nil{
            if additionalsArray[0].ManualBrew! == true{
                if additionalsArray[0].Grinders![sender.tag].Price > 0{
                    priceCalculation()
                }
            }
        }
    }
    @objc func beanInfoBtn_Tapped(_ sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "ImagePopUpTwoVC") as! ImagePopUpTwoVC
        obj.modalPresentationStyle = .overCurrentContext
        if additionalsArray[0].Grinders!.count > 0{
            if additionalsArray[0].Grinders![sender.tag].Image != ""{
                obj.imageName = additionalsArray[0].Grinders![sender.tag].Image
            }else{
                obj.imageName = ""
            }
        }else{
            obj.imageName = ""
        }
        
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
    @objc func playBtnTapped(_ sender: UIButton){
        let name = itemImagesArray[sender.tag].Image
         let fullNameArr = name.split(separator:".")
         if fullNameArr.count > 1 {
         if fullNameArr[1] == "mp4" {
             let indexPath = NSIndexPath(row: sender.tag, section: 0)
             let cell = bannerCollectionView.cellForItem(at: indexPath as IndexPath) as! ImagesCell?
             if cell!.playBtn.isHidden == true {
                 cell!.playBtn.isHidden = false
                 cell!.playerView!.player!.pause()
                 cell!.contentView.bringSubviewToFront(cell!.playBtn)
             }else{
                 cell!.playBtn.isHidden = true
                 cell?.playerView!.player!.play()
                 cell!.contentView.bringSubviewToFront(cell!.playBtn)
             }
         }
      }
    }
    //Banner auto scrool
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == bannerCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.bannerCollectionView.indexPathsForVisibleItems
            indexes.sort()
            var index = indexes.first!
            let name = itemImagesArray[index.row].Image
             let fullNameArr = name.split(separator:".")
             if fullNameArr.count > 1 {
             if fullNameArr[1] == "mp4" {
                let cell = bannerCollectionView.cellForItem(at: index as IndexPath) as! ImagesCell?
                 if cell!.playBtn.isHidden == true {
                     cell!.playBtn.isHidden = false
                     cell!.playerView!.player!.pause()
                    cell!.contentView.bringSubviewToFront(cell!.playBtn)

                 }
             }
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row - 1
                }else{
                    index.row = index.row + 1
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
            }
            
            if itemImagesArray.count > index.row {
                DispatchQueue.main.async {
                    self.bannerCollectionView.scrollToItem(at: index, at: .right, animated: true )
                }
            }
            DispatchQueue.main.async {
                self.bannerPageController.currentPage = index.row
            }
        }
    }
    }
}
@available(iOS 13.0, *)
extension AdditionalsVC:DropVCDelegate{
    func didTapAction(ID:String,name: String, whereType: Int) {
        self.selectNutrition = name
    }
}
//MARK: TextView Delegate
@available(iOS 13.0, *)
extension AdditionalsVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!) :"
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>~`/:;?-=\\¥'£•¢")
        if (textView.textInputMode?.primaryLanguage == "emoji") || text.rangeOfCharacter(from: validString) != nil{
            return false
        }
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
@available(iOS 13.0, *)
extension AdditionalsVC : AdditionalTypeTVCellDelegate {
    func collectionView(collectionviewcell: AdditionalLatteCVCell?, index: Int, didTappedInTableViewCell: AdditionalTypeTVCell, btnName: String, isRecommend: Bool, section: Int) {
        if let selectRow = didTappedInTableViewCell.itemsArray {
            print("You tapped the cell \(index) in the row of colors \(selectRow)")
            if isRecommend == true{
                if btnName == "Select"{
                    if latteDetailsArray[section].Additionals![index].ItemSelect! == true{
                        self.latteDetailsArray[section].Additionals![index].ItemSelect = !latteDetailsArray[section].Additionals![index].ItemSelect!
                        self.latteDetailsArray[section].Additionals![index].additionalQty! = 0
                        self.priceCalculation()
                        //additionalsTableView.reloadData()
                        let sectionIndex = IndexSet(integer: section)
                        additionalsTableView.reloadSections(sectionIndex, with: .none)
                    }else{
                        if self.latteDetailsArray[section].Additionals![index].IsOutOfStock! == 0{
                            if latteDetailsArray[section].Additionals!.count > 1{
                                for i in 0...latteDetailsArray[section].Additionals!.count-1{
                                    latteDetailsArray[section].Additionals![i].ItemSelect = false
                                    latteDetailsArray[section].Additionals![i].additionalQty! = 0
                                }
                            }
                            self.latteDetailsArray[section].Additionals![index].ItemSelect = !latteDetailsArray[section].Additionals![index].ItemSelect!
                            self.latteDetailsArray[section].Additionals![index].additionalQty! = 1
                            self.priceCalculation()
                            //additionalsTableView.reloadData()
                            let sectionIndex = IndexSet(integer: section)
                            additionalsTableView.reloadSections(sectionIndex, with: .none)
                        }else{
                            print("Item is Out Of Stock")
                        }
                    }
                }else if btnName == "Plus"{
                    if latteDetailsArray[section].Additionals![index].ItemSelect! == true{
                        if latteDetailsArray[section].Additionals![index].MaxQty > latteDetailsArray[section].Additionals![index].additionalQty!{
                            latteDetailsArray[section].Additionals![index].additionalQty! = latteDetailsArray[section].Additionals![index].additionalQty! + 1
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteDetailsArray[section].Additionals![index].MaxQty)")
                        }
                    }else{
                        if self.latteDetailsArray[section].Additionals![index].IsOutOfStock! == 0{
                            if latteDetailsArray[section].Additionals!.count > 1{
                                for i in 0...latteDetailsArray[section].Additionals!.count-1{
                                    latteDetailsArray[section].Additionals![i].ItemSelect = false
                                    latteDetailsArray[section].Additionals![i].additionalQty! = 0
                                }
                            }
                            self.latteDetailsArray[section].Additionals![index].ItemSelect = !latteDetailsArray[section].Additionals![index].ItemSelect!
                            self.latteDetailsArray[section].Additionals![index].additionalQty! = 1
                            self.priceCalculation()
                        }else{
                            print("Item is Out Of Stock")
                        }
                    }
                    //additionalsTableView.reloadData()
                    let sectionIndex = IndexSet(integer: section)
                    additionalsTableView.reloadSections(sectionIndex, with: .none)
                }else if btnName == "Minus"{
                    if latteDetailsArray[section].Additionals![index].ItemSelect! == true{
                        if latteDetailsArray[section].Additionals![index].additionalQty! == 1{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                        }else{
                            latteDetailsArray[section].Additionals![index].additionalQty! = latteDetailsArray[section].Additionals![index].additionalQty! - 1
                        }
                    }else{
                        var additionalName = ""
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additionalName = latteDetailsArray[section].Additionals![index].NameEn
                        }else{
                            additionalName = latteDetailsArray[section].Additionals![index].NameAr
                        }
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(additionalName)")
                    }
                    //additionalsTableView.reloadData()
                    let sectionIndex = IndexSet(integer: section)
                    additionalsTableView.reloadSections(sectionIndex, with: .none)
                }
            }else{
                if btnName == "Select"{
                    if latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect! == true{
                        self.latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect = !latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect!
                        self.latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! = 0
                        self.priceCalculation()
                        //additionalsNotRecommendTableView.reloadData()
                        let sectionIndex = IndexSet(integer: section)
                        additionalsNotRecommendTableView.reloadSections(sectionIndex, with: .none)
                    }else{
                        if self.latteNotRecommendDetailsArray[section].Additionals![index].IsOutOfStock! == 0{
                            if latteNotRecommendDetailsArray[section].Additionals!.count > 1{
                                for i in 0...latteNotRecommendDetailsArray[section].Additionals!.count-1{
                                    latteNotRecommendDetailsArray[section].Additionals![i].ItemSelect = false
                                    latteNotRecommendDetailsArray[section].Additionals![i].additionalQty! = 0
                                }
                            }
                            self.latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect = !latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect!
                            self.latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! = 1
                            self.priceCalculation()
                            //additionalsNotRecommendTableView.reloadData()
                            let sectionIndex = IndexSet(integer: section)
                            additionalsNotRecommendTableView.reloadSections(sectionIndex, with: .none)
                        }else{
                            print("Item is Out Of Stock")
                        }
                    }
                }else if btnName == "Plus"{
                    if latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect! == true{
                        if latteNotRecommendDetailsArray[section].Additionals![index].MaxQty > latteNotRecommendDetailsArray[section].Additionals![index].additionalQty!{
                            latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! = latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! + 1
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection is", value: "", table: nil))!) \(latteNotRecommendDetailsArray[section].Additionals![index].MaxQty)")
                        }
                    }else{
                        if self.latteNotRecommendDetailsArray[section].Additionals![index].IsOutOfStock! == 0{
                            if latteNotRecommendDetailsArray[section].Additionals!.count > 1{
                                for i in 0...latteNotRecommendDetailsArray[section].Additionals!.count-1{
                                    latteNotRecommendDetailsArray[section].Additionals![i].ItemSelect = false
                                    latteNotRecommendDetailsArray[section].Additionals![i].additionalQty! = 0
                                }
                            }
                            self.latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect = !latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect!
                            self.latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! = 1
                            self.priceCalculation()
                        }else{
                            print("Item is Out Of Stock")
                        }
                    }
                    //additionalsNotRecommendTableView.reloadData()
                    let sectionIndex = IndexSet(integer: section)
                    additionalsNotRecommendTableView.reloadSections(sectionIndex, with: .none)
                }else if btnName == "Minus"{
                    if latteNotRecommendDetailsArray[section].Additionals![index].ItemSelect! == true{
                        if latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! == 1{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum selection is 1", value: "", table: nil))!)
                        }else{
                            latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! = latteNotRecommendDetailsArray[section].Additionals![index].additionalQty! - 1
                        }
                    }else{
                        var additionalName = ""
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            additionalName = latteNotRecommendDetailsArray[section].Additionals![index].NameEn
                        }else{
                            additionalName = latteNotRecommendDetailsArray[section].Additionals![index].NameAr
                        }
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please select", value: "", table: nil))!) \(additionalName)")
                    }
                    //additionalsNotRecommendTableView.reloadData()
                    let sectionIndex = IndexSet(integer: section)
                    additionalsNotRecommendTableView.reloadSections(sectionIndex, with: .none)
                }
            }
        }
    }
}
