//
//  AddressSelectMapVC.swift
//  drCafe
//
//  Created by Devbox on 10/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

//MARK: Address DElegate Protocol
@available(iOS 13.0, *)
protocol AddressSelectMapVCDelegate {
    func selectLocation(address:String, latitude:Double, longitude:Double)
}

@available(iOS 13.0, *)
class AddressSelectMapVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {

    //MARK: Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var markerImg: UIImageView!
    
    var isNewUser:Bool!
    var Latitude:Double = 24.6815519
    var Longitude:Double = 46.6881631
    var changeLatitude:Double = 0.0
    var changeLongitude:Double = 0.0
    var locationManager = CLLocationManager()
    var addressSelectMapVCDelegate:AddressSelectMapVCDelegate!
    var whereObj = 0
    var isLocation = false
    var noLocationlatitude = 24.782765
    var noLocationlongitude = 46.767433
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            //doneBtn.backgroundColor = .white
            //doneBtn.setTitleColor(.black, for: .normal)
            //googleMapView.mapStyle = nil
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                addressLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.3215686275, blue: 0.368627451, alpha: 1)
            }else{
                addressLbl.textColor = .white
            }
        }else{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            doneBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            doneBtn.setTitleColor(.white, for: .normal)
        }
        doneBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, for: .normal)
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Done Button Action
    @IBAction func doneBtn_Tapped(_ sender: Any) {
        if whereObj == 2{
            self.addAddress()
        }else{
            addressSelectMapVCDelegate.selectLocation(address: addressLbl.text!, latitude: changeLatitude, longitude: changeLongitude)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func addAddress(){
        //My location
        let myLocation = CLLocation(latitude: Latitude, longitude: Longitude)
        //My buddy's location
        let myBuddysLocation = CLLocation(latitude: changeLatitude, longitude: changeLongitude)
        //Measuring my distance to my buddy's (in km)
        let distance = myLocation.distance(from: myBuddysLocation) / 1000
        if distance < 10.0{
            addressSelectMapVCDelegate.selectLocation(address: addressLbl.text!, latitude: changeLatitude, longitude: changeLongitude)
            self.navigationController?.popViewController(animated: true)
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location must be within 10 KM radius", value: "", table: nil))!)
        }
    }
    //MARK: Location Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
        initGoogleMaps()
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        self.changeLatitude = (location?.coordinate.latitude)!
        self.changeLongitude = (location?.coordinate.longitude)!
        isLocation = true
        self.locationManager.stopUpdatingLocation()
        initGoogleMaps()
    }
    func initGoogleMaps() {
        var camera:GMSCameraPosition!
        if isLocation == false{
            camera = GMSCameraPosition.camera(withLatitude:noLocationlatitude, longitude: noLocationlongitude, zoom: 5.0)
        }else{
            camera = GMSCameraPosition.camera(withLatitude:changeLatitude, longitude: changeLongitude, zoom: 18.0)
        }
        
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        self.googleMapView.camera = camera
        self.googleMapView.delegate = self
        self.googleMapView.isMyLocationEnabled = true
        self.googleMapView.settings.myLocationButton = true
        
//        if isLocation == true{
//            zoom = 15.0
//        }
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: changeLatitude, longitude: changeLongitude)
        marker.title = ""
        marker.map = mapView
        self.googleMapView.bringSubviewToFront(self.markerImg)
        marker.map = googleMapView
    }
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        var latitude: Double? = mapView.camera.target.latitude
        var longitude: Double? = mapView.camera.target.longitude
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(latitude!, longitude!), completionHandler: { response, error in
            if let aResults = response?.results() {
                print("\(aResults)")
            }
//            for addressObj: GMSAddress? in response?.results() ?? [GMSAddress?]() {
//                if let anObj = addressObj {
//                    print("lines=\((anObj.lines)!)")
//                    print("lines=\(anObj.coordinate.longitude)")
//                    latitude = anObj.coordinate.latitude
//                    longitude = anObj.coordinate.longitude
//                    self.changeLatitude = anObj.coordinate.latitude
//                    self.changeLongitude = anObj.coordinate.longitude
//                }
//                let addStr:[String] = (addressObj?.lines)!
//                var str=NSString()
//                for index in addStr {
//                    str=str.appending(index) as NSString
//                }
//                self.addressLbl.text = str as String;
//                return
//            }
            if let addressObj = response?.firstResult() {
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                latitude = addressObj.coordinate.latitude
                longitude = addressObj.coordinate.longitude
                self.changeLatitude = addressObj.coordinate.latitude
                self.changeLongitude = addressObj.coordinate.longitude
                
                self.addressLbl.text = "\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))"
                self.addressLbl.layoutIfNeeded()
                return
            }
        })
    }
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMapView.isMyLocationEnabled = true
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMapView.isMyLocationEnabled = true
        if (gesture) {
            mapView.selectedMarker = nil
        }
    }
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
        self.googleMapView.camera = camera
        self.dismiss(animated: true, completion: nil) // dismiss after select place
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil) // when cancel search
    }
}
