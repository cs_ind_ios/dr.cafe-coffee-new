//
//  SelectFeedbackTypeVC.swift
//  drCafe
//
//  Created by Devbox on 04/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import MessageUI

@available(iOS 13.0, *)
class SelectFeedbackTypeVC: UIViewController {

    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var shareYourExpNameLbl: UILabel!
    @IBOutlet weak var rateYourOrderNameLbl: UILabel!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var detailsArray = [CartDetailsModel]()
    static var instance: SelectFeedbackTypeVC!
    var isUserLogin = false
    
    var latitude = 0.0
    var longitude = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        SelectFeedbackTypeVC.instance = self
        self.tabBarController?.tabBar.isHidden = true
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationTitleLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationTitleLbl.textColor = .black
        }
        
        if detailsArray[0].ClosedOrder!.count > 0{
            if detailsArray[0].ClosedOrder![0].OrderType == "4"{
                if detailsArray[0].ClosedOrder![0].Address!.count > 0{
                    storeNameLbl.text = detailsArray[0].ClosedOrder![0].Address![0].Address
                    rateYourOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!
                }else{
                    rateYourOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your last order", value: "", table: nil))!
                    storeNameLbl.text = ""
                }
            }else{
                rateYourOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate your order", value: "", table: nil))!
                if AppDelegate.getDelegate().appLanguage == "English"{
                    storeNameLbl.text = detailsArray[0].ClosedOrder![0].StoreName.capitalized
                }else{
                    storeNameLbl.text = detailsArray[0].ClosedOrder![0].StoreNameAr.capitalized
                }
            }
        }else{
            storeNameLbl.text = ""
        }
        
        if isUserLogin == true{
            getDetailsService()
        }
    }
    //MARK: DashBoard Details Get Service
    func getDetailsService(){
        var originLat = String(format: "%.6f", latitude)
        var originLong = String(format: "%.6f", longitude)
        if latitude > 0 && longitude > 0{
            originLat = String(format: "%.6f", latitude)
            originLong = String(format: "%.6f", longitude)
        }else{
            originLat = ""
            originLong = ""
        }
        let appversion = AppUtility.methodForApplicationVersion().split(separator: "(")[0]
        let dic = ["UserId": UserDef.getUserId(),"AppVersion" : "\(appversion)","AppType": "IOS","DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "RequestBy":2, "IsSubscription":false, "Latitude":originLat,"Longitude":originLong, "action" : 0] as [String : Any]
        OrderModuleServices.DashBoardService(dic: dic, success: { (data) in
            if(data.Status == false){
                print("false")
            }else{
                DispatchQueue.main.async {
                    if data.Data!.CartDetails != nil{
                        if data.Data!.CartDetails!.ClosedOrder != nil{
                            self.detailsArray = [data.Data!.CartDetails!]
                        }
                    }
                    AppDelegate.getDelegate().cartQuantity = data.Data!.CartDetails!.CartItemsQuantity!
                    AppDelegate.getDelegate().subCartQuantity = data.Data!.CartDetails!.SubItemQty!
                    if data.Data!.CartDetails!.ImageBasePathUrl != nil{
                        if data.Data!.CartDetails!.ImageBasePathUrl! != ""{
                            AppDelegate.getDelegate().imagePathURL = data.Data!.CartDetails!.ImageBasePathUrl!
                        }else{
                            AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                        }
                    }else{
                        AppDelegate.getDelegate().imagePathURL = "\(displayImages.images.path())"
                    }
                    AppDelegate.getDelegate().cartAmount = data.Data!.CartDetails!.TotalPrice!
                    if data.Data!.CartDetails!.SubTotalPrice != nil{
                        AppDelegate.getDelegate().subCartAmount = data.Data!.CartDetails!.SubTotalPrice!
                    }
                    SaveAddressClass.themeColorArray = data.Data!.themes!
                    SaveAddressClass.cartDetailsArray = [data.Data!.CartDetails!]
                    AppDelegate.getDelegate().cartStoreId = data.Data!.CartDetails!.StoreId!
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameEn!
                    }else{
                        AppDelegate.getDelegate().cartStoreName = data.Data!.CartDetails!.StoreNameAr!
                    }
                    var themeColor = data.Data!.themes!.filter({$0.IsSelected == true})
                    if themeColor.count > 0{
                        AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                        AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                        AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                    }else{
                        themeColor = data.Data!.themes!.filter({$0.DefaultSelection == true})
                        if themeColor.count > 0{
                            AppDelegate.getDelegate().selectColor = themeColor[0].ForegroundColor!
                            AppDelegate.getDelegate().selectBGColor = themeColor[0].BackgroundColor!
                            AppDelegate.getDelegate().selectColorname = themeColor[0].ThemeName
                        }else{
                            AppDelegate.getDelegate().selectColor = "535353"
                            AppDelegate.getDelegate().selectBGColor = "D8D8D8"
                            AppDelegate.getDelegate().selectColorname = ""
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func shareYourExperienceBtn_Tapped(_ sender: Any) {
        let subject = "dr.CAFE Experience"
        let body = ""
        let coded = "mailto:info@dr-cafe.com?subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let emailURL:NSURL = NSURL(string: coded!){
            if UIApplication.shared.canOpenURL(emailURL as URL){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(emailURL as URL)
                } else {
                    UIApplication.shared.openURL(emailURL as URL)
                }
            }
        }
    }
    @IBAction func rateYourOrderBtn_Tapped(_ sender: Any) {
        if isUserLogIn() == true{
            if detailsArray[0].ClosedOrder!.count > 0{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = ReOrderVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "ReOrderVC") as! ReOrderVC
                obj.orderId = self.detailsArray[0].ClosedOrder![0].Id
                obj.isArchieve = false
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "You don't have orders to rate", value: "", table: nil))!)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.whereObj = 19
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
//MARK: Rate VC Delegate
@available(iOS 13.0, *)
extension SelectFeedbackTypeVC:RateVCDelegate{
    func didTapRating(vendorRating: Double, vendorComment: String, driverRating: Double, driverComment: String) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: false)
        }
    }
}
