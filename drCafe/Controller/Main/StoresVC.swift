//
//  StoresVC.swift
//  drCafe
//
//  Created by Devbox on 10/12/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@available(iOS 13.0, *)
class StoresVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITabBarControllerDelegate {

    //MARK: Store List Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var listOrMapBtn: UIButton!
    @IBOutlet weak var storesTableView: UITableView!
    @IBOutlet weak var storesTableViewTopConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var bottomCartBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var StoreListMainView: UIView!
    @IBOutlet weak var filterSearchView: UIView!
    @IBOutlet weak var filterSearchBtnView: UIView!
    @IBOutlet weak var filterMainView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    
    //MARK: Store Map Outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var storesMapMainView: UIView!
    @IBOutlet weak var storesMapView: GMSMapView!
    @IBOutlet weak var storesCollectionView: UICollectionView!
    
    @IBOutlet weak var bottomCartBtn: UIButton!
    @IBOutlet weak var bottomCartAmountLbl: UILabel!
    
    @IBOutlet weak var emptyStoresMsgMainView: UIView!
    
    var allStoreDetailsArray = [StoresListModel]()
    var allFilterStoreDetailsArray = [StoresListModel]()
    var storeDetailsArray = [StoreDetailsModel]()
    var filterDetailsArray = [StoreDetailsModel]()
    private let refreshControl = UIRefreshControl()
    var bannerDetailsArray = [BannerDetailsModel]()
    
    var isSearch : Bool = false
    var isCancel : Bool = false
    var isFilter : Bool = false
    var isLocation : Bool = false

    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
//    var latitude = 24.7233943939208
//    var longitude = 46.6365165710449
    //var latitude = 17.3168
    //var longitude = 78.5513
    var rotation:Double = 0
    var isBack = false
    
    var whereObj:Int!
    var isBanner = Bool()
    var bannerId = 0
    var orderType = 0
    var itemId = 0
    var categoryId = 0
    var itemTitle = ""
    var categoryName = ""
    var isHome = true
    var isManualLocation = false
    var isFirstManual = false
    var address:String!
    
    //Map
    var isFirst:Bool!
    var tappedMarker = GMSMarker()
    var infoWindow = StoreDetailsTVCell()
    var markers = [GMSMarker]()
    
    var selectStoreId:Int!
    var selectAddress:String!
    var isShiftOpen:Bool!
    var storeStatus:Bool!
    var orderTypeBusy:Bool!
    var is24x7:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFirstManual = true
        //Store List Functionality
        SaveAddressClass.selectFilterDetailsArray.removeAll()
        storesTableView.delegate = self
        storesTableView.dataSource = self
        storesCollectionView.delegate = self
        storesCollectionView.dataSource = self
        searchTF.addTarget(self, action: #selector(searchTextChanged(sender:)), for: .editingChanged)
        searchTF.delegate = self
        searchTF.clearButtonMode = .whileEditing
        searchTF.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Search nearest dr. CAFE available.", value: "", table: nil))!
        if whereObj == 2{
            //From Banners
            filterDetailsArray = storeDetailsArray
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
            emptyStoresMsgMainView.isHidden = true
            initGoogleMaps()
            //locationCheck()
        }
        //Map functionality
        storesMapView.delegate = self
        isFirst = true
        if AppDelegate.getDelegate().appLanguage == "English"{
            searchTF.textAlignment = .left
            searchTF.semanticContentAttribute = .forceLeftToRight
            storesCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            searchTF.textAlignment = .right
            searchTF.semanticContentAttribute = .forceRightToLeft
            storesCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        //Stores Display based on selection Map or List
        if whereObj == 1 ||  whereObj == 2{
            listOrMapBtn.setImage(UIImage(named: "Store map"), for: .normal)
            self.StoreListMainView.isHidden = false
            self.storesMapMainView.isHidden = true
            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store List", value: "", table: nil))!
            self.storesCollectionView.isHidden = true
        }else{
            self.storesCollectionView.isHidden = false
            if UserDefaults.standard.object(forKey: "mapType") != nil{
                let mapType = UserDefaults.standard.object(forKey: "mapType") as! [String:Any]
                if mapType["MapType"] as! String == "List"{
                    self.StoreListMainView.isHidden = false
                    self.storesMapMainView.isHidden = true
                    listOrMapBtn.setImage(UIImage(named: "Store map"), for: .normal)
                    navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store List", value: "", table: nil))!
                }else{
                    listOrMapBtn.setImage(UIImage(named: "List View"), for: .normal)
                    self.StoreListMainView.isHidden = true
                    self.storesMapMainView.isHidden = false
                    navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Map", value: "", table: nil))!
                }
            } else {
                 self.StoreListMainView.isHidden = false
                 self.storesMapMainView.isHidden = true
                 listOrMapBtn.setImage(UIImage(named: "Store map"), for: .normal)
                 navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store List", value: "", table: nil))!
            }
        }
    }
    func locationCheck(){
        if !hasLocationPermission() {
            if AppDelegate.getDelegate().manualLocationStr == "Done"{
                self.latitude = AppDelegate.getDelegate().currentlocation.coordinate.latitude
                self.longitude = AppDelegate.getDelegate().currentlocation.coordinate.longitude
                self.initGoogleMaps()
            }else{
                let vc = LocationAutocompleteVC(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LocationAutocompleteVC", value: "", table: nil))!, bundle: nil)
                vc.modalPresentationStyle = .overCurrentContext
                vc.locationAutocompleteVCDelegate = self
                if whereObj != nil{
                    vc.whereObj = whereObj
                }else{
                    vc.whereObj = 0
                }
                self.present(vc, animated: false, completion: nil)
            }
        }else{
            isLocation =  false
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    class func instanceFromNib() -> StoreDetailsTVCell {
        return UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreDetailsTVCell", value: "", table: nil))!, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! StoreDetailsTVCell
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        locationCheck()
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isBack == true{
            whereObj = nil
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.everyTimeCallingMethod()
    }
    override func viewDidAppear(_ animated: Bool) {
        if whereObj == nil{
            locationCheck()
        }
    }
    func everyTimeCallingMethod(){
        tabBarController?.tabBar.isHidden = false
        infoWindow.removeFromSuperview()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            if AppDelegate.getDelegate().cartQuantity > 0 || AppDelegate.getDelegate().subCartQuantity > 0 {
                tabItem.badgeValue = "\(AppDelegate.getDelegate().cartQuantity + AppDelegate.getDelegate().subCartQuantity)"
                tabItems[2].image = UIImage(named: "Cart Tab")
                tabItems[2].selectedImage = UIImage(named: "Cart Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart", value: "", table: nil))!
            }else{
                tabItem.badgeValue = nil
                tabItems[2].image = UIImage(named: "Tab Order")
                tabItems[2].selectedImage = UIImage(named: "Tab Order Select")
                tabItems[2].title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order", value: "", table: nil))!
            }
        }
        
        if whereObj == 1 || whereObj == 2{
            isBack = false
            backBtn.isHidden = false
            tabBarController?.tabBar.isHidden = true
            if isBanner == false{
                locationCheck()
            }
        }else{
            isBack = true
            backBtn.isHidden = true
            tabBarController?.tabBar.isHidden = false
            //locationCheck()
            self.StoreListMainView.isHidden = false
            self.storesMapMainView.isHidden = true
            listOrMapBtn.setImage(UIImage(named: "Store map"), for: .normal)
            navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store List", value: "", table: nil))!
        }
        
        self.StoreListMainView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            if StoreListMainView.isHidden == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
                }else{
                    self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                }
                navigationBarTitleLbl.textColor = .white
            }else{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
                }else{
                    self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                }
                navigationBarTitleLbl.textColor = .black
            }
            bottomCartBtn.backgroundColor = .darkGray
            bottomCartBtn.setTitleColor(.white, for: .normal)
        } else {
            // User Interface is Light
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
            bottomCartBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            bottomCartBtn.setTitleColor(.white, for: .normal)
        }
        
        if whereObj == 1 || whereObj == 2{
            if AppDelegate.getDelegate().cartQuantity > 0{
                self.bottomCartBtnHeight.constant = 55
                self.bottomCartAmountLbl.isHidden = false
                self.bottomCartAmountLbl.text = "\(AppDelegate.getDelegate().cartAmount.withCommas()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            }else{
                self.bottomCartBtnHeight.constant = 0
                self.bottomCartAmountLbl.isHidden = true
            }
        }else{
            self.bottomCartBtnHeight.constant = 0
            self.bottomCartAmountLbl.isHidden = true
        }
        
        if isBanner == true{
            filterSearchView.isHidden = true
            filterMainView.isHidden = true
            filterSearchBtnView.isHidden = true
            storesTableViewTopConstraints.constant = 100
        }else{
            //filterSearchView.isHidden = false
            //filterMainView.isHidden = false
            storesTableViewTopConstraints.constant = 150
        }
    }
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    hasPermission = false
                case .authorizedAlways, .authorizedWhenInUse:
                    hasPermission = true
                @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    func openSettingApp(title: String,message: String) {
        let alertController = UIAlertController (title: message, message:nil , preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
          guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
          }
          if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
          }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default) { (_) -> Void in
            let vc = LocationAutocompleteVC(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "LocationAutocompleteVC", value: "", table: nil))!, bundle: nil)
            vc.modalPresentationStyle = .overCurrentContext
            vc.locationAutocompleteVCDelegate = self
            if self.whereObj != nil{
                vc.whereObj = self.whereObj
            }else{
                vc.whereObj = 0
            }
            self.present(vc, animated: false, completion: nil)
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    func swipeCell(){
        if !UserDefaults.standard.bool(forKey: "StoreSwipe") {
            UserDefaults.standard.set(false, forKey: "StoreSwipe")
            UserDefaults.standard.synchronize()
        }
        //Automatic Swipe item Cell functionality
        let indexPath = NSIndexPath(row: 0, section: 0)
        let cell = storesTableView.cellForRow(at: indexPath as IndexPath);
        
        let swipeView = UIView()
        swipeView.frame = CGRect(x: cell!.bounds.size.width, y: 0, width: 200, height: cell!.bounds.size.height)
        swipeView.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7529411765, blue: 0.768627451, alpha: 1)

        var swipeStoreInfoView: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 80, height: cell!.bounds.size.height))
        if AppDelegate.getDelegate().appLanguage == "English"{
            swipeStoreInfoView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 80, height: cell!.bounds.size.height))
        }else{
            swipeStoreInfoView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: cell!.bounds.size.height))
        }
        swipeStoreInfoView.backgroundColor = .clear
        swipeView.addSubview(swipeStoreInfoView)

        let swipeStoreInfoImg: UIImageView = UIImageView.init(frame: CGRect(x: swipeStoreInfoView.frame.width/2 - 15, y: swipeStoreInfoView.frame.height/2 - 20, width: 20, height: 20))
        swipeStoreInfoImg.image = UIImage(named: "Alert")
        swipeStoreInfoImg.backgroundColor = .clear
        swipeStoreInfoView.addSubview(swipeStoreInfoImg)
        
        let swipeStoreInfoLbl: UILabel = UILabel.init(frame: CGRect(x: 5, y: Int(swipeStoreInfoImg.frame.maxY), width: Int(swipeStoreInfoView.frame.width) - 10, height: 45))
        swipeStoreInfoLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Info", value: "", table: nil))!
        swipeStoreInfoLbl.backgroundColor = .clear
        swipeStoreInfoLbl.textColor = .white
        swipeStoreInfoLbl.numberOfLines = 2
        swipeStoreInfoLbl.textAlignment = .center
        swipeStoreInfoLbl.font = UIFont(name: "Avenir Medium", size: 14.0)
        swipeStoreInfoView.addSubview(swipeStoreInfoLbl)
        
        var swipeFavView: UILabel = UILabel.init(frame: CGRect(x: swipeStoreInfoView.frame.size.width, y: 0, width: 120, height: cell!.bounds.size.height))
        if AppDelegate.getDelegate().appLanguage == "English"{
            swipeFavView = UILabel.init(frame: CGRect(x: swipeStoreInfoView.frame.size.width, y: 0, width: 120, height: cell!.bounds.size.height))
        }else{
            swipeFavView = UILabel.init(frame: CGRect(x: swipeStoreInfoView.frame.size.width, y: 0, width: 100, height: cell!.bounds.size.height))
        }
        swipeFavView.backgroundColor = .clear
        swipeView.addSubview(swipeFavView)

        let swipeFavImg: UIImageView = UIImageView.init(frame: CGRect(x: swipeFavView.frame.width/2 - 15, y: swipeFavView.frame.height/2 - 20, width: 20, height: 20))
        swipeFavImg.image = UIImage(named: "security-pin")
        swipeFavImg.backgroundColor = .clear
        swipeFavView.addSubview(swipeFavImg)
        
        let swipeFavLbl: UILabel = UILabel.init(frame: CGRect(x: 5, y: Int(swipeFavImg.frame.maxY), width: Int(swipeFavView.frame.width) - 10, height: 45))
        swipeFavLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make as a Favourite store", value: "", table: nil))!
        swipeFavLbl.backgroundColor = .clear
        swipeFavLbl.textColor = .white
        swipeFavLbl.numberOfLines = 2
        swipeFavLbl.textAlignment = .center
        swipeFavLbl.font = UIFont(name: "Avenir Medium", size: 14.0)
        swipeFavView.addSubview(swipeFavLbl)
        
        cell!.addSubview(swipeView)

        UIView.animate(withDuration: 0.80, animations: {
            cell!.frame = CGRect(x: cell!.frame.origin.x - 200, y: cell!.frame.origin.y, width: cell!.bounds.size.width + 200, height: cell!.bounds.size.height)
        }) { (finished) in
            UIView.animate(withDuration: 0.80, animations: {
                cell!.frame = CGRect(x: cell!.frame.origin.x + 200, y: cell!.frame.origin.y, width: cell!.bounds.size.width - 200, height: cell!.bounds.size.height)

            }, completion: { (finished) in
                for subview in swipeView.subviews {
                    subview.removeFromSuperview()
                }
                swipeView.removeFromSuperview()
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            let indexpath = IndexPath(row: 0, section: 0)
            self.storesTableView.reloadRows(at: [indexpath], with: .none)
        }
    }
    //MARK: Get Stores List Service
    func getStoreListService(){
        var originLat = String(format: "%.6f", latitude)
        var originLong = String(format: "%.6f", longitude)
        if latitude > 0 && longitude > 0{
            originLat = String(format: "%.6f", latitude)
            originLong = String(format: "%.6f", longitude)
        }else{
            originLat = ""
            originLong = ""
        }
        let dic:[String:Any] = ["DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "Latitude":originLat, "Longitude":originLong, "orderType":orderType, "onlineStores":1, "RequestBy":2, "UserId":"\(UserDef.getUserId())"]
        print(dic)
        OrderModuleServices.GetStoreListService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeDetailsArray = data.Data!
                self.filterDetailsArray = data.Data!
                self.allStoreDetailsArray = [data]
                if self.allStoreDetailsArray.count > 0{
                    if self.allStoreDetailsArray[0].OrderTypes != nil{
                        if self.allStoreDetailsArray[0].OrderTypes!.count > 0{
                            for i in 0...self.allStoreDetailsArray[0].OrderTypes!.count - 1{
                                self.allStoreDetailsArray[0].OrderTypes![i].isSelect = false
                            }
                        }
                    }
                }
                //print(data.Data!)
                DispatchQueue.main.async {
                    if data.Data!.count > 0{
                        self.storesTableView.reloadData()
                        self.emptyStoresMsgMainView.isHidden = true
                        self.listOrMapBtn.isHidden = false
                        self.filterMainView.isHidden = false
                        self.filterSearchView.isHidden = false
                        self.filterSearchBtnView.isHidden = false
                        if self.whereObj == 1 || self.whereObj == 2{
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                if UserDefaults.standard.value(forKey: "StoreSwipe") == nil{
                                    self.swipeCell()
                                }
//                                if AppDelegate.getDelegate().isStoreAnimationFirst == false{
//                                    AppDelegate.getDelegate().isStoreAnimationFirst = true
//                                    self.swipeCell()
//                                }
                            }
                        }
                    }else{
                        self.emptyStoresMsgMainView.isHidden = false
                        self.listOrMapBtn.isHidden = true
                        self.filterMainView.isHidden = true
                        self.filterSearchView.isHidden = true
                        self.filterSearchBtnView.isHidden = true
                    }
                    //self.storesTableView.reloadData()
                    if self.isFilter == true{
                        self.filterStores(dic: self.allFilterStoreDetailsArray)
                        self.isFilter = false
                    }
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Get Stores List Index Select Service
    func getStoresService(){
        var originLat = String(format: "%.6f", latitude)
        var originLong = String(format: "%.6f", longitude)
        if latitude > 0 && longitude > 0{
            originLat = String(format: "%.6f", latitude)
            originLong = String(format: "%.6f", longitude)
        }else{
            originLat = ""
            originLong = ""
        }
        let dic:[String:Any] = ["DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "Latitude":originLat, "Longitude":originLong, "onlineStores":1, "RequestBy":2, "UserId":"\(UserDef.getUserId())"]
        OrderModuleServices.GetStoresService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                self.storeDetailsArray = data.Data!
                self.filterDetailsArray = data.Data!
                self.allStoreDetailsArray = [data]
                //print(data.Data!)
                if self.allStoreDetailsArray.count > 0{
                    if self.allStoreDetailsArray[0].OrderTypes != nil{
                        if self.allStoreDetailsArray[0].OrderTypes!.count > 0{
                            for i in 0...self.allStoreDetailsArray[0].OrderTypes!.count - 1{
                                self.allStoreDetailsArray[0].OrderTypes![i].isSelect = false
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    if data.Data!.count > 0{
                        self.storesTableView.reloadData()
                        self.emptyStoresMsgMainView.isHidden = true
                        self.listOrMapBtn.isHidden = false
                        self.filterMainView.isHidden = false
                        self.filterSearchView.isHidden = false
                        self.filterSearchBtnView.isHidden = false
                        if self.whereObj == 1 || self.whereObj == 2{
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                if UserDefaults.standard.value(forKey: "StoreSwipe") == nil{
                                    self.swipeCell()
                                }
//                                if AppDelegate.getDelegate().isStoreAnimationFirst == false{
//                                    AppDelegate.getDelegate().isStoreAnimationFirst = true
//                                    self.swipeCell()
//                                }
                            }
                        }
                    }else{
                        self.emptyStoresMsgMainView.isHidden = false
                        self.listOrMapBtn.isHidden = true
                        self.filterMainView.isHidden = true
                        self.filterSearchView.isHidden = true
                        self.filterSearchBtnView.isHidden = true
                    }
                    //self.storesTableView.reloadData()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
        initGoogleMaps()
//        self.emptyStoresMsgMainView.isHidden = false
//        self.listOrMapBtn.isHidden = true
//        self.filterMainView.isHidden = true
//        self.filterSearchView.isHidden = true
//        self.filterSearchBtnView.isHidden = true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isLocation ==  false {
            isLocation =  true
            let location = locations.last
            latitude = (location?.coordinate.latitude)!
            longitude = (location?.coordinate.longitude)!
//            if latitude < 24{
                //latitude = 24.782765
                //longitude = 46.767433
//            }
           
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
//            for dic in filterDetailsArray{
//                if dic.StoreType == 2{
//                    createMarker(titleMarker: dic.NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(dic.Latitude)!, longitude: Double(dic.Longitude)!)
//                }else{
//                    createMarker(titleMarker: dic.NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(dic.Latitude)!, longitude: Double(dic.Longitude)!)
//                }
//            }
            initGoogleMaps()
        }
        self.locationManager.stopUpdatingLocation()
    }
    func dropMarkers(indepath:Int){
        if indepath >= filterDetailsArray.count {
            return
        }
        let dic = filterDetailsArray[indepath]
        if dic.StoreType == 2{
            createMarkers(titleMarker: dic.NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(dic.Latitude)!, longitude: Double(dic.Longitude)!, indexPath: indepath)
        }else{
            createMarkers(titleMarker: dic.NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(dic.Latitude)!, longitude: Double(dic.Longitude)!, indexPath: indepath)
        }
        
    }
    func createMarkers(titleMarker:String, iconMarker:UIImage , latitude: CLLocationDegrees, longitude:CLLocationDegrees, indexPath:Int){
        var viewPoints = self.storesMapView.projection.point(for: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
            viewPoints.y = 0
        let location = self.storesMapView.projection.coordinate(for: viewPoints)
        let marker = GMSMarker()
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.position = CLLocationCoordinate2DMake(location.latitude,location.longitude)
        marker.snippet = titleMarker
        marker.icon = iconMarker
        marker.map = self.storesMapView
        self.markers.append(marker)

       //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(0.5, forKey: kCATransactionAnimationDuration)
            marker.position = CLLocationCoordinate2DMake(latitude,longitude)
            marker.map = self.storesMapView
        CATransaction.commit()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.dropMarkers(indepath: indexPath + 1)
        }

//        let viewPoints1 = self.storesMapView.projection.point(for: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
//        let timeInterval: CFTimeInterval = 1.0
//        let rotateAnimation = CABasicAnimation(keyPath: "position.y")
//            rotateAnimation.fromValue = viewPoints.y
//            rotateAnimation.toValue = viewPoints1.y
//            rotateAnimation.duration = timeInterval
//            rotateAnimation.repeatCount = 1 //Float.infinity
//            marker.layer.add(rotateAnimation, forKey: nil)
//            self.markers.append(marker)
//            marker.position = CLLocationCoordinate2DMake(latitude,longitude)
//            marker.map = self.storesMapView
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            self.dropMarkers(indepath: indexPath + 1)
//        }
        
//        UIView.animate(withDuration: 1.0, delay: 0, options: .beginFromCurrentState, animations: {
//            marker.position = CLLocationCoordinate2DMake(latitude,longitude)
//            self.storesMapView.layoutIfNeeded()
//        }) { (value) in
//            marker.map = self.storesMapView
//            self.markers.append(marker)
//            self.storesMapView.layoutIfNeeded()
//            UIView.animate(withDuration: 1.0, animations: {
//                self.dropMarkers(indepath: indexPath + 1)
//            })
//        }
        
    }
    
    func createMarker(titleMarker:String, iconMarker:UIImage , latitude: CLLocationDegrees, longitude:CLLocationDegrees, indexPath:Int){
        let viewPoints = self.storesMapView.projection.point(for: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
            //viewPoints.y = 0
        _ = self.storesMapView.projection.coordinate(for: viewPoints)
        
        let pinView = UIView(frame: CGRect(x: viewPoints.x-22, y: viewPoints.y - 200, width: 44, height: 44))
        let marker = GMSMarker()
        marker.appearAnimation = GMSMarkerAnimation.pop
        //marker.position = CLLocationCoordinate2DMake(location.latitude,location.longitude)
        marker.position = CLLocationCoordinate2DMake(latitude,longitude)

        marker.snippet = titleMarker
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
            imgView.image = iconMarker
        imgView.contentMode = .scaleAspectFit
        pinView.addSubview(imgView)
        marker.iconView = pinView
        //marker.icon = iconMarker
        //marker.map = self.storesMapView
        //self.markers.append(marker)
        
        UIView.animate(withDuration: 1.0, delay: 0, options: .beginFromCurrentState, animations: {
                pinView.frame = CGRect(x: viewPoints.x - 22, y: viewPoints.y - 22, width: 44, height: 44)
            //marker.position = CLLocationCoordinate2DMake(latitude,longitude)
            marker.map = self.storesMapView
            self.markers.append(marker)
            self.storesMapView.layoutIfNeeded()
            }) { (value) in
                     marker.map = self.storesMapView
                    UIView.animate(withDuration: 1.0, animations: {
        //                //pinView.alpha = 0.0
                        self.dropMarkers(indepath: indexPath + 1)
                   })
         }


//        self.storesMapView.addSubview(pinView)
//        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
//            pinView.frame = CGRect(x: viewPoints.x - 22, y: viewPoints.y - 22, width: 44, height: 44)
//            self.storesMapView.layoutIfNeeded()
//        }) { (value) in
//             marker.map = self.storesMapView
//            UIView.animate(withDuration: 1.0, animations: {
//                //pinView.alpha = 0.0
//                marker.map = self.storesMapView
//                self.markers.append(marker)
//                self.storesMapView.layoutIfNeeded()
//                self.dropMarkers(indepath: indexPath + 1)
//
//            })
//        }
        
        
//        let viewPoints1 = self.storesMapView.projection.point(for: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
//        let timeInterval: CFTimeInterval = 4
//        let rotateAnimation = CABasicAnimation(keyPath: "position.y")
//            rotateAnimation.fromValue = 0
//            rotateAnimation.toValue = viewPoints1.y
//            rotateAnimation.isRemovedOnCompletion = false
//            rotateAnimation.duration = timeInterval
//            rotateAnimation.repeatCount = 1 //Float.infinity
//            marker.layer.add(rotateAnimation, forKey: nil)
//            UIView.animate(withDuration: 1, animations: {
//                marker.position = CLLocationCoordinate2DMake(latitude,longitude)
//                marker.map = self.storesMapView
//                self.markers.append(marker)
//                self.view.layoutIfNeeded()
//            })
//

            
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        rotation = heading.magneticHeading
    }
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:latitude, longitude: longitude, zoom: 11.0)//13.0
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            mapView.isMyLocationEnabled = true
        self.storesMapView.camera = camera
        self.storesMapView.animate(to: camera)
        self.storesMapView.delegate = self
        self.storesMapView.isMyLocationEnabled = true
        self.storesMapView.settings.myLocationButton = true
        if isFirst == true{
            if whereObj == 1{
                self.getStoreListService()
            }else{
                self.getStoresService()
            }
            self.isFirst = false
        }else{
            if whereObj != 1 && whereObj != 2{
                self.getStoresService()
            }else{
                if whereObj == 1{
                    self.getStoreListService()
                }
            }
        }
    }
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        mapView.isMyLocationEnabled = false
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        mapView.isMyLocationEnabled = false
        if (gesture){
            mapView.selectedMarker = nil
            print("Hello")
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.isMyLocationEnabled = true
        
        var locationDetails = [StoreDetailsModel]()
        if let index1 = markers.firstIndex(of: marker) {
            locationDetails = filterDetailsArray.filter{$0.NameEn == markers[index1].snippet!}
        }
        
        let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)

        tappedMarker = marker
        infoWindow.removeFromSuperview()

        infoWindow = StoresVC.instanceFromNib()

        selectStoreId = locationDetails[0].Id
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            selectAddress = locationDetails[0].NameEn
            infoWindow.storeNameLbl.text = locationDetails[0].NameEn
            infoWindow.storeAddressLbl.text = locationDetails[0].AddressEn
        }else{
            selectAddress = locationDetails[0].NameAr
            infoWindow.storeNameLbl.text = locationDetails[0].NameAr
            infoWindow.storeAddressLbl.text = locationDetails[0].AddressAr
        }
        
        infoWindow.storeSelectBtn.tag = 0
        infoWindow.storeSelectBtn.addTarget(self, action: #selector(storeSelectBtn_Tapped(_:)), for: .touchUpInside)
        
        isShiftOpen = locationDetails[0].isShiftOpen
        is24x7 = locationDetails[0].is24x7!
        storeStatus = locationDetails[0].StoreStatus
        if whereObj == 1 || whereObj == 2{
            if locationDetails[0].OrderTypeBusy != nil{
                orderTypeBusy = locationDetails[0].OrderTypeBusy!
            }else{
                orderTypeBusy = false
            }
        }else{
            if allStoreDetailsArray.count > 0{
                if allStoreDetailsArray[0].OrderTypes != nil{
                    if allStoreDetailsArray[0].OrderTypes!.count > 0{
                        let orderTypeArray = allStoreDetailsArray[0].OrderTypes!.filter({$0.isSelect! == true})
                        if orderTypeArray.count > 0{
                            let orderBusy = locationDetails[0].BusySchedule!.filter({$0.OrderTypeId == orderTypeArray[0].Id})
                            if orderBusy.count > 0{
                                orderTypeBusy = orderBusy[0].IsBusy
                            }else{
                                orderTypeBusy = false
                            }
                        }else{
                            orderTypeBusy = false
                        }
                    }
                }
            }
        }
        
        //Store Status Functionality
        if locationDetails[0].is24x7! == true{
            if orderTypeBusy! == true{
                infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                infoWindow.storeStatusBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
            }else{
                infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
            }
        }else{
            if locationDetails[0].StoreStatus == true{
                if locationDetails[0].isShiftOpen == true{
                    if orderTypeBusy! == true{
                        infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                        infoWindow.storeStatusBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                    }else{
                        infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                        infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                    }
                }else{
                    infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                    infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
                }
            }else{
                infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
            }
        }
        infoWindow.storeStatusBtn.titleLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        infoWindow.center = mapView.projection.point(for: location)

        infoWindow.center.y = infoWindow.center.y - 83

        self.view.addSubview(infoWindow)
        
        //Scrolling functionality
        if whereObj != 1 && whereObj != 2{
            var indexNum = 0
            if filterDetailsArray.count > 0{
                for i in 0...filterDetailsArray.count - 1{
                    if locationDetails[0].Id == filterDetailsArray[i].Id{
                        indexNum = i
                        break
                    }
                }
            }
            let index = IndexPath(item: indexNum, section: 0)
            self.storesCollectionView.scrollToItem(at: index, at: .left, animated: true)
        }
        
        return false
    }
    //MARK: Store Select Button Action
    @objc func storeSelectBtn_Tapped(_ sender: UIButton){
        if storeStatus == true{
            if isShiftOpen == true && orderTypeBusy == false || is24x7 == true && orderTypeBusy == false{
                if whereObj == 1 || whereObj == 2{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = MenuVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                    obj.address = selectAddress!
                    obj.storeID = selectStoreId!
                    obj.selectOrderType = orderType
                    obj.emptyCart = false
                    AppDelegate.getDelegate().cartStoreId = 0
                    AppDelegate.getDelegate().cartStoreName = selectAddress!
                    self.navigationController?.pushViewController(obj, animated: true)
                }else{
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = StoreInfoVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoreInfoVC") as! StoreInfoVC
                    obj.storeId = selectStoreId!
                    obj.orderType = orderType
                    obj.isShiftOpen = isShiftOpen!
                    obj.isBusy = orderTypeBusy!
                    obj.whereObj = 1
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let location = CLLocationCoordinate2D(latitude: tappedMarker.position.latitude, longitude: tappedMarker.position.longitude)
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 83
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        print("coordinate \(coordinate)")
    }
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.isMyLocationEnabled = true
        mapView.selectedMarker = nil
        return false
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        whereObj = nil
        if isHome == true || isManualLocation == true{
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Search Button Action
    @IBAction func searchBtn_Tapped(_ sender: UIButton) {
        let searchText = searchTF.text!
        if searchText != "" {
            self.filterDetailsArray.removeAll()
            if storeDetailsArray.count == 0{
                if whereObj == 1{
                    self.getStoreListService()
                }else{
                    self.getStoresService()
                }
                return
            }
            if storeDetailsArray.count > 0{
                filterDetailsArray = storeDetailsArray.filter({$0.NameEn.lowercased().contains(searchText.lowercased())})
            }
            self.storesTableView.reloadData()
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            self.storesMapView.clear()
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
//            if filterDetailsArray.count > 0{
//                for i in 0...self.filterDetailsArray.count-1{
//                    if filterDetailsArray[i].StoreType == 2{
//                        createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                    }else{
//                        createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                    }
//                }
//            }
        }else{
            filterDetailsArray = storeDetailsArray
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            self.storesMapView.clear()
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
//            for i in 0...self.filterDetailsArray.count-1{
//                if filterDetailsArray[i].StoreType == 2{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }else{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }
//            }
        }
    }
    //MARK: Filter Button Action
    @IBAction func filterBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresFilterVC") as! StoresFilterVC
        obj.storesFilterVCDelegate = self
        obj.filterDetailsArray = allStoreDetailsArray
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        if whereObj == 1 || whereObj == 2{
            obj.orderType = orderType
            obj.whereObj = 1
        }else{
            obj.orderType = 0
            obj.whereObj = 2
        }
        present(obj, animated: false, completion: nil)
    }
    //MARK: Bottom Cart Button Action
    @IBAction func bottomCartBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = CartVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        obj.whereObj = 2
        obj.isRemove = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: Store Map Button Action
    @IBAction func listOrMapBtn_Tapped(_ sender: Any) {
        infoWindow.removeFromSuperview()
        if StoreListMainView.isHidden == false{
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
            storesMapMainView.isHidden = false
            StoreListMainView.isHidden = true
            listOrMapBtn.setImage(UIImage(named: "List View"), for: .normal)
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
                if AppDelegate.getDelegate().appLanguage == "Arabic"{
                    if filterDetailsArray.count > 0{
                        self.storesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
                    }
                }
            }
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
            
//            self.storesMapView.clear()
//            for i in 0...filterDetailsArray.count-1{
//                if filterDetailsArray[i].StoreType == 2{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }else{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }
//            }

        }else{
            storesMapMainView.isHidden = true
            StoreListMainView.isHidden = false
            listOrMapBtn.setImage(UIImage(named: "Store map"), for: .normal)
            if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
                }else{
                    self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
                }
                navigationBarTitleLbl.textColor = .white
            } else {
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
                }else{
                    self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
                }
                navigationBarTitleLbl.textColor = .black
            }
        }
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension StoresVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        storesTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreListTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreListTVCell", value: "", table: nil))!)
        let cell = storesTableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoreListTVCell", value: "", table: nil))!, for: indexPath) as! StoreListTVCell

        if filterDetailsArray.count > 0{
            //Store Status
            var currentDate:Date!
            if filterDetailsArray[indexPath.row].StoreStatus == true{
                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                
                // Timing Details
                if filterDetailsArray[indexPath.row].CurrentDateTime != nil{
                    if filterDetailsArray[indexPath.row].CurrentDateTime!.contains("."){
                        var time = filterDetailsArray[indexPath.row].CurrentDateTime!
                        time = time.components(separatedBy: ".")[0]
                        currentDate = DateConverts.convertStringToDate(date: time, dateformatType: .dateTtime)
                    }else{
                        currentDate = DateConverts.convertStringToDate(date: filterDetailsArray[indexPath.row].CurrentDateTime!, dateformatType: .dateTtime)
                    }
                }else{
                    currentDate = DateConverts.convertStringToDate(date: "\(Date())", dateformatType: .dateTtime)
                }
                
                let day:String = DateConverts.convertDateToString(date: currentDate, dateformatType: .EEEE)
                let shifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == day})
                
                if shifts.count > 0{
                    var nextOpenTime = ""
                    let startTime = DateConverts.convertStringToDate(date: shifts[0].StartDateTime, dateformatType: .dateTtime)
                    let endTime = DateConverts.convertStringToDate(date: shifts[0].EndDateTime, dateformatType: .dateTtime)
                    if currentDate! > startTime{
                        if currentDate! > endTime{
                            for i in 1...7{
                                let newDate = Calendar.current.date(byAdding: .day, value: i, to: currentDate)!
                                let day:String = DateConverts.convertDateToString(date: newDate, dateformatType: .EEEE)
                                let nextShifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == day})
                                if nextShifts[0].IsClose == false {
                                    if i == 1 {
                                        nextOpenTime = nextShifts[0].StartTime
                                    }else{
                                        nextOpenTime = "\(nextShifts[0].wkday.prefix(3).capitalized) \(nextShifts[0].StartTime)"
                                    }
                                  break
                                }
                            }
                        }else{
                            nextOpenTime = shifts[0].EndTime
                        }
                    }else{
                        nextOpenTime = shifts[0].StartTime
                    }
                    
                    //Store Timing Functionality
                    var isOrderTypeBusy = false
                    if whereObj == 1 || whereObj == 2{
                        if filterDetailsArray[indexPath.row].OrderTypeBusy != nil{
                            isOrderTypeBusy = filterDetailsArray[indexPath.row].OrderTypeBusy!
                        }
                    }else{
                        if allStoreDetailsArray.count > 0{
                            if allStoreDetailsArray[0].OrderTypes != nil{
                                if allStoreDetailsArray[0].OrderTypes!.count > 0{
                                    let orderTypeArray = allStoreDetailsArray[0].OrderTypes!.filter({$0.isSelect! == true})
                                    if orderTypeArray.count > 0{
                                        let orderBusy = filterDetailsArray[indexPath.row].BusySchedule!.filter({$0.OrderTypeId == orderTypeArray[0].Id})
                                        if orderBusy.count > 0{
                                            isOrderTypeBusy = orderBusy[0].IsBusy
                                        }else{
                                            isOrderTypeBusy = false
                                        }
                                    }else{
                                        isOrderTypeBusy = false
                                    }
                                }
                            }
                        }
                    }
                    if filterDetailsArray[indexPath.row].is24x7! == true{
                        if isOrderTypeBusy == true{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                            //Change by swathi(12-04-2022)
                            //cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                            //cell.timeTwoLbl.text = nextOpenTime
                            if filterDetailsArray[indexPath.row].OpensInMinutes != nil{
                                cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open in", value: "", table: nil))!
                                cell.timeTwoLbl.text = "\(filterDetailsArray[indexPath.row].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)"
                            }else{
                                cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                cell.timeTwoLbl.text = nextOpenTime
                            }
                            cell.timeOneLbl.isHidden = false
                            cell.timeTwoLbl.isHidden = false
                            cell.timeImg.isHidden = true
                        }else{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                            cell.timeOneLbl.isHidden = true
                            cell.timeTwoLbl.isHidden = true
                            cell.timeImg.isHidden = false
                        }
                    }else{
                        cell.timeOneLbl.isHidden = false
                        cell.timeTwoLbl.isHidden = false
                        cell.timeImg.isHidden = true
                        if filterDetailsArray[indexPath.row].isShiftOpen == true{
                            if isOrderTypeBusy == true{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                                //Change by swathi(12-04-2022)
                                //cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                //cell.timeTwoLbl.text = nextOpenTime
                                if filterDetailsArray[indexPath.row].OpensInMinutes != nil{
                                    cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open in", value: "", table: nil))!
                                    cell.timeTwoLbl.text = "\(filterDetailsArray[indexPath.row].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)"
                                }else{
                                    cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                    cell.timeTwoLbl.text = nextOpenTime
                                }
                            }else{
                                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                                //Change by swathi(12-04-2022)
                                //cell.timeOneLbl.text = shifts[0].StartTime
                                //cell.timeTwoLbl.text = shifts[0].EndTime
                                if filterDetailsArray[indexPath.row].ShiftBusinessDate != nil{
                                    if filterDetailsArray[indexPath.row].ShiftBusinessDate! != ""{
                                        let dayName = DateConverts.convertStringToStringDates(inputDateStr: filterDetailsArray[indexPath.row].ShiftBusinessDate!, inputDateformatType: .dateR, outputDateformatType: .EEEE)
                                        let daysShifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == dayName})
                                        if daysShifts.count > 0{
                                            if daysShifts[0].StartTime.contains("00:00") && daysShifts[0].EndTime.contains("00:00"){
                                                cell.timeOneLbl.text = "12:\(daysShifts[0].StartTime.components(separatedBy: ":")[1])"
                                                cell.timeTwoLbl.text = "12:\(daysShifts[0].EndTime.components(separatedBy: ":")[1])"
                                            }else{
                                                if daysShifts[0].StartTime.contains("00:00"){
                                                    cell.timeOneLbl.text = "12:\(daysShifts[0].StartTime.components(separatedBy: ":")[1])"
                                                    cell.timeTwoLbl.text = daysShifts[0].EndTime
                                                }else if daysShifts[0].EndTime.contains("00:00"){
                                                    cell.timeOneLbl.text = daysShifts[0].StartTime
                                                    cell.timeTwoLbl.text = "12:\(daysShifts[0].EndTime.components(separatedBy: ":")[1])"
                                                }else{
                                                    cell.timeOneLbl.text = daysShifts[0].StartTime
                                                    cell.timeTwoLbl.text = daysShifts[0].EndTime
                                                }
                                            }
                                        }else{
                                            cell.timeOneLbl.text = ""
                                            cell.timeTwoLbl.text = ""
                                        }
                                    }else{
                                        if shifts[0].StartTime.contains("00:00") && shifts[0].EndTime.contains("00:00"){
                                            cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                            cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                        }else{
                                            if shifts[0].StartTime.contains("00:00"){
                                                cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                                cell.timeTwoLbl.text = shifts[0].EndTime
                                            }else if shifts[0].EndTime.contains("00:00"){
                                                cell.timeOneLbl.text = shifts[0].StartTime
                                                cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                            }else{
                                                cell.timeOneLbl.text = shifts[0].StartTime
                                                cell.timeTwoLbl.text = shifts[0].EndTime
                                            }
                                        }
                                    }
                                }else{
                                    if shifts[0].StartTime.contains("00:00") && shifts[0].EndTime.contains("00:00"){
                                        cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                        cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                    }else{
                                        if shifts[0].StartTime.contains("00:00"){
                                            cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                            cell.timeTwoLbl.text = shifts[0].EndTime
                                        }else if shifts[0].EndTime.contains("00:00"){
                                            cell.timeOneLbl.text = shifts[0].StartTime
                                            cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                        }else{
                                            cell.timeOneLbl.text = shifts[0].StartTime
                                            cell.timeTwoLbl.text = shifts[0].EndTime
                                        }
                                    }
                                }
                            }
                        }else{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
                            cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                            cell.timeTwoLbl.text = nextOpenTime
                        }
                    }
                }else{
                    cell.timeOneLbl.isHidden = false
                    cell.timeTwoLbl.isHidden = false
                    cell.timeImg.isHidden = true
                    cell.timeOneLbl.text = ""
                    cell.timeTwoLbl.text = ""
                }
            }else if filterDetailsArray[indexPath.row].StoreStatus == false{
                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)

                cell.timeOneLbl.isHidden = false
                cell.timeTwoLbl.isHidden = false
                cell.timeImg.isHidden = true
                cell.timeOneLbl.text = ""
                cell.timeTwoLbl.text = ""
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.storeNameLbl.text = filterDetailsArray[indexPath.row].NameEn
                cell.storeAddressLbl.text = filterDetailsArray[indexPath.row].AddressEn
            }else{
                cell.storeNameLbl.text = filterDetailsArray[indexPath.row].NameAr
                cell.storeAddressLbl.text = filterDetailsArray[indexPath.row].AddressAr
            }
            if filterDetailsArray[indexPath.row].Distance != nil{
                cell.distanceLbl.text = filterDetailsArray[indexPath.row].Distance
            }else{
                cell.distanceLbl.text = ""
            }
            
            //Facilities functionality
            if filterDetailsArray[indexPath.row].Facilities!.count > 0 || filterDetailsArray[indexPath.row].Segments!.count > 0{
                cell.facilitesStackView.isHidden = false
                if filterDetailsArray[indexPath.row].Facilities!.count > 0 && filterDetailsArray[indexPath.row].Facilities!.count < 4 {
                    for i in 0...filterDetailsArray[indexPath.row].Facilities!.count - 1{
                        //Image
                        let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(filterDetailsArray[indexPath.row].Facilities![i].Image)" as NSString
                        let charSet = CharacterSet.urlFragmentAllowed
                        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                        let url = URL.init(string: urlStr as String)
                        cell.faciliteisImg[i].kf.setImage(with: url)
                    }
                }else{
                    if filterDetailsArray[indexPath.row].Facilities!.count >= 4{
                        for i in 0...3{
                            //Image
                            let ImgStr:NSString = "\(AppDelegate.getDelegate().imagePathURL)\(filterDetailsArray[indexPath.row].Facilities![i].Image)" as NSString
                            let charSet = CharacterSet.urlFragmentAllowed
                            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                            let url = URL.init(string: urlStr as String)
                            cell.faciliteisImg[i].kf.setImage(with: url)
                        }
                    }else{
                        cell.facilitesStackView.isHidden = true
                    }
                }
                var stackWidth = 20.0
                if filterDetailsArray[indexPath.row].Facilities!.count == 1{
                    cell.faciliteisImg[0].isHidden = false
                    for i in 1...3{
                        cell.faciliteisImg[i].isHidden = true
                    }
                    stackWidth = 20.0
                }else if filterDetailsArray[indexPath.row].Facilities!.count == 2{
                    cell.faciliteisImg[2].isHidden = true
                    cell.faciliteisImg[3].isHidden = true
                    for i in 0...1{
                        cell.faciliteisImg[i].isHidden = false
                    }
                    stackWidth = 42.5
                }else if filterDetailsArray[indexPath.row].Facilities!.count == 3{
                    cell.faciliteisImg[3].isHidden = true
                    for i in 0...2{
                        cell.faciliteisImg[i].isHidden = false
                    }
                    stackWidth = 65.0
                }else if filterDetailsArray[indexPath.row].Facilities!.count == 4{
                    for i in 0...3{
                        cell.faciliteisImg[i].isHidden = false
                    }
                    stackWidth = 87.5
                }
                if filterDetailsArray[indexPath.row].Segments!.count > 0 && filterDetailsArray[indexPath.row].Segments!.count == 1{
                    cell.faciliteisImg[4].isHidden = false
                    cell.faciliteisImg[4].image = UIImage(named: "University")
                    stackWidth = stackWidth + 22.5
                }else{
                    cell.faciliteisImg[4].isHidden = true
                }
                cell.facilitiesImgWidth.constant = CGFloat(stackWidth)
            }else if filterDetailsArray[indexPath.row].Facilities!.count == 0 && filterDetailsArray[indexPath.row].Segments!.count == 0{
                cell.facilitesStackView.isHidden = true
            }
            
            if filterDetailsArray[indexPath.row].Favourite == true{
                cell.favouriteBtn.isHidden = false
                cell.favouriteBtnWidth.constant = 24
            }else{
                cell.favouriteBtn.isHidden = true
                cell.favouriteBtnWidth.constant = 0
            }
            
            if filterDetailsArray[indexPath.row].StoreType == 2{
                cell.v12ViewWidth.constant = 49
                cell.v12Line.isHidden = false
                cell.v12NameLbl.isHidden = false
            }else{
                cell.v12ViewWidth.constant = 0
                cell.v12Line.isHidden = true
                cell.v12NameLbl.isHidden = true
            }
        }else{
            cell.storeNameLbl.text = ""
            cell.storeAddressLbl.text = ""
            cell.distanceLbl.text = ""
            cell.statusBtnName.setTitle("", for: .normal)
            cell.statusBtnName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.timeImg.isHidden = true
            cell.timeOneLbl.text = ""
            cell.timeTwoLbl.text = ""
            cell.v12ViewWidth.constant = 0
            cell.v12Line.isHidden = true
            cell.v12NameLbl.isHidden = true
        }
        

        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let storeInfoAction = UIContextualAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Store Info", value: "", table: nil))!) { (action, view, completion) in
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = StoreInfoVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoreInfoVC") as! StoreInfoVC
            obj.storeId = self.filterDetailsArray[indexPath.row].Id
            obj.orderType = self.orderType
            obj.isShiftOpen = self.filterDetailsArray[indexPath.row].isShiftOpen
            var isOrderTypeBusy = false
            if self.filterDetailsArray[indexPath.row].OrderTypeBusy != nil{
                isOrderTypeBusy = self.filterDetailsArray[indexPath.row].OrderTypeBusy!
            }
            obj.isBusy = isOrderTypeBusy
            obj.whereObj = 0
            self.navigationController?.pushViewController(obj, animated: true)
            completion(true)
        }
        let favouriteAction = UIContextualAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Make as a Favourite store", value: "", table: nil))!) { (action, view, completion) in
            if isUserLogIn() == true{
                let dic = ["UserId": UserDef.getUserId(), "StoreId" : self.filterDetailsArray[indexPath.row].Id, "RequestBy":2] as [String : Any]
                OrderModuleServices.GetStoreMakeFavouriteService(dic: dic, success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                        }
                    }else{
                        DispatchQueue.main.async {
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                            }else{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                            }
                            if self.whereObj == 1{
                                self.getStoreListService()
                            }else if self.whereObj != 1 && self.whereObj != 2{
                                self.getStoresService()
                            }
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = LoginVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            completion(true)
        }
        //storeInfoAction.textlabel.textColor = .black
        storeInfoAction.image = UIImage(named: "Alert")
        storeInfoAction.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7529411765, blue: 0.768627451, alpha: 1)
        
        favouriteAction.image = UIImage(named: "security-pin")
        favouriteAction.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7529411765, blue: 0.768627451, alpha: 1)
        if whereObj == 1 ||  whereObj == 2{
            return UISwipeActionsConfiguration(actions: [favouriteAction, storeInfoAction])
        }else{
            return UISwipeActionsConfiguration(actions: [])
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if whereObj == 1 ||  whereObj == 2{
            if filterDetailsArray[indexPath.row].StoreStatus == true{
                var isOrderTypeBusy = false
                if filterDetailsArray[indexPath.row].OrderTypeBusy != nil{
                    isOrderTypeBusy = filterDetailsArray[indexPath.row].OrderTypeBusy!
                }
                if filterDetailsArray[indexPath.row].isShiftOpen == true && isOrderTypeBusy == false || filterDetailsArray[indexPath.row].is24x7! == true && isOrderTypeBusy == false{
                    self.selectStore(sender: indexPath.row, emptyCart: false)
                }
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = StoreInfoVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoreInfoVC") as! StoreInfoVC
            obj.storeId = self.filterDetailsArray[indexPath.row].Id
            obj.isShiftOpen = self.filterDetailsArray[indexPath.row].isShiftOpen
            var isOrderTypeBusy = false
            if filterDetailsArray[indexPath.row].OrderTypeBusy != nil{
                isOrderTypeBusy = filterDetailsArray[indexPath.row].OrderTypeBusy!
            }
            obj.isBusy = isOrderTypeBusy
            obj.whereObj = 1
            if let _ = self.filterDetailsArray[indexPath.row].StoreDistance {
                obj.storeDistance = self.filterDetailsArray[indexPath.row].StoreDistance!
            }else{
                obj.storeDistance = 0
            }
//            var distance = self.filterDetailsArray[indexPath.row].Distance!.replace(string: "KM", replacement: "")
//            distance = distance.replace(string: " ", replacement: "")
//            obj.storeDistance = Double(distance)!
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    func selectStore(sender:Int, emptyCart:Bool){
        if itemId != 0{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = AdditionalsVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
            obj.itemID = itemId
            obj.selectOrderType = orderType
            obj.sectionType = 0
            obj.storeId = self.filterDetailsArray[sender].Id
            obj.selectAddressId = 0
            obj.isV12 = false
            obj.titleLbl = itemTitle
            obj.isBanner = isBanner
            obj.categoryId = categoryId
            obj.categoryName = categoryName
            if AppDelegate.getDelegate().appLanguage == "English"{
                obj.address = self.filterDetailsArray[sender].NameEn
            }else{
                obj.address = self.filterDetailsArray[sender].NameAr
            }
            AppDelegate.getDelegate().cartStoreName = filterDetailsArray[sender].NameEn
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            if categoryId != 0{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let obj:ItemsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ItemsVC") as! ItemsVC
                obj.categoryId = categoryId
                obj.categoryName = categoryName
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.address = self.filterDetailsArray[sender].NameEn
                }else{
                    obj.address = self.filterDetailsArray[sender].NameAr
                }
                obj.storeId = filterDetailsArray[sender].Id
                obj.selectAddressID = 0
                obj.isV12 = false
                obj.selectOrderType = orderType
                obj.isBanner = isBanner
                obj.isStores = true
                AppDelegate.getDelegate().cartStoreName = filterDetailsArray[sender].NameEn
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var obj = MenuVC()
                obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                if AppDelegate.getDelegate().appLanguage == "English"{
                    obj.address = self.filterDetailsArray[sender].NameEn
                }else{
                    obj.address = self.filterDetailsArray[sender].NameAr
                }
                obj.storeID = filterDetailsArray[sender].Id
                obj.emptyCart = emptyCart
                obj.selectOrderType = orderType
                obj.isBanner = isBanner
                obj.categoryId = categoryId
                obj.categoryName = categoryName
                AppDelegate.getDelegate().cartStoreName = filterDetailsArray[sender].NameEn
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
}
//MARK: Collectionview delegate methods
@available(iOS 13.0, *)
extension StoresVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterDetailsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        storesCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoresListCVCell", value: "", table: nil))!, bundle: nil), forCellWithReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoresListCVCell", value: "", table: nil))!)
        let cell = storesCollectionView.dequeueReusableCell(withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "StoresListCVCell", value: "", table: nil))!, for: indexPath) as! StoresListCVCell
        
        if filterDetailsArray.count > 0{
            //Store Status
            var currentDate:Date!
            if filterDetailsArray[indexPath.row].StoreStatus == true{
                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                
                // Timing Details
                if filterDetailsArray[indexPath.row].CurrentDateTime!.contains("."){
                    var time = filterDetailsArray[indexPath.row].CurrentDateTime!
                    time = time.components(separatedBy: ".")[0]
                    currentDate = DateConverts.convertStringToDate(date: time, dateformatType: .dateTtime)
                }else{
                    currentDate = DateConverts.convertStringToDate(date: filterDetailsArray[indexPath.row].CurrentDateTime!, dateformatType: .dateTtime)
                }
                let day:String = DateConverts.convertDateToString(date: currentDate, dateformatType: .EEEE)
                let shifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == day})
                
                if shifts.count > 0{
                    var nextOpenTime = ""
                    let startTime = DateConverts.convertStringToDate(date: shifts[0].StartDateTime, dateformatType: .dateTtime)
                    let endTime = DateConverts.convertStringToDate(date: shifts[0].EndDateTime, dateformatType: .dateTtime)
                    if currentDate! > startTime{
                        if currentDate! > endTime{
                            for i in 1...7{
                                let newDate = Calendar.current.date(byAdding: .day, value: i, to: currentDate)!
                                let day:String = DateConverts.convertDateToString(date: newDate, dateformatType: .EEEE)
                                let nextShifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == day})
                                if nextShifts[0].IsClose == false {
                                    if i == 1 {
                                        nextOpenTime = nextShifts[0].StartTime
                                    }else{
                                        nextOpenTime = "\(nextShifts[0].wkday.prefix(3).capitalized) \(nextShifts[0].StartTime)"
                                    }
                                  break
                                }
                            }
                        }else{
                            nextOpenTime = shifts[0].EndTime
                        }
                    }else{
                        nextOpenTime = shifts[0].StartTime
                    }
                    
                    //Store Timing Functionality
                    var isOrderTypeBusy = false
                    if whereObj == 1 || whereObj == 2{
                        if filterDetailsArray[indexPath.row].OrderTypeBusy != nil{
                            isOrderTypeBusy = filterDetailsArray[indexPath.row].OrderTypeBusy!
                        }
                    }else{
                        if allStoreDetailsArray.count > 0{
                            if allStoreDetailsArray[0].OrderTypes != nil{
                                if allStoreDetailsArray[0].OrderTypes!.count > 0{
                                    let orderTypeArray = allStoreDetailsArray[0].OrderTypes!.filter({$0.isSelect! == true})
                                    if orderTypeArray.count > 0{
                                        let orderBusy = filterDetailsArray[indexPath.row].BusySchedule!.filter({$0.OrderTypeId == orderTypeArray[0].Id})
                                        if orderBusy.count > 0{
                                            isOrderTypeBusy = orderBusy[0].IsBusy
                                        }else{
                                            isOrderTypeBusy = false
                                        }
                                    }else{
                                        isOrderTypeBusy = false
                                    }
                                }
                            }
                        }
                    }
                    if filterDetailsArray[indexPath.row].is24x7! == true{
                        if isOrderTypeBusy == true{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                            //Change by swathi(12-04-2022)
                            //cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                            //cell.timeTwoLbl.text = nextOpenTime
                            if filterDetailsArray[indexPath.row].OpensInMinutes != nil{
                                cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open in", value: "", table: nil))!
                                cell.timeTwoLbl.text = "\(filterDetailsArray[indexPath.row].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)"
                            }else{
                                cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                cell.timeTwoLbl.text = nextOpenTime
                            }
                            cell.timeOneLbl.isHidden = false
                            cell.timeTwoLbl.isHidden = false
                            cell.timeImg.isHidden = true
                        }else{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                            cell.timeOneLbl.isHidden = true
                            cell.timeTwoLbl.isHidden = true
                            cell.timeImg.isHidden = false
                        }
                    }else{
                        cell.timeOneLbl.isHidden = false
                        cell.timeTwoLbl.isHidden = false
                        cell.timeImg.isHidden = true
                        if filterDetailsArray[indexPath.row].isShiftOpen == true{
                            if isOrderTypeBusy == true{
                                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                                //Change by swathi(12-04-2022)
                                //cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                //cell.timeTwoLbl.text = nextOpenTime
                                if filterDetailsArray[indexPath.row].OpensInMinutes != nil{
                                    cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open in", value: "", table: nil))!
                                    cell.timeTwoLbl.text = "\(filterDetailsArray[indexPath.row].OpensInMinutes!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "mins", value: "", table: nil))!)"
                                }else{
                                    cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                                    cell.timeTwoLbl.text = nextOpenTime
                                }
                            }else{
                                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                                //Changed by swathi
                                //cell.timeOneLbl.text = shifts[0].StartTime
                                //cell.timeTwoLbl.text = shifts[0].EndTime
                                if filterDetailsArray[indexPath.row].ShiftBusinessDate != nil{
                                    if filterDetailsArray[indexPath.row].ShiftBusinessDate! != ""{
                                        let dayName = DateConverts.convertStringToStringDates(inputDateStr: filterDetailsArray[indexPath.row].ShiftBusinessDate!, inputDateformatType: .dateR, outputDateformatType: .EEEE)
                                        let daysShifts = filterDetailsArray[indexPath.row].Shifts!.filter({$0.wkday == dayName})
                                        if daysShifts.count > 0{
                                            if daysShifts[0].StartTime.contains("00:00") && daysShifts[0].EndTime.contains("00:00"){
                                                cell.timeOneLbl.text = "12:\(daysShifts[0].StartTime.components(separatedBy: ":")[1])"
                                                cell.timeTwoLbl.text = "12:\(daysShifts[0].EndTime.components(separatedBy: ":")[1])"
                                            }else{
                                                if daysShifts[0].StartTime.contains("00:00"){
                                                    cell.timeOneLbl.text = "12:\(daysShifts[0].StartTime.components(separatedBy: ":")[1])"
                                                    cell.timeTwoLbl.text = daysShifts[0].EndTime
                                                }else if daysShifts[0].EndTime.contains("00:00"){
                                                    cell.timeOneLbl.text = daysShifts[0].StartTime
                                                    cell.timeTwoLbl.text = "12:\(daysShifts[0].EndTime.components(separatedBy: ":")[1])"
                                                }else{
                                                    cell.timeOneLbl.text = daysShifts[0].StartTime
                                                    cell.timeTwoLbl.text = daysShifts[0].EndTime
                                                }
                                            }
                                        }else{
                                            cell.timeOneLbl.text = ""
                                            cell.timeTwoLbl.text = ""
                                        }
                                    }else{
                                        if shifts[0].StartTime.contains("00:00") && shifts[0].EndTime.contains("00:00"){
                                            cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                            cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                        }else{
                                            if shifts[0].StartTime.contains("00:00"){
                                                cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                                cell.timeTwoLbl.text = shifts[0].EndTime
                                            }else if shifts[0].EndTime.contains("00:00"){
                                                cell.timeOneLbl.text = shifts[0].StartTime
                                                cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                            }else{
                                                cell.timeOneLbl.text = shifts[0].StartTime
                                                cell.timeTwoLbl.text = shifts[0].EndTime
                                            }
                                        }
                                    }
                                }else{
                                    if shifts[0].StartTime.contains("00:00") && shifts[0].EndTime.contains("00:00"){
                                        cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                        cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                    }else{
                                        if shifts[0].StartTime.contains("00:00"){
                                            cell.timeOneLbl.text = "12:\(shifts[0].StartTime.components(separatedBy: ":")[1])"
                                            cell.timeTwoLbl.text = shifts[0].EndTime
                                        }else if shifts[0].EndTime.contains("00:00"){
                                            cell.timeOneLbl.text = shifts[0].StartTime
                                            cell.timeTwoLbl.text = "12:\(shifts[0].EndTime.components(separatedBy: ":")[1])"
                                        }else{
                                            cell.timeOneLbl.text = shifts[0].StartTime
                                            cell.timeTwoLbl.text = shifts[0].EndTime
                                        }
                                    }
                                }
                            }
                        }else{
                            cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                            cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
                            cell.timeOneLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Next Open at", value: "", table: nil))!
                            cell.timeTwoLbl.text = nextOpenTime
                        }
                    }
                }else{
                    cell.timeOneLbl.isHidden = false
                    cell.timeTwoLbl.isHidden = false
                    cell.timeImg.isHidden = true
                    cell.timeOneLbl.text = ""
                    cell.timeTwoLbl.text = ""
                }
            }else if filterDetailsArray[indexPath.row].StoreStatus == false{
                cell.statusBtnName.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                cell.statusBtnName.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)

                cell.timeOneLbl.isHidden = false
                cell.timeTwoLbl.isHidden = false
                cell.timeImg.isHidden = true
                cell.timeOneLbl.text = ""
                cell.timeTwoLbl.text = ""
            }
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.storeNameLbl.text = filterDetailsArray[indexPath.row].NameEn
                cell.storeAddressLbl.text = filterDetailsArray[indexPath.row].AddressEn
            }else{
                cell.storeNameLbl.text = filterDetailsArray[indexPath.row].NameAr
                cell.storeAddressLbl.text = filterDetailsArray[indexPath.row].AddressAr
            }
            if filterDetailsArray[indexPath.row].Distance != nil{
                cell.distanceLbl.text = filterDetailsArray[indexPath.row].Distance
            }else{
                cell.distanceLbl.text = ""
            }
    
            cell.shareBtn.tag = indexPath.row
            cell.shareBtn.addTarget(self, action: #selector(shareBtn_Tapped(_:)), for: .touchUpInside)
            cell.callBtn.tag = indexPath.row
            cell.callBtn.addTarget(self, action: #selector(callBtn_Tapped(_:)), for: .touchUpInside)
            cell.viewDetailsBtn.tag = indexPath.row
            cell.viewDetailsBtn.addTarget(self, action: #selector(viewDetailsBtn_Tapped(_:)), for: .touchUpInside)
        }else{
            cell.storeNameLbl.text = ""
            cell.storeAddressLbl.text = ""
            cell.distanceLbl.text = ""
            cell.statusBtnName.setTitle("", for: .normal)
            cell.statusBtnName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.timeImg.isHidden = true
            cell.timeOneLbl.text = ""
            cell.timeTwoLbl.text = ""
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = storesCollectionView.frame.width
        return CGSize(width: width - 90, height: storesCollectionView.frame.height)
    }
    @objc func shareBtn_Tapped(_ sender: UIButton){
        var URLString = "https://www.google.com/maps/search/?api=1&query="
        if self.filterDetailsArray[sender.tag].Latitude != "" && self.filterDetailsArray[sender.tag].Longitude != ""{
            URLString = "https://www.google.com/maps/search/?api=1&query=\(self.filterDetailsArray[sender.tag].Latitude),\(self.filterDetailsArray[sender.tag].Longitude)"
        }
        let url = NSURL(string: URLString)
        var Desc = "\(self.filterDetailsArray[sender.tag].NameEn) , \(self.filterDetailsArray[sender.tag].AddressEn) \(url!)"
        if AppDelegate.getDelegate().appLanguage == "English"{
            Desc = "\(self.filterDetailsArray[sender.tag].NameEn) , \(self.filterDetailsArray[sender.tag].AddressEn) \(url!)"
        }else{
            Desc = "\(self.filterDetailsArray[sender.tag].NameAr) , \(self.filterDetailsArray[sender.tag].AddressAr) \(url!)"
        }
        let shareAll = [Desc] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func callBtn_Tapped(_ sender: UIButton){
        let mobile = filterDetailsArray[sender.tag].MobileNo
        if let url = URL(string: "tel://\(mobile.removeWhitespace())") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @objc func viewDetailsBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = StoreInfoVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoreInfoVC") as! StoreInfoVC
        obj.storeId = self.filterDetailsArray[sender.tag].Id
        obj.isShiftOpen = self.filterDetailsArray[sender.tag].isShiftOpen
        var isOrderTypeBusy = false
        if filterDetailsArray[sender.tag].OrderTypeBusy != nil{
            isOrderTypeBusy = filterDetailsArray[sender.tag].OrderTypeBusy!
        }
        obj.isBusy = isOrderTypeBusy
        obj.whereObj = 1
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        if scrollView == storesCollectionView{
            targetContentOffset.pointee = scrollView.contentOffset
            var indexes = self.storesCollectionView.indexPathsForVisibleItems
            indexes.sort()
            var index = indexes.first!
            if AppDelegate.getDelegate().appLanguage == "English"{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row
                }else{
                    index.row = index.row + 1
                }
                if filterDetailsArray.count > index.row {
                    DispatchQueue.main.async {
                        self.storesCollectionView.scrollToItem(at: index, at: .left, animated: true)
                    }
                }
            }else{
                if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
                    index.row = index.row + 1
                }else{
                    index.row = index.row
                }
                if filterDetailsArray.count > index.row {
                    DispatchQueue.main.async {
                        self.storesCollectionView.scrollToItem(at: index, at: .right, animated: true)
                    }
                }
            }
            
            self.infoWindow.removeFromSuperview()
            DispatchQueue.main.async {
                if index.row <= self.filterDetailsArray.count - 1 && index.row >= 0 {
                    if self.filterDetailsArray[index.row].Latitude != "" && self.filterDetailsArray[index.row].Longitude != ""{
                        let camera = GMSCameraPosition.camera(withLatitude: Double(self.filterDetailsArray[index.row].Latitude)!,longitude:Double(self.filterDetailsArray[index.row].Longitude)!, zoom: 14.0)
                        self.storesMapView.animate(to: camera)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            let location = CLLocationCoordinate2D(latitude: Double(self.filterDetailsArray[index.row].Latitude)!, longitude: Double(self.filterDetailsArray[index.row].Longitude)!)
                            self.infoWindow = StoresVC.instanceFromNib()
                            self.selectStoreId = self.filterDetailsArray[index.row].Id

                            if AppDelegate.getDelegate().appLanguage == "English"{
                                self.selectAddress = self.filterDetailsArray[index.row].NameEn
                                self.infoWindow.storeNameLbl.text = self.filterDetailsArray[index.row].NameEn
                                self.infoWindow.storeAddressLbl.text = self.filterDetailsArray[index.row].AddressEn
                            }else{
                                self.selectAddress = self.filterDetailsArray[index.row].NameAr
                                self.infoWindow.storeNameLbl.text = self.filterDetailsArray[index.row].NameAr
                                self.infoWindow.storeAddressLbl.text = self.filterDetailsArray[index.row].AddressAr
                            }

                            self.infoWindow.storeSelectBtn.tag = 0
                            self.infoWindow.storeSelectBtn.addTarget(self, action: #selector(self.storeSelectBtn_Tapped(_:)), for: .touchUpInside)

                            self.isShiftOpen = self.filterDetailsArray[index.row].isShiftOpen
                            self.is24x7 = self.filterDetailsArray[index.row].is24x7!
                            self.storeStatus = self.filterDetailsArray[index.row].StoreStatus
                            if self.whereObj == 1 || self.whereObj == 2{
                                if self.filterDetailsArray[index.row].OrderTypeBusy != nil{
                                    self.orderTypeBusy = self.filterDetailsArray[index.row].OrderTypeBusy!
                                }else{
                                    self.orderTypeBusy = false
                                }
                            }else{
                                if self.allStoreDetailsArray.count > 0{
                                    if self.allStoreDetailsArray[0].OrderTypes != nil{
                                        if self.allStoreDetailsArray[0].OrderTypes!.count > 0{
                                            let orderTypeArray = self.allStoreDetailsArray[0].OrderTypes!.filter({$0.isSelect! == true})
                                            if orderTypeArray.count > 0{
                                                let orderBusy = self.filterDetailsArray[index.row].BusySchedule!.filter({$0.OrderTypeId == orderTypeArray[0].Id})
                                                if orderBusy.count > 0{
                                                    self.orderTypeBusy = orderBusy[0].IsBusy
                                                }else{
                                                    self.orderTypeBusy = false
                                                }
                                            }else{
                                                self.orderTypeBusy = false
                                            }
                                        }
                                    }
                                }
                            }

                            //Store Status Functionality
                            if self.filterDetailsArray[index.row].is24x7! == true{
                                if self.orderTypeBusy! == true{
                                    self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                                    self.infoWindow.storeStatusBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                                }else{
                                    self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                                    self.infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                                }
                            }else{
                                if self.filterDetailsArray[index.row].StoreStatus == true{
                                    if self.filterDetailsArray[index.row].isShiftOpen == true{
                                        if self.orderTypeBusy! == true{
                                            self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "BUSY", value: "", table: nil))!, for: .normal)
                                            self.infoWindow.storeStatusBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
                                        }else{
                                            self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OPEN", value: "", table: nil))!, for: .normal)
                                            self.infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.6, green: 0.8549019608, blue: 0.9254901961, alpha: 1)
                                        }
                                    }else{
                                        self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                                        self.infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
                                    }
                                }else{
                                    self.infoWindow.storeStatusBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CLOSED", value: "", table: nil))!, for: .normal)
                                    self.infoWindow.storeStatusBtn.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7824793632, alpha: 1)
                                }
                            }
                            self.infoWindow.center = self.storesMapView.projection.point(for: location)
                            self.infoWindow.center.y = self.infoWindow.center.y - 83
                            self.view.addSubview(self.infoWindow)
                        }
                    }
                }
            }
        }
    }
}
@available(iOS 13.0, *)
extension StoresVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("text cleared")
        isSearch = false
        filterDetailsArray.removeAll()
        filterDetailsArray = storeDetailsArray
        storesTableView.reloadData()
        if whereObj != 1 &&  whereObj != 2{
            self.storesCollectionView.reloadData()
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    @objc func searchTextChanged(sender: NSObject){
        isSearch = true
        filterDetailsArray.removeAll()
        //filterDetailsArray = storeDetailsArray.filter({$0.NameEn.lowercased().contains(searchTF.text!.lowercased())})
        filterDetailsArray = storeDetailsArray.filter({$0.NameEn.lowercased().contains(searchTF.text!.lowercased()) || $0.AddressEn.lowercased().contains(searchTF.text!.lowercased())})
        if filterDetailsArray.count > 0{
            storesTableView.reloadData()
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            self.storesMapView.clear()
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
//            for i in 0...self.filterDetailsArray.count-1{
//                if filterDetailsArray[i].StoreType == 2{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }else{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }
//            }
        }else if searchTF.text!.count == 0{
            filterDetailsArray.removeAll()
            filterDetailsArray = storeDetailsArray
            isSearch = false
            storesTableView.reloadData()
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            self.storesMapView.clear()
            if filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
//            for i in 0...self.filterDetailsArray.count-1{
//                if filterDetailsArray[i].StoreType == 2{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "v12")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }else{
//                    createMarker(titleMarker: filterDetailsArray[i].NameEn, iconMarker: UIImage(named: "Group 2 Copy 11")!, latitude: Double(filterDetailsArray[i].Latitude)!, longitude: Double(filterDetailsArray[i].Longitude)!)
//                }
//            }
        }else{
            isSearch = false
            storesTableView.reloadData()
            if whereObj != 1 &&  whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            self.storesMapView.clear()
        }
    }
}
@available(iOS 13.0, *)
extension StoresVC : StoresFilterVCDelegate{
    func didTapAction(dic: [StoresListModel], where:Int) {
        let orderTypes = dic[0].OrderTypes!.filter({$0.isSelect! == true})
        allFilterStoreDetailsArray = dic
        SaveAddressClass.selectFilterDetailsArray = dic
        if whereObj == nil{
            isBack = true
        }
        if whereObj == 1{
            if orderTypes.count > 0{
                if orderType != orderTypes[0].Id{
                    isFilter = true
                    orderType = orderTypes[0].Id
                    self.getStoreListService()
                }else{
                    isFilter = false
                    filterDetailsArray.removeAll()
                    self.filterStores(dic: dic)
                }
            }else{
                isFilter = false
                filterDetailsArray.removeAll()
                self.filterStores(dic: dic)
            }
        }else{
            whereObj = 1
            if orderTypes.count > 0{
                isFilter = true
                orderType = orderTypes[0].Id
                self.getStoreListService()
            }else{
                isFilter = false
                filterDetailsArray.removeAll()
                self.filterStores(dic: dic)
            }
        }
    }
    func filterStores(dic: [StoresListModel]){
        DispatchQueue.main.async {
            let storeTypes = dic[0].StoreTypes!.filter({$0.isSelect! == true})
            if storeTypes.count > 0{
                let data = self.allStoreDetailsArray[0].Data!
                for dic in data {
                    for dic1 in storeTypes {
                        if dic.StoreType == dic1.Id{
                            self.filterDetailsArray.append(dic)
                        }
                    }
                }
            }else{
                 self.filterDetailsArray = self.allStoreDetailsArray[0].Data!
            }
            let facilities = dic[0].Facilities!.filter({$0.isSelect! == true})
            if facilities.count > 0{
                let data = self.filterDetailsArray
                self.filterDetailsArray.removeAll()
                for dic in data {
                        var isAdd : Bool = false
                        var isNoAdd : Bool = true
                        for dic2 in facilities {
                            let Array = dic.Facilities!.filter{$0.Id == dic2.Id}
                            if Array.count > 0 {
                                isAdd = true
                            }else{
                                isNoAdd = false
                            }
                        }
                    if isAdd == true && isNoAdd == true{
                        let storeArray = self.filterDetailsArray.filter{$0.Id == dic.Id}
                        if storeArray.count == 0 {
                            self.filterDetailsArray.append(dic)
                        }
                    }
                }
            }
                        
            let segments = dic[0].Segments!.filter({$0.isSelect! == true})
            if segments.count > 0{
                let data = self.filterDetailsArray
                self.filterDetailsArray.removeAll()
                for dic in data {
                        var isAdd : Bool = false
                        var isNoAdd : Bool = true
                        for dic2 in segments {
                            let Array = dic.Segments!.filter{$0.Id == dic2.Id}
                            if Array.count > 0 {
                                isAdd = true
                            }else{
                                isNoAdd = false
                            }
                        }
                    if isAdd == true && isNoAdd == true{
                        let storeArray = self.filterDetailsArray.filter{$0.Id == dic.Id}
                        if storeArray.count == 0 {
                            self.filterDetailsArray.append(dic)
                        }
                    }
                }
            }
        }
        DispatchQueue.main.async {
            self.storesTableView.reloadData()
            if self.whereObj != 1 &&  self.whereObj != 2{
                self.storesCollectionView.reloadData()
            }
            if self.filterDetailsArray.count > 0 {
                self.storesMapView.clear()
                self.dropMarkers(indepath: 0)
            }
        }
    }
}
//MARK: Hex Color coding
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: NSCharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
//MARK: Hex Color coding
extension UIColor {
    convenience init(hexString: String, Alpha : UInt32) {
        let hex = hexString.trimmingCharacters(in: NSCharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (Alpha, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (Alpha, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (Alpha, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
extension StoresVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String, Status: String) {
        if latitude == 0.0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if Status == "Allow"{
                    //Redirect to Settings app
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                }else if Status == "Back"{
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.locationCheck()
                }
            }
        }else{
            print(MainAddress)
            self.latitude = latitude
            self.longitude = longitude
            DispatchQueue.main.async() {
                self.initGoogleMaps()
            }
        }
    }
}
