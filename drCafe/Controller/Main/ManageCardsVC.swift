//
//  ManageCardsVC.swift
//  drCafe
//
//  Created by Devbox on 11/01/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import SafariServices
@available(iOS 13.0, *)
class ManageCardsVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var addNewCardBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var isSubscription = false
    var orderId = 0
    var subscriptionId = 0
    var cardDetailsArray = [GetSaveCardDetailsModel]()
    //Payment
    var isApplePay:Bool = false
    var isSTCPay:Bool = false
    var isMadaPay:Bool = false
    var isVisa:Bool = true
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    //let provider = OPPPaymentProvider(mode: OPPProviderMode.test)
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.drcafecoffee.payments"
    let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    // MARK: - The payment brands for Ready-to-use UI "APPLEPAY"
    static let checkoutPaymentBrands = ["VISA", "MASTER", "AMEX"]
    static let STCPAYPaymentBrands = ["STC_PAY"]
    static let MADAPAYPaymentBrands = ["MADA"]
    var merchantTransactionId:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
        }else{
            self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
        }
        tableView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        addNewCardBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
        self.getCardDetailsService()
    }
    //MARK: Get Card Details Service
    func getCardDetailsService(){
        let dic:[String:Any] = ["userId":UserDef.getUserId(), "issubscription":"\(isSubscription)"]
        PaymentModuleServices.getAllSaveCardDetailsService(dic: dic) { (data) in
            if(data.Status == false){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message)
            }else{
                self.cardDetailsArray = data.Data!
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        } onError: { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    
    @IBAction func addNewCardBtn_Tapped(_ sender: Any) {
        //ANLoader.showLoading("Loading..", disableUI: true)
        let userDic = getUserDetails()
        merchantTransactionId = "\(userDic.UserId)iOS\(self.TimeStamp())"
        let customerPhone = userDic.Mobile
        let dic:[String:Any] = [
            "amount":0.00,
            "shopperResultUrl": "\(self.urlScheme)://result" ,
            "isCardRegistration": true,
            "merchantTransactionId": merchantTransactionId,
            "customerEmail":userDic.Email,
            "userId":userDic.UserId,
            "isApplePay":false,
            "isSTCPay":false,
            "isMada":false,
            "isVisaMaster":isVisa,
            "customerPhone":customerPhone,
            "customerName":userDic.FullName
            //"notificationUrl":"http://csadms.com/ChefAppAPITest/api/Notify/NewInfo"
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
                }))
            }else{
                DispatchQueue.main.async {
                    self.checkoutID = data.Data![0].id
                    self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                    self.checkoutProvider?.delegate = self
                    self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: nil)
                    }
            }
        }){ (error) in
            //NotificationCenter.default.post(name: .paymentFailed, object: nil)
            //remove notification and write alert swathi
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            
        }
    }
}
@available(iOS 13.0, *)
extension ManageCardsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardDetailsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardsTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardsTVCell", value: "", table: nil))!, for: indexPath) as! ManageCardsTVCell
        
        cell.nameLbl.text = cardDetailsArray[indexPath.row].CardHolder
        cell.cardNumberLbl.text = "**** **** **** \(cardDetailsArray[indexPath.row].LastDigits)"
        cell.expiryDateLbl.text = "\(cardDetailsArray[indexPath.row].ExpireMonth)/\(cardDetailsArray[indexPath.row].ExpireYear)"
        
        if cardDetailsArray[indexPath.row].CardBrand == "VISA"{
            cell.cardTypeImg.image = UIImage(named: "visa")
        }else{
            cell.cardTypeImg.image = UIImage(named: "master")
        }
        
        cell.cardDeleteBtn.tag = indexPath.row
        cell.cardDeleteBtn.addTarget(self, action: #selector(cardDeleteBtn_Tapped(_:)), for: .touchUpInside)
        
        return cell
    }
    @objc func cardDeleteBtn_Tapped(_ sender: UIButton){
        Alert.TwoActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are you sure to delete the card?", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            return
        }), action2: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Yes", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            let dic:[String:Any] = ["userId":UserDef.getUserId(), "registrationId":"\(self.cardDetailsArray[sender.tag].SToken)"]
            PaymentModuleServices.getDeleteCardService(dic: dic) { (data) in
                if(data.Status == false){
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message)
                }else{
                    DispatchQueue.main.async {
                        self.getCardDetailsService()
                    }
                }
            } onError: { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }))
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSubscription == true{
            let dic:[String:Any] = ["OrderId": orderId,"SubscriptionId":"\(subscriptionId)", "CardId":"\(cardDetailsArray[indexPath.row].Id)"]
            CartModuleServices.CardUpdateService(dic: dic) { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } onError: { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
}
@available(iOS 13.0, *)
extension ManageCardsVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        completion(transaction.paymentParams.checkoutID, false)
    }
    // MARK: - Payment helpers
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        self.transaction = transaction
        if transaction.type == .synchronous {
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        } else {
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: error!.localizedDescription, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        //let provider = OPPPaymentProvider.init(mode: .test)
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = OPPCheckoutSettings.init()
        if self.isSTCPay == true{
            checkoutSettings.paymentBrands = ConfirmationVC.STCPAYPaymentBrands
        }else if self.isMadaPay == true {
            checkoutSettings.paymentBrands = ConfirmationVC.MADAPAYPaymentBrands
        }else{
            checkoutSettings.paymentBrands = ConfirmationVC.checkoutPaymentBrands
        }
        checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
        checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
        checkoutSettings.theme.confirmationButtonColor = self.mainColor
        checkoutSettings.theme.accentColor = self.mainColor
        checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
        checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            Alert.OneActionShowAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Failure", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resource path is invalid", value: "", table: nil))!, action1: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OK", value: "", table: nil))!, style: .default, handler: { UIAlertAction in
            }))
            return
        }
        ANLoader.showLoading("", disableUI: true)
        self.transaction = nil
        // self.processingView.startAnimating()
        let dic:[String:Any] = ["resourcePath": resourcePath,
                                "userId":"\(UserDef.getUserId())",
                                "isApplePay":self.isApplePay,
                                "isSTCPay":self.isSTCPay,
                                "isMada":self.isMadaPay,
                                "isVisaMaster" : self.isVisa]
        
        PaymentModuleServices.getAddCardStatusService(dic: dic) { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data.Message)
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                self.getCardDetailsService()
            }
        } onError: { (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
        
    }
    // MARK: - Async payment callback
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: self.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
}
