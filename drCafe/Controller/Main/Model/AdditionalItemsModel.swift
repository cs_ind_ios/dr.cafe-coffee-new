//
//  AdditionalItemsModel.swift
//  drCafe
//
//  Created by Devbox on 29/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

class AdditionalItemsModel {
    
    var MenuName:String!
    var sectionIsExpanded:Bool!
    var itemDetails:[AdditionalItemDetailsModel]!
    
    init(MenuName: String, sectionIsExpanded: Bool, itemDetails: [AdditionalItemDetailsModel]) {
        self.MenuName = MenuName
        self.sectionIsExpanded = sectionIsExpanded
        self.itemDetails = itemDetails
    }
}
class AdditionalItemDetailsModel {

    var ItemName:String!
    var ItemDesc:String!
    var ItemCal:String!
    var ItemPrice:String!
    var addBtnTapped:Bool!
    
    init(ItemName: String, ItemDesc: String, ItemCal: String, ItemPrice: String, addBtnTapped: Bool) {
        self.ItemName = ItemName
        self.ItemDesc = ItemDesc
        self.ItemCal = ItemCal
        self.ItemPrice = ItemPrice
        self.addBtnTapped = addBtnTapped
    }
}
