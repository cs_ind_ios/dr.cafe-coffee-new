//
//  BeverageModel.swift
//  drCafe
//
//  Created by Devbox on 29/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

class BeverageModel {
    
    var MenuName:String!
    var sectionIsExpanded:Bool!
    var types:[BeverageTypeModel]!
    
    init(MenuName: String, sectionIsExpanded: Bool, types: [BeverageTypeModel]) {
        self.MenuName = MenuName
        self.sectionIsExpanded = sectionIsExpanded
        self.types = types
    }
}
class BeverageTypeModel {

    var ItemName:String!
    var ItemCal:String!
    var ItemSelect:Bool!
    var ItemQuantity:String!
    
    init(ItemName: String, ItemCal: String, ItemQuantity: String, ItemSelect: Bool) {
        self.ItemName = ItemName
        self.ItemCal = ItemCal
        self.ItemQuantity = ItemQuantity
        self.ItemSelect = ItemSelect
    }
}

class RecommendedModel {

    var ItemName:String!
    var ItemDesc:String!
    var ItemCal:String!
    var ItemPrice:String!
    var ItemQuantity:Int!
    var addBtnTapped:Bool!
    
    init(ItemName: String, ItemDesc: String, ItemCal: String, ItemPrice: String, addBtnTapped: Bool, ItemQuantity:Int) {
        self.ItemName = ItemName
        self.ItemDesc = ItemDesc
        self.ItemCal = ItemCal
        self.ItemPrice = ItemPrice
        self.addBtnTapped = addBtnTapped
        self.ItemQuantity = ItemQuantity
    }
}
