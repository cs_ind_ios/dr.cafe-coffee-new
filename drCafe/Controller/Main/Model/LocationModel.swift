//
//  LocationModel.swift
//  drCafe
//
//  Created by Devbox on 02/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import Foundation

class LocationModel {
    
    var latitude:Double!
    var longitude:Double!
    var storeName:String!
    var storeAddress:String!
    var storeStatus:String!
    var storeStatusBGColor:String!
    var storeStatusLblColor:String!
    
    init(latitude: Double, longitude: Double, storeName: String, storeAddress:String, storeStatus:String!, storeStatusBGColor:String!, storeStatusLblColor:String!) {
        self.latitude = latitude
        self.longitude = longitude
        self.storeName = storeName
        self.storeAddress = storeAddress
        self.storeStatus = storeStatus
        self.storeStatusBGColor = storeStatusBGColor
        self.storeStatusLblColor = storeStatusLblColor
    }
}
