//
//  PlayerView.swift
//  alfanar
//
//  Created by Creative Solutions on 9/1/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
class PlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self;
    }
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer;
    }
    var player: AVPlayer? {
        get {
            return playerLayer.player;
        }
        set {
            playerLayer.player = newValue;
        }
    }
}
