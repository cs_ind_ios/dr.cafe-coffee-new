//
//  RewardVC.swift
//  drCafe
//
//  Created by Devbox on 03/08/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class RewardVC: UIViewController {

    @IBOutlet weak var navigationTitleLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var dicLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var codeNameLbl: UILabel!
    @IBOutlet weak var promocodeLbl: UILabel!
    @IBOutlet weak var durationNameLbl: UILabel!
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var couponLimitNameLbl: UILabel!
    @IBOutlet weak var couponAmountLbl: UILabel!
    @IBOutlet weak var paymentMethodNameLbl: UILabel!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var sizeNameLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var expireInNameLbl: UILabel!
    @IBOutlet weak var expireInLbl: UILabel!
    @IBOutlet weak var discountNameLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var orderTypeNameLbl: UILabel!
    @IBOutlet weak var orderTypeLbl: UILabel!
    
    @IBOutlet weak var validDaysNameLbl: UILabel!
    @IBOutlet weak var validDaysLbl: UILabel!
    
    @IBOutlet weak var locationNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var termsAndConditionsNameLbl: UILabel!
    @IBOutlet weak var termsAndConditionsOneLbl: UILabel!
    @IBOutlet weak var termsAndConditionsTwoLbl: UILabel!
    
    @IBOutlet weak var redeemBtn: UIButton!
    @IBOutlet weak var redeemBtnHeight: NSLayoutConstraint!
    
    var detailsArray = [DCSmilePromotionsDetailsModel]()
    var expireCoupon = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        navigationTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Details", value: "", table: nil))!
        codeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Code", value: "", table: nil))!
        sizeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Size", value: "", table: nil))!
        durationNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Duration", value: "", table: nil))!
        expireInNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expire in", value: "", table: nil))!
        couponLimitNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Per coupon limit", value: "", table: nil))!
        discountNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!
        paymentMethodNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Method's", value: "", table: nil))!
        orderTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Type's", value: "", table: nil))!
        validDaysNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Valid Days", value: "", table: nil))!
        locationNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location", value: "", table: nil))!
        termsAndConditionsNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Term & Condition", value: "", table: nil))!
        redeemBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Redeem this coupon at order confirmation", value: "", table: nil))!, for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        //Theme Color
        self.myScrollView.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            redeemBtn.backgroundColor = .darkGray
            redeemBtn.setTitleColor(.white, for: .normal)
        } else {
            redeemBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            redeemBtn.setTitleColor(.white, for: .normal)
        }
        
        if expireCoupon == true{
            redeemBtnHeight.constant = 0
        }else{
            redeemBtnHeight.constant = 55
        }
        
        if detailsArray.count > 0{
            titleLbl.text = detailsArray[0].PromoCode
            promocodeLbl.text = detailsArray[0].PromoCode
            //expireInLbl.text = "\(String(describing: detailsArray[0].ValidTill)) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "days", value: "", table: nil))!)"
            if detailsArray[0].StartDate != "" && detailsArray[0].EndDate != ""{
                let startDate = detailsArray[0].StartDate.components(separatedBy: ".")[0]
                let endDate = detailsArray[0].EndDate.components(separatedBy: ".")[0]
                let voucherEndDate = DateConverts.convertStringToDate(date: endDate, dateformatType: .dateTtime)
                let todayDate = DateConverts.convertDateToDateFormate(date: Date(), dateformatType: .dateTtime)
                if voucherEndDate >= todayDate{
                    let diffInDays = Calendar.current.dateComponents([.day], from: Date(), to: DateConverts.convertStringToDate(date: endDate, dateformatType: .dateTtime)).day
                    expireInLbl.text = "\(diffInDays!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "days", value: "", table: nil))!)"
                }else{
                    expireInLbl.text = "0 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "days", value: "", table: nil))!)"
                }
                fromDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: startDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) / \(detailsArray[0].StartTime)"
                toDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: endDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) / \(detailsArray[0].EndTime)"
            }else{
                if detailsArray[0].StartDate != "" {
                    let startDate = detailsArray[0].StartDate.components(separatedBy: ".")[0]
                    fromDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: startDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) / \(detailsArray[0].StartTime)"
                }else{
                    fromDateLbl.text = ""
                }
                if detailsArray[0].EndDate != ""{
                    let endDate = detailsArray[0].EndDate.components(separatedBy: ".")[0]
                    toDateLbl.text = "\(DateConverts.convertStringToStringDates(inputDateStr: endDate, inputDateformatType: .dateTtime, outputDateformatType: .date)) / \(detailsArray[0].EndTime)"
                    let voucherEndDate = DateConverts.convertStringToDate(date: endDate, dateformatType: .dateTtime)
                    let todayDate = DateConverts.convertDateToDateFormate(date: Date(), dateformatType: .dateTtime)
                    if voucherEndDate >= todayDate{
                        let diffInDays = Calendar.current.dateComponents([.day], from: Date(), to: DateConverts.convertStringToDate(date: endDate, dateformatType: .dateTtime)).day
                        expireInLbl.text = "\(diffInDays!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "days", value: "", table: nil))!)"
                    }else{
                        expireInLbl.text = "0 \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "days", value: "", table: nil))!)"
                    }
                }else{
                    toDateLbl.text = ""
                    expireInLbl.text = ""
                }
            }
            
            if detailsArray[0].DiscountType == 1{
                discountLbl.text = "\(detailsArray[0].DiscountValue.withDecimal()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            }else{
                discountLbl.text = "\(detailsArray[0].DiscountValue.withDecimal())%"
            }
            couponAmountLbl.text = "\(detailsArray[0].MaxAmt.withDecimal()) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)"
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                dicLbl.text = detailsArray[0].DescriptionEn
                termsAndConditionsOneLbl.text = detailsArray[0].MessageEn
            }else{
                dicLbl.text = detailsArray[0].DescriptionAr
                termsAndConditionsOneLbl.text = detailsArray[0].MessageAr
            }
            
            //Image
            var ImgStr:NSString!
            if AppDelegate.getDelegate().appLanguage == "English"{
                ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(detailsArray[0].ImageEn)" as NSString
            }else{
                ImgStr = "\(AppDelegate.getDelegate().imagePathURL)\(detailsArray[0].ImageAr)" as NSString
            }
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let url = URL.init(string: urlStr as String)
            img.kf.setImage(with: url)
            
            //Size
            if detailsArray[0].Sizes!.count > 0{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    sizeLbl.text = "\(detailsArray[0].Sizes![0].NameEn)"
                }else{
                    sizeLbl.text = "\(detailsArray[0].Sizes![0].NameAr)"
                }
            }else{
                sizeLbl.text = ""
            }
            //Order Type
            if detailsArray[0].OrderTypes!.count > 0{
                var orderTypes:[String] = [""]
                for i in 0...detailsArray[0].OrderTypes!.count-1{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        orderTypes.append(detailsArray[0].OrderTypes![i].NameEn)
                    }else{
                        orderTypes.append(detailsArray[0].OrderTypes![i].NameAr)
                    }
                }
                orderTypes.remove(at: 0)
                if orderTypes.contains("Delivery") &&  orderTypes.contains("PickUp") && orderTypes.contains("DineIn"){
                    orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All Order Types", value: "", table: nil))!
                }else{
                    orderTypeLbl.text = "\(orderTypes.joined(separator: ","))"
                }
            }else{
                orderTypeLbl.text = ""
            }
            //Payment Details
            if detailsArray[0].PaymentMethods!.count > 0{
                var paymentMethods:[String] = [""]
                for i in 0...detailsArray[0].PaymentMethods!.count-1{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        paymentMethods.append(detailsArray[0].PaymentMethods![i].NameEn)
                    }else{
                        paymentMethods.append(detailsArray[0].PaymentMethods![i].NameAr)
                    }
                }
                paymentMethods.remove(at: 0)
                paymentMethodLbl.text = "\(paymentMethods.joined(separator: ","))"
            }else{
                paymentMethodLbl.text = ""
            }
            // Valid days
            //Payment Details
            if detailsArray[0].ValidDays!.count > 0{
                var validDays:[String] = [""]
                for i in 0...detailsArray[0].ValidDays!.count-1{
                    validDays.append(WeekDaysModel.weekDayName(Num: detailsArray[0].ValidDays![i].WeekDay))
                }
                validDays.remove(at: 0)
                if validDays.count == 7{
                    validDaysLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All Days", value: "", table: nil))!
                }else{
                    validDaysLbl.text = "\(validDays.joined(separator: ","))"
                }
            }else{
                paymentMethodLbl.text = ""
            }
            
            //Store Details
            if detailsArray[0].StoresType == 1{
                locationLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All Stores", value: "", table: nil))!
            }else{
                if detailsArray[0].StoreList!.count > 0{
                    var location:[String] = [""]
                    for i in 0...detailsArray[0].StoreList!.count-1{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            location.append(detailsArray[0].StoreList![i].NameEn)
                        }else{
                            location.append(detailsArray[0].StoreList![i].NameAr)
                        }
                    }
                    location.remove(at: 0)
                    locationLbl.text = "\(location.joined(separator: ","))"
                }else{
                    locationLbl.text = ""
                }
            }
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func redeemBtn_Tapped(_ sender: Any) {
        //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "")
    }
}
