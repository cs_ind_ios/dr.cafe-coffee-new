//
//  ManageAddressVC.swift
//  drCafe
//
//  Created by Devbox on 08/06/20.
//  Copyright © 2020 Devbox. All rights reserved.
//

import UIKit

protocol ManageAddressVCDelegate {
    func didSelectAddress()
}

@available(iOS 13.0, *)
class ManageAddressVC: UIViewController{

    //MARK: Outlets
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAddressBtn: UIButton!
    @IBOutlet weak var noRecordsFoundLbl: UILabel!
    
    var manageAddressVCDelegate:ManageAddressVCDelegate!
    
    var whereObj:Int!
    var addressListArray = [AddressDetailsModel]()
    var storeId = 0
    var sectionType = 0
    var storeLatitude:String!
    var storeLongitude:String!
    var orderId = 0
    var isConfirmation = false
    var isOrderType = false
    var tableViewHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 180
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.getDelegate().selectBGColor == "#000000" || AppDelegate.getDelegate().selectColorname == "Black"{
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            addAddressBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            addAddressBtn.setTitleColor(.black, for: .normal)
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "back white"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr white"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .white
            noRecordsFoundLbl.textColor = .white
        } else {
            // User Interface is Light
            self.view.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectBGColor)")
            addAddressBtn.backgroundColor = UIColor(hexString: "#\(AppDelegate.getDelegate().selectColor)")
            addAddressBtn.setTitleColor(.white, for: .normal)
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.backBtn.setImage(UIImage(named: "Back"), for: .normal)
            }else{
                self.backBtn.setImage(UIImage(named: "backAr"), for: .normal)
            }
            navigationBarTitleLbl.textColor = .black
            noRecordsFoundLbl.textColor = .darkGray
        }
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manage Address", value: "", table: nil))!
        addAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add", value: "", table: nil))!, for: .normal)
        noRecordsFoundLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "No Records Found", value: "", table: nil))!
        
        if isUserLogIn() == true{
            self.getDetailsService()
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = LoginVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    //MARK: Get Details Service
    func getDetailsService(){
        let dic:[String:Any]!
        if whereObj == 1 || whereObj == 10{
            dic = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId" : 0, "SectionType": 0]
        }else{
            if sectionType == 1{
                dic = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId" : storeId, "SectionType": sectionType]
            }else{
                dic = ["UserId": UserDef.getUserId(), "RequestBy":2, "StoreId" : 0, "SectionType": 0]
            }
        }
        OrderModuleServices.AddressListService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                ANLoader.hide()
                self.addressListArray = data.Data!
                if data.Data!.count > 0{
                    self.noRecordsFoundLbl.isHidden = true
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else{
                    self.noRecordsFoundLbl.isHidden = false
                    self.tableView.isHidden = true
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    //MARK: Back Button Action
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if isOrderType == true{
            LoginVC.instance.whereObj = 1
            self.navigationController?.popViewController(animated: false)
        }else if whereObj == 11{
            self.navigationController?.popToRootViewController(animated: false)
            tabBarController?.tabBar.isHidden = false
            tabBarController?.selectedIndex = 4
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: Add Button Action
    @IBAction func addBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var Obj = AddAddressVC()
        Obj = mainStoryBoard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        Obj.isNewUser = true
        if whereObj == 2{
            Obj.storeLatitude = storeLatitude!
            Obj.storeLongitude = storeLongitude!
            Obj.whereObj = whereObj!
        }
        self.navigationController?.pushViewController(Obj, animated: true)
    }
}
//MARK: TableView Delegate Methods
@available(iOS 13.0, *)
extension ManageAddressVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressListArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //cellHeights[indexPath] = cell.frame.size.height
        print(cell.frame.size.height, self.tableViewHeight)
        self.tableViewHeight += cell.frame.size.height
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!)
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!, for: indexPath) as! ManageAddressTVCell
        
        cell.addressTypeLbl.text = addressListArray[indexPath.row].HouseName
        if addressListArray[indexPath.row].Default == 1{
            cell.lineLbl.isHidden = false
            cell.defaultLbl.isHidden = false
        }else{
            cell.lineLbl.isHidden = true
            cell.defaultLbl.isHidden = true
        }
        
        //Address Field check and Address Display
        var address = ""
        if addressListArray[indexPath.row].HouseNo != ""{
            address = addressListArray[indexPath.row].HouseNo
        }
        if addressListArray[indexPath.row].LandMark != ""{
            if address != ""{
                address = "\(addressListArray[indexPath.row].HouseNo), \(addressListArray[indexPath.row].LandMark)"
            }else{
                address = "\(addressListArray[indexPath.row].LandMark)"
            }
        }
        var contactDetails = ""
        if addressListArray[indexPath.row].ContactPerson != "" {
            contactDetails = "\(addressListArray[indexPath.row].ContactPerson): \(addressListArray[indexPath.row].ContactNo)"
        }
        
        if address != ""{
            if contactDetails != ""{
                cell.addressLbl.text = "\(address), \(addressListArray[indexPath.row].Address) \n\(contactDetails)"
            }else{
                cell.addressLbl.text = "\(address), \(addressListArray[indexPath.row].Address)"
            }
        }else{
            if contactDetails != ""{
                cell.addressLbl.text = "\(addressListArray[indexPath.row].Address) \n\(contactDetails)"
            }else{
                cell.addressLbl.text = "\(addressListArray[indexPath.row].Address)"
            }
        }
        //cell.addressLbl.text = addressListArray[indexPath.row].Address
        cell.addressLbl.layoutIfNeeded()
        cell.addressBGView.layoutIfNeeded()
        
        cell.defaultLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Default", value: "", table: nil))!
        
        cell.addressLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.3215686275, blue: 0.368627451, alpha: 1)
        cell.addressTypeLbl.textColor = #colorLiteral(red: 0.2549019608, green: 0.3215686275, blue: 0.368627451, alpha: 1)
        cell.defaultLbl.textColor = #colorLiteral(red: 0, green: 0.07450980392, blue: 0.368627451, alpha: 1)
        cell.editBtn.setImage(UIImage(named: "note"), for: .normal)
        cell.deleteBtn.setImage(UIImage(named: "delete"), for: .normal)
        cell.BGView.backgroundColor = .white
        cell.addressBGView.backgroundColor = .white
        cell.btnBGView[0].backgroundColor = .white
        cell.btnBGView[1].backgroundColor = .white
        
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editBtn_Tapped(_:)), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtn_Tapped(_:)), for: .touchUpInside)

        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if whereObj! == 123 {
            let dic:[String:Any] = ["UserId": UserDef.getUserId(), "RequestBy":2, "OrderId" : orderId, "AddressId": addressListArray[indexPath.row].Id]
            UserModuleServices.updateOrderAddressService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                    }
                }else{
                    ANLoader.hide()
                    if self.isConfirmation == true{
                        self.manageAddressVCDelegate.didSelectAddress()
                    }
                    self.navigationController?.popViewController(animated: false)
                }
            }) { (error) in
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
            return
        }else if whereObj == 10{
            print("Select")
            return
        }
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        obj.whereObj = whereObj!
        obj.sectionType = sectionType
        obj.storeID = storeId
        obj.addressDetailsArray = [addressListArray[indexPath.row]]
        obj.selectOrderType = 4
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func editBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var Obj = AddAddressVC()
        Obj = mainStoryBoard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        Obj.isNewUser = false
        Obj.Id = addressListArray[sender.tag].Id
        self.navigationController?.pushViewController(Obj, animated: true)
    }
    @objc func deleteBtn_Tapped(_ sender: UIButton){
        let dic:[String:Any] = ["Id" : addressListArray[sender.tag].Id,"UserId": UserDef.getUserId(), "RequestBy":2]
        OrderModuleServices.AddressDeleteService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageEn)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    self.getDetailsService()
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
