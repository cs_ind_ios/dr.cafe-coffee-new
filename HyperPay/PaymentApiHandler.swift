//
//  PaymentApiHandler.swift
//  drCafe
//
//  Created by Creative Solutions on 3/15/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class PaymentApiHandler {
    class func Request(url:String,params:Parameters,method:HTTPMethod,success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            ANLoader.showLoading("", disableUI: true)
            //let headers: HTTPHeaders = [:]
            let headers: HTTPHeaders = ["Authorization":getJWT(),"Content-Type":"application/json"]

            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
            print(params)
            print(url)
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                print(response.response!.statusCode)
                if response.response!.statusCode == 401 {
                    let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
                    JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: true, isUIDisable: true) { (result) in
                            success(result)
                    } onError: { error in
                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                    }
                }else{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
                if let _ = response.data{
                    success(response.data!)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
                }
                //success(response.data!)
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                ANLoader.hide()
            }
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
        }
    }
    
    class func RequestOptions(url:String, params:Parameters, method:HTTPMethod, isLoader:Bool, isUIDisable:Bool, success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            if isLoader == true {
                ANLoader.showLoading("", disableUI: isUIDisable)
            }
           // let headers: HTTPHeaders = [:]
            let headers: HTTPHeaders = ["Authorization":getJWT(),"Content-Type":"application/json"]
            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
           print(params)
            print(url)
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                print(response.response!.statusCode)
                if response.response!.statusCode == 401 {
                    let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
                    JWTHandler.Request(mainUrl: escapedString, params: params, method: method, isLoader: isLoader, isUIDisable: isUIDisable) { (result) in
                            success(result)
                    } onError: { error in
                        onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                    }
                }else{
                
                if isLoader == true {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        ANLoader.hide()
                    }
                }
                //success(response.data!)
                if let _ = response.data{
                    success(response.data!)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
                }
            }
        }else{
            if isLoader == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    ANLoader.hide()
                }
            }
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
        }
    }
    
}
