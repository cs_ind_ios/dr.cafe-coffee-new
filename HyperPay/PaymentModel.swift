//
//  PaymentModel.swift
//  drCafe
//
//  Created by Creative Solutions on 3/15/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
struct GetCheckOutModel : Decodable {
    var Status : Bool
    var Message : String
    var Data : [GetDataCheckOutModel]?
}
struct GetDataCheckOutModel : Decodable {
    var id : String
}

// Payment Status
struct GetPaymentStatusModel : Decodable {
    var Status : Bool
    var Message : String
    var Data : [GetPaymentStatusDataModel]?
}
struct GetPaymentStatusDataModel : Decodable {
    var customPayment : String
    //var registrationId : String?
    //var paymentBrand : String
    //var card : GetCardModel?
}
struct GetCardModel : Decodable {
    var last4Digits : String
    var holder : String
    var expiryMonth : String
    var expiryYear : String

}
struct GetSaveCardResponseModel : Decodable {
    var StatusCode : String
    var StatusMessage : String
}


struct GetSaveCardModel : Decodable {
    var Status : Bool
    var Message : String
    var Data : [GetSaveCardDetailsModel]?
}
struct GetSaveCardDetailsModel : Decodable {
    var Id : Int
    var SToken : String
    var LastDigits : String
    var CardHolder : String
    var ExpireMonth : String
    var ExpireYear : String
    var CardBrand : String
}


struct GetDeleteCardModel : Decodable {
    var Status : Bool
    var Message : String
}


// Add Card Status
struct GetAddCardStatusModel : Decodable {
    var Status : Bool
    var Message : String
    //var Data : [GetAddCardStatusDataModel]?
}
struct GetAddCardStatusDataModel : Decodable {
    var JArray : [GetJArrayDataModel]
}
struct GetJArrayDataModel : Decodable {
    var Key : String
    var Value : String
}
