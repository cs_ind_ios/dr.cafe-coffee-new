//
//  PaymentApiUrls.swift
//  drCafe
//
//  Created by Creative Solutions on 3/15/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
import Alamofire

//Test
//let PaymentBaseURL = "http://csadms.com/dcwebapi/"
//let PaymentBaseURL = "http://ircfood.com/foodappPayments/"
//let PaymentBaseURL = "https://drcafestore.azurewebsites.net/api/"

//Live
//let PaymentBaseURL = "https://drcafeapi.azurewebsites.net/"
//let PaymentBaseURL = "https://drcafeapi.azurewebsites.net/v2.1/"
let PaymentBaseURL = "https://drcafeapi.azurewebsites.net/v2.2/"



enum PaymentApiUrls: String{
    case getPaymentCheckOutService = "api/HyperpayAPI/CheckoutRequestFromSDK"
    case getPaymentResourcePathService = "api/HyperpayAPI/paymentStatus"
    case getDeleteCardService = "api/HyperpayAPI/deleteCard"
    case getAddCardStatusService = "api/HyperpayAPI/AddCardStatus"
    case getSaveCardService = "api/HyperpayAPI/InsertCreditCardDetails"
    case getAllSaveCardDetailsService = "api/HyperpayAPI/GetCreditCardDetails"
    func path() ->String { return PaymentBaseURL + self.rawValue }
}
