//
//  PaymentModuleServices.swift
//  drCafe
//
//  Created by Creative Solutions on 3/15/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import Foundation
@available(iOS 13.0, *)
class PaymentModuleServices{
class func getPaymentCheckOutService(dic:[String:Any],success: @escaping (_ result:GetCheckOutModel) -> Void, onError: @escaping (_ error:String) -> Void){
    let param = dic
    PaymentApiHandler.Request(url: PaymentApiUrls.getPaymentCheckOutService.path(), params: param, method: .post, success: { (result) in
        do{
            let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
            do{
                let result1 = try JSONDecoder().decode(GetCheckOutModel.self, from: result)
                success(result1)
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)

            }
        }catch{
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)

        }
        
    }) { (error) in
        onError(error)
    }
}
    
    class func getPaymentStatusService(dic:[String:Any],success: @escaping (_ result:GetPaymentStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.Request(url: PaymentApiUrls.getPaymentResourcePathService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetPaymentStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    class func getPaymentStatusServiceWithOptions(dic:[String:Any],isLoader:Bool, isUIDisable:Bool, success: @escaping (_ result:GetPaymentStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.RequestOptions(url: PaymentApiUrls.getPaymentResourcePathService.path(), params: param, method: .post, isLoader: isLoader, isUIDisable: isUIDisable, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetPaymentStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    
    class func insertSaveCardService(dic:[String:Any],success: @escaping (_ result:GetSaveCardResponseModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.RequestOptions(url:PaymentApiUrls.getSaveCardService.path(), params: param, method: .post, isLoader: false, isUIDisable: true, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetSaveCardResponseModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    
    class func getAllSaveCardDetailsService(dic:[String:Any],success: @escaping (_ result:GetSaveCardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.Request(url: PaymentApiUrls.getAllSaveCardDetailsService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetSaveCardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)

                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func getDeleteCardService(dic:[String:Any],success: @escaping (_ result:GetDeleteCardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.Request(url: PaymentApiUrls.getDeleteCardService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetDeleteCardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)

                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func getAddCardStatusService(dic:[String:Any],success: @escaping (_ result:GetAddCardStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        PaymentApiHandler.Request(url: PaymentApiUrls.getAddCardStatusService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetAddCardStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)

                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Something went wrong, try again after sometime.", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
}
