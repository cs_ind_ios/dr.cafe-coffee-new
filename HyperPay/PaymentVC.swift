//
//  PaymentVC.swift
//  drCafe
//
//  Created by Creative Solutions on 3/15/21.
//  Copyright © 2021 Devbox. All rights reserved.
//

import UIKit
import SafariServices

@available(iOS 13.0, *)
class PaymentVC: UIViewController, OPPCheckoutProviderDelegate {
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.drcafecoffee.payments"
    let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    
    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    var isMada:Bool = false
    var isApplePay:Bool = false
    var isSTCPay:Bool = false
    var isVisaMaster:Bool = false
    var isSubscription = false

    //@objc var amount:Double = 0.0
    @objc var amount = Double()
    static var instance: PaymentVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        PaymentVC.instance = self

       // print(amount)
        isMada = false
        isApplePay = true
        isSTCPay = false
        isVisaMaster = false
        isSubscription = false
        self.GetCheckOutIdPreparationMethod(amount: amount)
    }
    
    func getUserId() -> String{
        return UserDefaults.standard.value(forKey: "UserId") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "UserId")!)"
    }
//    func getMobileNumber() -> String{
//        return UserDefaults.standard.value(forKey: "Mobile") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "Mobile")!)"
//    }
//    func getSTCMobileNumber() -> String{
//        return UserDefaults.standard.value(forKey: "STCMobile") == nil ? "0" : "\(UserDefaults.standard.value(forKey: "STCMobile")!)"
//    }
    func getEmailID() -> String{
        return UserDefaults.standard.value(forKey: "Email") == nil ? "" : "\(UserDefaults.standard.value(forKey: "Email")!)"
    }
    func getUserName() -> String{
        return UserDefaults.standard.value(forKey: "FullName") == nil ? "" : "\(UserDefaults.standard.value(forKey: "FullName")!)"
    }
    
    func getInvoiceNo() -> String{
        return UserDefaults.standard.value(forKey: "InvoiceNo") == nil ? "" : "\(UserDefaults.standard.value(forKey: "InvoiceNo")!)"
    }
    func getOrderDate() -> String{
        return UserDefaults.standard.value(forKey: "OrderDate") == nil ? "" : "\(UserDefaults.standard.value(forKey: "OrderDate")!)"
    }
    func getExpectedDate() -> String{
        return UserDefaults.standard.value(forKey: "ExpectedDate") == nil ? "" : "\(UserDefaults.standard.value(forKey: "ExpectedDate")!)"
    }
    @IBAction func checkoutButtonAction(_ sender: Double) {
        isMada = false
        isApplePay = false
        isSTCPay = false
        self.GetCheckOutIdPreparationMethod(amount: sender)
    }
    func onlinePaymen(amount:Double) {
        isMada = false
        isApplePay = false
        isSTCPay = false
        self.GetCheckOutIdPreparationMethod(amount: amount)
    }
    @IBAction func checkout_isMada(_ sender: Double) {
        isMada = true
        isApplePay = false
        isSTCPay = false
        self.GetCheckOutIdPreparationMethod(amount: sender)
    }
    @IBAction func checkout_isApplePay(_ sender: Double) {
        isMada = false
        isApplePay = true
        isSTCPay = false
        self.GetCheckOutIdPreparationMethod(amount: sender)
    }
    @IBAction func checkout_isSTCPay(_ sender: Double) {
        isMada = false
        isApplePay = false
        isSTCPay = true
        self.GetCheckOutIdPreparationMethod(amount: sender)
    }
    func GetCheckOutIdPreparationMethod(amount:Double)  {
       // let merchantTransactionId = "\(getUserId())iOS\(self.TimeStamp())"
        let userDic = getUserDetails()
        var customerPhone = userDic.Mobile
        if self.isSTCPay == true{
            customerPhone = getSTCMobileNumber()
        }
        let dic:[String:Any] = [
            "amount":String(format: "%.2f", amount),
            "shopperResultUrl": "\(self.urlScheme)://result" ,
            "isCardRegistration": "false",
            "merchantTransactionId":getInvoiceNo(),
            "customerEmail":userDic.Email,
            "userId":userDic.UserId,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "isMada":isMada,
            "isVisaMaster":isVisaMaster,
            "customerPhone":customerPhone,
            "customerName":userDic.FullName,
            "IsSubscription":isSubscription
            //"notificationUrl":"http://csadms.com/ChefAppAPITest/api/Notify/NewInfo"
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                NotificationCenter.default.post(name: .paymentFailed, object: nil)
                if self.isApplePay == true {
                    self.dismiss(animated: false, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                    print(data.Data![0].id)
                    self.checkoutID = data.Data![0].id
                    if self.isApplePay == true {
                        let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.drcafecoffee", countryCode: "SA")
                        request.currencyCode = "SAR"
                        let amount = NSDecimalNumber(value: amount)
                        print("ApplePay:\(amount)")
                        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "dr.Cafe Coffee", amount: amount)]
                        if OPPPaymentProvider.canSubmitPaymentRequest(request) {
                            if let vc = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                            }else{
                                NotificationCenter.default.post(name: .paymentFailed, object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                      }
                    }else{
                    let checkoutSettings = OPPCheckoutSettings()
                    if self.isMada == true {
                        checkoutSettings.paymentBrands = ["MADA"]
                    }else if self.isSTCPay == true {
                        checkoutSettings.paymentBrands = ["STC_PAY"]
                    }else{
                        checkoutSettings.paymentBrands = ["VISA", "MASTER"]
                    }
                    checkoutSettings.storePaymentDetails = .prompt
                    checkoutSettings.skipCVV = .never
                    checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
                    checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
                    checkoutSettings.theme.confirmationButtonColor = self.mainColor
                    checkoutSettings.theme.accentColor = self.mainColor
                    checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
                    checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
                    
                    /*   if #available(iOS 11.0, *) {
                     paymentRequest.requiredShippingContactFields = Set([PKContactField.postalAddress])
                     } else {
                     paymentRequest.requiredShippingAddressFields = .postalAddress
                     } */
                    
                    self.checkoutProvider = OPPCheckoutProvider(paymentProvider: self.provider, checkoutID: self.checkoutID, settings: checkoutSettings)!
                    self.checkoutProvider?.delegate = self
                    self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                        guard let transaction = transaction else {
                            // Handle invalid transaction, check error
                            //print(error.debugDescription)
                            NotificationCenter.default.post(name: .paymentFailed, object: nil)
                            return
                        }
                        self.transaction = transaction
                        if transaction.type == .synchronous {
                            // If a transaction is synchronous, just request the payment status
                            // You can use transaction.resourcePath or just checkout ID to do it
                            self.getstatus(transaction: self.transaction!)
                            
                        } else if transaction.type == .asynchronous {
                            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
                            /* self.safariVC = SFSafariViewController(url: self.transaction!.redirectURL!)
                             self.safariVC?.delegate = self;
                             self.present(self.safariVC!, animated: true, completion: nil)
                             */
                        }else{
                            // Executed in case of failure of the transaction for any reason
                            //print(self.transaction.debugDescription)
                            NotificationCenter.default.post(name: .paymentFailed, object: nil)
                        }
                    }, cancelHandler: {
                        // Executed if the shopper closes the payment page prematurely
                        print(self.transaction.debugDescription)
                        NotificationCenter.default.post(name: .paymentFailed, object: nil)
                    })
                    }
                }
            }
        }){ (error) in
            NotificationCenter.default.post(name: .paymentFailed, object: nil)
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    func getstatus(transaction:OPPTransaction) {
        NotificationCenter.default.post(name: .ExpectedTime, object: nil)
        let dic:[String:Any] = [
            "resourcePath": transaction.resourcePath!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, "userId":"\(getUserId())",
            "isMada":isMada,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "InvoiceNo":"\(getInvoiceNo())",
            "OrderDate":"\(getOrderDate())",
            "ExpectedDate":"\(getExpectedDate())",
            "OrderStatus":"New"
        ]
       // print("\(transaction.resourcePath!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)")
        PaymentModuleServices.getPaymentStatusService(dic: dic, success: { (data) in
            if(data.Status == false){
                NotificationCenter.default.post(name: .paymentFailed, object: nil)
                if self.isApplePay == true {
                    self.dismiss(animated: false, completion: nil)
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .paymentSuccess, object: nil)
                        if self.isApplePay == true {
                            self.dismiss(animated: false, completion: nil)
                        }
                    }
                }else{
                    NotificationCenter.default.post(name: .paymentFailed, object: nil)
                    if self.isApplePay == true {
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
            
        }){ (error) in
            NotificationCenter.default.post(name: .paymentFailed, object: nil)
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func getstatus_applePay(resourcePath:NSString) {
        NotificationCenter.default.post(name: .ExpectedTime, object: nil)
        let dic:[String:Any] = [
            "resourcePath": resourcePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, "userId":"\(getUserId())",
            "isMada":isMada,
            "isApplePay":isApplePay,
            "isSTCPay":isSTCPay,
            "InvoiceNo":"\(getInvoiceNo())",
            "OrderDate":"\(getOrderDate())",
            "ExpectedDate":"\(getExpectedDate())",
            "OrderStatus":"New"
        ]
        PaymentModuleServices.getPaymentStatusService(dic: dic, success: { (data) in
            if(data.Status == false){
                NotificationCenter.default.post(name: .paymentFailed, object: nil)
                if self.isApplePay == true {
                    self.dismiss(animated: false, completion: nil)
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .paymentSuccess, object: nil)
                        if self.isApplePay == true {
                            self.dismiss(animated: false, completion: nil)
                        }
                    }
                }else{
                    NotificationCenter.default.post(name: .paymentFailed, object: nil)
                    if self.isApplePay == true {
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }){ (error) in
            NotificationCenter.default.post(name: .paymentFailed, object: nil)
            if self.isApplePay == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
        /*  self.safariVC?.dismiss(animated: true, completion: {
         DispatchQueue.main.async {
         self.getstatus(transaction: self.transaction!)
         }
         }) */
        
        /*  self.checkoutProvider?.dismissCheckout(animated: true, completion: {
         DispatchQueue.main.async {
         self.getstatus(transaction: self.transaction!)
         }
         }) */
        
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                //print(self.transaction?.resourcePath as Any)
                self.getstatus(transaction: self.transaction!)
            }
        }
    }
    
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS" //Specify your format that you want
        return dateFormatter.string(from: date)
    }
}

extension Notification.Name {
    static let paymentFailed = Notification.Name("paymentFailed")
    static let paymentSuccess = Notification.Name("paymentSuccess")
    static let ExpectedTime = Notification.Name("ExpectedTime")

}
@available(iOS 13.0, *)
extension PaymentVC:PKPaymentAuthorizationViewControllerDelegate{
   func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        //print("cancel")
        NotificationCenter.default.post(name: .paymentFailed, object: nil)
        controller.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
 func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    print(self.checkoutID)
    print(payment.token.paymentData)
        if let params = try? OPPApplePayPaymentParams(checkoutID: self.checkoutID, tokenData: payment.token.paymentData) as OPPApplePayPaymentParams? {
            self.transaction  = OPPTransaction(paymentParams: params)
            //print(self.transaction!)
           // print(self.transaction!.resourcePath!)

            self.provider.submitTransaction(OPPTransaction(paymentParams: params), completionHandler: { (transaction, error) in
                if (error != nil) {
                    controller.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: .paymentFailed, object: nil)
                    self.dismiss(animated: false, completion: nil)
                    //print(error?.localizedDescription as Any)
                    DispatchQueue.main.async {
                        Alert.showAlert(on: self, title: "Failure", message: error!.localizedDescription)
                    }
                } else {
                    // Send request to your server to obtain transaction status.
                    controller.dismiss(animated: true, completion: nil)
                    self.provider.requestCheckoutInfo(withCheckoutID: self.checkoutID) { (checkoutInfo, error) in
                        //print(checkoutInfo!.resourcePath)
                        DispatchQueue.main.async {
                            guard let _ = checkoutInfo?.resourcePath else {
                                NotificationCenter.default.post(name: .paymentFailed, object: nil)
                                self.dismiss(animated: false, completion: nil)
                               return
                            }
                            self.getstatus_applePay(resourcePath: checkoutInfo!.resourcePath as NSString)
                            //(transaction: checkoutInfo!.resourcePath)
                        }
                    }
                }
            })
        }else{
            NotificationCenter.default.post(name: .paymentFailed, object: nil)
            self.dismiss(animated: false, completion: nil)
        }
    }
}
