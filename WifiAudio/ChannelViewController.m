//
//  ChannelViewController.m
//  AudioTest
//
//  Created by Miguel Hernandez on 5/24/16.
//  Copyright © 2020 Listen Technologies Inc. All rights reserved.
//

#import "ChannelViewController.h"
#import "EAEExxtractorConnection.h"
#import "EAEConnectionConstants.h"
#import "EAEAudioChannel.h"
#import "ChannelCell.h"

@interface ChannelViewController ()
//Declaration of connection manager and UI elements for channels view
@property (strong, nonatomic) EAEExxtractorConnection *audioEverywhereManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *disconnectButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (weak, nonatomic) IBOutlet UILabel *channelLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

//Declaration of variables for handling the events
@property (weak, nonatomic) EAEAudioChannel *currentChannel;
@property BOOL isPlaying;

@end

@implementation ChannelViewController
//Actions from channels view

//Refresh button implementation, gets the channels list of an exxtractor
- (IBAction)refreshChannels:(id)sender {
    [self.audioEverywhereManager startSyncingChannelsWithExxtractor:nil isPrivateChannelsEnabled:NO isAdminEnabled:NO];
}

//Disconnect button implementation
- (IBAction)disconnectFromSource:(id)sender {
    //Releases the connection manager with the current exxtractor
    [self.audioEverywhereManager stopAndResetManager];
    //redirects the flow to the scan
    [self.delegate navigateToScanning];
    //updates information in the play bar (bottom of the screen)
    //non channel is playing
    self.isPlaying = NO;
    //there is no current channel
    self.currentChannel = nil;
    //there is no name of current channel to display in play bar
    self.channelLabel.text = @"N/A";
    //play button of the play bar changes the state to normal (no selected)
    [self.playButton setImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
}

//Define actions to interact with the list of channels
- (IBAction)toggleChannel:(id)sender {
    //if the current channel is playing and connection manager has an active channel
    if (self.isPlaying && self.audioEverywhereManager.activeAudioChannel != nil){
        //the streaming will be stopped
        [self stopPlaybackOnActiveChannel];
    }
    //if there is no current channel
    else if (!self.isPlaying && self.currentChannel != nil){
        //assign current channel and starts the playback
        [self startPlaybackOnChannelWithIndexPath:self.currentChannel];
    }
}

//At the time the screen is loaded
-(void)viewDidLoad {
    [super viewDidLoad];
    //no channels were be playing
    self.isPlaying = NO;
    
    //notifications that you can handle to update your views.
    
    //Success  synchronization of the manager's channels with the ExXtractor
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(audioEverywhereManagerChannelsHaveBeenSyncedWithExxtractor:)
                                               name:kEAENotificationChannelsHaveBeenSyncedWithExxtractor
                                             object:nil];
    //playback has been interrupted
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(audioEverywhereManagerPlaybackWasInterrupted:)
                                               name:kEAENotificationPlaybackWasInterrupted
                                             object:nil];
    // Exxtractor connection was interrupted
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(audioEverywhereManagerExxtractorConnectionWasInterrupted:)
                                               name:kEAENotificationExxtractorConnectionWasInterrupted
                                             object:nil];
}

//At the time the view needs to reload info
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Check if there is a connection
    if ([EAEExxtractorConnection sharedExxtractorConnection]) {
        self.audioEverywhereManager = [EAEExxtractorConnection sharedExxtractorConnection];
    }
    //if there is no connection will redirect the flow to the scan view
    else {
        [self.delegate navigateToScanning];
    }
    //refresh channels info in UI with new info retrieved
    [self audioEverywhereManagerChannelsHaveBeenSyncedWithExxtractor:nil];
    
    
    //Starts the sync functionality, once strated the polling will continue to update the channels.
    [self.audioEverywhereManager startSyncingChannelsWithExxtractor:nil isPrivateChannelsEnabled:NO isAdminEnabled:NO];
    
    //if there was an active channel the name of the channel should be updated after reload (playbar)
    if (self.audioEverywhereManager.activeAudioChannel != nil) {
        self.channelLabel.text = self.audioEverywhereManager.activeAudioChannel.title;
    }
}

//verifies device memory to show a warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Playback methods

//method to start the playback
- (void) startPlaybackOnChannelWithIndexPath:(EAEAudioChannel *) channel {
    
    __weak __typeof(self)weakSelf = self;
    // Sets the success and failure responses of the playback changes
    EAEConnectionSuccessResponse managerSuccess = ^(NSArray *array){
        // Success manager
    };
    
    //error handler
    EAEConnectionFailureResponse managerFailure = ^(NSError *error){
        //error s will be displayed on a pop up
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:error.description
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancel = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
        //the alert will contain a cancel button to dismiss the pop up
        [alert addAction:cancel];
        //no actions will be performed after the error
        [weakSelf presentViewController:alert animated:YES completion:nil];
    };
    
    //setting for the streaming, functionalities like: stereo, enhance dialog
    [self.audioEverywhereManager startChannelPlaybackWithChannel:channel
                                           playStereoIfAvailable:YES
                                                         success:managerSuccess
                                                         failure:managerFailure];
    //update variables after the playback is succeed
    self.currentChannel = channel;
    self.channelLabel.text = channel.title;
    self.isPlaying = YES;
    //update icon in playbar (show pause icon)
    [self.playButton setImage:[UIImage imageNamed:@"PauseIcon"] forState:UIControlStateNormal];
}

//Stops the streaming of the current channel
-(void) stopPlaybackOnActiveChannel{
    // Sets the success and faiure responses of the playback changes
    EAEConnectionSuccessResponse managerSuccess = ^(NSArray *array){
        //success manager
    };
    EAEConnectionFailureResponse managerFailure = ^(NSError *error){
        // Failure manager
    };
    
    //call to stop the streaming, returns the success or fail response
    [self.audioEverywhereManager stopChannelPlaybackWithSuccess:managerSuccess
                                                        failure:managerFailure];
    //change the image o the play bar from pause to play button
    [self.playButton setImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
    //update playing variable
    self.isPlaying = NO;
}

#pragma mark - Table View methods

//Define container of channels list according to the number of channels
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.audioEverywhereManager.audioChannels.count;
}

//Define each row for channels (image and info sections)
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChannelCell" forIndexPath:indexPath];
    
    //get info of each channels from the object retrived to show it in the current row
    if (self.audioEverywhereManager.audioChannels.count > indexPath.row) {
        EAEAudioChannel *channel = self.audioEverywhereManager.audioChannels[indexPath.row];
        //show title
        cell.cellTitle.text = channel.title;
        //if it has detials will show the information icon
        if(channel.detail) {
            cell.moreInfo = channel.detail;
            cell.moreInfoButton.hidden = NO;
        } else {
            cell.moreInfoButton.hidden = YES;
        }
        //show channel image
        [cell setImage:channel];
    }
    //return the current cell with all info
    return cell;
}

//Define how all works a channel selected (paused or playing)
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //determine the channel selected
    if (self.audioEverywhereManager.audioChannels.count >= indexPath.row && !self.audioEverywhereManager.isBusy) {
        EAEAudioChannel *channel = self.audioEverywhereManager.audioChannels[indexPath.item];
        //if the channel is ready to play but not playing
        if(channel.state == ChannelStateReady || channel.state == ChannelStateStopped) {
            //set the channel name in the playbar and starts the streaming
            self.channelLabel.text = channel.title;
            [self startPlaybackOnChannelWithIndexPath: self.audioEverywhereManager.audioChannels[indexPath.item]];
        }
        //if the channel is playing then the playback should stop
        else if(channel.state == ChannelStatePlaying){
            [self stopPlaybackOnActiveChannel];
        }
    }
    
}

#pragma mark -
#pragma mark - Notification handlers

/*!
 Method which handles the notification for the synchronization of the
 manager's channels with the ExXtractor
 
 @param notification The notification for the synchronization of the
 manager's channels with the ExXtractor
 */
- (void) audioEverywhereManagerChannelsHaveBeenSyncedWithExxtractor: (NSNotification* )notification {
    // Refreshes the collection view
    [self.tableView reloadData];
}

/*!
 Method which handles the notification when a playback has been interrupted
 
 @param notification The notification when a playback has been interrupted
 */
- (void) audioEverywhereManagerPlaybackWasInterrupted: (NSNotification* )notification {
    //update state variableac
    self.isPlaying = NO;
    //update text of current channel in playbar
    self.channelLabel.text = @"N/A";
    //change iconin playbar to play icon
    [self.playButton setImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
    //update current channel reference
    self.currentChannel = nil;
    //verify the app still connected to the exxtractor to reload channels
    if (self.audioEverywhereManager.state == EAEExxtractorConnectionStateConnected  || self.audioEverywhereManager.state == EAEExxtractorConnectionStateConnectedPolling){
        [self.tableView reloadData];
    }
    //if it isn't connected goes to the scan screen
    else {
        [self.delegate navigateToScanning];
    }
}

/*!
 Method which handles the notification when the manager's state changed
 
 @param notification The notification when the manager's state changed
 */
- (void) audioEverywhereManagerExxtractorConnectionWasInterrupted: (NSNotification* )notification {
    //update varible of state
    self.isPlaying = NO;
    //update reference to current channel
    self.currentChannel = nil;
    //clears channel name of the playbar
    self.channelLabel.text = @"N/A";
    //sets playbar icon with play button
    [self.playButton setImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
    //goes to scan screen
    [self.delegate navigateToScanning];
}

//define info button action: shows the information in the current channel with "more info" title
-(void)showMore:(NSString *)moreInfo{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"More Info..."
                                  message:moreInfo
                                  preferredStyle:UIAlertControllerStyleAlert];
    //define cancel action with "ok" label
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:nil];
    [alert addAction:cancel];
    //animated alert
    [self presentViewController:alert animated:YES completion:nil];
}

@end
