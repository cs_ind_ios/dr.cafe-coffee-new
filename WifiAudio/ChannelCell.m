//
//  ChannelCell.m
//  AudioTest
//
//  Created by Miguel Hernandez on 5/24/16.
//  Copyright © 2016 Exxothermic. All rights reserved.
//

#import "ChannelCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ChannelCell

//define action to show more information about channel
- (IBAction)showMoreInfo:(id)sender {
    [self.delegate showMore:self.moreInfo];
}

-(void)setImage: (EAEAudioChannel *) channel{
    
    // Sets a random background color for the image view
    self.channelImage.backgroundColor = channel.backgroundColor;
    
    // Sets the channel's images if available
    if (channel.smallImageUrl) {
        [self.channelImage sd_setImageWithURL:channel.smallImageUrl];
    }
}
@end
