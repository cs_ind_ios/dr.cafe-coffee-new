//
//  ViewController.m
//  AudioTest
//
//  Created by Miguel Hernandez on 5/16/16.
//  Copyright © 2020 Listen Technologies Inc. All rights reserved.
//

#import "MainViewController.h"
#import "EAEExxtractorConnection.h"
#import "EAEConnectionConstants.h"
#import "ScanViewController.h"
#import "ChannelViewController.h"
//#import "SettingsViewController.h"

@interface MainViewController ()

//Declaration of main UI components for the app
//List of views
@property (weak, nonatomic) IBOutlet UIView *scanView;
@property (weak, nonatomic) IBOutlet UIView *aboutView;
@property (weak, nonatomic) IBOutlet UIView *channelsView;
//Items contained in the navigation bar
@property (weak, nonatomic) IBOutlet UITabBarItem *connectionItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *aboutItem;
//Navigation bar of the app
@property (weak, nonatomic) IBOutlet UITabBar *navigationBar;
//Current view: will determine the flow of the app
@property (weak, nonatomic) UIView *currentView;
//Object that will make the SDK management
@property (strong, nonatomic) EAEExxtractorConnection *audioEverywhereManager;
//Variable that will determine wich exxtractors will connect to the app
@property (strong, nonatomic) NSString *partnerId;

@end

@implementation MainViewController
//initialization of the app
- (void)viewDidLoad {
    [super viewDidLoad];
    //assign objects for the first load
    //scan component
    self.currentView = self.scanView;
    //selected option of the navigation bar
    self.navigationBar.selectedItem = self.connectionItem;
    //assing partner id for the app MYE exxtractors = 2 will be scanned
    self.partnerId = @"2";
    //stablish connection with an exxtractor (according the partner id) if there is no connection
    if ([EAEExxtractorConnection sharedExxtractorConnection]) {
        self.audioEverywhereManager = [EAEExxtractorConnection sharedExxtractorConnection];
    } else {
        self.audioEverywhereManager = [EAEExxtractorConnection exxtractorConnectionWithPartnerUniqueIdentifier:@"2"
                                                                                                      loglevel:EAEExxtractorConnectionLogLevelFull];
    }
}

//verifies device memory to show a warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Define transitions of the app depending of the parameter id
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //Transition to scan view
    if ([segue.identifier  isEqual: @"embedScan"]){
        ScanViewController *vc = segue.destinationViewController;
        vc.delegate = self;
        vc.configDelegate = self;
    }
    //Transition to channels view
    else if ([segue.identifier  isEqual: @"embedChannels"]){
        ChannelViewController *vc = segue.destinationViewController;
        vc.delegate = self;
        vc.configDelegate = self;
    }
    //Transition to about view
    else if ([segue.identifier  isEqual: @"embedAbout"]){
        
    }
}

//Define behavior of navigation bar
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    //Selected: channels view
    if (item == self.connectionItem){
        [self navigateToChannels];
    }
    //Selected about view
    else if (item == self.aboutItem){
        [self navigateToAbout];
    }
}

//Define component for each view

//Show UI components for scanning view
-(void)navigateToScanning {
    if (self.currentView != self.scanView){
        self.currentView.hidden = true;
        self.scanView.hidden = false;
        self.currentView = self.scanView;
    }
}

//Show UI components for about view
-(void)navigateToAbout {
    if (self.currentView != self.aboutView){
        self.currentView.hidden = true;
        self.aboutView.hidden = false;
        self.currentView = self.aboutView;
    }
}

//Show UI components for channels view
-(void)navigateToChannels{
    //verify if the connection is active to get the info of channels
    if (self.audioEverywhereManager.state == EAEExxtractorConnectionStateConnected  || self.audioEverywhereManager.state == EAEExxtractorConnectionStateConnectedPolling){
        if (self.currentView != self.channelsView){
            self.currentView.hidden = true;
            self.channelsView.hidden = false;
            self.currentView = self.channelsView;
        }
    }
    //If there is no connection, go to scan view
    else {
        [self navigateToScanning];
    }
}

//Retrieves the current partner id
-(NSString *)getPartnerId{
    return self.partnerId;
}

@end


