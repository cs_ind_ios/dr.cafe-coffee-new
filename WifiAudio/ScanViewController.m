//
//  ViewController.m
//  AudioTest
//
//  Created by Miguel Hernandez on 4/25/16.
//  Copyright © 2020 Listen Technologies Inc. All rights reserved.
//

#import "ScanViewController.h"
#import "EAEExxtractorConnection.h"
#import "EAEConnectionConstants.h"

@interface ScanViewController ()

//Declaration of components for the scan view
//connection manager
@property (strong, nonatomic) EAEExxtractorConnection *audioEverywhereManager;
//UI scan button
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@end

@implementation ScanViewController

//Preparing the view to be added
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //if there is an active connection update the connection manager.
    if ([EAEExxtractorConnection sharedExxtractorConnection]) {
        self.audioEverywhereManager = [EAEExxtractorConnection sharedExxtractorConnection];
    }
    //if there is no connection, create a new connection using the partner id
    else {
        self.audioEverywhereManager = [EAEExxtractorConnection exxtractorConnectionWithPartnerUniqueIdentifier:[self.configDelegate getPartnerId]
                                                                               loglevel:EAEExxtractorConnectionLogLevelFull];
    }
}

//verifies device memory to show a warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn_tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//Scanning action
- (IBAction)startScaning:(id)sender {
    
    //reference to the current view
    __weak ScanViewController *weakSelf = self;
    
    //Update the label in the scan button
    [self.scanButton setTitle:@"Scanning..." forState:UIControlStateNormal];
    //disable interaction with the scan button
    self.scanButton.userInteractionEnabled = NO;
    // Starts the scan, 15 seconds timeout
    [self.audioEverywhereManager scanWithTimeout:15
                                        //no timeout
                                         success:^(NSArray *array){
                                            // Sets the success response for location
                                            EAEConnectionSuccessResponse success = ^(NSArray *result)
                                            {
                                               NSLog(@"Location retrieved");
                                                //if no channels returned
                                                [weakSelf.audioEverywhereManager startSyncingChannelsWithExxtractor:nil isPrivateChannelsEnabled:NO isAdminEnabled:NO];
                                                [weakSelf.delegate navigateToChannels];
                                                //updates the text in the scan button
                                                [weakSelf.scanButton setTitle:@"Start Scan" forState:UIControlStateNormal];
                                                // enables the intaction with the scan button
                                                weakSelf.scanButton.userInteractionEnabled = YES;                                                
                                                
                                            };
                                            
                                            // Sets the failure response for location
                                            EAEConnectionFailureResponse failure = ^(NSError *error)
                                            {
                                                NSLog(@"Error requesting location");
                                               [weakSelf handleAudioEverywhereError:error];
                                            };
                                            
                                             // Requests the audio everywhere manager to obtain the location
                                             [weakSelf.audioEverywhereManager getLocationWithSuccess:success
                                                                                            failure:failure];
        
                                             
                                         }
                                        //timeout
                                         failure:^(NSError *error) {
                                             [weakSelf handleAudioEverywhereError:error];
                                             //updates the text in the scan button
                                             [weakSelf.scanButton setTitle:@"Start Scan" forState:UIControlStateNormal];
                                             // enables the intaction with the scan button
                                             weakSelf.scanButton.userInteractionEnabled = YES;
                                         }];
}

//Connection error handler
- (void) handleAudioEverywhereError:(NSError *)error {
    //Declariation of error message variable
    NSString *errorMessage = @"";
    //
    if (error.code == EAEExxtractorConnectionErrorCodeForInvalidLocation) {
        errorMessage = kEAEErrorDescriptionForInvalidLocation;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForInterruptedScan){
        errorMessage = kEAEErrorDescriptionForInterruptedScan;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForNoConnectedService){
        errorMessage = kEAEErrorDescriptionForNoConnectedService;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForScanTimeout){
        errorMessage = kEAEErrorDescriptionForScanTimeout;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForUnavailableService){
        errorMessage = kEAEErrorDescriptionForUnavailableService;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForUnknownError){
        errorMessage = error.description;
    }
    else if (error.code == EAEExxtractorConnectionErrorCodeForUnreachableLAN){
        errorMessage = kEAEErrorDescriptionForUnreachableLAN;
    }
    
    //Declaration of pop up component to display errors found
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Error"
                                 message:errorMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Declaration of cancel action for the alert message
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:nil];
    
    //Add cancel action to alert message
    [alert addAction:cancel];
    //shows the alert with a subtle animation and after cancel it nothing else will be executed
    [self presentViewController:alert animated:YES completion:nil];
}

@end
