//
//  ChannelViewController.h
//  AudioTest
//
//  Created by Miguel Hernandez on 5/24/16.
//  Copyright © 2016 Exxothermic. All rights reserved.
//

#import "MainViewController.h"

@protocol ChannelDelegate <NSObject>

-(void)showMore: (NSString*) partnerId ;

@end

@interface ChannelViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, ChannelDelegate>

@property (nonatomic, weak) id<NavigationDelegate> delegate;
@property (nonatomic, weak) id<ConfigurationDelegate> configDelegate;

@end