//
//  ViewController.h
//  AudioTest
//
//  Created by Miguel Hernandez on 5/16/16.
//  Copyright © 2016 Exxothermic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavigationDelegate <NSObject>
//Methods contained in the controller to navigate
-(void)navigateToScanning;
-(void)navigateToAbout;
//-(void)navigateToConfiguration;
-(void)navigateToChannels;

@end

@protocol ConfigurationDelegate <NSObject>

//Methods and variables defined for configuration purposes
-(void)updatePartnerId: (NSString*) partnerId ;
-(NSString*)getPartnerId;

@end

//this interface will use the protocols NavigationDelegate and ConfigurationDelegate
@interface MainViewController : UIViewController<NavigationDelegate, ConfigurationDelegate, UITabBarDelegate>

@end


