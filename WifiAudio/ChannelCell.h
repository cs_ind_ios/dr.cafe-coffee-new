//
//  ChannelCell.h
//  AudioTest
//
//  Created by Miguel Hernandez on 5/24/16.
//  Copyright © 2020 Listen Technologies Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAEAudioChannel.h"
#import "ChannelViewController.h"

@interface ChannelCell : UITableViewCell
@property NSString *moreInfo;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoButton;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UIImageView *channelImage;
- (IBAction)showMoreInfo:(id)sender;
- (void)setImage: (EAEAudioChannel *) channel;
@property (nonatomic, weak) id<ChannelDelegate> delegate;

@end
