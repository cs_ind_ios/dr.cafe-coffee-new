//
//  ViewController.h
//  SDKDemo
//
//  Created by Miguel Hernandez on 4/25/16.
//  Copyright © 2016 exxothermic. All rights reserved.
//

#import "MainViewController.h"

@interface ScanViewController : UIViewController

@property (nonatomic, weak) id<NavigationDelegate> delegate;
@property (nonatomic, weak) id<ConfigurationDelegate> configDelegate;

@end

